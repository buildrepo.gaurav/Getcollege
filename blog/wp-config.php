<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'build123repo');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bXF80};a_+6qcZF[QC$aDsDab?#6SZ~b )2O7KyCOCriYjVxU$$+;vn NI!Wb*w(');
define('SECURE_AUTH_KEY',  '2 _Z?To/W`@D]O| U?V6T fT0gF(|,LsOyOij_[/3=Z7@)/aiKI*4:@psH!S,3o ');
define('LOGGED_IN_KEY',    ')gh>ZGx,=;hZ.##cw`7RPFT?DV#>IQD7%BE>_>^!f5tcW:h:AG*/RyyT%j;f/3+Q');
define('NONCE_KEY',        '22erw1]NP$hsA8CC}FbzvZjSgNmOD+a3q+V$gPmn;i.)&*hd%P>/sFOfvK~iB;E%');
define('AUTH_SALT',        'O&|;jGjH]<O1S3f]65z=i3v1fkYJgpQ3$}..N30#1HOHaq~}/|{3bJT+J,(B~bna');
define('SECURE_AUTH_SALT', 'b[.A[G2M7rXVQSY8aGs I!$h 2-F5.P$;>3V5I]Nsm0|>dGg($wJj^OwntxqM#cw');
define('LOGGED_IN_SALT',   '7QAjk@4]z#3RD]j!uoA+<C^uu~@Kg$E/HXU )mD[m>g-,AipIQ^ssPgYP^ZG&RaT');
define('NONCE_SALT',       '33CkmiNi_p%9M(Xg8:,DtM-y:`&am:U&(%[4MmD9,kP~.Xi]#~2;S8S0&Q#m!#1s');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
