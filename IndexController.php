<?php
 
class Buildrepo_Buildapi_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        echo 'Hello developer...';
    }
 
     public function productlistAction() {
        try {
            $param = Mage::app()->getRequest()->getParams();
		//	var_dump($param);die;
            $limit = 20;
            $cat_id = $param['cat_id'];
            if (empty($cat_id)) {
                $jsonArray['status'] = 'false';
                $jsonArray['msg'] = "Please enter valid category id.";
                //$jsonArray['returnCode'] = array('result' => 0, 'resultText' => 'fail');
                echo json_encode($jsonArray);
                die;
            }
            if ($param['page_id'])
                $page_no = $param['page_id'];
            else
                $param['page_id'] = 1;

            //-----custom option product id array------//--------Remove it in next version--------- 
            /* $Option=Mage::getResourceModel('catalog/product_option_collection')->addFieldToSelect('product_id');
              $Option->getSelect()->group('main_table.product_id');
              $entityIds = new Zend_Db_Expr($Option->getSelect()->__toString());
             */
            $catagory_model = Mage::getModel('catalog/category')->load($cat_id);
            $collection = Mage::getResourceModel('catalog/product_collection'); //->addAttributeToSort('position');
            $collection->addCategoryFilter($catagory_model); //category filter
            $collection->addAttributeToFilter('status', 1);
            $collection->addAttributeToSelect('image');
            $collection->addMinimalPrice();
            $collection->addAttributeToFilter('visibility', array('eq' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH));
            //$collection->addAttributeToFilter('type_id',array('in'=>array('simple')));//--will comment later in next version----------
            $collection->addAttributeToSelect('*');
            //$collection->getSelect()->where('e.entity_id not in(?)', $entityIds);//--will comment later in next version----------
            //echo $count = $collection->count();
          /*  if ($this->activePackage == self::Basic_Package) {

                $Option = Mage::getResourceModel('catalog/product_option_collection')->addFieldToSelect('product_id');
                $Option->getSelect()->group('main_table.product_id');
                $entityIds = new Zend_Db_Expr($Option->getSelect()->__toString());

                $collection->addAttributeToFilter('entity_id', array('nin' => $entityIds));
                $collection->addAttributeToFilter('type_id', array('in' => array('simple')));
            } else if ($this->activePackage == self::Basic_Exd_Package) {
                $collection->addAttributeToFilter('type_id', array('in' => array('simple')));
            } else if ($this->activePackage == self::Silver_Package) {
                $collection->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable', 'virtual')));
            } */

            if (isset($param['sort']) and ! empty($param['sort'])) {
                $sortArray = explode("_", $param['sort']);

                $order = $sortArray[0];
                $direction = strtoupper($sortArray[1]);

                $collection->addAttributeToSort($order, $direction);
                //$collection->getSelect()->order("$order $direction");
            } else {
                $collection->addAttributeToSort("position");
            }

            $colcBkp = clone $collection;
            $prod_count = count($colcBkp);

            $collection->setPageSize($limit)->setCurPage($page_no);
            /* $more = count($collection);
              if($more < $limit)
              $data['more'] = 0;
              else
              $data['more'] = 1; */
            $pageno = $param['page_id'];
            if ($prod_count <= ($limit * $pageno))
                $data['more'] = 0;
            else
                $data['more'] = 1;

            $data['currency_symbol'] = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
            $data['product'] = array();
            $i = 0;

            $update = $this->getLayout()->getUpdate();
            //$update->addHandle('default');
            //$this->addActionLayoutHandles();
            $update->addHandle('catalog_category_default');
            $this->loadLayoutUpdates();
            $this->generateLayoutXml();
            $this->generateLayoutBlocks();

            foreach ($collection as $product) {
                $data['product'][$i]['product_id'] = $product->getId();
                $data['product'][$i]['type_id'] = $product->getTypeId();
                $data['product'][$i]['name'] = $product->getName();
                $data['product'][$i]['final_price'] = number_format($product->getFinalPrice(), 2);
                $data['product'][$i]['price'] = number_format($product->getPrice(), 2);

                $data['product'][$i]['price_html'] = strip_tags($this->getLayout()->getBlock('product_list')->getPriceHtml($product, true));
                $imgUrl = '';
                if ($product->getImage() && (($product->getImage()) != 'no_selection')) {
                    $imgUrl = Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getImage());
                }

                if (empty($imgUrl)) {
                    if ($product->getThumbnail() && (($product->getThumbnail()) != 'no_selection')) {
                        $imgUrl = Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getThumbnail());
                    }
                }

                $data['product'][$i]['image'] = $imgUrl; //$product->getImageUrl();
                $data['product'][$i]['in_stock'] = $product->isSalable();
                $data['product'][$i]['created'] = $product->getcreated_at();

                $RatingOb = Mage::getModel('rating/rating')->getEntitySummary($product->getId());
                $ratings = $RatingOb->getCount()>0? ($RatingOb->getSum() / $RatingOb->getCount()): false;
                if ($ratings == false) {
                    $ratings = 0;
                }
                $data['product'][$i]['rating'] = $ratings;
                ++$i;
            }

            $jsonArray['data'] = $data;
            $jsonArray['status'] = 'true';
            $jsonArray['msg'] = 'success';
        } catch (Exception $e) {
            $jsonArray['msg'] = $e->getMessage();

            $jsonArray['status'] = 'false';
        }
        //echo "<pre>";
        //print_r($jsonArray);

        echo json_encode($jsonArray);
        die;
    }

     public function loginAction() {
        $json = json_decode(file_get_contents("php://input"));

     	$email = trim($json->login['0']->email);
     	$password = trim($json->login['0']->password);

     	if(!empty($email) && !empty($password)){
	     	$session = Mage::getSingleton( 'customer/session' );
			Mage::app()->getStore()->setWebsiteId(1);
			
			try {
                $resp = [];
			    if($session->login( $email, $password )){
    			    $customer = $session->getCustomer();
                    $resp['data'] = Mage::getModel('customer/customer')->load($customer->getId())->getData();
                    
                    $resp['status'] = "true";
                    $resp['msg'] = "success";
                    echo json_encode($resp);die;
                }else{
                    $resp['status'] = "false";
                    $resp['msg'] = "Authentication failed.";
                    echo json_encode($resp);die;
                }
			    //return json_encode(array('status' => 'OK', 'userData' =>$resp)));
			}
			catch( Exception $e ) {
                $resp['status'] = 'false';
                $resp['msg'] = $e->getMessage();
                echo json_encode($resp);die;
			}
		}else{
             $resp['status'] = "false";
              $resp['msg'] = "Email or Password can not be empty.";
              echo json_encode($resp);die;
			
		}	
		
     }


     public function signupAction(){
        //print_r($_POST);die;
        $json = json_decode(file_get_contents("php://input"));
        $email = trim($json->register['0']->email);
        if(empty($email)){
            $resp['status'] = "false";
            $resp['msg'] = "Email can not be empty.";
            echo json_encode($resp);die;
            
        }
        $lname = trim($json->register['0']->lname);
         if(empty($lname)){
            $resp['status'] = "false";
            $resp['msg'] = "Last Name can not be empty.";
            echo json_encode($resp);die;
            
        }
        $fname = trim($json->register['0']->fname);
         if(empty($fname)){
            $resp['status'] = "false";
            $resp['msg'] = "First Name can not be empty.";
            echo json_encode($resp);die;
            
        }
         $password = trim($json->register['0']->password);
         if(empty($password)){
            $resp['status'] = "false";
            $resp['msg'] = "Password can not be empty.";
            echo json_encode($resp);die;
            
        }
        $websiteId = Mage::app()->getWebsite()->getId();
        $store = Mage::app()->getStore();
         
        $customer = Mage::getModel("customer/customer");
        $customer   ->setWebsiteId($websiteId)
                    ->setStore($store)
                    ->setFirstname($fname)
                    ->setLastname($lname)
                    ->setEmail($email)
                    ->setPassword($password);
         $resp = [];
        try{
            if($customer->save()){
                $resp['status'] = "true";
                $resp['msg'] = "Customer created successfully.";
                    $storeId = $customer->getSendemailStoreId();
                    $customer->sendNewAccountEmail('confirmation', '', $storeId);
                   
                echo json_encode($resp);die;
            }

        }
        catch (Exception $e) {
            $resp['status'] = 'false';
            $resp['msg'] = $e->getMessage();
            echo json_encode($resp);die;
        }
     }

     public function getCategoryAction(){
        $_helper = Mage::helper('catalog/category');
        $_categories = $_helper->getStoreCategories();
        $resp = [];
        if (count($_categories) > 0){
            $i =0;
            foreach($_categories as $_category){
                $_category = Mage::getModel('catalog/category')->load($_category->getId());
                $_subcategories = $_category->getChildrenCategories();
                $resp['Category'][$i]['name'] = $_category->getName();
                $resp['Category'][$i]['id'] = $_category->getId();
                if (count($_subcategories) > 0){ 
                    $j =0;    
                    foreach($_subcategories as $_subcategory){
                        $resp['Category'][$i]['Subcategory'][$j]['name'] = $_subcategory->getName();
                        $resp['Category'][$i]['Subcategory'][$j]['id'] = $_subcategory->getId();
                         
                         $j++;
                    }
                }
                $i++;
            }
            $resp['status'] = 'true';
            $resp['msg'] = 'success';
            echo json_encode($resp);die;
        }else{
            $resp['status'] = 'true';
            $resp['msg'] = 'No Category Found.';
            echo json_encode($resp);die;
        }
     }


     public function getCustomerOrderDetailsAction(){
         $param = Mage::app()->getRequest()->getParams();
        $jsonArray = [];        
        $id = $param['customer_id'];
        if (empty($id)) {
            $jsonArray['status'] = 'false';
            $jsonArray['msg'] = "Please enter valid Customer id.";
            //$jsonArray['returnCode'] = array('result' => 0, 'resultText' => 'fail');
            
        }
        $orderCollection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('customer_id',$id)->addFieldToSelect('*');
            $i=0;
            foreach($orderCollection as $order){
            
                $jsonArray['data'][$i] = $order->getData();
                $i++;
            }
            $jsonArray['status'] = 'true';
            $jsonArray['msg'] = "success";
           
            if(empty($jsonArray['data'])){
                $jsonArray['status'] = 'false';
                $jsonArray['msg'] = "No Order Found.";
            }
            echo json_encode($jsonArray);
            die;
        
     }

    public function getProductDetailsAction(){
        $param = Mage::app()->getRequest()->getParams();
        $product_id = trim($param['pro_id']);
        if(!empty($product_id)){
            $model = Mage::getModel('catalog/product'); //getting product model
            $_product = $model->load($product_id); //getting product object for particular product id
            $resp = [];

            $resp['data']['id'] = $product_id;
            $resp['data']['ShortDescription'] =  $_product->getShortDescription(); //product's short description
            $resp['data']['escription'] =  $_product->getDescription(); // product's long description
            $resp['data']['Name'] =  $_product->getName(); //product name
            $resp['data']['Price'] =  $_product->getPrice(); //product's regular Price
            $resp['data']['SpecialPrice'] =  $_product->getSpecialPrice(); //product's special Price
            //$resp['data'][''] =  $_product->getProductUrl(); //product url
            $resp['data']['ImageUrl'] =  $_product->getImageUrl(); //product's image url
            $resp['data']['SmallImageUrl'] =  $_product->getSmallImageUrl(); //product's small image url
            $resp['data']['ThumbnailUrl'] =  $_product->getThumbnailUrl(); //product's thumbnail image url 

            $resp['data']['gallery'] = $_product->getMediaGallery('images');
            //print_r($galleryData);die;
           /* $attributes = $_product->getAttributes();
            //print_r($attributes);die;
            foreach ($attributes as $attribute) { 
                
                    $attributeLabel = $attribute->getFrontendLabel();
                    $value = $attribute->getFrontend()->getValue($product);
                    $resp['data'][$attributeLabel] = $value;
                    
                
            }*/
            $resp['status'] = 'true';
            $resp['msg'] = "success";
        }else{
            $resp['status'] = 'false';
            $resp['msg'] = "Product Not Found.";
        }   
        echo json_encode($resp);
        die; 
    } 

    public function addToWishlistAction(){
        $json = json_decode(file_get_contents("php://input"));
       // print_r($json);
        $customerId = trim($json->wishlist['0']->customerId);
        $product_id = trim($json->wishlist['0']->productId);
        $resp = [];
        if(empty($customerId)){
            $resp['status'] = "false";
            $resp['msg'] = "Customer Id can not be empty.";
            echo json_encode($resp);die;
            
        }
        if(empty($product_id)){
            $resp['status'] = "false";
            $resp['msg'] = "Product Id can not be empty.";
            echo json_encode($resp);die;
            
        }
        try{
            $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customerId, true);
            $product = Mage::getModel('catalog/product')->load($product_id);

            $buyRequest = new Varien_Object(array()); // any possible options that are configurable and you want to save with the product

            $result = $wishlist->addNewItem($product, $buyRequest);
            $wishlist->save();
            $resp['status'] = "true";
            $resp['msg'] = "Product added to wishlist.";
            }
        catch (Exception $e) {
            $resp['status'] = 'false';
            $resp['msg'] = $e->getMessage();
            echo json_encode($resp);die;
        }    
        
        echo json_encode($resp);
        die;
    }

    public function deleteWishlistAction(){
        $json = json_decode(file_get_contents("php://input"));
        //print_r($json);
        $customerId = trim($json->wishlist['0']->customerId);
        $product_id = trim($json->wishlist['0']->productId);
        $resp = [];
        if(empty($customerId)){
            $resp['status'] = "false";
            $resp['msg'] = "Customer Id can not be empty.";
            echo json_encode($resp);die;
            
        }
        if(empty($product_id)){
            $resp['status'] = "false";
            $resp['msg'] = "Product Id can not be empty.";
            echo json_encode($resp);die;
            
        }
        try{
            $k=0;
            $itemCollection = Mage::getModel('wishlist/item')->getCollection()
                        ->addCustomerIdFilter($customerId);

            foreach($itemCollection as $item) {
                //print_r($item);
                if($item->getProduct()->getId() == $product_id){
                    $item->delete();
                    $resp['status'] = "true";
                    $resp['msg'] = "Product deleted from whislist.";
                    $k = 1;
                }
            }

            if($k == 0){
                $resp['status'] = 'false';
                $resp['msg'] = 'Product not in wishlist';
                echo json_encode($resp);die;  
            }

        }
        catch (Exception $e) {
            $resp['status'] = 'false';
            $resp['msg'] = $e->getMessage();
            echo json_encode($resp);die;
        }    
        
        echo json_encode($resp);
        die;    
    }

    public function getWishlistAction(){
       $json = json_decode(file_get_contents("php://input"));
        //print_r($json);
        $customerId = trim($json->wishlist['0']->customerId);
        $product_id = trim($json->wishlist['0']->productId);
        $resp = [];
        if(empty($customerId)){
            $resp['status'] = "false";
            $resp['msg'] = "Customer Id can not be empty.";
            echo json_encode($resp);die;
            
        }
        if(empty($product_id)){
            $resp['status'] = "false";
            $resp['msg'] = "Product Id can not be empty.";
            echo json_encode($resp);die;
            
        } 
        try{
            $itemCollection = Mage::getModel('wishlist/item')->getCollection()
                        ->addCustomerIdFilter($customerId);
            $model = Mage::getModel('catalog/product'); //getting product model
            $k = 0;
            $resp = [];
            foreach($itemCollection as $item) {
                   
                $_product = $model->load($product_id); //getting product object for particular product id
                
                $resp['data'][$k]['id'] = $item->getProduct()->getId();
                $resp['data'][$k]['ShortDescription'] =  $_product->getShortDescription(); //product's short description
                $resp['data'][$k]['escription'] =  $_product->getDescription(); // product's long description
                $resp['data'][$k]['Name'] =  $_product->getName(); //product name
                $resp['data'][$k]['Price'] =  $_product->getPrice(); //product's regular Price
                $resp['data'][$k]['SpecialPrice'] =  $_product->getSpecialPrice(); //product's special Price
                //$resp['data'][$k][''] =  $_product->getProductUrl(); //product url
                $resp['data'][$k]['ImageUrl'] =  $_product->getImageUrl(); //product's image url
                $resp['data'][$k]['SmallImageUrl'] =  $_product->getSmallImageUrl(); //product's small image url
                $resp['data'][$k]['ThumbnailUrl'] =  $_product->getThumbnailUrl(); //product's thumbnail image url 

                $resp['data'][$k]['gallery'] = $_product->getMediaGallery('images');
                $k++;    
            }
            $resp['status'] = "true";
            $resp['msg'] = "success";
        }
        catch (Exception $e) {
            $resp['status'] = 'false';
            $resp['msg'] = $e->getMessage();
            echo json_encode($resp);die;
        }    
        
        echo json_encode($resp);
        die;
    }

    public function addToCartAction(){
        $json = json_decode(file_get_contents("php://input"));
        //print_r($json);

        $customerId = trim($json->cart['0']->customerId);
        $product_id = trim($json->cart['0']->productId);
        $qty = trim($json->cart['0']->quantity);
        $resp = [];

        if(empty($customerId)){
            $resp['status'] = "false";
            $resp['msg'] = "Customer Id can not be empty.";
            echo json_encode($resp);die;
            
        }
        if(empty($product_id)){
            $resp['status'] = "false";
            $resp['msg'] = "Product Id can not be empty.";
            echo json_encode($resp);die;
            
        } 
        if(empty($qty)){
            $resp['status'] = "false";
            $resp['msg'] = "Quantity can not be empty.";
            echo json_encode($resp);die;
            
        } 
        try
        {
            $product = Mage::getModel('catalog/product')->load($product_id);
            
            if($product->getId()){
                $customer_detail = Mage::getModel('customer/customer')->load($customerId);
                if($customer_detail->getId()){
                    $storeIds = Mage::app()->getWebsite(Mage::app()->getWebsite()->getId())->getStoreIds();
                    $quote = Mage::getModel('sales/quote')->setSharedStoreIds($storeIds)->loadByCustomer($customer_detail);
                    $quote->addProduct($product, $qty);
                    $quote->collectTotals()->save();

                    $resp['status'] = "true";
                    $resp['msg'] = "Product added to cart.";
                }else{
                    $resp['status'] = 'false';
                    $resp['msg'] = 'Customer not exist';
                }
            }else{
                $resp['status'] = 'false';
                $resp['msg'] = 'Product not found';
            }
        }
        catch (Exception $e) {
            $resp['status'] = 'false';
            $resp['msg'] = $e->getMessage();
            echo json_encode($resp);die;
        }    
        
        echo json_encode($resp);
        die;    
    }

    public function getCartAction(){
        $param = Mage::app()->getRequest()->getParams();
        $customerId = trim($param['customerId']);
        $customer_detail = Mage::getModel('customer/customer')->load($customerId);

        $storeIds = Mage::app()->getWebsite(Mage::app()->getWebsite()->getId())->getStoreIds();
        $quote = Mage::getModel('sales/quote')->setSharedStoreIds($storeIds)->loadByCustomer($customer_detail);
        print_r($quote);
        print_r($quote->getAllItems());die;
        if ($quote) {

            $productsResult = array();
            foreach ($quote->getAllItems() as $item) { 

                $product = $item->getProduct(); 

                $productsResult[] = array(// Basic product data
                    'product_id' => $product->getId(),
                    'sku' => $product->getSku(),
                    'name' => $product->getName(),
                    'set' => $product->getAttributeSetId(),
                    'type' => $product->getTypeId(),
                    'category_ids' => $product->getCategoryIds(),
                    'website_ids' => $product->getWebsiteIds()
                );
            }
            return $productsResult;
        }
    }

    public function emptyCartAction(){
        $param = Mage::app()->getRequest()->getParams();
        $customerId = trim($param['customerId']);
        $resp = [];
        if(empty($customerId)){
            $resp['status'] = "false";
            $resp['msg'] = "Customer Id can not be empty.";
            echo json_encode($resp);die;
            
        }
        try{
            $session = Mage::getSingleton('customer/session');
            $session->loginById($customerId);
            Mage::getSingleton('checkout/cart')->truncate()->save();
            Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
            $resp['status'] = 'true';
            $resp['msg'] = 'Cart empty successfully';
            echo json_encode($resp);die;
        }    
        catch (Exception $e) {
            $resp['status'] = 'false';
            $resp['msg'] = $e->getMessage();
            echo json_encode($resp);die;
        }   
        die;
    }
}
?>