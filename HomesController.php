<?php
ob_start();


App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
App::Import('Component','UploadComponent');
App::import('Vendor','TCPDF',array('file' => 'tcpdf/tcpdf.php'));  
class HomesController extends AppController {

    public $layout = 'home';
    public $uses = array('User');
  public $helpers = array('Session','Pdf');
  var $components = array('Session','Resize','Email','Auth' => array(
                          'loginAction' => array('controller'=>'homes', 'action'=>'index'),
                          'loginRedirect' => array('controller' => 'homes', 'action' => 'dashboard'),
                          'logoutRedirect' => array('controller' => 'homes', 'action' => 'login'),
                          'authenticate' => array('all' => array('scope' => array('User.status' => 1)),'Form' => array( 'fields' => array('username' => 'email'))),
                          'authError'=>"You can't access that page",
                          'authorize'=>array('Controller')
                         )
              );


    public function beforeFilter()      
     {
     parent::beforeFilter();

    $this->Auth->allow('med_reg','med_cul_mailstd', 'med_cul_eventtime','med_sports_co','med_sports','med_sport_tt_sch','med_sport_tt_rule','med_sport_cricket_sch','med_sport_cricket_rule','med_sport_chess_sch','med_sport_chess_rule','med_sport_carram_rule','med_sport_badmintion_sch','med_sport_badmintion','medhotsav','reg_form','roll_check','newsletter','innovativeproject','stguidline','marksheet','downloadform','notice','landingpage2','campus_placement','landingpage','admission_enquiry','uc','guest_lecture','ieee','thanks','view360','events','mediagallery','infrastructure','galler','careers','gems','why_iec','technologies','facilities','getimage','career','company','visit','conference','paper','record','partner','training_partners','officials','upswd_scholarship_list','contacts1','terms','privacy','career_series','career_guide','disclosure','login','rnd','placement_overview','manuscripts','journal','logout','index','registration_page','forgotpass','iecLogin','SaveContact','contacts','gallery','video','media','infra','quicklinks','international_collaboration','rform_1','rform_2','rform_3','rform_4','otp','otp_check'); 


   
      }
      public function uc()
      {
        $this->layout=null;
      }
      public function technologies()
      {

      }
      public function admission_enquiry()
      {

      }
      public function galler($id=null){
          $cat_id = $id;
          $this->loadModel('Category');
  $this->loadModel('Gallery');
  $this->Category->bindModel(
                            array(
                                  'hasMany' => array(
                                                    'Gallery' => array(
                                                                        'className' => 'Gallery',
                                                                        'foreignKey' => 'category_id'
                                                                      )
                                                    )
                                  )
                            );
  $galleries = $this->Category->findById($cat_id);
  //print_r($galleries);die;
$this->set(compact('galleries'));
      }
       public function mediagallery($id=null){
          $cat_id = $id;
          
          $this->loadModel('Category');
  $this->loadModel('Gallery');
  $this->Category->bindModel(
                            array(
                                  'hasMany' => array(
                                                    'Gallery' => array(
                                                                        'className' => 'Gallery',
                                                                        'foreignKey' => 'category_id'
                                                                      )
                                                    )
                                  )
                            );
  $galleries = $this->Category->findById($cat_id);
//  print_r($galleries);die;
$this->set(compact('galleries'));
      }
       public function infrastructure($id=null){
          $cat_id = $id;
          
          $this->loadModel('Category');
  $this->loadModel('Gallery');
  $this->Category->bindModel(
                            array(
                                  'hasMany' => array(
                                                    'Gallery' => array(
                                                                        'className' => 'Gallery',
                                                                        'foreignKey' => 'category_id'
                                                                      )
                                                    )
                                  )
                            );
  $galleries = $this->Category->findById($cat_id);
  //print_r($galleries);die;
$this->set(compact('galleries'));
      }
      public function gems()
      {
          $this->loadModel('Gem');
          $gems = $this->Gem->find('all');
          $this->set(compact('gems'));

      }
      public function events()
      {
          $this->loadModel('Event');
          $gems = $this->Event->find('all');
          $this->set(compact('gems'));

      }
      public function conference()
      {

      }
      public function why_iec()
      {

      }
      public function career()
      {

      }
      public function company() {

       }
      public function paper()
      {

      }
      public function facilities()
      {

      }
      public function visit()
      {

      }
      public function training_partners()
      {

      }
      public function placement_overview()
    {

      }
      public function upswd_scholarship_list()
      {

      }
    public function international_collaboration()
    {

      }
      public function thanks()
      {

      }
      public function record()
      {
        $this->loadModel('Record');
        $records = $this->Record->find('all');
        $this->set(compact('records'));
      }
      public function partner()
      {
        $this->loadModel('Partner');
        $partners = $this->Partner->find('all');
        $this->set(compact('partners'));
      }
      public function career_series()
      {

      }
      
      public function disclosure()
      {

      }
      public function career_guide()
      {

      }
      
    public function journal()
    {

      }
      public function manuscripts()
    {

      }
    public function rnd()
    {

      }
       public function officials()
      {

      }
      
  public function index() {
  // $this->layout=null;
   /* $this->loadModel('SliderImage');
    $images = $this->SliderImage->find('first');
    $this->set(compact('images'));*/
    $this->loadModel('Notice');
    $views = $this->Notice->find('all',array('limit' => 5));
    $this->set(compact('views'));
  }
    public function contacts1(){
    if($this->request->is('post')){
      $data = $this->data;
      $this->loadModel('AdmissionEnquiry');
      //pr($data);die;
      $data1['AdmissionEnquiry'] = $data['Contact'];
      $data1['AdmissionEnquiry']['query'] = $data['Contact']['message'];
      /*$cont = '<html><body><table>
                <tr>
                  <td colspan="2">Contact Information</td>
                </tr>
                <tr>
                  <td>Name</td>
                  <td>'.$data['Contact']['name'].'</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>'.$data['Contact']['email'].'</td>
                </tr>
                <tr>
                  <td>Message</td>
                  <td>'.$data['Contact']['message'].'</td>
                </tr>
                <tr>
                  <td>Phone</td>
                  <td>'.$data['Contact']['phone'].'</td>
                </tr>
                <tr>
                  <td>Course</td>
                  <td>'.$data['Contact']['course'].'</td>
                </tr>
              </table></body></html>';*/
      //'Name : '.$data['Contact']['name'].' Email : '.$data['Contact']['email'].' Message : '.$data['Contact']['message'].' Phone : '.$data['Contact']['phone'].' course : '.$data['Contact']['course'];
      //pr($data);die;
      //echo "<pre>"; print_r($data1);die;admissions@ieccollege
      $this->AdmissionEnquiry->save($data1);
      /*$this->Email->from    = 'info@iec.com';
$this->Email->to      = 'admissions@ieccollege.com';
$this->Email->subject = 'Contact Us';
$this->Email->send($cont);*/

      $Email = new CakeEmail();
          $Email->viewVars(compact('data'));
          //$Email->viewVars(array('value' => $val1,'value2' => $val2));
          //echo $link;die;



          $Email->template('contact')
              ->emailFormat('html')
              ->from(array('info@iec.com' => 'IEC'))
              ->to('admissions@ieccollege.com')
              ->subject('Contact Us')
              ->send();

$this->redirect(array('controller' => 'homes','action'=>'thanks'));
    }

  }
  public function contacts(){
    if($this->request->is('post')){
    
      $data1 = $this->data;
      //echo "<pre>";print_r($_POST);die;
      /*$cont = '<html><body><table>
                <tr>
                  <td colspan="2">Contact Information</td>
                </tr>
                <tr>
                  <td>Name</td>
                  <td>'.$data['Contact1']['name'].'</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>'.$data['Contact1']['email'].'</td>
                </tr>
                <tr>
                  <td>Message</td>
                  <td>'.$data['Contact1']['message'].'</td>
                </tr>
              </table></body></html>';
      //$cont = 'Name : '.$data['Contact1']['name'].' Email : '.$data['Contact1']['email'].' Message : '.$data['Contact1']['message'];
     // print_r($data);die;
      //$this->Email->from    = 'info@iec.com';
              //$this->Email->emailFormat('html');
      $this->Email->from    = 'gkatiyar27@gmail.com';
//$this->Email->to      = 'admissions@ieccollege.com';
$this->Email->to      = 'buildrepo.gaurav@gmail.com';
$this->Email->subject = 'Contact Us';
$this->Email->send($cont);*/

      $Email = new CakeEmail();
      $Email->viewVars(compact('data1'));
          //$Email->viewVars(array('value' => $val1,'value2' => $val2));
          //echo $link;die;



          $Email->template('contact')
              ->emailFormat('html')
              ->from(array('info@iec.com' => 'IEC'))
              ->to('admissions@ieccollege.com')
              ->subject('Contact Us')
              ->send();
 $this->Session->write('resp_msg','1');
$this->redirect(array('controller' => 'homes','action'=>'thanks'));
    }
  }
   public function quicklinks(){

  }
  public function registration_page()
    {
      $this->loadModel("User");
      $this->loadModel("Usermeta");
      $this->layout='registrationlayout';
       if ($this->request->is('post')) 
              {

                $data=$this->data;
                //pr($data);die;
                $password=$data['User']['password'];
                $data['User']['password']=@md5($data['User']['password']);
                if(!empty($data))
                {   $data1['Usermeta']['id']=$data['Usermeta']['id'];
                    $destination = realpath('../webroot/img/') . '/';
                    $image = $this->Components->load('resize');  

                   if (!empty($_FILES['logo']['tmp_name'])) 
                   {
                      if(is_uploaded_file($_FILES['logo']['tmp_name']))
                       {
                            
                            $filename = time().'-'.$_FILES['logo']['name'];
                            $image->resize($_FILES['logo']['tmp_name'],$destination.$filename,'aspect_fill',150,150,0,0,0,0);     
                            $data1['Usermeta']['logoImage']=$filename;
                        }
                    }

                    if (!empty($_FILES['restaurantimage']['tmp_name'])) 
                   {
                    if(is_uploaded_file($_FILES['restaurantimage']['tmp_name']))
                    {
                            $filename = time().'-'.$_FILES['restaurantimage']['name'];
                            $image->resize($_FILES['restaurantimage']['tmp_name'],$destination.$filename,'aspect_fill',300,200,0,0,0,0);     
                            $data1['Usermeta']['restaurantImage']=$filename;
                        
                     }
                   }
                   if (!empty($_FILES['backgroundimage']['tmp_name'])) 
                   {
                       if(is_uploaded_file($_FILES['backgroundimage']['tmp_name']))
                      {
                            $filename = time().'-'.$_FILES['backgroundimage']['name'];
                            $image->resize($_FILES['backgroundimage']['tmp_name'],$destination.$filename,'aspect_fill',1280,768,0,0,0,0);     
                            $data1['Usermeta']['backgroundImage']=$filename;
                      }  
                  }
                    $data['User']['status']=0;
                    $this->User->save($data);
                    $restautrantid=$this->User->getLastInsertId();
                    //functionality to send email
                    $to_mail=$data['User']['email'];
                    $name=$data['User']['fname'];
                    $user_pass=$password;  
                    $link = HTTP_ROOT.'homes/';
                    $replace = array('{name}','{password}','{email}','{link}');
                    $with= array($name,$user_pass,$to_mail,$link);
                    $this->send_email($replace,$with,'registration','support@restaurant.com' ,$to_mail); //calling  send_email() in app controller
                    //functionality to send email ends here
                    if(empty($restautrantid)){
                        $restaurantId=$data['User']['id'];
                    }else{
                       $restaurantId=$restautrantid;
                    }
                    $data1['Usermeta']['r_id']=$restaurantId;
                    
                    $this->Usermeta->save($data1);
                    $this->Session->write('alert','Your Password has been sent to '.$to_mail);

                                              
                  
                  $this->redirect(array(
                    'action' => 'index'
                ));
             
               }
           }
    }

  /************Forgot password start*******************/
  public function forgotpass(){

    $email = trim($_POST['email']);
    $user = $this->User->findByEmail($email);
 
    if(!empty($user)){
      $new_pass = $this->generateRandomString('8');
      $user['User']['password'] = md5($new_pass);
      if($this->User->save($user)){
        $Email = new CakeEmail();
        $Email->from(array('Support@DigitalRestaurant.com' => HTTP_ROOT));
        $Email->to($email);
        $Email->subject('Forgot Password');
        $Email->send('Your new pasword is '.$new_pass);
        //pr($Email);
        //echo "hi";die;
        $this->Session->write('alert','Password has been sent to '.$email);
        $this->redirect($this->referer());
      }else{
        $this->Session->write('alert','Server Error, Please try again.');
        $this->redirect($this->referer());
      }
    }else{
      $this->Session->write('alert',$email.' dosenot exist in our database. ');
      $this->redirect($this->referer());
    }
  }

  /************Forgot password end*******************/

  /************Login start*******************/
public function login()
{ 
     
     $data=$_POST['data'];
    // pr($data);
  $this->loadModel("User");
      if(!empty($_POST))
              {     
           
                $admin_info = $this->User->find('first',array(
                                      'conditions' =>array(
                                        'User.email'=>$data['User']['username'],
                                        'User.password'=>md5($data['User']['password']),
                                        'User.status'=>1
                                                )
                                      )
                                ); 
                 if (!empty($admin_info)) {
                  
                  //for superadmin login
                  if($admin_info['User']['user_type'] == 0){
                    $this->Auth->login($admin_info['User']);
                      $this->redirect(array('controller' => 'Users','action'=>'Dashboard','SuperAdmin'));
                      
                    }
                  else if($admin_info['User']['user_type'] == 1){//for admin login
                    $this->Auth->login($admin_info['User']);
                      $this->redirect(array('controller' => 'Users','action'=>'Dashboard','Admin'));
                      
                  }
                  else if($admin_info['User']['user_type'] == 2){//for admin login
                    $this->Auth->login($admin_info['User']);
                      $this->redirect(array('controller' => 'Users','action'=>'Dashboard','SubAdmin'));
                      
                  }
                  
                }else{
          $this->Session->write('alert','Login credential Wrong.');
          $this->redirect($this->referer());
        }  
    
        }else{
          $this->Session->write('alert','UserName or Password Cannot Be Empty.');
          $this->redirect($this->referer());
        }   
}
/************Login end*******************/
public function logout(){
  $this->Session->destroy();
  $this->redirect(array('action'=>'index'));
  
}
public function SaveContact($id=null){
  //Configure::write('debug', 2);
  Configure::write('debug',0);
    $this->loadModel('Contact');
     if($this->request->is('post'))//Condition to Save Customer Details(form Action)
       {
        
            $data=$this->data;
            //pr($data);die;
            $this->Contact->save($data);
            $this->Session->write('alert','Thank you for Contact us. We will revert back to you.');
            $this->redirect($this->referer());
          
            //pr($this->data);die;
           
      }

}
public function iecLogin(){

}
public function gallery(){
  $this->loadModel('Category');
  $this->loadModel('Gallery');
  $this->Category->bindModel(
                            array(
                                  'hasMany' => array(
                                                    'Gallery' => array(
                                                                        'className' => 'Gallery',
                                                                        'foreignKey' => 'category_id'
                                                                      )
                                                    )
                                  )
                            );
  $galleries = $this->Category->findAllByType('1');
  //pr($galleries);die;
$this->set(compact('galleries'));
}
public function media(){
  $this->loadModel('Category');
  $this->loadModel('Gallery');
  $this->Category->bindModel(
                            array(
                                  'hasMany' => array(
                                                    'Gallery' => array(
                                                                        'className' => 'Gallery',
                                                                        'foreignKey' => 'category_id'
                                                                      )
                                                    )
                                  )
                            );
  $galleries = $this->Category->findAllByType('2');
  $this->set(compact('galleries'));
}
public function video(){
  $this->loadModel('Category');
  $this->loadModel('Gallery');
  $this->Category->bindModel(
                            array(
                                  'hasMany' => array(
                                                    'Gallery' => array(
                                                                        'className' => 'Gallery',
                                                                        'foreignKey' => 'category_id'
                                                                      )
                                                    )
                                  )
                            );
  $galleries = $this->Category->findAllByType('3');
  //pr($galleries);die;
$this->set(compact('galleries'));
}
public function infra(){
$this->loadModel('Category');
  $this->loadModel('Gallery');
  $this->Category->bindModel(
                            array(
                                  'hasMany' => array(
                                                    'Gallery' => array(
                                                                        'className' => 'Gallery',
                                                                        'foreignKey' => 'category_id'
                                                                      )
                                                    )
                                  )
                            );
  $galleries = $this->Category->findAllByType('4');
  //pr($galleries);die;
$this->set(compact('galleries'));
}
public function terms(){

}
public function privacy(){
  
}
public function ieee(){
  
}
public function guest_lecture(){
  
}

public function view360(){
    $this->loadModel('LinkView');
    $view360 = $this->LinkView->find('all');
    $this->set(compact('view360'));
}

public function getimage(){
    $this->loadModel('Gallery');
    $gal = $this->Gallery->findAllByCategory_id($_POST['catId'],array('Gallery.image'));
    echo json_encode($gal);
    die;
}
public function careers(){
    if($this->request->is('post')){
        $data= $this->data['Career'];
        $pp = explode('tmp/',$_FILES['resume']['tmp_name']);
        $typ = explode('/',$_FILES['resume']['type']);
       // pr($_FILES);die;
        $cont = ' Applying for : '.$data['for'].', Full Name : '.$data['name'].', Email : '.$data['email'].', Contact No  : '.$data['contact'].', Address : '.$data['address'].', Highest Qualificationr : '.$data['qualification'].'Relative Work Experiencer : '.$data['year'].' year and '.$data['month'].' momnths';
       // pr($cont);die;
          $this->Email->from    = 'info@iec.com';
          $this->Email->filePaths  = array($pp['0'].'tmp/');
       $this->Email->attachments =array($pp['1']);
$this->Email->to      = 'hr@ieccollege.com';
$this->Email->subject = 'Careers';
$this->Email->send($cont);
//pr($this->Email);die;
$this->Session->write('career','1');
$this->redirect($this->referer());
    }
}
public function landingpage() {
    $this->layout='';
    $this->loadModel('LandingPage');
       if($this->request->is('post'))//Condition to Save Customer Details(form Action)
       {
      
        $data=$this->data;
        //pr($data);die;
        $this->LandingPage->save($data);
               $msg = 'FirstName : '.$data['LandingPage']['first_name'].' LastName : '.$data['LandingPage']['last_name'].' Mobile No. : '.$data['LandingPage']['mobile'].' Email : '.$data['LandingPage']['email'].'Course : '.$data['LandingPage']['course'].' Messgae : '.$data['LandingPage']['message'];
     
        $Email = new CakeEmail();
        $Email->from(array($data['LandingPage']['email'] => 'www.iec.edu.in'));
        $Email->to('adwordsieccollege@gmail.com');//enquiry@iecuniversity.com
        $Email->subject('IEC College Enquiry Form');
        $Email->send($msg);

          //$this->Session->write('alert','Thank you for Contact us. We will revert back to you.');
        //$this->redirect($this->referer());
         $this->redirect(array('action' => 'thanks'));
        //$this->Session->write('alert','Thank you for Contact us. We will revert back to you.');
        //$this->redirect($this->referer());
        //return $this->redirect(array('controller' => 'homes' 'action' => 'thanks'));
        //pr($this->data);die;
           
      }
}
public function campus_placement(){
    
}
public function landingpage2(){
  $this->layout='';
}
public function stguidline(){
 
}
public function innovativeproject(){
 
}
public function marksheet(){
    $this->loadModel('Marksheet');
    $this->loadModel('Category');
    $Views = $this->Marksheet->find('all');
    $this->set(compact('Views'));
    //print_r($Views);die;
}
public function notice(){
     $this->loadModel('Notice');
    if(!empty($_GET['cat'])){
      $condition[] = array('Notice.notice_category LIKE' => '%'.$_GET['cat'].'%');
      $cat = $_GET['cat'];
    }
    if(!empty($_GET['key'])){
      $condition[] = array('Notice.subject LIKE' => '%'.$_GET['key'].'%'); 
    }
    /*if(!empty($_GET['year'])){
      $date = $_GET['year'].'-'.$_GET['month'].'-00';
      $condition[] = array('Notice.notice_date =' => $date); 
    }*/
    $condition[] = array('Notice.status' => 1);
    $Views = $this->Notice->find('all',array('conditions' => array('AND' => $condition), 'order' => array('id') ));
    $this->set(compact('Views','cat'));
    //print_r($Views);die;
  
}
public function downloadform(){
     $this->loadModel('Download');
     $this->loadModel('Category');
    $Views = $this->Download->find('all');
    $this->set(compact('Views'));
    //print_r($Views);die;
  
}

//this is done by Aman

/*public function rform_1()
  {

    $this->loadModel('Registration');
    if($this->request->is('post'))
        {
         // pr($this->request->data);die;
          $data=$this->request->data;
          //$this->Registration->id=$last_id;
          if(empty($_POST['regis_id']))
         {
            $this->Registration->save($data);
            $last_id=$this->Registration->getLastInsertId();
            //pr("hello");
            //pr($last_id);die;
            return   $this->redirect(array('controller'=>'Homes','action'=>'rform_2',$last_id));
          }
          else
          {
            $this->Registration->id=$_POST['regis_id'];
            $this->Registration->save($data);
            $last_id=$_POST['regis_id'];
            //pr("hello");
            //pr($last_id);die;
            return   $this->redirect(array('controller'=>'Homes','action'=>'rform_2',$last_id));
          }
        }
  }*/
/*public function rform_2($last_id=null)
  {
    //pr($last_id);die;

    $this->loadModel('Registration');
    if($this->request->is('post'))
        {
          $this->Registration->id=$last_id;

          $this->Registration->save($this->request->data);
          //$last_id=$this->Registration->getLastInsertId();
          $this->redirect(array('action'=>'/rform_3',$last_id));
        }
  }
public function rform_3($last_id=null)
  {

    $this->loadModel('Registration');
    if($this->request->is('post'))
        {
          $this->Registration->id=$last_id;

          $this->Registration->save($this->request->data);
         // $last_id=$this->Registration->getLastInsertId();
          return   $this->redirect(array('action'=>'/rform_4',$last_id));
        }
  }*/
public function rform_1()
  {

    $this->loadModel('Registration');
    if($this->request->is('post'))
      {
        //temp
        $data=$this->request->data;
        if(empty($_POST['regis_id']))
         {
            $this->Registration->save($data);
            $last_id=$this->Registration->getLastInsertId();
            //pr("hello");
            //pr($last_id);die;
            //return   $this->redirect(array('controller'=>'Homes','action'=>'rform_2',$last_id));
          }
          else
          {
            $this->Registration->id=$_POST['regis_id'];
            $this->Registration->save($data);
            $last_id=$_POST['regis_id'];
            //pr("hello");
            //pr($last_id);die;
            //return   $this->redirect(array('controller'=>'Homes','action'=>'rform_2',$last_id));
          }

        /*if($_FILES['myimage']['type']=='image/jpeg' or $_FILES['myimage']['type']=='image/png' or $_FILES['myimage']['type']=='image/gif')
        {
         if (move_uploaded_file($_FILES['myimage']['tmp_name'],$_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/registration_img/full_size/ '.$_FILES['myimage']['name'])) 
         {
                  App::uses('ImageComponent', 'Controller/Component');
                $MyImageCom = new ImageComponent();

                $MyImageCom->prepare($_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/registration_img/full_size/ '.$_FILES['myimage']['name']);
                $MyImageCom->resize(50,50);//width,height,Red,Green,Blue
                $MyImageCom->save($_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/registration_img/thumbnail/ '.$_FILES['myimage']['name']);
          }
         //$HTTP_ROOT=echo HTTP_ROOT;
         //pr($HTTP_ROOT);die;
          $myimage_filepath=$_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/registration_img/thumbnail/ '.$_FILES['myimage']['name'];
         //pr($filepath);die;
        }

        if($_FILES['mysignature']['type']=='image/jpeg' or $_FILES['mysignature']['type']=='image/png' or $_FILES['mysignature']['type']=='image/gif')
        {
         if (move_uploaded_file($_FILES['mysignature']['tmp_name'],$_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/signature_img/full_size/ '.$_FILES['mysignature']['name'])) {
                  App::uses('ImageComponent', 'Controller/Component');
                $MyImageCom = new ImageComponent();

                $MyImageCom->prepare($_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/signature_img/full_size/ '.$_FILES['mysignature']['name']);
                $MyImageCom->resize(50,50);//width,height,Red,Green,Blue
                $MyImageCom->save($_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/signature_img/thumbnail/ '.$_FILES['mysignature']['name']);
            }
             //$HTTP_ROOT=echo HTTP_ROOT;
             //pr($HTTP_ROOT);die;
          $mysignature_filepath=$_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/signature_img/thumbnail/ '.$_FILES['mysignature']['name'];
             //pr($filepath);die;
          }
        if($_FILES['feereceipt']['type']=='image/jpeg' or $_FILES['feereceipt']['type']=='image/png' or $_FILES['feereceipt']['type']=='image/gif')
        {
         if (move_uploaded_file($_FILES['feereceipt']['tmp_name'],$_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/feereceipt_img/full_size/ '.$_FILES['feereceipt']['name'])) {
                  App::uses('ImageComponent', 'Controller/Component');
                $MyImageCom = new ImageComponent();

                $MyImageCom->prepare($_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/feereceipt_img/full_size/ '.$_FILES['feereceipt']['name']);
                $MyImageCom->resize(50,50);//width,height,Red,Green,Blue
                $MyImageCom->save($_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/feereceipt_img/thumbnail/ '.$_FILES['feereceipt']['name']);
            }
             //$HTTP_ROOT=echo HTTP_ROOT;
             //pr($HTTP_ROOT);die;
          $feereceipt_filepath=$_SERVER['DOCUMENT_ROOT'].'/app/webroot/img/feereceipt_img/thumbnail/ '.$_FILES['feereceipt']['name'];
             //pr($filepath);die;
          }
   //pr($filepath);die;
   //pr($this->request->data);die;
   //$user['User']['image_name']=$_FILES['myimage']['name']['0'];
    //  $user['User']['image_path']=$filepath;


//temp
          $this->Registration->id=$last_id;
          $data=array();
          $data['Registration']['myimage_name'] = $_FILES['myimage']['name'];
          $data['Registration']['myimage_path'] = $myimage_filepath;
          $data['Registration']['mysignature_name'] = $_FILES['mysignature']['name'];
          $data['Registration']['mysignature_path'] = $mysignature_filepath;
          $data['Registration']['feereceipt_name'] = $_FILES['feereceipt']['name'];
          $data['Registration']['feereceipt_path'] = $feereceipt_filepath;
            //pr($data);die;
             //$this->Registration-->saveField('myimage_name', '');
             //pr($_FILES['myimage']);die;
             //pr($this->request->data);die;
          $this->Registration->save($data);*/
          $reg_data = $this->Registration->findById($last_id);

          $var = '<span style="text-align:center"><h2>IEC Group of Institutions <br> Registration Form For Even Semester 2016-17</h2></span>
            <hr>
           
            <table>
        <tr>
        
          <th>Field</th>
          <th>Value</th>
         


        </tr>
<tr>
  <td>course</td>
  <td>'. @$reg_data["Registration"]["course"].'</td>
</tr>
<tr>
  <td>Status</td>
  <td>'. @$reg_data["Registration"]["status"].'</td>
</tr>
<tr>
  <td>Branch</td>
   <td>'. @$reg_data["Registration"]["branch"].'</td>
</tr>
<tr>
  <td>Shift</td>
  <td>'. @$reg_data["Registration"]["shift"].'</td>
</tr>
<tr>
  <td>Semester</td>
  <td>'. @$reg_data["Registration"]["sem"].'</td>
</tr>
<tr>
  <td>Roll No.</td>
  <td>'. @$reg_data["Registration"]["roll"].'</td>
</tr>
<tr>
  <td>First Name</td>
  <td>'. @$reg_data["Registration"]["fname"].'</td>
</tr>
<tr>
  <td>Middle Name</td>
  <td>'. @$reg_data["Registration"]["mname"].'</td>
</tr>
<tr>
  <td>Last Name</td>
  <td>'. @$reg_data["Registration"]["lname"].'</td>
</tr>
<tr>
  <td>Mobile</td>
  <td>'. @$reg_data["Registration"]["mob"].'</td>
</tr>
<tr>
  <td>Date</td>
  <td>'. @$reg_data["Registration"]["date"].'</td>
</tr>
<tr>
  <td>Blood group</td>
  <td>'. @$reg_data["Registration"]["bg"].'</td>
</tr>
<tr>
  <td>Gender</td>
  <td>'. @$reg_data["Registration"]["gender"].'</td>
</tr>
<tr>
  <td>Category</td>
  <td>'. @$reg_data["Registration"]["category"].'</td>
</tr>
<tr>
  <td>Accommodation</td>
  <td>'. @$reg_data["Registration"]["accommodation"].'</td>
</tr>
<tr>
  <td>Religion</td>
  <td>'. @$reg_data["Registration"]["religion"].'</td>
</tr>
<tr>
  <td>Nationality</td>
  <td>'. @$reg_data["Registration"]["nationality"].'</td>
</tr>
<tr>
  <td>Email</td>
  <td>'. @$reg_data["Registration"]["email"].'</td>
</tr>
<tr>
  <td>Current Address</td>
  <td>'. @$reg_data["Registration"]["current_address"].'</td>
</tr>
<tr>
  <td>Pincode</td>
  <td>'. @$reg_data["Registration"]["pin1"].'</td>
</tr>
<tr>
  <td>City</td>
  <td>'. @$reg_data["Registration"]["city1"].'</td>
</tr>
<tr>
  <td>State</td>
  <td>'. @$reg_data["Registration"]["state1"].'</td>
</tr>
<tr>
  <td>Permanent Address</td>
  <td>'. @$reg_data["Registration"]["permanent_Address"].'</td>
</tr>
<tr>
  <td>Pincode</td>
  <td>'. @$reg_data["Registration"]["pin"].'</td>
</tr>
<tr>
  <td>City</td>
  <td>'. @$reg_data["Registration"]["city"].'</td>
</tr>
<tr>
  <td>State</td>
  <td>'. @$reg_data["Registration"]["state"].'</td>
</tr>
<tr>
  <td>Local Guardian Name</td>
  <td>'. @$reg_data["Registration"]["local_guardian_name"].'</td>
</tr>
<tr>
  <td>Local Guardian Mobile</td>
  <td>'. @$reg_data["Registration"]["local_guardian_mob"].'</td>
</tr>
<tr>
  <td>Mode of Admission</td>
  <td>'. @$reg_data["Registration"]["mode_admission"].'</td>
</tr>
<tr>
  <td>Fee Status</td>
  <td>'. @$reg_data["Registration"]["fee_status"].'</td>
</tr>
<tr>
  <td>Pending Fee Amount</td>
  <td>'. @$reg_data["Registration"]["fee_amount"].'</td>
</tr>
<tr>
  <td>Scholarship Status</td>
  <td>'. @$reg_data["Registration"]["scholarship_status"].'</td>
</tr>
<tr>
  <td>Aadhar No</td>
  <td>'. @$reg_data["Registration"]["aadhar_no"].'</td>
</tr>
<tr>
  <td>Bank Account No</td>
  <td>'. @$reg_data["Registration"]["bank_account_no"].'</td>
</tr>
<tr>
  <td>Ifsc Code</td>
  <td>'. @$reg_data["Registration"]["ifsc_code"].'</td>
</tr>
<tr>
  <td>PAN</td>
  <td>'. @$reg_data["Registration"]["pan_no"].'</td>
</tr>
<tr>
  <td>Passport No</td>
  <td>'. @$reg_data["Registration"]["passport_no"].'</td>
</tr>
<tr>
  <td>Whatsapp No</td>
  <td>'. @$reg_data["Registration"]["whatsapp_no"].'</td>
</tr>
<tr>
  <td>Facebook Id</td>
  <td>'. @$reg_data["Registration"]["facebook_id"].'</td>
</tr>
<tr>
  <td>Linkedin Id</td>
  <td>'. @$reg_data["Registration"]["linkedin_id"].'</td>
</tr>
<tr>
  <td>Want Register</td>
  <td>'. @$reg_data["Registration"]["want_register"].'</td>
</tr>
<tr>
  <td>Father Name</td>
  <td>'. @$reg_data["Registration"]["father_name"].'</td>
</tr>
<tr>
  <td>Father Occupation</td>
  <td>'. @$reg_data["Registration"]["father_occupation"].'</td>
</tr>
<tr>
  <td>Father Mobile</td>
  <td>'. @$reg_data["Registration"]["father_mob"].'</td>
</tr>
<tr>
  <td>Father Email</td>
  <td>'. @$reg_data["Registration"]["father_email"].'</td>
</tr>
<tr>
  <td>Mother Name</td>
  <td>'. @$reg_data["Registration"]["mother_name"].'</td>
</tr>
<tr>
  <td>Mother Occupation</td>
  <td>'. @$reg_data["Registration"]["mother_occupation"].'</td>
</tr>
<tr>
  <td>Mother Mobile</td>
  <td>'. @$reg_data["Registration"]["mother_mob"].'</td>
</tr>
<tr>
  <td>Parents Annual Income</td>
  <td>'. @$reg_data["Registration"]["parent_income"].'</td>
</tr>
<tr>
  <td>Area of Interest</td>
  <td>'. @$reg_data["Registration"]["area_of_interest"].'</td>
</tr>

<tr>
  <td>Tenth Percentage</td>
  <td>'. @$reg_data["Registration"]["tenth_per"].'</td>
</tr>
<tr>
  <td>Tenth Roll No.</td>
  <td>'. @$reg_data["Registration"]["tenth_roll"].'</td>
</tr>
<tr>
  <td>Tenth Board</td>
  <td>'. @$reg_data["Registration"]["tenth_board"].'</td>
</tr>
<tr>
  <td>Tenth Passing Year</td>
  <td>'. @$reg_data["Registration"]["tenth_passing_year"].'</td>
</tr>
<tr>
  <td>Twelth Percentage</td>
  <td>'. @$reg_data["Registration"]["twelth_per"].'</td>
</tr>
<tr>
  <td>Twelth Roll No</td>
  <td>'. @$reg_data["Registration"]["twelth_roll"].'</td>
</tr>
<tr>
  <td>Twelth Board</td>
  <td>'. @$reg_data["Registration"]["twelth_board"].'</td>
</tr>
<tr>
  <td>Twelth Passing Year</td>
  <td>'. @$reg_data["Registration"]["twelth_passing_year"].'</td>
</tr>
<tr>
  <td>Graduation Percentage</td>
  <td>'. @$reg_data["Registration"]["graduation_per"].'</td>
</tr>

<tr>
  <td>Theory Sub Code 1</td>
  <td>'. @$reg_data["Registration"]["th_sub_code_1"].'</td>
</tr>
<tr>
  <td>Theory Sub Name 1</td>
  <td>'. @$reg_data["Registration"]["th_sub_name_1"].'</td>
</tr>
<tr>
  <td>Theory Sub Code 2</td>
  <td>'. @$reg_data["Registration"]["th_sub_code_2"].'</td>
</tr>
<tr>
  <td>Theory Sub Name 2</td>
  <td>'. @$reg_data["Registration"]["th_sub_name_2"].'</td>
</tr>
<tr>
  <td>Theory Sub Code 3</td>
  <td>'. @$reg_data["Registration"]["th_sub_code_3"].'</td>
</tr>
<tr>
  <td>Theory Sub Name 3</td>
  <td>'. @$reg_data["Registration"]["th_sub_name_3"].'</td>
</tr>
<tr>
  <td>Theory Sub Code 4</td>
  <td>'. @$reg_data["Registration"]["th_sub_code_4"].'</td>
</tr>
<tr>
  <td>Theory Sub Name 4</td>
  <td>'. @$reg_data["Registration"]["th_sub_name_4"].'</td>
</tr>
<tr>
  <td>Theory Sub Code 5</td>
  <td>'. @$reg_data["Registration"]["th_sub_code_5"].'</td>
</tr>
<tr>
  <td>Theory Sub Name 5</td>
  <td>'. @$reg_data["Registration"]["th_sub_name_5"].'</td>
</tr>
<tr>
  <td>Theory Sub Code 6</td>
  <td>'. @$reg_data["Registration"]["th_sub_code_6"].'</td>
</tr>
<tr>
  <td>Theory Sub Name 6</td>
  <td>'. @$reg_data["Registration"]["th_sub_name_6"].'</td>
</tr>
<tr>
  <td>Theory Sub Code 7</td>
  <td>'. @$reg_data["Registration"]["th_sub_code_7"].'</td>
</tr>
<tr>
  <td>Theory Sub Name 7</td>
  <td>'. @$reg_data["Registration"]["th_sub_name_7"].'</td>
</tr>
<tr>
  <td>Theory Sub Code 8</td>
  <td>'. @$reg_data["Registration"]["th_sub_code_8"].'</td>
</tr>
<tr>
  <td>Theory Sub Name 8</td>
  <td>'. @$reg_data["Registration"]["th_sub_name_8"].'</td>
</tr>
<tr>
  <td>Theory Sub Code 9</td>
  <td>'. @$reg_data["Registration"]["th_sub_code_9"].'</td>
</tr>
<tr>
  <td>Theory Sub Name 9</td>
  <td>'. @$reg_data["Registration"]["th_sub_name_9"].'</td>
</tr>
<tr>
  <td>Practical Sub Code 1</td>
  <td>'. @$reg_data["Registration"]["pr_sub_code_1"].'</td>
</tr>
<tr>
  <td>Practical Sub Name 1</td>
  <td>'. @$reg_data["Registration"]["pr_sub_name_1"].'</td>
</tr>
<tr>
  <td>Practical Sub Code 2</td>
  <td>'. @$reg_data["Registration"]["pr_sub_code_2"].'</td>
</tr>
<tr>
  <td>Practical Sub Name 2</td>
  <td>'. @$reg_data["Registration"]["pr_sub_name_2"].'</td>
</tr>
<tr>
  <td>Practical Sub Code 3</td>
  <td>'. @$reg_data["Registration"]["pr_sub_code_3"].'</td>
</tr>
<tr>
  <td>Practical Sub Name 3</td>
  <td>'. @$reg_data["Registration"]["pr_sub_name_3"].'</td>
</tr>
<tr>
  <td>Practical Sub Code 4</td>
  <td>'. @$reg_data["Registration"]["pr_sub_code_4"].'</td>
</tr>
<tr>
  <td>Practical Sub Name 4</td>
  <td>'. @$reg_data["Registration"]["pr_sub_name_4"].'</td>
</tr>
<tr>
  <td>Practical Sub Code 5</td>
  <td>'. @$reg_data["Registration"]["pr_sub_code_5"].'</td>
</tr>
<tr>
  <td>Practical Sub Name 5</td>
  <td>'. @$reg_data["Registration"]["pr_sub_name_5"].'</td>
</tr>
<tr>
  <td>Practical Sub Code 6</td>
  <td>'. @$reg_data["Registration"]["pr_sub_code_6"].'</td>
</tr>
<tr>
  <td>Practical Sub Name 6</td>
  <td>'. @$reg_data["Registration"]["pr_sub_name_6"].'</td>
</tr>
<tr>
  <td>Practical Sub Code 7</td>
  <td>'. @$reg_data["Registration"]["pr_sub_code_7"].'</td>
</tr>
<tr>
  <td>Practical Sub Name 7</td>
  <td>'. @$reg_data["Registration"]["pr_sub_name_7"].'</td>
</tr>
<tr>
  <td>Practical Sub Code 8</td>
  <td>'. @$reg_data["Registration"]["pr_sub_code_8"].'</td>
</tr>
<tr>
  <td>Practical Sub Name 8</td>
  <td>'. @$reg_data["Registration"]["pr_sub_name_8"].'</td>
</tr>
<tr>
  <td>Practical Sub Code 9</td>
  <td>'. @$reg_data["Registration"]["pr_sub_code_9"].'</td>
</tr>
<tr>
  <td>Practical Sub Name 9</td>
  <td>'. @$reg_data["Registration"]["pr_sub_name_9"].'</td>
</tr>
';
        
$var = $var.'</table><br><br><br><br>
          <span>I understand that I have to attend all the Theory and Practical Classes from 9th Jan 2017. In case I do not fulfill the attendance norms of minimum 75% , I will not be allowed to appear in end semester examinations.</span>
          <br><br>
          <span>In case there is any changes in the address/ Phone Number/E-Mail Id of my or my parents/local gaurdian , I will inform to the HOD in writing.</span>
          <br><br><br><br>
          <span>Signature : </span>
          <br><br>
          <span>Date : </span>
          ';  
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->addPage('', 'USLETTER');
$pdf->setFont('helvetica', '', 9);
//$pdf->writeHTMLcell(30, 0, $var);
$pdf->writeHTMLCell(0, 0, '', '', $var, 0, 1, 0, true, '', true);
$destination = realpath('../webroot/img/registraionpdf/') . '/';
$filename = time().'-'.$reg_data["Registration"]["fname"].'.pdf';
$pdf->Output($destination.$filename, 'F');
  //echo "<pre>";print_r($var);die;    

          //$this->set(compact('var'));
      $check_fee = $this->request->data;
      if($check_fee['Registration']['paid_fees'] == 'yes'){
        $Email = new CakeEmail();
        $Email->from(array('Support@Iec.com' => 'Iec'));
        $Email->to(array($reg_data["Registration"]["email"],'sharad.it@ieccollege.com'));
        //$Email->to('buildrepo@gmail.com');
        $Email->subject('Iec Registration');
        $Email->attachments(array(
            'details.pdf' => array(
              'file' => $destination.$filename,
              'mimetype' => 'pdf',
              'contentId' => 'registration details'
             )
          ));
        $Email->send('Registraion');

        $this->Session->write('success-msg',$filename);
      }
      else{
        $this->Session->write('unsuccess-msg','no');
      }

         // echo "<pre>";print_r($Email);die;
            //$last_id=$this->Registration->getLastInsertId();
          return   $this->redirect(array('action'=>'/'));
        }
  }


  public function otp(){
    //print_r($_POST);die;
    $mobile = $_POST['mobile'];
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 4; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    $string = 'Please%20do%20not%20share%20this%20otp%20with%20anyone%20else.Your%20otp%20for%20IEC%20Registration%20form%20is%20'.$randomString;
    $link = 'http://103.16.142.193/api.php?username=ieccet&password=amit123&sender=IECCET&sendto='.$mobile.'&message='.$string;
    file_get_contents($link);
    echo base64_encode($randomString);die;

  }
  public function otp_check(){
    $stu_otp = $_POST['otp'];
    $server_otp = base64_decode($_POST['check_otp']);
   // echo $server_otp;
    if($stu_otp == $server_otp){
      echo "true";
    }else{
      echo "false";
    }
    die;
  }

  public function newsletter(){
    $this->loadModel('SubscriberList');
    $data = $this->data;
    //echo "<pre>";print_r($data);die;
    $this->SubscriberList->save($data);
    //$this->Session->write('alert','Newsletter subscribed successfully');
    return $this->redirect($this->referer());
  }
  public function roll_check(){
    $this->loadModel('Registration');
    $uni_roll = $_POST['uni_roll'];
    //pr($uni_roll);die;
    $result = $this->Registration->find('first', array('conditions'=> array('Registration.roll' => $uni_roll)));
    if(!empty($result)){
      echo $result['Registration']['id'];
    }
    die;
  }
  public function reg_form(){

  }
  public function medhotsav(){

  }
  public function med_sport_badmintion(){

  }
    public function med_sport_badmintion_sch(){

  }
  public function med_sport_carram_rule(){

  }
  public function med_sport_carram_sch(){

  }
  public function med_sport_chess_rule(){

  }
  public function med_sport_chess_sch(){

  }
  public function med_sport_cricket_rule(){

  }
  public function med_sport_cricket_sch(){

  }
  public function med_sport_tt_rule(){

  }
  public function med_sport_tt_sch(){

  }
  public function med_sports(){

  }
  public function med_sports_co(){

  }
  public function med_cul_mailstd(){

  }
  public function med_cul_eventtime(){

  }
  public function med_reg(){

  }

}
