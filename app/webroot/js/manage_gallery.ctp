<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Gallery
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageGallery">Gallery</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Gallery</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form role="form" id="add-state-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>From Date</th>
                  <th>To Date</th>
                </tr>
                </thead>
                <tbody id="gallery-table">
                <tr>
                  <td>
                    <input type="text" class="form-control required" required placeholder="Gallery Name" name="data[1][Gallery][name]" value="<?php echo @$stream['Gallery']['name']?>">
                    <input type="hidden" name="data[1][Gallery][id]" value="<?php echo @$galleryId?>">
                  </td>
                  <td>
                    <input type="text" class="form-control required" required placeholder="Gallery Description" name="data[1][Gallery][description]" value="<?php echo @$stream['Gallery']['description']?>">
                  </td>
                  <td>
                    <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data[1][Gallery][from_date]" value="<?php echo @$stream['Gallery']['from_date']?>">
                  </td>
                  <td>
                    <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data[1][Gallery][to_date]" value="<?php echo @$stream['Gallery']['to_date']?>">
                  </td>
                  <td>
                    <a href="javascript:void(0);" class="append-gallery-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Gallery Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>From Date</th>
                    <th>To Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($galleries as $data){?>
                  <tr>
                    <td>
                      <?php echo $data['Gallery']['name']?>
                    </td>
                    <td>
                      <?php echo $data['Gallery']['description']?>
                    </td>
                    <td>
                      <?php echo $data['Gallery']['from_date']?>
                    </td>
                    <td>
                      <?php echo $data['Gallery']['to_date']?>
                    </td>
                    <td>
                      <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Colleges/manageGallery/'.$data['Gallery']['id']?>"><i class="fa fa-edit"></i></a> 
                      <a onclick="return confirm('Are you sure,You want to delete this ?');" class="fafa-icons" title="Delete" href="<?php echo HTTP_ROOT.'Colleges/delete/Gallery/'.base64_encode($data['Gallery']['id'])?>"><i class="fa fa-trash"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
             <?php if(!empty($galleries)){?>
                <?php echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script>
    //manage Program 

   let i = 100;
    $(document).on('click','.append-event-a',function(){
      
      let div = '<tr> <td> <input type="text" class="form-control required" required placeholder="Gallery Name" name="data['+i+'][Gallery][name]"> </td><td> <input type="text" class="form-control required" required placeholder="Gallery Description" name="data['+i+'][Gallery][description]" > </td><td> <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data['+i+'][Gallery][from_date]" > </td><td> <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data['+i+'][Gallery][to_date]" > </td><td> <a href="javascript:void(0);" class="append-gallery-a"><i class="fa fa-plus"></i></a> <a href="javascript:void(0);" class="delete-gallery-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td>';
      i++;
      $('#events-table').append(div);
       $('.event-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd",
      startDate : new Date()
    });
    });
    
    $(document).on('click','.delete-event-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });

    //manage Program 
  </script>