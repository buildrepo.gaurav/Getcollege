$(document).ready(function(){
	baseUrl = 'https://'+$(location).attr('hostname')+'/';
	// console.log(baseUrl);

	
	//find state
	$('#country').change(function(){
      var key = $(this).val();
      console.log(key);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Admins/findState/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        console.log(response);
                        $('#state').children().remove();
                        $('#state').append(resp);
                        //console.log(resp);
                    }
              })
    });

    $('#state').on('change',function(){
      var key = $(this).val();
      	//console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Admins/findCity/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#city').children().remove();
                        $('#city').append(resp);
                        
                    }
              })
    });

    //country form validate start
	$('#add-country-form').validate({
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
	});
	//Country form validate ends

	//state form validate start
	$('#add-state-form').validate({
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
	});

	//state form validate ends
	
	//admin login form validate start
	$('#login-form').validate({
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
	});
	//admin login form validate ends

	//admin forgot password form validate start 
	$('#forgotPassword-form').validate({
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
	});
	//admin forgot password form validate ends 

	//admin edit details form validate start 
	$('#edit-admin-form').validate({
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
	});
	//admin edit details form validate ends 

	//admin change password form validate start 
	$('#change-admin-password').validate({
		rules: {
			    oldpassword: {
			      	required: true,
			       remote: {
					        url: baseUrl+"Admins/checkPassword",
					        type: "post",
					        
			   			 }
			   	}		 
			  },
		messages : {
					oldpassword: {
			    		remote: "Please enter correct Current Password."
			    	}
				
				},
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },	  
	});
	$("#confirm-pass").rules('add',{equalTo: "#new-pass",
	messages: {equalTo: "New password and confirm password field doesn't match."}});
	//admin change password form validate ends 

	//manageSubadmin listing data table start
		$('#subadmin-listing').DataTable({
	      "paging": true,
	      "searching":true,
	      "ordering":true
	    });
	//manageSubadmin listing data table ends

	//add subadmin starts
	$('#addsubadmin-form').validate({
		rules: {
			    "data[Admin][email]": {
			      	required: true,
			      	email : true,
			       remote: {
					        url: baseUrl+"Admins/checkEmail",
					        type: "post"
					        
			   			 }
			   	}		 
			  },
		messages : {
					"data[Admin][email]": {
			    		remote : "Email already exists."
			    	}
				
				},
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },	
	});

	$("#add-confirm-pass").rules('add',{equalTo: "#add-new-pass",
	messages: {equalTo: "New password and confirm password field doesn't match."}});
	//add subadmin ends

	//manageCollege listing data table start
	
	//manageCollege listing data table ends

	
	//add College starts
	if($('textarea').is('#editor2')){
    	CKEDITOR.replace('editor2');
    	$('#cke_editor2').on('keypress',function(){
    		$('#editor2').value() = editor2.getData();
    	});
	}

    if($('textarea').is('#editor3'))
    	CKEDITOR.replace('editor3');

    if($('textarea').is('#editor4'))
    	CKEDITOR.replace('editor4');

	function addcollegedata(form){
		let formData= new FormData(form);	
	   
	  	$.ajax({
	  		type: 'post',
	  		mimeType:'application/json',
	        dataType:'json',
	        data: formData,
	        contentType: false,
	        processData: false,
	  		url:baseUrl+'Colleges/addCollege',
	  		success:function(resp){
	  			if(resp.status == 'true'){
	  				$('.college-id').val(resp.collegeId);
	  				$('a[href="#profile"]').tab('show') 
	  			}else{
	  				alert('Network error, Please try again!!!');
	  			}
	  		}
	  	});
	}
	$('#add-college-form').validate({
		rules: {
			    "data[College][email]": {
			      	required: true,
			      	email : true,
			       remote: {
					        url: baseUrl+"Colleges/checkCollegeEmail",
					        type: "post"
					        
			   			 }
			   	}		 
			  },
		messages : {
					"data[College][email]": {
			    		remote : "Email already exists."
			    	}
				
				},
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
		submitHandler: function(form) {
			//console.log(form);
		 	addcollegedata(form);
		  }	
	});
	$("#college-confirm-pass").rules('add',{equalTo: "#college-new-pass",
	messages: {equalTo: "New password and confirm password field doesn't match."}});

	function editcollegedata(form){
		let formData= new FormData(form);	
	   
	  	$.ajax({
	  		type: 'post',
	  		mimeType:'application/json',
	        dataType:'json',
	        data: formData,
	        contentType: false,
	        processData: false,
	  		url:baseUrl+'Colleges/editCollege',
	  		success:function(resp){
	  			if(resp){
	  				$('a[href="#profile"]').tab('show') 
	  			}else{
	  				alert('Network error, Please try again!!!');
	  			}
	  		}
	  	});
	}
	$('#edit-college-form').validate({
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
		submitHandler: function(form) {
			//console.log(form);
		 	editcollegedata(form);
		  }
	});

		function nearcollegeform(form){

	let formData= new FormData(form);	
	//formData.append(form);
	//console.log(this);
	$.ajax({
		type: 'post',
		mimeType:'application/json',
    dataType:'json',
    data: formData,
    contentType: false,
    processData: false,
		url:baseUrl+'Colleges/collegeNear',
		success:function(resp){
			if(resp){
				$('a[href="#settings"]').tab('show') 
			}else{
				alert('Network error, Please try again!!!');
			}
		}
	});	

	}


	$('#near-college-form').validate({
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
		submitHandler: function(form) {
			//console.log(form);
		 	nearcollegeform(form);
		  }
	});


	function aboutcollegeform(form){

	let formData= new FormData(form);	
	//formData.append(form);
	//console.log(this);
	$.ajax({
		type: 'post',
		mimeType:'application/json',
	    dataType:'json',
	    data: formData,
	    contentType: false,
	    processData: false,
			url:baseUrl+'Colleges/collegeAbout',
			success:function(resp){
				if(resp){
					$('a[href="#facility"]').tab('show') 
				}else{
					alert('Network error, Please try again!!!');
				}
			}
		});	

	}


	$('#about-college-form').validate({
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
		submitHandler: function(form) {
			//console.log(form);
		 	aboutcollegeform(form);
		  }
	});


	
	//add College ends

	// college ab0ut form
	$('#datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd"
    });
    $('#datepicker1').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd"
    });
    $('.common-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd"
    });
	if($('textarea').is('#editor1'))
    	CKEDITOR.replace('editor1');
    // college about form

    //facility form
    /*$('#facility-form').DataTable({
    	 "paging": false,
	      "searching":true,
	      "ordering":true
    });*/
    //facility form

    //manage stream 

    let i= 100;
    $(document).on('click','.append-stream-a',function(){
    	
    	let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i><input type="text" class="form-control required" required placeholder="Enter Stream" name="data['+i+'][Stream][stream]" > </td><td> <textarea class="form-control" placeholder="Stream Description" name="data['+i+'][Stream][description]"></textarea> </td><td> <a href="javascript:void(0);" class="append-stream-a fafa-icons"><i class="fa fa-plus"></i></a><a href="javascript:void(0);" class="delete-stream-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
    	i++;
    	$('#stream-table').append(div);
    	 $('.event-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd",
      startDate : new Date()
    });
    });
    
    $(document).on('click','.delete-stream-tr',function(){
    	let tr_id = $(this).attr('tr-id');
    	$('#'+tr_id).remove();
    });

    //manage facilty
    function facilitycollegeform(form){

	let formData= new FormData(form);	
	//formData.append(form);
	//console.log(this);
	$.ajax({
		type: 'post',
		mimeType:'application/json',
	    dataType:'json',
	    data: formData,
	    contentType: false,
	    processData: false,
			url:baseUrl+'Colleges/collegeFacilityajax',
			success:function(resp){
				if(resp){
					//alert('Data save successfully.'); 
					$('a[href="#fields"]').tab('show');
				}else{
					alert('Network error, Please try again!!!');
				}
			}
		});	

	}


	$('#facility-college-form').validate({
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
		submitHandler: function(form) {
			//console.log(form);
		 	facilitycollegeform(form);
		  }
	});
    //manage facility
   
   function fieldcollegeform(form){

  let formData= new FormData(form); 
  //formData.append(form);
  console.log(formData);
  $.ajax({
    type: 'post',
    mimeType:'application/json',
      dataType:'json',
      data: formData,
      contentType: false,
      processData: false,
      url:baseUrl+'Colleges/collegeFieldajax',
      success:function(resp){
        if(resp){
          //$('a[href="#fields"]').tab('show') 
          alert('Data save successfully.'); 
          location.replace(baseUrl+'Colleges/manageCollege');
        }else{
          alert('Network error, Please try again!!!');
        }
      }
    }); 

  }


  $('#field-college-form').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function(form) {
      console.log(form);
      fieldcollegeform(form);
      }
  }); 

    //add stream to college
    $(document).on('change','.stream-select',function(){
      var key = $(this).val();
      var id = $(this).attr('data-id');
      	//console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/findProgram/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#program-select-'+id).children().remove();
                        $('#course-select-'+id).children().remove();
                        $('#specialization-select-'+id).children().remove();
                        $('#program-select-'+id).append(resp);
                        
                    }
              })
    });

    $(document).on('change','.program-select',function(){
      var key = $(this).val();
      var id = $(this).attr('data-id');
      	//console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/findCourse/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#course-select-'+id).children().remove();
                        $('#specialization-select-'+id).children().remove();
                        $('#course-select-'+id).append(resp);
                        
                    }
              })
    });

    $(document).on('change','.course-select',function(){
      var key = $(this).val();
      var id = $(this).attr('data-id');
      	//console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/findSpecialization/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#specialization-select-'+id).children().remove();
                        $('#specialization-select-'+id).append(resp);
                        
                    }
              })
    });

    //add stream to college ends   

    //manage event
    $('.event-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd",
      startDate : new Date()
    });
    //manage event

    //team form
    $('#team-form').validate({
    	errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    });
    //team form

    //add college course
    $('#add-college-course').validate({
    	errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    });
    //add college course

    //manage slider
    $('#manage-slider').validate({
    	errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    });
    //manage slider

    //add-course
    $('#add-course-form').validate({
    	errorPlacement: function(error, element) {
    		//element.before('');
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    });
    //add-course

    //add event
    $('#add-event-form').validate({
    	errorPlacement: function(error, element) {
    		//element.before('');
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    });
    //add event



    //add facility
    $('#add-facility-form').validate({
    	errorPlacement: function(error, element) {
    		//element.before('');
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    });
    //add Facility


    //add student
    	$('#student-academic').validate({
    		errorPlacement: function(error, element) {
    			//element.before('');
	     		var icon = $(element).prev('i');
	            icon.removeClass('fa-check').addClass("fa-warning");  
	            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	  		},
	  		highlight: function (element) { // hightlight error inputs
	            $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },
	    
			success: function (label, element) {
	            var icon = $(element).prev('i');
	            $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
	            icon.removeClass("fa-warning").addClass("fa-check");
	                
            },
            submitHandler: function (form) {
            	console.log(form);
                let formData= new FormData(form);	
                $.ajax({
                    type:'POST',
                    dataType:'json',
				    contentType: false,
				    processData: false,
                    data:formData,
                    url:baseUrl+'Students/addStudentAcademic', 
                    success:function(resp)
                    {   
                       if(resp){
                       	$('a[href="#personal"]').tab('show') 

			  			}else{
			  				alert('Please Fill Basic Details First!!');
			  			}
			  				
			  			
                    },

                });
            }
    	});
        $("#student-basics").validate({ 
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                /*fullname: { 
                        required: true
                    }*/
            }, 

 
            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
                
            },

            submitHandler: function (form) {
                let formData= new FormData(form);	
                $.ajax({
                    type:'POST',
                    dataType:'json',
				    contentType: false,
				    processData: false,
                    data:formData,
                    url:baseUrl+'Students/addStudent', 
                    success:function(resp)
                    {   
                       if(resp.status == 'true'){
			  				$('.student-id').val(resp.studentId);
			  				$('a[href="#academic"]').tab('show') 
			  			}else{
			  				alert('Network error, Please try again!!!');
			  			}
                    }
                });
            }
        });



        $("#personal-details").validate({ 
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                /*fullname: { 
                        required: true
                    }*/
            }, 

 
            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
                
            },

            submitHandler: function (form) {
                let formData= new FormData(form);	
                $.ajax({
                    type:'POST',
                    dataType:'json',
				    contentType: false,
				    processData: false,
                    data:formData,
                    url:baseUrl+'Students/studentPersonal', 
                    success:function(resp)
                    {   
                       if(resp){
			  				$('a[href="#experience"]').tab('show') 
			  			}else{
			  				alert('Please fill Basic Details First!!!');
			  			}
                    }
                });
            }
        });


        $('#student-experience').validate({
    		errorPlacement: function(error, element) {
    			//element.before('');
	     		var icon = $(element).prev('i');
	            icon.removeClass('fa-check').addClass("fa-warning");  
	            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	  		},
	  		highlight: function (element) { // hightlight error inputs
	            $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },
	    
			success: function (label, element) {
	            var icon = $(element).prev('i');
	            $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
	            icon.removeClass("fa-warning").addClass("fa-check");
	                
            },
            submitHandler: function (form) {
            	console.log(form);
                let formData= new FormData(form);	
                $.ajax({
                    type:'POST',
                    dataType:'json',
				    contentType: false,
				    processData: false,
                    data:formData,
                    url:baseUrl+'Students/studentExperience', 
                    success:function(resp)
                    {   
                       if(resp){
			  				$('a[href="#awards"]').tab('show') 
			  			}else{
			  				alert('Please fill Basic Details First!!!');
			  			}
			  				
			  			
                    },

                });
            }
    	});



    	$('#student-awards').validate({
    		errorPlacement: function(error, element) {
    			//element.before('');
	     		var icon = $(element).prev('i');
	            icon.removeClass('fa-check').addClass("fa-warning");  
	            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	  		},
	  		highlight: function (element) { // hightlight error inputs
	            $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },
	    
			success: function (label, element) {
	            var icon = $(element).prev('i');
	            $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
	            icon.removeClass("fa-warning").addClass("fa-check");
	                
            },
            submitHandler: function (form) {
            	console.log(form);
                let formData= new FormData(form);	
                $.ajax({
                    type:'POST',
                    dataType:'json',
				    contentType: false,
				    processData: false,
                    data:formData,
                    url:baseUrl+'Students/studentAwards', 
                    success:function(resp)
                    {   
                       if(resp){
			  				$('a[href="#Docs"]').tab('show') 
			  			}else{
			  				alert('Please fill Basic Details First!!!');
			  			}
			  				
			  			
                    },

                });
            }
    	});



    	$('#student-docs').validate({
    		errorPlacement: function(error, element) {
    			//element.before('');
	     		var icon = $(element).prev('i');
	            icon.removeClass('fa-check').addClass("fa-warning");  
	            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
	  		},
	  		highlight: function (element) { // hightlight error inputs
	            $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	            },
	    
			success: function (label, element) {
	            var icon = $(element).prev('i');
	            $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
	            icon.removeClass("fa-warning").addClass("fa-check");
	                
            },
            submitHandler: function (form) {
            	console.log(form);
                let formData= new FormData(form);	
                $.ajax({
                    type:'POST',
                    dataType:'json',
				    contentType: false,
				    processData: false,
                    data:formData,
                    url:baseUrl+'Students/studentDocs', 
                    success:function(resp)
                    {   
                       
			  			if(resp){
			  				alert('Data save successfully.');
			  				location.replace(baseUrl+'Students/manageStudent');
			  			}else{
			  				alert('Please fill Basic Details First!!!');
			  			}	//$('a[href="#personal"]').tab('show') 
			  			
                    },

                });
            }
    	});
    //add student

    $('.fa-edit').on('click',function(){
    	
    	/*var link = $(this).parent().attr('href');
    	//console.log(link);
    	window.open(link, '_blank');
    	return false;*/
    });
});

function getpages(offset,action){
 let returnval;
 let Active = "Active";
	let Inactive = "Inactive";
	let check =1;
        $.ajax({
                  type : 'post',
                  
                  data : {offset : offset},
                  url : baseUrl +'AjaxPagination/'+action,
                  success : function(response){
                        returnval = $.parseJSON(response);
                       },
                async: false    
              })
        return returnval;
}

