$(document).ready(function(){
	baseUrl = 'https://'+$(location).attr('hostname')+'/';
	//console.log(baseUrl

        $('#countyus').on('change',function(){
      var key = $(this).val();
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Admins/findState/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#statevizag').children().remove();
                        $('#statevizag').append(resp);
                        
                    }
              })
    });

    $('#statevizag').on('change',function(){
      var key = $(this).val();
        //console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Admins/findCity/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#citylitu').children().remove();
                        $('#citylitu').append(resp);
                        
                    }
              })
          });        

        $('#refercountry').on('change',function(){
      var key = $(this).val();
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Admins/findState/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#referstate').children().remove();
                        $('#referstate').append(resp);
                        
                    }
              })
    });

    $('#referstate').on('change',function(){
      var key = $(this).val();
        //console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Admins/findCity/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#refercity').children().remove();
                        $('#refercity').append(resp);
                        
                    }
              })
    });

  //todolist
  $(document).on('click','.todo-done',function(){
    //var id = $(this).attr('data-id');
    var id_sent=0;
      $('.checkbox1').each(function(){
  
      if($(this).is(":checked"))
      {
        var id=$(this).attr('id');
        if(id_sent=="0")
        {
          id_sent=id;
        }else
        {
          id_sent=id_sent+','+id;
        }
        
      }
    });
      if(id_sent!="")
      {
      
         location.replace(baseUrl+"homes/todo_changestatus/"+id_sent);
          return false;
      } 
      else{
        alert('Please select a record ');
        return false;
      }
  });
  $(document).on('click','.todo-edit',function(){
    var id = $(this).attr('data-id');
      $.ajax({
        type : 'post',
        url : baseUrl+'homes/find_todo/'+id,
        success : function(resp){
          resps = $.parseJSON(resp);
          if(resps != ""){
            $("#todo-title").val(resps.ToDoList.title);
            $("#todo-id").val(resps.ToDoList.id);
            $("#todo-des").val(resps.ToDoList.description);
            $("#todo-date").val(resps.ToDoList.date);
            $("#todo-hour").val(resps.ToDoList.hour).change();
            $("#todo-min").val(resps.ToDoList.min).change();
            $("#edit-todolist").modal('show');
          }
        }

      });  
  });
  $(document).on('click','.todo-delete',function(){
    var id = $(this).attr('id');
    //console.log('hi');
      $.ajax({
        type : 'post',
        url : baseUrl+'homes/todoDelete/'+id,
        success : function(resp){
            if(resp == 'true'){
              id = atob(id);
              $("#todo-list-li-"+id).remove();
            }
            else{
              alert('Somthing Went Wrong !!!');
            }
        }

      });  
  });
  $('.todo-datepicker').datepicker({
      autoclose: true,
      format : "yyyy/mm/dd",
      startDate : new Date()
    });
    $('#todolist-form').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },

  });

  //review
  $(document).on('click','.like-count',function(){
        let id = $(this).attr('data-id');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Homes/addCount/1/'+id,
                  success : function(response){
                    console.log(response);
                        if(response == true){
                            $('#para-'+id).remove();
                            var count = parseInt($('#like-count-'+id).text())+1;
                            $('#like-count-'+id).text(count);
                          }else{
                            alert('Somthing went wrong !!!');
                          }
                       },
                async: false
              });

    });
  $(document).on('click','.dislike-count',function(){
        let id = $(this).attr('data-id');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Homes/addCount/2/'+id,
                  success : function(response){
                    console.log(response);
                        if(response == true){
                            $('#para-'+id).remove();
                              var count = parseInt($('#dislike-count-'+id).text())+1;
                            $('#dislike-count-'+id).text(count);
                          }else{
                            alert('Somthing went wrong !!!');
                          }
                       },
                async: false
              });

    });
    //Add cart
  $(document).on('click','.add-cart',function(){
        let college_id = $(this).attr('data-college-id');
        let course_id = $(this).attr('data-course-id');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Homes/addCart',
                  data : {college_id : college_id, course_id : course_id},
                  success : function(response){
                    console.log(response);
                        if(response == true){
                            var cart_count = parseInt($('#cart-count').text())+1;
                            $('#cart-count').text(cart_count);
                            alert('Add to cart successfully');
                          }else if(response == false){
                            alert('Already in cart !!!');
                          }else{
                            alert('Somthing went wrong !!!');
                          }
                       },
                async: false
              });

    });

  $(document).on('click','.cart-delete',function(){
    if(window.confirm("Are you sure you want to delete this ?")){
        let id = $(this).attr('data-clg-course-id');
        let cost = atob($(this).attr('data-cost'));
        cost = parseFloat(cost);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'homes/delete_cart/'+id,
                  success : function(response){
                        if(response == true){
                            var cart_count = parseInt($('#cart-count').text())-1;
                            $('#cart-count').text(cart_count);

                            var nof_cart = parseInt($('#nof-cart').text())-1;
                            $('#nof-cart').text(nof_cart);

                            var tot_cart = parseInt($('.tot-cart').text()-cost);
                            $('.tot-cart').text(tot_cart);

                            $('#row-id-'+id).remove();
                        }
                    },
                async: false
              });
      }
    });
  //end cart

    //membership plan
    $('#plan-form').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            }
    });

    $('.plan-name').on('click',function(){
      var plan_id = $(this).val();
      $.ajax({
          type : 'post',
          url : baseUrl +'Homes/findPlan/'+plan_id,
          success : function(response){
                        //resp = $.parseJSON(response);
                console.log(response);
                        //$('#state').children().remove();
                $('#plan-cost').val(response);
                        
          }
      })
    });
    //end membership plan

  //notify validate
  $('#notify-form').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function (form) {
            console.log(form);
              let formData= new FormData(form); 
                $.ajax({
                    type:'POST',
                    dataType:'json',
                    contentType: false,
                    processData: false,
                            data:formData,
                            url:baseUrl+'AjaxEdit/notifySave', 
                            success:function(resp)
                            {   
                               if(resp){
                                $('#notifyModal').modal('hide'); 

                                }
                
              
                    },

                });
            }
  });




  //date picker
  $('.event-datepicker').datepicker({
      autoclose: true,
      format : "dd/mm/yyyy",
      startDate : new Date()
    });

  //login model
  $('#login-std').prop('checked', true);
  $('.login-cls').on('click',function(){
      if($('#login-std').is(':checked')){
        $('.firstid_3_cls').addClass("in active");
        $('.firstid_2_cls').removeClass("in active");
       }
       else if($('#login-col').is(':checked')){
        $('.firstid_2_cls').addClass("in active");
        $('.firstid_3_cls').removeClass("in active");
       }
  });
  //login model

  //Registration model
  $('#reg-std').prop('checked', true);
  $('.reg-cls').on('click',function(){
      if($('#reg-col').is(':checked')){
        $('#firstid').addClass("in active");
        $('#secend-id').removeClass("in active");
       }
       else if($('#reg-std').is(':checked')){
        $('#secend-id').addClass("in active");
        $('#firstid').removeClass("in active");
       }
  });
  //login model
	//custom select box

	$('.custom-select').select2();

	//cusomt select box ends

	//find state
	$('#country').on('change',function(){
      var key = $(this).val();
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Admins/findState/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#state').children().remove();
                        $('#state').append(resp);
                        
                    }
              })
    });

    $('#state').on('change',function(){
      var key = $(this).val();
      	//console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Admins/findCity/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#city').children().remove();
                        $('#city').append(resp);
                        
                    }
              })
    });

    $('#notifycountry').on('change',function(){
      var key = $(this).val();
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Admins/findState/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#notifystate').children().remove();
                        $('#notifystate').append(resp);
                        
                    }
              })
    });

    $('#notifystate').on('change',function(){
      var key = $(this).val();
        //console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Admins/findCity/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#notifycity').children().remove();
                        $('#notifycity').append(resp);
                        
                    }
              })
    });

    //College signup start
	$('#college-signup').validate({
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },

	});

	$("#add-confirm-pass").rules('add',{equalTo: "#add-new-pass",
	messages: {equalTo: "New password and confirm password field doesn't match."}});


	//College signup validate ends

	 //CollegeStudent signup start
	$('#student-signup').validate({
    rules: {
          "email": {
              email : true,
             remote: {
                  url: baseUrl+"Homes/checkEmail",
                  type: "post"
                  
               }
          }    
        },
    messages : {
          "email": {
              remote : "Email already exists."
            }
        
        },
		errorPlacement: function(error, element) {
     		var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
  		},
  		highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
		success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },

	});

	$("#student-confirm-pass").rules('add',{equalTo: "#student-pass",
	messages: {equalTo: "New password and confirm password field doesn't match."}});

	$('.dob').datepicker({
      autoclose: true,
      format : "dd/mm/yyyy",
      endDate : new Date()
    });
	//Student signup validate ends

	$('.common-datepicker').datepicker({
      autoclose: true,
      format : "dd/mm/yyyy"
    });
	
  //show gallery images of colleges start
  $(document).on('click','.doctor-photo,.trnd-clg',function(){

          let collegeId = $(this).attr('college-id');
          let collegeName = $(this).attr('college-name');

          $.ajax({
                    type : 'post',
                    
                    data : {
                            collegeId : collegeId
                          },
                    url : baseUrl+'Homes/getGallery',
                    success : function(response){
                          let resp = $.parseJSON(response);
                          //console.log(resp);
                          let imgData = '<div class="owl-carousel owl-theme glry-popup">';
                          if(resp.status == true){
                            resp.data.forEach(function(element) {
                              if(element.Gallery.name){
                                imgData+='<div class="item"> <img src="'+baseUrl+'img/gallery/large/'+element.CollegeGallery.image+'" class="img-responsive name_colleg_1" alt="Gallery"> <p class="Fro_nt">'+element.Gallery.name +'</p></div>';
                              }
                               /*imgData+='<div class="item"> <img src="'+baseUrl+'img/gallery/large/'+element.CollegeGallery.image+'" class="img-responsive name_colleg_1" alt="Gallery"> <p class="Fro_nt">No Title</p></div>';*/
                            }); 
                          }else{
                              imgData+='<div class="item"><p>No images in gallery.</p></div>';
                            }  
                          imgData+='</div>';   
                          $('.gallery-images').html(imgData);
                          //let append = '';
                          $('.glry-clg-name').text(collegeName);
                          $('.loader').show();
                          
                          
                         },
                         complete: function(){
                          $('.glry-popup').owlCarousel({
                               loop: true,
                               items: 1,
                               margin: 10,
                               nav: true
                           })
                          
                        },
                        async: true 
                });
          $('#gallery-popup').modal('show');
          $('.loader').hide();
        });


  //show gallery images of colleges ends

  //College description page contact us start
  $('#clg-cnt-us').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function(form) {
      let formData= new FormData(form); 
      $.ajax({
        type: 'post',
        mimeType:'application/json',
        dataType:'json',
        data: formData,
        contentType: false,
        processData: false,
        url:baseUrl+'Homes/collegeContact',
        success:function(resp){
          //console.log(resp);
          if(resp.status == "true"){
              $('#scs-msg').text(resp.msg);
              $('#frontend-msg').modal('show');
          }else{
            $('#scs-msg').text(resp.msg);
            $('#frontend-msg').modal('show');
          }
        }
      });
      
      } 
  });


  //College description page contact us ends


  //College description page Review start
  $('#clg-rvw').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function(form) {
      let formData= new FormData(form); 
      $.ajax({
        type: 'post',
        mimeType:'application/json',
        dataType:'json',
        data: formData,
        contentType: false,
        processData: false,
        url:baseUrl+'Homes/collegeReview',
        success:function(resp){
          //console.log(resp);
          if(resp.status == "login"){
            $('#my_Login12').modal('show');
          }else if(resp.status == "true"){
              $('#review-modal').modal('hide');
              $('#scs-msg').text(resp.msg);
              $('#frontend-msg').modal('show');
          }else{
            $('#review-modal').modal('hide');
            $('#scs-msg').text(resp.msg);
            $('#frontend-msg').modal('show');
          }
        }
      });
      
      } 
  });

  $('#input-3').rating();
  $('.common-rating').rating({
  });
  //College description page Review ends

  //college compare
  $(document).on('click','.colg-cmp',function(){
        let clgId = $(this).attr('id');
        let obj = $(this);
           $.ajax({
                    type : 'post',
                    
                    data : {
                            collegeId : clgId
                          },
                    url : baseUrl+'Homes/addToCompare',
                    success : function(response){

                          let resp = $.parseJSON(response);
                          if(resp.status == 'true'){
                            let app = '<div class="college-padding row"><div class="col-md-12 college"><div class=col-md-7><img class="college-padding ig-responsive"src="'+baseUrl+'img/collegeImages/small/'+resp.image+'"width=100%></div><div class=col-md-3><h4>'+resp.name+'</h4></div><div class=col-md-1><a href="'+baseUrl+'Homes/removeCompare/'+resp.id+'"><i aria-hidden=true class="fa fa-times"></i></a></div></div></div>';
                            $('.compare-wrap').append(app);
                            $('.fdbk-pnl3').show();
                            $('#compare-cnt').text('Comparison('+resp.count+')');
                            obj.css({'color':'blue'});
                            obj.text('Added to Compare');
                          }else{
                          //console.log(resp.msg);

                            $('#scs-msg').text(resp.msg);
                            $('#frontend-msg').modal('show');
                            //let app = ''
                          }
                         },
                  async: true    
                });
      });
  //college copare ends


  //student login 
  $('#std-login-form').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function(form) {
      let formData= new FormData(form); 
      $.ajax({
        type: 'post',
        mimeType:'application/json',
        dataType:'json',
        data: formData,
        contentType: false,
        processData: false,
        url:baseUrl+'Homes/checkLogin',
        success:function(response){
          let resp = response;
          //console.log(resp.status);
          if(resp.status == "true"){
            form.submit();
          }else{
            $('.login-error-msg').text(resp.msg);
          }
          
        }
      });
      
      } 
  });
  //student login ends


    //college login 
  $('#clg-login-form').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function(form) {
      let formData= new FormData(form); 
      $.ajax({
        type: 'post',
        mimeType:'application/json',
        dataType:'json',
        data: formData,
        contentType: false,
        processData: false,
        url:baseUrl+'Homes/checkClgLogin',
        success:function(response){
          let resp = response;
          //console.log(resp.status);
          if(resp.status == "true"){
            form.submit();
          }else{
            $('.clg-login-error-msg').text(resp.msg);
          }
          
        }
      });
      
      } 
  });
  //college login ends


  //header location search
    $('#location-inpt').keyup(function(){
      let key = $(this).val();
      $.ajax({
        type: 'post',
        data: {key : key},
        url:baseUrl+'Homes/getStateByName',
        success:function(response){
          let resp = $.parseJSON(response);
         //console.log(resp);
         let data = '<h4>Top Cities</h4>';
          if(resp.status == "true"){
            
            resp.data.forEach(function(element) {
              data+='<li><a href="'+baseUrl+'Homes/search/?location='+element.State.statename+'-'+element.State.id+'">'+element.State.statename+',</a></li>';
            });
            $('.cit-opt').html(data);
            //$('.loc-name').text(key);
            
          }else{
            data+='<li><a href="javascript:void(0)">No location found</a></li>';
            $('.cit-opt').html(data);
          }
          
        }
      });
    });
    $('.location-srch').on('click',function(){

    });
  //header location search ends

   //add student
      $('#student-academic').validate({
        errorPlacement: function(error, element) {
          //element.before('');
          var icon = $(element).prev('i');
              icon.removeClass('fa-check').addClass("fa-warning");  
              icon.attr("title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) { // hightlight error inputs
              $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
              },
      
      success: function (label, element) {
              var icon = $(element).prev('i');
              $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
              icon.removeClass("fa-warning").addClass("fa-check");
                  
            },
            submitHandler: function (form) {
              console.log(form);
                let formData= new FormData(form); 
                $.ajax({
                    type:'POST',
                    dataType:'json',
            contentType: false,
            processData: false,
                    data:formData,
                    url:baseUrl+'Students/addStudentAcademic', 
                    success:function(resp)
                    {   
                       if(resp){
                        $('a[href="#personal"]').tab('show') 

              }else{
                alert('Please Fill Basic Details First!!');
              }
                
              
                    },

                });
            }
      });
        $("#student-basics").validate({ 
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                  "email": {
                            required: true,
                            email : true,
                           remote: {
                                url: baseUrl+"Homes/checkEmail",
                                type: "post"
                                
                             }
                        }    
              },
            messages : {
                "email": {
                    remote : "Email already exists."
                  }
              
              },

 
            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
                
            },

            submitHandler: function (form) {
                let formData= new FormData(form); 
                $.ajax({
                    type:'POST',
                    dataType:'json',
            contentType: false,
            processData: false,
                    data:formData,
                    url:baseUrl+'Students/addStudent', 
                    success:function(resp)
                    {   //alert(resp.formimage);
                       if(resp.status == 'true'){
                $('.student-id').val(resp.studentId);
                $('#formimage').attr('src',baseUrl+'img/studentImages/small/'+resp.formimage);
                $('a[href="#academic"]').tab('show') 
                $('html, body').animate({ scrollTop: 0 }, 0);
              }else{
                alert('Network error, Please try again!!!');
              }
                    }
                });
            }
        });



        $("#personal-details").validate({ 
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                /*fullname: { 
                        required: true
                    }*/
            }, 

 
            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
                
            },

            submitHandler: function (form) {
                let formData= new FormData(form); 
                $.ajax({
                    type:'POST',
                    dataType:'json',
            contentType: false,
            processData: false,
                    data:formData,
                    url:baseUrl+'Students/studentPersonal', 
                    success:function(resp)
                    {   
                       if(resp){
                $('a[href="#experience"]').tab('show') 
                $('html, body').animate({ scrollTop: 0 }, 0);
              }else{
                alert('Please fill Basic Details First!!!');
              }
                    }
                });
            }
        });


        $('#student-experience').validate({
        errorPlacement: function(error, element) {
          //element.before('');
          var icon = $(element).prev('i');
              icon.removeClass('fa-check').addClass("fa-warning");  
              icon.attr("title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) { // hightlight error inputs
              $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
              },
      
      success: function (label, element) {
              var icon = $(element).prev('i');
              $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
              icon.removeClass("fa-warning").addClass("fa-check");
                  
            },
            submitHandler: function (form) {
              console.log(form);
                let formData= new FormData(form); 
                $.ajax({
                    type:'POST',
                    dataType:'json',
            contentType: false,
            processData: false,
                    data:formData,
                    url:baseUrl+'Students/studentExperience', 
                    success:function(resp)
                    {   
                       if(resp){
                $('a[href="#awards"]').tab('show')
                $('html, body').animate({ scrollTop: 0 }, 0); 
              }else{
                alert('Please fill Basic Details First!!!');
              }
                
              
                    },

                });
            }
      });



      $('#student-awards').validate({
        errorPlacement: function(error, element) {
          //element.before('');
          var icon = $(element).prev('i');
              icon.removeClass('fa-check').addClass("fa-warning");  
              icon.attr("title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) { // hightlight error inputs
              $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
              },
      
      success: function (label, element) {
              var icon = $(element).prev('i');
              $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
              icon.removeClass("fa-warning").addClass("fa-check");
                  
            },
            submitHandler: function (form) {
              console.log(form);
                let formData= new FormData(form); 
                $.ajax({
                    type:'POST',
                    dataType:'json',
            contentType: false,
            processData: false,
                    data:formData,
                    url:baseUrl+'Students/studentAwards', 
                    success:function(resp)
                    {   
                       if(resp){
                $('a[href="#Docs"]').tab('show') 
                $('html, body').animate({ scrollTop: 0 }, 0);
              }else{
                alert('Please fill Basic Details First!!!');
              }
                
              
                    },

                });
            }
      });



      $('#student-docs').validate({
        errorPlacement: function(error, element) {
          //element.before('');
          var icon = $(element).prev('i');
              icon.removeClass('fa-check').addClass("fa-warning");  
              icon.attr("title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) { // hightlight error inputs
              $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
              },
      
      success: function (label, element) {
              var icon = $(element).prev('i');
              $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
              icon.removeClass("fa-warning").addClass("fa-check");
                  
            },
            submitHandler: function (form) {
              console.log(form);
                let formData= new FormData(form); 
                $.ajax({
                    type:'POST',
                    dataType:'json',
            contentType: false,
            processData: false,
                    data:formData,
                    url:baseUrl+'Students/studentDocs', 
                    success:function(resp)
                    {   
                       
              if(resp){
                alert('Data save successfully.');
                location.replace(baseUrl+'homes/memberList');
              }else{
                alert('Please fill Basic Details First!!!');
              } //$('a[href="#personal"]').tab('show') 
              
                    },

                });
            }
      });
    //add student

    //update image student

    $('#update-student-image').validate({
      errorPlacement: function(error, element) {
          //element.before('');
          var icon = $(element).prev('i');
              icon.removeClass('fa-check').addClass("fa-warning");  
              icon.attr("title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) { // hightlight error inputs
              $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
              },
      
      success: function (label, element) {
              var icon = $(element).prev('i');
              $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
              icon.removeClass("fa-warning").addClass("fa-check");
                  
            }
    });

    //udpate student image ends

    //udpate student password
    $('#update-student-password').validate({
      rules: {
          "current": {
              required: true,
             remote: {
                  url: baseUrl+"Homes/checkPassword",
                  type: "post"
                  
               }
          }    
        },
      messages : {
          "current": {
              remote : "Wrong current password."
            }
        
        },
      errorPlacement: function(error, element) {
          //element.before('');
          var icon = $(element).prev('i');
              icon.removeClass('fa-check').addClass("fa-warning");  
              icon.attr("title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) { // hightlight error inputs
              $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
              },
      
      success: function (label, element) {
              var icon = $(element).prev('i');
              $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
              icon.removeClass("fa-warning").addClass("fa-check");
                  
            }
    });
    $("#add-confirm-pass1").rules('add',{equalTo: "#add-new-pass1",
  messages: {equalTo: "New password and confirm password field doesn't match."}});

    //udpate student password ends


       //update College student

    $('#update-college-image').validate({
      errorPlacement: function(error, element) {
          //element.before('');
          var icon = $(element).prev('i');
              icon.removeClass('fa-check').addClass("fa-warning");  
              icon.attr("title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) { // hightlight error inputs
              $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
              },
      
      success: function (label, element) {
              var icon = $(element).prev('i');
              $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
              icon.removeClass("fa-warning").addClass("fa-check");
                  
            }
    });

    //udpate College image ends

    //udpate college password
    $('#update-college-password').validate({
      rules: {
          "current": {
              required: true,
             remote: {
                  url: baseUrl+"Homes/checkCollegePassword",
                  type: "post"
                  
               }
          }    
        },
      messages : {
          "current": {
              remote : "Wrong current password."
            }
        
        },
      errorPlacement: function(error, element) {
          //element.before('');
          var icon = $(element).prev('i');
              icon.removeClass('fa-check').addClass("fa-warning");  
              icon.attr("title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) { // hightlight error inputs
              $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
              },
      
      success: function (label, element) {
              var icon = $(element).prev('i');
              $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
              icon.removeClass("fa-warning").addClass("fa-check");
                  
            }
    });
    $("#add-confirm-pass2").rules('add',{equalTo: "#add-new-pass2",
  messages: {equalTo: "New password and confirm password field doesn't match."}});

    //udpate Colelge password ends



    //edit college stasts------------------------------------------>
    function editcollegedata(form){
    let formData= new FormData(form); 
     
      $.ajax({
        type: 'post',
        mimeType:'application/json',
          dataType:'json',
          data: formData,
          contentType: false,
          processData: false,
        url:baseUrl+'Colleges/editCollege',
        success:function(resp){
          if(resp){
            $('a[href="#profile"]').tab('show') 
          }else{
            alert('Network error, Please try again!!!');
          }
        }
      });
  }
  $('#edit-college-form').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function(form) {
      //console.log(form);
      editcollegedata(form);
      }
  });

    function nearcollegeform(form){

  let formData= new FormData(form); 
  //formData.append(form);
  //console.log(this);
  $.ajax({
    type: 'post',
    mimeType:'application/json',
    dataType:'json',
    data: formData,
    contentType: false,
    processData: false,
    url:baseUrl+'Colleges/collegeNear',
    success:function(resp){
      if(resp){
        $('a[href="#settings"]').tab('show') 
      }else{
        alert('Network error, Please try again!!!');
      }
    }
  }); 

  }


  $('#near-college-form').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function(form) {
      //console.log(form);
      nearcollegeform(form);
      }
  });


  function aboutcollegeform(form){

  let formData= new FormData(form); 
  //formData.append(form);
  //console.log(this);
  $.ajax({
    type: 'post',
    mimeType:'application/json',
      dataType:'json',
      data: formData,
      contentType: false,
      processData: false,
      url:baseUrl+'Colleges/collegeAbout',
      success:function(resp){
        if(resp){
          $('a[href="#facility"]').tab('show') 
        }else{
          alert('Network error, Please try again!!!');
        }
      }
    }); 

  }


  $('#about-college-form').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function(form) {
      //console.log(form);
      aboutcollegeform(form);
      }
  });

  function facilitycollegeform(form){

  let formData= new FormData(form); 
  //formData.append(form);
  //console.log(this);
  $.ajax({
    type: 'post',
    mimeType:'application/json',
      dataType:'json',
      data: formData,
      contentType: false,
      processData: false,
      url:baseUrl+'Colleges/collegeFacilityajax',
      success:function(resp){
        if(resp){
          $('a[href="#add-field"]').tab('show') 
          //alert('Data save successfully.'); 
        }else{
          alert('Network error, Please try again!!!');
        }
      }
    }); 

  }


  $('#facility-college-form').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function(form) {
      //console.log(form);
      facilitycollegeform(form);
      }
  });


  function fieldcollegeform(form){

  let formData= new FormData(form); 
  //formData.append(form);
  //console.log(this);
  $.ajax({
    type: 'post',
    mimeType:'application/json',
      dataType:'json',
      data: formData,
      contentType: false,
      processData: false,
      url:baseUrl+'Colleges/collegeFieldajax',
      success:function(resp){
        if(resp){
          //$('a[href="#add-field"]').tab('show') 
          alert('Data save successfully.'); 
        }else{
          alert('Network error, Please try again!!!');
        }
      }
    }); 

  }


  $('#field-college-form').validate({
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function(form) {
      //console.log(form);
      fieldcollegeform(form);
      }
  });

  if($('textarea').is('#editor2')){
      CKEDITOR.replace('editor2');
      $('#cke_editor2').on('keypress',function(){
        $('#editor2').value() = editor2.getData();
      });
  }

  if($('textarea').is('#editor1'))
      CKEDITOR.replace('editor1');

    if($('textarea').is('#editor3'))
      CKEDITOR.replace('editor3');

    if($('textarea').is('#editor4'))
      CKEDITOR.replace('editor4');

  //edit college ends -------------------------------------->


  //add college course
    $('#add-college-course').validate({
      errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    });
    //add college course

     //add stream to college
    $(document).on('change','.stream-select',function(){
      var key = $(this).val();
      var id = $(this).attr('data-id');
        //console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/findProgram/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#program-select-'+id).children().remove();
                        $('#course-select-'+id).children().remove();
                        $('#specialization-select-'+id).children().remove();
                        $('#program-select-'+id).append(resp);
                        
                    }
              })
    });

    $(document).on('change','.program-select',function(){
      var key = $(this).val();
      var id = $(this).attr('data-id');
        //console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/findCourse/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#course-select-'+id).children().remove();
                        $('#specialization-select-'+id).children().remove();
                        $('#course-select-'+id).append(resp);
                        
                    }
              })
    });

    $(document).on('change','.course-select',function(){
      var key = $(this).val();
      var id = $(this).attr('data-id');
        //console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/findSpecialization/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('#specialization-select-'+id).children().remove();
                        $('#specialization-select-'+id).append(resp);
                        
                    }
              })
    });

     $('.event-datepicker').datepicker({
      autoclose: true,
      format : "dd/mm/yyyy",
      startDate : new Date()
    });
    //add stream to college ends  


    //add event
    $('#add-event-form').validate({
      errorPlacement: function(error, element) {
        //element.before('');
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('td').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('td').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    });
    //add event

    //home page header search starts

    $('#hdr-search').keyup(function(){
      let q = $(this).val();
      if(q != ''){
        $.ajax({
            type : 'post',
            url : baseUrl +'Homes/clgSearch/'+q,
            success : function(response){
                        resp = $.parseJSON(response);
                        let app = '';
                        $('#srch-result').children().remove();
                        if(resp != ''){
                          resp.forEach(function(element) {
                            if(element.College.short_name != ""){
                             app+='<li><a href="javascript:void(0)" class="clg-name" data-id="'+element.College.id+'" data-loc="'+element.City.city_name+'">'+element.College.name+' ('+element.College.short_name+'),'+element.College.location+'</a></li>';
                            }else{
                              app+='<li><a href="javascript:void(0)" class="clg-name" data-id="'+element.College.id+'" data-loc="'+element.City.city_name+'">'+element.College.name+','+element.College.location+'</a></li>';
                            }
                          })
                        }else{
                           app +='<li><a href="#">No College Found</a></li>';
                        }  
                        $('#srch-result').append(app);
                        $('#srch-result').css({'display':'block'});
                          
            }
        })
      }else{
          $('#srch-result').css({'display':'none'});
      }
    });
    let college_name ;
    $(document).on('click','.clg-name',function(){
      let id = $(this).attr('data-loc');
      let data_id = $(this).attr('data-id');
      college_name = data_id;
      $('#hdr-search').val($(this).text());
      let app = '<option>'+id+'</option>';
      $('#clg-srch-location').children().remove();
      $('#advanced-srch').css({'display':'block'});
      $('#hdr-srch-btn').attr('type','button');
      $('#clg-srch-location').append(app);
      $('#srch-result').css({'display':'none'});
      $.ajax({
          type : 'post',
          url : baseUrl +'Homes/clgStrm/'+data_id,
          success : function(response){
                      resp = $.parseJSON(response);
                      let app = '<option>Select Stream</option>';
                      $('#clg-srch-strm').children().remove();
                        resp.forEach(function(element) {
                           app +='<option value="'+element.CollegeCourse.stream_id+'">'+element.Stream.stream+'</option>';
                        })
                      
                      $('#clg-srch-strm').append(app);
                      //$('#srch-result').css({'display':'block'});
                        
          }
      })
    });

    $(document).on('change','#clg-srch-strm',function(){
      let strm = $(this).val();
      $.ajax({
          type : 'post',
          url : baseUrl +'Homes/clgCrs/'+college_name+'/'+strm,
          success : function(response){
                      resp = $.parseJSON(response);
                      let app = '<option>Select Course</option>';
                      $('#clg-srch-crs').children().remove();
                        resp.forEach(function(element) {
                           app +='<option value="'+element.CollegeCourse.course_id+'">'+element.Course.course+'</option>';
                        })
                      
                      $('#clg-srch-crs').append(app);
                      //$('#srch-result').css({'display':'block'});
                        
          }
      })

    })

    $('#hdr-srch-btn').on('click',function(){
      let course = $('#clg-srch-crs').val();
        let collegename = 'srch-'+college_name;
        let link = baseUrl+'Homes/college/'+collegename+'/'+course;
        window.open(link,'_blank');
      

    });

    $('#hdr-search-crse').keyup(function(){
      let q = $(this).val();
      if(q != ''){
        $.ajax({
            type : 'post',
            url : baseUrl +'Homes/clgSearchcrse/'+q,
            success : function(response){
                        resp = $.parseJSON(response);
                        let app = '';
                        $('#srch-result-crse').children().remove();
                        if(resp != ''){
                          resp.forEach(function(element) {
                             app +='<li><a href="javascript:void(0)" class="course-name" data-id="'+element.Course.id+'" data-loc="'+element.Course.course+'">'+element.Course.course+'</a></li>';
                          })
                        }else{
                           app +='<li><a href="#">No Course Found</a></li>';
                        }  
                        $('#srch-result-crse').append(app);
                        $('#srch-result-crse').css({'display':'block'});
                          
            }
        })
      }else{
        $('#srch-result-crse').css({'display':'none'});
      }
    });

    $(document).on('click','.course-name',function(){
      let data_id = $(this).attr('data-id');
      $('#hdr-search-crse').attr('data-id',data_id);
      $('#hdr-search-crse').val($(this).text());
      $('#srch-result-crse').css({'display':'none'});
      
    });


    $('#hdr-srch-btn-crse').on('click',function(){
      let coursename = 'course-'+$('#hdr-search-crse').attr('data-id');
        let link = baseUrl+'search/?course='+coursename;
        window.location.replace(link);
    });
    //homepage header search ends


    //wallet transfer

    $('#wallet-form').validate({
    rules: {
          "email": {
              required: true,
              email : true,
           
          }    
        },
        
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function (form) {
            //console.log(form);
              let formData= new FormData(form); 
                $.ajax({
                    type:'POST',
                    dataType:'json',
                    contentType: false,
                    processData: false,
                            data:formData,
                            url:baseUrl+'Homes/walletTransfer', 
                            success:function(resp)
                            {   
                               if(resp['status'] == 'true'){
                                $('.wallet-error').text(resp['msg']);
                                if(resp['amount'] != '')
                                  $('#wallet-amt').text(resp['amount']); 


                                }
                
              
                    },

                });
            }        

  });

    //wallet transfer  ends

        //forgot password

    $('#forgot-form').validate({
    rules: {
          "email": {
              required: true,
              email : true,
           
          }    
        },
        
    errorPlacement: function(error, element) {
        var icon = $(element).prev('i');
            icon.removeClass('fa-check').addClass("fa-warning");  
            icon.attr("title", error.text()).tooltip({'container': 'body'});
      },
      highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },
    
    success: function (label, element) {
            var icon = $(element).prev('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
                
            },
    submitHandler: function (form) {
            //console.log(form);
              let formData= new FormData(form); 
                $.ajax({
                    type:'POST',
                    dataType:'json',
                    contentType: false,
                    processData: false,
                            data:formData,
                            url:baseUrl+'Homes/forgetPassword', 
                            success:function(response)
                            {   
                              let resp = response;
                              //console.log(resp.status);

                                $('#forgot-error').text(resp.msg);
                                
              
                    },

                });
            }        

  });

    //forgot pass ends
});
