<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AjaxEditController extends AppController {
	
	public $components = array('Session','Email','RequestHandler','Cookie','Paginator');
	public $helpers = array('Session','Html');
    var $layout = 'admin';
    var $uses = array('Admin','College','CollegeMeta','Facility','CollegeFacility','CollegeNear','CollegeCourse','Degree','Stream','Program','Course','Specialization','Event','Gallery','CollegeGallery','New','Link','CollegeMaster','State','City','Country');
    public function beforeFilter()
    {
        parent::beforeFilter();
        $actions = array('login','forgetPassword','editEvent','notifySave');
        if(!in_array($this->params['action'], $actions))
        {
            if(!$this->Session->check('Admin') )
            {
            	$this->redirect(array('controller'=>'Admins','action'=>'login'));
        	}
   		}/*else{
   			if($this->Session->check('Admin'))
            {
            	$this->redirect(array('controller'=>'Admins','action'=>'dashboard'));
        	}
   		}*/

        $adminDetails = $this->Admin->findById($this->Session->read('Admin.id'));
        $this->set(compact('adminDetails'));
  

    }
    public function editStream(){
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
       
         try{
            $this->Stream->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
    }

    public function editProgram(){
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
        
         try{
            $this->Program->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
    }   
    public function editCourse(){
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
        //pr($data);die;
        try{
            $this->Course->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
    }    

    public function editSpecialization(){
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
        $this->Specialization->save($data);die;
    } 

    public function editGallery(){
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
        try{
            $this->Gallery->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
        
    } 

    public function editNew(){
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
        $this->New->save($data);die;
    } 

    public function editLink(){
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
        $this->Link->save($data);die;
    }

    public function editCollegeMaster(){
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
        //$resp = [];
        try{
            $this->CollegeMaster->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
    } 
    public function editCity(){
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
             $data['updated_by'] = $this->Session->read('Admin.id');
            $data['updated_date'] = date('Y-m-d');
        }
        //pr($data);die;
        $resp = [];
        try{
            $this->City->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
    } 


    public function editState(){
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
            $data['updated_by'] = $this->Session->read('Admin.id');
            $data['updated_date'] = date('Y-m-d');
               
        }
        //pr($data);die;
        try{
            $this->State->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
    } 

    public function editCountry(){
        $predata = $this->request->data;
        pr($predata);die;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
            $data['updated_by'] = $this->Session->read('Admin.id');
            $data['updated_date'] = date('Y-m-d');
               
        }
        pr($data);die;
            $destination = realpath('../webroot/img/') . '/';
            $image = $this->Components->load('Resize');  
            //$data = $this->data;
            //pr($_FILES);
            if(is_uploaded_file($_FILES['image']['tmp_name']))
            {
                
                $filenameimg = time().'-'.$_FILES['image']['name'];
                $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                
                $data['Country']['symbol']=$filenameimg;
                
                
            }
        try{
            $this->Country->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
    } 

    public function editFacility(){
        $predata['Facility'] = $this->request->data;
        //$data = [];
         //pr($_FILES);
        if (!empty($_FILES['image']['name'])) 
                {
                    $destination = realpath('../webroot/img/facility/') . '/';
                    $image = $this->Components->load('Resize');  

                    if(is_uploaded_file($_FILES['image']['tmp_name']))
                    {
                        
                        $filenameimg = time().'-'.$_FILES['image']['name'];
                        $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',300,300,0,0,0,0);
                        $predata['Facility']['icon']=$filenameimg;
                        
                        
                    }
                }
        //pr($predata);die;
            
            // $data[$value['name']] = $value['value'];
        
        //pr($predata);die;
        $this->Facility->save($predata);
        echo "true";
        die;
    }
    public function editReference(){
        $this->loadModel('ReferenceAmount');
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
        $this->ReferenceAmount->save($data);die;
    }
    public function editEvent(){
        $this->loadModel('Event');
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
        //pr($data);die;
        //$this->Event->save($data);die;
        try{
            $this->Event->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
    }

    public function editExam(){
        $this->loadModel('Exam');
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
       
         try{
            $this->Exam->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
    }
    public function notifySave(){
        $this->loadModel('Notify');
        $data = $this->data;
        //pr($data);die;
        if(empty($data['Notify'])){
            echo "false";
            die;
        }
        else if(!empty($data['Notify'])){
            $check = $this->Notify->find('first', array('conditions' => array('Notify.email' => $data['Notify']['email'], 'Notify.course_id' => $data['Notify']['course_id'])));
            if(empty($check)){
                $this->Notify->create();
                $this->Notify->save($data);
            }
            
            echo "true";
            die;
        } 
        
    }
    public function editCollegeField(){
        $this->loadModel('CollegeField');
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
        //pr($data);die;
        //$this->Event->save($data);die;
        try{
            $this->CollegeField->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
    }
    public function editTestLevel(){
        $this->loadModel('TestLevel');
        $predata = $this->request->data;
        $data = [];
        foreach ($predata as $value) {
            
             $data[$value['name']] = $value['value'];
        }
        //pr($data);die;
        //$this->Event->save($data);die;
        try{
            $this->TestLevel->save($data);
            echo 'true';
         }
         catch(Exception $e){
                        echo 'false'; 

                    } 
        die;
    }
}
