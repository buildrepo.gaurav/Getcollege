<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class StudentsController extends AppController {
	
	public $components = array('Session','Email','RequestHandler','Cookie','Paginator');
	public $helpers = array('Session','Html');
    var $layout = 'admin';
    var $uses = array('Admin','Student');
    public function beforeFilter()
    {
        parent::beforeFilter();
        $actions = array('login','forgetPassword','addStudent','addStudentAcademic','studentPersonal','studentExperience','studentAwards','studentDocs');
        if(!in_array($this->params['action'], $actions))
        {
            if(!$this->Session->check('Admin') )
            {
            	$this->redirect(array('controller'=>'Admins','action'=>'login'));
        	}
   		}/*else{
   			if($this->Session->check('Admin'))
            {
            	$this->redirect(array('controller'=>'Admins','action'=>'dashboard'));
        	}
   		}*/

        $adminDetails = $this->Admin->findById($this->Session->read('Admin.id'));
        $this->set(compact('adminDetails'));
  

    }

    public function manageStudent(){
        //$this->Paginator->settings = array('Student' => array('conditions' => array('')));
        //$students=$this->paginate('Student');
        $students = $this->Student->find('all', array('limit' => 10, 'offset' => 0, 'conditions' => array('is_delete'=>'0') ));
        $this->set(compact('students'));
    }

    public function addStudent($id=null)
    {
        $this->loadModel('Student');
        $edit = $this->Student->findById($id);
        if(!empty($id)){
            $flag = 'edit';
        }
        $this->loadModel("Country");
        $country = $this->Country->find('all');
        $this->set(compact('country','edit','flag'));
        if($this->request->is('post') && !empty($this->data))
        {
            //pr($this->data);die;
            $data['Student'] = $this->data;
            $data['Student']['created_date'] = date("Y-m-d");
            $data['Student']['profile_complete_per'] = 1;
            /*pr($_FILES);
            pr($data);die;*/
            //$data['College']['password'] = md5($this->data['College']['password']);
            if (!empty($_FILES['image']['tmp_name'])) 
            {
                $destination = realpath('../webroot/img/studentImages/small/') . '/';
                $destination1 = realpath('../webroot/img/studentImages/large/') . '/';
                $image = $this->Components->load('Resize');  

                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination1 . $filenameimg);
                    $data['Student']['formimage']=$filenameimg;
                    
                    
                }
            }
            /*$formdata = $this->Form->find('list',array('conditions'=>array('college_id'=>'0','fields'=>'id')));
            $this->UserImage->saveAll($formdata);*/
            if($this->Student->save($data)){
                $resp['studentId'] = $this->Student->getLastInsertId();
                if(!empty($data['Student']['id'])){
                    $resp['studentId'] = $data['Student']['id'];
                }
                $formdata = $this->Student->findById($resp['studentId']);
                /*$uniqueId = uniqid($resp['studentId']);
                $this->College->id = $resp['studentId'];
                $this->College->saveField('unique_id', $uniqueId);
                $Email = new CakeEmail();
                $Email->from(array('info@Formsadda.com' => 'Formsadda'));
                $Email->to($data['College']['email']);
                $Email->subject('FormsAdda New Registration');
                $Email->send('Your User Id is '.$uniqueId.'and  password is'.$data['College']['password']);*/
                $resp['status'] = "true";
                $resp['formimage'] = $formdata['Student']['formimage'];
            }
            else{            
                $resp['status'] = "false";
            }
            echo json_encode($resp);die;
        }
    }


    public function addStudentAcademic(){
        $this->loadModel('StudentAcademic');
        $data = $this->data;
        //pr($data);die;
        if(empty($data['studentId'])){
            echo "false";
            die;
        }
        $this->StudentAcademic->deleteAll(array('student_id'=>$data['studentId']));
        foreach($data as $stu){
            if(!empty($stu['StudentAcademic'])){
                $stu['StudentAcademic']['student_id'] = $data['studentId'];
                //pr($stu);die;
                $this->StudentAcademic->create();
                $this->StudentAcademic->save($stu);
            }
        } 
        echo "true";
        die;
    }

    public function checkStudentEmail()
    {
        $email = trim($this->data['Student']['email']);
        $checkEmail = $this->Student->findByEmail($email);
        if(!empty($checkEmail))
            echo 'false';
        else
            echo 'true';
        die;
        
    }

    public function studentPersonal(){
        $this->loadModel('StudentMeta');
        $data = $this->data;
        if(empty($data['StudentMeta']['student_id'])){
            echo "false";
            die;
        }
        $this->StudentMeta->deleteAll(array('student_id'=>$data['StudentMeta']['student_id']));
        if($this->StudentMeta->save($data)){
            $resp['status'] = "true";
        }
        else{            
            $resp['status'] = "false";
        }
            echo json_encode($resp);die;
    }

    public function studentExperience(){
        $this->loadModel('StudentExperience');
        $data = $this->data;
        if(empty($data['studentId'])){
            echo "false";
            die;
        }
        //pr($data);die;
        $this->StudentExperience->deleteAll(array('student_id'=>$data['studentId']));
        foreach($data as $stu){
            if(!empty($stu['StudentExperience'])){
                $stu['StudentExperience']['student_id'] = $data['studentId'];
                //pr($stu);die;
                $this->StudentExperience->create();
                $this->StudentExperience->save($stu);
            }
        } 
        echo "true";
        die;
    }
    public function studentAwards(){
        $this->loadModel('StudentAward');
        $data = $this->data;
        if(empty($data['studentId'])){
            echo "false";
            die;
        }
        $this->StudentAward->deleteAll(array('student_id'=>$data['studentId']));
        //pr($data);die;
        foreach($data as $stu){
            if(!empty($stu['StudentAward'])){
                $stu['StudentAward']['student_id'] = $data['studentId'];
                //pr($stu);die;
                $this->StudentAward->create();
                $this->StudentAward->save($stu);
            }
        } 
        echo "true";
        die;
    }

    public function studentDocs(){
        $this->loadModel('StudentDocs');
        $data = $this->data;
        //pr($_FILES);
        if(empty($data['studentId'])){
            echo "false";
            die;
        }
        $this->StudentDocs->deleteAll(array('student_id'=>$data['studentId']));
        $destination = realpath('../webroot/img/docs/') . '/';
        
        foreach($data as $key => $val){
            if(!empty($val['StudentDocs']['name'])){
                $val['StudentDocs']['student_id'] = $data['studentId'];
                if (!empty($_FILES['file']['name'][$key])) 
                {
                    

                    if(is_uploaded_file($_FILES['file']['tmp_name'][$key]))
                    {
                        
                        $filenameimg = time().'-'.$_FILES['file']['name'][$key];

                        move_uploaded_file($_FILES['file']['tmp_name'][$key], $destination . $filenameimg);
                        $val['StudentDocs']['file']=$filenameimg;
                        
                        
                    }
                }
                //pr($_FILES);die;
                if(empty($val['StudentDocs']['id']))
                    $this->StudentDocs->create();
                $this->StudentDocs->save($val);
            }
        } 
        echo "true";
        die;
    }
    public function changeStatus($model = null, $id = null){
        $this->loadModel($model);
        $status = $this->$model->findById($id);
        
        if($status[$model]['status'] == '1'){
            $status[$model]['status'] = '0';
        }else{
            $status[$model]['status'] = '1';
        }
        
        $this->$model->save($status);
        echo $status[$model]['status'];
        die;
        //$this->redirect($this->referer());
    }
    public function delete($model = null, $id = null)
    {
        $this->loadModel($model);
        $CategoryId = base64_decode($id);
        $this->$model->id = $CategoryId;
        $this->$model->saveField('is_delete','1');
        //$this->redirect($this->referer());
        die;
    }

}
