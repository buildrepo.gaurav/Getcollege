<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor','TCPDF',array('file' => 'tcpdf/tcpdf.php'));  
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package     app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class HomesController extends AppController {
    
    public $components = array('Session','Email','RequestHandler','Cookie','Paginator');
    public $helpers = array('Session','Html');
    var $layout = 'newhome';
    var $uses = array('Wallet','Review','ReviewCount','OrderMeta','Order','Cart','StudentPlan','College','Country','State','City','Student','CollegeFacility','CollegeMeta','Course','Facility','CollegeGallery','CollegeCourse','CollegeContact','CollegeCompare','Program','Course','Stream','CollegeMaster','MembershipPlan');
    public function beforeFilter()
    {
        parent::beforeFilter();
        define('CorrectMarks',2);
        define('NegativeMarks',0.5);
        $country = $this->Country->find('all');
        $this->Stream->bindModel(
        array('hasMany' => array(
                'Program' => array(
                    'className' => 'Program',
                    'foreignKey' => 'stream_id',
                    'conditions' => array('Program.status'=>'1','Program.is_delete' => 0),
                    'fields' => array('program','id')
                    )
                )
            )
        );
        $this->Program->bindModel(
        array('hasMany' => array(
                'Course' => array(
                    'className' => 'Course',
                    'foreignKey' => 'program_id',
                    'conditions' => array('Course.status'=>'1','Course.is_delete' => 0),
                    'fields' => array('course','id')
                    )
                )
            )
        );
        $this->Course->bindModel(
        array('hasMany' => array(
                'Specialization' => array(
                    'className' => 'Specialization',
                    'foreignKey' => 'course_id',
                    'conditions' => array('Specialization.status'=>'1','Specialization.is_delete' => 0),
                    'fields' => array('specialization','id')
                    )
                )
            )
        );
        $this->Stream->bindModel(
        array('hasMany' => array(
                'CollegeCourse' => array(
                    'className' => 'CollegeCourse',
                    'foreignKey' => 'stream_id',
                    'fields' => array('CollegeCourse.college_id','id'),
                    'limit' => '5',
                    'order' => 'rand()'
                    )
                )
            )
        );
        $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'College' => array(
                    'className' => 'College',
                    'foreignKey' => 'college_id',
                    'fields' => array('name','id')
                    )
                )
            )
        );

        $hdrStates = $this->State->find('all',array('limit'=>'5'));
        $hdr_prgm = $this->Stream->find('all',array('conditions'=>array('status'=>'1', 'is_delete'=>'0'),'recursive'=>3,'fields'=>array('stream')));
        //pr($hdr_prgm);die;

        $compareclgs = $this->College->findAllById($this->Session->read('clgCompare'));
        $this->set(compact('compareclgs','hdrStates'));

        
        if($this->Session->check('Student') && $this->Session->check('Cart')){
            $data_carts = $this->Session->read('Cart');
            foreach(@$data_carts['applynow'] as $curs_id => $clg_id){
                $find_cart = $this->Cart->find('first', array('conditions' => array('college_id' => $clg_id, 'course_id' =>$curs_id, 'student_id' => $this->Session->read('Student.id'))));
                if(!empty($find_cart)){
                    $data['Cart']= $find_cart['Cart'];
                } else{
                    $data['Cart']['college_id'] = $clg_id;
                    $data['Cart']['course_id'] = $curs_id;
                    $data['Cart']['student_id'] = $this->Session->read('Student.id');
                }
                $this->Cart->create();
                $this->Cart->save($data);
            }
            $count = $this->Cart->find('count', array('conditions' => array('student_id' => $this->Session->read('Student.id'))));
        }
        else if($this->Session->check('Cart')){
            $count = count($this->Session->read('Cart.applynow'));
        }
        else if($this->Session->check('Student')){
            $count = $this->Cart->find('count', array('conditions' => array('student_id' => $this->Session->read('Student.id'))));
        }
        else{
            $count = 0;
        }

        
        if($this->Session->check('Student')){
            @$check_plan = $this->StudentPlan->findByStudentId($this->Session->read('Student.id'));
            @$plans = $this->MembershipPlan->find('all', array('conditions' => array('status' => 1,'id !=' => $check_plan['StudentPlan']['plan_id'])));
        }
        else{
            $plans = $this->MembershipPlan->find('all', array('conditions' => array('status' => 1)));
        }
        $this->set(compact('country','hdr_prgm','plans','check_plan','count'));


        $actions = array('referSchool','careerget','whygetcollege','contact','delete_cart','checkout','commentPage','addCart','login','forgetPassword','index','search','searchPagination','getGallery','college','collegeContact','addToCompare','checkEmail','studentSignup','studentLogin','checkLogin','checkClgLogin','getStateByName','logout','aboutus','privacy','terms','faqs','refereAFriend','ourteam','compare','activateStudent','activateCollege','collegeLogin','collegeSignup','downloadBrochure','findPlan','addToCompare','removeCompare','clgSearch','clgStrm','clgCrs','clgSearchcrse','landingPage','reviewPage','cart','contact_us','gsignin');

         if($this->Session->check('Student')){
           
                
                //pr($compareclgs);die;
                $details = $this->Student->findById($this->Session->read('Student.id'));
                //pr($details);die;
        
            $this->set(compact('details','compareclgs'));  
        }
        if(!in_array($this->params['action'], $actions))
        {
            // pr($this->Session->read());die;
            if($this->Session->check('Student'))
            {

                return true;
            }else if($this->Session->check('College')){
                $clgdetails = $this->College->findById($this->Session->read('College.id'));
                $this->set(compact('clgdetails'));
               return true;
            }else{
                if($this->request->is('ajax')){
                    $resp = [];
                    $resp['status'] ='login';
                    $resp['msg'] = 'Please login first!!!';
                    echo json_encode($resp);die;
                }
                $this->redirect(array('controller'=>'Homes','action'=>'index'));
            }
            
        } 

    }
    public function gsignin()
    {
        $this->loadModel('Student');
        //pr($_POST['name']);die;
        if($this->request->is('post'))
        {
            $count = $this->Student->find('first', array('conditions' => array('Student.email' => $_POST['email'])));
            if(!$count)
            {
                $data['Student']['email'] = $_POST['email'];
                $data['Student']['name'] = $_POST['name'];
                $data['Student']['image'] = $_POST['image'];
                //pr($data);die;
                $pass = RandomStringGenerator(8);
                $data['Student']['password'] = md5($pass);
                if($this->Student->save($data))
                {
                    $replace = array('{name}','{password}','{email}');
                    $with = array($checkEmailStudent['Student']['name'],$pass,$_POST['email']);
                    //$this->send_email('',$pass,'front_sociallogin_password',INFO_FORMSADDA,$checkEmailStudent['Student']['email']);
                    $this->send_email('',$replace,$with,'front_sociallogin_password',INFO_FORMSADDA,$_POST['email']);
                    $result = $this->Student->findByEmail($_POST['email']);
                    $this->Session->write('Student',$result['Student']);
                    echo 'true';

                }
                else
                {
                    echo 'false';
                }
                
            }
            else
            {
                $result = $this->Student->findByEmail($_POST['email']);
                $this->Session->write('Student',$result['Student']);
                //$this->redirect(array('action' => 'studentdashboard'));
                echo 'true';
            }
        }
        die;
    }
    public function contact_us()
    {
        $this->loadModel('Contact');
        if($this->request->is('post'))
        {
            $data = $this->data;
            //pr($data);die;
            $this->Contact->save($data);
            $this->Session->write('frontsuccess-msg','We shall contact you very soon!!');
            $this->redirect($this->referer());
        }
        
    }
    public function compare(){
        $this->loadModel('CourseExam');
        $this->College->bindModel(
            array('belongsTo' => array(
                    'City' => array(
                        'className' => 'City',
                        'foreignKey' => 'city_id'                        
                        )
                    )
                )
            );
        $this->College->bindModel(
            array('belongsTo' => array(
                    'State' => array(
                        'className' => 'State',
                        'foreignKey' => 'state_id'                        
                        )
                    )
                )
            );
        $this->College->bindModel(
            array('belongsTo' => array(
                    'Country' => array(
                        'className' => 'Country',
                        'foreignKey' => 'country_id'                        
                        )
                    )
                )
            );
        $this->College->bindModel(
            array('hasOne' => array(
                    'CollegeMeta' => array(
                        'className' => 'CollegeMeta',
                        'foreignKey' => 'college_id'
                        )
                    )
                )
            );
        $this->College->bindModel(
        array('hasMany' => array(
                'CollegeFacility' => array(
                    'className' => 'CollegeFacility',
                    'foreignKey' => 'college_id',
                    'fields' => array('facility_id')
                    )
                )
            )
        );
        $this->CollegeFacility->bindModel(
        array('belongsTo' => array(
                'Facility' => array(
                    'className' => 'Facility',
                    'foreignKey' => 'facility_id',
                    'fields' => array('icon','name')
                    )
                )
            )
        );
        $this->College->bindModel(
        array('hasMany' => array(
                'CollegeCourse' => array(
                    'className' => 'CollegeCourse',
                    'foreignKey' => 'college_id',
                    //'fields' => array('course_id','fees','brochure','avg_salary')
                    
                    )
                )
            )
        );
        $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Course' => array(
                    'className' => 'Course',
                    'foreignKey' => 'course_id',
                    'fields' => array('course')
                    )
                )
            )
        );
        $this->CollegeCourse->bindModel(
            array('hasMany' => array(
                    'CourseExam' => array(
                        'className' => 'CourseExam',
                        'foreignKey' => 'college_course_id'
                        )
                    )
                )
            );
         $this->CourseExam->bindModel(
            array('belongsTo' => array(
                    'Exam' => array(
                        'className' => 'Exam',
                        'foreignKey' => 'exam_id',
                        'fields' => array('exam')
                        )
                    )
                )
            );
       /* $this->College->bindModel(
        array('hasMany' => array(
                'Review' => array(
                    'className' => 'Review',
                    'foreignKey' => 'college_id',
                    'limit' => '1'
                    
                    )
                )
            )
        );*/
        $this->Course->unbindModel(array('hasMany'=>array('Specialization')));
        $this->College->recursive = 3;   
        $clgs = $this->College->findAllById($this->Session->read('clgCompare'));
        $this->set(compact('clgs'));
        //pr($clgs);die;

    }
    public function aboutus(){
         $this->loadModel('About');
        $about = $this->About->find('first');
        $this->set(compact('about'));
    }
     public function terms(){
        $this->loadModel('Term');
        $terms = $this->Term->find('first');
        $this->set(compact('terms'));
    }
     public function faqs(){
        $this->loadModel('Faq');
        if(empty($_GET['keyword'])){
            $faqs = $this->Faq->find('all',array('conditions'=>array('status'=>'1','is_delete'=>'0')));
        }
        else{
            $keyword = $_GET['keyword'];
            $faqs = $this->Faq->find('all',array('conditions'=>array('OR' => array(array('question LIKE' => '%'.$_GET['keyword'].'%'), array('answer LIKE' => '%'.$_GET['keyword'].'%')),'status'=>'1','is_delete'=>'0')));
        }
        
        $this->set(compact('faqs','keyword'));
    }
     public function privacy(){
        $this->loadModel('Privacy');
        $privacy = $this->Privacy->find('first');
        $this->set(compact('privacy'));
    }
     public function ourteam(){
         $this->loadModel('Team');
        $teams = $this->Team->find('all',array('conditions'=>array('status'=>'1','is_delete'=>'0')));
        $this->set(compact('teams'));
    }
    public function index(){
        $this->loadModel('Slider');
        $this->loadModel('New');
        $this->loadModel('Link');
        $this->loadModel('Testimonial');
        $this->loadModel('MembershipPlan');
        $testimonials = $this->Testimonial->find('all', array('conditions'=>array('status'=>'1')));
        $links = $this->Link->find('all', array('conditions'=>array('status'=>'1')));
        $slides = $this->Slider->find('all', array('conditions'=>array('status'=>'1','from_date <=' => date('Y-m-d'),'to_date >=' => date('Y-m-d'))));
        //;die;
        $news = $this->New->find('all',array('conditions'=>array('status'=>'1')));
        $trendingColleges = $this->College->find('all',array('conditions'=>array('status' =>'1','trending'=>'1')));

        $engColIds = $this->CollegeCourse->find('list', array('fields' => 'college_id','conditions' => array('stream_id' => 6)));
        $engineeringColleges = $this->College->find('all', array('limit' => 8,'conditions' => array('status' =>'1','id' => $engColIds)));

        $manColIds = $this->CollegeCourse->find('list', array('fields' => 'college_id','conditions' => array('stream_id' => 10)));
        $managementColleges = $this->College->find('all', array('limit' => 8,'conditions' => array('status' =>'1','id' => $manColIds)));

        $comColIds = $this->CollegeCourse->find('list', array('fields' => 'college_id','conditions' => array('stream_id' => 13)));
        $commerceColleges = $this->College->find('all', array('limit' => 8,'conditions' => array('status' =>'1','id' => $comColIds)));

        $mediColIds = $this->CollegeCourse->find('list', array('fields' => 'college_id','conditions' => array('stream_id' => 24)));
        $medicalColleges = $this->College->find('all', array('limit' => 8,'conditions' => array('status' =>'1','id' => $mediColIds)));

        $membership = $this->MembershipPlan->find('all', array('limit' => 3,'conditions' => array('status' => 1)));
        $this->set(compact('trendingColleges','slides','news','links','testimonials','membership','engineeringColleges','managementColleges','commerceColleges','medicalColleges'));
    }

    public function collegeLogin(){
         if($this->request->is('post') && !empty($this->request->data)){
            
            $data['unique_id'] = $this->request->data['username'];

            $data['password'] = md5($this->request->data['password']);
            //pr($data);
            $result = $this->College->find('first',array('conditions'=>array('unique_id'=>$data['unique_id'],'password'=>$data['password'])));
            //pr($result);die;
            if(count($result)>0)
            {   //pr($this->data['remember']);die;
                if(@$this->data['remember'] == '1'){

                }
                $this->Session->write('College',$result['College']);
                /*$result['Student']['current_ip'] = $_SERVER['REMOTE_ADDR'];
                $result['Student']['current_access']  = date("Y-m-d H:i:s");
                //pr($result);die;
                $this->Student->save($result); */   
                $this->redirect(array('action'=>'collegeDashboard'));
                    
            }else
            {
                $this->Session->write('loginerror-msg','Incorrect Username/Email or Password');
                $this->redirect(array('action'=>'index'));
                        
            }

        }
    }

    public function collegeDashboard(){

    }
    public function checkEmail(){
        //pr($_GET);
        $email = trim($this->data['email']);

        $checkEmail1 = $this->Student->findByEmail($email);
        $checkEmail2 = $this->College->findByEmail($email);
       
        if(!empty($checkEmail1) || !empty($checkEmail2))
            echo 'false';
        else
            echo 'true';
        die;
        die;
    }
    public function studentLogin(){
        if($this->request->is('post') && !empty($this->request->data)){
            
            $data['email'] = $this->request->data['username'];

            $data['password'] = md5($this->request->data['password']);
            $result = $this->Student->find('first',array('conditions'=>array('OR'=>array(array('email'=>$data['email']),array('mobile'=>$data['email'])),'password'=>$data['password'])));
            if(count($result)>0)
            {   //pr($this->data['remember']);die;
                if(@$this->data['remember'] == '1'){

                }
                $this->Session->write('Student',$result['Student']);
                $result['Student']['current_ip'] = $_SERVER['REMOTE_ADDR'];
                $result['Student']['current_access']  = date("Y-m-d H:i:s");
                //pr($result);die;
                $this->Student->save($result); 

                $wallet = $this->Wallet->findByStudentId($result['Student']['id']);
                if(empty($wallet)){
                    $wallet['Wallet']['student_id'] = $result['Student']['id'];
                    $wallet['Wallet']['amount'] = 0;
                    $wallet['Wallet']['created_date'] = date('Y-m-d H:i:s');
                            
                    $this->Wallet->create();
                    $this->Wallet->save($wallet); 
                }
                
                //$this->redirect(array('action'=>'studentdashboard'));
                //$this->Session->write('membership_popup','open');
                $this->redirect(array('action'=>'studentdashboard'));
                    
            }else
            {
                $this->Session->write('loginerror-msg','Incorrect Username/Email or Password');
                $this->redirect(array('action'=>'index'));
                        
            }

        }
    }
    public function studentdashboard(){
        Configure::write('debug', 0);
        //echo $this->Session->read('Student.plan_popup');die;
        /*$this->loadModel('StudentPlan');
        $currentplan = $this->StudentPlan->findByStudentId($this->Session->read('Student.id'));
        $cplan = $currentplan['StudentPlan']['plan_id'];
        $this->loadModel('MembershipPlan');
        $planoo = $this->MembershipPlan->findById($cplan);
        $this->set(compact('planoo'));
        if($this->Session->read('Student.plan_popup') == 0){
        $check_plan = $this->StudentPlan->findByStudentId($this->Session->read('Student.id'));
        if(empty($check_plan))
            $this->Session->write('membership_modal',$this->Session->read('Student.id'));
        }*/
        $this->loadModel('Compare');
        $this->loadModel('ToDoList');
        $this->loadModel('Student');
        $this->loadModel('StudentAcademic');
        $this->loadModel('StudentMeta');
        $this->loadModel('StudentExperience');
        $this->loadModel('StudentAward');
        $this->loadModel('StudentAnswer');
         $this->loadModel('Test');
         $testCount = $this->StudentAnswer->find('count',array('conditions'=>array('student_id'=>$this->Session->read('Student.id'))));
        $this->StudentAnswer->bindModel(
            array('belongsTo' => array(
                    'Test' => array(
                        'className' => 'Test',
                        'foreignKey' => 'test_id',
                        )
                    )
                )
            );
        $this->StudentAnswer->bindModel(
            array('belongsTo' => array(
                    'Student' => array(
                        'className' => 'Student',
                        'foreignKey' => 'student_id',
                        'fields' => 'name'
                        )
                    )
                )
            );
        $this->Test->bindModel(
            array('hasMany' => array(
                    'TestSection' => array(
                        'className' => 'TestSection',
                        'foreignKey' => 'test_id',
                        )
                    )
                )
            );
        $this->StudentAnswer->recursive = 3;
            $bestTest = $this->StudentAnswer->find('first',array('conditions'=>array('StudentAnswer.student_id'=>$this->Session->read('Student.id')),'order'=>'StudentAnswer.total_mark_obtain desc'));
            $this->StudentAnswer->bindModel(
            array('belongsTo' => array(
                    'Student' => array(
                        'className' => 'Student',
                        'foreignKey' => 'student_id',
                        'fields' => 'name'
                        )
                    )
                )
            );
            $topThreeInSameTest = $this->StudentAnswer->find('all',array('conditions'=>array('StudentAnswer.test_id'=>$bestTest['StudentAnswer']['test_id']),'order'=>'StudentAnswer.total_mark_obtain desc','limit'=>'3'));
            $this->StudentAnswer->bindModel(
            array('belongsTo' => array(
                    'Student' => array(
                        'className' => 'Student',
                        'foreignKey' => 'student_id',
                        'fields' => 'name'
                        )
                    )
                )
            );
            $topThreeInAll = $this->StudentAnswer->find('all',array('order'=>'StudentAnswer.total_mark_obtain desc','limit'=>'3'));
            $graphData = array();
            $TopperallData = json_decode($topThreeInSameTest['0']['StudentAnswer']['all_data'],true);
            $stuAllData = json_decode($bestTest['StudentAnswer']['all_data'],true);
            $k = 0;
            foreach($bestTest['Test']['TestSection'] as $tst){
               
                $graphData[$k][$bestTest['Student']['name']] = 0;
                $graphData[$k]['section'] = $tst['title'];
                $graphData[$k][$topThreeInSameTest['0']['Student']['name']] = 0;
                $graphData[$k]['section'] = $tst['title'];
                $graphData[$k]['loginStuName'] = $bestTest['Student']['name'];
                $graphData[$k]['topperStuName'] = $topThreeInSameTest['0']['Student']['name'];
                for($i = 1; $i < $bestTest['StudentAnswer']['total_question']; $i++){
                    if($TopperallData[$i]['sectionId'] == $tst['id'] && !empty($TopperallData[$i]['user_ans'])){
                        if(base64_decode($TopperallData[$i]['check']) == $TopperallData[$i]['user_ans'])
                           $graphData[$k][$topThreeInSameTest['0']['Student']['name']] += 2; 
                        
                    }
                    if($stuAllData[$i]['sectionId'] == $tst['id'] && !empty($stuAllData[$i]['user_ans'])){
                        if(base64_decode($stuAllData[$i]['check']) == $stuAllData[$i]['user_ans'])
                           $graphData[$k][$bestTest['Student']['name']] += 2; 
                        
                    }
                }
                $k++;
            }
        /*pr($bestTest);
        pr($allData);*/
        //pr($topThreeInAll);die;
        $this->set(compact('testCount','bestTest','topThreeInSameTest','topThreeInAll','graphData'));
        $studentId = $this->Session->read('Student.id');
        $this->Paginator->settings = array('ToDoList' => array('limit' => 6,'conditions'=>array('status'=>0, 'student_id' => $studentId)));
        $toDoList=$this->paginate('ToDoList');

        $count_compare = $this->Compare->find('count', array('conditions' => array('student_id' => $studentId)));

        $profile_count1 = $this->Student->find('first', array('fields' => array('profile_complete_per','current_access'),'conditions' => array('id'=>$studentId)));
        if($profile_count1['Student']['profile_complete_per'] == 0){
            $pro_count = 10;
        }
        else{
            $pro_count = 20;
        }
        $profile_count2 = $this->StudentAcademic->findByStudentId($studentId);
        if(!empty($profile_count2)){
            $pro_count = $pro_count+20;
        }
        $profile_count3 = $this->StudentMeta->findByStudentId($studentId);
        if(!empty($profile_count3)){
            $pro_count = $pro_count+20;
        }
        $profile_count4 = $this->StudentExperience->findByStudentId($studentId);
        if(!empty($profile_count4)){
            $pro_count = $pro_count+20;
        }
        $profile_count5 = $this->StudentAward->findByStudentId($studentId);
        if(!empty($profile_count5)){
            $pro_count = $pro_count+20;
        }
        $this->set(compact('toDoList','count_compare','pro_count','profile_count1'));
    }

    public function checkClgLogin(){
        $data['username'] = $this->request->data['username'];

        $data['password'] = md5($this->request->data['password']);
        $result = $this->College->find('first',array('conditions'=>array('unique_id'=>$data['username'],'password'=>$data['password'])));
        $resp = [];
        if(count($result)>0){
            if($result['College']['activation'] == '0'){
                $resp['status'] ='false';
                $resp['msg'] = 'Please activate your account!!!';
            }else if($result['College']['status'] == '0'){
                $resp['status'] ='false';
                $resp['msg'] = 'Account disabled by admin.Please contact Admin!!!';
            }else{
               $resp['status'] ='true'; 

            }
        }else{
            $resp['status'] = 'false';
            $resp['msg'] = 'Invalid login credentials!!!';

        }
        echo json_encode($resp);die;

    }
    public function checkLogin(){
        $data['email'] = $this->request->data['username'];

        $data['password'] = md5($this->request->data['password']);
        $result = $this->Student->find('first',array('conditions'=>array('OR'=>array(array('email'=>$data['email']),array('mobile'=>$data['email'])),'password'=>$data['password'])));
        $resp = [];
        if(count($result)>0){
            if($result['Student']['activation_status'] == '0'){
                $resp['status'] ='false';
                $resp['msg'] = 'Please activate your account!!!';
            }else if($result['Student']['status'] == '0'){
                $resp['status'] ='false';
                $resp['msg'] = 'Your account is disabled by admin.Please contact Admin!!!';
            }else{
               $resp['status'] ='true'; 

            }
        }else{
            $resp['status'] = 'false';
            $resp['msg'] = 'Invalid login credentials!!!';

        }
        echo json_encode($resp);die;
    }
    public function logout() 
    {
        $this->layout="";
        //$result = $this->Admin->findById($this->Session->read('Admin.id'));
        $result['Admin']['last_ip'] = $_SERVER['REMOTE_ADDR'];
        $result['Admin']['last _access']  = $result['Admin']['current_access'];
        $result['Admin']['online']  = '0';
        //$this->Admin->save($result);
        $this->Session->destroy();
        $this->redirect(array('controller'=>'Homes','action' => 'index'));
        
    }

    public function studentSignup(){
        $this->loadModel('ReferenceList');
        $this->loadModel('Wallet');
        $this->loadModel('StudentPlan');
        $this->loadModel('WalletTransaction');
        if($this->request->is('post') && !empty($this->request->data)){
            $data['Student'] = $this->request->data;
            $pass = $data['Student']['password'];
            $data['Student']['password'] = md5($data['Student']['password']);
            
            $refernce_code = $data['Student']['reference_code'];
            $data['Student']['dob'] = date('Y-m-d', strtotime($data['Student']['dob']));
            unset($data['Student']['reference_code']);
            //pr($data);die;
            $data['Student']['status'] = 1;
            $data['Student']['activation_status'] = 1;
            if($this->Student->save($data)){
                $studentId = $this->Student->getLastInsertId();
                /*if(!empty($refernce_code)){
                    $refernceData = $this->ReferenceList->find('first',array('conditions'=>array('OR'=>array(array('ReferenceList.referre_email' => $data['Student']['mobile'] ),array('ReferenceList.referre_email' => $data['Student']['email'])),'ReferenceList.reference_code'=>$refernce_code,'ReferenceList.is_redeemed'=>'0'))); 
                    
                    if(!empty($refernceData)){
                        //refferre wallet update start
                        $referre_wallet['Wallet']['student_id'] = $studentId;
                        $referre_wallet['Wallet']['amount'] = $refernceData['ReferenceList']['referre_amount'];
                        
                        $this->Wallet->create();
                        $this->Wallet->save($referre_wallet);
                        $walletId = $this->Wallet->getLastInsertId();

                        $walletTcn['WalletTransaction']['wallet_id'] = $walletId;
                        $walletTcn['WalletTransaction']['transaction_by'] = 'Automated';
                        $walletTcn['WalletTransaction']['description'] = 'Received referral amount because of referral code.';
                        $walletTcn['WalletTransaction']['type'] = '0';
                        $walletTcn['WalletTransaction']['due_to'] = 'Refere A Friend';
                        $walletTcn['WalletTransaction']['created_date'] = date('Y-m-d H:i:s');
                        $walletTcn['WalletTransaction']['student_id'] = $studentId;
                        $this->WalletTransaction->create();
                        $this->WalletTransaction->save($walletTcn);

                        //refferre wallet update ends

                        //refferrer wallet update starts
                        $referrer_amt = $this->Wallet->findByStudentId($refernceData['ReferenceList']['referrer_id']);
                        //pr($referrer_amt);
                        $referrer_wallet['Wallet']['student_id'] = $refernceData['ReferenceList']['referrer_id'];
                        $referrer_wallet['Wallet']['id'] = @$referrer_amt['Wallet']['id'];
                        $referrer_wallet['Wallet']['amount'] = @$referrer_amt['Wallet']['amount']+$refernceData['ReferenceList']['referrer_amount'];
                        
                        $this->Wallet->save($referrer_wallet);
                        $walletId1 = $this->Wallet->getLastInsertId();
                        $walletTcn1['WalletTransaction']['wallet_id'] = $walletId1;
                        $walletTcn1['WalletTransaction']['transaction_by'] = 'Automated';
                        $walletTcn1['WalletTransaction']['description'] = 'Received referral amount from '.$data['Student']['name'].'<br>'.'Mail Id:'.$data['Student']['email'].'<br>'.'Contact No. : '.$data['Student']['mobile'];
                        $walletTcn1['WalletTransaction']['type'] = '0';
                        $walletTcn1['WalletTransaction']['due_to'] = 'Refere A Friend';
                        $walletTcn1['WalletTransaction']['created_date'] = date('Y-m-d H:i:s');
                        $walletTcn1['WalletTransaction']['student_id'] = $refernceData['ReferenceList']['referrer_id'];
                        $this->WalletTransaction->create();
                        $this->WalletTransaction->save($walletTcn1);

                        $this->ReferenceList->id = $refernceData['ReferenceList']['id'];
                        $this->ReferenceList->saveField('is_redeemed','1');
                        //refferrer wallet update ends
                    }else{
                        $referre_wallet['Wallet']['student_id'] = $studentId;
                        $referre_wallet['Wallet']['amount'] = 0;
                        
                        $this->Wallet->create();
                        $this->Wallet->save($referre_wallet);
                    }    
                //pr($wallet);die;        
                
                } */  
                if(!empty($data['Student']['email'])){
                    $link = HTTP_ROOT.'Homes/activateStudent/'.base64_encode($studentId);
                    $replace = array('{name}','{email}','{password}','{link}');
                    $with = array($data['Student']['name'],$data['Student']['email'],$pass,$link);
                    $this->send_email('',$replace,$with,'confirm_registration',INFO_FORMSADDA,$data['Student']['email']);
                }
                

                //$this->Session->write('membership_modal',$studentId);
                $this->Session->write('signup_modal','open');
                $this->redirect(array('action' =>'index'));
                
            }else{
                echo 'Error in saving data,Please try again.';die;
            }  
        }
    }

    public function activateStudent($studentId =null){
        $stuId = base64_decode($studentId);
        $this->Student->id = $stuId;
        $this->Student->saveField('activation_status','1');
        $this->Student->saveField('status','1');
        $this->Session->write('activation_modal','open');
        $this->redirect(array('action' =>'index'));

    }
    public function activateCollege($collegeId =null){
        $clgId = base64_decode($collegeId);
        $this->College->id = $clgId;
        $this->College->saveField('activation','1');
        $this->College->saveField('status','1');
        $this->Session->write('activation_modal','open');
        $this->redirect(array('action' =>'index'));

    }

    public function collegeSignup(){
        
        if($this->request->is('post') && !empty($this->request->data)){
            $data['College'] = $this->request->data;
           // pr($data);die;
            $pass = $data['College']['password'];
            $data['College']['password'] = md5($data['College']['password']);
            if($this->College->save($data)){
                $collegeId = $this->College->getLastInsertId();
                $len = strlen($collegeId);
                if($len == 1)
                    $uniqueId = mt_rand(1000000, 9999999);
                else if($len == 2)
                    $uniqueId = mt_rand(100000, 999999);
                else if($len == 3)
                    $uniqueId = mt_rand(10000, 99999);
                else
                    $uniqueId = mt_rand(1000, 999);
                $usename = $uniqueId.$collegeId;
                //pr($uniqueId);die;
                $this->College->id = $collegeId;
                $this->College->saveField('unique_id', $usename);
               /* $Email = new CakeEmail();
                $Email->from(array('info@Formsadda.com' => 'Formsadda'));
                $Email->to($data['College']['email']);
                $Email->subject('FormsAdda New Registration');
                $Email->send('Your User Id is '.$usename.'and  password is'.$data['College']['password']);*/
                $link = HTTP_ROOT.'Homes/activateCollege/'.base64_encode($collegeId);
                $replace = array('{name}','{username}','{password}','{link}');
                $with = array($data['College']['name'],$usename,$pass,$link);
                $this->send_email('',$replace,$with,'clg_registration',INFO_FORMSADDA,$data['College']['email']);
                $this->Session->write('signup_modal','open');
                $this->redirect(array('action' =>'index'));

            }else{
                echo 'Error in saving data,Please try again.';die;
            }  
        }
    }

    public function search(){

        //Configure::write('debug', 0);
        //pr($this->request->query);die;
        //echo $this->Session->read('selectedCourse');
        $this->loadModel('Exam');
        $this->loadModel('CourseExam');
        $exams = $this->Exam->find('all',array('conditions'=>array('status'=>'1')));
        $states = $this->State->find('all',array('conditions'=>array('status'=>'1')));
        $courses = $this->Course->find('all',array('conditions'=>array('status'=>'1')));
        $facilities = $this->Facility->find('all',array('conditions'=>array('status'=>'1')));
        //pr($courses);
        //pr( $facilities);die;
        
        $conditions = [];
        $conditions[] = array('status' =>'1');
        $getData = $this->request->query;
        //pr($getData);
        if(!empty($getData['location'])){
            if($getData['location'] == 'Delhi'){
                $conditions[] = array('College.state_id'=>9);
                $this->Session->write('selectedLocation',9);
            }
            else if($getData['location'] == 'Chennai'){
                $conditions[] = array('College.city_id'=>817);
                $this->Session->write('selectedLocation',817);
            }
            else if($getData['location'] == 'Mumbai'){
                $conditions[] = array('College.city_id'=>596);
                $this->Session->write('selectedLocation',596);
            }
            else if($getData['location'] == 'Kolkata'){
                $conditions[] = array('College.city_id'=>145);
                $this->Session->write('selectedLocation',145);
            }
            else if($getData['location'] == 'Pune'){   
                $conditions[] = array('College.city_id'=>643);
                $this->Session->write('selectedLocation',643);
            }  
            $loc_id = $this->Session->read('selectedLocation');
            //pr($loc_id);
        }
        if(!empty($getData['spec'])){
            $this->loadModel('CourseSpecialization');
            $spec = explode('-', $getData['spec']);
            $spec_cnt = count($spec);
            $spec_id = $spec[$spec_cnt-1];
            $ids = $this->CourseSpecialization->find('list',array('conditions'=>array('specialization_id'=>$spec_id),'fields'=>'college_id'));
            $conditions[] = array('College.id'=>array_unique(@$ids));
             //pr($ids);

             $crs = explode('-', $getData['course']);
                $crs_cnt = count($crs);
                $crs_id = $crs[$crs_cnt-1];
              $this->Session->write('selectedCourse',$crs_id);
              
        }else if(!empty($getData['course'])){
                $crs = explode('-', $getData['course']);
                $crs_cnt = count($crs);
                $crs_id = $crs[$crs_cnt-1];
                $ids = $this->CollegeCourse->find('list',array('conditions'=>array('course_id'=>$crs_id),'fields'=>'college_id'));
                $conditions[] = array('College.id'=>array_unique(@$ids));
                $this->Session->write('selectedCourse',$crs_id);
                //pr($ids);
                
        }else if(!empty($getData['prgm'])){
                    $prgm = explode('-', $getData['prgm']);
                    $prgm_cnt = count($prgm);
                    $prgm_id = $prgm[$prgm_cnt-1];
                    $ids = $this->CollegeCourse->find('list',array('conditions'=>array('program_id'=>$prgm_id),'fields'=>'college_id'));
                    $conditions[] = array('College.id'=>array_unique(@$ids));
                    //pr($ids);
        }
        if(!empty($getData['q'])){
            $conditions[] = array('OR' => array( array('College.name Like'=>'%'.$getData['q'].'%'), array('College.short_name Like'=>'%'.$getData['q'].'%'), array('College.address Like'=>'%'.$getData['q'].'%') ) );
        }

        //pr($ids);pr($conditions);
        $this->College->bindModel(
            array('hasOne' => array(
                    'CollegeMeta' => array(
                        'className' => 'CollegeMeta',
                        'foreignKey' => 'college_id',
                        'fields' => array('college_type')
                        )
                    )
                )
            );
        $this->College->bindModel(
        array('hasMany' => array(
                'CollegeFacility' => array(
                    'className' => 'CollegeFacility',
                    'foreignKey' => 'college_id',
                    'fields' => array('facility_id'),
                    'limit'=>8
                    )
                )
            )
        );
        $this->CollegeFacility->bindModel(
        array('belongsTo' => array(
                'Facility' => array(
                    'className' => 'Facility',
                    'foreignKey' => 'facility_id',
                    'fields' => array('icon','name')
                    )
                )
            )
        );
        $this->College->bindModel(
        array('hasMany' => array(
                'CollegeCourse' => array(
                    'className' => 'CollegeCourse',
                    'foreignKey' => 'college_id',
                    'fields' => array('course_id','fees','brochure','id','to_date','from_date','registration_fee','formsadda_cost'),
                    'conditions'=>array('CollegeCourse.course_id'=>@$this->Session->read('selectedCourse'))
                    )
                )
            )
        );
        $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Course' => array(
                    'className' => 'Course',
                    'foreignKey' => 'course_id',
                    'fields' => array('course')
                    )
                )
            )
        );
        $this->CollegeCourse->bindModel(
            array('hasMany' => array(
                    'CourseExam' => array(
                        'className' => 'CourseExam',
                        'foreignKey' => 'college_course_id'
                        )
                    )
                )
            );
         $this->CourseExam->bindModel(
            array('belongsTo' => array(
                    'Exam' => array(
                        'className' => 'Exam',
                        'foreignKey' => 'exam_id',
                        'fields' => array('exam')
                        )
                    )
                )
            );
        $this->College->bindModel(
        array('hasMany' => array(
                'Review' => array(
                    'className' => 'Review',
                    'foreignKey' => 'college_id',
                    'conditions'=>array('Review.status'=>'1')

                    )
                )
            )
        );
        
          // echo $this->Session->read('selectedCourse');die;
        //pr($conditions);
        $this->College->recursive=4;
        $colleges = $this->College->find('all',array('limit'=>3,'conditions'=>$conditions));
        $this->set(compact('colleges','states','facilities','courses','exams','loc_id'));
        //pr($colleges);die;
    }

    public function searchPagination(){
        Configure::write('debug', 0);
        $this->loadModel('CourseExam');
        if($this->request->is('post')){
           
            $data = $this->request->data;
            //pr($data);
            $conditions = [];
            $conditions[] = array('College.status'=>'1');
            if(!empty($data['facility'])){
                $ids = $this->CollegeFacility->find('list',array('conditions'=>array('facility_id' => $data['facility']),'fields' => 'college_id'));
                $conditions[] = array('College.id'=>$ids);
                
            }
            if(!empty($data['exams'])){
                $ids = $this->CourseExam->find('list',array('conditions'=>array('exam_id' => $data['exams']),'fields' => 'college_id'));
                $conditions[] = array('College.id'=>$ids);
                
            }
            if(!empty($data['course'])){
                $allCourseId = $data['course'];
                /*$ids = $this->CollegeCourse->find('list',array('conditions'=>array('course_id' => $data['course']),'fields' => 'college_id'));
                $conditions[] = array('College.id'=>$ids);*/
                if($data['minfee']){
                    $ids = $this->CollegeCourse->find('list',array('conditions'=>array('course_id'=>$data['course'],'fees >'=>$data['minfee'],'fees <'=>$data['maxfee']),'fields'=>'college_id'));
                    $conditions[] = array('College.id'=>$ids);
                    
                }
                if($data['maxfee']){
                    $ids = $this->CollegeCourse->find('list',array('conditions'=>array('course_id'=>$data['course'],'fees >'=>$data['minfee'],'fees <'=>$data['maxfee']),'fields'=>'college_id'));
                    $conditions[] = array('College.id'=>$ids);
                    
                }
                //pr($ids);die;
                if(count($data['course'] == 1)){
                    $crs_ids = $data['course'];
                    $this->Session->write('selectedCourse',$crs_ids['0']);
                }

                
            }
            if(!empty($data['college_type'])){
                
                $conditions[] = array('CollegeMeta.college_type'=>$data['college_type']);
            }   
            if(!empty($data['location'])){
                
                $conditions[] = array('College.state_id'=>$data['location']);
            }   
            //pr($conditions);die;
            $this->College->bindModel(
            array('hasOne' => array(
                    'CollegeMeta' => array(
                        'className' => 'CollegeMeta',
                        'foreignKey' => 'college_id',
                        'fields' => array('college_type')
                        )
                    )
                )
            );
            $this->College->bindModel(
            array('hasMany' => array(
                    'CollegeFacility' => array(
                        'className' => 'CollegeFacility',
                        'foreignKey' => 'college_id',
                        'fields' => array('facility_id'),
                        'limit'=>8
                        )
                    )
                )
            );
            $this->CollegeFacility->bindModel(
            array('belongsTo' => array(
                    'Facility' => array(
                        'className' => 'Facility',
                        'foreignKey' => 'facility_id',
                        'fields' => array('icon','name')
                        )
                    )
                )
            );
            $this->College->bindModel(
            array('hasMany' => array(
                    'CollegeCourse' => array(
                        'className' => 'CollegeCourse',
                        'foreignKey' => 'college_id',
                        'fields' => array('course_id','fees','brochure','id','to_date','from_date','registration_fee','formsadda_cost'),
                        'conditions'=>array('CollegeCourse.course_id'=>@$data['course'])

                        )
                    )
                )
            );
            $this->CollegeCourse->bindModel(
            array('hasMany' => array(
                    'CourseExam' => array(
                        'className' => 'CourseExam',
                        'foreignKey' => 'college_course_id',
                        'fields' => array('exam_id'),

                        )
                    )
                )
            );
            $this->CourseExam->bindModel(
            array('belongsTo' => array(
                    'Exam' => array(
                        'className' => 'Exam',
                        'foreignKey' => 'exam_id',
                        'fields' => array('exam')
                        )
                    )
                )
            );
            $this->CollegeCourse->bindModel(
            array('belongsTo' => array(
                    'Course' => array(
                        'className' => 'Course',
                        'foreignKey' => 'course_id',
                        'fields' => array('course')
                        )
                    )
                )
            );
            $this->College->bindModel(
            array('hasMany' => array(
                    'Review' => array(
                        'className' => 'Review',
                        'foreignKey' => 'college_id',
                        'conditions'=>array('Review.status'=>'1')

                        )
                    )
                )
            );
            $this->College->recursive=3;
            $this->Course->unbindModel(array('hasMany'=>array('Specialization')));
            $colleges = $this->College->find(
                                                'all',
                                                array(
                                                        'limit' => 3,
                                                        'offset' => @$data['offset'],
                                                        'conditions' => $conditions
                                                        )
                                            );
            
            
            //pr($colleges);die;
            $allCourseId = array_unique($allCourseId);
            $this->set(compact('colleges','crs_ids','allCourseId'));
        }    
    }

    public function getGallery(){

        if($this->request->is('post')){

            $this->CollegeGallery->bindModel(
            array('belongsTo' => array(
                    'Gallery' => array(
                        'className' => 'Gallery',
                        'foreignKey' => 'gallery_id'
                        )
                    )
                )
            );
            $data = $this->request->data;
            $galleries = $this->CollegeGallery->findAllByCollegeId($data['collegeId']);
            $resp= [];
            if(!empty($galleries)){
                $resp['data'] = $galleries;
                $resp['status'] = true;
            }else{
                $resp['status'] = false;
            }
            echo json_encode($resp);die;

        }
    }

    public function college($collegeName = null,$courseId = null){
        //$this->Session->delete('selectedCourse');
        //pr($this->Session->read());
        $getcollegeName = explode('-', $collegeName);
        $collegeId = $getcollegeName[count($getcollegeName) - 1];
        $this->College->bindModel(
            array('belongsTo' => array(
                    'City' => array(
                        'className' => 'City',
                        'foreignKey' => 'city_id'                        
                        )
                    )
                )
            );
        //echo $courseId;die;
        if(isset($courseId)){
             //echo $courseId;
             //$_SESSION['selectedCourse'] = $courseId;
             //$this->Session->write('selectedCourse',$courseId);
            
             
        }
        //echo $this->Session->read('selectedCourse');die;
        if(!empty($this->Session->read('selectedLocation'))){
            $suggestions = $this->College->find('all',array('conditions'=>array('conditions'=>array('state_id'=>$this->Session->read('selectedLocation'),'College.id !='=>$collegeId),'limit'=>5)));
        }else if($this->Session->check('selectedCourse')){
            $clgIds = $this->CollegeCourse->find('list',array('fields'=>'college_id','conditions'=>array('course_id'=>$this->Session->read('selectedCourse'))));
            
            $suggestions = $this->College->find('all',array('conditions'=>array('College.id'=>$clgIds,'College.id !='=>$collegeId),'limit'=>5));
        }else{
            $suggestions = $this->College->find('all',array('conditions'=>array('College.id !='=>$collegeId),'limit'=>5));
        }
        //pr($suggestions);die;
        $this->College->bindModel(
            array('hasOne' => array(
                    'CollegeMeta' => array(
                        'className' => 'CollegeMeta',
                        'foreignKey' => 'college_id'
                        )
                    )
                )
            );
        $this->College->bindModel(
            array('hasOne' => array(
                    'CollegeNear' => array(
                        'className' => 'CollegeNear',
                        'foreignKey' => 'college_id'
                        )
                    )
                )
            );
        $this->College->bindModel(
            array('belongsTo' => array(
                    'City' => array(
                        'className' => 'City',
                        'foreignKey' => 'city_id'                        
                        )
                    )
                )
            );
        $this->College->bindModel(
            array('belongsTo' => array(
                    'State' => array(
                        'className' => 'State',
                        'foreignKey' => 'state_id'                        
                        )
                    )
                )
            );
        $this->College->bindModel(
            array('belongsTo' => array(
                    'Country' => array(
                        'className' => 'Country',
                        'foreignKey' => 'country_id'                        
                        )
                    )
                )
            );
        $this->College->bindModel(
        array('hasMany' => array(
                'CollegeFacility' => array(
                    'className' => 'CollegeFacility',
                    'foreignKey' => 'college_id',
                    'fields' => array('facility_id')
                    )
                )
            )
        );
        $this->CollegeFacility->bindModel(
        array('belongsTo' => array(
                'Facility' => array(
                    'className' => 'Facility',
                    'foreignKey' => 'facility_id',
                    'fields' => array('icon','name')
                    )
                )
            )
        );
        $this->College->bindModel(
        array('hasMany' => array(
                'CollegeGallery' => array(
                    'className' => 'CollegeGallery',
                    'foreignKey' => 'college_id'
                    
                    )
                )
            )
        );
        $this->College->bindModel(
        array('hasMany' => array(
                'Review' => array(
                    'className' => 'Review',
                    'foreignKey' => 'college_id',
                    'conditions'=>array('Review.status'=>'1'),
                    'order' => 'id desc'
                    )
                )
            )
        );
        $this->Review->bindModel(
        array('hasOne' => array(
                'ReviewCount' => array(
                    'className' => 'ReviewCount',
                    'foreignKey' => 'review_id'
                    )
                )
            )
        );
        $this->College->bindModel(
        array('hasMany' => array(
                'CollegeCourse' => array(
                    'className' => 'CollegeCourse',
                    'foreignKey' => 'college_id'
                    
                    )
                )
            )
        );
       /* $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Stream' => array(
                    'className' => 'Stream',
                    'foreignKey' => 'stream_id'
                    
                    )
                )
            )
        );*/
        $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Program' => array(
                    'className' => 'Program',
                    'foreignKey' => 'program_id'
                    
                    )
                )
            )
        );
        $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Course' => array(
                    'className' => 'Course',
                    'foreignKey' => 'course_id'
                    
                    )
                )
            )
        );
        $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Specialization' => array(
                    'className' => 'Specialization',
                    'foreignKey' => 'specialization_id',
                    //'fields' => 'id'
                    )
                )
            )
        );
        /*$this->CollegeGallery->bindModel(
            array('belongsTo' => array(
                    'Gallery' => array(
                        'className' => 'Gallery',
                        'foreignKey' => 'gallery_id'
                        )
                    )
                )
            );*/

        $this->College->recursive=3;
        $college = $this->College->findById($collegeId);
        $stream_ids = array_unique(Hash::extract($college, 'CollegeCourse.{n}.stream_id'));
        // pr($college);die;
        $streams = $this->Stream->find('all',array('conditions'=>array('Stream.id' => $stream_ids)));

        //pr($streams);die;
        $courses = $this->Course->find('all',array('conditions'=>array('status'=>'1')));
        $this->set(compact('college','streams','suggestions','courses'));
        //pr($this->Session->read());//die;
        
        
    }

    public function collegeContact(){
        if($this->request->is('post')){
            $data['CollegeContact'] = $this->request->data;
            $resp = [];
            if($this->CollegeContact->save($data)){
                $resp['status'] = "true";
                $resp['msg'] = 'College will contact you soon.';
            }else{
                $resp['status'] = "false";
                $resp['msg'] = 'Something went wrong,Please try again.';
            }
            echo json_encode($resp);die;
            
        }
    }

    public function collegeReview(){
       // echo "hi";die;
        $this->loadModel('Review');
        if($this->request->is('post')){
            $resp = [];
            if(!$this->Session->check('Student')){
                $resp['status'] = "false";
                $resp['msg'] = 'Please login to rate this college.';
                echo json_encode($resp);
                die;
            }
            $data = $this->request->data;
            //pr($data);die;

            $data['student_id'] = $this->Session->read('Student.id');
            $check = $this->Review->find('first',array('conditions'=>array('college_id'=>$data['college_id'],'student_id'=>$this->Session->read('Student.id'))));
            
            if(!empty($check)){
                $resp['status'] = "false";
                $resp['msg'] = 'You already provided your reivew.';
                echo json_encode($resp);
                die;
            }
            
            $rateCnt = $this->Review->find('all',array('conditions'=>array('college_id'=>$data['college_id']),'fields'=>array("avg(rating) As avg_rating",'count(id) as count')));
           
            $avg_rate =  (round($rateCnt['0']['0']['avg_rating']*$rateCnt['0']['0']['count'])+$data['rating'])/($rateCnt['0']['0']['count']+1);
            $this->College->id = $data['college_id'];
            $this->College->saveField('avg_rating',$avg_rate);

            if($this->Review->save($data)){
                
                $avg = 
                $resp['status'] = "true";
                $resp['msg'] = 'College rated successfully.';
            }else{
                $resp['status'] = "false";
                $resp['msg'] = 'Something went wrong,Please try again.';
            }
            echo json_encode($resp);die;
            
        }
    }
    public function addToCompare(){
        if($this->request->is('post')){
            $data = $this->request->data;
            $collegeId = $data['collegeId'];
            $resp =[];
             $cmp = [];
            // $this->Session->delete('clgCompare');
            if($this->Session->check('clgCompare')){
                 $cmp = $this->Session->read('clgCompare');
                 //pr($cmp);die;
            }
            if(count($cmp) >= 4){
                $resp['status'] = "false";
                $resp['msg'] = 'You can only add maximum 4 colleges to compare.';
                echo json_encode($resp);die;
            }
            //pr($cmp);die;
            if(in_array($collegeId , $cmp)){
                $resp['status'] = "false";
                $resp['msg'] = 'College already added to compare.'; 
                echo json_encode($resp);die;
            }else{
                $cmp[] = $collegeId;

                $this->Session->write('clgCompare',$cmp);
                $colg = $this->College->findById($collegeId,array('image','name','id'));
                $resp['name'] = $colg['College']['name'];
                $resp['id'] = $collegeId;
                $resp['image'] = $colg['College']['image'];
                $resp['count'] = count($this->Session->read('clgCompare'));
                $resp['status'] = "true";
                $resp['msg'] = 'College Added to compare list.';
                //$resp['msg'] = 'College already added to compare.'; 

            }
            
            echo json_encode($resp);die;
        }
    }

    public function getStateByName(){
        if($this->request->is('post')){
            //pr($this->request->data);die;
            $states = $this->State->find('all',array('conditions'=>array('statename Like' => '%'.$this->request->data['key'].'%')));
            $resp =[];
            if(!empty($states)){
                $resp['status'] = 'true';
                $resp['data'] = $states;
                echo json_encode($resp);die;
            }else{
                $resp['status'] = 'false';
                echo json_encode($resp);die;
            }
        }
    }

    public function refereAFriend(){
        $this->loadModel('ReferenceAmount');
        $this->loadModel('ReferenceList');
        if($this->request->is('post')){
            $post = $this->data;
            //pr($post);die;
            $resp= [];
            if(empty($post['email']) && empty($post['phone'])){
                $resp['status'] = 'false';
                $resp['msg'] = 'Please enter email or mobile no.';
                echo json_encode($resp);die;
            }
            if(empty($this->Session->read('Student.id'))){
               
                $resp['status'] = 'false';
                $resp['msg'] = 'Please login as student.';
                echo json_encode($resp);die;
            }
            $amount = $this->ReferenceAmount->find('first',array('conditions'=>array('status'=>'1','from_date <=' => date('Y-m-d'),'to_date >=' => date('Y-m-d'))));
            $data['ReferenceList']['referre_email'] = $post['email'] == '' ? $post['phone'] : $post['email'];
            $data['ReferenceList']['referrer_id'] = $this->Session->read('Student.id');
            $student = $this->Student->findById($data['ReferenceList']['referrer_id']);
            $data['ReferenceList']['referrer_amount'] = $amount['ReferenceAmount']['referrer_amount'];
            $data['ReferenceList']['referre_amount'] = $amount['ReferenceAmount']['referre_amount'];
            $data['ReferenceList']['reference_amount_id'] = $amount['ReferenceAmount']['id'];
            $data['ReferenceList']['reference_code'] = $this->RandomStringGenerator(8);
            if($this->ReferenceList->save($data)){
                $replace = array('{name}','{amount}');
                $with = array('asdas',$amount['ReferenceAmount']['referre_amount']);
                $this->send_email('',$replace,$with,'Reference_referre',INFO_FORMSADDA,$post['email'] );
                $replace = array('{name}','{amount}');
                $with = array('asdas',$amount['ReferenceAmount']['referrer_amount']);
                $this->send_email('',$replace,$with,'Reference_referrer',INFO_FORMSADDA,$student['Student']['email']);
                
                $resp['status'] = 'true';
                $resp['msg'] = 'Refere a friend done';
                echo json_encode($resp);die;

            }
            $resp['status'] = 'true';
            $resp['msg'] = 'Server error,try again';
            echo json_encode($resp);die;
        }
    }

    public function addMember($studentId = null){

        $this->loadModel("Country");
        $this->loadModel("StudentAcademic");
        $this->loadModel("StudentMeta");
        $this->loadModel("StudentExperience");
        $this->loadModel("StudentAward");
        $this->loadModel("StudentDocs");

        $country = $this->Country->find('all');
        if(!empty($studentId)){
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentAcademic' => array(
                            'className' => 'StudentAcademic',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );

            $this->Student->bindModel(
                array('hasOne' => array(
                        'StudentMeta' => array(
                            'className' => 'StudentMeta',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentExperience' => array(
                            'className' => 'StudentExperience',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentAward' => array(
                            'className' => 'StudentAward',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentDocs' => array(
                            'className' => 'StudentDocs',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $studentId = base64_decode($studentId);
            $editStudent = $this->Student->findById($studentId);
            $states = $this->State->findAllByCountryId($editStudent['Student']['country_id']);
            $cities = $this->City->findAllByStateId($editStudent['Student']['state_id']);
            //pr($editStudent);
            //die;
        }
        $this->set(compact('country','editStudent','states','cities'));
        if($this->request->is('post') && !empty($this->data))
        {
            $data['Student'] = $this->data;
            /*pr($_FILES);
            pr($data);die;*/
            //$data['College']['password'] = md5($this->data['College']['password']);
            if (!empty($_FILES['image']['tmp_name'])) 
            {
                $destination = realpath('../webroot/img/studentImages/small/') . '/';
                $destination1 = realpath('../webroot/img/studentImages/large/') . '/';
                $image = $this->Components->load('Resize');  

                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination1 . $filenameimg);
                    $data['Student']['image']=$filenameimg;
                    
                    
                }
            }else{
                $data['Student']['image']='adminDefault.png';
            }
            /*$formdata = $this->Form->find('list',array('conditions'=>array('college_id'=>'0','fields'=>'id')));
            $this->UserImage->saveAll($formdata);*/
            if($this->Student->save($data)){
                $resp['studentId'] = $this->Student->getLastInsertId();
                if(!empty($data['Student']['id'])){
                    $resp['studentId'] = $data['Student']['id'];
                }
                /*$uniqueId = uniqid($resp['studentId']);
                $this->College->id = $resp['studentId'];
                $this->College->saveField('unique_id', $uniqueId);
                $Email = new CakeEmail();
                $Email->from(array('info@Formsadda.com' => 'Formsadda'));
                $Email->to($data['College']['email']);
                $Email->subject('FormsAdda New Registration');
                $Email->send('Your User Id is '.$uniqueId.'and  password is'.$data['College']['password']);*/
                $resp['status'] = "true";
            }
            else{            
                $resp['status'] = "false";
            }
            echo json_encode($resp);die;
        }
    }

    public function removeCompare($compareId){
        $cmp = $this->Session->read('clgCompare');
        $key = array_search($compareId, $cmp);
        unset($cmp[$key]);
        $this->Session->write('clgCompare',$cmp);

        //pr($this->Session->read('clgCompare'));
        //die;
        $this->redirect($this->referer());
    }

    public function memberList(){
       $members = $this->Student->findAllByParentId($this->Session->read('Student.id'));
        $this->set(compact('members')); 
        //pr($members);die;
    }

    public function deleteMember($userid){
        echo $userId;die;
    }
    public function deleteData($model =null,$id=null){
        $this->loadModel($model);
        $this->$model->delete($id);
        if($this->request->is('ajax'))
            die;
        else
            $this->redirect($this->referer());
    }

    public function updateStudentImage(){
       if (!empty($_FILES['image']['tmp_name'])) 
            {
                $destination = realpath('../webroot/img/studentImages/small/') . '/';
                $destination1 = realpath('../webroot/img/studentImages/large/') . '/';
                $image = $this->Components->load('Resize');  

                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination1 . $filenameimg);
                    $data['Student']['image']=$filenameimg;
                    $data['Student']['id'] = $this->Session->read('Student.id');
                    $this->Student->save($data);
                    $this->redirect($this->referer());
                    
                } 
            }    
    }

    public function updateStudentPassword(){
        if($this->request->is('post')){
            $data = $this->data;
            $pass = md5($data['password']);
            $this->Student->id = $this->Session->read('Student.id');
            $this->Student->saveField('password',$pass);
            $this->redirect($this->referer());
        }
        
    }

    public function checkPassword(){
        //pr($this->data);die;
        $check = $this->Student->find('count',array('conditions'=>array('id'=>$this->Session->read('Student.id'),'password'=>md5($this->data['current']))));
        if($check !=0)
            echo 'true';
        else
            echo 'false';
        die;
    }  

    public function DeleteDoc($id){
      $this->loadModel('StudentDocs');
      $this->StudentDocs->deleteAll(array('id'=>$id));
    }

    public function studentDelete($id){
        $this->loadModel('StudentMeta');
        $this->loadModel('StudentAward');
        $this->loadModel('StudentExperience');
        $this->loadModel('StudentDocs');
        $this->loadModel('StudentAcademic');
        $this->Student->delete($id);
        $this->StudentMeta->deleteAll(array('student_id'=>$id));
        $this->StudentAward->deleteAll(array('student_id'=>$id));
        $this->StudentExperience->deleteAll(array('student_id'=>$id));
        $this->StudentDocs->deleteAll(array('student_id'=>$id));
        $this->StudentAcademic->deleteAll(array('student_id'=>$id));
    }

    public function updateCollegeImage(){
       if (!empty($_FILES['image']['tmp_name'])) 
            {
               $destination = realpath('../webroot/img/collegeImages/small/') . '/';
                $destination1 = realpath('../webroot/img/collegeImages/large/') . '/';
                $destination2 = realpath('../webroot/img/collegeImages/thumbnail/') . '/';
                $image = $this->Components->load('Resize');    

                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    $image->resize($_FILES['image']['tmp_name'],$destination2.$filenameimg,'aspect_fill',500,300,0,0,0,0);
                    //$image->resize($_FILES['image']['tmp_name'],$destination1.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination1 . $filenameimg);
                    $data['College']['image']=$filenameimg;
                    $data['College']['id'] = $this->Session->read('College.id');
                    //pr($data);die;
                    $this->College->save($data);
                    $this->redirect($this->referer());
                    
                } 
            }    
    }

    public function updateCollegePassword(){
        if($this->request->is('post')){
            $data = $this->data;
            $pass = md5($data['password']);
            $this->College->id = $this->Session->read('College.id');
            $this->College->saveField('password',$pass);
            $this->redirect($this->referer());
        }
        
    }

    public function checkCollegePassword(){
        //pr($this->data);die;
        $check = $this->College->find('count',array('conditions'=>array('id'=>$this->Session->read('College.id'),'password'=>md5($this->data['current']))));
        if($check !=0)
            echo 'true';
        else
            echo 'false';
        die;
    }

    public function editCollege($id=null)
    {
        $this->loadModel('CollegeNear');
        $this->loadModel('CollegeField');
        $this->loadModel('CollegeFieldMeta');
        $id = $this->Session->read('College.id');
        $collegemasters = $this->CollegeMaster->find('all');
        $this->loadModel("Country");
        $country = $this->Country->find('all');
        $this->College->bindModel(
        array('belongsTo' => array(
                'State' => array(
                    'className' => 'State',
                    'foreignKey' => 'state_id'
                    )
                )
            )
        );
        $this->College->bindModel(
        array('belongsTo' => array(
                'City' => array(
                    'className' => 'City',
                    'foreignKey' => 'city_id'
                    )
                )
            )
        );
        $college = $this->College->findById($id);
        //pr($college);die;
        $facilities = $this->Facility->find('all');
        $collegefacilities = $this->CollegeFacility->findAllByCollegeId($id);
        $collegeId = $id;

        $CollegeNear = $this->CollegeNear->findByCollegeId($collegeId);
        $facility = $this->Facility->findById($id);
        $collegeMeta = $this->CollegeMeta->findByCollegeId($collegeId);
        $fields = $this->CollegeField->find('all', array('conditions' => array('status' => 1, 'is_delete' => 0)));
        $collegeFieldMeta = $this->CollegeFieldMeta->find('list', array('fields' => array('field_id'), 'conditions' => array('college_id' => $collegeId)));
        $this->set(compact('collegeMeta','facilities','collegefacilities'));
        $this->set(compact('facility'));
        $this->set(compact('CollegeNear','collegeId'));
        $this->set(compact('college','country','collegemasters','collegeFieldMeta','fields'));
       /* pr($this->request);
        pr($this->data);
        pr($_POST);
        pr($_GET);
        echo "hi";die;*/

        if($this->request->is('post') && !empty($this->data))
        {
            $data = $this->data;
            if (!empty($_FILES['image']['tmp_name'])) 
            {
                $destination = realpath('../webroot/img/collegeImages/small/') . '/';
                $destination1 = realpath('../webroot/img/collegeImages/large/') . '/';
                $destination2 = realpath('../webroot/img/collegeImages/thumbnail/') . '/';
                $image = $this->Components->load('Resize');  

                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    $image->resize($_FILES['image']['tmp_name'],$destination2.$filenameimg,'aspect_fill',500,300,0,0,0,0);
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination1 . $filenameimg);
                    $data['College']['image']=$filenameimg;
                    
                    
                }
            }
            $data['College']['updated_by'] = $this->Session->read('Admin.id');
            $data['College']['update_date'] = date('Y-m-d');
            if($this->College->save($data)){
                echo "true";
                die;
               // $this->Session->write('adminsuccess-msg','College updated successfully.');

            }else{           
                echo "false";
                die;
               /* $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'manageCollege'));*/
            }
        }
    }
    public function manageCourse($collegeId = null){
        $collegeId = $this->Session->read('College.id');
        $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Specialization' => array(
                    'className' => 'Specialization',
                    'foreignKey' => 'specialization_id'
                    )
                )
            )
        );
        $courses  = $this->CollegeCourse->findAllByCollegeId($collegeId);
        $this->set(compact('courses','collegeId'));
    }

    public function addCourse($specializationId = null){
        $this->loadModel('Specialization');
        $this->loadModel('Exam');
        $this->loadModel('CourseExam');
        $collegeId = $this->Session->read('College.id');
        $streams = $this->Stream->find('all',array('conditions'=>array('status'=>'1','is_delete'=>'0')));
        $exams = $this->Exam->find('all',array('conditions'=>array('status'=>'1','is_delete'=>'0')));
       
        if(!empty($specializationId)){
            $specializationId = base64_decode($specializationId);
            $this->CollegeCourse->bindModel(
            array('hasMany' => array(
                    'CourseExam' => array(
                        'className' => 'CourseExam',
                        'foreignKey' => 'college_course_id'
                        )
                    )
                )
            );
            $special = $this->CollegeCourse->findById($specializationId);

            $programs = $this->Program->findAllByStreamId($special['CollegeCourse']['stream_id']);
            //pr($special);die;
            $allcourses = $this->Course->findAllByProgramId($special['CollegeCourse']['program_id']);
            $allspecial = $this->Specialization->findAllByCourseId($special['CollegeCourse']['course_id']);
            $this->set(compact('special','programs','allcourses','allspecial','specializationId'));
        }
       /* $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Specialization' => array(
                    'className' => 'Specialization',
                    'foreignKey' => 'specialization_id'
                    )
                )
            )
        );*/
        //$courses  = $this->CollegeCourse->findAllByCollegeId($collegeId);
        $this->set(compact('streams','courses','collegeId','exams'));
        if($this->request->is('post')){
            $data['CollegeCourse'] = $this->data['CollegeCourse'];
            
            $destination = realpath('../webroot/img/docs/') . '/';
            $image = $this->Components->load('Resize');  
            if (!empty($_FILES['brochure']['tmp_name'])) 
            {
                if(is_uploaded_file($_FILES['brochure']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['brochure']['name'];
                    
                    move_uploaded_file($_FILES['brochure']['tmp_name'], $destination . $filenameimg);
                    $data['CollegeCourse']['brochure']=$filenameimg;
                    
                    
                }
            }
             if (!empty($_FILES['Structure']['tmp_name'])) 
            {
                if(is_uploaded_file($_FILES['Structure']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['Structure']['name'];
                    
                    move_uploaded_file($_FILES['Structure']['tmp_name'], $destination . $filenameimg);
                    $data['CollegeCourse']['Structure']=$filenameimg;
                    
                    
                }
            }
             if (!empty($_FILES['Placement']['tmp_name'])) 
            {
                if(is_uploaded_file($_FILES['Placement']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['Placement']['name'];
                    
                    move_uploaded_file($_FILES['Placement']['tmp_name'], $destination . $filenameimg);
                    $data['CollegeCourse']['Placement']=$filenameimg;
                    
                    
                }
            }
             if (!empty($_FILES['Offline']['tmp_name'])) 
            {
                if(is_uploaded_file($_FILES['Offline']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['Offline']['name'];
                    
                    move_uploaded_file($_FILES['Offline']['tmp_name'], $destination . $filenameimg);
                    $data['CollegeCourse']['Offline']=$filenameimg;
                    
                    
                }
            }
               // pr($this->data['CourseExam']);die;
                $this->CollegeCourse->save($data);
                $course_id = $this->CollegeCourse->getLastInsertId();
                if(!empty($data['CollegeCourse']['id']))
                    $course_id = $data['CollegeCourse']['id'];
                if(!empty($this->data['CourseExam'])){
                    $this->CourseExam->deleteAll(array('college_course_id'=>$course_id));
                    foreach($this->data['CourseExam']['exam_id'] as $exam){
                        $exm['CourseExam']['exam_id'] = $exam;
                        $exm['CourseExam']['college_course_id'] = $course_id;
                        $exm['CourseExam']['college_id'] = $this->Session->read('College.id');
                        $this->CourseExam->create();
                        $this->CourseExam->save($exm);

                    }
                }
                //$this->Session->write('adminsuccess-msg','Course added successfully.');
            
            $this->redirect(array('action' => 'manageCourse'));

        }
    }

    public function collegeGallery(){
        $collegeId = $this->Session->read('College.id');
        $this->loadModel('Gallery');
        $this->CollegeGallery->bindModel(
        array('belongsTo' => array(
                'Gallery' => array(
                    'className' => 'Gallery',
                    'foreignKey' => 'gallery_id'
                    )
                )
            )
        );
        $gallery = $this->Gallery->find('all',array('fields'=>array('id','name')));
        $this->Paginator->settings = array('CollegeGallery' => array('limit' => 25,'conditions'=>array('college_id'=>$collegeId)));
        $galleries=$this->paginate('CollegeGallery');
        //pr($gallery);die;
        $this->set(compact('galleries','gallery','collegeId'));
         if($this->request->is('post')){
            //pr($_FILES);die;
            $destination = realpath('../webroot/img/gallery/small/') . '/';
            $destination1 = realpath('../webroot/img/gallery/large/') . '/';
            $image = $this->Components->load('Resize');
            foreach($_FILES['image']['name'] as $key => $val){
                if (!empty($_FILES['image']['tmp_name'][$key])) 
                {
                    if(is_uploaded_file($_FILES['image']['tmp_name'][$key]))
                    {
                        
                        $filenameimg = time().'-'.$_FILES['image']['name'][$key];
                        $image->resize($_FILES['image']['tmp_name'][$key],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                        
                        move_uploaded_file($_FILES['image']['tmp_name'][$key], $destination1 . $filenameimg);
                        $data['CollegeGallery']['image']=$filenameimg;
                        
                        
                    }
                }
                $data['CollegeGallery']['college_id']=$collegeId;
                //pr($data);die;
                $this->CollegeGallery->create();
                $this->CollegeGallery->save($data);
            }
            
            $this->Session->write('adminsuccess-msg','Gallery updated successfully.');
            
            $this->redirect(array('action' => 'collegeGallery'));
           
            
        }
    }

    public function saveGallery(){
        $data = $this->data;
        $this->CollegeGallery->saveAll($data);
        $this->redirect($this->referer());
    }

    public function manageEvent($collegeId = null,$eventId = null){
        $this->loadModel('Event');
        $collegeId = $this->Session->read('College.id');
        $events=$this->Event->findAllByCollegeId($collegeId);
        if(!empty($eventId)){
            $event = $this->Event->findById($eventId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('events','event','eventId','collegeId'));
        if($this->request->is('post')){
            $data = $this->data;
           //pr($data);die;
            if($this->Event->saveAll($data))
                $this->Session->write('adminsuccess-msg','Event updated successfully.');
            else            
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'manageEvent'));
            
        }
    }

        
        
    public function changeStatus($model = null, $id = null){
        $this->loadModel($model);
        $status = $this->$model->findById($id);
        
        if($status[$model]['status'] == '1'){
            $status[$model]['status'] = '0';
        }else{
            $status[$model]['status'] = '1';
        }
        
        $this->$model->save($status);
        echo $status[$model]['status'];
        die;
        //$this->redirect($this->referer());
    }    

    public function downloadBrochure($name = null) {
        $this->viewClass = 'Media';
        // Download app/outside_webroot_dir/example.zip
        $params = array(
            'id'        => $name,
            'name'      => 'example',
            'download'  => true,
            'path'      => APP . 'webroot/img/docs' . DS
        );
        $this->set($params);
    }

    public function findPlan($id=null){
        if(!empty($id)){
            $plan_cost = $this->MembershipPlan->findById($id);
            echo $plan_cost['MembershipPlan']['plan_cost'];
        }
        else{
            echo 0;
        }
        die;
    }
    public function studentPlan($studentId=null){

        $this->loadModel('StudentPlan');
        if(!empty($_GET)){
            if($_GET['show'] == 'never'){
                $std['Student']['id'] = $studentId;
                $std['Student']['plan_popup'] = 1;
                //pr($std);die;
                $this->Student->save($std);
                if($this->Session->check('Student'))
                    $this->Session->Write('Student.plan_popup',1);
                $this->redirect(array('action' => 'studentdashboard'));

            }
            else if($_GET['show'] == 'later'){
                $std['Student']['id'] = $studentId;
                $std['Student']['plan_popup'] = 2;
                $this->Student->save($std);
                if($this->Session->check('Student'))
                    $this->Session->Write('Student.plan_popup',1);
                $this->redirect(array('action' => 'studentdashboard'));
            }
        }    
        if($this->request->is('post')){
            $data = $this->data;
           //pr($data);die;
            $data['StudentPlan']['start_date'] = date('Y-m-d');
            //$data['MembershipPlan']['end_date'] = 
            if(empty($studentId)){
                $data['StudentPlan']['student_id'] = $this->Session->read('Student.id'); 
            }
            else{
                $data['StudentPlan']['student_id'] = $studentId; 
            }
            $planbenefit = $this->MembershipPlan->findById($data['StudentPlan']['plan_id']);
            //pr($planbenefit);die;
            if($planbenefit['MembershipPlan']['addon_feature3']){
                $this->loadModel('Wallet');
                $this->loadModel('WalletTransaction');
                $stuWallet = $this->Wallet->findByStudentId($this->Session->read('Student.id'));
                //pr($stuWallet);die;
                @$stuWallet['Wallet']['amount'] += @$planbenefit['MembershipPlan']['wallet_credit'];
                $stuWallet['Wallet']['student_id'] = $this->Session->read('Student.id');
                $stuWallet['Wallet']['created_date'] = date('Y-m-d H:i:s');
                $this->Wallet->save($stuWallet);
                $walletId = $this->Wallet->getLastInsertId();
                if(!empty($stuWallet['Wallet']['id']))
                    $walletId = $stuWallet['Wallet']['id'];
                $walletTranns['WalletTransaction']['wallet_id'] = $walletId;
                $walletTranns['WalletTransaction']['created_date'] = date('Y-m-d H:i:s');
                $walletTranns['WalletTransaction']['transaction_by'] = 'Automated';
                $walletTranns['WalletTransaction']['description'] = 'MembershipPlan wallet transfer amount';
                $walletTranns['WalletTransaction']['due_to'] = 'MembershipPlan';
                $walletTranns['WalletTransaction']['due_to'] = 'MembershipPlan';
                $walletTranns['WalletTransaction']['student_id'] = $this->Session->read('Student.id');
                //pr($walletTranns);die;
                $this->WalletTransaction->save($walletTranns);
                //$wallet['Wallet']['amount'] =  
            }
            if($this->StudentPlan->save($data))
                $this->Session->write('adminsuccess-msg','Your plan has been active');
            else            
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'studentdashboard'));
            
        }
        $this->redirect(array('action' => 'studentdashboard'));
    }
    public function studentWallet(){
        $this->loadModel('Wallet');
        $this->loadModel('StudentPlan');
        $this->loadModel('WalletTransaction');
        $id = $this->Session->read('Student.id');
        $walletdet = $this->Wallet->findByStudentId($id);
        //$walletdetails = $this->WalletTransaction->find('all', array('conditions' => array('student_id' => $id, 'status' => 0)));
        $walletdetails = $this->WalletTransaction->findAllByStudentId($id);
        
        //die;
        $plan_id = $this->StudentPlan->find('first', array('conditions' => array('student_id' => $id, 'status' => 0)));
        //$plan_prem = $this->MembershipPlan->findById($plan_id['StudentPlan']['plan_id']);
        $this->set(compact('walletdet','plan_prem','walletdetails'));
        if($this->request->is('post')){
             $data = $this->data;
             //print_r($data);
             //die;
             $walletdesdata['WalletTransaction']['student_id'] = $walletdet['Wallet']['student_id'];
             $walletdesdata['WalletTransaction']['wallet_id'] = $walletdet['Wallet']['id'];
             $walletdesdata['WalletTransaction']['description'] = $data['remarks'];
             $walletdesdata['WalletTransaction']['Amount'] = $data['amount'];
             $walletdesdata['WalletTransaction']['due_to'] = 'Added To Wallet';
             $walletdesdata['WalletTransaction']['created_date'] = date('Y-m-d H:i:s');
             $this->WalletTransaction->create();
             $this->WalletTransaction->save($walletdesdata);
             $walletdet['Wallet']['amount'] += $data['amount'];
             $walletdet['Wallet']['student_id']=$id;
             $this->Wallet->save($walletdet);
             //$this->redirect(array('action'=>'thank'));
             $this->redirect($this->referer());
        }
    }

    public function clgSearch($key = null){
        $key = $key;
        $this->College->bindModel(
            array('belongsTo' => array(
                    'City' => array(
                        'className' => 'City',
                        'foreignKey' => 'city_id'                        
                        )
                    )
                )
            );
        $clg = $this->College->find('all',array('conditions'=>array('College.status'=>'1','College.is_delete'=>'0','OR' => array( array('College.name Like'=>'%'.$key.'%'), array('College.short_name Like'=>'%'.$key.'%'), array('College.address Like'=>'%'.$key.'%') ) ),'fields'=>array('id','name','city_id','City.city_name','location','short_name')));
        //pr($clg);die;
        echo json_encode($clg);
        die;
    }

    public function clgStrm($collegeId =null){
        $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Stream' => array(
                    'className' => 'Stream',
                    'foreignKey' => 'stream_id'
                    
                    )
                )
            )
        );
        $strm = $this->CollegeCourse->findAllByCollegeId($collegeId,array('DISTINCT stream_id','college_id','Stream.stream'));
        //pr($strm);
        echo json_encode($strm);
        die;
    }

    public function clgCrs($collegeId = null,$streamId = null){
        $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Course' => array(
                    'className' => 'Course',
                    'foreignKey' => 'course_id'
                    
                    )
                )
            )
        );
        $crs = $this->CollegeCourse->find('all',array('conditions'=>array('CollegeCourse.college_id' => $collegeId,'CollegeCourse.stream_id'=>$streamId),'fields'=>array('Course.course','course_id','stream_id','college_id')));
        //pr($crs);
        echo json_encode($crs);
        die;
    }

    public function clgSearchcrse($q = null){
        $this->loadModel('Course');
        $crs = $this->Course->find('all',array('conditions'=>array('status'=>'1','is_delete'=>'0','course LIKE'=>'%'.$q.'%')));
        echo json_encode($crs);
        die;
    }

    public function landingPage(){
        $this->loadModel('Slider');
        $this->loadModel('New');
        $this->loadModel('Link');
        $this->loadModel('Testimonial');
        $this->loadModel('MembershipPlan');
        $testimonials = $this->Testimonial->find('all', array('conditions'=>array('status'=>'1')));
        $links = $this->Link->find('all', array('conditions'=>array('status'=>'1')));
        $slides = $this->Slider->find('all', array('conditions'=>array('status'=>'1','from_date <=' => date('Y-m-d'),'to_date >=' => date('Y-m-d'))));
        //;die;
        $news = $this->New->find('all',array('conditions'=>array('status'=>'1')));
        $trendingColleges = $this->College->find('all',array('conditions'=>array('status' =>'1','trending'=>'1')));
        $membership = $this->MembershipPlan->find('all', array('limit' => 3,'conditions' => array('status' => 1)));
        $this->set(compact('trendingColleges','slides','news','links','testimonials','membership'));

    }
    public function applicationPage(){
        $studentId = $this->Session->read('Student.id');
        $this->loadModel("Cart");
        $this->loadModel("Country");
        $this->loadModel("StudentAcademic");
        $this->loadModel("StudentMeta");
        $this->loadModel("StudentExperience");
        $this->loadModel("StudentAward");
        $this->loadModel("StudentDocs");
        $this->loadModel("CollegeFieldMeta");

        $this->loadModel('OrderApplicationForm');
        $this->loadModel('AfCompetitiveExam');
        $this->loadModel('AfStudentAcademic');
        $this->loadModel('AfStudentExperience');
        $this->loadModel('AfCustomField');

        $check_order = $this->Order->find('first', array('conditions' => array('student_id' => $studentId, 'status' =>0)));
        
        $cart_course_ids = $this->Cart->find('list', array('fields' => array('course_id'),'conditions' => array('student_id' => $studentId)));
        //pr($cart_course_ids);
        $course_data = $this->CollegeCourse->findAllById($cart_course_ids);
        //pr($course_data);die;
        foreach($course_data as $cost){
                @$amount = @$amount + $cost['CollegeCourse']['formsadda_cost'];
        }
        $nof = count($course_data);
        $orderData['Order']['amount'] = $amount;
        $orderData['Order']['no_of_forms'] = $nof;
        $orderData['Order']['student_id'] = $studentId;
            

        if(!empty($check_order)){
            $orderData['Order']['id'] = $check_order['Order']['id'];
            $this->Order->save($orderData);
            $orderId = $check_order['Order']['id'];
        }
        else{
            $this->Order->save($orderData);
            $orderId = $this->Order->getLastInsertId();
        }

        $this->OrderMeta->deleteAll(array('order_id' => $orderId));
        $this->AfCustomField->deleteAll(array('order_id' => $orderId));
        $this->AfCompetitiveExam->deleteAll(array('order_id' => $orderId));
        $this->AfStudentAcademic->deleteAll(array('order_id' => $orderId));
        $this->AfStudentExperience->deleteAll(array('order_id' => $orderId));
        $cart_data = $this->Cart->find('all', array('conditions' => array('student_id' => $studentId)));
        foreach($cart_data as $cartData){
            $orderMetaData['OrderMeta']['order_id'] = $orderId;
            $orderMetaData['OrderMeta']['student_id'] = $studentId;
            $orderMetaData['OrderMeta']['college_id'] = $cartData['Cart']['college_id'];
            $orderMetaData['OrderMeta']['course_id'] = $cartData['Cart']['course_id'];
            $this->OrderMeta->create();
            $this->OrderMeta->save($orderMetaData);
        }
        
        

        $country = $this->Country->find('all');
        if(!empty($studentId)){
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentAcademic' => array(
                            'className' => 'StudentAcademic',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $this->Student->bindModel(
                array('hasOne' => array(
                        'StudentMeta' => array(
                            'className' => 'StudentMeta',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentExperience' => array(
                            'className' => 'StudentExperience',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentAward' => array(
                            'className' => 'StudentAward',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentDocs' => array(
                            'className' => 'StudentDocs',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );

            //$studentId = base64_decode($studentId);
            $orderForm = $this->Student->findById($studentId);
            $states = $this->State->findAllByCountryId($orderForm['Student']['country_id']);
            $cities = $this->City->findAllByStateId($orderForm['Student']['state_id']);
            //pr($orderForm);
            //die;
        }

        $cart_college_ids = $this->Cart->find('list', array('fields' => array('college_id'),'conditions' => array('student_id' => $this->Session->read('Student.id'))));
        //echo $this->Session->read('Student.id');
        //pr($cart_college_ids);
        $this->CollegeFieldMeta->bindModel(
                array('belongsTo' => array(
                        'CollegeField' => array(
                            'className' => 'CollegeField',
                            'foreignKey' => 'field_id'
                            )
                        )
                    )
            );
        $customFields = $this->CollegeFieldMeta->find('all',array('conditions'=>array('college_id'=>$cart_college_ids),'fields'=>array('DISTINCT field_id','CollegeField.field_name')));

        $members = $this->Student->find('all',array('conditions'=>array('parent_id'=>$this->Session->read('Student.id')),'fields'=>array('name','parent_relation','id')));
        //pr($members);die;
        $this->set(compact('country','orderForm','states','cities','orderId','customFields','members'));
        if($this->request->is('post')){
            $data = $this->data;
            //pr($_FILES);
            $OrderApplicationForm['OrderApplicationForm'] = $data['OrderApplicationForm'];
            //$StudentAcademic['AfStudentAcademic'] = $data['StudentAcademic'];
            //$StudentExperience['AfStudentExperience'] = $data['StudentExperience'];
            //$CompetitiveExam['AfCompetitiveExam'] = $data['CompetitiveExam'];
            //$CustomFileds['AfCustomField'] = $CustomFileds['CustomField'];
            if(!empty($_FILES['Photo'])){
                $destination = realpath('../webroot/img/studentImages/small/') . '/';
                $destination1 = realpath('../webroot/img/studentImages/large/') . '/';
                $image = $this->Components->load('Resize');  

                if(is_uploaded_file($_FILES['Photo']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['Photo']['name'];
                    $image->resize($_FILES['Photo']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    
                    move_uploaded_file($_FILES['Photo']['tmp_name'], $destination1 . $filenameimg);
                    $OrderApplicationForm['OrderApplicationForm']['image']=$filenameimg;
                }    
            }
            //pr($OrderApplicationForm);die;
            $this->OrderApplicationForm->save($OrderApplicationForm);
            $application_id = $this->OrderApplicationForm->getLastInsertId();
            
            if(!empty($data['CompetitiveExam'])){
                foreach($data['CompetitiveExam'] as $comExm){
                    $StudentAcademic['AfCompetitiveExam'] =  $comExm;
                    $StudentAcademic['AfCompetitiveExam']['student_id'] =  $this->Session->read('Student.id');
                    $StudentAcademic['AfCompetitiveExam']['created_date'] =  date('Y-m-d H:i:s');
                    $StudentAcademic['AfCompetitiveExam']['af_id'] = $application_id;
                    $StudentAcademic['AfCompetitiveExam']['order_id'] = $OrderApplicationForm['OrderApplicationForm']['order_id'];
                    $this->AfCompetitiveExam->create();
                    $this->AfCompetitiveExam->save($StudentAcademic);

                }
            }
            if(!empty($data['CustomFileds'])){    
                foreach($data['CustomFileds'] as $key=>$val){
                    $StudentAcademic['AfCustomField']['field_name'] =  $key;
                    $StudentAcademic['AfCustomField']['value'] =  $val;
                    $StudentAcademic['AfCustomField']['student_id'] =  $this->Session->read('Student.id');
                    $StudentAcademic['AfCustomField']['created_date'] =  date('Y-m-d H:i:s');
                    $StudentAcademic['AfCustomField']['af_id'] = $application_id;
                    $StudentAcademic['AfCustomField']['order_id'] = $OrderApplicationForm['OrderApplicationForm']['order_id'];
                    $this->AfCustomField->create();
                    $this->AfCustomField->save($StudentAcademic);

                }
            }
            if(!empty($data['StudentAcademic'])){    
                foreach($data['StudentAcademic'] as $StuAcad){
                    $StudentAcademic['AfStudentAcademic'] =  $StuAcad;
                    $StudentAcademic['AfStudentAcademic']['student_id'] =  $this->Session->read('Student.id');
                    $StudentAcademic['AfStudentAcademic']['created_date'] =  date('Y-m-d H:i:s');
                    $StudentAcademic['AfStudentAcademic']['af_id'] = $application_id;
                    $StudentAcademic['AfStudentAcademic']['order_id'] = $OrderApplicationForm['OrderApplicationForm']['order_id'];
                    $this->AfStudentAcademic->create();
                    $this->AfStudentAcademic->save($StudentAcademic);

                }
            }
            if(!empty($data['StudentExperience'])){    
                 foreach($data['StudentExperience'] as $StuExp){
                    $StudentAcademic['AfStudentExperience'] =  $StuExp;
                    $StudentAcademic['AfStudentExperience']['student_id'] =  $this->Session->read('Student.id');
                    $StudentAcademic['AfStudentExperience']['created_date'] =  date('Y-m-d H:i:s');
                    $StudentAcademic['AfStudentExperience']['af_id'] = $application_id;
                    $StudentAcademic['AfStudentExperience']['order_id'] = $OrderApplicationForm['OrderApplicationForm']['order_id'];
                    $this->AfStudentExperience->create();
                    $this->AfStudentExperience->save($StudentAcademic);

                }
            }    

            $this->redirect(array('action'=>'checkout',base64_encode($OrderApplicationForm['OrderApplicationForm']['order_id'])));
            
        }

    }
    public function cart(){
        $this->loadModel("Cart");
        $this->loadModel("COurse");
        $selectedCourse = $this->Course->findById($this->Session->read('selectedCourse'));
        $this->loadModel("CourseSpecialization");
        $this->CollegeCourse->bindModel(
        array('hasMany' => array(
                'CourseSpecialization' => array(
                    'className' => 'CourseSpecialization',
                    'foreignKey' => 'college_course_id',
                    'conditions' => array('CourseSpecialization.status'=>'1','CourseSpecialization.is_delete'=>'0'),
                    'fields' => array('specialization_id','id')
                    )
                )
            )
        );
        $this->CourseSpecialization->bindModel(
        array('belongsTo' => array(
                'Specialization' => array(
                    'className' => 'Specialization',
                    'foreignKey' => 'specialization_id',
                    'fields' => array('specialization','id')
                    )
                )
            )
        );
        $this->CollegeCourse->recursive = '3';
        if($this->Session->check('Student')){
            $cart_course_ids = $this->Cart->find('list', array('fields' => array('course_id'),'conditions' => array('student_id' => $this->Session->read('Student.id'))));
            $college_courses = @$this->CollegeCourse->findAllById($cart_course_ids);
        }
        else{
            if($this->Session->check('Cart')){
                foreach(@$this->Session->read('Cart.applynow') as $key => $value){
                $college_course_ids[] = $key;
                }
                @$college_courses = @$this->CollegeCourse->findAllById($college_course_ids);
                //
                
            }
        }
        
        //pr($college_courses);die;
        $this->set(compact('college_courses','selectedCourse'));
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            $this->loadModel('CartSpecialization');
            $this->CartSpecialization->deleteAll(array('student_id'=>$this->Session->read('Student.id')));
            foreach($data as $cart){
                if(!empty($cart['specialization_id'])){
                    foreach($cart['specialization_id'] as $c){
                        $car['CartSpecialization']['student_id'] = $this->Session->read('Student.id');
                        $car['CartSpecialization']['college_course_id'] = $cart['college_course_id'];
                        $car['CartSpecialization']['specialization_id'] = $c;
                        $this->CartSpecialization->create();
                        $this->CartSpecialization->save($car);

                    }
                }    
            }
            $this->redirect(array('action'=>'applicationPage'));
        }
    }
    public function addCart(){
        $this->loadModel("Cart");
        $college_id = $_POST['college_id'];
        $course_id = $_POST['course_id'];

        //$this->Session->delete('Cart');
        //pr($this->Session->read('Cart.applynow'));
        if($this->Session->check('Cart')){
            if(array_key_exists($_POST['course_id'], $this->Session->read('Cart.applynow'))){
                echo false;die;
            }else{
            $this->Session->write('Cart.applynow.'.$course_id,$college_id);
            }
        }    
        else{
            $this->Session->write('Cart.applynow.'.$course_id,$college_id);
        }
        //$this->Session->write('Cart.course_id', $course_id);
        /*$this->Cookie->write('Cart.college_id', $college_id);
        $this->Cookie->write('Cart.course_id', $course_id);*/
        //pr($this->Session->read('Cart'));
        echo true;die;
    }
    public function delete_cart($id=null){
        $this->Session->read('Cart');
        unset($_SESSION['Cart']['applynow'][$id]);
        if($this->Session->check('Student')){
            $find_delete = $this->Cart->find('first', array('conditions' => array('student_id' => $this->Session->read('Student.id'), 'course_id' => $id)));
            if(!empty($find_delete)){
                $this->Cart->delete($find_delete['Cart']['id']);
            }
        }
        echo true;die;
    }
    public function checkout($orderId= null){
         $orderId = base64_decode($orderId);
         $this->loadModel('Wallet');
         $this->loadModel('Coupon');
         $this->loadModel('CartSpecialization');
        $this->Order->bindModel(
            array('hasMany' => array(
                    'OrderMeta' => array(
                        'className' => 'OrderMeta',
                        'foreignKey' => 'order_id',
                        )
                    )
                )
            );
        $this->OrderMeta->bindModel(
            array('belongsTo' => array(
                    'College' => array(
                        'className' => 'College',
                        'foreignKey' => 'college_id',
                        'fields' =>'name'
                        )
                    )
                )
            );
        $this->OrderMeta->bindModel(
            array('belongsTo' => array(
                    'CollegeCourse' => array(
                        'className' => 'CollegeCourse',
                        'foreignKey' => 'course_id',
                        'fields' => array('formsadda_cost','registration_fee','fees','id','college_id','direct_apply','link')
                        )
                    )
                )
            );
        $this->CollegeCourse->bindModel(
            array('hasMany' => array(
                    'CartSpecialization' => array(
                        'className' => 'CartSpecialization',
                        'foreignKey' => 'college_course_id',
                        'conditions' => array('CartSpecialization.student_id'=>$this->Session->read('Student.id'))
                        //'firelds' => array('formsadda_cost','fees')
                        )
                    )
                )
            );
        $this->CartSpecialization->bindModel(
            array('belongsTo' => array(
                    'Specialization' => array(
                        'className' => 'Specialization',
                        'foreignKey' => 'specialization_id',
                        'fields' => array('specialization')
                        )
                    )
                )
            );
        $this->Order->recursive = 4;
        $order = $this->Order->findById($orderId);
        $wallet = $this->Wallet->findByStudentId($this->Session->read('Student.id'));
        $plan_id = $this->StudentPlan->find('first', array('conditions' => array('student_id' => $this->Session->read('Student.id'), 'status' => 0)));
        $plan_prem = $this->MembershipPlan->findById($plan_id['StudentPlan']['plan_id']);
        
        $this->set(compact('order','wallet','plan_prem'));
        //pr($order);die;

        if($this->request->is('post')){
            $data= $this->data;
            //pr($data);
            $order_id = base64_decode($data['order_id']);
            $order = $this->Order->findById($order_id);
            $wallet = $this->Wallet->findByStudentId($this->Session->read('Student.id'));
            $total_amnt = 0;
            //pr($wallet);
            //pr($order);
            if(@$data['type_of_pay'] == 0 && !empty($data['promocode'])){
                $coupon = $this->Coupon->findByCode($data['promocode']);
                if(!empty($coupon)){
                    if($coupon['Coupon']['valid_from'] < date('Y-m-d') && $coupon['Coupon']['valid_to'] > date('Y-m-d')){
                        if($coupon['Coupon']['category'] == '0'){
                            if($order['Order']['amount'] > $coupon['Coupon']['amount']){
                                $total_amnt = $order['Order']['amount'] - $coupon['Coupon']['amount'];
                                $order['Order']['coupon_used'] = '1';
                                $order['Order']['coupon_id'] = $coupon['Coupon']['id'];
                            }
                        }else{
                            
                                $total_amnt = $order['Order']['amount'] - ($order['Order']['amount']*$coupon['Coupon']['amount']/100);
                                $order['Order']['coupon_used'] = '1';
                                $order['Order']['coupon_id'] = $coupon['Coupon']['id'];
                                
                                
                            
                        }
                   
                    
                    }
                }
            }
            if(@$data['type_of_pay'] == 1){
                //for member ship
                $plan_id = $this->StudentPlan->find('first', array('conditions' => array('student_id' => $this->Session->read('Student.id'), 'status' => 0)));
                $plan_prem = $this->MembershipPlan->findById($plan_id['StudentPlan']['plan_id']);
                if(@$plan_prem['MembershipPlan']['addon_feature4'] == 1){
                    $total_amnt = $plan_prem['MembershipPlan']['fixed_price'];
                }else{
                    $total_amnt = $order['Order']['amount'];
                }
                
            }
            if(@$data['wallet'] == 'on'){
                if(!empty($wallet['Wallet']['amount'])){
                    if(empty($total_amnt)){
                        //echo "hi";
                        if($wallet['Wallet']['amount'] > $order['Order']['amount']){
                            $wallet['Wallet']['amount'] = $wallet['Wallet']['amount'] - $order['Order']['amount'];
                             $order['Order']['wallet_used'] = '1';
                             $order['Order']['wallet_amount_used'] = $order['Order']['amount'];
                             $total_amnt = 0;
                             
                           
                        }else{
                            $total_amnt = $order['Order']['amount'] - $wallet['Wallet']['amount'];
                            $order['Order']['wallet_used'] = '1';
                            $order['Order']['wallet_amount_used'] = $wallet['Wallet']['amount'];
                            $wallet['Wallet']['amount'] = '0';
                            
                             
                        }
                    }else{
                        if($wallet['Wallet']['amount'] > $total_amnt){
                            $wallet['Wallet']['amount'] = $wallet['Wallet']['amount'] - $total_amnt;
                            $order['Order']['wallet_used'] = '1';
                            $order['Order']['wallet_amount_used'] = $total_amnt;
                            $total_amnt = '0';
                            
                            
                        }else{
                            $total_amnt = $total_amnt - $wallet['Wallet']['amount'];
                            $order['Order']['wallet_used'] = '1';
                            $order['Order']['wallet_amount_used'] = $wallet['Wallet']['amount'];
                            $wallet['Wallet']['amount']='0';
                            
                            
                        }
                    }
                }
            }

            $order['Order']['new_amount'] = $total_amnt;
             $order['Order']['status'] = '1';
            //echo $total_amnt;
            //pr($wallet);
            //pr($order);die;
            $this->Wallet->save($wallet); 
            $this->Order->save($order); 
            $this->loadModel('OrderApplicationForm');
            $this->loadModel('AfStudentAcademic');
            $this->loadModel('AfStudentExperience');
            $this->loadModel('AfCustomField');
            $this->loadModel('AfCompetitiveExam');
            $this->OrderApplicationForm->bindModel(
            array('belongsTo' => array(
                    'City' => array(
                        'className' => 'City',
                        'foreignKey' => 'city_id',
                        )
                    )
                )
            );
            $this->OrderApplicationForm->bindModel(
            array('belongsTo' => array(
                    'State' => array(
                        'className' => 'State',
                        'foreignKey' => 'state_id',
                        )
                    )
                )
            );
            $this->OrderMeta->bindModel(
            array('belongsTo' => array(
                    'College' => array(
                        'className' => 'College',
                        'foreignKey' => 'college_id',
                        'fields' => array('name','email')
                        )
                    )
                )
            );
            $this->OrderMeta->bindModel(
            array('belongsTo' => array(
                    'CollegeCourse' => array(
                        'className' => 'CollegeCourse',
                        'foreignKey' => 'course_id',
                        'fields' => array('course_id','formsadda_cost')
                        )
                    )
                )
            );
            $this->CollegeCourse->bindModel(
            array('belongsTo' => array(
                    'Course' => array(
                        'className' => 'Course',
                        'foreignKey' => 'course_id',
                        'fields' => array('course')
                        )
                    )
                )
            );
             $this->CollegeCourse->unbindModel(array('belongsTo'=>array('College')));
             $this->CollegeCourse->unbindModel(array('hasMany'=>array('CartSpecialization')));
             $this->Course->unbindModel(array('hasMany'=>array('Specialization')));
            $this->OrderMeta->recursive = 3;
            $detail = $this->OrderApplicationForm->findByOrderId($order['Order']['id']);
            $acds = $this->AfStudentAcademic->findAllByOrderId($order['Order']['id']);
            $wrks = $this->AfStudentExperience->findAllByOrderId($order['Order']['id']);
            $csts = $this->AfCustomField->findAllByOrderId($order['Order']['id']);
            $cmps = $this->AfCompetitiveExam->findAllByOrderId($order['Order']['id']);
            $ometas = $this->OrderMeta->findAllByOrderId($order['Order']['id']);
        //pr($detail);die;
            $var='';
            $img = $detail["OrderApplicationForm"]["image"] == '' ? HTTP_ROOT.'img/photo.jpg' : HTTP_ROOT.'img/studentImages/large/'.$detail["OrderApplicationForm"]["image"];
        $var .='<!DOCTYPE html>
<html>
<head>
    <title>Application</title>
    <!--<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>-->
    <style type="text/css">
        body
        {
            background-color: #595959;
        }

        .container
        {
            background-color: white!important;
            width: 832px!important;
            padding: 4%;
        }

        .logo
        {
            height: 50px;
            width:190px;
        }

        table
        {
            margin-top: 4%!important;
            border: 1px solid;
        }
        table th
        {
            background-color: #3C8B90;
            color: white;
        }

        .photo
        {
            width: 110px;
        }

        .row1
        {
            background-color: #bfbfbf;
        }
    </style>
</head>
<body style="background-color:#fff!important">
    <div class="container" style="margin-top: 1%;background-color:#fff!important">
        <img class="logo" src="http://52.33.154.113/Formsadda//frontend/mt-0362-home-logo.png">
        <div style="float:right;"><b>Application id : FA/'.date("Y").'/'.$order["Order"]["id"].'</b></div>
        <br>
    <table width="100%">
        <thead>
          <tr>
            <th colspan="3">Personal Details</th>
          </tr>
        </thead>
        <tbody>
            <tr>
                <td><b>Full Name: </b></td>
                <td>'.$detail["OrderApplicationForm"]["full_name"].'</td>
                <td rowspan="9"><img class="photo" src="'.$img.'"></td>
            </tr>
            <tr>
                <td><b>Email Id: </b></td>
                <td>'.$detail["OrderApplicationForm"]["email"].'</td>
                
            </tr>
            <tr>
                <td><b>Contact No.: </b></td>
                <td>'.$detail["OrderApplicationForm"]["mobile"].'</td>
                
            </tr>
            <tr>
                <td><b>Date Of Birth: </b></td>
                <td>'.$detail["OrderApplicationForm"]["dob"].'</td>
                
            </tr>
            <tr>
                <td><b>Gender: </b></td>
                <td>Male</td>
                
            </tr>
            <tr>
                <td><b>Category: </b></td>
                <td>'.$detail["OrderApplicationForm"]["category"].'</td>
                
            </tr>
            <tr>
                <td><b>Mother Tongue: </b></td>
                <td>'.$detail['OrderApplicationForm']['mother_tongue'].'</td>
                
            </tr>
            <tr>
                <td><b>Martial Status: </b></td>
                <td>Unmarried</td>
                
            </tr>
            <tr>
                <td><b>Nationality: </b></td>
                <td>Indian</td>
                
            </tr>
        </tbody>
    </table>
    <table width="100%">
        <thead>
          <tr>
            <th colspan="5">Academic Details</th>
          </tr>
        </thead>
        <tbody>
            <tr class="row1">
                <td><b>Exam Name</b></td>
                <td><b>Year of Passing</b></td>
                <td><b>Institution</b></td>
                <td><b>Board/University</b></td>
                <td><b>Percentage(%)</b></td>
            </tr>';
        $a = '';
            foreach($acds as $ac){
            $a .= '<tr>
                <td>'.$ac['AfStudentAcademic']['exam'].'</td>
                <td>'.$ac['AfStudentAcademic']['passing_year'].'</td>
                <td>'.$ac['AfStudentAcademic']['institute'].'</td>
                <td>'.$ac['AfStudentAcademic']['university'].'</td>
                <td>'.$ac['AfStudentAcademic']['percentage'].'</td>
            </tr>';
            }
            
         $var .= $a; 
         $var .='</tbody>
    </table>
    <table width="100%">
        <thead>
          <tr>
            <th colspan="4">Work Experience</th>
          </tr>
        </thead>
        <tbody>
            <tr class="row1">
                <td><b>Oragnizations Name</b></td>
                <td><b>Designation</b></td>
                <td><b>Joining Date</b></td>
                <td><b>Leaving Date</b></td>
            </tr>';
          $b = '';
            foreach($wrks as $wrk){
            $b .= '<tr>
                <td>'.$wrk['AfStudentExperience']['name'].'</td>
                <td>'.$wrk['AfStudentExperience']['designation'].'</td>
                <td>'.$wrk['AfStudentExperience']['joining'].'</td>
                <td>'.$wrk['AfStudentExperience']['releasing'].'</td>
            </tr>';
            }
          $var .= $b;
          $var .='</tbody>
    </table>
    <table width="100%">
        <thead>
          <tr>
            <th colspan="4">Competitive Exam Details</th>
          </tr>
        </thead>
        <tbody>
            <tr class="row1">
                <td><b>Exam Name</b></td>
                <td><b>Exam Year</b></td>
                <td><b>Composite Score</b></td>
                <td><b>Percentile</b></td>
            </tr>'; 
         $c= '';
        foreach($cmps as $cmp){
           $c =' <tr>
                <td>'.$cmp['AfCompetitiveExam']['name'].'</td>
                <td>'.$cmp['AfCompetitiveExam']['year'].'</td>
                <td>'.$cmp['AfCompetitiveExam']['score'].'</td>
                <td>'.$cmp['AfCompetitiveExam']['percentile'].'</td>
            </tr>';
        }    
        $var .=$c;
        $var .= '</tbody>
    </table>
    <table width="100%">
        <thead>
          <tr>
            <th colspan="4">Address Details</th>
          </tr>
        </thead>
        <tbody>
            <tr><td colspan="3"><div>'.$detail["OrderApplicationForm"]["address"].'</div></td></tr>
            <tr><td width="33.33%"><div><b>City: </b>'.$detail["City"]["city_name"].'</div></td>
                <td width="33.33%"><div><b>State: </b>'.$detail["State"]["statename"].'</div></td>
                <td width="33.33%"><div><b>Pin Code: </b>'.$detail["OrderApplicationForm"]["pincode"].'</div></td>
            </tr>
        </tbody>
    </table>
    
    <table width="100%">
        <thead>
          <tr>
            <th colspan="4">Application for College</th>
          </tr>
        </thead>
        <tbody>
            <tr class="row1">
                <td><b>College Name</b></td>
                <td><b>Course Name</b></td>
            </tr>';     
        $d ='';
            foreach($ometas as $omt){
            $d = '<tr>
                <td><b>'.$omt['College']['name'].'</b></td>
                <td><b>'.$omt['CollegeCourse']['Course']['course'].'</b></td>
            </tr>';
            }
        $var .= $d;
        $var .= '</tbody>
    </table>
    
    </div> 
    
</body>
</html>';         
      //  echo $var;die;
$this->loadModel('Cart');

$this->Cart->deleteAll(array('student_id'=>$detail['OrderApplicationForm']['student_id']));
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->addPage('', 'USLETTER');
        $pdf->setFont('helvetica', '', 9);
        //$pdf->writeHTMLcell(30, 0, $var);
        //$pdf->writeHTMLCell(0, 0, '', '', $var, 0, 1, 0, true, '', true);
        $pdf->writeHTML($var, true, 0, true, 0);
        $destination = realpath('../webroot/img/docs/') . '/';
        $filename = time().'-'.$this->Session->read('Student.name').'-'.$this->Session->read('Student.id').'.pdf';
        $pdf->Output($destination.$filename, 'F');
        $order['Order']['pdf'] = $filename;
        $this->Order->save($order);
        $Email = new CakeEmail();
        $Email->template('orders');
        $Email->viewVars(array('order' => $order,'ordermetas' => $ometas,'details'=>$detail));
        $Email->emailFormat('html');
        $Email->to($detail['OrderApplicationForm']['email']);
        $Email->from('info@Formsadda.com');
            
        $Email->attachments(array(
            $filename => array(
                'file' =>  $destination.$filename,
                'mimetype' => 'pdf',
                'contentId' =>  time().'-'.$this->Session->read('Student.name').'-'.$this->Session->read('Student.id')
            )
        ));
        //pr($ometas);die;
        $Email->send();
        foreach($ometas as $omt){
            $Email = new CakeEmail();
            $Email->from(array('info@formsadda.com' => 'Formsadda.com'));
            $Email->to($omt['College']['email']);
            $Email->subject('Applcation for your college.');
            $msg ='';
            $msg .= 'Hi '.$omt['College']['email'].'      ';
            $em = substr($detail['OrderApplicationForm']['email'],0,4);
            $emai = 'xxxx'.$em;
            $na = substr($detail['OrderApplicationForm']['full_name'],0,5);
            $nam = 'xxxxx'.$na;
            $ph = substr($detail['OrderApplicationForm']['mobile'],0,7);
            $phn = 'xxxxxxx'.$ph;
            $msg .= $nam.' is interested in taking addmission.Please contact him/her. Email : '.$emai.' Phone no. :'.$phn;
            $Email->send($msg);
        }
        $this->redirect(array('action'=>'thank'));
        }

    }
    public function thank(){
        
    }
    public function commentPage(){
        
    }
    public function collegeSpecialization($collegeCourseId = null,$editId = null){
        $this->layout="home";
        $this->loadModel('CourseSpecialization');
        $this->loadModel('Specialization');
        $collegeId = $this->Session->read('College.id');
        
        if(!empty($editId)){
            $this->CourseSpecialization->bindModel(
            array('belongsTo' => array(
                    'Specialization' => array(
                        'className' => 'Specialization',
                        'foreignKey' => 'specialization_id',
                        )
                    )
                )
            );
            $special = $this->CourseSpecialization->findById($editId);    
        }
        $this->CourseSpecialization->bindModel(
        array('belongsTo' => array(
                'Specialization' => array(
                    'className' => 'Specialization',
                    'foreignKey' => 'specialization_id',
                    )
                )
            )
        );
        $specials = $this->CourseSpecialization->find('all',array('conditions'=>array('college_course_id'=>$collegeCourseId)));
        $specializations = $this->Specialization->find('all',array('conditions'=>array('status'=>'1','is_delete'=>0)));
        $this->set(compact('specials','special','collegeCourseId','collegeId','specializations'));
    }
     public function saveCollegeSpecialization($collegeSpecializationId = null){
        $this->loadModel('CourseSpecialization');
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            $this->CourseSpecialization->save($data);
            $this->redirect(array('action'=>'collegeSpecialization',$data['CourseSpecialization']['college_course_id']));
        }
    }

    public function addCount($type=null,$review_id=null){
        $this->loadModel('ReviewCount');
        $count = $this->ReviewCount->findByReviewId($review_id);
        $student_id = $this->Session->read('Student.id');
        if($type==1){
            $data['ReviewCount'] = $count['ReviewCount'];
            $data['ReviewCount']['review_like'] = $count['ReviewCount']['review_like']+1;
            $this->ReviewCount->save($data);
            
            $this->Session->write('review_user',$student_id);
        }
        else if($type == 2){
            $data['ReviewCount'] = $count['ReviewCount'];
            $data['ReviewCount']['review_dislike'] = $count['ReviewCount']['review_dislike']+1;
            
            $this->ReviewCount->save($data);
            $this->Session->write('review_user',$student_id);
        }
        echo true;die;
    }
    public function applicationFormChange($id = null){
        $studentId = $id;
        $this->loadModel("Cart");
        $this->loadModel("Country");
        $this->loadModel("StudentAcademic");
        $this->loadModel("StudentMeta");
        $this->loadModel("StudentExperience");
        $this->loadModel("StudentAward");
        $this->loadModel("StudentDocs");
        $this->loadModel("CollegeFieldMeta");

        $check_order = $this->Order->find('first', array('conditions' => array('student_id' => $studentId, 'status' =>0)));
        
        $cart_course_ids = $this->Cart->find('list', array('fields' => array('course_id'),'conditions' => array('student_id' => $this->Session->read('Student.id'))));
        //pr($cart_course_ids);die;
        $course_data = $this->CollegeCourse->findAllById($cart_course_ids);
        //pr($course_data);die;
        foreach($course_data as $cost){
                @$amount = @$amount + $cost['CollegeCourse']['formsadda_cost'];
        }
        $nof = count($course_data);
        $orderData['Order']['amount'] = $amount;
        $orderData['Order']['no_of_forms'] = $nof;
        $orderData['Order']['student_id'] = $studentId;
            

        if(!empty($check_order)){
            $orderData['Order']['id'] = $check_order['Order']['id'];
            $this->Order->save($orderData);
            $orderId = $check_order['Order']['id'];
        }
        else{
            $this->Order->save($orderData);
            $orderId = $this->Order->getLastInsertId();
        }

        $order_meta_data = $this->OrderMeta->deleteAll(array('order_id' => $orderId));
        $cart_data = $this->Cart->find('all', array('conditions' => array('student_id' => $studentId)));
        foreach($cart_data as $cartData){
            $orderMetaData['OrderMeta']['order_id'] = $orderId;
            $orderMetaData['OrderMeta']['student_id'] = $studentId;
            $orderMetaData['OrderMeta']['college_id'] = $cartData['Cart']['college_id'];
            $orderMetaData['OrderMeta']['course_id'] = $cartData['Cart']['course_id'];
            $this->OrderMeta->create();
            $this->OrderMeta->save($orderMetaData);
        }
        
        

        $country = $this->Country->find('all');
        if(!empty($studentId)){
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentAcademic' => array(
                            'className' => 'StudentAcademic',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $this->Student->bindModel(
                array('hasOne' => array(
                        'StudentMeta' => array(
                            'className' => 'StudentMeta',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentExperience' => array(
                            'className' => 'StudentExperience',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentAward' => array(
                            'className' => 'StudentAward',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );
            $this->Student->bindModel(
                array('hasMany' => array(
                        'StudentDocs' => array(
                            'className' => 'StudentDocs',
                            'foreignKey' => 'student_id'
                            )
                        )
                    )
            );

            //$studentId = base64_decode($studentId);
            $orderForm = $this->Student->findById($studentId);
            $states = $this->State->findAllByCountryId($orderForm['Student']['country_id']);
            $cities = $this->City->findAllByStateId($orderForm['Student']['state_id']);
            //pr($orderForm);
            //die;
        }

        $cart_college_ids = $this->Cart->find('list', array('fields' => array('college_id'),'conditions' => array('student_id' => $this->Session->read('Student.id'))));
        //echo $this->Session->read('Student.id');
        //pr($cart_college_ids);
        $this->CollegeFieldMeta->bindModel(
                array('belongsTo' => array(
                        'CollegeField' => array(
                            'className' => 'CollegeField',
                            'foreignKey' => 'field_id'
                            )
                        )
                    )
            );
        $customFields = $this->CollegeFieldMeta->find('all',array('conditions'=>array('college_id'=>$cart_college_ids),'fields'=>array('DISTINCT field_id','CollegeField.field_name')));

        $members = $this->Student->find('all',array('conditions'=>array('parent_id'=>$this->Session->read('Student.id')),'fields'=>array('name','parent_relation','id')));
        //pr($members);die;
        $this->set(compact('country','orderForm','states','cities','orderId','customFields','members'));  

    }

    public function checkCode($orderId = null){
        $orderId = base64_decode($orderId);
        //pr($this->data);die;
        $data = $this->data;
       // $code = $code;
        $this->loadModel('Order');
        $this->loadModel('Wallet');
        $this->loadModel('Coupon');
        $data['promo'] = trim($data['promo']);
        $order = $this->Order->findById($orderId);
        $wallet = $this->Wallet->findByStudentId($this->Session->read('Student.id'));
        $resp= [];
        if(empty($data['promo']) && $data['wallet'] == 'false' && $data['ontime'] == 'false'){
            $resp['amount'] = $order['Order']['amount'];
            $resp['walletAmount'] = $wallet['Wallet']['amount'];
            $resp['status'] = 'true';
            //$resp['msg'] = 'Coupon Applied successfully !!!';
        }
        if($data['ontime'] == 'true'){

            $plan_id = $this->StudentPlan->find('first', array('conditions' => array('student_id' => $this->Session->read('Student.id'), 'status' => 0)));
            $plan_prem = $this->MembershipPlan->findById($plan_id['StudentPlan']['plan_id']);
           /* if($plan_prem['MembershipPlan']['fixed_price'] > $order['Order']['amount']){
                $resp['amount'] = $plan_prem['MembershipPlan']['fixed_price'];
            }else{

            }*/
            $resp['amount'] = $plan_prem['MembershipPlan']['fixed_price'];
            $resp['walletAmount'] = $wallet['Wallet']['amount'];
            $resp['status'] = 'true';
        }
        if(!empty($data['promo'])){
            $coupon = $this->Coupon->findByCode($data['promo']);
            if($coupon['Coupon']['valid_from'] < date('Y-m-d') && $coupon['Coupon']['valid_to'] > date('Y-m-d')){
            if($coupon['Coupon']['category'] == '0'){
                if($order['Order']['amount'] > $coupon['Coupon']['amount']){
                    $resp['amount'] = $order['Order']['amount'] - $coupon['Coupon']['amount'];
                    $resp['status'] = 'true';
                    $resp['msg'] = 'Coupon Applied successfully !!!';
                    //pr($resp);
                    
                }else{
                    $resp['amount'] = '0';
                    $resp['status'] = 'true';
                    $resp['msg'] = 'Coupon Applied successfully !!!';
                    
                }
            }else{
                
                    $resp['amount'] = $order['Order']['amount'] - ($order['Order']['amount']*$coupon['Coupon']['amount']/100);
                    $resp['status'] = 'true';
                    $resp['msg'] = 'Coupon Applied successfully !!!';
                    
                
            }
            $resp['walletAmount'] = $wallet['Wallet']['amount'];
            
            /*if($coupon['Coupon']['coupon_type_id']){

            }else{
                $resp['status'] = 'invalide';
                $resp['msg'] = 'Coupon not valid for this Form !!!';
                echo json_encode($resp);die;
            }*/
            }
        }
        if($data['wallet'] != 'false'){
            //echo "hi";
            
            
            if(!empty($wallet['Wallet']['amount'])){
                if(empty(@$resp['amount'])){
                    if($wallet['Wallet']['amount'] > $order['Order']['amount']){
                        $resp['walletAmount'] = $wallet['Wallet']['amount'] - $order['Order']['amount'];
                        $resp['amount'] = '0';
                        $resp['status'] = 'true';
                        $resp['msg'] = 'successfull';
                    }else{
                        $resp['amount'] = $order['Order']['amount'] - $wallet['Wallet']['amount'];
                        $resp['walletAmount'] = '0';
                        $resp['status'] = 'true';
                        $resp['msg'] = 'successfull';
                    }
                }else{
                    if($wallet['Wallet']['amount'] > $resp['amount']){
                        $resp['walletAmount'] = $wallet['Wallet']['amount'] - $resp['amount'];
                        $resp['amount'] = '0';
                        $resp['status'] = 'true';
                        $resp['msg'] = 'successfull';
                    }else{
                        $resp['amount'] = $resp['amount'] - $wallet['Wallet']['amount'];
                        $resp['walletAmount']='0';
                        $resp['status'] = 'true';
                        $resp['msg'] = 'successfull';
                    }
                }
            }
        }
        echo json_encode($resp);die;
        
    }

    public function walletTransfer(){
        
        $this->loadModel('Wallet');
        $this->loadModel('WalletTransaction');
        $stu = $this->Student->findByEmail($this->data['email']);

        $resp = [];
       /* if(empty($stu['Student']['id'] = $this->Session->read('Student.id'))){
            $resp['status'] = 'true';
            $resp['msg'] = 'You cannot transfer amount to yourself.';
        }*/
        if(empty($stu)){
            $resp['status'] = 'true';
            $resp['msg'] = 'Email doesn"t exist.';
        }else{
            $senderWallet = $this->Wallet->findByStudentId($this->Session->read('Student.id'));
            if($senderWallet['Wallet']['amount'] < $this->data['amount']){
                $resp['status'] = 'true';
                $resp['msg'] = 'Insufficient amount in wallet.';
            }else{
                $receiverWallet = $this->Wallet->findByStudentId($stu['Student']['id']);
                if(empty($receiverWallet)){
                    //die('hi2');
                     $wallet['Wallet']['student_id'] = $stu['Student']['id'];
                     $wallet['Wallet']['amount'] = $this->data['amount'];
                     $wallet['Wallet']['created_date'] = date('Y-m-d H:i:s');
                     $this->Wallet->save($wallet);
                     $walletId = $this->Wallet->getLastInsertId();
                }else{
                    //die('hi');
                    $receiverWallet['Wallet']['amount'] += $this->data['amount'];
                    $this->Wallet->save($receiverWallet);
                    //die('hi');
                    $walletId = $receiverWallet['Wallet']['id'];
                }
                //pr();
                $senderWallet['Wallet']['amount'] -= $this->data['amount'];
                $this->Wallet->save($senderWallet);
                

                $RwalletTranns['WalletTransaction']['wallet_id'] = $walletId;
                $RwalletTranns['WalletTransaction']['created_date'] = date('Y-m-d H:i:s');
                $RwalletTranns['WalletTransaction']['transaction_by'] = 'Automated';
                $RwalletTranns['WalletTransaction']['description'] = 'MembershipPlan wallet to wallet transfer';
                $RwalletTranns['WalletTransaction']['due_to'] = 'MembershipPlan';
                $RwalletTranns['WalletTransaction']['type'] = '0';
                $RwalletTranns['WalletTransaction']['student_id'] = $stu['Student']['id'];
                //pr($RwalletTranns);die;
                $this->WalletTransaction->create();
                $this->WalletTransaction->save($RwalletTranns);

                $SwalletTranns['WalletTransaction']['wallet_id'] = $senderWallet['Wallet']['id'];
                $SwalletTranns['WalletTransaction']['created_date'] = date('Y-m-d H:i:s');
                $SwalletTranns['WalletTransaction']['transaction_by'] = 'Automated';
                $SwalletTranns['WalletTransaction']['description'] = 'MembershipPlan wallet to wallet transfer';
                $SwalletTranns['WalletTransaction']['due_to'] = 'MembershipPlan';
                $SwalletTranns['WalletTransaction']['type'] = '1';
                $SwalletTranns['WalletTransaction']['student_id'] = $this->Session->read('Student.id');
                //pr($walletTranns);die;
                $this->WalletTransaction->create();
                $this->WalletTransaction->save($SwalletTranns);
                $resp['status'] = 'true';
                $resp['msg'] = 'Amount transfer successfully.';
                $resp['amount'] = $senderWallet['Wallet']['amount'];
            }    
        }
        echo json_encode($resp);die;
    }

    public function formApplied(){
        $this->loadModel('Order');
        //$this->loadModel('OrderApplicationForm');
        $orders = $this->Order->findAllByStudentId($this->Session->read('Student.id'));
        //pr($order);
        $this->set(compact('orders'));
    }

    public function viewOrder($orderId = null){
        $orderId = base64_decode($orderId);
        $this->loadModel('Order');
        $this->loadModel('OrderApplicationForm');
        $this->Order->bindModel(
                array('hasMany' => array(
                        'OrderMeta' => array(
                            'className' => 'OrderMeta',
                            'foreignKey' => 'order_id'
                            )
                        )
                    )
            );
        $this->Order->bindModel(
                array('hasMany' => array(
                        'OrderMeta' => array(
                            'className' => 'OrderMeta',
                            'foreignKey' => 'order_id'
                            )
                        )
                    )
            );
        $this->OrderMeta->bindModel(
            array('belongsTo' => array(
                    'College' => array(
                        'className' => 'College',
                        'foreignKey' => 'college_id',
                        'fields' =>'name'
                        )
                    )
                )
            );
        $this->OrderMeta->bindModel(
            array('hasOne' => array(
                    'DirectApplication' => array(
                        'className' => 'DirectApplication',
                        'foreignKey' => 'order_meta_id'
                        )
                    )
                )
            );
        $this->OrderMeta->bindModel(
            array('belongsTo' => array(
                    'CollegeCourse' => array(
                        'className' => 'CollegeCourse',
                        'foreignKey' => 'course_id',
                        'fields' => array('formsadda_cost','registration_fee','fees','id','college_id','direct_apply','link')
                        )
                    )
                )
            );
        $this->Order->bindModel(
            array('hasOne' => array(
                    'OrderApplicationForm' => array(
                        'className' => 'OrderApplicationForm',
                        'foreignKey' => 'order_id',
                        )
                    )
                )
            );
        $this->Order->bindModel(
            array('hasMany' => array(
                    'AfCustomField' => array(
                        'className' => 'AfCustomField',
                        'foreignKey' => 'order_id',
                        'fields' => array('field_name','value')
                        )
                    )
                )
            );
        $this->Order->bindModel(
            array('hasMany' => array(
                    'AfCompetitiveExam' => array(
                        'className' => 'AfCompetitiveExam',
                        'foreignKey' => 'order_id',
                        'fields' => array('name','year','score','percentile')
                        )
                    )
                )
            );
        $this->Order->bindModel(
            array('hasMany' => array(
                    'AfStudentExperience' => array(
                        'className' => 'AfStudentExperience',
                        'foreignKey' => 'order_id',
                        'fields' => array('name','designation','joining','releasing')
                        )
                    )
                )
            );
        $this->Order->bindModel(
            array('hasMany' => array(
                    'AfStudentAcademic' => array(
                        'className' => 'AfStudentAcademic',
                        'foreignKey' => 'order_id',
                        'fields' => array('exam','passing_year','institute','university','percentage')
                        )
                    )
                )
            );
        $this->OrderApplicationForm->bindModel(
            array('belongsTo' => array(
                    'City' => array(
                        'className' => 'City',
                        'foreignKey' => 'city_id',
                        
                        )
                    )
                )
            );
        $this->OrderApplicationForm->bindModel(
            array('belongsTo' => array(
                    'State' => array(
                        'className' => 'State',
                        'foreignKey' => 'state_id',
                        
                        )
                    )
                )
            );
        $this->OrderApplicationForm->bindModel(
            array('belongsTo' => array(
                    'Country' => array(
                        'className' => 'Country',
                        'foreignKey' => 'country_id',
                        
                        )
                    )
                )
            );
        $this->Order->recursive=3;
        $order = $this->Order->findById($orderId);
        //pr($order);die;
        $this->set(compact('order'));
    }
  

    public function saveDirectForm(){
        if($this->request->is('post')){
            $this->loadModel('DirectApplication');
            $this->loadModel('OrderMeta');
            $data= $this->data;
            //pr($data);die;
            if(!empty($data)){
               $i=1;
                foreach($data as $d){
                   if(!empty($d['DirectApplication']['application_no'])){ 
                    $sav = $d;
                   // echo $_FILES['file']['name'][$i];
                        if(!empty($_FILES['file']['name'][$i])){
                           // echo "hi";
                            $destination = realpath('../webroot/img/docs/') . '/';
                            $destination1 = realpath('../webroot/img/docs/') . '/';
                            $image = $this->Components->load('Resize');  

                            if(is_uploaded_file($_FILES['file']['tmp_name'][$i]))
                            {
                                
                                $filenameimg = time().'-'.$_FILES['file']['name'][$i];
                                $image->resize($_FILES['file']['tmp_name'][$i],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                                
                                move_uploaded_file($_FILES['file']['tmp_name'][$i], $destination1 . $filenameimg);
                                $sav['DirectApplication']['image']=$filenameimg;
                                
                                
                            }
                        }
                        //pr($sav);die;
                        if(!empty($sav['DirectApplication']['image']) && !empty($sav['DirectApplication']['application_no'])){

                            $this->OrderMeta->saveField('reciept_status','1');
                        }
                        $sav['DirectApplication']['student_id']=$this->Session->read('Student.id');
                        $this->DirectApplication->create();
                        $this->DirectApplication->save($sav);
                        $i++;
                    }
                }
            }
            $this->redirect(array('action'=>'viewOrder',base64_encode($data['1']['DirectApplication']['order_id'])));
        }
    }  

    public function forgetPassword()
    {
        $this->loadModel('Student');
        $this->loadModel('College');
        
        //pr($this->data);die;
        $email = $this->data['email'];
        $checkEmailStudent = $this->Student->findByEmail($email);
        $checkEmailCollege = $this->College->findByEmail($email);
        if (!empty( $checkEmailStudent) || !empty( $checkEmailCollege)) 
        {
            if(!empty( $checkEmailStudent)){
                $password = $this->RandomStringGenerator(10);
                $encryptPassword = md5($password);
                $this->Student->id = $checkEmailStudent['Student']['id'];
                $this->Student->saveField('password',$encryptPassword);

                $replace = array('{name}','{password}');
                $with = array($checkEmailStudent['Student']['name'],$password);
                $this->send_email('',$replace,$with,'front_forgot_password',INFO_FORMSADDA,$checkEmailStudent['Student']['email']);
                $resp['status'] = 'true';
                $resp['msg'] = 'New password has been sent, Please check your registered email.';
            }
            if(!empty( $checkEmailCollege)){
                $password = $this->RandomStringGenerator(10);
                $encryptPassword = md5($password);
                $this->College->id = $checkEmailCollege['College']['id'];
                $this->College->saveField('password',$password);

                $replace = array('{name}','{password}');
                $with = array($checkEmailCollege['College']['name'],$encryptPassword);
                $this->send_email('',$replace,$with,'front_forgot_password',INFO_FORMSADDA,$checkEmailCollege['College']['email']);
                $resp['status'] = 'true';
                $resp['msg'] = 'New password has been sent, Please check your registered email.';
            }

        } else {
            $resp['status'] = 'false';
            $resp['msg'] = 'Email does not exist.';
        }
        echo json_encode($resp);die;
    }

    public function saveCompare(){
        $this->loadModel('Compare');
        $this->loadModel('CollegeCompare');
        $comp = $this->Session->read('clgCompare');
        //pr($comp);die;
        $compare['Compare']['student_id'] = $this->Session->read('Student.id');
        $compare['Compare']['created_date'] = date('Y-m-d H:i:s');
        $this->Compare->save($compare);
        $compareId = $this->Compare->getLastInsertId();
        foreach($comp as $c){
            $clgcomp['CollegeCompare']['college_id'] = $c;
            $clgcomp['CollegeCompare']['compare_id'] = $compareId;
            $clgcomp['CollegeCompare']['student_id'] = $this->Session->read('Student.id');
            $compare['CollegeCompare']['created_date'] = date('Y-m-d H:i:s');
            $this->CollegeCompare->create();
            $this->CollegeCompare->save($clgcomp);
        }
        $this->redirect(array('action'=>'compare'));
    }

    public function compareList(){
        $this->loadModel('Compare');
        $this->loadModel('CollegeCompare');
        $this->Compare->bindModel(
        array('hasMany' => array(
                'CollegeCompare' => array(
                    'className' => 'CollegeCompare',
                    'foreignKey' => 'compare_id',
                    'fields' => array('id'),
                    'conditions' => array('student_id' => $this->Session->read('Student.id'))
                    )
                )
            )
        );
        $comps = $this->Compare->find('all', array('conditions'=>array('Compare.student_id'=>$this->Session->read('Student.id'))));
        $this->set(compact('comps'));
        //echo $this->Session->read('Student.id');
        //pr($comps);die;
    }

    public function newCompare($compareId = null){
        $compareId = base64_decode($compareId);
        $this->loadModel('CollegeCompare');
        $col = $this->CollegeCompare->find('list',array('conditions'=>array('compare_id'=>$compareId),'fields'=>'college_id'));
        $this->Session->write('clgCompare',$col);
        $this->redirect(array('action'=>'compare'));
    }
    public function contact(){
        //$this->layout="home";
        $this->loadModel('Contact');
        if($this->request->is('post')){
            $data = $this->request->data;
            $this->Contact->save($data);
            $this->Session->write('frontsuccess-msg','We shall contact you very soon!!');
            $this->redirect(array('action'=>'contact'));
        }
    }
    public function whygetcollege(){
        $this->layout="newhome";
    }
    public function careerget(){
        $this->layout="newhome";
    }
    public function addTodolist(){
        $this->layout="newhome";
        $this->loadModel('ToDoList');
        if($this->request->is('post')){
            $data = $this->request->data;
            $data['ToDoList']['work_date'] = date('Y-m-d', strtotime($data['ToDoList']['work_date']));
            $data['ToDoList']['work_time'] = $data['ToDoList']['work_time_hour'].':'.$data['ToDoList']['work_time_minute'].':'.'00';
            $data['ToDoList']['status'] = 0;
            $data['ToDoList']['student_id'] = $this->Session->read('Student.id');
            //pr($data);die;
            $this->ToDoList->save($data);
            $this->redirect(array('action'=>'studentdashboard'));
        }
    }
    public function editTodolist(){
        $this->layout="newhome";
        $this->loadModel('ToDoList');
        if($this->request->is('post')){
            $data = $this->request->data;
            $data['ToDoList']['work_date'] = date('Y-m-d', strtotime($data['ToDoList']['work_date']));
            $data['ToDoList']['work_time'] = $data['ToDoList']['work_time_hour'].':'.$data['ToDoList']['work_time_minute'].':'.'00';
            //$data['ToDoList']['status'] = 0;
            $data['ToDoList']['student_id'] = $this->Session->read('Student.id');
            //pr($data);die;
            $this->ToDoList->save($data);
            $this->redirect(array('action'=>'studentdashboard'));
        }
    }
    public function todoDelete($id=null){
        //$this->layout="home";
        $this->loadModel('ToDoList');
        $id = base64_decode($id);
        $this->ToDoList->delete($id);
        echo "true";die;
        //$this->redirect($this->referer());
    }
    public function find_todo($id=null){
        $this->loadModel('ToDoList');
        $id = base64_decode($id);
        $data = $this->ToDoList->findById($id);
        $time = explode(":",$data['ToDoList']['work_time']);
        $data['ToDoList']['date'] = date('Y/m/d', strtotime($data['ToDoList']['work_date']));
        $data['ToDoList']['hour'] = (int)$time[0];
        $data['ToDoList']['min'] = (int)$time[1];
        echo json_encode($data);die;
    }
    public function todo_changestatus($id=null){
        $this->layout="home";
        $this->loadModel('ToDoList');
        $ids=explode("," ,$id);
        //echo '<pre>';print_r($ids);die;
        foreach ($ids as $id) {
            $this->ToDoList->id = $id;
            $this->ToDoList->saveField('status',1);
        }
        $this->redirect(array('action'=>'studentdashboard'));
    }
    public function send_mail_by_student(){
        $this->loadModel('SendMail');
        if($this->request->is('post')){
            $data = $this->request->data;
            $savedata['SendMail']['sender_id'] = $this->Session->read('Student.id');
            $savedata['SendMail']['receiver_id'] = $data['to'];
            $savedata['SendMail']['subject'] = $data['subject'];
            $savedata['SendMail']['message'] = $data['message'];
            $savedata['SendMail']['created_date'] = date('Y-m-d H:i:s');
            $this->SendMail->save($savedata);
            $Email = new CakeEmail();

            $Email->emailFormat('html')
              ->from($this->Session->read('Student.email'))
              ->to('info@getcollege.in')
              ->subject($data['subject'])
              ->send($data['message']);
            $this->Session->write('frontsuccess-msg','Your mail sent successfully.');
            $this->redirect(array('action'=>'studentdashboard'));
        }
    }

    public function testInstruction($testType=null,$test_id=null){
        $this->loadModel('TestLevel');
        $this->loadModel('StudentAnswer');
        $testId = base64_decode($test_id);
        $checkTest = $this->Student->findById($this->Session->read('Student.id'));
        //pr($checkTest);die;
        if($testType == 'practise'){
            if($checkTest['Student']['practise_take_test'] == 1){
                $testTaken  =true;
                //pr($score);die;
                $this->set(compact("testTaken",'checkTest'));
            }
            $testIds = $this->StudentAnswer->find('list',array('conditions'=>array('student_id'=>$this->Session->read('Student.id')),'fields'=>'test_id'));
            $this->TestLevel->bindModel(
            array('hasOne' => array(
                    'Test' => array(
                        'className' => 'Test',
                        'foreignKey' => 'test_level_id',
                        'order'=>'rand()',
                        'conditions' => array('Test.id !=' => $testIds)
                        )
                    )
                )
            );
            $test = $this->TestLevel->find('first',array('conditions'=>array('level_order'=>0)));
        }
        else if($testType == 'level1'){
            if($checkTest['Student']['level_1'] == 1){
                $testTaken  =true;
                $this->StudentAnswer->bindModel(
                array('belongsTo' => array(
                        'Test' => array(
                            'className' => 'Test',
                            'foreignKey' => 'test_id',
                            )
                        )
                    )
                );
                $score = $this->StudentAnswer->findAllByStudentId($this->Session->read('Student.id'));
                //pr($score);die;
                $this->set(compact("testTaken",'score','checkTest'));
                $this->TestLevel->bindModel(
                array('hasOne' => array(
                        'Test' => array(
                            'className' => 'Test',
                            'foreignKey' => 'test_level_id',
                            'conditions' => array('Test.id' => $testId)
                            )
                        )
                    )
                );
                $test = $this->TestLevel->find('first',array('conditions'=>array('level_order'=>1)));
            }
            else{
                //$testIds = $this->StudentAnswer->find('list',array('conditions'=>array('student_id'=>$this->Session->read('Student.id')),'fields'=>'test_id'));
                $this->TestLevel->bindModel(
                array('hasOne' => array(
                        'Test' => array(
                            'className' => 'Test',
                            'foreignKey' => 'test_level_id',
                            'conditions' => array('Test.id' => $testId)
                            )
                        )
                    )
                );
                $test = $this->TestLevel->find('first',array('conditions'=>array('level_order'=>1)));
            }
            
        }
        else if($testType == 'level2'){
            if($checkTest['Student']['level_2'] == 1){
                $testTaken  =true;
                $this->StudentAnswer->bindModel(
                array('belongsTo' => array(
                        'Test' => array(
                            'className' => 'Test',
                            'foreignKey' => 'test_id',
                            )
                        )
                    )
                );
                $score = $this->StudentAnswer->findAllByStudentId($this->Session->read('Student.id'));
                //pr($score);die;
                $this->set(compact("testTaken",'score','checkTest'));
                $this->TestLevel->bindModel(
                array('hasOne' => array(
                        'Test' => array(
                            'className' => 'Test',
                            'foreignKey' => 'test_level_id',
                            'conditions' => array('Test.id' => $testId)
                            )
                        )
                    )
                );
                $test = $this->TestLevel->find('first',array('conditions'=>array('level_order'=>2)));
            }
            else{
                //$testIds = $this->StudentAnswer->find('list',array('conditions'=>array('student_id'=>$this->Session->read('Student.id')),'fields'=>'test_id'));
                $this->TestLevel->bindModel(
                array('hasOne' => array(
                        'Test' => array(
                            'className' => 'Test',
                            'foreignKey' => 'test_level_id',
                            'conditions' => array('Test.id' => $testId)
                            )
                        )
                    )
                );
                $test = $this->TestLevel->find('first',array('conditions'=>array('level_order'=>2)));
            }
            
        }
        else if($testType == 'level3'){
            if($checkTest['Student']['level_3'] == 1){
                $testTaken  =true;
                $this->StudentAnswer->bindModel(
                array('belongsTo' => array(
                        'Test' => array(
                            'className' => 'Test',
                            'foreignKey' => 'test_id',
                            )
                        )
                    )
                );
                $score = $this->StudentAnswer->findAllByStudentId($this->Session->read('Student.id'));
                //pr($score);die;
                $this->set(compact("testTaken",'score','checkTest'));
                $this->TestLevel->bindModel(
                array('hasOne' => array(
                        'Test' => array(
                            'className' => 'Test',
                            'foreignKey' => 'test_level_id',
                            'conditions' => array('Test.id' => $testId)
                            )
                        )
                    )
                );
                $test = $this->TestLevel->find('first',array('conditions'=>array('level_order'=>3)));
            }
            else{
                //$testIds = $this->StudentAnswer->find('list',array('conditions'=>array('student_id'=>$this->Session->read('Student.id')),'fields'=>'test_id'));
                $this->TestLevel->bindModel(
                array('hasOne' => array(
                        'Test' => array(
                            'className' => 'Test',
                            'foreignKey' => 'test_level_id',
                            'conditions' => array('Test.id' => $testId)
                            )
                        )
                    )
                );
                $test = $this->TestLevel->find('first',array('conditions'=>array('level_order'=>3)));
            }
            
        }

        

        $this->set(compact('test','testType'));
    }

    public function test($testId = null,$testType=null){
        $this->layout='';
        $this->loadModel('StudentAnswer');
        $this->loadModel('Test');
        $this->loadModel('TestSection');
        $this->loadModel('TestLevel');
        $this->loadModel('TestQuestion');
        $this->Test->bindModel(
        array('hasMany' => array(
                'TestSection' => array(
                    'className' => 'TestSection',
                    'foreignKey' => 'test_id',
                    )
                )
            )
        );
        $this->TestSection->bindModel(
        array('hasMany' => array(
                'TestQuestion' => array(
                    'className' => 'TestQuestion',
                    'foreignKey' => 'test_section_id',
                    'order' => 'rand()',
                    'limit' => '30',
                    'conditions' => array('status' => 1)
                    )
                )
            )
        );
        $this->TestQuestion->bindModel(
        array('hasMany' => array(
                'TestQuestionOption' => array(
                    'className' => 'TestQuestionOption',
                    'foreignKey' => 'test_question_id',
                    
                    )
                )
            )
        );
        $this->Test->recursive = 4;

        $test = $this->Test->findById(base64_decode($testId));
        $checkTest = $this->Student->findById($this->Session->read('Student.id'));
        if($testType == 'practise'){
            if($checkTest['Student']['practise_take_test'] != '1'){
                $testlevel = $this->TestLevel->findById($test['Test']['test_level_id']);
               // pr($test);die;
                    $crt['StudentAnswer']['all_data'] = 0;
                    $crt['StudentAnswer']['test_level'] = $testlevel['TestLevel']['level_order'];
                    $crt['StudentAnswer']['test_id'] = base64_decode($testId);
                    $crt['StudentAnswer']['question_attempt'] =  0;
                    $crt['StudentAnswer']['question_leave'] = 0;
                    $crt['StudentAnswer']['total_question'] = 0;
                    $crt['StudentAnswer']['question_wrong'] = 0;
                    $crt['StudentAnswer']['question_correct'] = 0;
                    $crt['StudentAnswer']['total_mark_obtain'] = 0;
                    $crt['StudentAnswer']['total_marks'] = 0;
                    $crt['StudentAnswer']['percentage']  = 0;
                    $crt['StudentAnswer']['student_id'] = 0;
                    $crt['StudentAnswer']['created_date'] = date('Y-m-d H:i:s');
                    
                    $this->StudentAnswer->save($crt);
                    $getId = $this->StudentAnswer->getLastInsertId();
                
                $this->Student->id = $this->Session->read('Student.id');
                $this->Student->saveField('practise_take_test',1);
            }
        }
        else if($testType == 'level1'){
            if($checkTest['Student']['level_1'] != '1'){
                $testlevel = $this->TestLevel->findById($test['Test']['test_level_id']);
               // pr($test);die;
                    $crt['StudentAnswer']['all_data'] = 0;
                    $crt['StudentAnswer']['test_level'] = $testlevel['TestLevel']['level_order'];
                    $crt['StudentAnswer']['test_id'] = base64_decode($testId);
                    $crt['StudentAnswer']['question_attempt'] =  0;
                    $crt['StudentAnswer']['question_leave'] = 0;
                    $crt['StudentAnswer']['total_question'] = 0;
                    $crt['StudentAnswer']['question_wrong'] = 0;
                    $crt['StudentAnswer']['question_correct'] = 0;
                    $crt['StudentAnswer']['total_mark_obtain'] = 0;
                    $crt['StudentAnswer']['total_marks'] = 0;
                    $crt['StudentAnswer']['percentage']  = 0;
                    $crt['StudentAnswer']['student_id'] = 0;
                    $crt['StudentAnswer']['created_date'] = date('Y-m-d H:i:s');
                    
                    $this->StudentAnswer->save($crt);
                    $getId = $this->StudentAnswer->getLastInsertId();
                
                $this->Student->id = $this->Session->read('Student.id');
                $this->Student->saveField('level_1',1);
                $this->Student->updateAll(
                                                array('Student.test_taken' => 'Student.test_taken + 1'),
                                                array('Student.id' => $this->Session->read('Student.id'))
                                            );
            }

        }
        else if($testType == 'level2'){
            if($checkTest['Student']['level_2'] != '1'){
                $testlevel = $this->TestLevel->findById($test['Test']['test_level_id']);
               // pr($test);die;
                    $crt['StudentAnswer']['all_data'] = 0;
                    $crt['StudentAnswer']['test_level'] = $testlevel['TestLevel']['level_order'];
                    $crt['StudentAnswer']['test_id'] = base64_decode($testId);
                    $crt['StudentAnswer']['question_attempt'] =  0;
                    $crt['StudentAnswer']['question_leave'] = 0;
                    $crt['StudentAnswer']['total_question'] = 0;
                    $crt['StudentAnswer']['question_wrong'] = 0;
                    $crt['StudentAnswer']['question_correct'] = 0;
                    $crt['StudentAnswer']['total_mark_obtain'] = 0;
                    $crt['StudentAnswer']['total_marks'] = 0;
                    $crt['StudentAnswer']['percentage']  = 0;
                    $crt['StudentAnswer']['student_id'] = 0;
                    $crt['StudentAnswer']['created_date'] = date('Y-m-d H:i:s');
                    
                    $this->StudentAnswer->save($crt);
                    $getId = $this->StudentAnswer->getLastInsertId();
                
                $this->Student->id = $this->Session->read('Student.id');
                $this->Student->saveField('level_2',1);
                $this->Student->updateAll(
                                                array('Student.test_taken_level2' => 'Student.test_taken_level2 + 1'),
                                                array('Student.id' => $this->Session->read('Student.id'))
                                            );
            }
        }
        else if($testType == 'level3'){
            if($checkTest['Student']['level_3'] != '1'){
                $testlevel = $this->TestLevel->findById($test['Test']['test_level_id']);
               // pr($test);die;
                    $crt['StudentAnswer']['all_data'] = 0;
                    $crt['StudentAnswer']['test_level'] = $testlevel['TestLevel']['level_order'];
                    $crt['StudentAnswer']['test_id'] = base64_decode($testId);
                    $crt['StudentAnswer']['question_attempt'] =  0;
                    $crt['StudentAnswer']['question_leave'] = 0;
                    $crt['StudentAnswer']['total_question'] = 0;
                    $crt['StudentAnswer']['question_wrong'] = 0;
                    $crt['StudentAnswer']['question_correct'] = 0;
                    $crt['StudentAnswer']['total_mark_obtain'] = 0;
                    $crt['StudentAnswer']['total_marks'] = 0;
                    $crt['StudentAnswer']['percentage']  = 0;
                    $crt['StudentAnswer']['student_id'] = 0;
                    $crt['StudentAnswer']['created_date'] = date('Y-m-d H:i:s');
                    
                    $this->StudentAnswer->save($crt);
                    $getId = $this->StudentAnswer->getLastInsertId();
                
                $this->Student->id = $this->Session->read('Student.id');
                $this->Student->saveField('level_3',1);
                $this->Student->updateAll(
                                                array('Student.test_taken_level3' => 'Student.test_taken_level3 + 1'),
                                                array('Student.id' => $this->Session->read('Student.id'))
                                            );
            }
        }
        
        //pr($test);die;
        $this->set(compact('test','getId'));
        if($this->request->is('post')){
           // pr($this->data);//die;
            $attempt = 0;
            $leave = 0;
            $totalQue = count($this->data['question']);
            $wrong = 0;
            $right = 0;
            $totalMarksObtain = 0;
            $totalMarks = $totalQue*2;
            foreach($this->data['question'] as $chk){
                if(!empty($chk['user_ans'])){
                    $attempt++;
                    $this->TestQuestion->updateAll(
                                            array('TestQuestion.question_attempted' => 'TestQuestion.question_attempted + 1'),
                                            array('TestQuestion.id' => $chk['qId'])
                                        );
                    if($chk['user_ans'] == base64_decode($chk['check'])){
                        $right++;
                        $totalMarksObtain +=2;
                        $this->TestQuestion->updateAll(
                                            array('TestQuestion.question_corrected' => 'TestQuestion.question_corrected + 1'),
                                            array('TestQuestion.id' => $chk['qId'])
                                        );
                    }else{
                        $wrong++;
                        $totalMarksObtain -=0.5;
                        $this->TestQuestion->updateAll(
                                            array('TestQuestion.question_wrong' => 'TestQuestion.question_wrong + 1'),
                                            array('TestQuestion.id' => $chk['qId'])
                                        );
                    }
                }else{
                    $leave++;
                    $this->TestQuestion->updateAll(
                                            array('TestQuestion.question_leave' => 'TestQuestion.question_leave + 1'),
                                            array('TestQuestion.id' => $chk['qId'])
                                        );
                }
            }
            $store['StudentAnswer']['all_data'] = json_encode($this->data['question']);
            $store['StudentAnswer']['test_id'] = $this->data['test']['testId'];
            $store['StudentAnswer']['id'] = $this->data['test']['rowId'];
            $store['StudentAnswer']['question_attempt'] =  $attempt;
            $store['StudentAnswer']['question_leave'] = $leave;
            $store['StudentAnswer']['total_question'] = $totalQue;
            $store['StudentAnswer']['question_wrong'] = $wrong;
            $store['StudentAnswer']['question_correct'] = $right;
            $store['StudentAnswer']['total_mark_obtain'] = $totalMarksObtain;
            $store['StudentAnswer']['total_marks'] = $totalMarks;
            $store['StudentAnswer']['percentage']  = ($totalMarksObtain*100)/$totalMarks;
            $store['StudentAnswer']['student_id'] = $this->Session->read('Student.id');
            $store['StudentAnswer']['created_date'] = date('Y-m-d H:i:s');
            
            $this->StudentAnswer->save($store);
           echo true; die;
            $this->redirect(array('action'=>'studentdashboard'));
            
        }
    }

    public function testAgain(){
        $this->Student->id = $this->Session->read('Student.id');
        $this->Student->saveField('level_1',0);
        $this->redirect(array('action'=>'testInstruction'));
    }
    public function testScore(){
        $this->loadModel('StudentAnswer');
        $this->StudentAnswer->bindModel(
            array('belongsTo' => array(
                    'Test' => array(
                        'className' => 'Test',
                        'foreignKey' => 'test_id',
                        )
                    )
                )
            );
            $score = $this->StudentAnswer->findAllByStudentId($this->Session->read('Student.id'));
            //pr($score);die;
            $this->set(compact('score'));
    }

    public function payForTest(){
     //echo "payment gatway";die; 
        /*if($this->request->is('post')){
            $data = $this->data;
            $paramList["MID"] = 'Buildr96903791066370';
            $paramList["ORDER_ID"] = $data['Order']['ORDER_ID'];
            $paramList["CUST_ID"] = $this->Session->read('Student.id');
            $paramList["INDUSTRY_TYPE_ID"] = 'Retail';
            $paramList["CHANNEL_ID"] = 'WEB';
            $paramList["TXN_AMOUNT"] = 50;
            $paramList["WEBSITE"] = 'WEB_STAGING';

            $checkSum = $this->getChecksumFromArray($paramList,'Cc2ubg6Q5TJPQ_kA');

            $this->set(compact('paramList','checkSum'));
            $this->redirect(array('action'=>'paynow',$paramList,$checkSum));
        }*/
    }
    public function paynow($paramList=array(), $checkSum=null){
        $this->layout='';
        if($this->request->is('post')){
            $data = $this->data;
            $paramList["MID"] = 'Buildr96903791066370';
            $paramList["ORDER_ID"] = $data['Order']['ORDER_ID'];
            $paramList["CUST_ID"] = $this->Session->read('Student.id');
            $paramList["INDUSTRY_TYPE_ID"] = 'Retail';
            $paramList["CHANNEL_ID"] = 'WEB';
            $paramList["TXN_AMOUNT"] = 1;
            $paramList["WEBSITE"] = 'WEB_STAGING';

            $checkSum = $this->getChecksumFromArray($paramList,'Cc2ubg6Q5TJPQ_kA');

            $this->set(compact('paramList','checkSum'));
            //$this->redirect(array('action'=>'paynow',$paramList,$checkSum));
        }
        //$this->set(compact('paramList','checkSum'));
    }
    public function paymentError(){
        echo 'Unsuccessfull Payment !!! <a href='.HTTP_ROOT.'>Click here for Home page.</a>';die;
    }
    public function testDetail(){
        $this->loadModel('TestLevel');
        $this->loadModel('Test');
        $this->loadModel('Student');
        $this->loadModel('StudentAnswer');
        
        $score = $this->StudentAnswer->findAllByStudentId($this->Session->read('Student.id'));

        $std = $this->Student->findById($this->Session->read('Student.id'));

        $testLevel1 = $this->TestLevel->find('first',array('conditions'=>array('level_order'=>1, 'status' => 1)));

        $testLevel2 = $this->TestLevel->find('first',array('conditions'=>array('level_order'=>2, 'status' => 1)));

        $testLevel3 = $this->TestLevel->find('first',array('conditions'=>array('level_order'=>3, 'status' => 1)));
        $this->set(compact('testLevel1','testLevel2','testLevel3','std','score'));
    }

    public function referSchool(){
        $this->loadModel('ReferSchool');
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            if($this->Session->check('Student')){
                $data['ReferSchool']['student_id'] = $this->Session->read('Student.id');
            }
            $data['ReferSchool']['created_date'] = date('Y-m-d H:i:s');
            $this->ReferSchool->save($data);
            $this->redirect($this->referer());
        }
    }
}
