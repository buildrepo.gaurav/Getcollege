<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	function RandomStringGenerator($length = null)
    { 
      //print_r($length);die;
      $string = "";   
      $pattern = "abcdefgh1234567890ijklmnopqrst1234567890uvwxyzABCDEFGHI1234567890JKLMNOPQRSTUVWXYZ1234567890";
        for($i=0; $i<$length; $i++)
          {
               $string .= $pattern{rand(0,61)};
        }
        return $string;
    }
  function CapitalRandomStringGenerator($length = null)
    { 
      //print_r($length);die;
      $string = "";   
      $pattern = "123ABCDEFGHIJK456LMNOPQRS789TUVWXYZ987ZYXWVUT654SRQPONML321KJIHGFEDCBA";
        for($i=0; $i<$length; $i++)
          {
               $string .= $pattern{rand(0,61)};
        }
        return $string;
    }   

  function send_email($process = "",$replace_fields=array(),$replace_with=array(),$email_template=null,$from=null,$to=null)
    {
      $this->loadModel('EmailTemplate');
      
      $template=$this->EmailTemplate->find("first",array("conditions"=>array('EmailTemplate.alias'=>$email_template)));
      $template_data=$template['EmailTemplate']['description']; 
      $template_info=str_replace($replace_fields,$replace_with,$template_data);
      
      if($_SERVER['HTTP_HOST']=='localhost')
      {
        pr($template_info);die;
      }
      
      $this->set('data',$template_info);
      $this->Email->to = $to;
      $this->Email->subject = $template['EmailTemplate']['subject'];
      $this->Email->from = $from;
      //$this->Email->attachments = $from;
      $this->Email->template = 'email_template';
      //pr($this->Email);die;
      $this->Email->sendAs = 'both'; 

      ob_start();
      $this->Email->send();
      ob_end_clean();
    }





    ////Paytm gateway Payment function
    function encrypt_e($input, $ky) {
      $key = $ky;
      $size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
      $input = $this->pkcs5_pad_e($input, $size);
      $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
      $iv = "@@@@&&&&####$$$$";
      mcrypt_generic_init($td, $key, $iv);
      $data = mcrypt_generic($td, $input);
      mcrypt_generic_deinit($td);
      mcrypt_module_close($td);
      $data = base64_encode($data);
      return $data;
    }

    function decrypt_e($crypt, $ky) {

      $crypt = base64_decode($crypt);
      $key = $ky;
      $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
      $iv = "@@@@&&&&####$$$$";
      mcrypt_generic_init($td, $key, $iv);
      $decrypted_data = mdecrypt_generic($td, $crypt);
      mcrypt_generic_deinit($td);
      mcrypt_module_close($td);
      $decrypted_data = $this->pkcs5_unpad_e($decrypted_data);
      $decrypted_data = rtrim($decrypted_data);
      return $decrypted_data;
    }

    function pkcs5_pad_e($text, $blocksize) {
      $pad = $blocksize - (strlen($text) % $blocksize);
      return $text . str_repeat(chr($pad), $pad);
    }

    function pkcs5_unpad_e($text) {
      $pad = ord($text{strlen($text) - 1});
      if ($pad > strlen($text))
        return false;
      return substr($text, 0, -1 * $pad);
    }

    function generateSalt_e($length) {
      $random = "";
      srand((double) microtime() * 1000000);

      $data = "AbcDE123IJKLMN67QRSTUVWXYZ";
      $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
      $data .= "0FGH45OP89";

      for ($i = 0; $i < $length; $i++) {
        $random .= substr($data, (rand() % (strlen($data))), 1);
      }

      return $random;
    }

    function checkString_e($value) {
      if ($value == 'null')
        $value = '';
      return $value;
    }

    function getChecksumFromArray($arrayList, $key, $sort=1) {
      if ($sort != 0) {
        ksort($arrayList);
      }
      $str = $this->getArray2Str($arrayList);
      $salt = $this->generateSalt_e(4);
      $finalString = $str . "|" . $salt;
      $hash = hash("sha256", $finalString);
      $hashString = $hash . $salt;
      $checksum = $this->encrypt_e($hashString, $key);
      return $checksum;
    }
    function getChecksumFromString($str, $key) {
      
      $salt = $this->generateSalt_e(4);
      $finalString = $str . "|" . $salt;
      $hash = hash("sha256", $finalString);
      $hashString = $hash . $salt;
      $checksum = $this->encrypt_e($hashString, $key);
      return $checksum;
    }

    function verifychecksum_e($arrayList, $key, $checksumvalue) {
      $arrayList = $this->removeCheckSumParam($arrayList);
      ksort($arrayList);
      $str = $this->getArray2Str($arrayList);
      $paytm_hash = $this->decrypt_e($checksumvalue, $key);
      $salt = substr($paytm_hash, -4);

      $finalString = $str . "|" . $salt;

      $website_hash = hash("sha256", $finalString);
      $website_hash .= $salt;

      $validFlag = "FALSE";
      if ($website_hash == $paytm_hash) {
        $validFlag = "TRUE";
      } else {
        $validFlag = "FALSE";
      }
      return $validFlag;
    }

    function verifychecksum_eFromStr($str, $key, $checksumvalue) {
      $paytm_hash = $this->decrypt_e($checksumvalue, $key);
      $salt = substr($paytm_hash, -4);

      $finalString = $str . "|" . $salt;

      $website_hash = hash("sha256", $finalString);
      $website_hash .= $salt;

      $validFlag = "FALSE";
      if ($website_hash == $paytm_hash) {
        $validFlag = "TRUE";
      } else {
        $validFlag = "FALSE";
      }
      return $validFlag;
    }

    function getArray2Str($arrayList) {
      $findme   = 'REFUND';
      $findmepipe = '|';
      $paramStr = "";
      $flag = 1;  
      foreach ($arrayList as $key => $value) {
        $pos = strpos($value, $findme);
        $pospipe = strpos($value, $findmepipe);
        if ($pos !== false || $pospipe !== false) 
        {
          continue;
        }
        
        if ($flag) {
          $paramStr .= $this->checkString_e($value);
          $flag = 0;
        } else {
          $paramStr .= "|" . $this->checkString_e($value);
        }
      }
      return $paramStr;
    }

    function redirect2PG($paramList, $key) {
      $hashString = $this->getchecksumFromArray($paramList);
      $checksum = $this->encrypt_e($hashString, $key);
    }

    function removeCheckSumParam($arrayList) {
      if (isset($arrayList["CHECKSUMHASH"])) {
        unset($arrayList["CHECKSUMHASH"]);
      }
      return $arrayList;
    }

    function getTxnStatus($requestParamList) {
      return callAPI(PAYTM_STATUS_QUERY_URL, $requestParamList);
    }

    function getTxnStatusNew($requestParamList) {
      return callNewAPI(PAYTM_STATUS_QUERY_NEW_URL, $requestParamList);
    }

    function initiateTxnRefund($requestParamList) {
      $CHECKSUM = $this->getRefundChecksumFromArray($requestParamList,PAYTM_MERCHANT_KEY,0);
      $requestParamList["CHECKSUM"] = $CHECKSUM;
      return $this->callAPI(PAYTM_REFUND_URL, $requestParamList);
    }

    function callAPI($apiURL, $requestParamList) {
      $jsonResponse = "";
      $responseParamList = array();
      $JsonData =json_encode($requestParamList);
      $postData = 'JsonData='.urlencode($JsonData);
      $ch = curl_init($apiURL);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);                                                                  
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
      curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                         
      'Content-Type: application/json', 
      'Content-Length: ' . strlen($postData))                                                                       
      );  
      $jsonResponse = curl_exec($ch);   
      $responseParamList = json_decode($jsonResponse,true);
      return $responseParamList;
    }

    function callNewAPI($apiURL, $requestParamList) {
      $jsonResponse = "";
      $responseParamList = array();
      $JsonData =json_encode($requestParamList);
      $postData = 'JsonData='.urlencode($JsonData);
      $ch = curl_init($apiURL);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);                                                                  
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
      curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                         
      'Content-Type: application/json', 
      'Content-Length: ' . strlen($postData))                                                                       
      );  
      $jsonResponse = curl_exec($ch);   
      $responseParamList = json_decode($jsonResponse,true);
      return $responseParamList;
    }
    function getRefundChecksumFromArray($arrayList, $key, $sort=1) {
      if ($sort != 0) {
        ksort($arrayList);
      }
      $str = $this->getRefundArray2Str($arrayList);
      $salt = $this->generateSalt_e(4);
      $finalString = $str . "|" . $salt;
      $hash = hash("sha256", $finalString);
      $hashString = $hash . $salt;
      $checksum = $this->encrypt_e($hashString, $key);
      return $checksum;
    }
    function getRefundArray2Str($arrayList) { 
      $findmepipe = '|';
      $paramStr = "";
      $flag = 1;  
      foreach ($arrayList as $key => $value) {    
        $pospipe = strpos($value, $findmepipe);
        if ($pospipe !== false) 
        {
          continue;
        }
        
        if ($flag) {
          $paramStr .= $this->checkString_e($value);
          $flag = 0;
        } else {
          $paramStr .= "|" . $this->checkString_e($value);
        }
      }
      return $paramStr;
    }
    function callRefundAPI($refundApiURL, $requestParamList) {
      $jsonResponse = "";
      $responseParamList = array();
      $JsonData =json_encode($requestParamList);
      $postData = 'JsonData='.urlencode($JsonData);
      $ch = curl_init($apiURL); 
      curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_URL, $refundApiURL);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);  
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
      $headers = array();
      $headers[] = 'Content-Type: application/json';
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);  
      $jsonResponse = curl_exec($ch);   
      $responseParamList = json_decode($jsonResponse,true);
      return $responseParamList;
    }


////end Paytm gateway

}
