<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class TestAdminController extends AppController {
	
	public $components = array('Session','Email','RequestHandler','Cookie','Paginator');
	public $helpers = array('Session','Html');
    var $layout = 'admin';
    var $uses = array('Admin','Notify','TestLevel','Test','TestSection','TestQuestion','TestQuestionOption');
    public function beforeFilter()
    {
        parent::beforeFilter();
        $actions = array('login','forgetPassword','findState','findCity');
        if(!in_array($this->params['action'], $actions))
        {
            if(!$this->Session->check('Admin') )
            {
            	$this->redirect(array('controller'=>'Admins','action'=>'login'));
        	}
   		}/*else{
   			if($this->Session->check('Admin'))
            {
            	$this->redirect(array('action'=>'dashboard'));
        	}
   		}*/

        $adminDetails = $this->Admin->findById($this->Session->read('Admin.id'));
        $this->set(compact('adminDetails'));

        $subadminBlockedAction = array('manageSubadmin','editsubadmin','addSubadmin','deletesubadmin');

        if(!empty($adminDetails['Admin']['type']) && $adminDetails['Admin']['type'] == '1')
        {
            if(in_array($this->params['action'], $subadminBlockedAction))
                {
                    $this->Session->write('adminerror-msg','You are not authorized to view this page!!!');
                    $this->redirect(array('action'=>'dashboard'));
                 
                }   
        }
  

    }

    public function ManageTestLevel($levelId = null){
        $levels=$this->TestLevel->find('all');
        //pr($streams);die;
        if(!empty($levelId)){
            $level = $this->TestLevel->findById($levelId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('levels','level','levelId'));
        if($this->request->is('post')){
            $data = $this->data;
            
            foreach ($data as $val) {
                if(!empty($val['TestLevel']['id'])){
                   /* $val['TestLevel']['updated_by'] = $this->Session->read('Admin.id');
                    $val['TestLevel']['update_date'] = date('Y-m-d H:i:s');*/
                }else{
                    //$val['Stream']['created_by'] = $this->Session->read('Admin.id');
                }
                $val['TestLevel']['created_date'] = date('Y-m-d H:i:s');
                $this->TestLevel->create();
               // pr($val);die;
                try{
                    if($this->TestLevel->save($val)){
                        $this->Session->write('adminsuccess-msg','TestLevel added successfully.');
                    }else{
                        $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    }
                }
                catch(Exception $e){
                    //echo $e;die;
                        $this->Session->write('adminerror-msg','Duplicate entry for '.$val['TestLevel']['level'].' not allowed!!!');
                        $this->redirect(array('action' => 'ManageTestLevel'));
                    }      
            }
            
            $this->redirect(array('action' => 'ManageTestLevel'));
           
            
        } 
    }

    public function ManageTest($testId = null){
        $this->Test->bindModel(
        array('belongsTo' => array(
                'TestLevel' => array(
                    'className' => 'TestLevel',
                    'foreignKey' => 'test_level_id'
                    )
                )
            )
        );
        $tests=$this->Test->find('all');
        $levels=$this->TestLevel->find('all',array('conditions'=>array('status'=>'1','is_delete'=>'0')));
        //pr($streams);die;
        if(!empty($testId)){
            $test = $this->Test->findById($testId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('levels','tests','test','testId'));
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            foreach ($data as $val) {
                if(!empty($val['Test']['id'])){
                    $val['Test']['updated_by'] = $this->Session->read('Admin.id');
                    $val['Test']['update_date'] = date('Y-m-d H:i:s');
                }else{
                    $val['Test']['created_by'] = $this->Session->read('Admin.id');
                    $val['Test']['created_date'] = date('Y-m-d H:i:s');
                }
                $val['Test']['created_date'] = date('Y-m-d H:i:s');
                $this->Test->create();
                try{
                    if($this->Test->save($val)){
                        $this->Session->write('adminsuccess-msg','Test added successfully.');
                    }else{
                        $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    }
                }
                catch(Exception $e){
                    //echo $e;die;
                        $this->Session->write('adminerror-msg','Duplicate entry for '.$val['Test']['title'].' not allowed!!!');
                        $this->redirect(array('action' => 'ManageTest'));
                    }      
            }
            
            $this->redirect(array('action' => 'ManageTest'));
           
            
        } 
    }

    public function ManageTestSection($sectionId = null){
        $this->TestSection->bindModel(
        array('belongsTo' => array(
                'Test' => array(
                    'className' => 'Test',
                    'foreignKey' => 'test_id'
                    )
                )
            )
        );
        $sections=$this->TestSection->find('all');
        $tests=$this->Test->find('all',array('conditions'=>array('status'=>'1','is_delete'=>'0')));
        //pr($streams);die;
        if(!empty($sectionId)){
            $section = $this->TestSection->findById($sectionId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('tests','sections','section','sectionId'));
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            foreach ($data as $val) {
                if(!empty($val['TestSection']['id'])){
                   /* $val['TestLevel']['updated_by'] = $this->Session->read('Admin.id');
                    $val['TestLevel']['update_date'] = date('Y-m-d H:i:s');*/
                }else{
                    //$val['Stream']['created_by'] = $this->Session->read('Admin.id');
                }
                $val['TestSection']['created_date'] = date('Y-m-d H:i:s');
                $levId = $this->Test->findById($val['TestSection']['test_id']);
                $val['TestSection']['test_level_id'] = $levId['Test']['test_level_id'];
                $this->TestSection->create();
                try{
                    if($this->TestSection->save($val)){
                        $this->Session->write('adminsuccess-msg','TestSection added successfully.');
                    }else{
                        $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    }
                }
                catch(Exception $e){
                    //echo $e;die;
                        $this->Session->write('adminerror-msg','Duplicate entry for '.$val['TestSection']['title'].' not allowed!!!');
                        $this->redirect(array('action' => 'ManageTestSection'));
                    }      
            }
            
            $this->redirect(array('action' => 'ManageTestSection'));
           
            
        } 
    }

    public function manageTestQuestion(){
        $this->TestQuestion->bindModel(
        array('belongsTo' => array(
                'Test' => array(
                    'className' => 'Test',
                    'foreignKey' => 'test_id'
                    )
                )
            )
        );
        $this->TestQuestion->bindModel(
        array('belongsTo' => array(
                'TestLevel' => array(
                    'className' => 'TestLevel',
                    'foreignKey' => 'test_level_id'
                    )
                )
            )
        );
        $this->TestQuestion->bindModel(
        array('belongsTo' => array(
                'TestSection' => array(
                    'className' => 'TestSection',
                    'foreignKey' => 'test_section_id'
                    )
                )
            )
        );
        $questions = $this->TestQuestion->find('all');
        //pr($questions);die;
        $this->set(compact('questions'));
    }

    public function addTestQuestion($testQuestionId = null){
        $levels = $this->TestLevel->find('all',array('conditions'=>array('status'=>'1','is_delete'=>'0')));
        if(!empty($testQuestionId)){
            $this->TestQuestion->bindModel(
            array('hasMany' => array(
                    'TestQuestionOption' => array(
                        'className' => 'TestQuestionOption',
                        'foreignKey' => 'test_question_id'
                        )
                    )
                )
            );
            $testquestion = $this->TestQuestion->findById($testQuestionId);
            $tests = $this->Test->findAllByTestLevelId($testquestion['TestQuestion']['test_level_id']);
            $sections = $this->TestSection->findAllByTestId($testquestion['TestQuestion']['test_id']);
           // pr($testquestion);die;
        }
        $this->set(compact('levels','testquestion','sections','tests','testQuestionId'));

        if($this->request->is('post')){
            //pr($this->data);die;
            $testQuestion = $this->data['TestQuestion'];
            $testQuestion['created_by'] = $this->Session->read('Admin.id');
            $testQuestion['created_date'] = date('Y-m-d H:i:s');
            //pr($testQuestion);die;
            $this->TestQuestion->save($testQuestion);
            $testqueId = $this->TestQuestion->getLastInsertId();
            if(!empty($testQuestion['id'])){
                $testqueId = $testQuestion['id'];
                $this->TestQuestionOption->deleteAll(array('test_question_id'=>$testqueId));
            }
            
            foreach($this->data['TestQuestionOption'] as $key => $val){
                $option['TestQuestionOption']['test_question_id'] = $testqueId;
                $option['TestQuestionOption']['option'] = $val;
                $option['TestQuestionOption']['created_by'] = $this->Session->read('Admin.id');
                $option['TestQuestionOption']['created_date'] = date('Y-m-d H:i:s');
                if($key == $this->data['answer_id']){
                    $this->TestQuestionOption->create();
                    $this->TestQuestionOption->save($option);
                    $qId = $this->TestQuestionOption->getLastInsertId();
                    $this->TestQuestion->id = $testqueId;
                    $this->TestQuestion->saveField('answer_id',$qId);
                    
                }else{
                    $this->TestQuestionOption->create();
                    $this->TestQuestionOption->save($option);
                }
            }
            $this->redirect(array('action'=>'manageTestQuestion'));
        }
    }
    public function findTest($levelId=null){
        $data = $this->Test->find('all', array('conditions' => array('Test.test_level_id' => $levelId)));
        $test = '<option>Select Test</option>';
        if(!empty($data)){
            
            foreach ($data as $da) {
                $test .= '<option value="'.$da['Test']['id'].'">'.$da['Test']['title'].'</option>';
            }
            
        }
        echo json_encode($test);die;
        
    }

    public function findSection($testId=null){
        $data = $this->TestSection->find('all', array('conditions' => array('TestSection.test_id' => $testId)));
        $test = '<option>Select Test Section</option>';
        if(!empty($data)){
            
            foreach ($data as $da) {
                $test .= '<option value="'.$da['TestSection']['id'].'">'.$da['TestSection']['title'].'</option>';
            }
            
        }
        echo json_encode($test);die;
        
    }

}
