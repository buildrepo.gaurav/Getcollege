<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class CmsController extends AppController {
	
	public $components = array('Session','Email','RequestHandler','Cookie','Paginator');
	public $helpers = array('Session','Html');
    var $layout = 'admin';
    var $uses = array('Admin');
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->loadModel('Country');
        $this->loadModel('Program');
        $this->loadModel('Course');
        $this->loadModel('Specialization');
        $country = $this->Country->find('all');
        $this->Program->bindModel(
        array('hasMany' => array(
                'Course' => array(
                    'className' => 'Course',
                    'foreignKey' => 'program_id',
                    'conditions' => array('Course.status'=>'1'),
                    'fields' => array('course','id')
                    )
                )
            )
        );
        $this->Course->bindModel(
        array('hasMany' => array(
                'Specialization' => array(
                    'className' => 'Specialization',
                    'foreignKey' => 'course_id',
                    'conditions' => array('Specialization.status'=>'1'),
                    'fields' => array('specialization','id')
                    )
                )
            )
        );

        $hdr_prgm = $this->Program->find('all',array('conditions'=>array('status'=>'1', 'is_delete'=>'0'),'recursive'=>3,'fields'=>array('program')));
        //pr($hdr_prgm);die;
        $this->set(compact('country','hdr_prgm'));
        $actions = array('login','forgetPassword','contact');
       if(!in_array($this->params['action'], $actions))
        {
            if(!$this->Session->check('Admin') )
            {
                $this->redirect(array('controller'=>'Admins','action'=>'login'));
            }
        }else{
            /*if($this->Session->check('Admin'))
            {
                if($this->params['action'] == 'contact')
                {
                    $this->redirect(array('controller'=>'Cms','action'=>'contact'));
                }
                else{
                    $this->redirect(array('controller'=>'Admins','action'=>'dashboard'));
                }
                
            }*/
        }

        $adminDetails = $this->Admin->findById($this->Session->read('Admin.id'));
        $this->set(compact('adminDetails'));

        $subadminBlockedAction = array('manageSubadmin','editsubadmin','addSubadmin','deletesubadmin');

        if(!empty($adminDetails['Admin']['type']) && $adminDetails['Admin']['type'] == '1')
        {
            if(in_array($this->params['action'], $subadminBlockedAction))
                {
                    $this->Session->write('adminerror-msg','You are not authorized to view this page!!!');
                    $this->redirect(array('action'=>'dashboard'));
                 
                }   
        }
  

    }

    public function delete($model = null, $id = null)
    {
        $this->loadModel($model);
        $CategoryId = base64_decode($id);
        $status = $this->$model->delete($CategoryId);
        
        die;
    }

    public function manageAboutus(){
        $this->loadModel('About');
        $about = $this->About->find('first');
        $this->set(compact('about'));
        if($this->request->is('post')){

            $data = $this->data;
            $data['About']['updated_by'] = $this->Session->read('Admin.id');
            $data['About']['updated_date'] =date('Y-m-d');
            $this->About->save($data);
            $this->Session->write('adminsuccess-msg','Data Updated successfully.');
            $this->redirect(array('action'=>'manageAboutus'));
        }
    }

    public function managePrivacy(){
        $this->loadModel('Privacy');
        $about = $this->Privacy->find('first');
        $this->set(compact('about'));
        if($this->request->is('post')){
            $data = $this->data;
            $data['Privacy']['updated_by'] = $this->Session->read('Admin.id');
            $data['Privacy']['updated_date'] =date('Y-m-d');
            //pr($data);die;
            $this->Privacy->save($data);
            $this->Session->write('adminsuccess-msg','Data Updated successfully.');
            $this->redirect(array('action'=>'managePrivacy'));
        }
    }

    public function manageTerm(){
        $this->loadModel('Term');
        $about = $this->Term->find('first');
        $this->set(compact('about'));
        if($this->request->is('post')){
            $data = $this->data;
            $data['Term']['updated_by'] = $this->Session->read('Admin.id');
            $data['Term']['updated_date'] =date('Y-m-d');
            //pr($data);die;
            $this->Term->save($data);
            $this->Session->write('adminsuccess-msg','Data Updated successfully.');
            $this->redirect(array('action'=>'manageTerm'));
        }
    }

    public function manageFaq($faqId = null){
        $this->loadModel('Faq');
        $faqs = $this->Faq->find('all');
        if(!empty($faqId)){
            $faq = $this->Faq->findById($faqId);
        }
        $this->set(compact('faqs','faq'));
        if($this->request->is('post')){
            $data = $this->data;
            if(empty($data['Faq']['id'])){
                $data['Faq']['create_by'] = $this->Session->read('Admin.id');
           
            }else{
                $data['Faq']['update_by'] = $this->Session->read('Admin.id');
                $data['Faq']['update_date'] =date('Y-m-d');
            }
            //pr($data);die;
            $this->Faq->save($data);
            $this->Session->write('adminsuccess-msg','Data Updated successfully.');
            $this->redirect(array('action'=>'manageFaq'));
        }
    }

    public function coupon($couponId = null){
        $this->loadModel('Coupon');
        $this->loadModel('CouponType');
        $this->loadModel('College');
        $this->loadModel('Course');
        $colleges = $this->College->find('list',array('conditions'=>array('status'=>'1','is_delete'=>'0'),'fields' => array('name')));
        $courses = $this->Course->find('list',array('conditions'=>array('status'=>'1','is_delete'=>'0'),'fields' => array('course')));
       // pr();die;
        if(!empty($couponId)){
            $coupon = $this->Coupon->findById($couponId);
        }
        $couponTypes = $this->CouponType->find('all');
        $this->Coupon->bindModel(
            array('belongsTo' => array(
                    'CouponType' => array(
                        'className' => 'CouponType',
                        'foreignKey' => 'Coupon_type_id'
                        )
                    )
                )
        );
        $coupons = $this->Coupon->find('all',array('limit'=>3, 'offset' => 0));
        $this->set(compact('couponTypes','coupons','coupon','colleges','courses'));
        if($this->request->is('post')){
            $data = $this->data;
            if(empty($data['Coupon']['id'])){
                $data['Coupon']['created_by'] = $this->Session->read('Admin.id');
           
            }else{
                $data['Coupon']['updated_by'] = $this->Session->read('Admin.id');
                $data['Coupon']['updated_date'] =date('Y-m-d');
            }
            //pr($data);die;
            try{
                $this->Coupon->save($data);
                $this->Session->write('adminsuccess-msg','Data Updated successfully.');
                $this->redirect(array('action'=>'coupon'));
            }
            catch(Exception $e){
                $this->Session->write('adminsuccess-msg','Duplicate entry for code '.$data['Coupon']['code'].' not allowed.');
                $this->redirect(array('action'=>'coupon'));
            }
        }

    }

    public function getCode(){
        $code = $this->CapitalRandomStringGenerator(8);
        echo $code;die;
    }

    public function manageReference(){
        $this->loadModel('ReferenceAmount');
        $this->loadModel('ReferenceList');
        $amounts = $this->ReferenceAmount->find('all',array('conditions'=>array('status'=>'1','is_delete'=>'0'),'limit'=>'5'));
        $this->set(compact('amounts'));
        if($this->request->is('post')){
            $data = $this->request->data;
            $data['ReferenceAmount']['updated_by'] = $this->Session->read('Admin.id');
            $data['ReferenceAmount']['updated_date'] = date('Y-m-d');
            if($this->ReferenceAmount->save($data)){
               $this->Session->write('adminsuccess-msg','Amount Updated Successfully.');
                $this->redirect(array('action' => 'manageReference'));
            } else {
                $this->Session->write('adminerror-msg',"Server error,please try again !!!");
                $this->redirect(array('action' => 'manageReference'));
            }
        }
    }

    public function referenceList(){
        $this->loadModel('ReferenceList');

        $this->ReferenceList->bindModel(
        array('belongsTo' => array(
                'Student' => array(
                    'className' => 'Student',
                    'foreignKey' => 'referrer_id',
                    'fields' => array('name')
                    )
                )
            )
        );
        $references = $this->ReferenceList->find('all',array('limit'=>'10')); 
        $this->set(compact('amount','references'));
    }
    public function manageContact(){
        $this->loadModel('Contact');
        $contact = $this->Contact->find('all');
        $this->set(compact('contact'));
    }
    public function contact(){
        $this->layout="home";
        $this->loadModel('Contact');
        if($this->request->is('post')){
            $data = $this->request->data;
            $this->Contact->save($data);
            $this->redirect(array('action'=>'contact'));
        }
    }
    public function manageEmailTemp(){
        $this->loadModel('EmailTemplate');
        $emails = $this->EmailTemplate->findAllByStatus(1);
        $this->set(compact('emails'));
    }
    public function editEmailTemp($id=null){
        $this->loadModel('EmailTemplate');
        $data = $this->EmailTemplate->findById($id);
        $this->set(compact('data'));
        if($this->request->is('post')){
            $data = $this->request->data;
            $this->EmailTemplate->save($data);
            $this->redirect(array('action'=>'manageEmailTemp'));
        }
    }
    
}
