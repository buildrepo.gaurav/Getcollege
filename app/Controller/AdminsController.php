<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AdminsController extends AppController {
	
	public $components = array('Session','Email','RequestHandler','Cookie','Paginator');
	public $helpers = array('Session','Html');
    var $layout = 'admin';
    var $uses = array('Admin','Notify');
    public function beforeFilter()
    {
        parent::beforeFilter();
        $actions = array('login','forgetPassword','findState','findCity');
        if(!in_array($this->params['action'], $actions))
        {
            if(!$this->Session->check('Admin') )
            {
            	$this->redirect(array('action'=>'login'));
        	}
            
   		// }else{
   		// 	if($this->Session->check('Admin'))
     //        {
     //        	$this->redirect(array('action'=>'dashboard'));
     //    	}
   		// }

        $adminDetails = $this->Admin->findById($this->Session->read('Admin.id'));
        $this->set(compact('adminDetails'));

        $subadminBlockedAction = array('manageSubadmin','editsubadmin','addSubadmin','deletesubadmin');

        if(!empty($adminDetails['Admin']['type']) && $adminDetails['Admin']['type'] == '1')
        {
            if(in_array($this->params['action'], $subadminBlockedAction))
                {
                    $this->Session->write('adminerror-msg','You are not authorized to view this page!!!');
                    $this->redirect(array('action'=>'dashboard'));
                 
                }   
        }
  

    }
}

    public function login() 
    {
       $this->layout="adminLoginLayout";
        $this->loadModel('Admin');
        if($this->request->is('post')) 
        {
        	
            $data['Admin'] = $this->data['Admin'];

            $data['Admin']['password'] = md5($data['Admin']['password']);
            $result = $this->Admin->find('first',array('conditions'=>array('OR'=>array(array('Admin.username'=>$data['Admin']['name']),array('Admin.email'=>$data['Admin']['name'])),'Admin.password'=>$data['Admin']['password'])));
            if(count($result)>0)
            {	//pr($this->data['remember']);die;
            	if($this->data['remember'] == '1'){

            	}
				$this->Session->write('Admin',$result['Admin']);
				$result['Admin']['current_ip'] = $_SERVER['REMOTE_ADDR'];
				$result['Admin']['current_access']  = date("Y-m-d H:i:s");
				//pr($result);die;
				$this->Admin->save($result);    
				$this->redirect(array('action'=>'dashboard'));
                    
            }else
            {
                $this->Session->write('loginerror-msg','Incorrect Username/Email or Password');
                $this->redirect(array('controller'=>'Admins','action'=>'login'));
                        
            }
                 
        }
    }

    public function logout() 
    {
        $this->layout="";
        $result = $this->Admin->findById($this->Session->read('Admin.id'));
        $result['Admin']['last_ip'] = $_SERVER['REMOTE_ADDR'];
        $result['Admin']['last _access']  = $result['Admin']['current_access'];
        $result['Admin']['online']  = '0';
        $this->Admin->save($result);
        $this->Session->delete('Admin');
        $this->redirect(array('controller'=>'Admins','action' => 'login'));
        
    }

    public function forgetPassword()
    {
        $this->loadModel('Admin');
		
		//pr($this->data);die;
		$email = $this->data['email'];
        $checkEmail = $this->Admin->findByEmail($email);
        if (!empty( $checkEmail)) 
        {
			$password = $this->RandomStringGenerator(10);
			$encryptPassword = md5($password);
			$this->Admin->id = $checkEmail['Admin']['id'];
			$this->Admin->saveField('password',$encryptPassword);
			/*$Email = new CakeEmail();
			//$Email->viewVars(array('value' => $p,'value2'=>$email1,'value3'=>$email3['User']['name']));//echo $link;die;



			$Email->template('forgotmail')
			$Email->emailFormat('html')
			$Email->from(array('info@Formsadda.com' => 'Formsadda'));
			$Email->to($email);
			$Email->subject('Account Password');
			$Email->send('Your new password is'.$password);*/
			$this->Session->write('forgetpassword-msg','Password sent to given email. Please check.');
        	$this->redirect(array('action' => 'login'));
        } else {
            $this->Session->write('forgetpassword-msg',"Email doesn't exist. Please contact to Admin.");
        	$this->redirect(array('action' => 'login'));
        }
        
    }

    public function dashboard()
    {
    	
    }
    public function quickmail()
    {
        $this->loadModel('SendMail');
        if($this->request->is('post'))
        {
            //pr($this->data);die;
            $data = $this->request->data;
            $savedata['SendMail']['sender_id'] = $this->Session->read('Admin.id');
            $savedata['SendMail']['receiver_id'] = $data['to'];
            $savedata['SendMail']['subject'] = $data['subject'];
            $savedata['SendMail']['message'] = $data['message'];
            $savedata['SendMail']['created_date'] = date('Y-m-d H:i:s');
            $this->SendMail->save($savedata);
            $Email = new CakeEmail();

            $Email->emailFormat('html')
              ->from($this->Session->read('Admin.email'))
              ->to($data['to'])
              ->subject($data['subject'])
              ->send($data['message']);
            $this->Session->write('adminsuccess-msg','Your mail sent successfully.');
            $this->redirect(array('action'=>'dashboard'));
        }
    }

    public function editAdmin()
    {
        $admin = $this->Admin->findById($this->Session->read('Admin.id'));
        $this->set(compact('admin'));
        if($this->request->is('post'))
        {
            $data = $this->data;
            $data['Admin']['id'] = $this->Session->read('Admin.id');
            
            if (!empty($_FILES['image']['tmp_name'])) 
            {
                $destination = realpath('../webroot/img/adminImages/small/') . '/';
                $destination1 = realpath('../webroot/img/adminImages/large/') . '/';
                $image = $this->Components->load('Resize');  

                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination1 . $filenameimg);
                    $data['Admin']['image']=$filenameimg;
                    
                    
                }
            }
            
            if($this->Admin->save($data))
                $this->Session->write('adminsuccess-msg','Admin details updated successfully.');
            else            
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'editAdmin'));
        }
    }

    public function ChangePasswordAdmin()
    {
        if($this->request->is('post'))
        {
            //pr($this->data);
            $data['Admin']['password'] = md5($this->data['password']);
            $data['Admin']['id'] = $this->Session->read('Admin.id');
            //pr($data);die;
            $this->Admin->save($data);
            $this->Session->write('adminsuccess-msg','Password change successfully.');
            $this->redirect(array('action' => 'dashboard'));


        }
    }

    public function checkPassword()
    {
        $password = md5(trim($this->data['oldpassword']));
        $checkPassword = $this->Admin->findByPassword($password);
        if(!empty($checkPassword))
            echo 'true';
        else
            echo 'false';
        die;
        
    }

    public function checkEmail()
    {
        $email = trim($this->data['Admin']['email']);
        $checkEmail = $this->Admin->findByEmail($email);
        if(!empty($checkEmail))
            echo 'false';
        else
            echo 'true';
        die;
        
    }

    public function manageSubadmin(){
        $subadmins = $this->Admin->findAllByType('1');
        $this->set(compact('subadmins'));
    }

    public function deletesubadmin($id = null)
    {
        
        if($this->Admin->delete($id))
            $this->Session->write('adminsuccess-msg','SubAdmin deleted successfully.');
        else            
            $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
            $this->redirect(array('action' => 'manageSubadmin'));
    }

    public function editsubadmin($id = null)
    {
        $admin = $this->Admin->findById($id);
        $this->set(compact('admin'));
        if($this->request->is('post'))
        {
            $data = $this->data;
            
            if (!empty($_FILES['image']['tmp_name'])) 
            {
                $destination = realpath('../webroot/img/adminImages/small/') . '/';
                $destination1 = realpath('../webroot/img/adminImages/large/') . '/';
                $image = $this->Components->load('Resize');  

                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination1 . $filenameimg);
                    $data['Admin']['image']=$filenameimg;
                    
                    
                }
            }
            
            if($this->Admin->save($data))
                $this->Session->write('adminsuccess-msg','SubAdmin details updated successfully.');
            else            
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'manageSubadmin'));
        }
    }
    public function gallery()
    {
        $this->loadModel('Photo');
        $pics = $this->Photo->find('all');
        $this->set(compact('pics'));
        //pr($pics);die;
    }
    public function chngestatus($id=null,$stat=null)
    {
        $this->loadModel('Photo');
        $id = base64_decode($id);
        $data['Photo']['id'] = $id;
        $data['Photo']['to_show'] = $stat;
        //pr($data);die;
        $this->Photo->save($data);
        $this->redirect($this->referer());
    }
    public function addphoto()
    {
        $this->loadModel('Photo');
        if($this->request->is('post'))
        {
            //pr($_FILES);pr(count($_FILES['image']['name']));die;
            if(!empty($_FILES['image']['name']['0']))
            {
                $count = count($_FILES['image']['name']);
                for($i=0;$i<$count;$i++)
                  {
                    App::Import('Component','UploadComponent');
                    $imgName=pathinfo($_FILES['image']['name'][$i]);
                    $ext=$imgName['extension'];
                    $newImgName = rand(10,100000);
                    $tempFile = $_FILES['image']['tmp_name'][$i];
                    $destination = realpath('../webroot/img/photos/'). '/';
                       //pr($_FILES);die;
                    if(is_uploaded_file($_FILES['image']['tmp_name'][$i]))
                    {
                      $src = $_FILES['image']['tmp_name'][$i];
                      //list( $width, $height, $source_type ) = getimagesize($src);
                        move_uploaded_file($_FILES['image']['tmp_name'][$i],$destination.$newImgName.".".$ext);
                    }
                    $Image2['Photo']['image']=$newImgName.".".$ext;
                    $Image2['Photo']['to_show']= 0;      
                    $Image2['Photo']['uploaded_date']= date("Y-m-d");
                    $this->Photo->create();
                    $this->Photo->save($Image2);
                  }
            }
            $this->Session->write('adminsuccess-msg','Photos added successfully.');
            $this->redirect($this->referer());
        }
    }
    public function addSubadmin($id = null)
    {
        if($this->request->is('post') && !empty($this->data))
        {
            $data = $this->data;
            $data['Admin']['password'] = md5($this->data['Admin']['password']);
            if (!empty($_FILES['image']['tmp_name'])) 
            {
                $destination = realpath('../webroot/img/adminImages/small/') . '/';
                $destination1 = realpath('../webroot/img/adminImages/large/') . '/';
                $image = $this->Components->load('Resize');  

                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination1 . $filenameimg);
                    $data['Admin']['image']=$filenameimg;
                    
                    
                }
            }else{
                $data['Admin']['image']='adminDefault.png';
            }
            
            if($this->Admin->save($data))
                $this->Session->write('adminsuccess-msg','SubAdmin added successfully.');
            else            
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'manageSubadmin'));
        }
    }

    public function findState($country_id=null){
        $this->loadModel("State");
        $data = $this->State->find('all', array('conditions' => array('State.country_id' => $country_id)));
        if(!empty($data)){
            $state = '<option>Select State</option>';
            foreach ($data as $da) {
                $state .= '<option value="'.$da['State']['id'].'">'.$da['State']['statename'].'</option>';
            }
            //pr($state);die;
            echo json_encode($state);die;
        }
        die;
        
    }
    public function findCity($state_id=null){
        $this->loadModel("City");
        $data = $this->City->find('all', array('conditions' => array('City.state_id' => $state_id)));
        if(!empty($data)){
            $state = '<option>Select City</option>';
            foreach ($data as $da) {
                $state .= '<option value="'.$da['City']['id'].'">'.$da['City']['city_name'].'</option>';
            }
            echo json_encode($state);die;
        }
        die;
    }
    public function delete($model = null, $id = null)
    {
        $this->loadModel($model);
        $CategoryId = base64_decode($id);
        $this->$model->delete($CategoryId);
        $this->redirect($this->referer());
    }
    public function manageCountry($countryId = null){
        $this->loadModel("Country");
        if(!empty($countryId)){
            $country = $this->Country->findById($countryId);
            $this->set(compact('country'));
        }
        $countries = $this->Country->find('all',array('conditions'=>array('is_delete'=>'0')));
        $this->set(compact('countries'));
    }

    public function addCountry($countryId = null){
        $this->loadModel("Country");

        if($this->request->is('post') && !empty($this->data))
        {
            $destination = realpath('../webroot/img/') . '/';
            $image = $this->Components->load('Resize');  
            $data = $this->data;
            //pr($_FILES);
            if(is_uploaded_file($_FILES['image']['tmp_name']))
            {
                
                $filenameimg = time().'-'.$_FILES['image']['name'];
                $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                
                $data['Country']['symbol']=$filenameimg;
                
                
            }
            if(!empty($data['Country']['id'])){
                    $data['Country']['update_by'] = $this->Session->read('Admin.id');
                    $data['Country']['update_date'] = date('Y-m-d');
                }else{
                    $data['Country']['created_by'] = $this->Session->read('Admin.id');
                    $data['Country']['created_date'] = date("Y-m-d");
                }
            
            //pr($data);die;
            try{
                if($this->Country->save($data))
                    $this->Session->write('adminsuccess-msg','Country added successfully.');
                else            
                    $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    $this->redirect(array('action' => 'manageCountry'));
                }
                catch(Exception $e){
                        $this->Session->write('adminerror-msg','Duplicate entry for '. $data['Country']['country_name'].' not allowed!!!');
                        $this->redirect(array('action' => 'manageCountry'));
                    }    
        }
    }

    public function manageState($stateid=null){
        $this->loadModel("State");
        $this->loadModel("Country");
        $country1 = $this->Country->find('all');
        $this->set(compact('country1'));
        if(!empty($stateid)){
            $state = $this->State->findById($stateid);
            $this->set(compact('state'));
        }
        $this->State->bindModel(
            array('belongsTo' => array(
                    'Country' => array(
                        'className' => 'Country',
                        'foreignKey' => 'country_id'
                        )
                    )
                )
            );
            $countries = $this->State->find('all', array('limit' => 10, 'offset' => 0,'conditions'=>array('State.is_delete'=>'0')));
            $this->set(compact('countries'));
        if($this->request->is('ajax')){
            @$limit = $_POST['limit'];
            @$offset = $_POST['offset'];
            $this->State->bindModel(
            array('belongsTo' => array(
                    'Country' => array(
                        'className' => 'Country',
                        'foreignKey' => 'country_id'
                        )
                    )
                )
            );
            $countries = $this->State->find('all', array('limit' => $limit, 'offset' => $offset));
            $this->set(compact('countries'));
            $this->render('scroll_state');
        }
        
    }
    public function addState(){
        $this->loadModel("Country");
        $this->loadModel("State");
        $country = $this->Country->find('all');
        $this->set(compact('country'));
        if($this->request->is('post') && !empty($this->data))
        {
            $data = $this->data;
            if(!empty($data['State']['id'])){
                    $data['State']['update_by'] = $this->Session->read('Admin.id');
                    $data['State']['update_date'] = date('Y-m-d');
                }else{
                    $data['State']['created_by'] = $this->Session->read('Admin.id');
                }
            try{    
                if($this->State->save($data))
                    $this->Session->write('adminsuccess-msg','State added successfully.');
                else            
                    $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    $this->redirect(array('action' => 'manageState'));
                }
            catch(Exception $e){
                        $this->Session->write('adminerror-msg','Duplicate entry for '. $data['State']['statename'].' not allowed!!!');
                        $this->redirect(array('action' => 'manageState'));
                    }    
        }
    }

    public function manageCity(){
        $this->loadModel("City");
        $this->loadModel("State");
        $this->loadModel("Country");
        $country = $this->Country->find('all');
        $state = $this->State->find('all');
        $this->set(compact('country','state'));
        $this->City->bindModel(
        array('belongsTo' => array(
                'State' => array(
                    'className' => 'State',
                    'foreignKey' => 'state_id'
                    )
                )
            )
        );
        $countries = $this->City->find('all', array('limit' => 10, 'offset' => 0));
        /*$this->Paginator->settings = array('City' => array('limit' => 10));
        $countries=$this->paginate('City');*/
        $this->set(compact('countries'));
        if($this->request->is('ajax')){
            @$limit = $_POST['limit'];
            @$offset = $_POST['offset'];
            $this->City->bindModel(
            array('belongsTo' => array(
                    'State' => array(
                        'className' => 'State',
                        'foreignKey' => 'state_id'
                        )
                    )
                )
            );
            $countries = $this->City->find('all', array('limit' => $limit, 'offset' => $offset));
            
            $this->set(compact('countries'));
            $this->render('scroll_city');
        }
    }
    public function addCity(){
        $this->loadModel("Country");
        $this->loadModel("City");
        $this->loadModel("State");
        $country = $this->Country->find('all');
        $this->set(compact('country'));
        if($this->request->is('post') && !empty($this->data))
        {
            $data = $this->data;
            $data['City']['created_date'] = date('Y-m-d H:i:s');
            try{
                if($this->City->save($data))
                    $this->Session->write('adminsuccess-msg','City added successfully.');
                else            
                    $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    $this->redirect(array('action' => 'manageCity'));
               }
               catch(Exception $e){
                        $this->Session->write('adminerror-msg','Duplicate entry for '. $data['City']['city_name'].' not allowed!!!');
                        $this->redirect(array('action' => 'manageCity'));
                    }      
        }
    }
    public function manageReviews(){
        $this->loadModel("Review");
        $this->loadModel("Profiler");
        $words = $this->Profiler->find('list',array('fields'=>'word'));
        $reviews = $this->Review->find('all',array('conditions'=>array('status !='=>'1','is_delete'=>'0'),'order'=>"id desc"));
        $this->set(compact('reviews','words'));
    }
    public function review_mail(){
        $this->loadModel("Review");
        $this->loadModel("Student");
        $this->loadModel("College");
        if($this->request->is('post') && !empty($this->data))
        {
            $data = $this->data;
            $this->Review->save($data);
            $reviews = $this->Review->findById($data['Review']['id']);
            $std_det = $this->Student->findById($reviews['Review']['student_id']);
            $clg_det = $this->College->findById($reviews['Review']['college_id']);
            /*$replace = array('{name}','{review}','{rating}','{college_name}','{message}');
            $with = array($std_det['Student']['name'],$reviews['Review']['review'],$reviews['Review']['rating'],$clg_det['College']['name'],$data['Review']['message']);
            $this->send_email('',$replace,$with,'Review_reject',INFO_FORMSADDA,$std_det['Student']['email'] );*/
            /*$msg = 'Your review has been rejected because '.$data['Review']['message'];
            $Email = new CakeEmail();
            $Email->from(array('Info@Formsadda.com' => 'Formsadda'))
                ->to($std_det['Student']['email'])
                ->subject('Review')
                ->send($msg);*/

            $this->redirect(array('action' => 'manageReviews'));
        }
    }

    public function manageExam($examId = null){
        $this->loadModel('Exam');
        $this->Paginator->settings = array('Exam' => array('order' => array('Exam.id' => 'DESC'), 'limit' => 4, 'offset' => 0));

        $exams=$this->paginate('Exam');
        //pr($exams);die;
        if(!empty($examId)){
            $exam = $this->Exam->findById($examId);
            //pr($CollegeFacility );die;
        }

        $this->set(compact('exam','exams','examId'));

        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            foreach ($data as $val) {
                if(!empty($val['Exam']['id'])){
                    $val['Exam']['last_updated_by'] = $this->Session->read('Admin.id');
                    $val['Exam']['last_update_date'] = date('Y-m-d');
                }else{
                    $val['Exam']['created_by'] = $this->Session->read('Admin.id');
                    $val['Exam']['created_date'] = date("Y-m-d");
                }
                $this->Exam->create();
                try{
                    if($this->Exam->save($val)){
                        $this->Session->write('adminsuccess-msg','Exam added successfully.');
                    }else{
                        $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    }
                }
                catch(Exception $e){
                        $this->Session->write('adminerror-msg','Duplicate entry for '.$val['Exam']['exam'].' not allowed!!!');
                        $this->redirect(array('action' => 'manageExam'));
                    }      
            }
            
            $this->redirect(array('action' => 'manageExam'));
           
            
        } 
    }

    public function manageMembership($id=null){
        $this->loadModel('MembershipPlan');
        if(!empty($id)){
            $plan = $this->MembershipPlan->findById($id);
        }
        $plans = $this->MembershipPlan->find('all', array('limit' => 4, 'offset' => 0));
        $this->set(compact('plan','plans'));
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            if(!empty($data['MembershipPlan']['id'])){
                $data['MembershipPlan']['last_updated_by'] = $this->Session->read('Admin.id');
                $data['MembershipPlan']['last_update_date'] = date('Y-m-d');
            }else{
                $data['MembershipPlan']['created_by'] = $this->Session->read('Admin.id');
            }
            try{
                    if($this->MembershipPlan->save($data)){
                        $this->Session->write('adminsuccess-msg','Membership Plan added successfully.');
                    }else{
                        $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    }
                }
                catch(Exception $e){
                        $this->Session->write('adminerror-msg','Duplicate entry for '.$data['MembershipPlan']['plan_name'].' not allowed!!!');
                        
                } 
            $this->redirect(array('action' => 'manageMembership'));
        }
    }
    public function notifyList(){
        $this->loadModel('Notify');
        $this->Notify->bindModel(
            array('belongsTo' => array(
                    'City' => array(
                        'className' => 'City',
                        'foreignKey' => 'city_id',
                        'fields' => 'city_name'                        
                        )
                    )
                )
            );
        $this->Notify->bindModel(
            array('belongsTo' => array(
                    'State' => array(
                        'className' => 'State',
                        'foreignKey' => 'state_id',
                        'fields' => 'statename'                        
                        )
                    )
                )
            );
        $this->Notify->bindModel(
            array('belongsTo' => array(
                    'Country' => array(
                        'className' => 'Country',
                        'foreignKey' => 'country_id',
                        'fields' => 'country_name'                        
                        )
                    )
                )
            );
        $this->Notify->bindModel(
            array('belongsTo' => array(
                    'Course' => array(
                        'className' => 'College',
                        'foreignKey' => 'course_id'                        
                        )
                    )
                )
            );
        $notify = $this->Notify->find('all', array('conditions' => array('Notify.is_delete' => 0),'limit' => 10, 'offset' => 0));
        //pr($notify);die;
        $this->set(compact('notify'));
    }

}
