<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package     app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class CollegesController extends AppController {
    
    public $components = array('Session','Email','RequestHandler','Cookie','Paginator','Resize');
    public $helpers = array('Session','Html');
    var $layout = 'admin';
    var $uses = array('Admin','College','CollegeMeta','Facility','CollegeFacility','CollegeNear','CollegeCourse','Degree','Stream','Program','Course','Specialization','Event','Gallery','CollegeGallery','CollegeMaster','Contact','CollegeField','CollegeFieldMeta');
    public function beforeFilter()
    {
        parent::beforeFilter();
        $actions = array('login','forgetPassword','collegeFacilityajax','collegeAbout','collegeNear','editCollege','findProgram','findCourse','findSpecialization');
        if(!in_array($this->params['action'], $actions))
        {
            if(!$this->Session->check('Admin') )
            {
                $this->redirect(array('controller'=>'Admins','action'=>'login'));
            }
        }/*else{
            if($this->Session->check('Admin'))
            {
                $this->redirect(array('controller'=>'Admins','action'=>'dashboard'));
            }
        }*/

        $adminDetails = $this->Admin->findById($this->Session->read('Admin.id'));
        $this->set(compact('adminDetails'));
  

    }
    public function delete($model = null, $id = null)
    {
        $this->loadModel($model);
        $CategoryId = base64_decode($id);
        $status = $this->$model->findById($CategoryId);
        
        if($status[$model]['is_delete'] == '1'){
            $status[$model]['is_delete'] = '0';
        }else{
            $status[$model]['is_delete'] = '1';
        }
        
        $this->$model->id = $CategoryId;
        $this->$model->saveField('is_delete',$status[$model]['is_delete']);
        //echo $status[$model]['is_delete'];die;
        $this->redirect($this->referer());
    }
    
    public function manageCollege(){

        $this->College->bindModel(
        array('belongsTo' => array(
                'CreatedBy' => array(
                    'className' => 'Admin',
                    'foreignKey' => 'created_by',
                    'fields' => array('username')
                    )
                )
            )
        );
        $this->College->bindModel(
        array('belongsTo' => array(
                'UpdatedBy' => array(
                    'className' => 'Admin',
                    'foreignKey' => 'updated_by',
                    'fields' => array('username')
                    )
                )
            )
        );
        //$this->Paginator->settings = array('College' => array('limit' => 10));
        $colleges=$this->College->find('all',array('limit' => 25));
       //pr($colleges);die;
       // $colleges = $this->College->find('all');
        $this->set(compact('colleges'));
    }

    public function addCollege()
    {

        $this->loadModel("Country");
       /* $this->loadModel("CollegeForm");
        $this->loadModel("Form");*/
        $country = $this->Country->find('all');
        $collegemasters = $this->CollegeMaster->find('all');
        $facilities = $this->Facility->find('all');
        $fields = $this->CollegeField->find('all', array('conditions' => array('status' => 1, 'is_delete' => 0)));
        $this->set(compact('country','facilities','collegemasters','fields'));
        if($this->request->is('post') && !empty($this->data))
        {
            $data = $this->data;
            //pr($_FILES);
            //pr($data);die;
           // $data['College']['password'] = md5($this->data['College']['password']);
            if (!empty($_FILES['image']['tmp_name'])) 
            {
                $destination = realpath('../webroot/img/collegeImages/small/') . '/';
                $destination1 = realpath('../webroot/img/collegeImages/large/') . '/';
                $destination2 = realpath('../webroot/img/collegeImages/thumbnail/') . '/';
                $image = $this->Components->load('Resize');  

                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    $image->resize($_FILES['image']['tmp_name'],$destination2.$filenameimg,'aspect_fill',500,300,0,0,0,0);
                    //$image->resize($_FILES['image']['tmp_name'],$destination1.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination1 . $filenameimg);
                    $data['College']['image']=$filenameimg;
                    
                    
                }
            }else{
                $data['College']['image']='collegeDefault.jpg';
            }
            /*$formdata = $this->Form->find('list',array('conditions'=>array('college_id'=>'0','fields'=>'id')));
            $this->UserImage->saveAll($formdata);*/
            $data['College']['created_by'] = $this->Session->read('Admin.id');
            if($this->College->save($data)){
                $resp['collegeId'] = $this->College->getLastInsertId();

               /* $uniqueId = uniqid($resp['collegeId']);
                $this->College->id = $resp['collegeId'];
                $this->College->saveField('unique_id', $uniqueId);
                $Email = new CakeEmail();
                $Email->from(array('info@Formsadda.com' => 'Formsadda'));
                $Email->to($data['College']['email']);
                $Email->subject('FormsAdda New Registration');
                $Email->send('Your User Id is '.$uniqueId.'and  password is'.$data['College']['password']);*/
                $resp['status'] = "true";
            }
            else{            
                $resp['status'] = "false";
            }
            echo json_encode($resp);die;
        }
    }

    public function changeStatus($model = null, $id = null){
        $this->loadModel($model);
        $status = $this->$model->findById($id);
        
        if($status[$model]['status'] == '1'){
            $status[$model]['status'] = '0';
        }else{
            $status[$model]['status'] = '1';
        }
        
        $this->$model->save($status);
        echo $status[$model]['status'];
        die;
        //$this->redirect($this->referer());
    }

    public function editCollege($id=null)
    {
        $collegemasters = $this->CollegeMaster->find('all');
        $this->loadModel("Country");
        $country = $this->Country->find('all');
        $this->College->bindModel(
        array('belongsTo' => array(
                'State' => array(
                    'className' => 'State',
                    'foreignKey' => 'state_id'
                    )
                )
            )
        );
        $this->College->bindModel(
        array('belongsTo' => array(
                'City' => array(
                    'className' => 'City',
                    'foreignKey' => 'city_id'
                    )
                )
            )
        );
        $college = $this->College->findById($id);
        //pr($college);die;
        $facilities = $this->Facility->find('all');
        $collegefacilities = $this->CollegeFacility->findAllByCollegeId($id);
        $collegeId = $id;
        $CollegeNear = $this->CollegeNear->findByCollegeId($collegeId);
        $facility = $this->Facility->findById($id);
        $collegeMeta = $this->CollegeMeta->findByCollegeId($collegeId);
        $fields = $this->CollegeField->find('all', array('conditions' => array('status' => 1, 'is_delete' => 0)));
        $collegeFieldMeta = $this->CollegeFieldMeta->find('list', array('fields' => array('field_id'), 'conditions' => array('college_id' => $collegeId)));
        //pr($collegeFieldMeta);die;
        $this->set(compact('collegeMeta','facilities','collegefacilities'));
        $this->set(compact('facility'));
        $this->set(compact('CollegeNear','collegeId'));
        $this->set(compact('college','country','collegemasters','collegeFieldMeta','fields'));
       /* pr($this->request);
        pr($this->data);
        pr($_POST);
        pr($_GET);
        echo "hi";die;*/

        if($this->request->is('post') && !empty($this->data))
        {
            $data = $this->data;
            if (!empty($_FILES['image']['tmp_name'])) 
            {
                $destination = realpath('../webroot/img/collegeImages/small/') . '/';
                $destination1 = realpath('../webroot/img/collegeImages/large/') . '/';
                $destination2 = realpath('../webroot/img/collegeImages/thumbnail/') . '/';
                $image = $this->Components->load('Resize'); 
                $image2 = $this->Components->load('Resize');  

                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    $image->resize($_FILES['image']['tmp_name'],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                    $image2->resize($_FILES['image']['tmp_name'],$destination2.$filenameimg,'aspect_fill',500,300,0,0,0,0);
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination1 . $filenameimg);
                    $data['College']['image']=$filenameimg;
                    
                    
                }
            }
            $data['College']['updated_by'] = $this->Session->read('Admin.id');
            $data['College']['update_date'] = date('Y-m-d');
            if($this->College->save($data)){
                echo "true";
                die;
               // $this->Session->write('adminsuccess-msg','College updated successfully.');

            }else{           
                echo "false";
                die;
               /* $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'manageCollege'));*/
            }
        }
    }

    public function checkCollegeEmail()
    {
        $email = trim($this->data['College']['email']);
        $checkEmail = $this->College->findByEmail($email);
        if(!empty($checkEmail))
            echo 'false';
        else
            echo 'true';
        die;
        
    }

    public function manageFacility($facilityId = null){
        $this->loadModel('Facility');
        //$facilities = $this->Facility->find('all', array('limit' => 3, 'offset' => 1));
         $this->Paginator->settings = array('Facility' => array('limit' => 3, 'offset' => 0));
        $facilities=$this->paginate('Facility');
        $this->set(compact('facilities'));
        if(!empty($facilityId)){
            $facility = $this->Facility->findById($facilityId);
            $this->set(compact('facility','facilityId'));
        }
        if($this->request->is('post') && !empty($this->data))
        {
            $data = $this->data;
            //pr($_FILES);
            foreach($data as $key => $val){
                
                if (!empty($_FILES['file']['name'][$key])) 
                {
                    $destination = realpath('../webroot/img/facility/') . '/';
                    $image = $this->Components->load('Resize');  

                    if(is_uploaded_file($_FILES['file']['tmp_name'][$key]))
                    {
                        
                        $filenameimg = time().'-'.$_FILES['file']['name'][$key];
                        $image->resize($_FILES['file']['tmp_name'][$key],$destination.$filenameimg,'aspect_fill',300,300,0,0,0,0);
                        $val['Facility']['icon']=$filenameimg;
                        
                        
                    }
                }
                if(!empty($val['Facility']['id'])){
                    $val['Facility']['updated_by'] = $this->Session->read('Admin.id');
                    $val['Facility']['update_date'] = date('Y-m-d');
                }else{
                    $val['Facility']['created_by'] = $this->Session->read('Admin.id');
                }
                
                try{
                    $this->Facility->create();
                    $this->Facility->save($val);
                    }
                    catch(Exception $e){
                        $this->Session->write('adminerror-msg','Duplicate entry for '.$val['Facility']['name'].' not allowed!!!');
                        $this->redirect(array('action' => 'manageFacility'));
                    }  
                
            }
                $this->Session->write('adminsuccess-msg','Facility added successfully.');
                $this->redirect(array('action' => 'manageFacility'));
        }
    }


    public function manageCollegeCourse($collegeId = null){
          $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Course' => array(
                    'className' => 'Course',
                    'foreignKey' => 'course_id'
                    )
                )
            )
        );
        $courses  = $this->CollegeCourse->findAllByCollegeId($collegeId);
        $this->set(compact('courses','collegeId'));
    }

    public function addCollegeCourse($collegeId = null,$specializationId = null){
        $streams = $this->Stream->find('all');
        $this->loadModel('Exam');
        $this->loadModel('CourseExam');
        $exams = $this->Exam->find('all',array('conditions'=>array('status'=>'1','is_delete'=>'0')));
        if(!empty($specializationId)){
            $this->CollegeCourse->bindModel(
            array('hasMany' => array(
                    'CourseExam' => array(
                        'className' => 'CourseExam',
                        'foreignKey' => 'college_course_id'
                        )
                    )
                )
            );
            $special = $this->CollegeCourse->findById($specializationId);

            $programs = $this->Program->findAllByStreamId($special['CollegeCourse']['stream_id']);
            //pr($programs);die;
            $allcourses = $this->Course->findAllByProgramId($special['CollegeCourse']['program_id']);
            $allspecial = $this->Specialization->findAllByCourseId($special['CollegeCourse']['course_id']);
            $this->set(compact('special','programs','allcourses','allspecial','specializationId'));
        }
        $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Specialization' => array(
                    'className' => 'Specialization',
                    'foreignKey' => 'specialization_id'
                    )
                )
            )
        );
        //$courses  = $this->CollegeCourse->findAllByCollegeId($collegeId);
        $this->set(compact('streams','courses','collegeId','exams'));
        if($this->request->is('post')){
            $data['CollegeCourse'] = $this->data['CollegeCourse'];
            //pr($data);die;
            $destination = realpath('../webroot/img/docs/') . '/';
            $image = $this->Components->load('Resize');  
            if (!empty($_FILES['brochure']['tmp_name'])) 
            {
                if(is_uploaded_file($_FILES['brochure']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['brochure']['name'];
                    
                    move_uploaded_file($_FILES['brochure']['tmp_name'], $destination . $filenameimg);
                    $data['CollegeCourse']['brochure']=$filenameimg;
                    
                    
                }
            }
             if (!empty($_FILES['Structure']['tmp_name'])) 
            {
                if(is_uploaded_file($_FILES['Structure']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['Structure']['name'];
                    
                    move_uploaded_file($_FILES['Structure']['tmp_name'], $destination . $filenameimg);
                    $data['CollegeCourse']['Structure']=$filenameimg;
                    
                    
                }
            }
             if (!empty($_FILES['Placement']['tmp_name'])) 
            {
                if(is_uploaded_file($_FILES['Placement']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['Placement']['name'];
                    
                    move_uploaded_file($_FILES['Placement']['tmp_name'], $destination . $filenameimg);
                    $data['CollegeCourse']['Placement']=$filenameimg;
                    
                    
                }
            }
             if (!empty($_FILES['Offline']['tmp_name'])) 
            {
                if(is_uploaded_file($_FILES['Offline']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['Offline']['name'];
                    
                    move_uploaded_file($_FILES['Offline']['tmp_name'], $destination . $filenameimg);
                    $data['CollegeCourse']['Offline']=$filenameimg;
                    
                    
                }
            }
                //pr($data);die;
                $this->CollegeCourse->save($data);
                $course_id = $this->CollegeCourse->getLastInsertId();
                if(!empty($data['CollegeCourse']['id']))
                    $course_id = $data['CollegeCourse']['id'];
                if(!empty($this->data['CourseExam'])){
                    $this->CourseExam->deleteAll(array('college_course_id'=>$course_id));
                    foreach($this->data['CourseExam']['exam_id'] as $exam){
                        $exm['CourseExam']['exam_id'] = $exam;
                        $exm['CourseExam']['college_course_id'] = $course_id;
                        $exm['CourseExam']['college_id'] = $collegeId;
                        $this->CourseExam->create();
                        $this->CourseExam->save($exm);

                    }
                }
            
                $this->Session->write('adminsuccess-msg','Course added successfully.');
            
            $this->redirect(array('action' => 'manageCollegeCourse',$collegeId));

        }
    }
    public function manageStream($streamId = null){
        $this->Paginator->settings = array('Stream' => array('order' => array('Stream.id' => 'DESC'), 'limit' => 4, 'offset' => 0));
        $streams=$this->paginate('Stream');
        //pr($streams);die;
        if(!empty($streamId)){
            $stream = $this->Stream->findById($streamId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('stream','streams','streamId'));
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            foreach ($data as $val) {
                if(!empty($val['Stream']['id'])){
                    $val['Stream']['last_updated_by'] = $this->Session->read('Admin.id');
                    $val['Stream']['last_update_date'] = date('Y-m-d');
                }else{
                    $val['Stream']['created_by'] = $this->Session->read('Admin.id');
                }
                $val['Stream']['created_date'] = date('Y-m-d H:i:s');
                $this->Stream->create();
                try{
                    if($this->Stream->save($val)){
                        $this->Session->write('adminsuccess-msg','Stream added successfully.');
                    }else{
                        $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    }
                }
                catch(Exception $e){
                    //echo $e;die;
                        $this->Session->write('adminerror-msg','Duplicate entry for '.$val['Stream']['stream'].' not allowed!!!');
                        $this->redirect(array('action' => 'manageStream'));
                    }      
            }
            
            $this->redirect(array('action' => 'manageStream'));
           
            
        } 
    }
    public function manageProgram($programId = null){
        $this->Program->bindModel(
        array('belongsTo' => array(
                'Stream' => array(
                    'className' => 'Stream',
                    'foreignKey' => 'stream_id'
                    )
                )
            )
        );
        $this->Paginator->settings = array('Program' => array('limit' => 4, 'offset' => 0));
        $programs=$this->paginate('Program');
        $streams = $this->Stream->find('all');
        if(!empty($programId)){
            $program = $this->Program->findById($programId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('program','programs','streams','programId'));
         if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;

            foreach ($data as $val) {
                if(!empty($val['Program']['id'])){
                    $val['Program']['last_updated_by'] = $this->Session->read('Admin.id');
                    $val['Program']['last_update_date'] = date('Y-m-d');
                }else{
                    $val['Program']['created_by'] = $this->Session->read('Admin.id');
                }
                $val['Program']['date_created'] = date('Y-m-d H:i:s');
                try{
                    $this->Program->create();

                    if($this->Program->save($val)){
                        $this->Session->write('adminsuccess-msg','Program added successfully.');
                    }else{
                        $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    }
                }
                catch(Exception $e){
                        $this->Session->write('adminerror-msg','Duplicate entry not allowed!!!');
                        $this->redirect(array('action' => 'manageProgram'));
                    }      
            }
            
            $this->redirect(array('action' => 'manageProgram'));
           
            
        }
    }
    public function manageCourse($courseId = null){
        $this->Course->bindModel(
        array('belongsTo' => array(
                'Program' => array(
                    'className' => 'Program',
                    'foreignKey' => 'program_id'
                    )
                )
            )
        );
        $this->Paginator->settings = array('Course' => array('limit' => 4, 'offset' => 0));
        $courses=$this->paginate('Course');
        $programs = $this->Program->find('all');
        if(!empty($courseId)){
            $course = $this->Course->findById($courseId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('course','programs','courses'));
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            foreach($data as $co){
                $stream_id = $this->Program->findById($co['Course']['program_id']);
                $co['Course']['stream_id'] = $stream_id['Program']['stream_id'];
                if(!empty($co['Course']['id'])){
                    $co['Course']['last_updated_by'] = $this->Session->read('Admin.id');
                    $co['Course']['last_update_date'] = date('Y-m-d');
                }else{
                    $co['Course']['created_by'] = $this->Session->read('Admin.id');
                }
                $co['Course']['date_created'] = date('Y-m-d H:i:s');
                $this->Course->create();
                try{
                $this->Course->save($co);
                }
                catch(Exception $e){
                    //echo $e;die;
                        $this->Session->write('adminerror-msg','Duplicate entry for not allowed!!!');
                        $this->redirect(array('action' => 'manageCourse'));
                    }  
                //pr($co);
            }
            //die;
                $this->Session->write('adminsuccess-msg','Course added successfully.');
      /*      else            
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');*/
                $this->redirect(array('action' => 'manageCourse'));
            
        } 
    }

        public function manageSpecialization($specialId = null){
        $this->Specialization->bindModel(
        array('belongsTo' => array(
                'Course' => array(
                    'className' => 'Course',
                    'foreignKey' => 'course_id'
                    )
                )
            )
        );
        $this->Paginator->settings = array('Specialization' => array('limit' => 4, 'offset' => 0));
        $specials=$this->paginate('Specialization');
        $courses = $this->Course->find('all');
        if(!empty($specialId)){
            $special = $this->Specialization->findById($specialId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('specials','special','courses','specialId'));
        if($this->request->is('post')){
            $data = $this->data;
           //pr($data);die;
            foreach($data as $co){
                $program_id = $this->Course->findById($co['Specialization']['course_id']);
                $co['Specialization']['program_id'] = $program_id['Course']['program_id'];
                $co['Specialization']['stream_id'] = $program_id['Course']['stream_id'];
                if(!empty($co['Specialization']['id'])){
                    $co['Specialization']['last_updated_by'] = $this->Session->read('Admin.id');
                    $co['Specialization']['last_update_date'] = date('Y-m-d');
                }else{
                    $co['Specialization']['created_by'] = $this->Session->read('Admin.id');
                }
                $co['Specialization']['date_created'] = date('Y-m-d H:i:s');
                $this->Specialization->create();
                // pr($co);die;
                $this->Specialization->save($co);
                //pr($co);
            }
            //die;
                $this->Session->write('adminsuccess-msg','Specialization added successfully.');
      /*      else            
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');*/
                $this->redirect(array('action' => 'manageSpecialization'));
            
        } 
    }

  

    public function collegeAbout($collegeId = null){
        $collegeMeta = $this->CollegeMeta->findByCollegeId($collegeId);
        $this->set(compact('collegeMeta','collegeId'));
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);
            if($this->CollegeMeta->save($data)){
                echo "true";die;
                //$this->Session->write('adminsuccess-msg','College About Updated successfully.');
            }
            else {
                echo "false";die;
            }           
               /* $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'manageCollege'));*/
            
        }
    }

    public function collegeFacility($collegeId = null,$collegeFacilityId = null){
        $this->CollegeFacility->bindModel(
        array('belongsTo' => array(
                'Facility' => array(
                    'className' => 'Facility',
                    'foreignKey' => 'facility_id'
                    )
                )
            )
        );
        $this->Paginator->settings = array('CollegeFacility' => array('limit' => 10,'conditions'=>array('CollegeFacility.college_id'=>$collegeId)));
        $CollegeFacilities=$this->paginate('CollegeFacility');
        if(!empty($collegeFacilityId)){
            $CollegeFacility = $this->CollegeFacility->findById($collegeFacilityId);
            //pr($CollegeFacility );die;
        }
        $facilities = $this->Facility->find('all');
        $this->set(compact('CollegeFacility','CollegeFacilities','collegeId','facilities'));
        if($this->request->is('post')){
            $data = $this->data;
            if($this->CollegeFacility->save($data))
                $this->Session->write('adminsuccess-msg','College Facility Updated successfully.');
            else            
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'collegeFacility',$collegeId));
            
        }
    }

    public function collegeFacilityajax(){
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            $this->CollegeFacility->deleteAll(array('CollegeFacility.college_id' => $data['CollegeFacility']['college_id']));
            foreach($data['CollegeFacility']['facilities'] as $fac){
                $facility['CollegeFacility']['facility_id'] = $fac;
                $facility['CollegeFacility']['date_created'] = date('Y-m-d H:i:s');
                $facility['CollegeFacility']['college_id'] = $data['CollegeFacility']['college_id'];
                $this->CollegeFacility->create();
                $this->CollegeFacility->save($facility);
            }
            
                echo "true";die;
            
        }
    }

    public function collegeFieldajax(){
        $this->loadModel('CollegeFieldMeta');
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            $this->CollegeFieldMeta->deleteAll(array('CollegeFieldMeta.college_id' => $data['CollegeFieldMeta']['college_id']));
            foreach($data['CollegeFieldMeta']['field_id'] as $fac){
                $facility['CollegeFieldMeta']['field_id'] = $fac;
                $facility['CollegeFieldMeta']['college_id'] = $data['CollegeFieldMeta']['college_id'];
                $this->CollegeFieldMeta->create();
                $this->CollegeFieldMeta->save($facility);
            }
            
                echo "true";die;
            
        }
    }

    public function collegeCourses($collegeId = null,$collegeCourseId = null){
        $this->CollegeCourse->bindModel(
        array('belongsTo' => array(
                'Degree' => array(
                    'className' => 'Degree',
                    'foreignKey' => 'degree_id'
                    )
                )
            )
        );
        $this->Paginator->settings = array('CollegeCourse' => array('limit' => 10));
        $CollegeCourses=$this->paginate('CollegeCourse');
        $allDegree = $this->Degree->find('all');
        if(!empty($collegeCourseId)){
            $CollegeCourse = $this->CollegeCourse->findById($collegeCourseId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('CollegeCourses','allDegree','CollegeCourse','collegeId'));
        if($this->request->is('post')){
            $data = $this->data;
            if($this->CollegeCourse->save($data))
                $this->Session->write('adminsuccess-msg','College Courses Updated successfully.');
            else            
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'collegeCourses',$collegeId));
            
        }  
    }
    public function collegeNear($collegeId = null){
       
        if($this->request->is('post')){
            $data = $this->data;
            if($this->CollegeNear->save($data)){
                echo "true";die;
                //$this->Session->write('adminsuccess-msg','College Near Updated successfully.');
            }
            else{
                echo "false";die;
            }
                /*$this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'manageCollege'));*/
            
        }
    }

    /*public function manageDegree($degreeId = null){
        $this->Paginator->settings = array('Degree' => array('limit' => 10));
        $degrees=$this->paginate('Degree');
        if(!empty($degreeId)){
            $degree = $this->Degree->findById($degreeId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('degree','degrees'));
        if($this->request->is('post')){
            $data = $this->data;
            if($this->Degree->save($data))
                $this->Session->write('adminsuccess-msg','Degree added successfully.');
            else            
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'manageDegree'));
            
        }  
    }*/

    public function findProgram($stream_id=null){
        $data = $this->Program->find('all', array('conditions' => array('Program.stream_id' => $stream_id)));
        $program = '<option>Select Program</option>';
        if(!empty($data)){
            
            foreach ($data as $da) {
                $program .= '<option value="'.$da['Program']['id'].'">'.$da['Program']['program'].'</option>';
            }
            
        }
        echo json_encode($program);die;
        
    }

    public function findCourse($program_id=null){
        $data = $this->Course->find('all', array('conditions' => array('Course.program_id' => $program_id)));
         $course = '<option>Select Course</option>';
        if(!empty($data)){
           
            foreach ($data as $da) {
                $course .= '<option value="'.$da['Course']['id'].'">'.$da['Course']['course'].'</option>';
            }
            
        }
        echo json_encode($course);die;
        
    }

    public function findSpecialization($course_id=null){
        $data = $this->Specialization->find('all', array('conditions' => array('Specialization.course_id' => $course_id)));
        $specialization = '<option>Select Specialization</option>';
        if(!empty($data)){
            
            foreach ($data as $da) {
                $specialization .= '<option value="'.$da['Specialization']['id'].'">'.$da['Specialization']['specialization'].'</option>';
            }
            
        }
        echo json_encode($specialization);die;
    }

    public function manageEvent($collegeId = null,$eventId = null){
        $events=$this->Event->findAllByCollegeId($collegeId);
        if(!empty($eventId)){
            $event = $this->Event->findById($eventId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('events','event','eventId','collegeId'));
        if($this->request->is('post')){
            $data = $this->data;
           //pr($data);die;
            if($this->Event->saveAll($data))
                $this->Session->write('adminsuccess-msg','Event updated successfully.');
            else            
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                $this->redirect(array('action' => 'manageEvent',$collegeId));
            
        }
    }    

    public function changeSequence($collegeId = null){
        $data = $this->data;
        //pr($data);die;
        $oldSeq = $this->College->findById($collegeId);
        $newSeq = $this->College->findBySequence($data['sequence']);
        if(!empty($newSeq)){
            $newSeq['College']['sequence'] = $oldSeq['College']['sequence'];
            //pr($newSeq);
            $this->College->save($newSeq);
        }
        //pr($oldSeq);die;
        $oldSeq['College']['sequence'] = $data['sequence'];
        $this->College->save($oldSeq);
        $this->redirect($this->referer());
    }

    public function manageGallery($galleryId = null){
        $this->Paginator->settings = array('Gallery' => array('limit' => 4, 'offset' =>  0));
        $galleries=$this->paginate('Gallery');
        if(!empty($galleryId)){
            $gallery = $this->Gallery->findById($galleryId);
            //pr($CollegeFacility );die;
        }
        $this->set(compact('gallery','galleries','galleryId'));
         if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            foreach ($data as $val) {
                if(!empty($val['Gallery']['id'])){
                    $val['Gallery']['last_updated_by'] = $this->Session->read('Admin.id');
                    $val['Gallery']['last_update_date'] = date('Y-m-d');
                }else{
                    $val['Gallery']['created_by'] = $this->Session->read('Admin.id');
                }
                $val['Gallery']['created_date'] = date('Y-m-d H:i:s');
                $this->Gallery->create();
                try{
                    if($this->Gallery->save($val)){
                        $this->Session->write('adminsuccess-msg','Gallery updated successfully.');
                    }else{
                        $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    }
                }
                catch(Exception $e){
                        $this->Session->write('adminerror-msg','Duplicate entry not allowed!!!');
                        $this->redirect(array('action' => 'manageGallery'));
                    }      
            }
            
            $this->redirect(array('action' => 'manageGallery'));
           
            
        }
    }

    public function collegeGallery($collegeId = null){
        $this->CollegeGallery->bindModel(
        array('belongsTo' => array(
                'Gallery' => array(
                    'className' => 'Gallery',
                    'foreignKey' => 'gallery_id'
                    )
                )
            )
        );
        $gallery = $this->Gallery->find('all',array('fields'=>array('id','name')));
        $this->Paginator->settings = array('CollegeGallery' => array('limit' => 25,'conditions'=>array('college_id'=>$collegeId)));
        $galleries=$this->paginate('CollegeGallery');
        //pr($gallery);die;
        $this->set(compact('galleries','gallery','collegeId'));
         if($this->request->is('post')){
            //pr($_FILES);die;
            $destination = realpath('../webroot/img/gallery/small/') . '/';
            $destination1 = realpath('../webroot/img/gallery/large/') . '/';
            $image = $this->Components->load('Resize');
            foreach($_FILES['image']['name'] as $key => $val){
                if (!empty($_FILES['image']['tmp_name'][$key])) 
                {
                    if(is_uploaded_file($_FILES['image']['tmp_name'][$key]))
                    {
                        
                        $filenameimg = time().'-'.$_FILES['image']['name'][$key];
                        $image->resize($_FILES['image']['tmp_name'][$key],$destination.$filenameimg,'aspect_fill',50,50,0,0,0,0);
                        
                        move_uploaded_file($_FILES['image']['tmp_name'][$key], $destination1 . $filenameimg);
                        $data['CollegeGallery']['image']=$filenameimg;
                        
                        
                    }
                }
                $data['CollegeGallery']['college_id']=$collegeId;
                $data['CollegeGallery']['created_date'] = date("Y-m-d");
                //pr($data);die;
                $this->CollegeGallery->create();
                $this->CollegeGallery->save($data);
            }
            
            $this->Session->write('adminsuccess-msg','Gallery updated successfully.');
            
            $this->redirect(array('action' => 'collegeGallery',$collegeId));
           
            
        }
    }

    public function saveGallery(){
        $data = $this->data;
        $this->CollegeGallery->saveAll($data);
        $this->redirect($this->referer());
    }

    public function manageTeam($teamId = null){
        $this->loadModel('Team');
        $teams = $this->Team->find('all', array('limit' => 4, 'offest' => 0));
        if(!empty($teamId)){
            $team = $this->Team->findById($teamId);
        }
        $this->set(compact('team','teams'));
        if($this->request->is('post')){
            $data = $this->data;
            //pr($_FILES);die;
            if (!empty($_FILES['image']['tmp_name'])) 
            {
                $destination = realpath('../webroot/img/userImages/') . '/';
                
                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination . $filenameimg);
                    $data['Team']['image']=$filenameimg;
                    
                    
                }
            }
            if(!empty($data['Team']['id'])){
                    $data['Team']['last_updated_by'] = $this->Session->read('Admin.id');
                    $data['Team']['last_update_date'] = date('Y-m-d');
                }else{
                    $data['Team']['created_by'] = $this->Session->read('Admin.id');
                }
            if($this->Team->save($data)){
                $this->Session->write('adminsuccess-msg','Team updated successfully.');
            }else{
                $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
            }
            $this->redirect(array('action' => 'manageTeam'));
        }
        
    }

    public function manageNews($newsId = null){
        $this->loadModel('New');
        $news = $this->New->find('all', array('limit' => 2, 'offest' => 0));
        //pr($news);die;
        if(!empty($newsId)){
            $new = $this->New->findById($newsId);
        }
        $this->set(compact('new','news'));
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            foreach ($data as $val) {
                if(!empty($val['New']['id'])){
                    $val['New']['last_updated_by'] = $this->Session->read('Admin.id');
                    $val['New']['last_update_date'] = date('Y-m-d');
                }else{
                    $val['New']['created_by'] = $this->Session->read('Admin.id');
                }
                $this->New->create();
                if($this->New->save($val)){
                    $this->Session->write('adminsuccess-msg','News updated successfully.');
                }else{
                    $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                }
            }
            
            $this->redirect(array('action' => 'manageNews'));
        }
        
    }

    public function manageLink($linkId = null){
        $this->loadModel('Link');
        $links = $this->Link->find('all', array('limit' => 3, 'offest' => 0));
        //pr($news);die;
        if(!empty($linkId)){
            $link = $this->Link->findById($linkId);
        }
        $this->set(compact('link','links'));
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            foreach ($data as $val) {
                if(!empty($val['Link']['id'])){
                    $val['Link']['last_updated_by'] = $this->Session->read('Admin.id');
                    $val['Link']['last_update_date'] = date('Y-m-d');
                }else{
                    $val['Link']['created_by'] = $this->Session->read('Admin.id');
                    $val['Link']['created_date'] = date("Y-m-d");
                }
                $this->Link->create();
                if($this->Link->save($val)){
                    $this->Session->write('adminsuccess-msg','Link updated successfully.');
                }else{
                    $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                }
            }
            
            $this->redirect(array('action' => 'manageLink'));
        }
        
    }

    public function manageTestimonial($testimonialId = null){
        $this->loadModel('Testimonial');
        $testimonials = $this->Testimonial->find('all', array('limit' => 3, 'offest' => 0));
        //pr($news);die;
        if(!empty($testimonialId)){
            $testimonial = $this->Testimonial->findById($testimonialId);
        }
        $this->set(compact('testimonials','testimonial'));
        if($this->request->is('post')){
            $data = $this->data;
           // pr($_FILES);
           foreach($data as $key => $val){
                
                if (!empty($_FILES['image']['name'][$key])) 
                {
                    $destination = realpath('../webroot/img/userImages/') . '/';
                    $image = $this->Components->load('Resize');  

                    if(is_uploaded_file($_FILES['image']['tmp_name'][$key]))
                    {
                        
                        $filenameimg = time().'-'.$_FILES['image']['name'][$key];
                        $image->resize($_FILES['image']['tmp_name'][$key],$destination.$filenameimg,'aspect_fill',150,150,0,0,0,0);
                        $val['Testimonial']['image']=$filenameimg;
                        
                        
                    }
                }
               //pr($val);die;
               if(empty($testimonialId))
               {
                    $val['Testimonial']['created_by'] = $this->Session->read('Admin.id');
                }
                $val['Testimonial']['last_update_date'] = date("Y-m-d");
                $val['Testimonial']['last_update_by'] = $this->Session->read('Admin.id');
                //pr($val);die;
                $this->Testimonial->create();
                $this->Testimonial->save($val);
                
            }
               $this->Session->write('adminsuccess-msg','Testimonial updated successfully.');
            
            $this->redirect(array('action' => 'manageTestimonial'));
        }
        
    }

    public function manageSlider($sliderId = null){
        $this->loadModel('Slider');
        $sliders = $this->Slider->find('all', array('limit' => 4, 'offest' => 0));
        if(!empty($sliderId)){
            $slider = $this->Slider->findById($sliderId);
        }
        //pr($sliders);die;
        $this->set(compact('slider','sliders'));
        if($this->request->is('post')){
            $data = $this->data;
            if(!empty($_FILES['image']['name'])) 
            {
                $destination = realpath('../webroot/img/gallery/') . '/';
                $image = $this->Components->load('Resize');  

                if(is_uploaded_file($_FILES['image']['tmp_name']))
                {
                    
                    $filenameimg = time().'-'.$_FILES['image']['name'];
                    move_uploaded_file($_FILES['image']['tmp_name'], $destination . $filenameimg);
                    $data['Slider']['image']=$filenameimg;                            
               }
            }
                //pr($data);die;
                try{
                    if($this->Slider->save($data)){
                        $this->Session->write('adminsuccess-msg','Slider updated successfully.');
                    }else{
                        $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                    }

                    $this->redirect(array('action' => 'manageSlider'));
                }
                catch(Exception $e){
                    //print_r($e);die;
                    //pr($e);die;
                        $this->Session->write('adminerror-msg','Duplicate entry not allowed!!!');
                        $this->redirect(array('action' => 'manageSlider'));
                    } 
        }
    }

    public function collegeMaster($collegeMasterId = null){
        $this->loadModel('CollegeMaster');
        $collegeMasters = $this->CollegeMaster->find('all',array('limit'=>25));
        if(!empty($collegeMasterId)){
           $collegeMaster = $this->CollegeMaster->findById($collegeMasterId); 
        }
        $this->set(compact('collegeMasters','collegeMaster','collegeMasterId'));
        if($this->request->is('post')){
            $data = $this->data;
            foreach($data as $dat)
            {
                $dat['CollegeMaster']['date_created'] = date("Y-m-d");
                //pr($dat);die;
                $this->CollegeMaster->create();
                $this->CollegeMaster->save($dat);
            }
            //pr($data);die;
            // try{
            //     if($this->CollegeMaster->saveAll($data)){
            //         $this->Session->write('adminsuccess-msg','CollegeMaster updated successfully.');
            //     }else{
            //         $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
            //     }
            //     $this->redirect(array('action' => 'collegeMaster'));
            // }
            // catch(Exception $e){
            //         //print_r($e);die;
            //             $this->Session->write('adminerror-msg','Duplicate entry not allowed!!!');
            //             $this->redirect(array('action' => 'collegeMaster'));
            //         }   
        }
    }
    public function manageCollegeField(){
        $fields = $this->CollegeField->find('all',array('limit'=>4, 'offset' => 0));
        $this->set(compact('fields'));
        if($this->request->is('post')){
            $data = $this->data;
            
                //pr($data);die;
            try{
                if($this->CollegeField->saveAll($data)){
                    $this->Session->write('adminsuccess-msg','CollegeFields updated successfully.');
                }else{
                    $this->Session->write('adminerror-msg','Something went wrong,Please try again later!!!');
                }
                $this->redirect(array('action' => 'manageCollegeField'));
            }
            catch(Exception $e){
                    //print_r($e);die;
                        $this->Session->write('adminerror-msg','Duplicate entry not allowed!!!');
                        $this->redirect(array('action' => 'manageCollegeField'));
                    }   
        }
    }

    public function collegeSpecialization($collegeId=null,$collegeCourseId = null,$editId = null){
        $this->loadModel('CourseSpecialization');
        
        
        if(!empty($editId)){
            $this->CourseSpecialization->bindModel(
            array('belongsTo' => array(
                    'Specialization' => array(
                        'className' => 'Specialization',
                        'foreignKey' => 'specialization_id',
                        )
                    )
                )
            );
            $special = $this->CourseSpecialization->findById($editId);    
        }
        $this->CourseSpecialization->bindModel(
        array('belongsTo' => array(
                'Specialization' => array(
                    'className' => 'Specialization',
                    'foreignKey' => 'specialization_id',
                    )
                )
            )
        );
        $specials = $this->CourseSpecialization->find('all',array('conditions'=>array('college_course_id'=>$collegeCourseId)));
        $specializations = $this->Specialization->find('all',array('conditions'=>array('status'=>'1','is_delete'=>0)));
        $this->set(compact('specials','special','collegeCourseId','collegeId','specializations'));
    }
     public function saveCollegeSpecialization($collegeSpecializationId = null){
        $this->loadModel('CourseSpecialization');
        if($this->request->is('post')){
            $data = $this->data;
            //pr($data);die;
            $this->CourseSpecialization->save($data);
            $this->redirect(array('action'=>'collegeSpecialization',$data['CourseSpecialization']['college_id'],$data['CourseSpecialization']['college_course_id']));
        }
    }
    public function collegeContact(){
        $this->loadModel('CollegeContact');
        $this->CollegeContact->bindModel(
            array('belongsTo' => array(
                    'College' => array(
                        'className' => 'College',
                        'foreignKey' => 'college_id',
                        'fields' => 'name'                        
                        )
                    )
                )
            );
        $contacts = $this->CollegeContact->find('all', array('conditions' => array('CollegeContact.is_delete' => 0),'limit' => 10, 'offset' => 0));
        //pr($contacts);die;
        $this->set(compact('contacts'));
    }

    public function manageApplications(){
        $this->loadModel('Order');
        $this->Order->bindModel(
        array('belongsTo' => array(
                'Student' => array(
                    'className' => 'Student',
                    'foreignKey' => 'student_id',
                    'fields' => array('name')
                    )
                )
            )
        );
        //$this->loadModel('OrderApplicationForm');
        $orders = $this->Order->find('all',array('conditions'=>array('Order.status'=>'1')));
        //pr($orders);die;
        $this->set(compact('orders'));
    }
    public function viewOrder($orderId = null){
        //$orderId = base64_decode($orderId);
        $this->loadModel('Order');
        $this->loadModel('OrderApplicationForm');
        $this->loadModel('OrderMeta');
        $this->Order->bindModel(
                array('hasMany' => array(
                        'OrderMeta' => array(
                            'className' => 'OrderMeta',
                            'foreignKey' => 'order_id'
                            )
                        )
                    )
            );
        $this->Order->bindModel(
                array('hasMany' => array(
                        'OrderMeta' => array(
                            'className' => 'OrderMeta',
                            'foreignKey' => 'order_id'
                            )
                        )
                    )
            );
        $this->OrderMeta->bindModel(
            array('belongsTo' => array(
                    'College' => array(
                        'className' => 'College',
                        'foreignKey' => 'college_id',
                        'fields' =>'name'
                        )
                    )
                )
            );
        $this->OrderMeta->bindModel(
            array('hasOne' => array(
                    'DirectApplication' => array(
                        'className' => 'DirectApplication',
                        'foreignKey' => 'order_meta_id'
                        )
                    )
                )
            );
        $this->OrderMeta->bindModel(
            array('belongsTo' => array(
                    'CollegeCourse' => array(
                        'className' => 'CollegeCourse',
                        'foreignKey' => 'course_id',
                        'fields' => array('formsadda_cost','registration_fee','fees','id','college_id','direct_apply','link')
                        )
                    )
                )
            );
        $this->Order->bindModel(
            array('hasOne' => array(
                    'OrderApplicationForm' => array(
                        'className' => 'OrderApplicationForm',
                        'foreignKey' => 'order_id',
                        )
                    )
                )
            );
        $this->Order->bindModel(
            array('hasMany' => array(
                    'AfCustomField' => array(
                        'className' => 'AfCustomField',
                        'foreignKey' => 'order_id',
                        'fields' => array('field_name','value')
                        )
                    )
                )
            );
        $this->Order->bindModel(
            array('hasMany' => array(
                    'AfCompetitiveExam' => array(
                        'className' => 'AfCompetitiveExam',
                        'foreignKey' => 'order_id',
                        'fields' => array('name','year','score','percentile')
                        )
                    )
                )
            );
        $this->Order->bindModel(
            array('hasMany' => array(
                    'AfStudentExperience' => array(
                        'className' => 'AfStudentExperience',
                        'foreignKey' => 'order_id',
                        'fields' => array('name','designation','joining','releasing')
                        )
                    )
                )
            );
        $this->Order->bindModel(
            array('hasMany' => array(
                    'AfStudentAcademic' => array(
                        'className' => 'AfStudentAcademic',
                        'foreignKey' => 'order_id',
                        'fields' => array('exam','passing_year','institute','university','percentage')
                        )
                    )
                )
            );
        $this->OrderApplicationForm->bindModel(
            array('belongsTo' => array(
                    'City' => array(
                        'className' => 'City',
                        'foreignKey' => 'city_id',
                        
                        )
                    )
                )
            );
        $this->OrderApplicationForm->bindModel(
            array('belongsTo' => array(
                    'State' => array(
                        'className' => 'State',
                        'foreignKey' => 'state_id',
                        
                        )
                    )
                )
            );
        $this->OrderApplicationForm->bindModel(
            array('belongsTo' => array(
                    'Country' => array(
                        'className' => 'Country',
                        'foreignKey' => 'country_id',
                        
                        )
                    )
                )
            );
        $this->Order->recursive=3;
        $order = $this->Order->findById($orderId);
        //pr($order);die;
        $this->set(compact('order'));
    }

    public function trending($collegeId = null){
        $this->loadModel('College');

        $status = $this->College->findById($collegeId);
        
        if($status['College']['trending'] == '1'){
            $status['College']['trending'] = '0';
        }else{
            $status['College']['trending'] = '1';
        }
        
        $this->College->id = $collegeId;
        $this->College->saveField('trending',$status['College']['trending']);
        echo $status['College']['trending'];
        die;
    }
}
