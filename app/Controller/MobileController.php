<?php
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('HttpSocket', 'Network/Http');
App::uses('Xml', 'Utility');
ob_start();
class MobileController extends AppController
{
    public $name = "MobileController";
    var $uses = array('Review','ReviewCount','OrderMeta','Order','Cart','StudentPlan','College','Country','State','City','Student','CollegeFacility','CollegeMeta','Course','Facility','CollegeGallery','CollegeCourse','CollegeContact','CollegeCompare','Program','Course','Stream','CollegeMaster','MembershipPlan');

    function Login(){
        //$this->loadModel('User');
        $user = $this->decode_request();
        //pr($user);die;
        $data = array();
        if(empty($user->student['0']->email)){
            $data['status'] = false;
            $data['msg'] = 'Email can not empty';
            echo json_encode($data);
            die;
        }
        if(empty($user->student['0']->password)){
            $data['status'] = false;
            $data['msg'] = 'Email can not empty';
            echo json_encode($data);
            die;
        }
        $email = $user->student['0']->email; //set user email by login
        $password = md5($user->student['0']->password); //set user password by login

        $result=$this->Student->find('first', array('conditions'=>array('Student.email'=>$email, 'Student.password'=>$password))); 
        //pr($count);die;   
        
        if(count($result)>0){
            if($result['Student']['activation_status'] == '0'){
                $resp['status'] ='false';
                $resp['msg'] = 'Please activate your account!!!';
            }else if($result['Student']['status'] == '0'){
                $resp['status'] ='false';
                $resp['msg'] = 'Your account is disabled by admin.Please contact Admin!!!';
            }else{
                $resp['Student'] = $result['Student'];
                $resp['status'] ='true'; 
               $resp['msg'] = 'Successfull';
            }
        }else{
            $resp['status'] = 'false';
            $resp['msg'] = 'Invalid login credentials!!!';

        }
        echo json_encode($resp);
        die;
    }
    public function decode_request()
    {
        return $this->request->input('json_decode',false);
    }
    public function signup_student(){

        $user = $this->decode_request();

        if(empty($user->student['0']->email)) {
            $data['status'] = false;
            $data['msg'] = 'Email can not empty';
            echo json_encode($data);
            die;
        }
        if(empty($user->student['0']->password)){
            $data['status'] = false;
            $data['msg'] = 'Password can not empty';
            echo json_encode($data);
            die;
        }
        if(empty($user->student['0']->name)){
            $data['status'] = false;
            $data['msg'] = 'Name can not empty';
            echo json_encode($data);
            die;
        }
        if($user->student['0']->gender == ""){
            $data['status'] = false;
            $data['msg'] = 'gender can not empty';
            echo json_encode($data);
            die;
        }
        if(empty($user->student['0']->dob)){
            $data['status'] = false;
            $data['msg'] = 'Email can not empty';
            echo json_encode($data);
            die;
        }
        if(empty($user->student['0']->mobile)){
            $data['status'] = false;
            $data['msg'] = 'Mobile can not empty';
            echo json_encode($data);
            die;
        }
        $detail['Student']['name'] = $user->student['0']->name;
        $detail['Student']['email'] = $user->student['0']->email;
        $detail['Student']['password'] = md5($user->student['0']->password);
       
        $detail['Student']['dob'] = $user->student['0']->dob;
        $detail['Student']['gender'] = $user->student['0']->gender;
        $detail['Student']['mobile'] = $user->student['0']->mobile;
        //$detail['Student']['cpassword'] = $user->Student['0']->address;
        //pr($detail);die;
        $srch=$this->Student->find('first', array('conditions'=>array('Student.email'=>$detail['Student']['email'])));
        if(!empty($srch)){
            $data['status'] = false;
            $data['msg'] = 'Email id already exist';
        }
        else{
            if($this->Student->save($detail, array('validate' => false))){
                $studentId = $this->Student->getLastInsertId();
                $count=$this->Student->find('first', array('conditions'=>array('Student.id'=>$studentId)));
                //pr($count);die;
                $data['status'] = true;
                $data['msg'] = 'successfull';
                $data['Student'] = $count['Student'];

                $link = HTTP_ROOT.'Homes/activateStudent/'.base64_encode($studentId);
                /*$replace = array('{name}','{email}','{password}','{link}');
                $with = array($count['Student']['name'],$count['Student']['email'],$user->student['0']->password,$link);
                $this->send_email('',$replace,$with,'confirm_registration',INFO_FORMSADDA,$count['Student']['email']);*/

                $mail = '<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>Hello '.$count['Student']['name'].' ,</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>Thanks for joining us at GetCollege.in.<br />
            Your account has been successfully created on the GetCollege.in website. Here are your Account credentials:<br />
            <br />
            Login name: '.$count['Student']['email'].'<br />
            Password : '.$user->student['0']->password.'<br />
            Click on this link to activate your account <a href="'.$link.'">Clik here to activate</a></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">In case you have questions or need further assistance, call us on +120 4202822 | +91 9958330934. We would be glad to help you. We are available 24 hours a day, 7 days a week. getincolleges@gmail.com</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
            <p>Regards,<br />
            Team GetCollege.in<br />
            <br />
            <strong>Note</strong>: This is a system generated email.&nbsp;Please do not reply.</p>

            <p>&nbsp;</p>

            <hr />
            <p>Notice: The information in this email and in any attachments is confidential and intended solely for the attention and use of the named addressee. This information may be subject to legal professional or other privilege or may otherwise be protected by work product immunity or other legal rules. It must not be disclosed to any person without authorization. If you are not the intended recipient, or a person responsible for delivering it to the intended recipient, you are not authorized and must not disclose, copy, distribute, or retain this message or any part of it.</p>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
    </tbody>
</table>';
                $Email = new CakeEmail();

                $Email->emailFormat('html')
                  ->from(array('info@getcollege.in' => 'GetCollege'))
                  ->to($count['Student']['email'])
                  ->subject('New Registration')
                  ->send($mail);
            }
            else{
                $data['status'] = false;
                $data['msg'] = 'Unsuccessfull';
            }
        }
        echo json_encode($data);
        die;
    }

    
}
?>