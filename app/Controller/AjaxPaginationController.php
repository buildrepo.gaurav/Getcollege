<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AjaxPaginationController extends AppController {
	
	public $components = array('Session','Email','RequestHandler','Cookie','Paginator');
	public $helpers = array('Session','Html');
    var $layout = 'admin';
    var $uses = array('Admin','College','CollegeMeta','Facility','CollegeFacility','CollegeNear','CollegeCourse','Degree','Stream','Program','Course','Specialization','Event','Gallery','CollegeGallery','New','Link','Testimonial','Slider','Country','State','City','Student');
    public function beforeFilter()
    {
        parent::beforeFilter();
        $actions = array('login','forgetPassword');
        if(!in_array($this->params['action'], $actions))
        {
            if(!$this->Session->check('Admin') )
            {
            	$this->redirect(array('controller'=>'Admins','action'=>'login'));
        	}
   		}else{
   			if($this->Session->check('Admin'))
            {
            	$this->redirect(array('controller'=>'Admins','action'=>'dashboard'));
        	}
   		}

        $adminDetails = $this->Admin->findById($this->Session->read('Admin.id'));
        $this->set(compact('adminDetails'));
  

    }
    public function getCollege(){
        $this->College->bindModel(
        array('belongsTo' => array(
                'CreatedBy' => array(
                    'className' => 'Admin',
                    'foreignKey' => 'created_by',
                    'fields' => array('username')
                    )
                )
            )
        );
        $this->College->bindModel(
        array('belongsTo' => array(
                'UpdatedBy' => array(
                    'className' => 'Admin',
                    'foreignKey' => 'updated_by',
                    'fields' => array('username')
                    )
                )
            )
        );
        $data = $this->request->data;
        $colleges = $this->College->find('all', array('limit' => 25, 'offset' => $data['offset']));
        echo json_encode($colleges);die;
        
    }
    public function getFacility(){
        $data = $this->request->data;
        $facilities = $this->Facility->find('all', array('limit' => 3, 'offset' => $data['offset']));
        echo json_encode($facilities);die;
        
    }
    public function getStream(){
        $data = $this->request->data;
        $streams = $this->Stream->find('all', array('order' => array('Stream.id' => 'DESC'), 'limit' => 4, 'offset' => $data['offset']));
        echo json_encode($streams);die;
        
    }
    public function getProgram(){
        $data = $this->request->data;
        $this->Program->bindModel(
        array('belongsTo' => array(
                'Stream' => array(
                    'className' => 'Stream',
                    'foreignKey' => 'stream_id'
                    )
                )
            )
        );
        $programs = $this->Program->find('all', array('limit' => 4, 'offset' => $data['offset']));
        echo json_encode($programs);die;
        
    }
    public function getCourse(){
        $data = $this->request->data;
        $this->Course->bindModel(
        array('belongsTo' => array(
                'Program' => array(
                    'className' => 'Program',
                    'foreignKey' => 'program_id'
                    )
                )
            )
        );
        $courses = $this->Course->find('all', array('limit' => 4, 'offset' => $data['offset']));
        echo json_encode($courses);die;
        
    }
    public function getSpecial(){
        $data = $this->request->data;
        $this->Specialization->bindModel(
        array('belongsTo' => array(
                'Course' => array(
                    'className' => 'Course',
                    'foreignKey' => 'course_id'
                    )
                )
            )
        );
        $special = $this->Specialization->find('all', array('limit' => 4, 'offset' => $data['offset']));
        echo json_encode($special);die;
        
    }
    public function getGallery(){
        $data = $this->request->data;
        $gallery = $this->Gallery->find('all', array('limit' => 4, 'offset' => $data['offset']));
        echo json_encode($gallery);die;
        
    }
    public function getTeam(){
        $this->loadModel('Team');
        $data = $this->request->data;
        $gallery = $this->Team->find('all', array('limit' => 4, 'offset' => $data['offset']));
        echo json_encode($gallery);die;
        
    }
    public function getNews(){
        $data = $this->request->data;
        $gallery = $this->New->find('all', array('limit' => 2, 'offset' => $data['offset']));
        echo json_encode($gallery);die;
        
    }
    public function getLink(){
        $data = $this->request->data;
        $gallery = $this->Link->find('all', array('limit' => 3, 'offset' => $data['offset']));
        echo json_encode($gallery);die;
        
    }
    public function getTesti(){
        $data = $this->request->data;
        $gallery = $this->Testimonial->find('all', array('limit' => 3, 'offset' => $data['offset']));
        echo json_encode($gallery);die;
        
    }
    public function getSlider(){
        $data = $this->request->data;
        $gallery = $this->Slider->find('all', array('limit' => 3, 'offset' => $data['offset']));
        echo json_encode($gallery);die;
        
    }
    public function getReference(){
        $this->loadModel('ReferenceList');
        $data = $this->request->data;
        $this->ReferenceList->bindModel(
        array('belongsTo' => array(
                'Student' => array(
                    'className' => 'Student',
                    'foreignKey' => 'referrer_id',
                    'fields' => array('name')
                    )
                )
            )
        );
        $reference = $this->ReferenceList->find('all', array('limit' => 5, 'offset' => $data['offset']));
        echo json_encode($reference);die;
        
    }
    public function getCoupon(){
        
        $this->loadModel('Coupon');
        $this->loadModel('CouponType');
        $this->Coupon->bindModel(
            array('belongsTo' => array(
                    'CouponType' => array(
                        'className' => 'CouponType',
                        'foreignKey' => 'Coupon_type_id'
                        )
                    )
                )
        );
        $data = $this->request->data;
        $coupons = $this->Coupon->find('all', array('limit' => 3, 'offset' => $data['offset']));
        echo json_encode($coupons);die;
        
    }
    public function getState(){
        $data = $this->request->data;
        $this->State->bindModel(
            array('belongsTo' => array(
                    'Country' => array(
                        'className' => 'Country',
                        'foreignKey' => 'country_id'
                        )
                    )
                )
            );
        $countries = $this->State->find('all', array('limit' => 10, 'offset' => $data['offset']));
        //$gallery = $this->Slider->find('all', array('limit' => 3, 'offset' => $data['offset']));
        echo json_encode($countries);die;
        
    }
    public function getCity(){
        $data = $this->request->data;
        $this->City->bindModel(
        array('belongsTo' => array(
                'State' => array(
                    'className' => 'State',
                    'foreignKey' => 'state_id'
                    )
                )
            )
        );
        $countries = $this->City->find('all', array('limit' => 10, 'offset' => $data['offset']));
        echo json_encode($countries);die;
        
    }
    public function getStudent(){
        $data = $this->request->data;
        $countries = $this->Student->find('all', array('limit' => 10, 'offset' => $data['offset']));
        echo json_encode($countries);die;
    }
     public function getReferenceamt(){
        $this->loadModel('ReferenceAmount');
        $data = $this->request->data;
        $reference = $this->ReferenceAmount->find('all', array('limit' => 5, 'offset' => $data['offset']));
        echo json_encode($reference);die;
        
    }

    public function getExam(){
        $this->loadModel('Exam');
        $data = $this->request->data;
        $exams = $this->Exam->find('all', array('order' => array('Exam.id' => 'DESC'), 'limit' => 4, 'offset' => $data['offset']));
        echo json_encode($exams);die;
        
    }
    public function getPlan(){
        $this->loadModel('MembershipPlan');
        $data = $this->request->data;
        $exams = $this->MembershipPlan->find('all', array('limit' => 4, 'offset' => $data['offset']));
        echo json_encode($exams);die;
        
    }
    public function getFields(){
        $this->loadModel('CollegeField');
        $data = $this->request->data;
        $exams = $this->CollegeField->find('all', array('limit' => 4, 'offset' => $data['offset']));
        echo json_encode($exams);die;
        
    }
    public function getNotify(){
        $this->loadModel('Notify');
        $this->Notify->bindModel(
            array('belongsTo' => array(
                    'City' => array(
                        'className' => 'City',
                        'foreignKey' => 'city_id',
                        'fields' => 'city_name'                        
                        )
                    )
                )
            );
        $this->Notify->bindModel(
            array('belongsTo' => array(
                    'State' => array(
                        'className' => 'State',
                        'foreignKey' => 'state_id',
                        'fields' => 'statename'                        
                        )
                    )
                )
            );
        $this->Notify->bindModel(
            array('belongsTo' => array(
                    'Country' => array(
                        'className' => 'Country',
                        'foreignKey' => 'country_id',
                        'fields' => 'country_name'                        
                        )
                    )
                )
            );
        $this->Notify->bindModel(
            array('belongsTo' => array(
                    'Course' => array(
                        'className' => 'College',
                        'foreignKey' => 'course_id'                        
                        )
                    )
                )
            );
        $data = $this->request->data;
        $exams = $this->Notify->find('all', array('conditions' => array('Notify.is_delete' => 0),'limit' => 10, 'offset' => $data['offset']));
        echo json_encode($exams);die;
        
    }
    public function getCollegeContact(){
        $this->loadModel('CollegeContact');
        $this->CollegeContact->bindModel(
            array('belongsTo' => array(
                    'College' => array(
                        'className' => 'College',
                        'foreignKey' => 'college_id',
                        'fields' => 'name'                        
                        )
                    )
                )
            );
        $contacts = $this->CollegeContact->find('all', array('conditions' => array('CollegeContact.is_delete' => 0),'limit' => 10, 'offset' => $data['offset']));
        //pr($contacts);die;
        echo json_encode($exams);die;
    }

}
