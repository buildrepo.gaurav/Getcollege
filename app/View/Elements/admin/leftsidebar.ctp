 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo HTTP_ROOT?>img/adminImages/small/<?php echo $adminDetails['Admin']['image']?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $adminDetails['Admin']['username']?></p>
          <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <!-- <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li> -->
        <li class="active"><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-circle-o text-red"></i> <span>Dashboard</span></a></li>
        <?php if($this->Session->read('Admin.type') == 0){?>
        <li class="treeview">
          <a href="javascript:void(0);">
            <i class="fa fa-pie-chart"></i>
            <span>College Management </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu">
            <li>
              <a href="<?php echo HTTP_ROOT.'Colleges/manageCollege'?>">
               <i class="fa fa-laptop"></i>
               Manage College
              </a>
            </li>
            <li>
              <a href="<?php echo HTTP_ROOT.'Colleges/addCollege'?>">
               <i class="fa fa-laptop"></i>
               Add College
              </a>
            </li>
            <li>
              <a href="<?php echo HTTP_ROOT.'Colleges/collegeContact'?>">
               <i class="fa fa-laptop"></i>
               College Contact
              </a>
            </li>
           </ul>   
        </li>
        <li class="treeview">
          <a href="javascript:void(0);">
            <i class="fa fa-pie-chart"></i>
            <span>Student Management </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu">
            <li>
              <a href="<?php echo HTTP_ROOT.'Students/manageStudent'?>">
               <i class="fa fa-laptop"></i>
               Manage Student List
              </a>
            </li>
            <li>
              <a href="<?php echo HTTP_ROOT.'Students/addStudent'?>">
               <i class="fa fa-laptop"></i>
               Add Student
              </a>
            </li>
           </ul>   
        </li>
        
         
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageStream"><i class="fa fa-circle-o"></i> Stream</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageProgram"><i class="fa fa-circle-o"></i> Program</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageCourse"><i class="fa fa-circle-o"></i> Course</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageSpecialization"><i class="fa fa-circle-o"></i> Specialization</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageGallery"><i class="fa fa-circle-o"></i> Gallery</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageTeam"><i class="fa fa-circle-o"></i> Team</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageNews"><i class="fa fa-circle-o"></i> News</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageLink"><i class="fa fa-circle-o"></i> Important Links</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageTestimonial"><i class="fa fa-circle-o"></i> Testimonials</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageSlider"><i class="fa fa-circle-o"></i> Slider</a></li>
            <li>
              <a href="<?php echo HTTP_ROOT.'Colleges/manageFacility'?>">
               <i class="fa fa-laptop"></i>
               Manage Facility
              </a></li>
            <li><a href="<?php echo HTTP_ROOT?>Admins/manageCountry"><i class="fa fa-circle-o"></i> Country</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Admins/manageState"><i class="fa fa-circle-o"></i> State</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Admins/manageCity"><i class="fa fa-circle-o"></i> City</a></li>
            <!-- <li><a href="<?php echo HTTP_ROOT?>Colleges/collegeMaster"><i class="fa fa-circle-o"></i> College</a></li> -->
            <li><a href="<?php echo HTTP_ROOT?>Admins/manageExam"><i class="fa fa-circle-o"></i> Exams</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageCollegeField"><i class="fa fa-circle-o"></i> Manage College Fields</a></li>

          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Cms</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo HTTP_ROOT?>Cms/manageAboutus"><i class="fa fa-circle-o"></i> About us</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Cms/manageContact"><i class="fa fa-circle-o"></i> Contact us</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Cms/manageFaq"><i class="fa fa-circle-o"></i> Faqs</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Cms/manageTerm"><i class="fa fa-circle-o"></i> Terms & Conditions</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Cms/managePrivacy"><i class="fa fa-circle-o"></i> Privacy policy</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Cms/manageEmailTemp"><i class="fa fa-circle-o"></i> Email Templete</a></li>
          </ul>
        </li>
        <!-- <li>
          <a href="<?php echo HTTP_ROOT?>Admins/notifyList">
            <i class="fa fa-th"></i> <span>Notify List</span>
            </span>
          </a>
        </li> -->
        <!-- <li>
          <a href="<?php echo HTTP_ROOT?>Admins/manageMembership">
            <i class="fa fa-th"></i> <span>Manage Membership Plan</span>
            </span>
          </a>
        </li> -->
        <li>
          <a href="<?php echo HTTP_ROOT?>Admins/manageReviews">
            <i class="fa fa-th"></i> <span>Manage Reviews</span>
            </span>
          </a>
        </li>
        <li>
          <a href="<?php echo HTTP_ROOT?>Admins/gallery">
            <i class="fa fa-th"></i> <span>Gallery</span>
            </span>
          </a>
        </li>
         <!-- <li>
          <a href="<?php echo HTTP_ROOT?>Cms/coupon">
            <i class="fa fa-th"></i> <span>Manage Coupons</span>
            </span>
          </a>
        </li>
         <li>
          <a href="<?php echo HTTP_ROOT?>Colleges/manageApplications">
            <i class="fa fa-th"></i> <span>Manage Applications</span>
            </span>
          </a>
        </li> -->
        
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Refer A Friend</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo HTTP_ROOT?>Cms/manageReference">
            <i class="fa fa-circle-o"></i> Reference Amount
          </a></li>
            <li><a href="<?php echo HTTP_ROOT?>Cms/referenceList"><i class="fa fa-circle-o"></i>Reference List</a></li>
           

          </ul>
        </li> -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Manage Test</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTestLevel"><i class="fa fa-circle-o"></i> Manage Test Level</a></li>
            <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTest"><i class="fa fa-circle-o"></i> Manage Test </a></li>
            <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTestSection"><i class="fa fa-circle-o"></i> Manage Test Section</a></li>
            <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTestQuestion"><i class="fa fa-circle-o"></i> Manage Test Question</a></li>
          </ul>
        </li>
        <?php } elseif($this->Session->read('Admin.type') == 1){?>
        <li class="treeview">
          <a href="javascript:void(0);">
            <i class="fa fa-pie-chart"></i>
            <span>College Management </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu">
            <li>
              <a href="<?php echo HTTP_ROOT.'Colleges/manageCollege'?>">
               <i class="fa fa-laptop"></i>
               Manage College
              </a>
            </li>
            <li>
              <a href="<?php echo HTTP_ROOT.'Colleges/addCollege'?>">
               <i class="fa fa-laptop"></i>
               Add College
              </a>
            </li>
            <li>
              <a href="<?php echo HTTP_ROOT.'Colleges/collegeContact'?>">
               <i class="fa fa-laptop"></i>
               College Contact
              </a>
            </li>
           </ul>   
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageStream"><i class="fa fa-circle-o"></i> Stream</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageProgram"><i class="fa fa-circle-o"></i> Program</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageCourse"><i class="fa fa-circle-o"></i> Course</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Colleges/manageSpecialization"><i class="fa fa-circle-o"></i> Specialization</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Admins/manageCountry"><i class="fa fa-circle-o"></i> Country</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Admins/manageState"><i class="fa fa-circle-o"></i> State</a></li>
            <li><a href="<?php echo HTTP_ROOT?>Admins/manageCity"><i class="fa fa-circle-o"></i> City</a></li>

          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Manage Test</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTestLevel"><i class="fa fa-circle-o"></i> Manage Test Level</a></li>
            <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTest"><i class="fa fa-circle-o"></i> Manage Test </a></li>
            <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTestSection"><i class="fa fa-circle-o"></i> Manage Test Section</a></li>
            <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTestQuestion"><i class="fa fa-circle-o"></i> Manage Test Question</a></li>
          </ul>
        </li>
        <?php }?>
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li>
        <li>
          <a href="pages/widgets.html">
            <i class="fa fa-th"></i> <span>Widgets</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Charts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>UI Elements</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
            <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
            <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
            <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
            <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
            <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Forms</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
            <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
            <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
          </ul>
        </li>
        <li>
          <a href="pages/calendar.html">
            <i class="fa fa-calendar"></i> <span>Calendar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
        <li>
          <a href="pages/mailbox/mailbox.html">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Examples</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
            <li><a href="pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
            <li><a href="pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
            <li><a href="pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
            <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
            <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
            <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
            <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
            <li><a href="pages/examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li>
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>
        <li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>