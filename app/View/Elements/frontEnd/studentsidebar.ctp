<style>
  .profile-sidebar1{
    padding: 20px 0 10px 0;
      background: #fff;
  }
  .pic_chnge_cam
  {
  	display: none;
  	position: absolute;
  }
  .profile-userpic:hover .pic_chnge_cam
  {
  	display: block;
  }
</style>
<div class="col-md-3">
				<div class="profile-sidebar1">
					<!-- SIDEBAR USERPIC -->
					
					<div class="profile-userpic">
						<a class="" data-toggle="modal" href="javascript:void(0)" data-target="#student-image">
							<div class="pic_chnge_cam" style="color:#008190; padding-left: 57%;"><i class="fa fa-camera" aria-hidden="true"></i></div>

							<img src="<?php echo HTTP_ROOT?>img/<?php echo empty($details['Student']['image']) ? 'default-user.png' : 'studentImages/large/'.$details['Student']['image'];?>" class="img-responsive" alt="">
						</a>
					</div>
					<!-- END SIDEBAR USERPIC -->
					<!-- SIDEBAR USER TITLE -->
					<div class="profile-usertitle">
						<div class="profile-usertitle-name">
							 <?php echo $details['Student']['name'];?>
						</div>
						<div class="profile-usertitle-job">
							<?php echo $details['Student']['email'];?>
						</div>
					</div>
					<!-- END SIDEBAR USER TITLE -->
					<!-- SIDEBAR BUTTONS -->
					<div class="profile-userbuttons">
					

						<!-- <a data-toggle="modal" href="javascript:void(0)" data-target="#student-password-update" style="background:#007d94;color:#fff" class="btn btn-sm">Change Password</a> -->
					</div>
					<!-- END SIDEBAR BUTTONS -->
					<!-- SIDEBAR MENU -->
					<div class="profile-usermenu">
						<ul class="nav">
							<li class="<?php echo $this->params['action'] == 'studentdashboard' ? 'active' : '';?>">
								<a href="<?php echo HTTP_ROOT?>Homes/studentdashboard">
								<i class="glyphicon glyphicon-home"></i>
								Dashboard </a>
							</li>
							<li>
								<a href="<?php echo HTTP_ROOT?>Homes/addMember/<?php echo base64_encode($details['Student']['id']);?>">
								<i class="fa fa-edit"></i>
								Edit Profile </a>
							</li>
							<!-- <li>
								<a href="<?php echo HTTP_ROOT?>Homes/memberList">
								<i class="glyphicon glyphicon-user"></i>
								Memeber List </a>
							</li>
							<li>
								<a href="<?php echo HTTP_ROOT?>Homes/addMember">
								<i class="fa fa-plus"></i>
								Add More Applicants Details </a>
							</li> -->
							
							<li>
								<a href="<?php echo HTTP_ROOT?>Homes/compareList">
								<i class="fa fa-exchange"></i>
								Compare List </a>
							</li>
							<li>
								<a href="<?php echo HTTP_ROOT?>Homes/studentWallet">
								<i class="fa fa-credit-card"></i>
								Wallet </a>
							</li>
							<!-- <li>
								<a href="<?php echo HTTP_ROOT?>Homes/formApplied">
								<i class="fa fa-credit-card"></i>
								My Applications </a>
							</li> -->
							<li>
								<a href="<?php echo HTTP_ROOT?>Homes/testDetail">
								<i class="fa fa-credit-card"></i>
								Take Test </a>
							</li>
							<li>
								<a href="<?php echo HTTP_ROOT?>Homes/testScore">
								<i class="fa fa-credit-card"></i>
								Test Results </a>
							</li>
						</ul>
					</div>
					<!-- END MENU -->
				</div>
			</div>