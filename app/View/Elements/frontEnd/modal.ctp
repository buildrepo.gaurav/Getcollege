<style>
.has-success{
    border-color: #219174 !important;
    -webkit-box-shadow: none;
    box-shadow: none;
}
.has-error{
    border-color: #f13e64 !important;
    -webkit-box-shadow: none;
    box-shadow: none;
}
.input-error{
    float: right;
    right: 8px;
    margin: 11px 13px;
    position: absolute;
}
.fa-warning{
    color : red;
}
.fa-check{
    color :green;
}
.input-icon.right > i {
    right: 8px;
    float: right;
}
.input-icon > i {
    display: block;
    position: absolute;
    margin: 11px 12px 4px 10px;
    z-index: 3;
    width: 16px;
    font-size: 16px;
    text-align: center;
}
.select2{
    width: 100% !important;
}

      </style>
        <div class="modal fade" id="my_Login12" role="dialog">
            <div class="modal-dialog">
                <div class="">
                    <div class="modal-header color_bg-header">
                        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-12 col-md-offset_4 col-md-offset-3 col-sm-8 col-sm-offset-2">
                            <div class="panel panel-info" >
                                <div class="panel-heading" style="background: #36454F; color:#fff;">
                                    <button type="button" class="close close_2" data-dismiss="modal">&times;</button>
                                    <div class="panel-title">Sign In</div>
                                </div>
                                <div style="padding-top:30px" class="panel-body" >
                                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                                    <form id="loginform" class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="top_button_2">
                                                        <input type="radio" name="login" class="login-cls" id="login-std"> Student&nbsp;&nbsp;
                                                        <input type="radio" name="login" class="login-cls" id="login-col"> College
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="tab-content">
                                        <div class="modal-body firstid_3_cls color_bg-header2 tab-pane fade in active" id="firstid_3">
                                            <form id="std-login-form" method="post" action="<?php echo HTTP_ROOT.'/Homes/studentLogin'?>" class="form-horizontal" role="form">
                                                <div style="margin-bottom: 25px" class="input-group">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                    <i class="fa input-error"></i>
                                                    <input id="login-username" type="text" class="form-control required" name="username" value="" placeholder="username or email or mobile">                                        
                                                </div>
                                                <div style="margin-bottom: 25px" class="input-group">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                                    <i class="fa input-error"></i>
                                                    <input id="login-password" type="password" class="form-control required" name="password" placeholder="password">
                                                    
                                                </div>
                                                <p class="login-error-msg" style="color:red;"></p>

                                                <a href="javascript:void(0)" data-target="#forgotPassword" data-toggle="modal" class="pull-right">Forgot password?</a>
                                                <div class="input-group">
                                                    <div class="checkbox">
                                                        <label>
                                                        <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                                        </label>
                                                    </div>
                                                </div>
                                                <div style="margin-top:10px" class="form-group">
                                                    <!-- Button -->
                                                    <div class="col-sm-12 controls">
                                                        <button id="btn-login" href="#" class="btn btn-success">Login  </button>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12 control">
                                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                                            Don't have an account! 
                                                            <a data-toggle="modal" href="#myModall" data-target="#my_Login13" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link ">
                                    <i class="fa fa-user"></i> Registration &nbsp; &nbsp;</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-body firstid_2_cls color_bg-header2 tab-pane fade" id="firstid_2">
                                            <form id="clg-login-form" class="form-horizontal" role="form" action="<?php echo HTTP_ROOT?>Homes/collegeLogin" method="post">
                                                <div style="margin-bottom: 25px" class="input-group">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                    <i class="fa input-error"></i>
                                                    <input id="login-username" type="text" class="form-control required" name="username" value="" placeholder="username">                                        
                                                </div>
                                                <div style="margin-bottom: 25px" class="input-group">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                                    <i class="fa input-error"></i>
                                                    <input id="login-password" type="password" class="form-control required" name="password" placeholder="password">
                                                </div>
                                                <p class="clg-login-error-msg" style="color:red;"></p>
                                                <a href="javascript:void(0)" data-target="#forgotPassword" data-toggle="modal" class="pull-right">Forgot password?</a>
                                                <div class="input-group">
                                                    <div class="checkbox">
                                                        <label>
                                                        <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                                        </label>
                                                    </div>
                                                </div>
                                                <div style="margin-top:10px" class="form-group">
                                                    <!-- Button -->
                                                    <div class="col-sm-12 controls">
                                                        <button id="btn-login" href="#" class="btn btn-success" type="submit">Login  </button>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12 control">
                                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                                            Don't have an account! 
                                                            <a href="javascript:void(0)" data-target="#my_Login13" data-toggle="modal">
                                                            Sign Up Here
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div id="signupbox" style="display:none; margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">Sign Up</div>
                                    <div style="float:right; font-size: 85%; position: relative; top:-10px">
                                        <a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a>
                                    </div>
                                </div>
                                <div class="panel-body" >
                                    <form id="signupform" class="form-horizontal" role="form">
                                        <div id="signupalert" style="display:none" class="alert alert-danger">
                                            <p>Error:</p>
                                            <span></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-md-3 control-label">Email</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="email" placeholder="Email Address">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="firstname" class="col-md-3 control-label">First Name</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="firstname" placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname" class="col-md-3 control-label">Last Name</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="lastname" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-md-3 control-label">Password</label>
                                            <div class="col-md-9">
                                                <input type="password" class="form-control" name="passwd" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="icode" class="col-md-3 control-label">Invitation Code</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="icode" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <!-- Button ->                                        
                                            <div class="col-md-offset-3 col-md-9">
                                                <button id="btn-signup" type="button" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Sign Up</button>
                                                <span style="margin-left:8px;">or</span>  
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="my_Login13" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content modal_content_123">
                    <div class="modal-header" style="background: #36454F; color:#fff;">
                        
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h2 class="Regist_color" style="color:#fff;">Registration</h2>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="top_button_2">
                                            
                                            <form class="form-horizontal" role="form">
                                            <input type="radio" name="regis" class="reg-cls" id="reg-std" checked> Student&nbsp;&nbsp;
                                            <input type="radio" name="regis" class="reg-cls" id="reg-col"> College
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                    <div class="tab-content">
                            
                        <div class="modal-body color_bg-header2 tab-pane fade" id="firstid">
                            <form id="college-signup" class="form-horizontal" role="form" method="post" action="<?php echo HTTP_ROOT.'Homes/collegeSignup'?>">
                                <div class="form-group">
                                    <label for="firstName" class="col-sm-3 control-label label_left">College Name</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="text" id="firstName" placeholder="College Name" class="form-control required" autofocus name="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="birthDate" class="col-sm-3 control-label label_left">Address</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="text" class="form-control required" placeholder="Address" name="address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="country" class="col-sm-3 control-label label_left">Country</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <select id="country" class="form-control required " name="country_id">
                                            <option value="">Select Country</option>
                                            <?php foreach($country as $coun){?>
                                            <option value="<?php echo $coun['Country']['id']?>"><?php echo $coun['Country']['country_name']?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="state" class="col-sm-3 control-label label_left">State *</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <select id="state" name="state_id" class="form-control required">
                                            <option value="">Select State</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="state" class="col-sm-3 control-label label_left">City *</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <select id="city" name="city_id" class="form-control required">
                                            <option value="">Select City</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="email" class="col-sm-3 control-label label_left">Email Id</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="email" id="email" placeholder="Email" class="form-control required" name="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-3 control-label label_left">Password</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="password" id="add-new-pass" placeholder="Password" class="form-control required" name="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-3 control-label label_left">Confirm Password *</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="password" id="add-confirm-pass" placeholder="Confirm Password" class="form-control required" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="code" class="col-sm-3 control-label label_left">Mobile Number *</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="text" placeholder="Mobile Number" class="form-control required number" name="phone1" minlength="10" maxlength="10" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="code" class="col-sm-3 control-label label_left">Alternate Number</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="text" placeholder="Alternate Number" class="form-control number" minlength="10" maxlength="10" name="phone2">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="code" class="col-sm-3 control-label label_left">Website</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="text" id="website" placeholder="Website" class="form-control" name="website">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <div class="checkbox">
                                            <label class="label_left">
                                            <input type="checkbox" class="required">I accept <a href="<?php echo HTTP_ROOT?>Homes/terms" target="_blank">terms</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary btn-block">Register</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-body color_bg-header2 tab-pane fade in active" id="secend-id" style="    padding: 15px 15px;">
                            <form class="form-horizontal" role="form" id="student-signup" method="post" action="<?php echo HTTP_ROOT?>Homes/studentSignup">
                                <div class="form-group">
                                    <label for="firstName" class="col-sm-3 control-label label_left">Full Name</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="text" id="firstName" placeholder="Full Name" class="form-control required" autofocus name="name">
                                        <span class="help-block"> First Name, Last Name, eg.: Sunil, Singh </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-3 control-label label_left">Email</label>
                                    <div class="col-sm-9">
                                        
                                        <input type="email" id="" placeholder="Email" class="form-control" name="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="mobile" class="col-sm-3 control-label label_left">Mobile Number*</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="text" id="mobile" placeholder="Mobile" class="form-control required" name="mobile" minlength="10" maxlength="10">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-3 control-label label_left">Password</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="password" id="student-pass" placeholder="Password" class="form-control required" name="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-3 control-label label_left">Confirm Password*</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="password" id="student-confirm-pass" placeholder="Confirm Password" class="form-control required" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="birthDate" class="col-sm-3 control-label label_left">Date of Birth</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="text" id="birthDate" placeholder="Date of Birth" class="form-control dob required" name="dob">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 label_left">Gender</label>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="radio-inline label_left">
                                                <input type="radio" name="gender" value="1" required clas="required">Female
                                                </label>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="radio-inline label_left">
                                                <input type="radio" name="gender" value="0" required class="required">Male
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label for="birthDate" class="col-sm-3 control-label label_left">Reference Code(if any)</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <input type="text" placeholder="Reference Code" class="form-control" name="reference_code">
                                    </div>
                                </div> -->
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <div class="checkbox">
                                            <label class="label_left">
                                            <input type="checkbox" class="required">I accept <a href="<?php echo HTTP_ROOT?>Homes/terms" target="_blank">terms</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary btn-block">Register</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



         


          <!--===================secend-popup========================-->
        <div class="modal fade" id="myModal-id20" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header modal-header_bg2">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="my_title">Suggest A College</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="padding: 2%;">
                            <div class="col-md-6">
                                <div class="main_col">
                                    <form>
                                        <div class="form-group">
                                            <label for="Name">Name:</label>
                                            <input type="text" class="form-control" id="Name" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="Time">City:</label>
                                            <select id="user-City" class="form-control">
                                                <option value="">1</option>
                                                <option value="" selected="selected">Noida</option>
                                                <option value="">2</option>
                                                <option value="">3</option>
                                                <option value="">4</option>
                                                <option value="">5</option>
                                                <option value="">6</option>
                                                <option value="">7</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="Contact">Contact No:</label>
                                            <input type="text" class="form-control" id="Contact" placeholder="Contact No">
                                        </div>
                                        <div class="form-group">
                                            <label for="Time">Course:</label>
                                            <select id="college-Course" class="form-control">
                                                <option value="">Animation</option>
                                                <option value="" selected="selected">MBA</option>
                                                <option value="">B Com</option>
                                                <option value="">B Sc</option>
                                                <option value="">BBA</option>
                                                <option value="">CA</option>
                                                <option value="">Fashion & Textile Design</option>
                                                <option value="">Hotel Management</option>
                                                <option value="">Law</option>
                                                <option value="">MassCommunicationMBBS</option>
                                                <option value="">MCA</option>
                                                <option value="">M Tech</option>
                                                <option value="">BCA</option>
                                                <option value="">Pharmacy</option>
                                                <option value="">M Sc</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="Message">Message:</label>
                                            <textarea name="message" rows="3" cols="30" placeholder="Message"></textarea>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="main_col2">
                                    <form>
                                        <div class="form-group">
                                            <label for="email">Email Id:</label>
                                            <input type="text" class="form-control" id="Email" placeholder="Email Id">
                                        </div>
                                        <div class="form-group">
                                            <label for="Category">Locality:</label>
                                            <select id="user_time_zone" class="form-control">
                                                <option value="1">1</option>
                                                <option value="">2</option>
                                                <option value="">3</option>
                                                <option value="">4</option>
                                                <option value="">5</option>
                                                <option value="" selected="selected">select Your City First Category</option>
                                                <option value="">7</option>
                                                <option value="">8</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="Pincode">Pincode:</label>
                                            <input type="text" class="form-control" id="Pincode" placeholder="Pincode">
                                        </div>
                                        <div class="form-group">
                                            <label for="location">Time to Contact:</label>
                                            <input type="text" class="form-control" id="Time2Contact" placeholder="Time to Contact">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding: 2%;">
                            <div class="main_col2">
                                <div class="top_button_2">
                                    <button type="submit" class="btn btn-default but1-Submit_but">Submit</button>
                                    <button type="submit" class="btn btn-default but2-Reset_but">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal-id21" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header modal-header_bg2">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="my_title">List with Us</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="main_col">
                                    <form>
                                        <div class="form-group">
                                            <label for="business-name">Business name:</label>
                                            <input type="text" class="form-control" id="business-name" placeholder="Business Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="Name">Name:</label>
                                            <input type="text" class="form-control" id="Name" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="Contact">Contact No:</label>
                                            <input type="text" class="form-control" id="Contact" placeholder="Contact No">
                                        </div>
                                        <div class="form-group">
                                            <label for="Time">Time Contact:</label>
                                            <select id="user_time_zone" class="form-control">
                                                <option value="Central Time (US &amp; Canada)" selected="selected">select Timing</option>
                                                <option value="Hawaii">(GMT-10:00) Hawaii</option>
                                                <option value="Alaska">(GMT-09:00) Alaska</option>
                                                <option value="Pacific Time (US &amp; Canada)">(GMT-08:00) Pacific Time (US &amp; Canada)</option>
                                                <option value="Arizona">(GMT-07:00) Arizona</option>
                                                <option value="Mountain Time (US &amp; Canada)">(GMT-07:00) Mountain Time (US &amp; Canada)</option>
                                                <option value="Eastern Time (US &amp; Canada)">(GMT-05:00) Eastern Time (US &amp; Canada)</option>
                                                <option value="Indiana (East)">(GMT-05:00) Indiana (East)</option>
                                            </select>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="main_col2">
                                    <form>
                                        <div class="form-group">
                                            <label for="location">Business Location:</label>
                                            <input type="text" class="form-control" id="business-location" placeholder="Business Location">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email Id:</label>
                                            <input type="text" class="form-control" id="Email" placeholder="Email Id">
                                        </div>
                                        <div class="form-group">
                                            <label for="Category">Category:</label>
                                            <select id="user_time_zone" class="form-control">
                                                <option value="" selected="selected">Select Category</option>
                                                <option value="1">1</option>
                                                <option value="">2</option>
                                                <option value="">3</option>
                                                <option value="">4</option>
                                                <option value="">5</option>
                                                <option value="">7</option>
                                                <option value="">8</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="Message">Message:</label>
                                            <textarea rows="3" cols="30" placeholder="Message"></textarea>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="main_col2">
                                <div class="login_type">
                                    <button type="submit" class="btn btn-default but1-Submit_but">Submit</button>
                                    <button type="submit" class="btn btn-default but2-Reset_but">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--===================End of secend-popup========================-->

        <!---------------college image gallery ---------------------------->

        <div class="modal fade" id="gallery-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4 class="modal-title modal-title_a2 glry-clg-name" id="myModalLabel"></h4>
                  </div>
                  <div class="modal-body gallery-images">
                     <div class="owl-carousel owl-theme glry-popup">
                        
                        <div class="item">
                           <img src="./Home_files/eee.jpg" class="img-responsive name_colleg_1" alt="Product">
                           <p class="Fro_nt">laboratory of college</p>
                        </div>
                        <div class="item">
                           <img src="./Home_files/eee.jpg" class="img-responsive name_colleg_1" alt="Product">
                           <p class="Fro_nt">play ground of college</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <style>
             .Fro_nt{
                margin-bottom:0px;
             }
         </style>
        <!----------------------college image gallery end--------------------------------------------->



        <!---------------------------message modal ------------------------------------>

        <!--admin success-msg Modal -->
            <div id="frontend-msg" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header" style="color:#fff !important;background:#36454F !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:#fff !important">Success Message</h4>
                  </div>
                  <div class="modal-body">
                    <div class="login-box-body" style="margin-left: 4%;">
               
                  
                  <div class="row">
                    <div class="col-xs-12">
                      
                      <h4 id="scs-msg"><?php echo $this->Session->read('frontsuccess-msg');?></h4>
                      
                    </div>
                    
                  </div>

                
              </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
            <!-- modal ends---------------->

        <!---------------------------message modal ends---------------------------------->


        <!-------------change image------------------------------>

            <div id="student-image" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:#000 !important">Change Image</h4>
                  </div>
                  <div class="modal-body">
                    <div class="login-box-body">
               
                  
                  <div class="row">
                    <div class="col-xs-12">
                      
                      <form method="post" action="<?php echo HTTP_ROOT?>Homes/updateStudentImage" enctype="multipart/form-data" id="update-student-image">
                          <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">User Image<span class="require" aria-required="true">*</span></label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                  <input type="file" class="form-control required" name="image">
                                </div>
                              </div>
                            </div>
                            <div class="box-footer col-md-12">
                            <button type="submit" class="btn btn-primary">Update Image</button>
                          </div>
                      </form>
                      
                    </div>
                    
                  </div>

                
              </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
        <!-------------------change image ends---------------------------->


        <!-------------------change student password ---------------------------->

             <div id="student-password-update" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header" style="color:#000 !important;background:#36454F !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:#fff !important">Change Password</h4>
                  </div>
                  <div class="modal-body">
                    <div class="login-box-body">
               
                  
                  <div class="row">
                    <div class="col-xs-12">
                      
                      <form method="post" action="<?php echo HTTP_ROOT?>Homes/updateStudentPassword" enctype="multipart/form-data" id="update-student-password">
                          <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">Current Password<span class="require" aria-required="true">*</span></label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                  <input type="password" class="form-control required" name="current">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">New Password<span class="require" aria-required="true">*</span></label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                  <input type="password" id="add-new-pass1" class="form-control required" name="password">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">Confirm Password<span class="require" aria-required="true">*</span></label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                  <input type="password" id="add-confirm-pass1" class="form-control required" >
                                </div>
                              </div>
                            </div>
                            <div class="box-footer col-md-12">
                            <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                      </form>
                      
                    </div>
                    
                  </div>

                
              </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>


        <!-------------------change student password ends---------------------------->




                <!-------------change college image------------------------------>

            <div id="college-image" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:#000 !important">Change Image</h4>
                  </div>
                  <div class="modal-body">
                    <div class="login-box-body">
               
                  
                  <div class="row">
                    <div class="col-xs-12">
                      
                      <form method="post" action="<?php echo HTTP_ROOT?>Homes/updateCollegeImage" enctype="multipart/form-data" id="update-college-image">
                          <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">College Image<span class="require" aria-required="true">*</span></label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                  <input type="file" class="form-control required" name="image">
                                </div>
                              </div>
                            </div>
                            <div class="box-footer col-md-12">
                            <button type="submit" class="btn btn-primary">Update Image</button>
                          </div>
                      </form>
                      
                    </div>
                    
                  </div>

                
              </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
        <!-------------------change college image ends---------------------------->


        <!-------------------change college password ---------------------------->

             <div id="college-password-update" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header" style="color:#000 !important;background:#36454F !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:#000 !important">Change Password</h4>
                  </div>
                  <div class="modal-body" >
                    <div class="login-box-body">
               
                  
                  <div class="row">
                    <div class="col-xs-12">
                      
                      <form method="post" action="<?php echo HTTP_ROOT?>Homes/updateCollegePassword" enctype="multipart/form-data" id="update-college-password">
                          <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">Current Password<span class="require" aria-required="true">*</span></label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                  <input type="password" class="form-control required" name="current">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">New Password<span class="require" aria-required="true">*</span></label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                  <input type="password" id="add-new-pass2" class="form-control required" name="password">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">Confirm Password<span class="require" aria-required="true">*</span></label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                  <input type="password" id="add-confirm-pass2" class="form-control required" >
                                </div>
                              </div>
                            </div>
                            <div class="box-footer col-md-12">
                            <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                      </form>
                      
                    </div>
                    
                  </div>

                
              </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>


        <!-------------------change college password ends---------------------------->


        <!-- Notify Modal-->
        <div class="modal fade" id="notifyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="notify-form" method="post" action="javascript:void(0)">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" style="color:#000 !important;">Notify</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-header">
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Name</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" class="form-control required" placeholder="Name" name="data[Notify][name]" value="<?php echo @$this->Session->read('Student.name')?>">
          </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Email</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" class="form-control required" placeholder="Email" name="data[Notify][email]" value="<?php echo @$this->Session->read('Student.email')?>">
          </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Mobile</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" class="form-control required" placeholder="Mobile" name="data[Notify][mobile]" value="<?php echo @$this->Session->read('Student.mobile')?>">
          </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Course</label>
          <div class="col-sm-9">
            <i class="fa input-error"></i>
            <select name="data[Notify][course_id]" class="form-control required">
              <option>Select Course</option>
              <?php //foreach($courses as $course){?>
              <option value="<?php //echo $course['Course']['id']?>" <?php //echo $course['Course']['id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php //echo $course['Course']['course']?></option>
              <?php //}?>
            </select>
          </div>
        </div>
        <div class="form-group col-md-12">
                                    <label for="country" class="col-sm-3 control-label label_left">Country</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <select id="notifycountry" class="form-control required custom-select" name="data[Notify][country_id]">
                                            <option value="">Select Country</option>
                                            <?php foreach($country as $coun){?>
                                            <option value="<?php echo $coun['Country']['id']?>"><?php echo $coun['Country']['country_name']?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="state" class="col-sm-3 control-label label_left">State *</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <select id="notifystate" name="data[Notify][state_id]" class="form-control required custom-select">
                                            <option value="">Select State</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="state" class="col-sm-3 control-label label_left">City *</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <select id="notifycity" name="data[Notify][city_id]" class="form-control required custom-select">
                                            <option value="">Select City</option>
                                            
                                        </select>
                                    </div>
                                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>
    </div>
  </div>
</div>






<!-- end Notify modal-->

<!-- Membership plan Modal-->
<div class="modal fade" id="membershipModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="plan-form" method="post" action="<?php echo HTTP_ROOT.'Homes/studentPlan/'.@$this->Session->read('membership_modal');?>">
      <div class="modal-header" style="color:#000 !important;background:#36454F !important;">
        <h4 class="modal-title" id="exampleModalLabel" style="color:#fff !important;">Membership Plans</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-header">
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Plan Name</label>
            <div class="col-sm-9">
              <?php foreach($plans as $plan){?>
              <input type="radio" class="plan-name required" name="data[StudentPlan][plan_id]" value="<?php echo $plan['MembershipPlan']['id']?>"> <?php echo $plan['MembershipPlan']['plan_name'].' ';?> 
              <?php }?>
            </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Plan Cost</label>
            <div class="col-sm-9">
              <input readonly type="text" name="data[StudentPlan][plan_cost]" id="plan-cost" class="form-control" value="" placeholder="Cost">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <?php if($this->Session->check('membership_modal')){?>
        <a href="<?php echo HTTP_ROOT.'Homes/studentPlan/'.@$this->Session->read('membership_modal').'?show=never';?>" class="btn btn-danger">Never Show</a>
        <a href="<?php echo HTTP_ROOT.'Homes/studentPlan/'.@$this->Session->read('membership_modal').'?show=later/'.@$this->Session->read('membership_modal');?>"  class="btn btn-warning">Remember Later</a>
        <?php }?>
        <button type="submit" class="btn btn-primary">Next</button>
      </div>
    </form>
    </div>
  </div>
</div>
<!--End Membership plan Modal-->

<!-- Wallet transfer Modal-->
<div class="modal fade" id="walletModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="wallet-form" method="post" action="javascript:void(0)">
      <div class="modal-header" style="color:#fff !important;background:#36454F !important;">
        <h4 class="modal-title" id="exampleModalLabel" style="color:#fff !important;">Wallet Transfer</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-header">
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Email</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="email" class="form-control required" placeholder="Email" name="email" value="">
          </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Amount</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" class="form-control required number" maxlength="<?php echo @$walletdet['Wallet']['amount'];?>" placeholder="Amount" name="amount">
          </div>
          <p class="wallet-error"></p>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>
    </div>
  </div>
</div>
   
<!-- Forgot password Modal-->
<div class="modal fade" id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style=" display: block;width: 87%!important;float: right!important; margin-left: 6%!important; top: 5%!important;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="forgot-form" method="post" action="javascript:void(0)">
      <div class="modal-header" style="color:#000 !important;background:#36454F !important;">
        <h4 class="modal-title" id="exampleModalLabel" >Forgot Password</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-header">
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Email</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="email" class="form-control required" placeholder="Email" name="email" value="">
          </div><br><br><br>
          <p id="forgot-error" style="text-align: center;"></p>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Send</button>
      </div>
    </form>
    </div>
  </div>
</div>


<!--Signup-msg Modal -->
            <div id="signup-msg" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header" style="color:#000 !important;background:#36454F !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Thanks For Registration</h4>
                  </div>
                  <div class="modal-body">
                    <div class="login-box-body">
               
                  
                  <div class="row">
                    <div class="col-xs-12">
                      
                      <h4 style="color:green;">Please check your email id to activate your account. </h4>
                      <h5 style="color:red;">Note : Do check your spam folder if you don't receive in your inbox.</h5>
                    </div>
                    
                  </div>

                
              </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
            <!-- modal ends---------------->

<!--Signup-msg Modal -->
            <div id="activate-msg" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header" style="color:#000 !important;background:#36454F !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Thanks For Registration</h4>
                  </div>
                  <div class="modal-body">
                    <div class="login-box-body">
               
                  
                  <div class="row">
                    <div class="col-xs-12">
                      
                      <h4 style="color:green;">Your account has been activated succucessfully. Please login with your credential. </h4>
                    </div>
                    
                  </div>

                
              </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
            <!-- modal ends---------------->

<!-- Forgot password Modal-->
<div class="modal fade" id="todolist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="todolist-form" method="post" action="<?php echo HTTP_ROOT?>Homes/addTodolist">
      <div class="modal-header" style="color:#000 !important;background:#36454F !important;">
        <h4 class="modal-title" id="exampleModalLabel" >To Do List</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-header">
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Title</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" class="form-control required" placeholder="Title" name="data[ToDoList][title]" value="">
          </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Description</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" class="form-control required" placeholder="Description" name="data[ToDoList][description]" value="">
          </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Date</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" class="form-control todo-datepicker required" placeholder="Date" name="data[ToDoList][work_date]" value="">
          </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Time</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <!-- <input type="email" class="form-control required" placeholder="Time" name="data[ToDoList][work_time]" value=""> -->
          <select class="form-control col-md-6 required" name="data[ToDoList][work_time_hour]">
            <option selected disabled>Select Hour</option>
            <?php for($i=1;$i<=24;$i++){?>
            <option value="<?php echo $i;?>"><?php echo $i;?></option>
            <?php }?>
          </select>
          <select class="form-control col-md-6 required" name="data[ToDoList][work_time_minute]">
            <option selected disabled>Select Minute</option>
            <?php for($i=1;$i<=60;$i++){?>
            <option value="<?php echo $i;?>"><?php echo $i;?></option>
            <?php }?>
          </select>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>
    </div>
  </div>
</div>


        <!--admin success-msg Modal -->
            <div id="disclaimer-msg" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header" style="color:#fff !important;background:#36454F !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:#fff !important">Disclaimer</h4>
                  </div>
                  <div class="modal-body">
                    <div class="login-box-body">
               
                  
                  <div class="row">
                    <div class="col-xs-12">
                      
                      <p>
                        This is a Beta version of getcollege.in, which is on trial run before its official release. getcollege.in its software and all content found on it are provided on “as is” and “as available” basis. The primary purpose of the Beta Version launch is to obtain feedback on the performance, identification of technical problems (if any) and their resolution to deliver enhanced interfaces and experience to the users. getcollege.in,will not be liable for any loss, whether such loss is direct, indirect, special or consequential, suffered by any party as a result of their use of the getcollege.in portal, its software or content. Though every effort is made to keep this website running smoothly,  getcollege.in  take no responsibility, and will not be liable for the website being temporarily unavailable due to technical issues beyond their control. In case you encounter any technical problems on this website, please contact us on email: info@getcollege.in. Your feedback/support in this regard will be greatly appreciated.
                      </p>
                      
                    </div>
                    
                  </div>

                
              </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
            <!-- modal ends---------------->


        <!-- student refer Modal-->
<div class="modal fade" id="referModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="refer-form" method="post" action="<?php echo HTTP_ROOT.'homes/referSchool';?>">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" style="color:#000 !important;">Refer Your School</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-header">
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Your Name</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" class="form-control required" placeholder="Name" name="data[ReferSchool][student_name]" value="<?php echo @$this->Session->read('Student.name')?>">
          </div>
        </div>

        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Principle Name</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" class="form-control required" placeholder="Name" name="data[ReferSchool][principle_name]" value="">
          </div>
        </div>

        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Principle Email</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" class="form-control required" placeholder="Email" name="data[ReferSchool][principle_email]" value="">
          </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Principle Mobile</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" class="form-control required" placeholder="Mobile" name="data[ReferSchool][principle_mobile]" value="">
          </div>
        </div>

        <div class="form-group col-md-12">
                                    <label for="country" class="col-sm-3 control-label label_left">Country</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <select id="refercountry" class="form-control required" name="data[ReferSchool][country_id]">
                                            <option value="">Select Country</option>
                                            <?php foreach($country as $coun){?>
                                            <option value="<?php echo $coun['Country']['id']?>"><?php echo $coun['Country']['country_name']?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="state" class="col-sm-3 control-label label_left">State *</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <select id="referstate" name="data[ReferSchool][state_id]" class="form-control required">
                                            <option value="">Select State</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="state" class="col-sm-3 control-label label_left">City *</label>
                                    <div class="col-sm-9">
                                        <i class="fa input-error"></i>
                                        <select id="refercity" name="data[ReferSchool][city_id]" class="form-control required">
                                            <option value="">Select City</option>
                                            
                                        </select>
                                    </div>
                                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!--msg Modal -->
            <div id="test-msg" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header" style="color:#000 !important;background:#36454F !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Message</h4>
                  </div>
                  <div class="modal-body">
                    <div class="login-box-body">
               
                  
                  <div class="row">
                    <div class="col-xs-12">
                      
                      <h4 style="color:green;">Qualify your previous level.</h4>
                    </div>
                    
                  </div>

                
              </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
            <!-- modal ends---------------->


<script>
$(document).ready(function() {
  $('.refer-select').select2();
  $('#refer-form').validate();
});
</script>



<!-- end student refer modal-->

  <!-- <img style='margin-left: 50%;margin-top: 6%;display:none;' class='loader' src='https://www.pavoterservices.pa.gov/images/loader.gif'>  -->   

