<style>
.lang
{
	margin-left: -8%!important;
}
.user-image {
    float: left;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    margin-right: 10px;
    margin-top: -2px;
}
.dropdown-menu {
    position: absolute;
    right: 0;
    left: auto;
}
.dropdown-menu {
    top: 100%;
    z-index: 1000;
    float: left;
    min-width: 350px;
    padding: 5px 0;
    margin: 2px 0 0;
    font-size: 14px;
    text-align: left;
    list-style: none;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ccc;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: 4px;
    box-shadow: 0 6px 12px rgba(0,0,0,.175);
}
.dropdown-menu>li.user-header {
    height: 140px;
    padding: 10px;
    text-align: center;
}
.img-circle {
    border-radius: 50%;
}
.user-header>img {
    z-index: 5;
    height: 90px;
    width: 90px;
    border: 3px solid;
    border-color: transparent;
    border-color: rgba(255,255,255,0.2);
}
.user-body {
    padding: 15px;
    border-bottom: 1px solid #f4f4f4;
    border-top: 1px solid #dddddd;
}
.user-footer {
    background-color: #f9f9f9;
    padding: 10px;
}



/*=======================*/
.dropdown_first {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropdown_first {
    position: relative;
    display: inline-block;
}

.dropdown-content_sub {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 1016px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content_sub a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content_sub a:hover {background-color: #f1f1f1}

.dropdown_first:hover .dropdown-content_sub {
    display: block;
}

.dropdown_first:hover .dropbtn_1 {
    background-color: #3e8e41;
}

ul.moto-widget-menu-sublist.sub_menu_list {
    
    background: #fff !important;
    padding: 35px !important;
}
ul.list_style-sub {
    list-style: none;
}
ul.list_style-sub li {
    padding: 4px 0;
}
li.BROWSE_BY {
    padding: 0 0 10px !important;
}
ul.list_style-sub li a {
    color: #000;
}
h3.heading_list {
    font-size: 17px;
    color: #ef8d36;
    margin: 10px 0 10px;
}
</style>
<header id="section-header" class="header moto-section" data-widget="section" data-container="section">
                <div class="page-header-top" style="background:#008190;">
                    <div class="row">
                        <div class="grid-row clear-fix col-md-12" style="width:100%;font-size: 13px;">
                            <div class="col-md-6 col-sm-12">
                                <address style="display:none">
                                    <div id="google_translate_element"></div>
                                    </address>
                                <address class="pull-left" style="
                                    margin-top: 4px;">
                                    <ul class="location-ul">
                                    <li class="dropdown">
                                        <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle Gurgaon" href="">
                                        <img src="<?php echo HTTP_ROOT?>/frontend/img/english.png" class="123abc" style="width: 20px;"> English</a>
                                        <ul class="dropdown-menu dropdown-custom dropdown-custom_1" >
                                            <!-- <form class="form-inline">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control loc-input" id="exampleInputAmount" placeholder="Select Language">
                                                        <div class="input-group-addon addon-btn"><i class="fa fa-search"></i></div>
                                                    </div>
                                                </div>
                                            </form> -->
                                            <li>
                                                <ul class="cit-opt">
                                                    <h4>Select Language</h4>
                                                    <div class="row lang">
													<div class="col-md-1"></div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																	<a href="javascript:void(0)" class="flag_4 lang-cnvt" id="Hindi">Hindi</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																   <a href="javascript:void(0)" class="flag_4 lang-cnvt" id="Gujarati">Gujarati</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/img/english.png" class="123abc" style="width: 24px;">
																</div>
																<div class="col-md-8">
																	<a href="javascript:void(0);" class="lang-cnvt" id="English">English</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																   <a href="javascript:void(0)" class="flag_4 lang-cnvt" id="Marathi">Marathi</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 24px;">
                                                            </div>
                                                            <div class="col-md-8">
                                                                <a href="javascript:void(0);" class="lang-cnvt" id="Nepali">Nepali</a>
                                                            </div>
                                                        </div>
                                                    </li>
													</div>
													<div class="col-md-1"></div>
													</div>
													<div class="row lang">
													<div class="col-md-1"></div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																	<a href="javascript:void(0)" class="flag_4 lang-cnvt" id="Bengali">Bengali</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																   <a href="javascript:void(0)" class="flag_4 lang-cnvt" id="Kannada">Kannada</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 24px;">
																</div>
																<div class="col-md-8">
																	<a href="javascript:void(0);" class="lang-cnvt" id="Tamil">Tamil</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
													
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																   <a href="javascript:void(0)" class="flag_4 lang-cnvt" id="Telugu">Telugu</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 24px;">
																</div>
																<div class="col-md-8">
																	<a href="javascript:void(0);" class="lang-cnvt" id="Malayalam">Malayalam</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-1"></div>
													</div>
													<div class="row lang">
													<div class="col-md-1"></div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																	<a href="javascript:void(0)" class="flag_4 lang-cnvt" id="Punjabi">Punjabi</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																   <a href="javascript:void(0)" class="flag_4 lang-cnvt" id="Urdu">Urdu</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/1234567.png" class="123abc" style="width: 24px;">
																</div>
																<div class="col-md-8">
																	<a href="javascript:void(0);" class="lang-cnvt" id="sindhi">sindhi</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/img/france.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																   <a href="javascript:void(0)" class="flag_4 lang-cnvt" id="French">French</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/img/spain.png" class="123abc" style="width: 24px;">
																</div>
																<div class="col-md-8">
																	<a href="javascript:void(0);" class="lang-cnvt" id="Spanish">Spanish</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-1"></div>
													</div>
													<div class="row lang">
													<div class="col-md-1"></div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/img/myanmar.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																	<a href="javascript:void(0)" class="flag_4 lang-cnvt" id="Myanmar">Myanmar</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/img/russia.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																   <a href="javascript:void(0)" class="flag_4 lang-cnvt" id="Russian">Russian</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/img/germany.png" class="123abc" style="width: 24px;">
																</div>
																<div class="col-md-8">
																	<a href="javascript:void(0);" class="lang-cnvt" id="German">German</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/img/japan.png" class="123abc" style="width: 20px;">
																</div>
																<div class="col-md-8">
																   <a href="javascript:void(0)" class="flag_4 lang-cnvt" id="Japanese">Japanese</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-2">
														<li>
															<div class="row">
																<div class="col-md-4">
																	<img src="<?php echo HTTP_ROOT?>/frontend/img/portugal.png" class="123abc" style="width: 24px;">
																</div>
																<div class="col-md-8">
																	<a href="javascript:void(0);" class="lang-cnvt" id="Portuguese">Portuguese</a>
																</div>
															</div>
														</li>
													</div>
													<div class="col-md-1"></div>
													</div>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    </ul>
                                </address>
                                <address style="margin-top:1%;">
                                    <span class="phone-number"><i class="fa fa-whatsapp" style="background: rgb(101, 188, 84) none repeat scroll 0% 0%; border-radius: 50%; font-size: 15px;"></i>&nbsp; &nbsp;<i class="fa fa-phone"></i> For any Enquiry +91 7631900600</span>&nbsp; &nbsp;
                                    <span class="email"> <i class="fa fa-envelope-o"></i> info@formsadda.com</span>
                                </address>
                            </div>
                            <div class="col-md-6 col-sm-12 pull-right" style="padding-top:5px;">
                                <address class='pull-right'> 
                                  <ul>
                                    <li class="dropdown user user-menu">
                                          <?php if($this->Session->check('Student')){?>
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <?php if(!empty($this->Session->read('Student.image'))){?>
                                              <img src="<?php echo HTTP_ROOT.'img/studentImages/small/'.$this->Session->read('Student.image');?>" class="user-image" alt="User Image">
                                              <?php } else{?>
                                              <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="user-image" alt="User Image">
                                              <?php }?>
                                              <span class="hidden-xs"><?php echo $this->Session->read('Student.name');?></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                              <!-- User image -->
                                              <li class="user-header">
                                                <div class="col-md-6">
                                                <?php if(!empty($this->Session->read('Student.image'))){?>
                                                  <img src="<?php echo HTTP_ROOT.'img/studentImages/small/'.$this->Session->read('Student.image');?>" class="img-circle" alt="User Image" width="110px">
                                                  <?php } else{?>
                                                  <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="img-circle" alt="User Image" width="110px">
                                                  <?php }?>
                                                </div>
                                                <div class="col-md-6">
                                                  <h4><?php echo $this->Session->read('Student.name');?></h4>
                                                  <small style="color:black;"><?php echo $this->Session->read('Student.email');?></small>
                                                  
                                                  <a href="<?php echo HTTP_ROOT?>Homes/studentdashboard" class="btn btn-primary" style="background:#007d94;color:#fff">My Profile</a>
                                                </div>
                                                
                                                  
                                        
                                              </li>
                                              <!-- Menu Body -->

                                              <!-- Menu Footer-->
                                              <li class="user-footer">
                                                <div class="pull-left">
                                                  <a class="btn btn-primary" data-toggle="modal" href="javascript:void(0)" data-target="#student-password-update" style="background:#007d94;color:#fff">Change Password</a>
                                                  
                                                </div>
                                                <div class="pull-right">
                                                  <a href="<?php echo HTTP_ROOT?>Homes/logout" class="btn btn-primary" style="background:#007d94;color:#fff">Sign out</a>
                                                </div>
                                              </li>
                                            </ul>
                                          <?php }?>
                                          <?php if($this->Session->check('College')){?>
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <?php if(!empty($this->Session->read('College.image'))){?>
                                              <img src="<?php echo HTTP_ROOT.'img/collegeImages/small/'.$this->Session->read('College.image');?>" class="user-image" alt="User Image">
                                              <?php } else{?>
                                              <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="user-image" alt="User Image">
                                              <?php }?>
                                              <span class="hidden-xs"><?php echo $this->Session->read('College.name');?></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                              <!-- User image -->
                                              <li class="user-header">
                                                <div class="col-md-6">
                                                <?php if(!empty($this->Session->read('College.image'))){?>
                                                  <img src="<?php echo HTTP_ROOT.'img/collegeImages/small/'.$this->Session->read('College.image');?>" class="img-circle" alt="User Image">
                                                  <?php } else{?>
                                                  <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="img-circle" alt="User Image">
                                                  <?php }?>
                                                </div>
                                                <div class="col-md-6">
                                                  <h4><?php echo $this->Session->read('College.name');?></h4>
                                                  <small style="color:black;"><?php echo $this->Session->read('College.email');?></small>
                                                  <a href="<?php echo HTTP_ROOT?>Homes/collegeDashboard" class="btn btn-primary" style="background:#007d94;color:#fff">My Profile</a>
                                                </div>
                                              </li>
                                              <!-- Menu Body -->

                                              <!-- Menu Footer-->
                                              <li class="user-footer">
                                                <div class="pull-left">
                                                  <a class="btn btn-primary" data-toggle="modal" href="javascript:void(0)" data-target="#student-password-update" style="background:#007d94;color:#fff">Change Password</a>
                                                </div>
                                                <div class="pull-right">
                                                  <a href="<?php echo HTTP_ROOT?>Homes/logout" class="btn btn-primary" style="background:#007d94;color:#fff">Sign out</a>
                                                </div>
                                              </li>
                                            </ul>
                                          <?php }?>
                                            
                                          </li>
                                        </ul>
                                </address>
                                <!-- <address class="pull-right">
                                    <ul class="location-ul" style="margin-top: 0px;margin-bottom: 0px;">
                                        <li class="dropdown">
                                            <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle Gurgaon loc-name" href="javascript:void(0);"><i class="fa fa-map-marker"></i> <?php echo @$_GET['location'] != '' ? explode('-', $_GET['location'])['0'] : 'Location';?></a>
                                            <ul class="dropdown-menu dropdown-custom" >
                                                <form class="form-inline">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control loc-input" id="location-inpt" placeholder="Enter your city">
                                                            <div class="input-group-addon addon-btn location-srch"><i class="fa fa-search"></i></div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <li>
                                                    <ul class="cit-opt">
                                                        <h4>Top Cities</h4>
                                                        <li>
                                                            <a href="">Ahmedabad,</a>
                                                        </li>
                                                        <li>
                                                            <a href="">Bangalore,</a>
                                                        </li>
                                                        <li>
                                                            <a href="">Chandigarh,</a>
                                                        </li>
                                                        <li>
                                                            <a href="">Chennai,</a>
                                                        </li>
                                                        <li>
                                                            <a href="">Coimbatore,</a>
                                                        </li>
                                                        <li>
                                                            <a href="">Delhi,</a>
                                                        </li>
                                                        <li>
                                                            <a href="">Gurgaon,</a>
                                                        </li>
                                                        <li>
                                                            <a href="">Hyderabad,</a>
                                                        </li>
                                                        <li>
                                                            <a href="">Jaipur,</a>
                                                        </li>
                                                        <li>
                                                            <a href="">Kochi,</a>
                                                        </li>
                                                        <li>
                                                            <a href="">Kolkata,</a>
                                                        </li>
                                                        <li>
                                                            <a href="">Lucknow,</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                    </ul>
                                </address> -->
                                <address class='pull-right'> 
                                    <?php if(!$this->Session->check('College') && !$this->Session->check('Student')){?>
                                    <a data-toggle="modal" data-keyboard="true" href="#myModal" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link " data-target="#my_Login12">
                                    <i class="fa fa-user"></i> Login &nbsp; &nbsp;</a>
                                    <a data-toggle="modal" href="#myModall" data-target="#my_Login13" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link ">
                                    <i class="fa fa-user"></i> Registration &nbsp; &nbsp;</a>
                                    <?php }?>    
                                    <a data-toggle="modal" href="http://www.formsadda.com/view_applyforClg"><i class="fa fa-bank"></i>
                                    My Cart </a> &nbsp; <span style="background: rgb(255, 156, 0) none repeat scroll 0% 0%; border-radius: 50%; padding: 2px 6px; margin-right: 3px;"><?php echo $count;?></span>
                                </address>
                                <address class="pull-right">
                                    <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width=79&layout=button_count&action=like&show_faces=true&share=false&height=21&appId" width="90" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                                    <a href="https://twitter.com/TwitterDev" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-moto-sticky="{ }" data-css-name="moto-container_header_56e2c116" class="moto-widget moto-widget-container moto-container_header_56e2c116 moto-sticky__bootstrapped moto-sticky__attached moto-sticky__attached_top" data-widget="container" data-container="container" style="width: 1349px;">
                    <div class="moto-widget moto-widget-row row-fixed" data-widget="row" style="position: unset!important;">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="moto-cell col-sm-3" data-container="container" style="position: unset!important;">
                                    <div data-grid-type="xs" class="moto-widget moto-widget-row" data-widget="row">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="moto-cell col-md-9 col-xs-3 margin_logo" data-container="container" style="position: unset!important;">
                                                    <div data-widget-id="wid__image__58d1ff0657cad" class="moto-widget moto-widget-image moto-preset-default moto-align-right moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-preset="default" data-widget="image" style="position: unset!important;">
                                                        <a class="moto-widget-image-link moto-link" href="<?php echo HTTP_ROOT?> " data-action="page">
                                                        <img src="<?php echo HTTP_ROOT?>/frontend/mt-0362-home-logo.png" class="moto-widget-image-picture chng_logo" data-id="128" title="" alt="" draggable="false">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="moto-cell col-sm-9" data-container="container" style="position: unset!important;">
                                    <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="" style="position: unset!important;">
                                    </div>
                                    <div data-widget-id="wid__menu__58d1ff065be54" class="moto-widget moto-widget-menu moto-preset-default moto-align-right moto-align-center_mobile-h moto-align-center_mobile-v moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-preset="default" data-widget="menu" style="position: unset!important;">
                                        <a href=" #" class="moto-widget-menu-toggle-btn"><i class="moto-widget-menu-toggle-btn-icon fa fa-bars"></i></a>
                                        <!--<div class="dropdown_first">
                                          <button class="dropbtn_1">Engineering</button>
                                          <div class="dropdown-content_sub">
                                            <a href="#">Link 1</a>
                                            <a href="#">Link 2</a>
                                            <a href="#">Link 3</a>
                                          </div>
                                        </div>-->
                                        <ul class="moto-widget-menu-list moto-widget-menu-list_horizontal customdes">
                                        <li class="moto-widget-menu-item">
                                                <a href=" " data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-widget-menu-link-active moto-link"><i class="fa fa-home"></i> Home</a>
                                            </li>
											<li class="moto-widget-menu-item">
                                                <a href=" services/" data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link"><i class="fa fa-child"></i> Play School</a>
                                            </li>
											<li class="moto-widget-menu-item">
                                                <a href=" services/" data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link"><i class="fa fa-book"></i> School</a>
                                            </li>
                                        
                                            <li class="moto-widget-menu-item">
                                                <a href=" services/" data-action="page" class="moto-widget-menu-link moto-widget-menu-link-level-1 moto-link"><i class="fa fa-graduation-cap"></i> College</a>
                                            </li>
                                            
                                            <?php if($this->Session->check('Student')){?>
                                            <li class="moto-widget-menu-item color_black">
                                                <button data-toggle="modal" data-target="#membershipModal" data-action="page" class="btn btn-info moto-widget-menu-link blink moto-widget-menu-link-level-1 moto-link level_moto_2"><?php echo empty($check_plan['StudentPlan']) ? 'Become A Member' : 'Upgrade Membership'?></button>
                                            </li>
                                            <?php } else{?>
                                            <li class="moto-widget-menu-item color_black">
                                                <button data-toggle="modal" data-target="#my_Login13" data-action="page" class="btn btn-info moto-widget-menu-link blink moto-widget-menu-link-level-1 moto-link level_moto_2">Become A Member</button>
                                            </li>
                                            <?php }?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="moto-sticky-pseudo-element" style="display: block; width: 1349px;"></div>
            </header>
<script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
  }
</script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>

<script type="text/javascript">
    $('.lang-cnvt').on('click',function(){
     var lang = $(this).attr('id');
      var $frame = $('.goog-te-menu-frame:first');
      if (!$frame.size()) {
        alert("Error: Could not find Google translate frame.");
        return false;
      }
      $frame.contents().find('.goog-te-menu2-item span.text:contains('+lang+')').get(0).click();
      return false;
});
</script>