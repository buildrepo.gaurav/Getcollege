 <footer>
            <div class="footer-bottom row" style="">
                <div class="n-footer3">
                    <div class="container-fluid" style="">
                        <div class="row">
                            <div class="n-footer3">
                                <div class="container" style="">
                                    <ul class="fotr_seo" style=" list-style: none;">
                                        <li>
                                            <div class="Buildrepo_col">Play School<i>:</i></div>
                                            <div><a track="FOOTER_MBA_HOME" href="">Euro Kids</a>
                                                <i></i>
                                                <a href=""> Shanti Juniors</a><i></i>
                                                <a href="">Kidzee</a><i></i>
                                                <a href="">Little Elly</a><i></i>
												<a href="">Kangaroo Kids</a><i></i>
												<a href="">Little Einsteins</a><i></i>
												<a href="">Kids Campus</a><i></i>
												<a href="">Smart Kidz</a><i></i>
												<a href="">Podar Jumbo</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="Buildrepo_col">School<i>:</i></div>
                                            <div>
                                                <a href="">The Doon School</a><i></i>
                                                <a href="">Bishop Cotton School</a><i></i>
                                                <a href="">Mayo School</a><i></i>
												<a href="">City Montessori School</a><i></i>
												<a href="">Jaipuria School</a><i></i>
												<a href="">Delhi Public School</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="Buildrepo_col">College<i>:</i></div>
                                            <div>
                                                <a href="">IEC College of Engineering & Technology</a><i></i>
                                                <a href="">GL Bajaj Institute of Technology & Management</a><i></i>
												<a href="">Galgotias Group of Institutions</a>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    <p class="clr"></p>
                                    <div class="n-oPartnrFotr">
                                        <ul class="fotr_seo">  </ul>
                                    </div>
                                    <div class="col-md-12">
                                        <p></p>
                                        <p class="clr"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        </div>
                        <p class="clr"></p>
                        <div class="n-oPartnrFotr">
                            <ul class="fotr_seo"> </ul>
                        </div>
                        <div class="col-md-12">
                            <p></p>
                            <p class="clr"></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
             <section class="last-footer" style="border-top: 1px solid #006b7a">
            <div class="container">
                <div class="row" style="background-color: #1ca0af;">
                    <div class="col-md-12 text-center"><img src="<?php echo HTTP_ROOT?>/frontend/mt-0362-home-logo-footer1.png" class="ft-logo"></div>
                    <div class="col-md-12 text-center m-t-15">
                        <ul style="display:inline-flex;list-style: none; " class="ft-sm social-network social-circle">
                            <li style="">
                                <a href="#" class="icoFacebook"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoTwitter"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoInstagram"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoYoutube"><i class="fa fa-youtube"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoPinterest"><i class="fa fa-pinterest"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoGoogle"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoLinkedin"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12 text-center">
                        <ul style="display:inline-flex;list-style: none;" class="footer-links">
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/aboutus">About us</a>
                            </li>
                            <li>
                                <a href="#">Why Formsadda</a>
                            </li>
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/ourteam">Our Team</a>
                            </li>
                            <li>
                                <a  data-toggle="modal" data-target="#myModal-id20" href="#">Suggest a College</a>
                            </li>
                            <li>
                                <a  data-toggle="modal" data-target="#myModal-id21"  href="#">List with Us</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12 text-center ">
                        <ul style="display:inline-flex;list-style: none;" class="footer-links">
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Cms/contact">Contect Us</a>
                            </li>
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/terms">Careers</a>
                            </li>
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/privacy">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/terms">Terms & Conditions</a>
                            </li>
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/faqs">FAQs</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12 m-t-25 ft-bm">
                        <div class="row">
                            <div class="col-md-6 pull-left Buildrepo_col">
                                <p class="Buildrepo_col">Copyright 2017 © Formsadda.com. All Rights Reserved.</p>
                            </div>
                            <div class="col-md-6">
                                <p class="pull-right Buildrepo_col">Design and Developed by <a href="www.Buildrepo.com" class="Buildrepo_color">Buildrepo</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="moto-sticky-pseudo-element" style="display: none; height: 363px; width: 1349px;"></div>
        <div data-moto-back-to-top-button="" class="moto-back-to-top-button"><a ng-click="toTop($event)" class="moto-back-to-top-button-link"><span class="moto-back-to-top-button-icon"></span></a></div>