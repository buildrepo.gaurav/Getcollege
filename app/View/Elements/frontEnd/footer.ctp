 <footer>
            <div class="footer-bottom row" style="">
                <div class="n-footer3">
                    <div class="container-fluid" style="">
                        <div class="row">
                            <div class="n-footer3">
                                <div class="container" style="">
                                    <ul class="fotr_seo" style=" list-style: none;">
                                        <li>
                                            <div class="Buildrepo_col">MBA<i></i></div>
                                            <div><a track="FOOTER_MBA_HOME" href="#">MBA Home</a>
                                                <i></i>
                                                <a track="FOOTER_TOP_MBA_COLLEGES" href="#">
                                                Top MBA Colleges</a><i></i>
                                                <a track="FOOTER_MBA_COLLEGES" href="#">
                                                MBA Colleges</a><i></i>
                                                <a track="FOOTER_EXECUTIVE_MBA_COLLEGES" href="#">Executive MBA Colleges</a><i></i><a track="FOOTER_MBA_EXAMS" href="#">
                                                MBA Exams</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="Buildrepo_col">Engineering<i>:</i></div>
                                            <div>
                                                <a track="FOOTER_ENG_HOME" href="#">Engineering Home</a><i></i>
                                                <a track="FOOTER_TOP_ENG_COLLEGES" href="#">Top Engineering Colleges</a><i></i>
                                                <a track="FOOTER_ENG_COLLEGES" href="#">Engineering Colleges</a>
                                                <i></i><a track="FOOTER_ENG_EXAMS" href="#">Engineering Exams</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="Buildrepo_col">Other Courses<i>:</i></div>
                                            <div>
                                                <a track="FOOTER_ANIMATION" href="#">
                                                Animation</a><i></i>
                                                <a track="FOOTER_BCOM" href="#">B Com</a>
                                                <i></i><a track="FOOTER_BSC" href="#">B Sc</a><i></i>
                                                <a track="FOOTER_BBA" href="#">BBA</a><i></i>
                                                <a track="FOOTER_CA" href="#">CA</a>
                                                <i></i><a track="FOOTER_FASHION_TEXTILE" href="#">
                                                Fashion &amp; Textile Design</a><i></i>
                                                <a track="FOOTER_HOTEL_MGMT" href="#">
                                                Hotel Management</a><i></i><!-- <a track='FOOTER_INTERIOR_DESIGN' href="">Interior Design</a><br/><br/> -->
                                                <!-- <a track='FOOTER_JOURNALISM' href="">Journalism</a><i>
                                                    </i> --><a track="FOOTER_LAW" href="">Law</a><i>
                                                </i><a track="FOOTER_MASS_COMMUNICATION" href="#">Mass Communication</a>
                                                <a track="FOOTER_MBBS" href="#">MBBS</a><i>
                                                </i><a track="FOOTER_MCA" href="#">MCA</a><i>
                                                </i><a track="FOOTER_MTECH" href="#">M.Tech</a>
                                                <i></i><!-- <a track='FOOTER_BIOTECHNOLOGY' href="">Biotechnology</a><i></i> -->
                                                <a track="FOOTER_BCA" href="#">BCA</a><i></i>
                                                <!-- <a track='FOOTER_EVENT_MANAGEMENT' href="">Event Management</a><br/>
                                                    <br/> --><a track="FOOTER_PHARMACY" href="#">
                                                Pharmacy</a><i></i><!-- <a track='FOOTER_GRAPHIC_DESIGN' href="">Graphic Design</a><i></i> -->
                                                <a track="FOOTER_MSC" href="#">M Sc</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="Buildrepo_col">Study Abroad<i>:</i></div>
                                            <div><a track="FOOTER_SA_HOME" href="#">
                                                Study Abroad Home</a><i></i><a track="FOOTER_SA_BTECH_HOME" href="#">BTech abroad</a><i></i>
                                                <a track="FOOTER_SA_MBA_HOME" href="#">MBA abroad</a><i></i>
                                                <a track="FOOTER_SA_MS_HOME" href="#">MS abroad</a><i></i>
                                                <a track="FOOTER_SA_GRE_HOME" href="$">GRE</a><i></i>
                                                <a track="FOOTER_SA_GMAT_HOME" href="#">GMAT</a><i></i>
                                                <a track="FOOTER_SA_SAT_HOME" href="#">SAT</a><i></i>
                                                <a track="FOOTER_SA_IELTS_HOME" href="#">IELTS</a><i></i>
                                                <a track="FOOTER_SA_TOEFL_HOME" href="#">TOEFL</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="Buildrepo_col">Resources<i>:</i></div>
                                            <div>
                                                <a track="FOOTER_CAREERS_AFTER_12TH" href="http://www.getcollege.in/blog/2017/07/13/career-guidance-career-options-after-12th/" target="_blank" title="All Careers">
                                                Careers after 12th</a><i></i>
                                                <a track="FOOTER_ASK_QUESTION" href="#" title="Ask A Question">Ask a Question</a>
                                                <i></i><a track="FOOTER_DISCUSSIONS" href="#" title="Discussions">
                                                Discussions</a><i></i><a track="FOOTER_WRITE_COLLEGE_REVIEW" href="#">
                                                Write a college review</a><i></i>
                                                <a track="FOOTER_ARTICLES" href="#" title="Featured Articles">Articles</a>
                                                <i></i><a track="FOOTER_formsadda_ASK_ANSWER_APP" href="#" title="getCollege Ask &amp; Answer App" target="_blank">
                                                GetCollege Ask &amp; Answer App</a>
                                            </div>
                                        </li>
                                    </ul>
                                    <p class="clr"></p>
                                    <div class="n-oPartnrFotr">
                                        <ul class="fotr_seo">  </ul>
                                    </div>
                                    <div class="col-md-12">
                                        <p></p>
                                        <p class="clr"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        </div>
                        <p class="clr"></p>
                        <div class="n-oPartnrFotr">
                            <ul class="fotr_seo"> </ul>
                        </div>
                        <div class="col-md-12">
                            <p></p>
                            <p class="clr"></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
             <section class="last-footer" style="border-top: 1px solid #006b7a">
            <div class="container">
                <div class="row" style="background-color: #36454F;">
                    <div class="col-md-12 text-center"><img src="<?php echo HTTP_ROOT?>/frontend/mt-0362-home-logo.png" class="ft-logo"></div>
                    <div class="col-md-12 text-center m-t-15">
                        <ul style="display:inline-flex;list-style: none; " class="ft-sm social-network social-circle">
                            <li style="">
                                <a href="#" class="icoFacebook"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoTwitter"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoInstagram"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoYoutube"><i class="fa fa-youtube"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoPinterest"><i class="fa fa-pinterest"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoGoogle"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li style="">
                                <a href="#" class="icoLinkedin"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12 text-center">
                        <ul style="display:inline-flex;list-style: none;" class="footer-links">
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/aboutus">About us</a>
                            </li>
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/whygetcollege">Why GetCollege</a>
                            </li>
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/ourteam">Our Team</a>
                            </li>
                            <li>
                                <a  data-toggle="modal" data-target="#myModal-id20" href="#">Suggest a College</a>
                            </li>
                            <li>
                                <a  data-toggle="modal" data-target="#referModal"  href="#">Suggest a School</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12 text-center ">
                        <ul style="display:inline-flex;list-style: none;" class="footer-links">
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/contact">Contact Us</a>
                            </li>
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/careerget">Careers</a>
                            </li>
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/privacy">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/terms">Terms & Conditions</a>
                            </li>
                            <li>
                                <a href="<?php echo HTTP_ROOT?>Homes/faqs">FAQs</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12 m-t-25 ft-bm">
                        <div class="row">
                            <div class="col-md-6 pull-left Buildrepo_col">
                                <p class="Buildrepo_col">Copyright 2017 © getcollege.in All Rights Reserved.</p>
                            </div>
                            <div class="col-md-6">
                                <p class="pull-right Buildrepo_col">Design and Developed by <a href="www.Buildrepo.com" class="Buildrepo_color">Buildrepo</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="moto-sticky-pseudo-element" style="display: none; height: 363px; width: 1349px;"></div>
        <div data-moto-back-to-top-button="" class="moto-back-to-top-button"><a ng-click="toTop($event)" class="moto-back-to-top-button-link"><span class="moto-back-to-top-button-icon"></span></a></div>