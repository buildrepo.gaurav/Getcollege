    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="footer-about">
              <img src="<?php echo HTTP_ROOT?>/frontend/getcollege-small-logo.png">
              <p>GetCollege.in walks you through all phases of your college academics to your first job, by providing interactive dashboard. We help you to choose college, prerpare for competitive exams or jobs, scholarships and otherfacilities to accelerate your performance by saving your time searching for them.</p>
            </div>
          </div>
          <div class="col-md-3">  
            <ul class="footer-list">
              <li class="m-b-10">Categories</li>
              <li>
                  <a href="<?php echo HTTP_ROOT?>Homes/careerget">Careers</a>
              </li>
              <li>
                  <a href="<?php echo HTTP_ROOT?>Homes/whygetcollege">Why GetCollege</a>
              </li>
              <li>
                  <a href="<?php echo HTTP_ROOT?>Homes/ourteam">Our Team</a>
              </li>
              <li>
                  <a  data-toggle="modal" data-target="#myModal-id20" href="#">Suggest a College</a>
              </li>
              <li>
                  <a  data-toggle="modal" data-target="#referModal"  href="#">Suggest a School</a>
                            </li>
            </ul>
          </div>
          <div class="col-md-3">  
            <ul class="footer-list">
              <li class="m-b-10">About GetCollege</li>
              <li><a href="<?php echo HTTP_ROOT?>homes/faqs">FAQs</a></li>
              <li><a href="<?php echo HTTP_ROOT?>homes/aboutus">About Us</a></li>
              <li><a href="<?php echo HTTP_ROOT?>homes/contact">Contact Us</a></li>
              <li><a href="<?php echo HTTP_ROOT?>homes/terms">Terms & Conditions</a></li>
              <li><a href="<?php echo HTTP_ROOT?>homes/privacy">Privacy Policy</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&tabs=timeline&width=280&height=270&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="300" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
          </div>
        </div>
      </div>
      <div class="copyright text-center">
        <p>Copyright © 2017 GetCollege.in All Rights Reserved</p>
      </div>
    </footer>