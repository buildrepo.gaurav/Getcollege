<style>
.user-image {
    float: left;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    margin-right: 10px;
    margin-top: -2px;
}
.dropdown-menu {
    position: absolute;
    right: 0;
    left: auto;
}
.dropdown-menu {
    top: 100%;
    z-index: 1000;
    float: left;
    min-width: 350px;
    padding: 5px 0;
    margin: 2px 0 0;
    font-size: 14px;
    text-align: left;
    list-style: none;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ccc;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: 4px;
    box-shadow: 0 6px 12px rgba(0,0,0,.175);
}
.dropdown-menu>li.user-header {
    height: 140px;
    padding: 10px;
    text-align: center;
}
.img-circle {
    border-radius: 50%;
}
.user-header>img {
    z-index: 5;
    height: 90px;
    width: 90px;
    border: 3px solid;
    border-color: transparent;
    border-color: rgba(255,255,255,0.2);
}
</style>
<nav class="navbar navbar-cstm navbar-cstm-top">
          <div class="container-fluid p-0">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo HTTP_ROOT?>"><img class="logo" src="<?php echo HTTP_ROOT?>/frontend/getcollege-small-logo.png"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right navbar-ul-cstm">
                <li><a href="https://www.facebook.com/GetCollege.IN/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/GetcollegeIn" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/getcollege.in/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li>
                  <?php if($this->Session->check('Student')){?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php if(!empty($this->Session->read('Student.image'))){?>
                      <img src="<?php echo HTTP_ROOT.'img/studentImages/small/'.$this->Session->read('Student.image');?>" class="user-image" alt="User Image">
                      <?php } else{?>
                      <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="user-image" alt="User Image">
                      <?php }?>
                      <span class="hidden-xs"><?php echo $this->Session->read('Student.name');?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header">
                        <div class="col-md-6">
                        <?php if(!empty($this->Session->read('Student.image'))){?>
                          <img src="<?php echo HTTP_ROOT.'img/studentImages/small/'.$this->Session->read('Student.image');?>" class="img-circle" alt="User Image" width="110px">
                          <?php } else{?>
                          <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="img-circle" alt="User Image" width="110px">
                          <?php }?>
                        </div>
                        <div class="col-md-6">
                          <label style="color:black;font-size:12px;     word-wrap: break-word;"><?php echo $this->Session->read('Student.name');?></label>
                          <label style="color:black;font-size:12px;    word-wrap: break-word;"><?php echo $this->Session->read('Student.email');?></label>
                          
                          <a href="<?php echo HTTP_ROOT?>Homes/studentdashboard" class="btn btn-primary" style="background:#36454F;color:#fff; padding: 6px 12px;">My Profile</a>
                        </div>
                        
                          
                
                      </li>
                      <!-- Menu Body -->

                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-left" style="; padding-left: 5px;">
                          <a class="btn btn-primary" data-toggle="modal" href="javascript:void(0)" data-target="#student-password-update" style="background:#36454F;color:#fff; padding: 6px 12px;">Change Password</a>
                          
                        </div>
                        <div class="pull-right" style="; padding-right: 5px;">
                          <a href="<?php echo HTTP_ROOT?>Homes/logout" class="btn btn-primary" style="background:#36454F;color:#fff; padding: 6px 12px;">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  <?php }?>
                  <?php if($this->Session->check('College')){?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <?php if(!empty($this->Session->read('College.image'))){?>
                      <img src="<?php echo HTTP_ROOT.'img/collegeImages/small/'.$this->Session->read('College.image');?>" class="user-image" alt="User Image">
                      <?php } else{?>
                      <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="user-image" alt="User Image">
                      <?php }?>
                      <span class="hidden-xs"><?php echo $this->Session->read('College.name');?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header">
                        <div class="col-md-6">
                        <?php if(!empty($this->Session->read('College.image'))){?>
                          <img src="<?php echo HTTP_ROOT.'img/collegeImages/small/'.$this->Session->read('College.image');?>" class="img-circle" alt="User Image" width="110px">
                          <?php } else{?>
                          <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="img-circle" alt="User Image" width="110px">
                          <?php }?>
                        </div>
                        <div class="col-md-6">
                          <label><?php echo $this->Session->read('College.name');?></label>
                          <label style="color:black;font-size:12px;     word-wrap: break-word;"><?php echo $this->Session->read('College.email');?></label>
                          <a href="<?php echo HTTP_ROOT?>Homes/collegeDashboard" class="btn btn-primary" style="background:#36454F;color:#fff; padding: 6px 12px;">My Profile</a>
                        </div>
                      </li>
                      <!-- Menu Body -->

                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-left" style="; padding-left: 5px;">
                          <a class="btn btn-primary" data-toggle="modal" href="javascript:void(0)" data-target="#student-password-update" style="background:#36454F;color:#fff; padding: 6px 12px;">Change Password</a>
                        </div>
                        <div class="pull-right" style="; padding-right: 5px;">
                          <a href="<?php echo HTTP_ROOT?>Homes/logout" class="btn btn-primary" style="background:#36454F;color:#fff; padding: 6px 12px;">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  <?php }?>
                </li>
                <?php if(!$this->Session->check('College') && !$this->Session->check('Student')){?>
                <li><a href="#" data-toggle="modal" data-target="#my_Login13"><span class="lnr lnr-user"></span> &nbsp;Register</a></li>
                <li class="nav-highlighted"><a href="#" data-toggle="modal" data-target="#myModal"><span class="lnr lnr-user"></span> &nbsp;Log In</a></li>
                <?php } ?>
              </ul>
              <ul class="nav navbar-nav navbar-ul-cstm">
                <li><a href="#"><span class="lnr lnr-phone-handset"></span> &nbsp;+91 120 420 2822</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        <nav class="navbar navbar-cstm navbar-cstm-scroll">
          <div class="container-fluid p-0">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo HTTP_ROOT?>"><img class="logo" src="<?php echo HTTP_ROOT?>/frontend/getcollege-small-logo.png"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right navbar-ul-cstm">
                <li>
                  <?php if($this->Session->check('Student')){?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php if(!empty($this->Session->read('Student.image'))){?>
                      <img src="<?php echo HTTP_ROOT.'img/studentImages/small/'.$this->Session->read('Student.image');?>" class="user-image" alt="User Image">
                      <?php } else{?>
                      <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="user-image" alt="User Image">
                      <?php }?>
                      <span class="hidden-xs"><?php echo $this->Session->read('Student.name');?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header">
                        <div class="col-md-6">
                        <?php if(!empty($this->Session->read('Student.image'))){?>
                          <img src="<?php echo HTTP_ROOT.'img/studentImages/small/'.$this->Session->read('Student.image');?>" class="img-circle" alt="User Image" width="110px">
                          <?php } else{?>
                          <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="img-circle" alt="User Image" width="110px">
                          <?php }?>
                        </div>
                        <div class="col-md-6">
                          <label style="color:black;font-size:12px;     word-wrap: break-word;"><?php echo $this->Session->read('Student.name');?></label>
                          <label style="color:black;font-size:12px;    word-wrap: break-word;"><?php echo $this->Session->read('Student.email');?></label>
                          
                          <a href="<?php echo HTTP_ROOT?>Homes/studentdashboard" class="btn btn-primary" style="background:#36454F;color:#fff">My Profile</a>
                        </div>
                        
                          
                
                      </li>
                      <!-- Menu Body -->

                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-left">
                          <a class="btn btn-primary" data-toggle="modal" href="javascript:void(0)" data-target="#student-password-update" style="background:#36454F;color:#fff">Change Password</a>
                          
                        </div>
                        <div class="pull-right">
                          <a href="<?php echo HTTP_ROOT?>Homes/logout" class="btn btn-primary" style="background:#36454F;color:#fff">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  <?php }?>
                  <?php if($this->Session->check('College')){?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <?php if(!empty($this->Session->read('College.image'))){?>
                      <img src="<?php echo HTTP_ROOT.'img/collegeImages/small/'.$this->Session->read('College.image');?>" class="user-image" alt="User Image">
                      <?php } else{?>
                      <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="user-image" alt="User Image">
                      <?php }?>
                      <span class="hidden-xs"><?php echo $this->Session->read('College.name');?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header">
                        <div class="col-md-6">
                        <?php if(!empty($this->Session->read('College.image'))){?>
                          <img src="<?php echo HTTP_ROOT.'img/collegeImages/small/'.$this->Session->read('College.image');?>" class="img-circle" alt="User Image" width="110px">
                          <?php } else{?>
                          <img src="<?php echo HTTP_ROOT.'img/default-user.png'?>" class="img-circle" alt="User Image" width="110px">
                          <?php }?>
                        </div>
                        <div class="col-md-6">
                          <label><?php echo $this->Session->read('College.name');?></label>
                          <label style="color:black;font-size:12px;     word-wrap: break-word;"><?php echo $this->Session->read('College.email');?></label>
                          <a href="<?php echo HTTP_ROOT?>Homes/collegeDashboard" class="btn btn-primary" style="background:#36454F;color:#fff">My Profile</a>
                        </div>
                      </li>
                      <!-- Menu Body -->

                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-left">
                          <a class="btn btn-primary" data-toggle="modal" href="javascript:void(0)" data-target="#student-password-update" style="background:#36454F;color:#fff">Change Password</a>
                        </div>
                        <div class="pull-right">
                          <a href="<?php echo HTTP_ROOT?>Homes/logout" class="btn btn-primary" style="background:#36454F;color:#fff">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  <?php }?>
                </li>
                <?php if(!$this->Session->check('College') && !$this->Session->check('Student')){?>
                <li><a href="#" data-toggle="modal" data-target="#my_Login13"><span class="lnr lnr-user"></span> &nbsp;Register</a></li>
                <li class="nav-highlighted"><a href="#" data-toggle="modal" data-target="#myModal"><span class="lnr lnr-user"></span> &nbsp;Log In</a></li>
                <?php } ?>
              </ul>
              <form class="form-inline">
                <div class="nav-scroll-input-wrap">
                  <div class="select-wrap">
                    <span class="lnr lnr-apartment select-wrap-span select-wrap-span2"></span>
                    <select id="source">
                        <option selected="selected" value="IN">Colleges</option>
                        <!-- <option value="IN">Videographer</option> -->
                    </select>
                    <span class="lnr lnr-chevron-down select-chevron select-chevron2"></span>
                  </div>
                  <div class="input-wrap2">
                    <span class="lnr lnr-map-marker"></span>
                    <input type="text" name="cities" class="form-control nav-scroll-input" id="autocompletetop" placeholder="Select Your Location">
                  </div>
                  <div class="input-wrap2">
                    <span class="lnr lnr-list"></span>
                    <input type="text" name="category" class="form-control nav-scroll-input" id="autocomplete2top" placeholder="Select a Category">
                  </div>
                  <button type="submit" class="nav-scroll-btn"><span class="lnr lnr-magnifier"></span></button>
                </div>
              </form>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>