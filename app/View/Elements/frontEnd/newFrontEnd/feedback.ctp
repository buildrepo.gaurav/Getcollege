<div class="feedback-panel" style="height: 281px;">
    <a href="" class="feedback-tab"><span>Contact Us</span></a>
    <h3>Contact Us</h3>
    <div id="form-wrap">
      <form id="ask" action="<?php echo HTTP_ROOT?>Homes/contact_us" method="post" novalidate="novalidate">
        <input type="hidden" name="data[Contact][name]" value="User" placeholder="Name" class="form-control" style="padding:6px 6px;">
        <div style="position:relative;float: left; width: 100%;" class="">
        <label><span class="asterik">*</span>Email: </label>
        <input type="text" name="data[Contact][email]" class="form-control required" id="email1"><br>
        </div>
        <div style="position:relative;float: left; width: 100%;" class="">
          <label><span class="asterik">*</span>Message:</label>
          <textarea style="resize:none;" col="0" rows="0" name="data[Contact][message]" class="form-control required" id="message"></textarea>
        </div>      
        <br>
        <input type="submit" class="btn btn-primary m-t-15 btn-block button srch-btn" value="Submit" style="margin:0;">
      </form>
    </div>
  </div>
  <style type="text/css">
    .asterik
    {
      color: red;
    }
  </style>