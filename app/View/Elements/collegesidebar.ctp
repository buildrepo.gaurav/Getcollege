<style>
  .profile-sidebar1{
    padding: 20px 0 10px 0;
      background: #fff;
  }
</style>
<div class="col-md-3">
				<div class="profile-sidebar1">
					<!-- SIDEBAR USERPIC -->
					<div class="profile-userpic">
						<img src="<?php echo HTTP_ROOT?>img/<?php echo empty($clgdetails['College']['image']) ? 'default-user.png' : 'collegeImages/thumbnail/'.$clgdetails['College']['image'];?>" class="img-responsive" alt="">
					</div>
					<!-- END SIDEBAR USERPIC -->
					<!-- SIDEBAR USER TITLE -->
					<div class="profile-usertitle">
						<div class="profile-usertitle-name">
							 <?php echo $clgdetails['College']['name'];?>
						</div>
						<div class="profile-usertitle-job">
							<?php echo $clgdetails['College']['email'];?>
						</div>
					</div>
					<!-- END SIDEBAR USER TITLE -->
					<!-- SIDEBAR BUTTONS -->
					<div class="profile-userbuttons">
					
						<a class="btn btn-sm" style="background:#007d94;color:#fff" data-toggle="modal" href="javascript:void(0)" data-target="#college-image">Change Image</a>

						<a data-toggle="modal" href="javascript:void(0)" data-target="#college-password-update" style="background:#007d94;color:#fff" class="btn btn-sm">Change Password</a>
					</div>
					<!-- END SIDEBAR BUTTONS -->
					<!-- SIDEBAR MENU -->
					<div class="profile-usermenu">
						<ul class="nav">
							<li class="<?php echo $this->params['action'] == 'collegeDashboard' ? 'active' : '';?>">
								<a href="<?php echo HTTP_ROOT?>Homes/collegeDashboard">
								<i class="glyphicon glyphicon-home"></i>
								Dashboard </a>
							</li>
							<li class="<?php echo $this->params['action'] == 'editCollege' ? 'active' : '';?>">
								<a href="<?php echo HTTP_ROOT?>Homes/editCollege/<?php echo base64_encode($clgdetails['College']['id']);?>">
								<i class="fa fa-edit"></i>
								Edit Profile </a>
							</li>
							<li class="<?php echo $this->params['action'] == 'manageCourse' ? 'active' : '';?>">
								<a href="<?php echo HTTP_ROOT?>Homes/manageCourse/<?php echo base64_encode($clgdetails['College']['id']);?>">
								<i class="fa fa-graduation-cap"></i>
								Manage Courses </a>
							</li>
							<li class="<?php echo $this->params['action'] == 'addCourse' ? 'active' : '';?>">
								<a href="<?php echo HTTP_ROOT?>Homes/addCourse">
								<i class="fa fa-plus"></i>
								Add Course </a>
							</li>
							<li class="<?php echo $this->params['action'] == 'collegeGallery' ? 'active' : '';?>">
								<a href="<?php echo HTTP_ROOT?>Homes/collegeGallery">
								<i class="fa fa-picture-o"></i>
								College Gallery </a>
							</li>
							<li class="<?php echo $this->params['action'] == 'manageEvent' ? 'active' : '';?>">
								<a href="<?php echo HTTP_ROOT?>Homes/manageEvent">
								<i class="fa fa-info"></i>
								College Events </a>
							</li>
						</ul>
					</div>
					<!-- END MENU -->
				</div>
			</div>