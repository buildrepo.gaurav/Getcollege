<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 12%;
    position: absolute;
}
.form-control{
  width:100% !important;
}
.input-sm{
  float: right;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Program
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageProgram">Program</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Program</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form role="form" id="add-course-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Stream</th>
                  <th>Program</th>
                  <th>Alias Name</th>
                  <th>Description</th>
                  <!-- <th>From Date</th>
                  <th>To Date</th> -->
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="program-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                    <select class="form-control required" required name="data[1][Program][stream_id]">
                      <option value="">Select Stream</option>
                      <?php foreach($streams as $stream){?>
                        <option value="<?php echo $stream['Stream']['id']?>" <?php echo @$stream['Stream']['id'] == @$program['Program']['stream_id'] ? 'selected' : '';?> ><?php echo $stream['Stream']['stream']?></option>
                      <?php }?>  
                  </select>
                  </td>
                  <td>
                  <i class="fa input-error1"></i>

                    <input class="form-control required" placeholder="Program" name="data[1][Program][program]" value="<?php echo @$program['Program']['program']?>">
                    <input type="hidden" name="data[1][Program][id]" value="<?php echo @$programId?>">
                  </td>
                   <td>
                  <i class="fa input-error1"></i>

                    <input class="form-control required" placeholder="Alias Name" name="data[1][Program][alias]" value="<?php echo @$program['Program']['alias']?>">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>

                    <textarea class="form-control" placeholder="Description" name="data[1][Program][description]"><?php echo @$program['Program']['description']?></textarea>
                  </td>
                  <!-- <td>
                  <i class="fa input-error1"></i>
                    
                    <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data[1][Program][from_date]" value="<?php echo @$program['Program']['from_date']?>">
                  </td>
                  <td>
                                 <i class="fa input-error1"></i>
                    <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data[1][Program][to_date]" value="<?php echo @$program['Program']['to_date']?>">
                  </td> -->
                  <td>
                    <a href="javascript:void(0);" class="append-program-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Program Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Stream</th>
                    <th>Program</th>
                    <th>Alias</th>
                    <th>Description</th>
                    <!-- <th>To Date</th>
                    <th>From Date</th> -->
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($programs as $data){?>
                  <tr>
                   <td>
                       <select class="form-control required" readonly required name="stream_id">
                      <option value="">Select Stream</option>
                      <?php foreach($streams as $stream){?>
                        <option value="<?php echo $stream['Stream']['id']?>" <?php echo @$stream['Stream']['id'] == @$data['Program']['stream_id'] ? 'selected' : '';?> ><?php echo $stream['Stream']['stream']?></option>
                      <?php }?>  
                  </select>
                    </td>
                    <td>
                      <input class="form-control required" readonly placeholder="Program" name="data[1][Program][program]" value="<?php echo @$data['Program']['program']?>">
                    <input type="hidden"  name="id" value="<?php echo @$data['Program']['id']?>">
                    </td>
                    <td>
                     <input class="form-control required" placeholder="Alias Name" readonly name="alias" value="<?php echo @$data['Program']['alias']?>">
                    </td>
                    <td>
                      <textarea class="form-control" placeholder="Description" readonly name="description"><?php echo @$data['Program']['description']?></textarea>
                    </td>
                    <!-- <td>
                       <input type="text" class="form-control required event-datepicker" required placeholder="To Date" readonly name="to_date" value="<?php echo @$data['Program']['to_date']?>">
                    </td>
                    <td>
                       <input type="text" class="form-control required event-datepicker" required placeholder="From Date" readonly name="from_date" value="<?php echo @$data['Program']['from_date']?>">
                    </td> -->
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                      <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a> 
                      <a model = "Program" data-id="<?php echo $data['Program']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Program']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Program']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a  model = "Program" data-id="<?php echo base64_encode($data['Program']['id'])?>"  class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Program']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
             <?php if(!empty($programs)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script>
    //manage Program 

   let i = 100;
    $(document).on('click','.append-program-a',function(){
      
      let div = ' <tr id="append_'+i+'"> <td> <i class="fa input-error1"></i><select class="form-control required" required name="data['+i+'][Program][stream_id]"> <option value="">Select Stream</option> <?php foreach($streams as $stream){?> <option value="<?php echo $stream["Stream"]["id"]?>"  ><?php echo $stream["Stream"]["stream"]?></option> <?php }?> </select> </td><td><i class="fa input-error1"></i> <input class="form-control required" placeholder="Program" name="data['+i+'][Program][program]" >  </td><td><i class="fa input-error1"></i><input class="form-control required" placeholder="Alias Name" name="data['+i+'][Program][alias]" value="<?php echo @$program['Program']['alias']?>"></td><td> <i class="fa input-error1"></i><textarea class="form-control" placeholder="Description" name="data['+i+'][Program][description]"><?php echo @$program['Program']['description']?></textarea> </td><td> <a href="javascript:void(0);" class="append-program-a"><i class="fa fa-plus"></i></a><a href="javascript:void(0);" class="delete-program-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#program-table').append(div);
       $('.event-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd",
      startDate : new Date()
    });
    });
    
    $(document).on('click','.delete-program-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });

    //manage Program 
  </script>
  <script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editProgram',
          data : {data : data},
          success : function(resp){
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          
          }
      });
    })
  </script>
        <script type="text/javascript">

    let offset = 4;
    let action  = 'getProgram';
    let resp = '';

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           console.log(resp);
           resp.forEach(function(element) {
              if(element.Program.status == 1){
                var is_delete = (element.Program.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                  table.row.add(['<select id="pro-'+element.Program.id+'" class="form-control required" readonly required name="stream_id"><option value="">Select Stream</option><?php foreach($streams as $stream){?><option value="<?php echo $stream['Stream']['id']?>" ><?php echo $stream['Stream']['stream']?></option><?php }?></select>','<input class="form-control required" readonly placeholder="Program" name="data[1][Program][program]" value="'+element.Program.program+'"><input type="hidden"  name="id" value="'+element.Program.program+'">','<input class="form-control required" placeholder="Alias Name" readonly name="alias" value="'+element.Program.alias+'">','<textarea class="form-control" placeholder="Description" readonly name="description">'+element.Program.description+'</textarea>','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Program" data-id="'+element.Program.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a  model = "Program" data-id="'+btoa(element.Program.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw(); 
                }    
              else{
                var is_delete = (element.Program.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                  table.row.add(['<select id="pro-'+element.Program.id+'" class="form-control required" readonly required name="stream_id"><option value="">Select Stream</option><?php foreach($streams as $stream){?><option value="<?php echo $stream['Stream']['id']?>" ><?php echo $stream['Stream']['stream']?></option><?php }?></select>','<input class="form-control required" readonly placeholder="Program" name="data[1][Program][program]" value="'+element.Program.program+'"><input type="hidden"  name="id" value="'+element.Program.program+'">','<input class="form-control required" placeholder="Alias Name" readonly name="alias" value="'+element.Program.alias+'">','<textarea class="form-control" placeholder="Description" readonly name="description">'+element.Program.description+'</textarea>','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Program" data-id="'+element.Program.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a model = "Program" data-id="'+btoa(element.Program.id)+'" class="fafa-icons delete-row"  title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a> ']).draw();
                  }
                  $('#pro-'+element.Program.id).val(element.Program.stream_id).change();
              });
           offset += 4;
 
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
     // }
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
