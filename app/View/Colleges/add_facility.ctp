<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Facility
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageFacility">Facility</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Facility</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form role="form" id="add-state-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Icon</th>
                  <th>Name</th>
                  <th>From Date</th>
                  <th>To Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="stream-table">
                <tr>
                  <td>
                    <input type="file" class="form-control" required placeholder="Enter Facility Name" name="file[1]">
                  </td>  
                  <td>
                   <i class="fa input-error"></i> <input type="text" class="form-control required" required placeholder="Enter Facility Name" name="data[1][Facility][name]" value="<?php echo @$facility['Facility']['name']?>">
                    <input type="hidden" name="data[1][Facility][id]" value="<?php echo @$facilityId?>">
                  </td>
                  <td>
                   <i class="fa input-error"></i> <input type="text" class="form-control required event-datepicker" required placeholder="Effective From date" name="data[1][Facility][from_date]" value="<?php echo @$facility['Facility']['from_date']?>">
                  </td> 
                  <td>
                  <i class="fa input-error"></i> <input type="text" class="form-control required event-datepicker" required placeholder="Effective To Date" name="data[1][Facility][to_date]" value="<?php echo @$facility['Facility']['to_date']?>">
                  </td> 
                  <td>
                    <a href="javascript:void(0);" class="append-stream-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Stream Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Icon</th>
                    <th>Name</th>
                    <th>From Date</th>
                    <th>TO Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($facilities as $data){?>
                  <tr>
                    <td>
                      <img src="<?php echo HTTP_ROOT.'img/'.$data['Facility']['icon']?>" class="img-responisve">
                    </td>
                     <td>
                      <?php echo $data['Facility']['name']?>
                    </td>
                     <td>
                      <?php echo $data['Facility']['to_date']?>
                    </td>
                     <td>
                      <?php echo $data['Facility']['from_date']?>
                    </td>
                    <td>
                      <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Colleges/addFacility/'.$data['Stream']['id']?>"><i class="fa fa-edit"></i></a> 
                      <a class="fafa-icons" title="Delete" href="<?php echo HTTP_ROOT.'Colleges/delete/Facility/'.base64_encode($data['Stream']['id'])?>"><i class="fa fa-trash"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
             <?php if(!empty($facilities)){?>
                <?php echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>s