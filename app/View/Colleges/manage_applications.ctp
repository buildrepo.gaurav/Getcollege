<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style>
  a.fafa-icons{
      margin-left: 10px;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Applications
        <!-- <a href="<?php echo HTTP_ROOT?>Colleges/addCollege" class="btn btn-primary">Add Colleges</a> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageApplications">Manage Applications</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Applications Listing</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="college-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  
                  <th>Student Name</th>
                  <th>No. of forms</th>
                  <th>Total Amount</th>
                  <th>Date</th>
                  
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($orders as $order){?>
                <tr>
                  
                  <td>
                    <?php echo $order['Student']['name']?>
                  </td>
                  <td>
                    <?php echo $order['Order']['no_of_forms']?>
                  </td>
                   <td>
                    <?php echo $order['Order']['amount']?>
                  </td>
                 
                  <td>
                    <?php echo $order['Order']['created_date']?>
                  </td>
                  <td>
                    <a target="_blank" class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Colleges/viewOrder/'.$order['Order']['id']?>"><i class="fa fa-eye"></i></a> 
                    
                    
                    
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
             
            </div>
            <!-- /.box-body -->
            
          </div>
         
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
  $('select').select2();

    let offset = 25;
    let action  = 'getCollege';
    let resp = '';

    let table =   $('#college-listing').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           resp.forEach(function(element) {
                  if(element.College.status == 1){
                    var is_delete = (element.College.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add([element.College.name,element.College.email,element.College.phone1,' <form action="'+baseUrl+'/Colleges/changeSequence/'+element.College.id+'" method="post"> <input type="number" name="sequence" value="element.College.sequence" class="form-control" style="width: 70px;" min="0"> <button type="submit" id="view-form" class="btn btn-xs btn-primary">submit</button> </form>',element.CreatedBy.username,element.UpdatedBy.username,element.College.update_date,'<a target="_blank" class="fafa-icons" title="Edit" href="'+baseUrl+'Colleges/editCollege/'+element.College.id+'"><i class="fa fa-edit"></i></a> <a target="_blank" class="fafa-icons" title="Manage Courses" href="'+baseUrl+'Colleges/manageCollegeCourse/'+element.College.id+'"><i class="fa fa-graduation-cap"></i></a> <a target="_blank" class="fafa-icons" title="Manage Gallery" href="'+baseUrl+'Colleges/collegeGallery/'+element.College.id+'"><i class="fa fa-picture-o"></i></a> <a target="_blank" class="fafa-icons" title="Manage Events" href="'+baseUrl+'Colleges/manageEvent/'+element.College.id+'"><i class="fa fa-info"></i></a><a model = "College" data-id="'+element.College.id+'" class="fafa-icons change-status"  title="Active" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a> <a model = "College" data-id="'+btoa(element.College.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0);"><i class="fa '+is_delete+'"></i></a> ']).draw();
                }
                  else
                  {
                    var is_delete = (element.College.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add([element.College.name,element.College.email,element.College.phone1,' <form action="'+baseUrl+'/Colleges/changeSequence/'+element.College.id+'" method="post"> <input type="number" name="sequence" value="element.College.sequence" class="form-control" style="width: 70px;" min="0"> <button type="submit" id="view-form" class="btn btn-xs btn-primary">submit</button> </form>',element.CreatedBy.username,element.UpdatedBy.username,element.College.update_date,'<a target="_blank" class="fafa-icons" title="Edit" href="'+baseUrl+'Colleges/editCollege/'+element.College.id+'"><i class="fa fa-edit"></i></a> <a target="_blank" class="fafa-icons" title="Manage Courses" href="'+baseUrl+'Colleges/manageCollegeCourse/'+element.College.id+'"><i class="fa fa-graduation-cap"></i></a> <a target="_blank" class="fafa-icons" title="Manage Gallery" href="'+baseUrl+'Colleges/collegeGallery/'+element.College.id+'"><i class="fa fa-picture-o"></i></a> <a target="_blank" class="fafa-icons" title="Manage Events" href="'+baseUrl+'Colleges/manageEvent/'+element.College.id+'"><i class="fa fa-info"></i></a><a model = "College" data-id="'+element.College.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a>  <a model = "College" data-id="'+btoa(element.College.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0);"><i class="fa '+is_delete+'"></i></a>']).draw(); 
                }
              });
           offset += 25;
 
        }
  });
$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
        /* table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>