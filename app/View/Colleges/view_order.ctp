<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Application
        <a href="<?php echo HTTP_ROOT?>img/docs/<?php echo @$order['Order']['pdf']?>" target="_blank" class="btn btn-primary">Check PDF</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageApplications">Applications</a></li>
      </ol>
    </section>

    <section class="content">
  <div class="">
      <div class="row">
      <div class="col-md-12">
              <div class="profile-content">
           <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style>
  a.fafa-icons{
      margin-left: 10px;
}
.fa-trash,.fa-trash-o,.fa-eye,.fa-plus,.fa-save,.fa-building{
  font-size: 24px;
}

</style>
<div class="">

    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
    <h4 style="margin-left: 3%; background-color:#666666; margin:0px; padding: 1%; margin-top:-1.5%; color: white;">Formsadda Colleges</h4>
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box box-primary" style="margin-top: 2%;">
          <div class="row">
          
          <div class="col-md-1"></div>
            <div class="col-md-5"><b>Colleges</b></div>
            <div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>Form Fees</b></div></div>
            <div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>FormsADDA Fees</b></div></div>
            <div class="col-md-1 Head_Fees"><div width="100%" style="text-align:centre;"><b>Total</b></div></div>
          </div>
          <br>
          <?php $i=1; $cnt = count($order['OrderMeta']);  foreach($order['OrderMeta'] as $ord){?>
          
            <?php if(@$ord['CollegeCourse']['direct_apply'] == 0){ ?>
            <div class="row" id="row-id-<?php echo $ord['CollegeCourse']['id']?>">
            <div class="col-md-1"></div>
            <div class="col-md-5"><?php echo $ord['College']['name']?></div>
            
            <div class="col-md-2"><div width="100%" class="Price"><strike>Rs.<?php echo $ord['CollegeCourse']['registration_fee']?></strike></div></div>
            <div class="col-md-2"><div width="100%" class="Price">Rs.<?php echo $ord['CollegeCourse']['formsadda_cost']?></div></div>
            <?php }?>
            <?php @$tot = @$tot + $ord['CollegeCourse']['formsadda_cost'];?>

            <?php if($cnt == $i){?>
              <div class="col-md-1"><div width="100%" class="Price"><?php echo @$tot?></div></div>
            <?php }?>  
          </div>
          <br>
          <?php $i++;  }?>
</div>
        </div>
        <div class="box box-primary">
          <div class="row">
          <h4 style="margin-left: 3.5%; margin-bottom: 2%; background-color: #666666; padding: 1%; color: white ; margin-right: 3.3%; ">Direct Apply College</h4>
          <div class="col-md-1"></div>
            <div class="col-md-5"><div style="margin-left: 1%;"><b>Colleges</b></div></div>
            <div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>Apply Fees</b></div></div>
            <div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>Application No.</b></div></div>
            <div class="col-md-1 Head_Fees"><div width="100%" ><b>Application Reciept</b></div></div>
          </div>
          <br>
          <form method="post" action="<?php echo HTTP_ROOT?>Homes/saveDirectForm" enctype="multipart/form-data">
          <?php $i=1; $j=1;$cnt = count($order['OrderMeta']);  foreach($order['OrderMeta'] as $ord){?>
          
            <?php if(@$ord['CollegeCourse']['direct_apply'] == 1){ ?>
            
              <div class="row" id="row-id-<?php echo $ord['CollegeCourse']['id']?>">
            <div class="col-md-1"></div>
            <div class="col-md-5"><?php echo $ord['College']['name']?></div>
            
            <div class="col-md-2"><div width="100%" class="Price">Direct Applied</div></div>
            <div class="col-md-2"><div width="100%" class="Price">
            <?php if(empty($ord['DirectApplication']['application_no'])){?>
              <p>NO Entered Yet !!!</p>
            
            <?php }else{$j++;?>
              <?php echo $ord['DirectApplication']['application_no']?>
             <?php }?> 
           
            </div></div>
            <?php if(empty($ord['DirectApplication']['image'])){?>
            <div class="col-md-2"><div width="100%" class="Price">Not uploaded</div></div>
            <?php }else{?>
              <a href="<?php echo HTTP_ROOT?>img/docs/<?php echo $ord['DirectApplication']['image']?>" class="btn btn-primary" target="_blank">open file</a>
             <?php }?> 
            <?php }?>
              
          </div>
          <br>
          <?php $i++;  }?>
       
          </form>
        
          </div>
          <!-- </form> -->
          <!-- /.box -->
    
        <!-- /.col -->
      </div>
      <div>
      <?php echo $this->element('frontEnd/applicationdetails');?>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
              </div>
      </div>
    </div>
  </div>
</section>