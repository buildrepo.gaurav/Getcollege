<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 15%;
    position: absolute;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Event
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageCollege">Manage College</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageEvent/<?php echo $collegeId?>">Event</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Event</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="add-event-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>From Date</th>
                  <th>To Date</th>
                  <th>Location</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="events-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                   <input class="form-control required" placeholder="Event Name" name="data[1][Event][title]" value="<?php echo @$event['Event']['title']?>">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <textarea class="form-control" placeholder="Event Details" name="data[1][Event][details]"><?php echo @$event['Event']['details']?></textarea>
                  <input type="hidden" name="data[1][Event][id]" value="<?php echo @$eventId?>">
                  <input type="hidden" name="data[1][Event][college_id]" value="<?php echo @$collegeId?>">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <input class="form-control event-datepicker required" placeholder="From Date" name="data[1][Event][start]" value="<?php echo @$event['Event']['start']?>">
               
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <input class="form-control required event-datepicker" placeholder="End Date" name="data[1][Event][end]" value="<?php echo @$event['Event']['end']?>">
                
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <input type="text"  class="form-control required" name="data[1][Event][location]" value="<?php echo @$event['Event']['location']?>" id="" onclick="initAutocomplete(this)" placeholder="Enter your address"  >
                  </td>
                  <td>
                    <a href="javascript:void(0);" class="append-event-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Events Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>From Date</th>
                    <th>To Date</th>
                    <th>Location</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($events as $data){?>
                  <tr>
                   <td>
                      <input type="text" class="form-control required" required readonly placeholder="Enter Stream" name="title" value="<?php echo $data['Event']['title']?>">
                      <input type="hidden" name="id" value="<?php echo $data['Event']['id']?>">
                      
                    </td>
                    <td>
                      <textarea class="form-control required" placeholder="Event Details" name="details" readonly><?php echo @$data['Event']['details']?></textarea>
                    </td>
                    <td>
                      <input type="text" class="form-control required event-datepicker" required readonly placeholder="Enter Date" name="start" value="<?php echo date('Y-m-d', strtotime($data['Event']['start']))?>">
                      
                    </td>
                    <td>
                      <input class="form-control required event-datepicker" placeholder="End Date" name="end" value="<?php echo date('Y-m-d', strtotime($data['Event']['end']))?>" readonly>
                      
                    </td>
                    <td>
                      <input type="text"  class="form-control required" name="location" value="<?php echo @$data['Event']['location']?>" id="" onclick="initAutocomplete(this)" placeholder="Enter your address"  readonly>
                    </td>
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                      <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a> 
                      <a model = "Event" data-id="<?php echo $data['Event']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Event']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Event']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "Event" data-id="<?php echo base64_encode($data['Event']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Event']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
             <?php if(!empty($programs)){?>
                <?php echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
   <script>
    //manage Program 

   let i = 100;
    $(document).on('click','.append-event-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i><input class="form-control required" placeholder="Event Name" name="data['+i+'][Event][title]" > </td><td> <i class="fa input-error1"></i><textarea class="form-control required" placeholder="Event Details" name="data['+i+'][Event][details]"></textarea> <i class="fa input-error1"></i><input type="hidden" name="data['+i+'][Event][college_id]" value="<?php echo @$collegeId?>"><i class="fa input-error1"></i><input type="hidden" name="data['+i+'][Event][id]" value="<?php echo @$eventId?>"> </td><td> <i class="fa input-error1"></i><input class="form-control event-datepicker required" placeholder="Start Date" name="data['+i+'][Event][start]" > </td><td> <i class="fa input-error1"></i><input class="form-control required event-datepicker" placeholder="End Date" name="data['+i+'][Event][end]" > </td><td> <i class="fa input-error1"></i><input type="text" class="form-control required" name="data['+i+'][Event][location]" id="autocomplete'+i+'" placeholder="Enter your address" onfocus="initAutocomplete(this)" > </td><td> <a href="javascript:void(0);" class="append-event-a"><i class="fa fa-plus"></i></a> <a href="javascript:void(0);" class="delete-event-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td>';
      i++;
      $('#events-table').append(div);
       $('.event-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd",
      startDate : new Date()
    });
    });
    

    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea").removeAttr('readonly');
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea").serializeArray();
      $(this).closest('tr').find("input,textarea").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editEvent',
          data : {data : data},
          success : function(resp){
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          
          }
      });
    })

    $(document).on('click','.delete-event-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
    $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      }
  });
    //manage Program 
  </script>
<script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name',

      };
      let currentId;
      function initAutocomplete(currentId) {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(currentId),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        //autocomplete2.addListener('place_changed', fillInAddress);
      }

     /* function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete2.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }*/

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
/*      function geolocate() {
        console.log('hi');
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }*/

    </script> 
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeV79BXVKaMejet1fTcWUDT4a9kci1cnI&libraries=places&callback=initAutocomplete"
        async defer></script>