<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 15%;
    position: absolute;
}
</style>
<script>
  $(document).ready(function(){
    $('.img-input').hide();
  });
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Facility
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageFacility">Facility</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Facility</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form role="form" id="add-facility-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Icon<span style="color: red">*</span></th>
                  <th>Name<span style="color: red">*</span></th><!-- 
                  <th>From Date</th>
                  <th>To Date</th> -->
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="facility-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="file" class="form-control required" placeholder="Enter Facility Name" name="file[1]">
                  </td>  
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required" required placeholder="Enter Facility Name" name="data[1][Facility][name]" value="<?php echo @$facility['Facility']['name']?>">
                    <input type="hidden" name="data[1][Facility][id]" value="<?php echo @$facilityId?>">
                  </td>
                  <!-- <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required event-datepicker" required placeholder="Effective From date" name="data[1][Facility][from_date]" value="<?php echo @$facility['Facility']['from_date']?>">
                  </td> 
                  <td>
                  <i class="fa input-error1"></i>
                   <input type="text" class="form-control required event-datepicker" required placeholder="Effective To Date" name="data[1][Facility][to_date]" value="<?php echo @$facility['Facility']['to_date']?>">
                  </td>  -->
                  <td>
                    <a href="javascript:void(0);" class="append-facility-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Facility Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Icon</th>
                    <th>Name</th><!-- 
                    <th>From Date</th>
                    <th>TO Date</th> -->
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($facilities as $data){?>
                  <form role="form" method='post' enctype="multipart/form-data">
                  <tr>
                    <td>
                      <input onchange="readURL(this);" type="file" class="form-control img-input" placeholder="Enter Facility Name" name="file">
                      <img src="<?php echo HTTP_ROOT.'img/facility/';?><?php echo empty($data['Facility']['icon']) ? "default.jpg" : $data['Facility']['icon'];?>" class="img-responisve img-img" width="50px">
                    </td>
                     <td>
                      <input type="text" class="form-control required" required placeholder="Enter Facility Name" name="name" value="<?php echo $data['Facility']['name']?>" readonly>
                    <input type="hidden" name="id" value="<?php echo $data['Facility']['id']?>" readonly>
                      
                      
                    </td>
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                      <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a>
                      <a model = "Facility" data-id="<?php echo $data['Facility']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Facility']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Facility']['status'] == '0' ? 'inactive.png' : 'active.png';?>" width="30px";></a>
                      <a model = "Facility" data-id="<?php echo base64_encode($data['Facility']['id'])?>" class="delete-row fafa-icons" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Facility']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                </form>
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
            
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find(".img-responisve").hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).next().show();
      $(this).closest('tr').find(".img-input").show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).closest('tr').find(".img-input").hide();
      $(this).prev().show();
      $(this).closest('tr').find(".img-responisve").show();
      //$('.img-img').show();

      //let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      var input = $(this).closest('tr').find(".img-input");


      console.log(input);
      file = input[0].files[0];
      formData= new FormData();
      formData.append("image", file);
      formData.append("name", $(this).closest('tr').find("input[type=text]").val());
      formData.append("id", $(this).closest('tr').find("input[type=hidden]").val());
      //console.log(formData);
      $.ajax({
          type: 'post',
          mimeType:'application/json',
          dataType:'json',
          contentType: false,
          processData: false,
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editFacility',
          data : formData,
          success : function(resp){
            console.log(resp);
            if(resp == true)
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          }
      });
    })
  </script>
     <script>
     function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                input.next()
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    //manage Program 

   let i = 100;
    $(document).on('click','.append-facility-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i><input type="file" class="form-control required"  placeholder="Enter Facility Name" name="file['+i+']"> </td><td> <i class="fa input-error1"></i><input type="text" class="form-control required" required placeholder="Enter Facility Name" name="data['+i+'][Facility][name]"> </td><td> <a href="javascript:void(0);" class="append-facility-a"><i class="fa fa-plus"></i></a><a href="javascript:void(0);" class="delete-facility-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#facility-table').append(div);
       $('#events-table').append(div);
       $('.event-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd",
      startDate : new Date()
    });
    });
    
    $(document).on('click','.delete-facility-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });
    $(document).on('click','.det-edit',function(){
      ('hide.det').hide();
    });

    //manage Program 
  </script>
    <script type="text/javascript">

    let offset = 3;
    let action  = 'getFacility';
    let resp = '';

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           //console.log(resp);
           resp.forEach(function(element) {
                  if(element.Facility.status == 1){
                    var is_delete = (element.Facility.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<input type="file" class="form-control img-input" placeholder="Enter Facility Name" name="file"><img src="'+baseUrl+'img/facility/'+element.Facility.icon+'" class="img-responisve img-img" width="50px">','<input type="text" class="form-control required" required placeholder="Enter Facility Name" name="name" value="'+element.Facility.name+'" readonly><input type="hidden" name="id" value="'+element.Facility.id+'" readonly>','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Facility" data-id="'+element.Facility.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0);" ><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a model = "Facility" data-id="'+btoa(element.Facility.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0);"><i class="fa '+is_delete+'"></i></a>']).draw(); 
                  }
                  else{
                    var is_delete = (element.Facility.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<input type="file" class="form-control img-input" placeholder="Enter Facility Name" name="file"><img src="'+baseUrl+'img/facility/'+element.Facility.icon+'" class="img-responisve img-img" width="50px">','<input type="text" class="form-control required" required placeholder="Enter Facility Name" name="name" value="'+element.Facility.name+'" readonly><input type="hidden" name="id" value="'+element.Facility.id+'" readonly>','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Facility" data-id="'+element.Facility.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a onclick="return confirm("Are you sure,You want to delete this ?");" class="fafa-icons delete-row" title="Delete" model = "Facility" data-id="'+btoa(element.Facility.id)+'" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw(); 
                  }
              });
           offset += 3;
            $('.img-input').hide();
        }
  });
   $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
