<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Team
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageTeam">Add Team</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Team</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="team-form">
                 
                  <input type="hidden" name="data[Team][id]" value="<?php echo @$team['Team']['id']?>" class="college-id">
                  <div class="box-body">
                   
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Member Name</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required" placeholder="Member Name" name="data[Team][name]" value="<?php echo @$team['Team']['name']?>">
                    </div>

                    
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Education</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required" placeholder="Education" name="data[Team][education]" value="<?php echo @$team['Team']['education']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Experience(In Yrs)</label>
                      <i class="fa input-error"></i>
                      <input class="form-control number required" placeholder="Experience" name="data[Team][experience]" value="<?php echo @$team['Team']['experience']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Google+</label>
                      <i class="fa input-error"></i>
                      <input class="form-control" placeholder="Google+" name="data[Team][google]" value="<?php echo @$team['Team']['google']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Facebook Link</label>
                      <i class="fa input-error"></i>
                      <input class="form-control" placeholder="Facebook Link" name="data[Team][fb]" value="<?php echo @$team['Team']['fb']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Twitter</label>
                      <i class="fa input-error"></i>
                      <input class="form-control" placeholder="Twitter" name="data[Team][twitter]" value="<?php echo @$team['Team']['twitter']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Instagram</label>
                      <i class="fa input-error"></i>
                      <input class="form-control" placeholder="Instagram" name="data[Team][insta]" value="<?php echo @$team['Team']['insta']?>">
                    </div>
                     
                    
                    <div class="form-group col-md-6">
                      <label for="exampleInputFile">Image</label>
                      <i class="fa input-error"></i>
                      <input type="file" name="image">

                      
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">LinkedIn</label>
                      <i class="fa input-error"></i>
                      <input class="form-control" placeholder="LinkedIn" name="data[Team][linkedin]" value="<?php echo @$team['Team']['linkedin']?>">
                    </div>

                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">About Member</label>
                      <i class="fa input-error"></i>
                      <textarea id="editor1" class="form-control" name="data[Team][about_member]"><?php echo @$team['Team']['about_member']?></textarea>
                    </div>


                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Team Members</h3>
              </div>
              <table id="team-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Education</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($teams as $data){?>
                <tr>
                  <td>
                    <img src="<?php echo HTTP_ROOT.'img/userImages/'.$data['Team']['image']?>" width="50px;">
                  </td>
                  <td>
                    <?php echo $data['Team']['name']?>
                  </td>
                  <td>
                    <?php echo $data['Team']['education']?>
                  </td>
                  <td>
                    <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Colleges/manageTeam/'.$data['Team']['id']?>"><i class="fa fa-edit"></i></a> 
                    <a model = "Team" data-id="<?php echo $data['Team']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Team']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Team']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                    <a model = "Team" data-id="<?php echo base64_encode($data['Team']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Team']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
             <?php if(!empty($teams)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script type="text/javascript">

    let offset = 4;
    let action  = 'getTeam';
    let resp = '';

    let table =   $('#team-listing').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           console.log(resp);
           resp.forEach(function(element) {
                  if(element.Team.status == 1){
                    var is_delete = (element.Team.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<img src="'+baseUrl+'img/userImages/'+element.Team.image+'" width="50px;">',element.Team.name,element.Team.education,'<a class="fafa-icons" title="Edit" href="'+baseUrl+'Colleges/manageTeam/'+element.Team.id+'"><i class="fa fa-edit"></i></a><a model = "Team" data-id="'+element.Team.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a model = "Team" data-id="'+btoa(element.Team.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw(); 
                  }
                  else{
                    var is_delete = (element.Team.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<img src="'+baseUrl+'img/userImages/'+element.Team.image+'" width="50px;">',element.Team.name,element.Team.education,'<a class="fafa-icons" title="Edit" href="'+baseUrl+'Colleges/manageTeam/'+element.Team.id+'"><i class="fa fa-edit"></i></a><a model = "Team" data-id="'+element.Team.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></i></a><a  model = "Team" data-id="'+btoa(element.Team.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();

                      //$('#course-'+element.Course.id+' select').attr(element.Course.program_id);
                  }
              });
           offset += 4;
 
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>


  