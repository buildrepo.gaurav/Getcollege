<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 15%;
    position: absolute;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Specialization
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageSpecialization">Specialization</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Specialization</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="add-course-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Course</th>
                  <th>Specialization</th>
                  <th>Description</th>
                  <!-- <th>From Date</th>
                  <th>To Date</th> -->
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="specialization-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                   <select class="form-control required" required name="data[1][Specialization][course_id]">
                    <option value="">Select Course</option>
                  <?php foreach($courses as $course){?>
                    <option value="<?php echo $course['Course']['id']?>" <?php echo @$course['Course']['id'] == @$special['Specialization']['course_id'] ? 'selected' : '';?> ><?php echo $course['Course']['course']?></option>
                  <?php }?>  
                  </select>
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <input class="form-control required" placeholder="Specialization" name="data[1][Specialization][specialization]" value="<?php echo @$special['Specialization']['specialization']?>">
                  <input type="hidden" name="data[1][Specialization][id]" value="<?php echo @$specialId?>">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <input class="form-control" placeholder="Description" name="data[1][Specialization][description]" value="<?php echo @$special['Specialization']['description']?>">
               
                  </td>
                  
                  <!-- <td>
                  <i class="fa input-error1"></i>
                  <input class="form-control required event-datepicker" placeholder="From Date" name="data[1][Specialization][from_date]" value="<?php echo @$special['Specialization']['from_date']?>">
                
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <input class="form-control required event-datepicker" placeholder="To Date" name="data[1][Specialization][to_date]" value="<?php echo @$special['Specialization']['to_date']?>">
                
                  </td> -->
                  <td>
                    <a href="javascript:void(0);" class="append-specialization-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tbody>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Course Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Course</th>
                    <th>Specialization</th>
                    <th>Description</th>
                    <!-- <th>To Date</th>
                    <th>From Date</th> -->
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($specials as $data){?>
                  <tr class="course-list">
                   <td>
                      <select class="form-control required" required readonly name="course_id">
                    <option value="">Select Course</option>
                  <?php foreach($courses as $course){?>
                    <option value="<?php echo $course['Course']['id']?>" <?php echo @$course['Course']['id'] == @$data['Specialization']['course_id'] ? 'selected' : '';?> ><?php echo $course['Course']['course']?></option>
                  <?php }?>  
                  </select>
                    </td>
                    <td>
                        <input class="form-control required" placeholder="Specialization" readonly name="specialization" value="<?php echo @$data['Specialization']['specialization']?>">
                  <input type="hidden" readonly name="id" value="<?php echo @$data['Specialization']['id']?>">
                    </td>
                    <td>
                      <input class="form-control" placeholder="Description" readonly name="description" value="<?php echo @$data['Specialization']['description']?>">
                    </td>
                    <!-- <td>
                      <input class="form-control required event-datepicker" placeholder="To Date" readonly name="to_date" value="<?php echo @$data['Specialization']['to_date']?>">
                    </td>
                    <td>
                      <input class="form-control required event-datepicker" placeholder="From Date" readonly name="from_date" value="<?php echo @$data['Specialization']['from_date']?>">
                    </td> -->
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                      <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a> 
                       <a model = "Specialization" data-id="<?php echo $data['Specialization']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Specialization']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Specialization']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a  model = "specialization" data-id="<?php echo base64_encode($data['Specialization']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Specialization']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tbody>
                </table>    
             <?php if(!empty($specials)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
   <script>
    //manage Program 

   let i = 100;
    $(document).on('click','.append-specialization-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i><select class="form-control required" required name="data['+i+'][Specialization][course_id]"> <option value="">Select Program</option> <?php foreach($courses as $course){?><option value="<?php echo $course["Course"]["id"]?>" ><?php echo $course["Course"]["course"]?></option><?php }?>   </select> </td><td> <i class="fa input-error1"></i><input class="form-control required" placeholder="specialization" name="data['+i+'][Specialization][specialization]" > </td><td><i class="fa input-error1"></i> <input class="form-control" placeholder="Description" name="data['+i+'][Specialization][description]" value="<?php echo @$special['Specialization']['decripttion']?>"> </td><td><a href="javascript:void(0);" class="append-specialization-a"><i class="fa fa-plus"></i></a><a href="javascript:void(0);" class="delete-specialization-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#specialization-table').append(div);
       $('.event-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd",
      startDate : new Date()
    });
    });
    
    $(document).on('click','.delete-specialization-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });

    //manage Program 
  </script>
<script type="text/javascript">
  $('select').select2();
  $('.course-list').closest('tr').find("select").select2().enable(false);
</script>
   <script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).closest('tr').find("select").select2().enable(true);
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      $(this).closest('tr').find("select").select2().enable(false);
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editSpecialization',
          data : {data : data},
          success : function(resp){
            alert('Data Save successfully.');
          }
      });
    })
  </script>
  <script type="text/javascript">

    let offset = 4;
    let action  = 'getSpecial';
    let resp = '';

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           console.log(resp);
           resp.forEach(function(element) {
                           if(element.Specialization.status == 1){
                            var is_delete = (element.Specialization.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<select id="course-'+element.Specialization.id+'" class="form-control required" required name="course_id" readonly><option value="">Select Course</option><?php foreach($courses as $course){?><option value="<?php echo $course['Course']['id']?>" ><?php echo $course['Course']['course']?></option><?php }?></select>','<input class="form-control required" placeholder="Specialization" readonly name="specialization" value="'+element.Specialization.specialization+'"><input type="hidden" readonly name="id" value="'+element.Specialization.id+'">','<input class="form-control" placeholder="Description" readonly name="description" value="'+element.Specialization.description+'">','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Specialization" data-id="'+element.Specialization.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a model = "Specialization" data-id="'+btoa(element.Specialization.id)+'"  class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw(); 
                  }
                  else{
                    var is_delete = (element.Specialization.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<select id="course-'+element.Specialization.id+'" class="form-control required" required name="course_id" readonly><option value="">Select Course</option><?php foreach($courses as $course){?><option value="<?php echo $course['Course']['id']?>" ><?php echo $course['Course']['course']?></option><?php }?></select>','<input class="form-control required" placeholder="Specialization" readonly name="specialization" value="'+element.Specialization.specialization+'"><input type="hidden" readonly name="id" value="'+element.Specialization.id+'">','<input class="form-control" placeholder="Description" readonly name="description" value="'+element.Specialization.description+'">','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Specialization" data-id="'+element.Specialization.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a  model = "Specialization" data-id="'+btoa(element.Specialization.id)+'"  class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();
                    //$('#course-'+element.Course.id+' select').attr(element.Course.program_id);
                  }
                  $('#course-'+element.Specialization.id).val(element.Specialization.course_id).change();
              });
           offset += 4;
 
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      //}
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
