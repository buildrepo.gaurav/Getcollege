<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 15%;
    position: absolute;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Important Links
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageLink">Important Links</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Important Links</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="add-course-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Link</th>
                  <th>Description</th>
                  <th>Effective From Date</th>
                  <th>Effective To Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="links-table">
                <tr>
                  <td>
                   <i class="fa input-error1"></i>
                   <input class="form-control required" placeholder="Link Title" name="data[1][Link][title]" value="<?php echo @$link['Link']['title']?>">
                  </td>
                 
                  <td>
                   <i class="fa input-error1"></i>
                  <input class="form-control required" placeholder="Link" name="data[1][Link][link]" value="<?php echo @$link['Link']['link']?>">
               
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <textarea class="form-control" placeholder="Description" name="data[1][Link][description]" ><?php echo @$link['Link']['description']?></textarea>
                
                  </td>
                  <td>
                      <i class="fa input-error1"></i>
                       <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data[1][Link][effective_from_date]" value="<?php echo @$link['Link']['effective_from_date']?>">
                    </td>
                  <td>
                    <i class="fa input-error1"></i>
                       <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data[1][Link][effective_to_date]" value="<?php echo @$link['Link']['effective_to_date']?>">
                    </td>

                  <input type="hidden" name="data[1][Link][id]" value="<?php echo @$link['Link']['id']?>"  >
                  
                  <td>
                    <a href="javascript:void(0);" class="append-links-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Important Links Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Link</th>
                    <th>Effective From Date</th>
                    <th>Effective To Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($links as $data){?>
                  <tr>
                   <td>
                      <input class="form-control required" placeholder="Link Title" name="title" readonly value="<?php echo @$data['Link']['title']?>">
                       <input type="hidden" name="id" readonly value="<?php echo @$data['Link']['id']?>"  >
                    </td>
                    <td>
                      <textarea class="form-control" placeholder="Description" name="description" readonly ><?php echo @$data['Link']['description']?></textarea>
                    </td>
                    <td>
                      <input class="form-control required" placeholder="Link" name="link" readonly value="<?php echo @$data['Link']['link']?>">
                    </td>
                    <td>
                       <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="effective_from_date" value="<?php echo @$data['Link']['effective_from_date']?>" readonly>
                    </td>
                  <td>
                       <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="effective_to_date" value="<?php echo @$data['Link']['effective_to_date']?>" readonly>
                    </td>
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                      <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a>  
                      <a model = "Link" data-id="<?php echo $data['Link']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Link']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Link']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "Link" data-id="<?php echo base64_encode($data['Link']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Link']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
             <?php if(!empty($links)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
   <script>
    //manage Program 

   let i = 100;
    $(document).on('click','.append-links-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i><input class="form-control required" placeholder="Link Title" name="data['+i+'][Link][title]" > </td><td> <i class="fa input-error1"></i><input class="form-control required" placeholder="Link" name="data['+i+'][Link][link]" > </td><td> <i class="fa input-error1"></i><textarea class="form-control" placeholder="Description" name="data['+i+'][Link][description]" ></textarea> </td><td><i class="fa input-error1"></i><input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data['+i+'][Link][effective_from_date]" value=""></td><td><i class="fa input-error1"></i><input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data['+i+'][Link][effective_to_date]" value=""></td><td> <a href="javascript:void(0);" class="append-links-a"><i class="fa fa-plus"></i></a> <a href="javascript:void(0);" class="delete-links-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td>';
      i++;
      $('#links-table').append(div);
      
    });
    
    $(document).on('click','.delete-links-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });

    //manage Program 
  </script>
   <script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editLink',
          data : {data : data},
          success : function(resp){
            alert('Data Save successfully.');
          }
      });
    })
  </script>
<script type="text/javascript">

    let offset = 3;
    let action  = 'getLink';
    let resp = '';

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           console.log(resp);
           resp.forEach(function(element) {
                  if(element.Link.status == 1){
                    var is_delete = (element.Link.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<input class="form-control required" placeholder="Link Title" name="title" readonly value="'+element.Link.title+'"><input type="hidden" name="id" readonly value="'+element.Link.id+'"  >','<textarea class="form-control" placeholder="Description" name="description" readonly >'+element.Link.description+'</textarea>','<input class="form-control required" placeholder="Link" name="link" readonly value="'+element.Link.link+'">','<input readonly type="text" class="form-control required event-datepicker" required placeholder="From Date" name="effective_from_date" value="'+element.Link.effective_from_date+'">','<input readonly type="text" class="form-control required event-datepicker" required placeholder="To Date" name="effective_to_date" value="'+element.Link.effective_to_date+'">','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Link" data-id="'+element.Link.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a model = "Link" data-id="'+btoa(element.Link.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();
                    } 
                  else{
                    var is_delete = (element.Link.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<input class="form-control required" placeholder="Link Title" name="title" readonly value="'+element.Link.title+'"><input type="hidden" name="id" readonly value="'+element.Link.id+'"  >','<textarea class="form-control" placeholder="Description" name="description" readonly >'+element.Link.description+'</textarea>','<input class="form-control required" placeholder="Link" name="link" readonly value="'+element.Link.link+'">','<input readonly type="text" class="form-control required event-datepicker" required placeholder="From Date" name="effective_from_date" value="'+element.Link.effective_from_date+'">','<input readonly type="text" class="form-control required event-datepicker" required placeholder="To Date" name="effective_to_date" value="'+element.Link.effective_to_date+'">','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Link" data-id="'+element.Link.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a model = "Link" data-id="'+btoa(element.Link.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();

                      //$('#course-'+element.Course.id+' select').attr(element.Course.program_id);
                  }
              });
           offset += 3;
            $('.event-datepicker').datepicker({
              autoclose: true,
              format : "yyyy-mm-dd",
              startDate : new Date()
            });
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
      //console.log('hii');
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
