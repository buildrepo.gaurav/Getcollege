<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Course Specialization
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageCollegeCourse/<?php echo @$collegeId?>"><i class="fa fa-dashboard"></i> Manage College Course</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/collegeSpecialization/<?php echo @$collegeId?>/<?php echo $collegeCourseId?>">Manage Course Specialization</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Course Specialization</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="add-college-course" action="<?php echo HTTP_ROOT.'Colleges/saveCollegeSpecialization'?>">
                  
                  <input type="hidden" placeholder="Enter Name" name="data[CourseSpecialization][id]" value="<?php echo @$special['CourseSpecialization']['id']?>">
                  <input type="hidden" placeholder="Enter Name" name="data[CourseSpecialization][college_id]" value="<?php echo @$collegeId?>">
                  <input type="hidden" placeholder="Enter Name" name="data[CourseSpecialization][college_course_id]" value="<?php echo @$collegeCourseId?>">
                  <div class="box-body">
                    <div class="form-group col-md-6">
                      <label for="exampleInputEmail1">Specialization</label>
                      <i class="fa input-error"></i>
                       <select class="form-control required stream-select" name="data[CourseSpecialization][specialization_id]" data-id="1">
                      <option value="">Select Specialization</option>
                      <?php foreach($specializations as $specialization){?>
                        <option value="<?php echo $specialization['Specialization']['id']?>" <?php echo @$specialization['Specialization']['id'] == @$special['CourseSpecialization']['specialization_id'] ? "selected" : ""; ?>><?php echo $specialization['Specialization']['specialization']?></option>
                      <?php }?>
                    </select>
                    </div>
                    
                    
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Fees</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Fees" name="data[CourseSpecialization][fees]" value="<?php echo @$special['CourseSpecialization']['fees']?>">
                    </div>

                    
                  
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Max Salary</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Max Salary" name="data[CourseSpecialization][max_salary]" value="<?php echo @$special['CourseSpecialization']['max_salary']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Min Salary</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Min Salary" name="data[CourseSpecialization][min_salary]" value="<?php echo @$special['CourseSpecialization']['min_salary']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Average Salary</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Average Salary" name="data[CourseSpecialization][avg_salary]" value="<?php echo @$special['CourseSpecialization']['avg_salary']?>">
                    </div>
                    
                    
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Seat</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Trending To Date" name="data[CourseSpecialization][seat]" value="<?php echo @$special['CourseSpecialization']['seat']?>">
                    </div>
                    
                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>


            
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">College Courses</h3>
              </div>
              <table id="college-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Specialization</th>
                  <th>Fee</th>
                  <th>Avg Salary</th>
                  <th>Min Salary</th>
                  <th>Max Salary</th>
                  <th>seat</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($specials as $data){?>
                <tr>
                  
                   <td>
                    <?php echo $data['Specialization']['specialization']?>
                  </td>
                  <td>
                    <?php echo $data['CourseSpecialization']['fees']?>
                  </td>
                  <td>
                    <?php echo $data['CourseSpecialization']['avg_salary']?>
                  </td>
                  <td>
                    <?php echo $data['CourseSpecialization']['min_salary']?>
                  </td>
                  <td>
                    <?php echo $data['CourseSpecialization']['max_salary']?>
                  </td>
                  <td>
                    <?php echo $data['CourseSpecialization']['seat']?>
                  </td>
                  <td>
                    <a target="_blank" class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Colleges/collegeSpecialization/'.$collegeId.'/'.$collegeCourseId.'/'.$data['CourseSpecialization']['id']?>"><i class="fa fa-edit"></i></a> 
                    
                    <a model = "CourseSpecialization" data-id="<?php echo $data['CourseSpecialization']['id']?>" class="fafa-icons change-status" title="<?php echo $data['CourseSpecialization']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['CourseSpecialization']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "CourseSpecialization" data-id="<?php echo base64_encode($data['CourseSpecialization']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['CourseSpecialization']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
             <?php if(!empty($courses)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- </form> -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  
  
  <script type="text/javascript">
  //$('select').select2();

     $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
    $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      }
  });
</script>