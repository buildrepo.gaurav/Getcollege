<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 10%;
    position: absolute;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Testimonials
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageTestimonial">Testimonials</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Testimonials</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="add-course-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Content</th>
                  <th>Contact No</th>
                  <th>Effective From Date</th>
                  <th>Effective To Date</th>
                  <th width="12%">Image</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="testimonial-table">
                <tr>
                  <td>
                   <i class="fa input-error1"></i>
                   <input type="text" class="form-control required" placeholder="Name" name="data[1][Testimonial][name]" value="<?php echo @$testimonial['Testimonial']['name']?>">
                  </td>
                 
                  <td>
                  <i class="fa input-error1"></i>
                  <input type="email" class="form-control required email" placeholder="Email" name="data[1][Testimonial][email]" value="<?php echo @$testimonial['Testimonial']['email']?>">
               
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <textarea class="form-control required " placeholder="Content" name="data[1][Testimonial][content]" ><?php echo @$testimonial['Testimonial']['content']?></textarea>
                
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <input type="text" class="form-control required number" minlength="10" maxlength="10" placeholder="Contact No" name="data[1][Testimonial][contact_no]" value="<?php echo @$testimonial['Testimonial']['contact_no']?>">
               
                  </td>
                  <td>
                      <i class="fa input-error1"></i>
                       <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data[1][Testimonial][from_date]" value="<?php echo @$testimonial['Testimonial']['from_date']?>">
                    </td>
                  <td>
                    <i class="fa input-error1"></i>
                       <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data[1][Testimonial][to_date]" value="<?php echo @$testimonial['Testimonial']['to_date']?>">
                    </td>
                    
                  <td>
                  <i class="fa input-error1"></i>
                  <input class="form-control" type="file" name="image[1]">
                
                  </td>
                  <input type="hidden" name="data[1][Testimonial][id]" value="<?php echo @$testimonial['Testimonial']['id']?>"  >
                  
                  <td>
                    <a href="javascript:void(0);" class="append-testimonial-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Testimonials Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Content</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Effective From Date</th>
                    <th>Effective To Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($testimonials as $data){?>
                  <tr>
                  <td>
                      <img src="<?php echo HTTP_ROOT.'img/userImages/'.$data['Testimonial']['image']?>" width="50px">
                    </td>
                   <td>
                      <?php echo $data['Testimonial']['name']?>
                    </td>
                    <td>
                      <?php echo $data['Testimonial']['content']?>
                    </td>
                    <td>
                      <?php echo $data['Testimonial']['email']?>
                    </td>
                    <td>
                      <?php echo $data['Testimonial']['contact_no']?>
                    </td>
                    <td>
                      <?php echo $data['Testimonial']['from_date']?>
                    </td>
                    <td>
                      <?php echo $data['Testimonial']['to_date']?>
                    </td>
                    <td>
                      <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Colleges/manageTestimonial/'.$data['Testimonial']['id']?>"><i class="fa fa-edit"></i></a> 
                      <a model = "Testimonial" data-id="<?php echo $data['Testimonial']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Testimonial']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Testimonial']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "Testimonial" data-id="<?php echo base64_encode($data['Testimonial']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Testimonial']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
             <?php if(!empty($Testimonials)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
   <script>
    //manage Program 

   let i = 100;
    $(document).on('click','.append-testimonial-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i><input type="text" class="form-control required" placeholder="Name" name="data['+i+'][Testimonial][name]"> </td><td> <i class="fa input-error1"></i><input type="email" class="form-control required email" placeholder="Email" name="data['+i+'][Testimonial][email]"> </td><td> <i class="fa input-error1"></i><textarea class="form-control required " placeholder="Content" name="data['+i+'][Testimonial][content]" ></textarea> </td><td><i class="fa input-error1"></i><input type="text" class="form-control required number" placeholder="Contact No" name="data['+i+'][Testimonial][contact_no]" value=""></td><td><i class="fa input-error1"></i><input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data['+i+'][Testimonial][from_date]" value=""></td><td><i class="fa input-error1"></i><input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data['+i+'][Testimonial][to_date]" value=""></td><td> <i class="fa input-error1"></i><input class="form-control" type="file" name="image['+i+']"> </td><td> <a href="javascript:void(0);" class="append-testimonial-a"><i class="fa fa-plus"></i></a> <a href="javascript:void(0);" class="delete-testimonial-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#testimonial-table').append(div);
      
    });
    
    $(document).on('click','.delete-testimonial-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });

    //manage Program 
  </script>
  <script type="text/javascript">

    let offset = 3;
    let action  = 'getTesti';
    let resp = '';

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           console.log(resp);
           resp.forEach(function(element) {
                  if(element.Testimonial.status == 1){
                    var is_delete = (element.Testimonial.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<img src="'+baseUrl+'img/userImages/'+element.Testimonial.image+'" width="50px">',element.Testimonial.name,element.Testimonial.content,element.Testimonial.email,element.Testimonial.contact_no,element.Testimonial.from_date,element.Testimonial.to_date,'<a class="fafa-icons" title="Edit" href="'+baseUrl+'Colleges/manageTestimonial/'+element.Testimonial.id+'"><i class="fa fa-edit"></i></a><a class="fafa-icons change-status" model = "Testimonial" data-id="'+element.Testimonial.id+'" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a  model = "Testimonial" data-id="'+btoa(element.Testimonial.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a> ']).draw(); 
                  }
                  else{
                    var is_delete = (element.Testimonial.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<img src="'+baseUrl+'img/userImages/'+element.Testimonial.image+'" width="50px">',element.Testimonial.name,element.Testimonial.content,element.Testimonial.email,element.Testimonial.contact_no,element.Testimonial.from_date,element.Testimonial.to_date,'<a class="fafa-icons" title="Edit" href="'+baseUrl+'Colleges/manageTestimonial/'+element.Testimonial.id+'"><i class="fa fa-edit"></i></a><a class="fafa-icons change-status" model = "Testimonial" data-id="'+element.Testimonial.id+'" title="Inactive" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a  model = "Testimonial" data-id="'+btoa(element.Testimonial.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();

                      //$('#course-'+element.Course.id+' select').attr(element.Course.program_id);
                  }
              });
           offset += 3;
 
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
