<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style>
  a.fafa-icons{
      margin-left: 10px;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage College Courses
        <a href="<?php echo HTTP_ROOT?>Colleges/addCollegeCourse/<?php echo $collegeId?>" class="btn btn-primary">Add Course</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageCollegeCourse/<?php echo $collegeId?>">Manage Colleges Courses</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">College Courses</h3>
              </div>
              <table id="college-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Course</th>
                  <th>Fee</th>
                  <th>Cost</th>
                  <th>Effective from</th>
                  <th>Effective to</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($courses as $data){?>
                <tr>
                  
                   <td>
                    <?php echo $data['Course']['course']?>
                  </td>
                  <td>
                    <?php echo $data['CollegeCourse']['fees']?>
                  </td>
                  <td>
                    <?php echo $data['CollegeCourse']['formsadda_cost']?>
                  </td>
                  <td>
                    <?php echo $data['CollegeCourse']['from_date']?>
                  </td>
                  <td>
                    <?php echo $data['CollegeCourse']['to_date']?>
                  </td>
                  <td>
                    <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Colleges/addCollegeCourse/'.$collegeId.'/'.$data['CollegeCourse']['id']?>"><i class="fa fa-edit"></i></a> 
                    <a target="_blank" class="fafa-icons" title="Manage Specialization" href="<?php echo HTTP_ROOT?>Colleges/collegeSpecialization/<?php echo $collegeId.'/'.$data['CollegeCourse']['id']?>"><i class="fa fa-building" ></i></a>
                    <a model = "CollegeCourse" data-id="<?php echo $data['CollegeCourse']['id']?>" class="fafa-icons change-status" title="<?php echo $data['CollegeCourse']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['CollegeCourse']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "CollegeCourse" data-id="<?php echo base64_encode($data['CollegeCourse']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['CollegeCourse']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
             <?php if(!empty($courses)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- </form> -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
  $('select').select2();

     $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
    $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      }
  });
</script>