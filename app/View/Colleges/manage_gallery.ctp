<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 38%;
    position: absolute;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Gallery
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageGallery">Gallery</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Gallery</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form role="form" id="add-facility-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th><!-- 
                  <th>From Date</th>
                  <th>To Date</th> -->
                </tr>
                </thead>
                <tbody id="gallery-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required" required placeholder="Gallery Name" name="data[1][Gallery][name]" value="<?php echo @$gallery['Gallery']['name']?>">
                    <input type="hidden" name="data[1][Gallery][id]" value="<?php echo @$galleryId?>">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control" placeholder="Gallery Description" name="data[1][Gallery][description]" value="<?php echo @$gallery['Gallery']['description']?>">
                  </td>
                  <!-- <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data[1][Gallery][from_date]" value="<?php echo @$gallery['Gallery']['from_date']?>">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data[1][Gallery][to_date]" value="<?php echo @$gallery['Gallery']['to_date']?>">
                  </td> -->
                  <td>
                    <a href="javascript:void(0);" class="append-gallery-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Gallery Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th><!-- 
                    <th>From Date</th>
                    <th>To Date</th> -->
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($galleries as $data){?>
                  <tr>
                    <td>
                      <input type="text" class="form-control required" required placeholder="Gallery Name" readonly name="name" value="<?php echo @$data['Gallery']['name']?>">
                    <input type="hidden" readonly name="id" value="<?php echo @$data['Gallery']['id']?>">
                    </td>
                    <td>
                      <input type="text" class="form-control" placeholder="Gallery Description" readonly name="description" value="<?php echo @$data['Gallery']['description']?>">
                    </td>
                    <!--td>
                      <?php echo $data['Gallery']['from_date']?>
                    </td>
                    <td>
                      <?php echo $data['Gallery']['to_date']?>
                    </td-->
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                      <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a>  
                      <a model = "Gallery" data-id="<?php echo $data['Gallery']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Gallery']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Gallery']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a  model = "Gallery" data-id="<?php echo base64_encode($data['Gallery']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Gallery']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script>
    //manage Program 

   let i = 100;
    $(document).on('click','.append-gallery-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i><input type="text" class="form-control required" required placeholder="Gallery Name" name="data['+i+'][Gallery][name]"> </td><td> <i class="fa input-error1"></i><input type="text" class="form-control" placeholder="Gallery Description" name="data['+i+'][Gallery][description]" > </td><td> <a href="javascript:void(0);" class="append-gallery-a"><i class="fa fa-plus"></i></a> <a href="javascript:void(0);" class="delete-gallery-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td>';
      i++;
      $('#gallery-table').append(div);
       $('.event-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd",
      startDate : new Date()
    });
    });
    
    $(document).on('click','.delete-gallery-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });

    //manage Program 
  </script>
   <script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editGallery',
          data : {data : data},
          success : function(resp){
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          
          }
      });
    })
  </script>
    <script type="text/javascript">

    let offset = 4;
    let action  = 'getGallery';
    let resp = '';

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
          // console.log(resp);
           resp.forEach(function(element) {
                           if(element.Gallery.status == 1){
                      var is_delete = (element.Gallery.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<input type="text" class="form-control required" required placeholder="Gallery Name" readonly name="name" value="'+element.Gallery.name+'"><input type="hidden" readonly name="id" value="'+element.Gallery.id+'">','<input type="text" class="form-control" placeholder="Gallery Description" readonly name="description" value="'+element.Gallery.description+'">','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Gallery" data-id="'+element.Gallery.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a  model = "Gallery" data-id="'+btoa(element.Gallery.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw(); 
                  }
                  else
                  {
                    var is_delete = (element.Gallery.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<input type="text" class="form-control required" required placeholder="Gallery Name" readonly name="name" value="'+element.Gallery.name+'"><input type="hidden" readonly name="id" value="'+element.Gallery.id+'">','<input type="text" class="form-control" placeholder="Gallery Description" readonly name="description" value="'+element.Gallery.description+'">','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Gallery" data-id="'+element.Gallery.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a  model = "Gallery" data-id="'+btoa(element.Gallery.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();

                      //$('#course-'+element.Course.id+' select').attr(element.Course.program_id);
                  }
              });
           offset += 4;
 
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      //}
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
