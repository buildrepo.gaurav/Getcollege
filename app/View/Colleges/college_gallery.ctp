<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage College Gallery
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageCollege">Manage College</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/collegeGallery/<?php echo $collegeId?>">College Gallery</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage College Gallery</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form role="form" id="add-state-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Select Images<span style="color: red;">*</span></th>
                </tr>
                </thead>
                <tbody id="facility-table">
                <tr>
                  <td>
                    <i class="fa input-error"></i>
                    <input type="file" class="form-control required" name="image[]" multiple>
                  </td>  
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All College Gallery Listing</h3>
              </div>
              <form action="<?php echo HTTP_ROOT.'Colleges/saveGallery'?>" method="post"> 
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Gallery Name</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php $i=1; foreach($galleries as $data){?>
                  <tr>
                    <td>
                      <select name="data[<?php echo $i;?>][CollegeGallery][gallery_id]" class="form-control">
                        <option value="">Select Gallery Name</option>  
                        <?php foreach($gallery as $gal){?>
                          <option value="<?php echo $gal['Gallery']['id']?>" <?php echo $gal['Gallery']['id'] == $data['CollegeGallery']['gallery_id'] ? "selected" : '';?>><?php echo $gal['Gallery']['name']?></option>
                        <?php }?>
                      </select>  
                      <input type="hidden" name="data[<?php echo $i;?>][CollegeGallery][id]" value="<?php echo $data['CollegeGallery']['id']?>">
                    </td>
                    <td>
                      <img src="<?php echo HTTP_ROOT.'img/gallery/small/'.$data['CollegeGallery']['image'];?>" class="img-responisve" >
                    </td>
                     
                    
                    <td>
                      <!-- a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Colleges/manageFacility/'.$data['CollegeGallery']['id']?>"><i class="fa fa-edit"></i></a --> 
                      <a class="fafa-icons" title="Delete" href="<?php echo HTTP_ROOT.'Colleges/delete/CollegeGallery/'.base64_encode($data['CollegeGallery']['id'])?>"><i class="fa fa-trash"></i></a> 
                    </td>
                  </tr>
                  <?php $i++;}?>
                  </tbody> 
                  </tfoot>
                </table>
                <div class="box-footer col-md-12">
                  <button class="btn btn-primary" type="submit">Save All</button>
                </div>
               </form>
             <?php if(!empty($facilities)){?>
                <?php echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
    <script type="text/javascript">
    	$("#add-state-form").validate();
    </script>
    <style type="text/css">
    	.error{color: red;}
    </style>