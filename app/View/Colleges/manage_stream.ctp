<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 18%;
    position: absolute;
}

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Stream
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageStream">Stream</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Stream</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form role="form" id="add-course-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Stream</th>
                  <th>Description</th>
                  <!-- <th>From Date</th>
                  <th>To Date</th> -->
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="stream-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required" required placeholder="Enter Stream" name="data[1][Stream][stream]" value="<?php echo @$stream['Stream']['stream']?>">
                    <input type="hidden" name="data[1][Stream][id]" value="<?php echo @$streamId?>">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <textarea class="form-control" placeholder="Stream Description" name="data[1][Stream][description]"><?php echo @$stream['Stream']['description']?></textarea>
                  </td>
                  <!-- <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data[1][Stream][from_date]" value="<?php echo @$stream['Stream']['from_date']?>">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data[1][Stream][to_date]" value="<?php echo @$stream['Stream']['to_date']?>">
                  </td> -->
                  <td>
                    <a href="javascript:void(0);" class="append-stream-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Stream Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Stream</th>
                    <th>Description</th>
                    <!-- <th>To Date</th>
                    <th>From Date</th> -->
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($streams as $data){?>
                  <tr>
                    <td>
                      <input type="text" class="form-control required" required readonly placeholder="Enter Stream" name="stream" value="<?php echo $data['Stream']['stream']?>">
                      <input type="hidden" name="id" value="<?php echo $data['Stream']['id']?>">
                    </td>
                    <td>
                      <textarea class="form-control" readonly placeholder="Stream Description" name="description"><?php echo @$data['Stream']['description']?></textarea>
                    </td>
                    <!-- <td>
                      <input type="text" class="form-control required event-datepicker" readonly required placeholder="To Date" name="to_date" value="<?php echo @$data['Stream']['to_date']?>">
                    </td>
                    <td>
                      <input type="text" class="form-control required event-datepicker" readonly required placeholder="From Date" name="from_date" value="<?php echo @$data['Stream']['from_date']?>">
                    </td> -->
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                      <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a> 
                      <a model = "Stream" data-id="<?php echo $data['Stream']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Stream']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Stream']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "Stream" data-id="<?php echo base64_encode($data['Stream']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Stream']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tbody> 
                  
                </table>
               
             <?php if(!empty($streams)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea").removeAttr('readonly');
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea").serializeArray();
      $(this).closest('tr').find("input,textarea").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editStream',
          data : {data : data},
          success : function(resp){
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          
          }
      });
    })
  </script>
      <script type="text/javascript">

    let offset = 4;
    let action  = 'getStream';
    let resp = '';

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           console.log(resp);
           resp.forEach(function(element) {
                           if(element.Stream.status == 1){
                            if(element.Stream.is_delete == 0){
                              table.row.add(['<input type="text" class="form-control required" required readonly placeholder="Enter Stream" name="stream" value="'+element.Stream.stream+'"><input type="hidden" name="id" value="'+element.Stream.id+'">','<textarea class="form-control required" readonly placeholder="Stream Description" name="description">'+element.Stream.description+'</textarea>','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Stream" data-id="'+element.Stream.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a model = "Stream" data-id="'+btoa(element.Stream.id)+'" class="delete-row fafa-icons" title="Delete" href="javascript:void(0)"><i class="fa fa-trash-o"></i></a>']).draw();
                            }
                            else{
                              table.row.add(['<input type="text" class="form-control required" required readonly placeholder="Enter Stream" name="stream" value="'+element.Stream.stream+'"><input type="hidden" name="id" value="'+element.Stream.id+'">','<textarea class="form-control required" readonly placeholder="Stream Description" name="description">'+element.Stream.description+'</textarea>','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Stream" data-id="'+element.Stream.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a model = "Stream" data-id="'+btoa(element.Stream.id)+'" class="delete-row fafa-icons" title="Delete" href="javascript:void(0)"><i class="fa fa-trash"></i></a>']).draw();
                            }
                     }
                  else{
                    if(element.Stream.is_delete == 0){
                      table.row.add(['<input type="text" class="form-control required" required readonly placeholder="Enter Stream" name="stream" value="'+element.Stream.stream+'"><input type="hidden" name="id" value="'+element.Stream.id+'">','<textarea class="form-control required" readonly placeholder="Stream Description" name="description">'+element.Stream.description+'</textarea>','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Stream" data-id="'+element.Stream.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a  class="fafa-icons delete-row" model = "Stream" data-id="'+btoa(element.Stream.id)+'" title="Delete" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a>']).draw();
                    }
                    else{
                      table.row.add(['<input type="text" class="form-control required" required readonly placeholder="Enter Stream" name="stream" value="'+element.Stream.stream+'"><input type="hidden" name="id" value="'+element.Stream.id+'">','<textarea class="form-control required" readonly placeholder="Stream Description" name="description">'+element.Stream.description+'</textarea>','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Stream" data-id="'+element.Stream.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a  class="fafa-icons delete-row" model = "Stream" data-id="'+btoa(element.Stream.id)+'" title="Delete" href="javascript:void(0);"><i class="fa fa-trash"></i></a>']).draw(); 
                    }
                  }
              });
           offset += 4;
 
        }
  });
$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      //}
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
