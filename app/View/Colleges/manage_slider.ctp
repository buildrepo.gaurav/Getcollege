<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Slider Image
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageSlider">Add Slider</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Slider</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="manage-slider">
                  <input type="hidden" placeholder="Enter Name" name="data[Slider][id]" value="<?php echo @$slider['Slider']['id']?>" >
                  <div class="box-body">
                    
                    
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Slider Name<span style="color: red">*</span></label>
                      <i class="fa input-error"></i>
                      <input class="form-control required" placeholder="Slider Name" name="data[Slider][name]" value="<?php echo @$slider['Slider']['name']?>">
                    </div>

                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Slider Message<span style="color: red">*</span></label>
                      <i class="fa input-error"></i>
                      <input class="form-control required" placeholder="Slider Message" name="data[Slider][message]" value="<?php echo @$slider['Slider']['message']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Slider Link<span style="color: red">*</span></label>
                      <i class="fa input-error"></i>
                      <input type="url" class="form-control required" placeholder="Link" name="data[Slider][link]" value="<?php echo @$slider['Slider']['link']?>">
                    </div>
                    
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Effective From Date<span style="color: red">*</span></label>
                      <i class="fa input-error"></i>
                      <input class="form-control required event-datepicker" placeholder="Effective From Date" name="data[Slider][from_date]" value="<?php echo @$slider['Slider']['from_date']?>">
                    </div>
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Effective To Date<span style="color: red">*</span></label>
                      <i class="fa input-error"></i>
                      <input class="form-control required event-datepicker" placeholder="Effective To Date" name="data[Slider][to_date]" value="<?php echo @$slider['Slider']['to_date']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputFile">Slider Image<span style="color: red">*</span></label>
                      <input type="file" name="image" class="form-control required">                      
                    </div>
                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Sliders List</h3>
              </div>
              <table id="college-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Message</th>
                  <th>Link</th>
                  <th>Effective from</th>
                  <th>Effective to</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($sliders as $data){?>
                <tr>
                  
                   <td>
                    <img src="<?php echo HTTP_ROOT.'img/gallery/'.$data['Slider']['image']?>" width="50px">
                  </td>
                  <td>
                    <?php echo $data['Slider']['name']?>
                  </td>
                  <td>
                    <?php echo $data['Slider']['message']?>
                  </td>
                  <td>
                    <?php echo $data['Slider']['link']?>
                  </td>
                  <td>
                    <?php echo $data['Slider']['from_date']?>
                  </td>
                  <td>
                    <?php echo $data['Slider']['to_date']?>
                  </td>
                  <td>
                    <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Colleges/manageSlider/'.$data['Slider']['id']?>"><i class="fa fa-edit"></i></a> 
                    <a model = "Slider" data-id="<?php echo $data['Slider']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Slider']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Slider']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                    <a model = "Slider" data-id="<?php echo base64_encode($data['Slider']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Slider']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
             <?php if(!empty($sliders)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">

    let offset = 4;
    let action  = 'getSlider';
    let resp = '';

    let table =   $('#college-listing').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages(offset, action);
           console.log(resp);
           resp.forEach(function(element) {
                  if(element.Slider.status == 1){
                    var is_delete = (element.Slider.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<img src="'+baseUrl+'img/gallery/'+element.Slider.image+'" width="50px">',element.Slider.name,element.Slider.message,element.Slider.link,element.Slider.from_date,element.Slider.to_date,'<a class="fafa-icons" title="Edit" href="'+baseUrl+'Colleges/manageSlider/'+baseUrl+'"><i class="fa fa-edit"></i></a><a model = "Slider" data-id="'+element.Slider.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a model = "Slider" data-id="'+btoa(element.Slider.id)+'" class="fafa-icons delete-row"  title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw(); 
                  }
                  else
                  {
                    var is_delete = (element.Slider.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<img src="'+baseUrl+'img/gallery/'+element.Slider.image+'" width="50px">',element.Slider.name,element.Slider.message,element.Slider.link,element.Slider.from_date,element.Slider.to_date,'<a class="fafa-icons" title="Edit" href="'+baseUrl+'Colleges/manageSlider/'+baseUrl+'"><i class="fa fa-edit"></i></a><a model = "Slider" data-id="'+element.Slider.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a model = "Slider" data-id="'+btoa(element.Slider.id)+'" class="fafa-icons delete-row"  title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();

                      //$('#course-'+element.Course.id+' select').attr(element.Course.program_id);
                  }
              });
           offset += 3;
 
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
