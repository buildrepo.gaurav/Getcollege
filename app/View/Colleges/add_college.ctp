<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Add College
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageCollege">Manage College</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/addCollege">Add College</a></li>
      </ol>
    </section>

      <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Add Details</a></li>
      <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"> Near By</a></li>
      <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">College About</a></li>
      <li role="presentation"><a href="#facility" aria-controls="facility" role="tab" data-toggle="tab">College Facilities</a></li>
      <li role="presentation"><a href="#fields" aria-controls="fields" role="tab" data-toggle="tab">Additional Fields</a></li>
    </ul>
    <div class="tab-content">
      <!-- Main content -->
      <div role="tabpanel" class="tab-pane active" id="home">
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add College Details</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" id="add-college-form" method='post' enctype="multipart/form-data" action="javascript:void(0);">
                  <input type="hidden" placeholder="Enter Name" name="data[College][id]" class="college-id">
                  <div class="box-body">
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Parent College</label>
                      <i class="fa input-error"></i>
                      <select class="form-control" name="data[College][collegemaster_id]">
                        <option value="">Select Parent College</option>
                        <?php foreach($collegemasters as $coun){?>
                        <option value="<?php echo $coun['CollegeMaster']['id']?>"><?php echo $coun['CollegeMaster']['college']?></option>
                        <?php }?>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputEmail1">Name<span style="color:red;">*</span></label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control required" placeholder="Enter Name" name="data[College][name]" value="<?php echo @$college['College']['name']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Location<span style="color:red;">*</span></label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control required" placeholder="Location" name="data[College][location]" value="<?php echo @$college['College']['location']?>">
                    </div>
                      <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Short Name</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control " placeholder="Short Name"  name="data[College][short_name]" value="<?php echo @$college['College']['short_name']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Email<span style="color:red;">*</span></label>
                      <i class="fa input-error"></i>
                      <input type="email" class="form-control required email" placeholder="Email" value="<?php echo @$college['College']['email']?>" name="data[College][email]">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="">Address<span style="color:red;">*</span></label>
                      <i class="fa input-error"></i>
                      <textarea class="form-control required " placeholder="Address"  name="data[College][address]"><?php echo @$college['College']['address']?></textarea>
                    </div>
                    <!-- <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">New Password</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control required " placeholder="New Password" id="college-new-pass" name="data[College][password]">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Confirm Password</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control required" id="college-confirm-pass" placeholder="Confirm Password">
                    </div> -->

                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Country<span style="color:red;">*</span></label>
                      <i class="fa input-error"></i>
                      <select id="country" class="form-control required" name="data[College][country_id]">
                        <option value="">Select Country</option>
                        <?php foreach($country as $coun){?>
                        <option value="<?php echo $coun['Country']['id']?>" <?php echo @$college['College']['country_id'] == $coun['Country']['id'] ? 'selected' : '';?>><?php echo $coun['Country']['country_name']?></option>
                        <?php }?>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">State<span style="color:red;">*</span></label>
                      <i class="fa input-error"></i>
                      <select id="state" class="form-control required" name="data[College][state_id]">
                        <option value="">Select State</option>
                        <option value="<?php echo @$college['State']['id']?>" selected><?php echo @$college['State']['statename']?></option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">City<span style="color:red;">*</span></label>
                      <i class="fa input-error"></i>
                      <select id="city" class="form-control required" name="data[College][city_id]">
                        <option value="">Select City</option>
                        <option value="<?php echo @$college['City']['id']?>" selected><?php echo @$college['City']['city_name']?></option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Pincode</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control number" placeholder="Pincode" minlength="6" maxlength="6" name="data[College][pincode]" value="<?php echo @$college['College']['pincode']?>">
                    </div>
                      <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Mobile No.</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control number" placeholder="Mobile" minlength="10" maxlength="10" name="data[College][phone1]" value="<?php echo @$college['College']['phone1']?>">
                    </div>
                      <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Alternate No.</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control number" placeholder="Alternate No." minlength="10" maxlength="10" name="data[College][phone2]" value="<?php echo @$college['College']['phone2']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Admission Enquiry No.</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control number" placeholder="Admission Enquiry No." name="data[College][enquiry_no]" value="<?php echo @$college['College']['enquiry_no']?>"></textarea>
                    </div>
                      <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Website<span style="color:red;">*</span></label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control required " placeholder="Website"  name="data[College][website]" value="<?php echo @$college['College']['website']?>">
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label for="exampleInputFile">Image</label>
                      <i class="fa input-error"></i>
                      <input type="file" name="image">

                      
                    </div>
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Continue</button>
                  </div>
                </form>
              </div>
              <!-- /.box -->

              

            </div>
            <!--/.col (left) -->
            <!-- right column -->
            
            <!--/.col (right) -->
          </div>
          <!-- /.row -->
        </section>
      </div> 
      <div role="tabpanel" class="tab-pane" id="profile">
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Near by locations</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" id="near-college-form" method='post' enctype="multipart/form-data">
                  <div class="box-body">
                  <input type="hidden" name="data[CollegeNear][id]" value="<?php echo @$CollegeNear['CollegeNear']['id']?>">
                  <input type="hidden" name="data[CollegeNear][college_id]" class="college-id" value="<?php echo @$collegeId?>">
                  <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Nearest Hospital</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Nearest Hospital" name="data[CollegeNear][near_hospital]" value="<?php echo @$CollegeNear['CollegeNear']['near_hospital']?>">
                    </div>
                     <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Nearest Bank ATM</label>
                      <i class="fa input-error"></i>
                      <textarea class="form-control" placeholder="Nearest Bank ATM" name="data[CollegeNear][near_atm]"><?php echo @$CollegeNear['CollegeNear']['near_atm']?></textarea>
                    </div>
                     <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Nearest Bank</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Nearest Bank" name="data[CollegeNear][near_bank]" value="<?php echo @$CollegeNear['CollegeNear']['near_bank']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Nearest Bus Stand</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Nearest Bus Stand" name="data[CollegeNear][near_bus]" value="<?php echo @$CollegeNear['CollegeNear']['near_bus']?>">
                    </div>
                     <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Nearest Railway Station</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Nearest Railway Station" name="data[CollegeNear][near_railway]" value="<?php echo @$CollegeNear['CollegeNear']['near_railway']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Nearest Police Station</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Nearest Police Station" name="data[CollegeNear][near_police]" value="<?php echo @$CollegeNear['CollegeNear']['near_police']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Nearest Shopping</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Nearest Shopping" name="data[CollegeNear][near_shopping]" value="<?php echo @$CollegeNear['CollegeNear']['near_shopping']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Nearest Restaurant</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Nearest Restaurant" name="data[CollegeNear][near_restaurant]" value="<?php echo @$CollegeNear['CollegeNear']['near_restaurant']?>">
                    </div>
                    <div class="form-group col-md-6">
                      
                      <label for="exampleInputEmail1">Nearest Metro Station</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Nearest Metro Station" name="data[CollegeNear][near_metro]" value="<?php echo @$CollegeNear['CollegeNear']['near_metro']?>">
                    </div>
                
                  <!-- /.box-body -->

                  <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Continue</button>
                  </div>
                </form>
              </div>
              <!-- /.box -->

              

            </div>
            <!--/.col (left) -->
            <!-- right column -->
            
            <!--/.col (right) -->
          </div>
        </section>
      <!-- /.row -->
      </div>
      <div role="tabpanel" class="tab-pane" id="settings">
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add About College Details</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" id="about-college-form" method='post' enctype="multipart/form-data">
                  <div class="box-body">
                  <input type="hidden" name="data[CollegeMeta][id]" value="<?php echo @$collegeMeta['CollegeMeta']['id']?>">
                  <input type="hidden" name="data[CollegeMeta][college_id]" class="college-id" value="<?php echo @$collegeId?>">
                 <!--  <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">College Short Name</label>
                      <input type="text" class="form-control" placeholder="College Short Name" name="data[CollegeMeta][short_name]" value="<?php echo @$collegeMeta['CollegeMeta']['short_name']?>">
                    </div> -->
                     <!-- <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">College Description</label>
                      <textarea class="form-control" placeholder="College Description" name="data[CollegeMeta][description]"><?php echo @$collegeMeta['CollegeMeta']['description']?></textarea>
                    </div> -->
                    
                     <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Establishment</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control number" placeholder="Establishment" name="data[CollegeMeta][establishment]" value="<?php echo @$collegeMeta['CollegeMeta']['establishment']?>" >
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Contact Person Name</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Contact Person Name" name="data[CollegeMeta][contact_person]" value="<?php echo @$collegeMeta['CollegeMeta']['contact_person']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Toll Free Number</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control number" placeholder="Toll Free Number" name="data[CollegeMeta][toll_free]" value="<?php echo @$collegeMeta['CollegeMeta']['toll_free']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Fax Number</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Fax Number" name="data[CollegeMeta][fax]" value="<?php echo @$collegeMeta['CollegeMeta']['fax']?>">
                    </div>
                    <div class="form-group col-md-6">
                      
                      <label for="exampleInputEmail1">Land Area</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Land Area" name="data[CollegeMeta][land_area]" value="<?php echo @$collegeMeta['CollegeMeta']['land_area']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Trust</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control" placeholder="Trust" name="data[CollegeMeta][trust_name]" value="<?php echo @$collegeMeta['CollegeMeta']['trust_name']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Afilited By</label>
                      <i class="fa input-error"></i>
                      <input class="form-control  " placeholder="Afilited By"  name="data[CollegeMeta][affiliated_by]" value="<?php echo @$collegeMeta['CollegeMeta']['affiliated_by']?>">
                    </div>

                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Type (private / Government)</label>
                      <i class="fa input-error"></i>
                      <select class="form-control" name="data[CollegeMeta][college_type]">
                         <option value="">Select College Type</option>
                         <option value="0" <?php echo @$collegeMeta['CollegeMeta']['college_type'] == '0' ? 'selected' : '' ;?>>Government </option>
                         <option value="1" <?php echo @$collegeMeta['CollegeMeta']['college_type'] == '1' ? 'selected' : '' ;?>>Private</option>
                      </select> 
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">College Map Latitude</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control number"  placeholder="CollegeMeta Map Lattitude" value="<?php echo @$collegeMeta['CollegeMeta']['latitude']?>" name="data[CollegeMeta][latitude]">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">College Map Longitude</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control number" placeholder="College Map Langitude"  name="data[CollegeMeta][longitude]" value="<?php echo @$collegeMeta['CollegeMeta']['longitude']?>">
                    </div>
                      <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">About College</label>
                      <i class="fa input-error"></i>
                      <textarea id="editor1" class="form-control" name="data[CollegeMeta][about]"><?php echo @$collegeMeta['CollegeMeta']['about']?></textarea>
                   
                  </div>
                   <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Placement</label>
                      <i class="fa input-error"></i>
                      <textarea id="editor2" class="form-control" name="data[CollegeMeta][placement]"><?php echo @$collegeMeta['CollegeMeta']['placement']?></textarea>
                   
                  </div>
                  <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Infrastructure</label>
                      <i class="fa input-error"></i>
                      <textarea id="editor3" class="form-control" name="data[CollegeMeta][infrastructure]"><?php echo @$collegeMeta['CollegeMeta']['infrastructure']?></textarea>
                   
                  </div>
                  <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Campus</label>
                      <i class="fa input-error"></i>
                      <textarea id="editor4" class="form-control" name="data[CollegeMeta][campus]"><?php echo @$collegeMeta['CollegeMeta']['campus']?></textarea>
                   
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Continue</button>
                  </div>
                </form>
              </div>
              <!-- /.box -->

              

            </div>
            <!--/.col (left) -->
            <!-- right column -->
            
            <!--/.col (right) -->
          </div>
        <!-- /.row -->
        </section>
      </div>
      <div role="tabpanel" class="tab-pane" id="facility">
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">College Facilities</h3>
                </div>
                <form role="form" id="facility-college-form" method='post' enctype="multipart/form-data">
                   <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Select Facilities</label>
                        <input type="hidden" name="data[CollegeFacility][college_id]" value="<?php echo @$collegeId;?>" class="college-id">
                        <i class="fa input-error"></i>
                        <select class="form-control required" id="facility-select" name="data[CollegeFacility][facilities][]" multiple >
                          <option disabled value="">Select Facilties<span style="color:red;">*</span></option>
                          <?php foreach($facilities as $facility){?>
                            <option value="<?php echo $facility['Facility']['id']?>"
                              
                            ><?php echo $facility['Facility']['name']?></option>
                          <?php }?>
                        </select>
                    </div>
                  </div>  
                  <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Continue</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>        
      </div>


      <div role="tabpanel" class="tab-pane" id="fields">
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">College Fields</h3>
                </div>
                <form role="form" id="field-college-form" method='post' enctype="multipart/form-data" >
                  <input type="hidden" name="data[CollegeFieldMeta][college_id]" value="<?php echo @$collegeId;?>" class="college-id">
                   <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Select Fields</label><br>
                        <?php foreach($fields as $field){?>
                        <input type="checkbox" name="data[CollegeFieldMeta][field_id][]" class="field_name" value="<?php echo $field['CollegeField']['id']?>"> <?php echo $field['CollegeField']['field_name']?>
                        <?php }?>
                    </div>

                  </div>  
                  <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>        
      </div>
      <!-- /.content -->
    </div>
  </div>
  <script>
    $(document).ready(function() {
        $('#facility-select').multiselect();
    });
  </script>
  <script type="text/javascript">
  $('#state').select2();
  $('#city').select2();
</script>