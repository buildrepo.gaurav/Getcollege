<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 8%;
    position: absolute;
}
.form-control{
  width:100% !important;
}
.input-sm{
  float: right;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Course
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageCourse">Course</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Course</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="add-course-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Program</th>
                  <th>Course</th>
                  <th>Description</th>
                  <th>Alias</th>
                  <th>Duration Year</th>
                  <th>Duration Month</th>
                  <th>Duration Day</th>
                  <!-- <th>From Date</th>
                  <th>To Date</th> -->
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="course-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                   <select class="form-control required" required name="data[1][Course][program_id]">
                    <option value="">Select Program</option>
                  <?php foreach($programs as $program){?>
                    <option value="<?php echo $program['Program']['id']?>" <?php echo @$program['Program']['id'] == @$course['Course']['program_id'] ? 'selected' : '';?> ><?php echo $program['Program']['program']?></option>
                  <?php }?>  
                  </select>
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <input class="form-control required" placeholder="Course" name="data[1][Course][course]" value="<?php echo @$course['Course']['course']?>">
                  
                  <input type="hidden" name="data[1][Course][id]" value="<?php echo @$course['Course']['id']?>">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <textarea class="form-control" placeholder="Description" name="data[1][Course][description]"><?php echo @$course['Course']['description']?></textarea>
                  </td>
                  <td>
                    <i class="fa input-error1"></i>
                    <input type="text" class="form-control" placeholder="Alias" name="data[1][Course][alias]" value="<?php echo @$course['Course']['alias']?>">
                  </td>
                  <td>
                    <i class="fa input-error1"></i>
                    <select class="form-control required" name="data[1][Course][duration]">
                      <option value="">Select Year</option>
                    <?php for($i=0; $i<=8; $i++){?>
                      <option value="<?php echo $i?>" <?php echo @$course['Course']['duration'] == $i ? 'selected' : '';?> ><?php echo $i;?></option>
                    <?php }?>  
                    </select>
                  </td>
                  <td>
                    <i class="fa input-error1"></i>
                    <select class="form-control required" name="data[1][Course][duration_month]">
                      <option value="">Select Month</option>
                    <?php for($i=0; $i<=12; $i++){?>
                      <option value="<?php echo $i?>" <?php echo @$course['Course']['duration_month'] == $i ? 'selected' : '';?> ><?php echo $i;?></option>
                    <?php }?>  
                    </select>
                  </td>
                  <td>
                    <i class="fa input-error1"></i>
                    <select class="form-control required" name="data[1][Course][duration_days]">
                      <option value="">Select Days</option>
                    <?php for($i=0; $i<=31; $i++){?>
                      <option value="<?php echo $i?>" <?php echo @$course['Course']['duration_days'] == $i ? 'selected' : '';?> ><?php echo $i;?></option>
                    <?php }?>  
                    </select>
                  </td>
                  <!-- <td>
                    <i class="fa input-error1"></i>
                    <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data[1][Course][from_date]" value="<?php echo @$course['Course']['from_date']?>">
                  </td>
                  <td>
                    <i class="fa input-error1"></i>
                    <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data[1][Course][to_date]" value="<?php echo @$course['Course']['to_date']?>">
                  </td> -->
                  <td>
                    <a href="javascript:void(0);" class="append-course-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Course Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Program</th>
                    <th>Course</th>
                    <th>Description</th>
                    <th>Alias</th>
                    <th>Duration Year</th>
                    <th>Duration Month</th>
                    <th>Duration Day</th>
                    <!-- <th>To Date</th>
                    <th>From Date</th> -->
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($courses as $data){?>
                  <tr class="course-list">
                   <td>
                      <select class="form-control required" required name="program_id" readonly>
                    <option value="">Select Program</option>
                  <?php foreach($programs as $program){?>
                    <option value="<?php echo $program['Program']['id']?>" <?php echo @$program['Program']['id'] == @$data['Course']['program_id'] ? 'selected' : '';?> ><?php echo $program['Program']['program']?></option>
                  <?php }?>  
                  </select>
                    </td>
                    <td>
                      <input class="form-control required" placeholder="Course" name="course" readonly value="<?php echo @$data['Course']['course']?>">
                  
                  <input type="hidden" name="id" readonly value="<?php echo @$data['Course']['id']?>">
                    </td>
                    <td>
                      <textarea class="form-control" placeholder="Description" name="description" readonly><?php echo @$data['Course']['description']?></textarea>
                    </td>
                    <td>
                      <input type="text" class="form-control" placeholder="Alias" name="alias" readonly value="<?php echo @$data['Course']['alias']?>">
                    </td>
                    <td>
                      <select class="form-control required" name="duration" readonly>
                        <option value="">Select Year</option>
                      <?php for($i=0; $i<=8; $i++){?>
                        <option value="<?php echo $i?>" <?php echo @$data['Course']['duration'] == $i ? 'selected' : '';?> ><?php echo $i;?></option>
                      <?php }?>  
                      </select>
                    </td>
                    <td>
                      <select class="form-control required" name="duration_month" readonly>
                        <option value="">Select Month</option>
                      <?php for($i=0; $i<=12; $i++){?>
                        <option value="<?php echo $i?>" <?php echo @$data['Course']['duration_month'] == $i ? 'selected' : '';?> ><?php echo $i;?></option>
                      <?php }?>  
                      </select>
                    </td>
                    <td>
                      <select class="form-control required" name="duration_days" readonly>
                        <option value="">Select Days</option>
                      <?php for($i=0; $i<=31; $i++){?>
                        <option value="<?php echo $i?>" <?php echo @$data['Course']['duration_days'] == $i ? 'selected' : '';?> ><?php echo $i;?></option>
                      <?php }?>  
                      </select>
                    </td>
                     <!-- <td>
                      <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="to_date" readonly value="<?php echo @$data['Course']['to_date']?>">
                    </td>
                    <td>
                      <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="from_date" readonly value="<?php echo @$data['Course']['from_date']?>">
                    </td> -->
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                      <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a> 
                       <a model = "Course" data-id="<?php echo $data['Course']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Course']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0);"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Course']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "Course" data-id="<?php echo base64_encode($data['Course']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0);"><i class="fa <?php echo @$data['Course']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
             <?php if(!empty($programs)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
   <script>
    //manage Program 

   let i = 100;
    $(document).on('click','.append-course-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i><select class="form-control required" required name="data['+i+'][Course][program_id]"> <option value="">Select Program</option> <?php foreach($programs as $program){?> <option value="<?php echo $program['Program']['id']?>" ><?php echo $program['Program']['program']?></option> <?php }?> </select> </td><td> <i class="fa input-error1"></i><input class="form-control required" placeholder="Course" name="data['+i+'][Course][course]" > </td><td> <textarea class="form-control" placeholder="Description" name="data['+i+'][Course][description]"><?php echo @$course['Course']['description']?></textarea><td> <i class="fa input-error1"></i><input type="text" class="form-control" placeholder="Alias" name="data['+i+'][Course][alias]" value="<?php echo @$course['Course']['alias']?>"> </td> </td><td><i class="fa input-error1"></i><select class="form-control required" name="data['+i+'][Course][duration]"><option value="">Select Year</option><?php for($i=0; $i<=8; $i++){?><option value="<?php echo $i?>" <?php echo @$course['Course']['duration'] == $i ? 'selected' : '';?> ><?php echo $i;?></option><?php }?></select></td><td><i class="fa input-error1"></i><select class="form-control required" name="data['+i+'][Course][duration_month]"><option value="">Select Month</option><?php for($i=0; $i<=12; $i++){?><option value="<?php echo $i?>" <?php echo @$course['Course']['duration_month'] == $i ? 'selected' : '';?> ><?php echo $i;?></option><?php }?></select></td><td><i class="fa input-error1"></i><select class="form-control required" name="data['+i+'][Course][duration_days]"><option value="">Select Day</option><?php for($i=0; $i<=31; $i++){?><option value="<?php echo $i?>" <?php echo @$course['Course']['duration_days'] == $i ? 'selected' : '';?> ><?php echo $i;?></option><?php }?></select></td><td> <a href="javascript:void(0);" class="append-course-a"><i class="fa fa-plus"></i></a><a href="javascript:void(0);" class="delete-course-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a></td></tr>';
      i++;
      $('#course-table').append(div);
       $('.event-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd",
      startDate : new Date()
    });
    });
    
    $(document).on('click','.delete-course-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });

    //manage Program 
  </script>
    <script type="text/javascript">
  $('select').select2();
  $('.course-list').closest('tr').find("select").select2().enable(false);
</script>
<script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).closest('tr').find("select").select2().enable(true);
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      $(this).closest('tr').find("select").select2().enable(false);
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editCourse',
          data : {data : data},
          success : function(resp){
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          
          }
      });
    })
  </script>
  <script type="text/javascript">

    let offset = 4;
    let action  = 'getCourse';
    let resp = '';

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           console.log(resp);
           resp.forEach(function(element) {
                           if(element.Course.status == 1){
                          var is_delete = (element.Course.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<select id="course-'+element.Course.id+'" class="form-control required" required name="program_id" readonly><option value="">Select Program</option><?php foreach($programs as $program){?><option value="<?php echo $program['Program']['id']?>"><?php echo $program['Program']['program']?></option><?php }?></select>','<input class="form-control required" placeholder="Course" name="course" readonly value="'+element.Course.course+'"><input type="hidden" name="id" readonly value="'+element.Course.id+'">','<textarea class="form-control" placeholder="Description" name="description" readonly>'+element.Course.description+'</textarea>','<input type="text" class="form-control" placeholder="Alias" name="alias" readonly value="'+element.Course.alias+'">','<select readonly id="year-'+element.Course.id+'" class="form-control required" name="duration"><option value="">Select Year</option><?php for($i=0; $i<=8; $i++){?><option value="<?php echo $i?>"><?php echo $i;?></option><?php }?></select>','<select readonly id="month-'+element.Course.id+'" class="form-control required" name="duration_month"><option value="">Select Month</option><?php for($i=0; $i<=12; $i++){?><option value="<?php echo $i?>"><?php echo $i;?></option><?php }?></select>','<select readonly id="day-'+element.Course.id+'" class="form-control required" name="duration_days"><option value="">Select Day</option><?php for($i=0; $i<=31; $i++){?><option value="<?php echo $i?>"><?php echo $i;?></option><?php }?></select>','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Course" data-id="'+element.Course.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a model = "Course" data-id="'+btoa(element.Course.id)+'" class="fafa-icons delete-row"  href="javascript:void(0);" title="Delete"><i class="fa '+is_delete+'"></i></a>']).draw(); 
                  }
                  else{
                    var is_delete = (element.Course.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<select id="course-'+element.Course.id+'" class="form-control required" required name="program_id" readonly><option value="">Select Program</option><?php foreach($programs as $program){?><option value="<?php echo $program['Program']['id']?>"><?php echo $program['Program']['program']?></option><?php }?></select>','<input class="form-control required" placeholder="Course" name="course" readonly value="'+element.Course.course+'"><input type="hidden" name="id" readonly value="'+element.Course.id+'">','<textarea class="form-control" placeholder="Description" name="description" readonly>'+element.Course.description+'</textarea>','<input type="text" class="form-control" placeholder="Alias" name="alias" readonly value="'+element.Course.alias+'">','<select readonly id="year-'+element.Course.id+'" class="form-control required" name="duration"><option value="">Select Year</option><?php for($i=0; $i<=8; $i++){?><option value="<?php echo $i?>"><?php echo $i;?></option><?php }?></select>','<select readonly id="month-'+element.Course.id+'" class="form-control required" name="duration_month"><option value="">Select Month</option><?php for($i=0; $i<=12; $i++){?><option value="<?php echo $i?>"><?php echo $i;?></option><?php }?></select>','<select readonly id="day-'+element.Course.id+'" class="form-control required" name="duration_days"><option value="">Select Day</option><?php for($i=0; $i<=31; $i++){?><option value="<?php echo $i?>"><?php echo $i;?></option><?php }?></select>','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "Course" data-id="'+element.Course.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a model = "Course" data-id="'+btoa(element.Course.id)+'" class="fafa-icons delete-row"  href="javascript:void(0);" title="Delete"><i class="fa '+is_delete+'"></i></a>']).draw();
                  }

                      $('#course-'+element.Course.id).val(element.Course.program_id).change();
                      $('#year-'+element.Course.id).val(element.Course.duration).change();
                      $('#month-'+element.Course.id).val(element.Course.duration_month).change();
                      $('#day-'+element.Course.id).val(element.Course.duration_days).change();
              });
           offset += 4;
 
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      //}
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
