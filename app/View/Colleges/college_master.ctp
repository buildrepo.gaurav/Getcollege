<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 71%;
    position: absolute;
}
.hidecol{
  display:none;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        College Master
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/collegeMaster">College Master</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">College Master</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form role="form" id="add-course-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>College</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="collegeMaster-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required" required placeholder="Enter College Name" name="data[1][CollegeMaster][college]" value="<?php echo @$collegeMaster['CollegeMaster']['college']?>">
                    <input type="hidden" name="data[1][CollegeMaster][id]" value="<?php echo @$collegeMasterId?>">
                  </td>
                  
                  <td>
                    <a href="javascript:void(0);" class="append-collegeMaster-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">College Master Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>College Name</th>
                    
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($collegeMasters as $data){?>
                  
                  <tr>
                  <form method="post" action="javascript:void(0);">
                    <td >
                      <input type="text" class="form-control required" required placeholder="Enter College Name" readonly name="college" value="<?php echo @$data['CollegeMaster']['college']?>" style="width:100%">
                    <input type="hidden" readonly name="id" value="<?php echo @$data['CollegeMaster']['id']?>">
                    </td>
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                      <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a>  
                      <a model = "CollegeMaster" data-id="<?php echo $data['CollegeMaster']['id']?>" class="fafa-icons change-status" title="<?php echo $data['CollegeMaster']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['CollegeMaster']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "CollegeMaster" data-id="<?php echo base64_encode($data['CollegeMaster']['id'])?>"  class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['CollegeMaster']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                    </form>
                  </tr>
                  
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
             <?php if(!empty($streams)){?>
                <?php echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script>
    //manage Program 

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

   let i = 100;
    $(document).on('click','.append-collegeMaster-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i> <input type="text" class="form-control required" required placeholder="Enter College Name" name="data['+i+'][CollegeMaster][college]" > </td><td> <a href="javascript:void(0);" class="append-collegeMaster-a"><i class="fa fa-plus"></i></a><a href="javascript:void(0);" class="delete-collegeMaster-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#collegeMaster-table').append(div);
      
    });
    
    $(document).on('click','.delete-collegeMaster-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });

    //manage Program 

  </script>
     <script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editCollegeMaster',
          data : {data : data},
          success : function(resp){
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          
          }
      });
    })
  
$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title', 'Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title', 'Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
