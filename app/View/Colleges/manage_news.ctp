<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 15%;
    position: absolute;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage News
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageNews">News</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage News</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="add-course-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Link</th>
                  <th>Description</th>
                  <th>Effective From Date</th>
                  <th>Effective To Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="news-table">
                <tr>
                  <td>
                   <i class="fa input-error1"></i>
                   <input class="form-control required" placeholder="News Title" name="data[1][New][title]" value="<?php echo @$new['New']['title']?>">
                  </td>
                 
                  <td>
                   <i class="fa input-error1"></i>
                  <input class="form-control required" placeholder="News link" name="data[1][New][link]" value="<?php echo @$new['New']['link']?>">
               
                  </td>
                  <td>
                   
                  <textarea class="form-control" placeholder="Description" name="data[1][New][description]" ><?php echo @$new['New']['description']?></textarea>
                
                  </td>
                  <td>
                      <i class="fa input-error1"></i>
                       <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data[1][New][from_date]" value="<?php echo @$data['New']['from_date']?>">
                    </td>
                  <td>
                    <i class="fa input-error1"></i>
                       <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data[1][New][to_date]" value="<?php echo @$data['New']['to_date']?>">
                    </td>
                  <input type="hidden" name="data[1][New][id]" value="<?php echo @$new['New']['id']?>"  >
                  
                  <td>
                    <a href="javascript:void(0);" class="append-news-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All News Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Link</th>
                    <th>Effective From Date</th>
                    <th>Effective To Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($news as $data){?>
                  <tr>
                   <td>
                      <input class="form-control required" placeholder="News Title" readonly name="title" value="<?php echo @$data['New']['title']?>">
                      <input type="hidden" readonly name="id" value="<?php echo @$data['New']['id']?>"  >
                    </td>
                    <td>
                      <textarea class="form-control required " placeholder="Description" readonly name="description" ><?php echo @$data['New']['description']?></textarea>
                    </td>
                    <td>
                      <input class="form-control required" placeholder="News link" readonly name="link" value="<?php echo @$data['New']['link']?>">
                    </td>
                    <td>
                       <input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="from_date" value="<?php echo @$data['New']['from_date']?>" readonly>
                    </td>
                    <td>
                       <input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="to_date" value="<?php echo @$data['New']['to_date']?>" readonly>
                    </td>
                    <td>
                       <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                      <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a>  
                      <a model = "New" data-id="<?php echo $data['New']['id']?>" class="fafa-icons change-status" title="<?php echo $data['New']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['New']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "New" data-id="<?php echo base64_encode($data['New']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['New']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
             <?php if(!empty($news)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
   <script>
    //manage Program 

   let i = 100;
    $(document).on('click','.append-news-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i> <input class="form-control required" placeholder="News Title" name="data['+i+'][New][title]" > </td><td> <i class="fa input-error1"></i><input class="form-control required" placeholder="News link" name="data['+i+'][New][link]" > </td><td> <textarea class="form-control" placeholder="Description" name="data['+i+'][New][description]" ></textarea> </td><td><i class="fa input-error1"></i><input type="text" class="form-control required event-datepicker" required placeholder="From Date" name="data['+i+'][New][from_date]" value=""></td><td><i class="fa input-error1"></i><input type="text" class="form-control required event-datepicker" required placeholder="To Date" name="data['+i+'][New][to_date]" value=""></td><td> <a href="javascript:void(0);" class="append-news-a"><i class="fa fa-plus"></i></a> <a href="javascript:void(0);" class="delete-news-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#news-table').append(div);
      
    });
    
    $(document).on('click','.delete-news-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });

    //manage Program 
  </script>
   <script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editNew',
          data : {data : data},
          success : function(resp){
            alert('Data Save successfully.');
          }
      });
    })
  </script>
   <script type="text/javascript">

    let offset = 2;
    let action  = 'getNews';
    let resp = '';

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           console.log(resp);
           resp.forEach(function(element) {
                  if(element.New.status == 1){
                    var is_delete = (element.New.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<input class="form-control required" placeholder="News Title" readonly name="title" value="'+element.New.title+'"><input type="hidden" readonly name="id" value="'+element.New.id+'"  >','<textarea class="form-control" placeholder="Description" readonly name="description" >'+element.New.description+'</textarea>','<input class="form-control required" placeholder="News link" readonly name="link" value="'+element.New.link+'">','<input readonly type="text" class="form-control required event-datepicker" required placeholder="From Date" name="from_date" value="'+element.New.from_date+'">','<input readonly type="text" class="form-control required event-datepicker" required placeholder="To Date" name="to_date" value="'+element.New.to_date+'">','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "New" data-id="'+element.New.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a model = "New" data-id="'+btoa(element.New.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();
                  }
                  else{
                    var is_delete = (element.New.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<input class="form-control required" placeholder="News Title" readonly name="title" value="'+element.New.title+'"><input type="hidden" readonly name="id" value="'+element.New.id+'"  >','<textarea class="form-control" placeholder="Description" readonly name="description" >'+element.New.description+'</textarea>','<input class="form-control required" placeholder="News link" readonly name="link" value="'+element.New.link+'">','<input readonly type="text" class="form-control required event-datepicker" required placeholder="From Date" name="from_date" value="'+element.New.from_date+'">','<input readonly type="text" class="form-control required event-datepicker" required placeholder="To Date" name="to_date" value="'+element.New.to_date+'">','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a model = "New" data-id="'+element.New.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a model = "New" data-id="'+btoa(element.New.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();

                      //$('#course-'+element.Course.id+' select').attr(element.Course.program_id);
                  }
              });
           offset += 2;
          $('.event-datepicker').datepicker({
              autoclose: true,
              format : "yyyy-mm-dd",
              startDate : new Date()
            });
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
