<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">

    <div class="container bg_meet-team">
        <div class="bg_meet-tea">
            <div class="meet_heading">
                <h2>Meet the <b>Team</b></h2>
            </div>
            <?php $i=0;foreach($teams as $team){?>
            	<?php if($i==0){
            		?>
		            <div class="row">
		                <div class="col-md-5 text-center">
		                    <div class="main_div-round">
		                        <!-- <div class="img_round-bg">

		                        </div> -->

		                        <div class="img_round-img">

		                            <img src="<?php echo HTTP_ROOT.'img/userImages/'.$team['Team']['image']?>" alt="<?php echo $team['Team']['name']?>" class="img-responsive">
		                        </div>
		                        <div class="icon_div icon_div_2">
		                            <div class="icon_div-roun icon_div-roun_154">
		                                <a href="<?php echo $team['Team']['twitter']?>" class="inst_icon_1 icoTwitter"><i class="fa fa-twitter"></i></a>
		                                <a href="<?php echo $team['Team']['linkedin']?>" class="inst_icon_2 icoLinkedin"><i class="fa fa-linkedin"></i></a>
		                                <a href="<?php echo $team['Team']['google']?>" class="inst_icon_4 icoGoogle"><i class="fa fa-google-plus"></i></a>
		                            </div>
		                            <div class="icon_div-roun">
		                                <a href="<?php echo $team['Team']['insta']?>" class="inst_icon_3 icoVimeo"><i class="fa fa-instagram"></i></a>
		                                <a href="<?php echo $team['Team']['fb']?>" class="inst_icon_5 icoFacebook"><i class="fa fa-facebook"></i></a>
		                            </div>
		                        </div>
		                    </div>
		                </div>

		                <div class="col-md-7">
		                    <div class="meet_team-text">
		                        <div class="text_meeet text_meeet_2">
		                            <h3><?php echo $team['Team']['name']?></h3>
		                            <h4><?php echo $team['Team']['education']?></h4>
		                            <P><?php echo $team['Team']['about_member']?></P>
		                        </div>
		                    </div>
		                </div>
		            </div>
	           		 <!--==end of first row=-->
	            <?php }?>
	            <?php if($i%2 != 0 && $i != 0){?>
		            <div class="row margin_row <?php if($i == (count($teams)-1)){?>margin_bu_row<?php }?>" style="margin-top: 3%!important;">
		                <div class="col-md-7">
		                    <div class="meet_team-text meet_team-text_12">
		                        <div class="text_meeet">
		                            <h3><?php echo $team['Team']['name']?></h3>
		                            <h4><?php echo $team['Team']['education']?></h4>
		                            <P><?php echo $team['Team']['about_member']?></P>
		                        </div>
		                    </div>
		                </div>
		                <div class="col-md-5 text-center">
		                    <div class="main_div-round">
		                        <!-- <div class="img_round-bg">

		                        </div> -->

		                        <div class="img_round-img">

		                            <img src="<?php echo HTTP_ROOT.'img/userImages/'.$team['Team']['image']?>" alt="<?php echo $team['Team']['name']?>" class="img-responsive">
		                        </div>
		                        <div class="icon_div icon_div_2">
		                            <div class="icon_div-roun icon_div-roun_154">
		                                <a href="<?php echo $team['Team']['twitter']?>" class="inst_icon_1 icoTwitter"><i class="fa fa-twitter"></i></a>
		                                <a href="<?php echo $team['Team']['linkedin']?>" class="inst_icon_2 icoLinkedin"><i class="fa fa-linkedin"></i></a>
		                                <a href="<?php echo $team['Team']['google']?>" class="inst_icon_4 icoGoogle"><i class="fa fa-google-plus"></i></a>
		                            </div>
		                            <div class="icon_div-roun">
		                                <a href="<?php echo $team['Team']['insta']?>" class="inst_icon_3 icoVimeo"><i class="fa fa-instagram"></i></a>
		                                <a href="<?php echo $team['Team']['fb']?>" class="inst_icon_5 icoFacebook"><i class="fa fa-facebook"></i></a>
		                            </div>
		                        </div>
		                    </div>
		                </div>

		               <!--  <div class="col-md-5 col_2nd">
		                    <div class="main_div-round main_div-round_2 main_div-round_3">
		                        <div class="img_round-bg">

		                        </div>
		                        <div class="img_round-img">
		                            <img src="<?php echo HTTP_ROOT.'img/userImages/'.$team['Team']['image']?>" alt="<?php echo $team['Team']['name']?>" class="img-responsive bg_overly bg_overly_2 bg_overly_3">
		                        </div>
		                        <div class="icon_div">
		                            <div class="icon_div-roun">
		                                <a target='_blank' href="<?php echo $team['Team']['twitter']?>" class="inst_icon_1 icoTwitter"><i class="fa fa-twitter"></i></a>
		                                <a target='_blank' href="<?php echo $team['Team']['linkedin']?>" class="inst_icon_2 icoLinkedin"><i class="fa fa-linkedin"></i></a>
		                                <a target='_blank' href="<?php echo $team['Team']['google']?>" class="inst_icon_4 icoGoogle"><i class="fa fa-google-plus"></i></a>
		                            </div>
		                            <div class="icon_div-roun">
		                                <a target='_blank' href="<?php echo $team['Team']['insta']?>" class="inst_icon_3 icoVimeo"><i class="fa fa-instagram"></i></a>
		                                <a target='_blank' href="<?php echo $team['Team']['fb']?>" class="inst_icon_5 icoFacebook"><i class="fa fa-facebook"></i></a>
		                            </div>
		                        </div>
		                    </div>
		                </div> -->
		            </div>
		            <!--==end of 3th row=-->
	            <?php }?>



	            <?php if($i%2 == 0 && $i != 0){?>
		            <div class="row margin_row <?php if($i == (count($teams)-1)){?>margin_bu_row<?php }?>" style="margin-top: 3%!important;">
		              <!--   <div class="col-md-5">
		                    <div class="main_div-round main_div-round_2">
		                        <div class="img_round-bg"></div>

		                        <div class="img_round-img">
		                            <img src="<?php echo HTTP_ROOT.'img/userImages/'.$team['Team']['image']?>" alt="<?php echo $team['Team']['name']?>" class="img-responsive bg_overly bg_overly_2">
		                        </div>
		                        <div class="icon_div">
		                            <div class="icon_div-roun">
		                                <a target='_blank'  href="<?php echo $team['Team']['twitter']?>" class="inst_icon_1 icoTwitter"><i class="fa fa-twitter"></i></a>
		                                <a target='_blank' href="<?php echo $team['Team']['linkedin']?>" class="inst_icon_2 icoLinkedin"><i class="fa fa-linkedin"></i></a>
		                                <a target='_blank' href="<?php echo $team['Team']['google']?>" class="inst_icon_4 icoGoogle"><i class="fa fa-google-plus"></i></a>
		                            </div>
		                            <div class="icon_div-roun">
		                                <a target='_blank'  href="<?php echo $team['Team']['insta']?>" class="inst_icon_3 icoVimeo"><i class="fa fa-instagram"></i></a>
		                                <a target='_blank' href="<?php echo $team['Team']['fb']?>" class="inst_icon_5 icoFacebook"><i class="fa fa-facebook"></i></a>
		                            </div>
		                        </div>

		                    </div>
		                </div> -->
<div class="col-md-5 text-center">
		                    <div class="main_div-round">
		                        <!-- <div class="img_round-bg">

		                        </div> -->

		                        <div class="img_round-img">

		                            <img src="<?php echo HTTP_ROOT.'img/userImages/'.$team['Team']['image']?>" alt="<?php echo $team['Team']['name']?>" class="img-responsive">
		                        </div>
		                        <div class="icon_div icon_div_2">
		                            <div class="icon_div-roun icon_div-roun_154">
		                                <a href="<?php echo $team['Team']['twitter']?>" class="inst_icon_1 icoTwitter"><i class="fa fa-twitter"></i></a>
		                                <a href="<?php echo $team['Team']['linkedin']?>" class="inst_icon_2 icoLinkedin"><i class="fa fa-linkedin"></i></a>
		                                <a href="<?php echo $team['Team']['google']?>" class="inst_icon_4 icoGoogle"><i class="fa fa-google-plus"></i></a>
		                            </div>
		                            <div class="icon_div-roun">
		                                <a href="<?php echo $team['Team']['insta']?>" class="inst_icon_3 icoVimeo"><i class="fa fa-instagram"></i></a>
		                                <a href="<?php echo $team['Team']['fb']?>" class="inst_icon_5 icoFacebook"><i class="fa fa-facebook"></i></a>
		                            </div>
		                        </div>
		                    </div>
		                </div>

		                <div class="col-md-7">
		                    <div class="meet_team-text">
		                        <div class="text_meeet text_meeet_2">
		                            <h3><?php echo $team['Team']['name']?></h3>
		                            <h4><?php echo $team['Team']['education']?></h4>
		                            <P><?php echo $team['Team']['about_member']?></P>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <!--==end of 3th row=-->
	            <?php }?>
            <?php $i++;}?>
        </div>
    </div>

</section>