<div class="main">
      
      <section class="slider">
        <div class="fslider">
          <div class="container-fluid">
            <div class="row">
              <div class="flexslider">
                <ul class="slides" style="margin-bottom: 0px;">
                  <!-- <li>
                    <img src="images/top5.jpg" />
                  </li>
                  <li>
                    <img src="images/top2.jpg" />
                  </li> -->
                  <li>
                    <img src="<?php echo HTTP_ROOT?>images/top5.jpg" />
                    <div class="college-name">
                      <a href="">IIT, Roorkee, UK</a>
                    </div>
                  </li>
                  <li>
                    <img src="<?php echo HTTP_ROOT?>images/top5.jpg" />
                    <div class="college-name">
                      <a href="">Thapar University, Patiala, PB</a>
                    </div>
                  </li>
                  <!-- <li>
                    <img src="images/top4.jpg" />
                  </li> -->
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".search-main-btn").hover(function() {
                    $(".srch-inner").show(800);
                    $(this).hide(600);
                    $(".slider-featured").hide(600);
                })
                $(".close-btn").click(function() {
                    $(".srch-inner").hide(800);
                    $(".search-main-btn").show(600);
                    $(".slider-featured").show(600);
                })
              });
      </script>
        <div class="search-wrap">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="search-main1">
                  <h2>Best Platform to search Top colleges in india</h2>
                  <h4>Apply to your favourite Top College today!</h4>
                     <div class="search-main" style="margin-top: 2%;">
                                    <i class="fa fa-search search-main-btn"></i>
                                    <!-- Nav tabs -->
                                    <div class="srch-inner">
                                        <i class="fa fa-close close-btn"></i>
                                        <ul class="nav nav-tabs nav-tab-custom" role="tablist">
                                            <li role="presentation" class="active"><a href="#home1" aria-controls="home1" role="tab" data-toggle="tab" >College</a></li>
                                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" onclick="$('#advanced-srch').hide();">Courses</a></li>
                                            <!-- <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" onclick="$('#advanced-srch').hide();">Institue</a></li> -->
                                            <li role="presentation"><a href="#profile" aria-controls="settings" role="tab" data-toggle="tab" onclick="$('#advanced-srch').hide();">Keyword</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content tab-content_21">
                                            <div role="tabpanel" class="tab-pane tab-pane_2 tab-panel-custom active" id="home1">
                                                <form action="<?php echo HTTP_ROOT?>Homes/search" class="form-inline row search" method="get">
                                                    <div class="form-group  col-md-9 col-sm-9">
                                                        <input id="hdr-search" class="form-control search-main-input input_2" placeholder="Enter college name" name="q" autocomplete="off">

                                                         <ul class="results" id="srch-result">
                                                            
                                                         </ul>
                                                        <select class="form-control search-main-select" data-val="true" data-val-number="The field Type must be a number." data-val-required="The Type field is required." id="clg-srch-location" name="">
                                                            <option>Choose Location</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 col-sm-2">
                                                        <button type="submit" class="btn btn-success btn-srch p-t-10" id="hdr-srch-btn">Search</button>
                                                    </div>
                                                </form>
                                                <div class="tab-content tab-content_21 tab-content_222" id="advanced-srch" style="display:none;">
                                            <div class="">
                                                <div class="row" style="float: left;width: 100%;margin-top:20px;">
                                                    <form action="/Home/SearchEvent" class="form_32" style="margin-top: 0px;">
                                                    <span class="text-left col-md-3" style="margin-top: 8px;">
                                                     Advanced Options :
                                                     </span>
                                                     <div class="col-md-4">
                                                     <select name="" style="width: 100%!important;float: left;" class="form-control search-main-select" id="clg-srch-strm">
                                                       <option value="">Select Stream</option>
                                                       
                                                     </select>
                                                     </div>
                                                     <div class="col-md-5">
                                                     <select name="" style="width: 100%!important;float: left;" class="form-control search-main-select" id="clg-srch-crs">
                                                       <option value="">Select Course</option>
                                                       
                                                     </select> 
                                                     </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane tab-panel-custom" id="profile">
                                                <form action="<?php echo HTTP_ROOT?>Homes/search" class="form-inline row search" method="get">
                                                    <div class="form-group  col-md-9 col-sm-9 p-5">
                                                        <input id="hdr-search-crse" class="form-control search-main-input input_2" placeholder="Search" name="course" style="width: 100%!important;" autocomplete="off">

                                                         <ul class="results" id="srch-result-crse">
                                                            
                                                         </ul>
                                                    </div>
                                                    <div class="col-md-3 col-sm-2 p-5">
                                                        <button type="button" class="btn btn-success btn-srch p-t-10" id="hdr-srch-btn-crse">Search</button>
                                                    </div>
                                                </form>
                                            </div>

                                            <div role="tabpanel" class="tab-pane tab-panel-custom" id="messages">3</div>
                                            <div role="tabpanel" class="tab-pane tab-panel-custom" id="settings">4</div>
                                        </div>
                    
                                    </div>
                                </div>
                  
                                  <ul class="slider-featured">
                                    <li>
                                      <a href="">
                                        <img src="<?php echo HTTP_ROOT?>images/centurian.png"><br>
                                        <small>Centurian University</small>
                                      </a>
                                    </li>
                                    <li>
                                      <a href="">
                                        <img src="<?php echo HTTP_ROOT?>images/panjabuniversity.png"><br>
                                        <small>Panjab University</small>
                                      </a>
                                    </li>
                                    <li>
                                      <a href="">
                                        <img src="<?php echo HTTP_ROOT?>images/amity.png"><br>
                                        <small>Amity University</small>
                                      </a>
                                    </li>
                                    <li>
                                      <a href="">
                                        <img src="<?php echo HTTP_ROOT?>images/lpu.png"><br>
                                        <small>Lovely Pro. University</small>
                                      </a>
                                    </li>
                                    <li>
                                      <a href="">
                                        <img src="<?php echo HTTP_ROOT?>images/iitrorkee.jpg"><br>
                                        <small>IIT Roorkee</small>
                                      </a>
                                    </li>
                                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <marquee style="color: #ff9900; background:#fff; font-weight: bold; padding:12px 0px;  scrolldelay=85 scrollamount=3" onmouseover="this.stop();" onmouseout="this.start();">
          <a target="_blank" href="http:getcollege.in">Important Link</a>&nbsp;&nbsp; |
          <a target="_blank" href="http:buildrepo.com">Buildrepo</a>&nbsp;&nbsp; |
          <a target="_blank" href="http:getcollege.in">GetCollege.in</a>&nbsp;&nbsp; |
    </marquee>
    <section class="featured">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 text-center m-tb-15">
            <div class="section-headings">
              <h2 class="m-b-10">Featured Colleges in India</h2>
              <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p> -->
              <div class="heading-border"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="top-colleges-wrap">
              



              <div class="top-colleges">
                <div class="colleges-thumbnail">
                  <img src="<?php echo HTTP_ROOT?>images/best-of-week-logo.png" alt="featured" class="best_icon">
                  <img src="<?php echo HTTP_ROOT?>images/iitrorkee.jpg" alt="iitrorkee" class="colleges-logo">
                  <small class="colleges_stats"><span class="lnr lnr-heart"></span> 1848</small>
                  <img src="<?php echo HTTP_ROOT?>images/top5.jpg" alt="ben-white-165564" class="colleges_img">
                  <div class="overlay"></div>
                  <div class="colleges-title-wrap">
                    <h3 class="colleges-title">Indian Institute of Technology Roorkee</h3>
                    <small class="colleges-location"><span class="lnr lnr-map-marker"></span> Roorkee, Uttarakhand, India</small>
                  </div>
                </div>
                <div class="colleges-other-info">
                  <div class="wrapper">
                    <input type="checkbox" id="st1" value="1" />
                    <label for="st1"></label>
                    <input type="checkbox" id="st2" value="2" checked="checked"/>
                    <label for="st2"></label>
                    <input type="checkbox" id="st3" value="3" />
                    <label for="st3"></label>
                    <input type="checkbox" id="st4" value="4" />
                    <label for="st4"></label>
                    <input type="checkbox" id="st5" value="5"  />
                    <label for="st5"></label>

                  </div>
                  <small class="rating-stat">9.5 / 10</small>
                  <div class="courses-wrap">
                    <small>B.Tech. (All Streams)</small>
                    <small>B.Arch.</small>
                  </div>
                  <a href="">View all Courses</a>
                  <button class="btn colleges-cta"><i class="fa fa-arrows-alt" aria-hidden="true"></i> &nbsp;Add to Compare</button>
                  <button class="btn colleges-cta"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> &nbsp;Apply Now</button>
                </div>
              </div>
              





              <div class="top-colleges">
                <div class="colleges-thumbnail">
                  <img src="<?php echo HTTP_ROOT?>images/best-of-week-logo.png" alt="featured" class="best_icon">
                  <img src="<?php echo HTTP_ROOT?>images/iitrorkee.jpg" alt="iitrorkee" class="colleges-logo">
                  <small class="colleges_stats"><span class="lnr lnr-heart"></span> 1848</small>
                  <img src="<?php echo HTTP_ROOT?>images/top5.jpg" alt="ben-white-165564" class="colleges_img">
                  <div class="overlay"></div>
                  <div class="colleges-title-wrap">
                    <h3 class="colleges-title">Indian Institute of Technology Roorkee</h3>
                    <small class="colleges-location"><span class="lnr lnr-map-marker"></span> Roorkee, Uttarakhand, India</small>
                  </div>
                </div>
                <div class="colleges-other-info">
                  <div class="wrapper">
                    <input type="checkbox" id="st1" value="1" />
                    <label for="st1"></label>
                    <input type="checkbox" id="st2" value="2" checked="checked"/>
                    <label for="st2"></label>
                    <input type="checkbox" id="st3" value="3" />
                    <label for="st3"></label>
                    <input type="checkbox" id="st4" value="4" />
                    <label for="st4"></label>
                    <input type="checkbox" id="st5" value="5"  />
                    <label for="st5"></label>

                  </div>
                  <small class="rating-stat">9.5 / 10</small>
                  <div class="courses-wrap">
                    <small>B.Tech. (All Streams)</small>
                    <small>B.Arch.</small>
                  </div>
                  <a href="">View all Courses</a>
                  <button class="btn colleges-cta"><i class="fa fa-arrows-alt" aria-hidden="true"></i> &nbsp;Add to Compare</button>
                  <button class="btn colleges-cta"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> &nbsp;Apply Now</button>
                </div>
              </div>
              <div class="top-colleges">
                <div class="colleges-thumbnail">
                  <img src="<?php echo HTTP_ROOT?>images/best-of-week-logo.png" alt="featured" class="best_icon">
                  <img src="<?php echo HTTP_ROOT?>images/iitrorkee.jpg" alt="iitrorkee" class="colleges-logo">
                  <small class="colleges_stats"><span class="lnr lnr-heart"></span> 1848</small>
                  <img src="<?php echo HTTP_ROOT?>images/top5.jpg" alt="ben-white-165564" class="colleges_img">
                  <div class="overlay"></div>
                  <div class="colleges-title-wrap">
                    <h3 class="colleges-title">Indian Institute of Technology Roorkee</h3>
                    <small class="colleges-location"><span class="lnr lnr-map-marker"></span> Roorkee, Uttarakhand, India</small>
                  </div>
                </div>
                <div class="colleges-other-info">
                  <div class="wrapper">
                    <input type="checkbox" id="st1" value="1" />
                    <label for="st1"></label>
                    <input type="checkbox" id="st2" value="2" checked="checked"/>
                    <label for="st2"></label>
                    <input type="checkbox" id="st3" value="3" />
                    <label for="st3"></label>
                    <input type="checkbox" id="st4" value="4" />
                    <label for="st4"></label>
                    <input type="checkbox" id="st5" value="5"  />
                    <label for="st5"></label>

                  </div>
                  <small class="rating-stat">9.5 / 10</small>
                  <div class="courses-wrap">
                    <small>B.Tech. (All Streams)</small>
                    <small>B.Arch.</small>
                  </div>
                  <a href="">View all Courses</a>
                  <button class="btn colleges-cta"><i class="fa fa-arrows-alt" aria-hidden="true"></i> &nbsp;Add to Compare</button>
                  <button class="btn colleges-cta"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> &nbsp;Apply Now</button>
                </div>
              </div>
              <div class="top-colleges">
                <div class="colleges-thumbnail">
                  <img src="<?php echo HTTP_ROOT?>images/best-of-week-logo.png" alt="featured" class="best_icon">
                  <img src="<?php echo HTTP_ROOT?>images/iitrorkee.jpg" alt="iitrorkee" class="colleges-logo">
                  <small class="colleges_stats"><span class="lnr lnr-heart"></span> 1848</small>
                  <img src="<?php echo HTTP_ROOT?>images/top5.jpg" alt="ben-white-165564" class="colleges_img">
                  <div class="overlay"></div>
                  <div class="colleges-title-wrap">
                    <h3 class="colleges-title">Indian Institute of Technology Roorkee</h3>
                    <small class="colleges-location"><span class="lnr lnr-map-marker"></span> Roorkee, Uttarakhand, India</small>
                  </div>
                </div>
                <div class="colleges-other-info">
                  <div class="wrapper">
                    <input type="checkbox" id="st1" value="1" />
                    <label for="st1"></label>
                    <input type="checkbox" id="st2" value="2" checked="checked"/>
                    <label for="st2"></label>
                    <input type="checkbox" id="st3" value="3" />
                    <label for="st3"></label>
                    <input type="checkbox" id="st4" value="4" />
                    <label for="st4"></label>
                    <input type="checkbox" id="st5" value="5"  />
                    <label for="st5"></label>

                  </div>
                  <small class="rating-stat">9.5 / 10</small>
                  <div class="courses-wrap">
                    <small>B.Tech. (All Streams)</small>
                    <small>B.Arch.</small>
                  </div>
                  <a href="">View all Courses</a>
                  <button class="btn colleges-cta"><i class="fa fa-arrows-alt" aria-hidden="true"></i> &nbsp;Add to Compare</button>
                  <button class="btn colleges-cta"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> &nbsp;Apply Now</button>
                </div>
              </div>
              
           
              <!-- <div class="top-colleges">
                <div class="colleges-thumbnail">
                  <img src="<?php echo HTTP_ROOT?>images/best-of-week-logo.png" alt="featured" class="best_icon">
                  <img src="<?php echo HTTP_ROOT?>images/iitrorkee.jpg" alt="iitrorkee" class="colleges-logo">
                  <small class="colleges_stats"><span class="lnr lnr-heart"></span> 1848</small>
                  <img src="<?php echo HTTP_ROOT?>images/top5.jpg" alt="ben-white-165564" class="colleges_img">
                  <div class="overlay"></div>
                  <div class="colleges-title-wrap">
                    <h3 class="colleges-title">Indian Institute of Technology Roorkee</h3>
                    <small class="colleges-location"><span class="lnr lnr-map-marker"></span> Roorkee, Uttarakhand, India</small>
                  </div>
                </div>
                <div class="colleges-other-info">
                  <div class="wrapper">
                    <input type="checkbox" id="st1" value="1" />
                    <label for="st1"></label>
                    <input type="checkbox" id="st2" value="2" checked="checked"/>
                    <label for="st2"></label>
                    <input type="checkbox" id="st3" value="3" />
                    <label for="st3"></label>
                    <input type="checkbox" id="st4" value="4" />
                    <label for="st4"></label>
                    <input type="checkbox" id="st5" value="5"  />
                    <label for="st5"></label>

                  </div>
                  <small class="rating-stat">9.5 / 10</small>
                  <div class="courses-wrap">
                    <small>B.Tech. (All Streams)</small>
                    <small>B.Arch.</small>
                  </div>
                  <a href="">View all Courses</a>
                  <button class="btn colleges-cta"><i class="fa fa-arrows-alt" aria-hidden="true"></i> &nbsp;Add to Compare</button>
                  <button class="btn colleges-cta"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> &nbsp;Apply Now</button>
                </div>
              </div> -->
              
              


            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="career-selection p-t-30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="career-wrap">
              <!-- Nav tabs -->
              <div class="nav-tabs-wrap">
                <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                  <li role="presentation" class="active"><a href="#top-fields" aria-controls="top-fields" role="tab" data-toggle="tab">Streams</a></li>
                  <li role="presentation"><a href="#top-courses" aria-controls="top-courses" role="tab" data-toggle="tab">News</a></li>
                  <li role="presentation"><a href="#top-unis" aria-controls="top-unis" role="tab" data-toggle="tab">Top Colleges</a></li>
                  <li role="presentation"><a href="#top-exams" aria-controls="top-exams" role="tab" data-toggle="tab">From the Blog</a></li>
                </ul>
              </div>
              <div class="tab-content-wrap">
                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="top-fields">
                    <div class="container">
                      <div class="row m-t-25">
                        <div class="top-fields-icon-wrap">
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#solar-system"/>
                            </svg>
                            <p>Science</p>
                          </div>
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#computer"/>
                            </svg>
                            <p>Mass Communication</p>
                          </div>
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#dinner-1"/>
                            </svg>
                            <p>Hotel Management</p>
                          </div>
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#laptop"/>
                            </svg>
                            <p>Computer Applications</p>
                          </div>
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#pharmacy"/>
                            </svg>
                            <p>Medical</p>
                          </div>
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#drug"/>
                            </svg>
                            <p>Pharmacy</p>
                          </div>
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#auction"/>
                            </svg>
                            <p>LAW</p>
                          </div>
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#needle"/>
                            </svg>
                            <p>Fashion Design</p>
                          </div>
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#paint-palette"/>
                            </svg>
                            <p>Art</p>
                          </div>
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#blueprint"/>
                            </svg>
                            <p>Architecture</p>
                          </div>
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#manager"/>
                            </svg>
                            <p>Management</p>
                          </div>
                          <div class="top-fields-icon">
                            <svg class="icon">
                                <use xlink:href="#store"/>
                            </svg>
                            <p>Commerce</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="top-courses">
                    <div class="news" style="">
                          <div class="news_head"></div>
                            <ul>
                              <marquee direction="up" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();" style="height: 300px;" height="300">
                                <li><p class="date" style="color: #ED8D38;"><b>12/07/2017</b></p><a href="">Explore what all is going on in education sector. Current notification of Scholarship, Test Olympiad, NTSE.</a>
                                </li>
                                <li><br></li>
                                <li><!--<p class="date">05/04/2017</p>--><a href="">Notification of IIT-JEE, NEET and all State Entrance Exams.</a>
                                </li>
                                <li><br></li>
                                <li><!--<p class="date">05/04/2017</p>--><a href="http://scholarship.up.nic.in/" target="_blank">Uttar Pradesh UP Scholarship Schemes 2017 for Class 9th, 10th, 11th and 12th | Apply Online for Uttar Pradesh UG, PG, Pre and Post Matric Scholarships Online Form</a>
                                </li>
                                <li><br></li>
                                <li><!--<p class="date">05/04/2017</p>--><a href="#">AIIMS MBBS 2017 Application Form, Exam Date</a>
                                </li>
                                <li><br></li>
                                <li><!--<p class="date">05/04/2017</p>--><a href="#">JIPMER MBBS 2017 Application Form, Exam Dates</a>
                                </li>
                                <li><br></li>
                                <li><!--<p class="date">05/04/2017</p>--><a href="#">Class 10th 12th Exam Time Table 2018 / Board Exam Date Sheet</a>
                                </li>
                                <li><br></li>
                                <li><!--<p class="date">05/04/2017</p>--><a href="#">UP Board Date Sheet 2018 | Time Table for 12th Class Exams</a>
                                </li>
                                </marquee>
                            </ul>
                            
                        </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="top-unis">3</div>
                  <div role="tabpanel" class="tab-pane" id="top-exams">
                  <style>
                  .moto-widget-image.moto-preset-3 .moto-widget-image-link:hover .moto-widget-image-picture 
                  {
                    -webkit-transform: scale(1.1);
                    transform: scale(1.1);
                  }

                  .moto-widget-image .moto-widget-image-link .moto-widget-image-picture 
                  {
                      -webkit-transition: .5s;
                      transition: .5s;
                  }

                  .moto-link .moto-link_12 a:hover, a:focus
                  {      
                     color: #23527c;
                     text-decoration: none;
                  }

                  .moto-widget-image-link {
                      margin-bottom: 3%;
                  }
                  .testimonial-name
                  {
                    flex-basis: 20%!important;
                  }
                  </style>
                          <div class="moto-widget moto-widget-row row-fixed" style="margin-top: 3%;" data-widget="row">
                          <div class="container-fluid">
                              <div class="row">
                                  <div class="moto-cell col-sm-4" data-container="container">
                                      <div data-widget-id="wid__image__58d1ff0671466" class="moto-widget moto-widget-image moto-preset-3 moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto wow fadeIn" data-preset="3" data-widget="image" style="visibility: visible;">
                                          <div class="moto-widget-image-link">
                                              <a target="_self" href="http://www.getcollege.in/blog/2017/07/12/central-sector-scheme-of-scholarship-for-college-and-university-students-get-college-in/">
                                              <img src="http://www.getcollege.in//frontend/mt-0362-home-blog1.jpg" class="moto-widget-image-picture" data-id="104" title="" alt="" draggable="false"></a>
                                          </div>
                                      </div>
                                      <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                          <div class="moto-widget-spacer-block" style="height: 5px;"></div>
                                      </div>
                                      <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default                   wow fadeIn moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" style="visibility: visible;">
                                          <div class="moto-widget-text-content moto-widget-text-editable">
                                              <p class="moto-text_system_13" style="text-align: center;"><a class="moto-link moto-link_12" data-action="url" target="_self" href="http://www.getcollege.in/blog/2017/07/12/central-sector-scheme-of-scholarship-for-college-and-university-students-get-college-in/">SCHOLARSHIP</a><br></p>
                                          </div>
                                      </div>
                                      <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default                wow fadeIn moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" style="visibility: visible;">
                                          <div class="moto-widget-text-content moto-widget-text-editable">
                                              <p class="moto-text_system_10 stem_1012" style="text-align:left;">Central Sector Scheme of Scholarship for College and University Students | Get College.in ... <a href="http://www.getcollege.in/blog/2017/07/12/central-sector-scheme-of-scholarship-for-college-and-university-students-get-college-in/" class="text_justi">Read More</a><br></p>
                                          </div>
                                      </div>
                                      <div class="moto-widget moto-widget-row" data-widget="row">
                                          <div class="container-fluid">
                                              <div class="row">
                                                  <div class="moto-cell col-sm-7" data-container="container">
                                                      <p class="moto-text_system_11 text_system"><i class="fa fa-user icon_gree"></i> Super User</p>
                                                  </div>
                                                  <div class="moto-cell col-sm-5" data-container="container">
                                                      <p class="moto-text_system_11 moto_system_2"><i class="fa fa-calendar icon_gree"></i> 12 july 2017</p>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="moto-cell col-sm-4" data-container="container">
                                      <div data-widget-id="wid__image__58d1ff0672d7c" class="moto-widget moto-widget-image moto-preset-3 moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto wow fadeIn" data-preset="3" data-widget="image" style="visibility: visible;">
                                          <div class="moto-widget-image-link">
                                              <a target="_self" href="http://www.getcollege.in/blog/2017/07/12/education-loan-in-india/">
                                              <img src="http://www.getcollege.in//frontend/mt-0362-home-blog2.jpg" class="moto-widget-image-picture" data-id="105" title="" alt="" draggable="false"></a>
                                          </div>
                                      </div>
                                      <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                          <div class="moto-widget-spacer-block" style="height: 5px;"></div>
                                      </div>
                                      <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default                wow fadeIn moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" style="visibility: visible;">
                                          <div class="moto-widget-text-content moto-widget-text-editable">
                                              <p class="moto-text_system_13" style="text-align: center;"><a class="moto-link moto-link_12" data-action="url" target="_self" href="http://www.getcollege.in/blog/2017/07/12/education-loan-in-india/">EDUCATION LOAN</a><br></p>
                                          </div>
                                      </div>
                                      <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default                wow fadeIn moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" style="visibility: visible;">
                                          <div class="moto-widget-text-content moto-widget-text-editable">
                                              <p class="moto-text_system_10 stem_1012" style="text-align:left;">Education Loan in India ...<a href="http://www.getcollege.in/blog/2017/07/12/education-loan-in-india/" class="text_justi">Read More</a></p>
                                          </div>
                                      </div>
                                      <div class="moto-widget moto-widget-row" data-widget="row">
                                          <div class="container-fluid">
                                              <div class="row">
                                                  <div class="moto-cell col-sm-7" data-container="container">
                                                      <p class="moto-text_system_11 text_system"><i class="fa fa-user icon_gree"></i> Super User</p>
                                                  </div>
                                                  <div class="moto-cell col-sm-5" data-container="container">
                                                      <p class="moto-text_system_11 moto_system_2"><i class="fa fa-calendar icon_gree"></i> 13 july 2017</p>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="moto-cell col-sm-4" data-container="container">
                                      <div data-widget-id="wid__image__58d1ff06736fb" class="moto-widget moto-widget-image moto-preset-3 moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto wow fadeIn" data-preset="3" data-widget="image" style="visibility: visible;">
                                          <div class="moto-widget-image-link">
                                              <a target="_self" href="http://www.getcollege.in/blog/2017/07/12/1-step-on-the-career-ladder/">
                                              <img src="http://www.getcollege.in//frontend/mt-0362-home-blog23jpg.jpg" class="moto-widget-image-picture" data-id="109" title="" alt="" draggable="false"></a>
                                          </div>
                                      </div>
                                      <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                          <div class="moto-widget-spacer-block" style="height: 5px;"></div>
                                      </div>
                                      <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default                wow fadeIn moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" style="visibility: visible;">
                                          <div class="moto-widget-text-content moto-widget-text-editable">
                                              <p class="moto-text_system_13" style="text-align: center;"><a class="moto-link moto-link_12" data-action="url" target="_self" href="http://www.getcollege.in/blog/2017/07/12/1-step-on-the-career-ladder/">CAREER / SUCCESS AFTER 12TH</a><br></p>
                                          </div>
                                      </div>
                                      <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default                wow fadeIn moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa" style="visibility: visible;">
                                          <div class="moto-widget-text-content moto-widget-text-editable">
                                              <p class="moto-text_system_10 stem_1012" style="text-align:left;">+1 Step on the career ladder | Get College.in ...<a href="http://www.getcollege.in/blog/2017/07/12/1-step-on-the-career-ladder/" class="text_justi">Read More</a><br></p>
                                          </div>
                                      </div>
                                      <div class="moto-widget moto-widget-row" data-widget="row">
                                          <div class="container-fluid">
                                              <div class="row">
                                                  <div class="moto-cell col-sm-7" data-container="container">
                                                      <p class="moto-text_system_11 text_system"><i class="fa fa-user icon_gree"></i> Super User</p>
                                                  </div>
                                                  <div class="moto-cell col-sm-5" data-container="container">
                                                      <p class="moto-text_system_11 moto_system_2"><i class="fa fa-calendar icon_gree"></i> 14 july 2017</p>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>


                  </div>
                </div>
              </div>
            </div>    
          </div>
        </div>
      </div>
    </section>
    <section class="newsletter">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 text-center m-tb-15">
            <div class="section-headings">
              <h2 class="m-b-10">Our Newsletter</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
              <div class="heading-border"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <form>
              <div class="form-group">
                <div class="newsletter-wrap">
                  <input type="text" class="form-control" id="exampleInputAmount" placeholder="Your Email Address">
                  <button class="btn"> Subscribe</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <section class="videos-section">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 text-center m-tb-15">
            <div class="section-headings">
              <h2 class="m-b-10">Video Testimonials</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
              <div class="heading-border"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="owl-carousel owl-theme">
                <div class="item-video" data-merge="2">
                  <iframe width="100%" height="315" src="https://www.youtube.com/watch?v=vqkz9wEbIPI&feature=youtu.be" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="item-video" data-merge="1">
                  <iframe width="100%" height="315" src="https://www.youtube.com/watch?v=vqkz9wEbIPI&feature=youtu.be" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="item-video" data-merge="2">
                  <iframe width="100%" height="315" src="https://www.youtube.com/watch?v=vqkz9wEbIPI&feature=youtu.be" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="item-video" data-merge="1">
                  <iframe width="100%" height="315" src="https://www.youtube.com/watch?v=vqkz9wEbIPI&feature=youtu.be" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="item-video" data-merge="2">
                  <iframe width="100%" height="315" src="https://www.youtube.com/watch?v=vqkz9wEbIPI&feature=youtu.be" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="item-video" data-merge="3">
                  <iframe width="100%" height="315" src="https://www.youtube.com/watch?v=vqkz9wEbIPI&feature=youtu.be" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="testimonials">
    <div class="row">
          <div class="col-md-8 col-md-offset-2 text-center m-tb-15">
            <div class="section-headings">
              <h2 class="m-b-10">Our Happy Users.</h2>
              <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p> -->
              <div class="heading-border"></div>
            </div>
          </div>
        </div>
              <div class="testimonial_sloder">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12" data-wow-delay="0.2s">
                                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                                    <div class="carousel-inner text-center">
                                        <?php $i=0;foreach($testimonials as $testi){?>
                                          <div class="item <?php echo $i==0 ? 'active' : '';?>">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                  <div class="testimonial-wrap">
                                                      <div class="testimonial-head">
                                                        <?php if($testi['Testimonial']['image']!=''){ ?>
                                                          <img src="<?php echo HTTP_ROOT?>/img/userImages/<?php echo $testi['Testimonial']['image']?>">
                                                       <?php } else{ ?>
                                                          <img src="<?php echo HTTP_ROOT?>/img/dummy-profile-pic-male1.jpg">
                                                        
                                                        <?php } ?>
                                                        <div class="testimonial-name">
                                                          <h4><?php echo $testi['Testimonial']['name']?></h4>
                                                          <small><?php echo $testi['Testimonial']['email']?></small>
                                                        </div>
                                                      </div>
                                                      <div class="testimonial-desc">
                                                        <small><?php echo $testi['Testimonial']['content']?></small>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $i++; }?>
                                          <!-- <div class="item">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <div class="testimonial-wrap">
                                                      <div class="testimonial-head">
                                                        <img src="<?php echo HTTP_ROOT?>images/129.jpg">
                                                        <div class="testimonial-name">
                                                          <h4>Mario M.</h4>
                                                          <small>@mario_M85</small>
                                                        </div>
                                                      </div>
                                                      <div class="testimonial-desc">
                                                        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                        cillum dolore eu fugiat nulla pariatur.</small>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                  <div class="testimonial-wrap">
                                                    <div class="testimonial-head">
                                                      <img src="<?php echo HTTP_ROOT?>images/127.jpg">
                                                      <div class="testimonial-name">
                                                        <h4>Mario M.</h4>
                                                        <small>@mario_M85</small>
                                                      </div>
                                                    </div>
                                                    <div class="testimonial-desc">
                                                      <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                      cillum dolore eu fugiat nulla pariatur.</small>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <div class="testimonial-wrap">
                                                      <div class="testimonial-head">
                                                        <img src="<?php echo HTTP_ROOT?>images/128.jpg">
                                                        <div class="testimonial-name">
                                                          <h4>Mario M.</h4>
                                                          <small>@mario_M85</small>
                                                        </div>
                                                      </div>
                                                      <div class="testimonial-desc">
                                                        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                                        cillum dolore eu fugiat nulla pariatur.</small>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        
                                        
                                    </div>
                                    <!-- Carousel Buttons Next/Prev -->
                                    <a data-slide="prev" href="#quote-carousel" class="pull-left"><i class="fa fa-chevron-left"></i></a>
                                    <a data-slide="next" href="#quote-carousel" class="pull-right"><i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>