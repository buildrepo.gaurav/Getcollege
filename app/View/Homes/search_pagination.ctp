<?php if(!empty($colleges)){

                                @$crs_id = $crs_ids['0'];
                                //pr($crs_id);
                                    foreach ($colleges as $college){
                                       
                            ?>
                                      <div class="doctor-section">
                                         <div class="col-md-3 col-sm-3">
                                            <div class="doctor-photo" college-id="<?php echo $college['College']['id']?>" college-name="<?php echo $college['College']['name']?>"> <a href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/collegeImages/large/'.$college['College']['image']?>" class="img-responsive wow bounceInDown"></a> </div>
                                            <div class="margntopbtm">
                                               <div class="add_compair"><a href="javascript:void(0)" id="<?php echo $college['College']['id']?>" class="colg-cmp" style="<?php echo @in_array($college['College']['id'], $this->Session->read('clgCompare')) ? 'color:blue' : '';?>"><i class="fa fa-exchange"></i> <?php echo @in_array($college['College']['id'], $this->Session->read('clgCompare')) ? 'Added to Compare' : 'Add to Compare';?></a></div>
                                               <div class="add_compair">
                                               <?php if(count(@$allCourseId) == 1){?>
                                                  <?php if(!empty($crs_id)){?>
                                                  <a target="_blank" href="<?php 
                                                              foreach($college['CollegeCourse'] as $clgcrs){
                                                                if($clgcrs['course_id'] == $crs_id){
                                                                    echo HTTP_ROOT.'Homes/downloadBrochure/'.$clgcrs['brochure'];
                                                                }
                                                              }
                                                        ?>"><i class="fa fa-arrow-circle-down"></i> Download Brochure</a>
                                                <?php }}?> 
                                               </div>
                                            </div>
                                         </div>
                                         <div class="col-md-6 col-sm-6">
                                            <div class="about-doctor">
                                               <div class="misc-title" style="margin: 0px;">
                                                  <h2><a href="<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>" target="_blank"><?=@$college['College']['name']?></a></h2>
                                               </div>
                                               
                                               <p class="margin0">
                                                 <?php if(!empty($college['CollegeCourse'])){
                                                          $crsarray = [];
                                                          $len = count($college['CollegeCourse']);
                                                          $i = 1;
                                                          foreach($college['CollegeCourse'] as $course){
                                                             if($course['course_id'] == $this->Session->read('selectedCourse') && !in_array($course['course_id'], $crsarray)){
                                                              echo @$course['Course']['course'];
                                                              $crsarray[] = $course['course_id'];
                                                                if($len > $i)
                                                                    echo "";
                                                                      
                                                                $i++;
                                                              }
                                                          }
                                                      }else{
                                                        echo "No course found.";
                                                      }  
                                                 ?>
                                               </p>
                                               <?php if(count(@$allCourseId) == 1){?>
                                               <p><b>Application Fees</b>: <span class="ori-fee"><?php echo $college['CollegeCourse']['0']['registration_fee']?></span><span class="dis-fee dis-fee_1"> <?php echo $college['CollegeCourse']['0']['formsadda_cost']?></span></p>
                                               <p ><b>Exams accepted</b>: 
                                               <?php foreach($college['CollegeCourse'] as $crse){
                                                    foreach($crse['CourseExam'] as $crsexam){
                                                     echo  $crsexam['Exam']['exam'].',';
                                                    }
                                                  }?></p>
                                               <?php }?>
                                               <ul class="collage_faclity">
                                                <?php if(!empty($college['CollegeFacility'])){
                                                        foreach($college['CollegeFacility'] as $fac){
                                                ?>
                                                  <li>
                                                     <img width="20" src="<?php echo HTTP_ROOT.'img/facility/';?><?php echo empty($fac['Facility']['icon']) ? "default.jpg" : $fac['Facility']['icon'];?>" data-toggle="tooltip" title="<?php echo $fac['Facility']['name']?>" >
                                                  </li>
                                                <?php   }
                                                      }
                                                ?>  
                                                  
                                               </ul>
                                               <?php if(count(@$allCourseId) == 1){?>
                                               <p ><b>Last Apply Date</b>: 
                                               
                                             <?php 
                                             $date=date_create(@$college['CollegeCourse']['0']['to_date']);
                                             echo DATE_FORMAT($date,'d M Y')?></p>
                                             <?php }?>
                                            </div>
                                         </div>
                                         <div class="col-md-3 col-sm-3">
                                            <div class="time-table">
                                               <ul>
                                                  <li>
                                                     <div class="tm-desc">
                                                        <p><span class="green"><i class="fa fa-thumbs-up ft-10"></i> 97%</span> <sub>301 vots</sub></p>
                                                     </div>
                                                  </li>
                                                  <li>
                                                     <div class="tm-desc">
                                                        <p><a href="#"><i class="fa fa-commenting"></i> <?php echo count(@$college['Review']);?> Feedback</a></p>
                                                     </div>
                                                  </li>
                                                  <li>
                                                     <div class="tm-desc">
                                                        <p><a href="#"><i class="fa fa-map-marker"></i> <?=@$college['College']['address']?></a></p>
                                                     </div>
                                                  </li>
                                                  <?php if(count(@$allCourseId) == 1){?>
                                                  <?php if(!empty($crs_id)){?>
                                                  <li>
                                                     <div class="tm-desc">
                                                        <p><i class="fa fa-inr"></i> 
                                                        Course Fee : 
                                                        <?php 
                                                        //pr($college['CollegeCourse']);

                                                              foreach($college['CollegeCourse'] as $clgcrs){
                                                                //pr($clgcrs['fees']);
                                                                if($clgcrs['course_id'] == $crs_id){
                                                                    echo $clgcrs['fees'];
                                                                }
                                                              }
                                                        ?>
                                                        </p>
                                                     </div>
                                                  </li>
                                                  <?php }}?>
                                                  <li>
                                                     <div class="tm-desc">
                                                        <p><i class="fa fa-line-chart"></i> 1 Order</p>
                                                        <p class="green">1 Application sold in last 7 days</p>
                                                     </div>
                                                  </li>
                                                  <?php if(count(@$allCourseId) == 1){?>
                                                  <?php 
                                                   if($college['CollegeCourse']['0']['from_date'] <= date('Y-m-d') && $college['CollegeCourse']['0']['to_date'] >= date('Y-m-d')){?>
                                                    <!-- <button data-college-id="<?php echo @$college['College']['id']?>" data-course-id="<?php echo @$college['CollegeCourse']['0']['id']?>" class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm add-cart">Apply</button> -->
                                                  <?php }else{?>
                                                    <!-- <button class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm" data-toggle="modal" data-target="#notifyModal">Notify me</button> -->
                                                  <?php }?> 
                                                  <?php }else{?>

                                                    <!-- <button class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm" data-toggle="modal" data-target="#notifyModal">Notify me</button> -->
                                                    <?php }?>
                                               </ul>
                                            </div>
                                         </div>
                                      </div>
                            <?php    }
                                  }                            
                            ?>  