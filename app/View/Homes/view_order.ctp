<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
  <div class="container">
      <div class="row profile">
      <?php echo $this->element('frontEnd/studentsidebar');?>
      <div class="col-md-9">
              <div class="profile-content">
           <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style>
  a.fafa-icons{
      margin-left: 10px;
}
.fa-trash,.fa-trash-o,.fa-eye,.fa-plus,.fa-save,.fa-building{
  font-size: 24px;
}
.profile-content {
    background: #fff none repeat scroll 0 0;
    min-height: 460px;
    padding: 0!important;
}
</style>
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 style="margin-top:0px; background-color: #008190; color: white; padding: 1%; padding-left: 3%;">
        All Details
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    <h4 style="margin-left: 3%; background-color:#666666; margin:0px; padding: 1%; margin-top:-1.5%; color: white;">Formsadda Colleges</h4>
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box box-primary" style="margin-top: 2%;">
          <div class="row">
          
          <div class="col-md-1"></div>
            <div class="col-md-5"><b>Colleges</b></div>
            <div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>Form Fees</b></div></div>
            <div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>FormsADDA Fees</b></div></div>
            <div class="col-md-1 Head_Fees"><div width="100%" style="text-align:centre;"><b>Total</b></div></div>
          </div>
          <br>
          <?php $i=1; $cnt = count($order['OrderMeta']);  foreach($order['OrderMeta'] as $ord){?>
          
            <?php if(@$ord['CollegeCourse']['direct_apply'] == 0){ ?>
            <div class="row" id="row-id-<?php echo $ord['CollegeCourse']['id']?>">
            <div class="col-md-1"></div>
            <div class="col-md-5"><?php echo $ord['College']['name']?></div>
            
            <div class="col-md-2"><div width="100%" class="Price"><strike>Rs.<?php echo $ord['CollegeCourse']['registration_fee']?></strike></div></div>
            <div class="col-md-2"><div width="100%" class="Price">Rs.<?php echo $ord['CollegeCourse']['formsadda_cost']?></div></div>
            <?php }?>
            <?php @$tot = @$tot + $ord['CollegeCourse']['formsadda_cost'];?>

            <?php if($cnt == $i){?>
              <div class="col-md-1"><div width="100%" class="Price"><?php echo @$tot?></div></div>
            <?php }?>  
          </div>
          <br>
          <?php $i++;  }?>
</div>
        </div>
        <div class="box box-primary">
          <div class="row">
          <h4 style="margin-left: 3.5%; margin-bottom: 2%; background-color: #666666; padding: 1%; color: white ; margin-right: 3.3%; ">Direct Apply College</h4>
          <div class="col-md-1"></div>
            <div class="col-md-5"><div style="margin-left: 1%;"><b>Colleges</b></div></div>
            <div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>Apply Fees</b></div></div>
            <div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>Application No.</b></div></div>
            <div class="col-md-1 Head_Fees"><div width="100%" ><b>Application Reciept</b></div></div>
          </div>
          <br>
          <form method="post" action="<?php echo HTTP_ROOT?>Homes/saveDirectForm" enctype="multipart/form-data">
          <?php $i=1; $j=1;$cnt = count($order['OrderMeta']);  foreach($order['OrderMeta'] as $ord){?>
          
            <?php if(@$ord['CollegeCourse']['direct_apply'] == 1){ ?>
            
              <div class="row" id="row-id-<?php echo $ord['CollegeCourse']['id']?>">
            <div class="col-md-1"></div>
            <div class="col-md-5"><?php echo $ord['College']['name']?></div>
            
            <div class="col-md-2"><div width="100%" class="Price">Direct Applied</div></div>
            <div class="col-md-2"><div width="100%" class="Price">
            <?php if(empty($ord['DirectApplication']['application_no'])){?>
            <input type="text" class="form-control" Placeholder="Application No." name="data[<?php echo $i;?>][DirectApplication][application_no]">
            
            <?php $j++;}else{ ?>
              <input type="hidden" class="form-control" Placeholder="Application No." name="data[<?php echo $i;?>][DirectApplication][id]" value="<?php echo @$ord['DirectApplication']['id']?>">
              <input type="hidden" class="form-control" Placeholder="Application No." name="data[<?php echo $i;?>][DirectApplication][application_no]" value="<?php echo @$ord['DirectApplication']['application_no']?>">
              <?php echo $ord['DirectApplication']['application_no']?>
             <?php }?> 
            <input type="hidden" class="form-control" Placeholder="Application No." name="data[<?php echo $i;?>][DirectApplication][order_meta_id]" value="<?php echo $ord['id'];?>">
            <input type="hidden" class="form-control" Placeholder="Application No." name="data[<?php echo $i;?>][DirectApplication][order_id]" value="<?php echo $order['Order']['id'];?>">
            </div></div>
            <?php if(empty($ord['DirectApplication']['image'])){?>
            <div class="col-md-2"><div width="100%" class="Price"><input type="file" name="file[<?php echo $i;?>]"></div></div>
            <?php $j++;}else{?>
              file Uploaded
             <?php }?> 
            <?php }?>
              
          </div>
          <br>
          <?php $i++;  }?>
          <?php if($j != 1){?>
          <button type="submit" class="btn btn-primary pull-right">Submit Reciepts</button>
          <?php }?>
          </form>
        
          </div>
          <!-- </form> -->
          <!-- /.box -->
    
        <!-- /.col -->
      </div>
      <div>
      <?php echo $this->element('frontEnd/applicationdetails');?>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
              </div>
      </div>
    </div>
  </div>
</section>