<section style="background:#ECECEC;" class="p-tb-50">
	<div class="container">
	<div class="row" >
  			
      		<div class="col-md-12" style="background:#fff;">

					<div id="sec0" class="container-fluid ">
					  <h3 class="privacyhead m-t-10"><?php echo $terms['Term']['title'];?></h3>
					</div>  
					<?php echo $terms['Term']['content'];?>
			</div>
  	</div>
</div>
</section>
<script type="text/javascript">
	$('#sidebar').affix({
		      offset: { 
		      	top: 230,
		      	bottom: 580
		      }
		});

		var $body   = $(document.body);
		var navHeight = $('.navbar').outerHeight(true) + 20;

		$body.scrollspy({
			target: '#leftCol',
			offset: navHeight
		});
</script>