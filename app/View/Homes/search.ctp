<style>
.select2{
  width:100% !important;
}
.margn_left15 {
    min-width: 166px !important;
    margin-right: 7% !important;
}
.about-doctor p{
  margin-bottom: 0px !important;
}

</style>
<section id="section-content" class="content page-1 moto-section bg_gray" data-widget="section" data-container="section">
<div class="header_sec-img">
               <div class="container">
                  <div class="search_h">
                     <div class="row">
                        <div class="col-md-2">
                           <div class="button-group">
                              <button type="button" class="btn btn-default btn-sm dropdown-toggle full_width" data-toggle="dropdown">Location <span class="caret"></span></button>
                              <ul class="dropdown-menu margn_left15" id="alllocation">
                                  <li><a href="javascript:void(0);" class="small"><input type="checkbox" id="checkall-location" class="">&nbsp; Select All</a></li>
                                  <?php 
                                   
                                    foreach($states as $state){?>
                                    <li><a href="javascript:void(0);" class="small" data-value="option2" tabIndex="-1"><input name="loc" type="checkbox" value="<?php echo @$state['State']['id']?>" <?php echo @$loc_id == $state['State']['id'] ? 'checked' : '';?> class="srch-page-location"/>&nbsp; <?php echo $state['State']['statename']?></a></li>
                                  <?php }?>
                                 
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2">
                           <div class="button-group">
                              <button type="button" class="btn btn-default btn-sm dropdown-toggle full_width" data-toggle="dropdown">Courses <span class="caret"></span></button>
                              <ul class="dropdown-menu margn_left15" id="allcourse">
                                <li><a href="javascript:void(0);" class="small"><input type="checkbox" id="checkall-course" class="">&nbsp; Select All</a></li>
                                <?php   
                                @$crs = explode('-', @$_GET['course']);
                                @$crs_cnt = count(@$crs);
                                @$crs_id = @$crs[@$crs_cnt-1];

                                foreach($courses as $course){?>
                                  <li><a href="javascript:void(0);" class="small" data-value="option1" tabIndex="-1"><input name="cou" type="checkbox" class="course" value="<?php echo @$course['Course']['id']?>" <?php echo @$crs_id == @$course['Course']['id'] ? 'checked' : '';?>/>&nbsp; <?php echo @$course['Course']['course']?></a></li>
                                <?php }?>
                                
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2">
                           <div class="button-group">
                              <button type="button" class="btn btn-default btn-sm dropdown-toggle full_width" data-toggle="dropdown">Recognition <span class="caret"></span></button>
                              <ul class="dropdown-menu margn_left15" id="allrec">
                                <li><a href="javascript:void(0);" class="small"><input type="checkbox" id="checkall-rec" class="">&nbsp; Select All</a></li>
                                 <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input name="rec" type="checkbox" class="recognition" value="0"/>&nbsp; government </a></li>
                                 <li><a href="#" class="small" data-value="option2" tabIndex="-1"><input name="rec" type="checkbox" class="recognition" value="1"/>&nbsp; Non-government </a></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2">
                           <div class="button-group">
                              <button type="button" class="btn btn-default btn-sm dropdown-toggle full_width" data-toggle="dropdown">Exam <span class="caret"></span></button>
                              <ul class="dropdown-menu margn_left15" id="allfac">
                                <li><a href="javascript:void(0);" class="small"><input type="checkbox" id="checkall-exam" class="">&nbsp; Select All</a></li>
                                <?php foreach($exams as $exam){?>
                                 <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input name="exam" class="exam" type="checkbox" value="<?php echo $exam['Exam']['id']?>"/>&nbsp; <?php echo $exam['Exam']['exam']?></a></li>
                                <?php }?> 
                                 
                              </ul>
                           </div>
                        </div>
                         <div class="col-md-2">
                           <div class="button-group">
                              <button type="button" class="btn btn-default btn-sm dropdown-toggle full_width" data-toggle="dropdown">Facilities <span class="caret"></span></button>
                              <ul class="dropdown-menu margn_left15" id="allfac">
                                <li><a href="javascript:void(0);" class="small"><input type="checkbox" id="checkall-fac" class="">&nbsp; Select All</a></li>
                                <?php foreach($facilities as $facility){?>
                                 <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input name="fac" class="facility" type="checkbox" value="<?php echo $facility['Facility']['id']?>"/>&nbsp; <?php echo $facility['Facility']['name']?></a></li>
                                <?php }?> 
                                 
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2">
                           <div class="button-group">
                              <button type="button" class="fee-filter btn btn-default btn-sm dropdown-toggle full_width <?php echo empty(@$_GET['course']) ? 'disabled' : '';?>" data-toggle="dropdown">Fees <span class="caret"></span></button>
                              <ul class="dropdown-menu margn_left15">
                                <li class="col-md-6">
                                  <select class="form-control fee-min">
                                    <option value="">Min</option>
                                    <option value="10000">10000</option>
                                    <option value="20000">20000</option>
                                    <option value="30000">30000</option>
                                    <option value="40000">40000</option>
                                    <option value="50000">50000</option>
                                  </select>
                                  </li>
                                  <li class="col-md-6">
                                  <select class="form-control fee-max" >
                                    <option value="">Max</option>
                                    <option value="20000">20000</option>
                                    <option value="30000">30000</option>
                                    <option value="40000">40000</option>
                                    <option value="50000">50000</option>
                                    <option value="60000">60000</option>
                                  </select>
                                </li>
                                
                                 
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- filter section end here -->           
            <div class="container">
               <div class="row">
                  <div class="col-md-9 col-sm-9 result_box">
                     <div class="row">
                        <div class="col-md-8 col-sm-8">
                           <div class="found-result">
                              <p><i class="fa fa-caret-right"></i> 1024 matches found for <strong>College</strong> in Delhi</p>
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                           <select class="short_by sort-fee">
                              <option value="">Sort by</option>
                              <option value="1">Price - High to low</option>
                              <option value="2">Price - Low to high</option>
                           </select>
                        </div>
                     </div>
                     <div class="border_box">
                        <div class="">
                           <div class="col-md-12 col-sm-12 append-result" >
                            <?php if(!empty($colleges)){
                                    foreach ($colleges as $college){
                                       
                            ?>
                                      <div class="doctor-section">
                                         <div class="col-md-3 col-sm-3">
                                            <div class="doctor-photo" college-id="<?php echo $college['College']['id']?>" college-name="<?php echo $college['College']['name']?>"> <a  href="javascript:void(0);" ><img src="<?php echo HTTP_ROOT.'img/collegeImages/large/'.$college['College']['image']?>" class="img-responsive wow bounceInDown"></a> </div>
                                            <div class="margntopbtm">
                                               <div class="add_compair"><a class="colg-cmp" href="javascript:void(0)" id="<?php echo $college['College']['id']?>" style="<?php echo @in_array($college['College']['id'], $this->Session->read('clgCompare')) ? 'color:blue' : '';?>" ><i class="fa fa-exchange"></i> <?php echo @in_array($college['College']['id'], $this->Session->read('clgCompare')) ? 'Added to Compare' : 'Add to Compare';?></a></div>
                                               <div class="add_compair">
                                               <?php if(!empty($crs_id)){?>
                                                <a target="_blank" href="<?php 
                                                              foreach($college['CollegeCourse'] as $clgcrs){
                                                                if($clgcrs['course_id'] == $crs_id){
                                                                    echo HTTP_ROOT.'Homes/downloadBrochure/'.$clgcrs['brochure'];
                                                                }
                                                              }
                                                        ?>"><i class="fa fa-arrow-circle-down"></i> Download Brochure</a>
                                                <?php }?>        
                                              </div>
                                            </div>
                                         </div>
                                         <div class="col-md-6 col-sm-6">
                                            <div class="about-doctor">
                                               <div class="misc-title" style="margin-bottom:0px;">
                                                  <a href="<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>" style="margin: 0px;" target="_blank"><?=@$college['College']['name']?></a>
                                               </div>
                                               <?php if($this->Session->check('selectedCourse')){?>
                                               <p class="margin0">
                                                 <?php if(!empty($college['CollegeCourse'])){
                                                          $crsarray = [];
                                                          $len = count($college['CollegeCourse']);
                                                          $i = 1;
                                                          foreach($college['CollegeCourse'] as $course){
                                                            if($course['course_id'] == $this->Session->read('selectedCourse') && !in_array($course['course_id'], $crsarray)){
                                                              echo @$course['Course']['course'];
                                                              $crsarray[] = $course['course_id'];
                                                                if($len > $i)
                                                                    echo "";
                                                                      
                                                                $i++;
                                                              }
                                                          }
                                                      }else{
                                                        echo "No course found.";
                                                      }  
                                                 ?>
                                               </p>
                                                <p><b>Application Fees</b>: <span class="ori-fee"><?php echo @$college['CollegeCourse']['0']['registration_fee']?></span><span class="dis-fee dis-fee_1"> <?php echo @$college['CollegeCourse']['0']['formsadda_cost']?></span></p>
                                               <p ><b>Exams accepted</b>: 
                                               <?php foreach($college['CollegeCourse'] as $crse){
                                                    foreach($crse['CourseExam'] as $crsexam){
                                                     echo  @$crsexam['Exam']['exam'].',';
                                                    }
                                                  }?></p>
                                               <?php }?>
                                               <ul class="collage_faclity">
                                                <?php if(!empty($college['CollegeFacility'])){
                                                        foreach($college['CollegeFacility'] as $fac){
                                                ?>
                                                  <li>
                                                     <img src="<?php echo HTTP_ROOT.'img/facility/';?><?php echo empty($fac['Facility']['icon']) ? "default.jpg" : $fac['Facility']['icon'];?>" data-toggle="tooltip" title="<?php echo $fac['Facility']['name']?>" width="20">
                                                  </li>
                                                <?php   }
                                                      }
                                                ?>  
                                                  
                                               </ul>
                                              <?php if($this->Session->check('selectedCourse')){?>
                                             <p ><b>Last Apply Date</b>: 
                                             <?php 
                                             $date=date_create(@$college['CollegeCourse']['0']['to_date']);
                                             echo DATE_FORMAT($date,'d M Y')?></p>
                                             <?php }?>
                                            </div>
                                         </div>
                                         <div class="col-md-3 col-sm-3">
                                            <div class="time-table">
                                               <ul>
                                                  <li>
                                                     <div class="tm-desc">
                                                        <p style="margin-top: 0px;"><span class="green"><i class="fa fa-thumbs-up ft-10"></i> 97%</span> <sub>301 votes</sub></p>
                                                     </div>
                                                  </li>
                                                  <li>
                                                     <div class="tm-desc">
                                                        <p><a target="_blank" href="<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>"><i class="fa fa-commenting"></i> <?php echo count(@$college['Review']);?> Review</a></p>
                                                     </div>
                                                  </li>
                                                  <li>
                                                     <div class="tm-desc">
                                                        <p><a href="#"><i class="fa fa-map-marker"></i> <?=@$college['College']['address']?></a></p>
                                                     </div>
                                                  </li>
                                                  <?php if(!empty($crs_id)){?>
                                                  <li>
                                                     <div class="tm-desc">
                                                        <p><i class="fa fa-inr"></i>
                                                        Course Fee : 
                                                        <?php 
                                                              foreach($college['CollegeCourse'] as $clgcrs){
                                                                if($clgcrs['course_id'] == $crs_id){
                                                                    echo $clgcrs['fees'];
                                                                }
                                                              }
                                                        ?>
                                                        </p>
                                                     </div>
                                                  </li>
                                                  <?php }?>
                                                  <li>
                                                     <div class="tm-desc">
                                                        <p><i class="fa fa-line-chart"></i> 1 Application Sold</p>
                                                        <p class="green">1 Application sold in last 7 days</p>
                                                     </div>
                                                  </li>
                                                  <?php 
                                                   if(@$college['CollegeCourse']['0']['from_date'] <= date('Y-m-d') && @$college['CollegeCourse']['0']['to_date'] >= date('Y-m-d')){?>
                                                    <!-- <button data-college-id="<?php echo @$college['College']['id']?>" data-course-id="<?php echo @$college['CollegeCourse']['0']['id']?>" class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm add-cart">Apply</button> -->
                                                  <?php }else{?>
                                                    <!-- <button class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm" data-toggle="modal" data-target="#notifyModal">Notify me</button> -->
                                                  <?php }?>  
                                               </ul>
                                            </div>
                                         </div>
                                      </div>
                            <?php    }
                                  }                            
                            ?>  
                              
                           </div>
                        </div>
                        <img style='margin-left: 50%;margin-top: 6%;display:none;' class='loader' src='https://www.pavoterservices.pa.gov/images/loader.gif'>
                        <!--=========end of 1 tab==========-->
                        <!-- <ul class="pagination pull-right">
                           <li class="disabled"><a href="#">&laquo;</a></li>
                           <li class="active"><a href="#">1</a></li>
                           <li><a href="#">2</a></li>
                           <li><a href="#">3</a></li>
                           <li><a href="#">4</a></li>
                           <li><a href="#">5</a></li>
                           <li><a href="#">&raquo;</a></li>
                        </ul> -->
                     </div>

                        
                  </div>
                  <div class="col-md-3 col-sm-3 advert">
                     <img src="<?php echo HTTP_ROOT?>frontend/ad3.jpg" class="img-responsive">
                     <img src="<?php echo HTTP_ROOT?>frontend/ad2.jpg" class="img-responsive" style="margin-top: 15px;">
                     <img style='margin-bottom: 10%;margin-top: 15px;' src="<?php echo HTTP_ROOT?>frontend/ad3.jpg" class="img-responsive">
                  </div>
               </div>
            </div>
  
             <script type="text/javascript">
         $(window).scroll(function() {    
            var scroll = $(window).scrollTop();
            if (scroll >= 100) {
                $(".chng_logo").addClass("smaller");
            }
            else  {
                $(".chng_logo").removeClass("smaller");
            }
         });
      </script>
      <script>
         $(document).ready(function() {
             $(".search-main-btn").hover(function() {
                 $(".srch-inner").show(800);
                 $(this).hide(600);
                 $(".bx-caption").hide(600);
             })
             $(".close-btn").click(function() {
                 $(".srch-inner").hide(800);
                 $(".search-main-btn").show(600);
                 $(".bx-caption").show(600);
             })
            
         });
      </script>
      <script>
         $(document).ready(function(){
             $('[data-toggle="tooltip"]').tooltip();   
         });
          $(window).scroll(function(){
               if ($(this).scrollTop() > 80) {
                   $('.search_h').addClass('fixed');
               } else {
                   $('.search_h').removeClass('fixed');
               }
           });
      </script>
      <script>
      let offset = 3;
      let course = [];
      let loca = [];
      let facility = [];
      let minfee = 0;
      let maxfee = 100000000;
      let sort;
      let exam=[];
      let checkImage = true;
      <?php if($this->Session->check('selectedCourse')){?>
      course.push(<?php echo $this->Session->read('selectedCourse');?>);
      <?php }?>
      <?php if(!empty($_GET['course'])){ ?>
          course.push(<?php echo $crs_id?>);
      <?php }?> 
       <?php if(!empty($_GET['location'])){ ?>
          loca.push(<?php echo $loc_id?>);
      <?php }?>  
      let college_type = [];
       $(document).ready(function(){
      $('#checkall-location').on('click', function() {
        loca = [];
        if (this.checked == true){
              
              $('.srch-page-location').each(function(){
                $(this).prop('checked',true);
                loca.push($(this).val());
              })
            }else{
                  $('.srch-page-location').each(function(){
                    $(this).prop('checked',false);
                    //loca.push($(this).val());
                  })
                }  
          
        
        
          getdata();
      });
      $('#checkall-course').on('click', function() {
        course = [];
        if (this.checked == true){
              
              $('.course').each(function(){
                $(this).prop('checked',true);
                course.push($(this).val());
              })
              $('.fee-filter').removeClass('disabled');
            }else{
                  $('.course').each(function(){
                    $(this).prop('checked',false);
                    
                    //loca.push($(this).val());
                  })
                  $('.fee-filter').addClass('disabled');
                }  
          
        
        
          getdata();
      });
      $('#checkall-rec').on('click', function() {
        college_type = [];
        if (this.checked == true){
              
              $('.recognition').each(function(){
                $(this).prop('checked',true);
                college_type.push($(this).val());
              })
            }else{
                  $('.recognition').each(function(){
                    $(this).prop('checked',false);
                    
                    //loca.push($(this).val());
                  })
                } 
          getdata();
      });
      $('#checkall-fac').on('click', function() {
        facility = [];
        if (this.checked == true){
              
              $('.facility').each(function(){
                $(this).prop('checked',true);
                facility.push($(this).val());
              })
            }else{
                  $('.facility').each(function(){
                    $(this).prop('checked',false);
                    
                    //loca.push($(this).val());
                  })
                } 
          getdata();
      
     }); 
     $('#checkall-exam').on('click', function() {
        exam = [];
        if (this.checked == true){
              
              $('.exam').each(function(){
                $(this).prop('checked',true);
                exam.push($(this).val());
              })
            }else{
                  $('.exam').each(function(){
                    $(this).prop('checked',false);
                    
                    //loca.push($(this).val());
                  })
                } 
          getdata();
      
     });
     });  
      $(document).ready(function(){

        $('.sort-fee').on('change',function(){
          let val = $(this).val();  
          if(val == '1')
            sort = 'desc';   
          else    
            sort = 'asc';      
          getdata();
        });

        $('.fee-min').on('change',function(){
          minfee = 0;
          minfee = $(this).val();          
          getdata();
        });
        $('.fee-max').on('change',function(){
          maxfee = 0;
          maxfee = $(this).val();          
          getdata();
        });
       
        $('.srch-page-location').on('click',function(){
          loca = [];
          $('.srch-page-location').each(function(){
            if($(this).is(':checked'))
              loca.push($(this).val());
          });
        
          getdata();
        });
        $('.course').on('click',function(){
          course = [];
          $('.course').each(function(){
            if($(this).is(':checked')){
              course.push($(this).val());
              $('.fee-filter').removeClass('disabled');
            }
          });
          getdata();
        });
        $('.exam').on('click',function(){
          exam = [];
          $('.exam').each(function(){
            if($(this).is(':checked')){
              exam.push($(this).val());
              
            }
          });
          getdata();
        });
        $('.facility').on('click',function(){
          facility = [];
          $('.facility').each(function(){
            if($(this).is(':checked'))
              facility.push($(this).val());
          });
          getdata();
        });
        $('.recognition').on('click',function(){
          college_type = [];
          $('.recognition').each(function(){
            if($(this).is(':checked'))
              college_type.push($(this).val());
          });
          getdata();
        });
      });
      $(window).scroll(function() {

          
          if($(window).scrollTop() + $(window).height() > $('#section-content').height()) {
            if(checkImage)
              $('.loader').show();
           $.ajax({
                    type : 'post',
                    data : {  
                            offset : offset,
                            facility : facility,
                            course : course,
                            college_type : college_type,
                            location : loca,
                            minfee : minfee,
                            maxfee : maxfee,
                            sort : sort,
                            exams:exam
                          },
                    url : '<?php echo HTTP_ROOT?>Homes/searchPagination',
                    success : function(response){
                         //console.log(response);
                         //console.log(checkImage);
                          if(response)
                             checkImage = false;
                          else 
                              checkImage = true; 
                          //console.log(checkImage);
                          $('.loader').hide();
                          $('.append-result').append(response);
                         },
                  async: true    
                });
          offset += 3;
                       
        }     
      });
      function getdata(){
        if(course == '')
          $('.fee-filter').addClass('disabled');
        else
          $('.fee-filter').removeClass('disabled');
        $.ajax({
                    type : 'post',
                    
                    data : {  
                            
                            facility : facility,
                            course : course,
                            college_type : college_type,
                            location : loca,
                            minfee : minfee,
                            maxfee : maxfee,
                            sort : sort,
                            exams:exam
                          },
                    url : '<?php echo HTTP_ROOT?>Homes/searchPagination',
                    success : function(response){
                          //console.log(response);
                          $('.append-result').html(response);
                         },
                  async: true    
                });
          offset = 3;
      }        
    
      </script>