<style>
.txt-btn{
	width: 85%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
}
.faq-head > p{
	color:#fff !important;
}
strong
{
	/*color: white!important;*/
}
</style>
<section style="background:#ECECEC;" class="p-lr-50 p-tb-20">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="background:#fff;">
				<div class="col-md-6">
					<h4 class="m-tb-25 faqheads">Frequently Asked Questions</h4>
				</div>
				<div class="col-md-6">
					<form method="get" action="">
						<input type="text" style="margin-top: 3%;" class="txt-btn" name="keyword" placeholder="Search By Topic" value="<?php echo @$keyword;?>">
						<button type="submit" class="btn btn-primary">Go</button>
					</form>
				</div>
				<div class="col-md-12">
				<?php foreach($faqs as $faq){?>
				<div class="panel panel-custom">
				    <div id="headingOne" role="tab" class="panel-heading">
				        <h4 class="panel-title panel-title-cstm">
				            <a aria-controls="collapse<?php echo $faq['Faq']['id']?>" aria-expanded="false" href="#collapse<?php echo $faq['Faq']['id']?>" data-parent="#accordion" data-toggle="collapse" class="collapsed">
				                <span class="faq-head" style="color:#fff !important;"><?php echo $faq['Faq']['question'];?></span>
				                <i class="indicator glyphicon glyphicon-plus pull-right" style="margin-top: -2%;"></i>
				            </a>
				        </h4>
				    </div>
				    <div aria-labelledby="headingOne" role="tabpanel" class="panel-collapse collapse" id="collapse<?php echo $faq['Faq']['id']?>" aria-expanded="false" style="height: 0px;">
				        <div class="panel-body">
				           <?php echo $faq['Faq']['answer'];?>
				        </div>
				    </div>
				</div>
				<?php }?>
				</div>
			</div>
		</div>
	</div>
</section>
	<script type="text/javascript">
		$(document).on("hide.bs.collapse show.bs.collapse", ".collapse", function (event) {
    $(this).prev().find(".glyphicon").toggleClass("glyphicon-plus glyphicon-minus");
    $(this).prev().find("span.pull-right.text-muted").toggleClass("expandir fechar");
    event.stopPropagation();
});
					    </script>