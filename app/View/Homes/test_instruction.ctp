<style>
  .ml5{
    margin-left: 5px;
  }
</style>
<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
  <div class="p-lr-50 p-tb-20">
    <div class="row">
      <?php echo $this->element('frontEnd/studentsidebar');?>
      <div class="col-md-9" style="background:#fff">
      <?php if(!empty($test['Test']['id'])){?>
      <?php if(!@$testTaken){?>
        <div class="profile-content">
          <div class="col-md-12">
            <div class="col-md-12 text-center">
              <h2>Instructions</h2>
            </div>
            
          </div>
          <div class="col-md-12">
            <label>Test Time : <?php echo $test['Test']['hour'].' hrs '.$test['Test']['minute'].' mins'?></label>
          </div>
          <div class="col-md-12">
           <!-- <h3>Instructions :</h3> -->
            <p><?php //echo @$test['Test']['instruction']?>
																				  <br />
			<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" id="m_5599073061881841407gmail-Table10" >
			<tbody>
			<tr>
			<td width="100%" align="left" ><br />
			Get College .IN Scholarship Test (National Level)</td>
			</tr>

			<tr>
			
			<td width="100%" align="left" ><br />
			Read the <a href="https://www.getcollege.in/Homes/terms" target="_blank">Terms & Conditions</a> and the instructions below prior to taking the test.</td>
			</tr>

			</tbody>
			</table>

			<br />
			<table cellpadding="4" cellspacing="1" border="1" width="100%" >
			<tbody>
			<tr>
			<td align="left" width="5%" > </td>
			<td width="95%" align="left" >This test will cover the following topics:</td>
			</tr>

			<tr>
			<td align="left" width="5%" > </td>
			<td width="95%" align="left" ><ul>
			<li>Spelling</li>
			</ul>

			<b>Who Should Take This Test:</b><br />
			<br />
			The US English Spelling Test is designed for both native English speakers and non-native speakers at the intermediate to advanced level.<br />
			<br />
			Non-native speakers at the beginning level may find this test too challenging and may prefer to take the U.S. English Basic Skills Test for Non-native Speakers.<br />
			<br />
			<b>What the Test Is About:</b><br />
			<br />
			The questions on this multiple-choice test cover tricky spelling patterns and commonly misspelled words.</td>
			</tr>

			<tr>
			<td align="left" width="5%" > </td>
			<td width="95%" align="left" >Duration</td>
			</tr>

			<tr>
			<td align="left" width="5%" > </td>
			<td width="95%" align="left" >The test consists of 30 multiple choice questions and the duration is approximately 20 minutes.<br />
			<br />
			</td>
			</tr>

			<tr>
			<td align="left" width="5%" > </td>
			<td width="95%" align="left" >Instructions:</td>
			</tr>

			<tr>
			<td align="left" width="5%" > </td>
			<td width="95%" align="left" ><ul>
			<li>Each question has between 2 and 8 options. One or more answers may be correct.<br />
			<br />
			</li>
			<li>Attempt all questions since there are no penalties for incorrect answers.<br />
			<br />
			</li>
			<li>Each question must be answered in the time limit shown at the top of the test window.<br />
			<br />
			</li>
			<li>The test is best viewed using Internet Explorer 6.0+, Mozilla Firefox 2.0+ or Google Chrome.<br />
			<br />
			</li>
			<li>You must answer each question before proceeding to the next question. You will not be able to change an answer once you've moved to the next question.<br />
			<br />
			</li>
			<li>If you leave the test midway through your session, you can resume it where you left off by returning to this page.<br />
			</li>
			</ul>

			</td>
			</tr>

			</tbody>
			</table>

			
			
			</p>
          </div>
          <div class="col-md-12">
            <button class="btn btn-primary" id="start-test">Start Test</button>
          </div>
        </div>
        <?php }else{?>
          <div class="profile-content">
          <?php if($checkTest['Student']['test_taken'] <= 0){?>
          <div class="col-md-12">
            <a class="btn btn-primary" href="<?php echo HTTP_ROOT.'Homes/testAgain'?>">Take Test Again</a>

          </div>
          <?php }else{?>
            <div class="col-md-12">
              <h2>If you are willing to take more test,you have to pay Rs.50 per test,</h2>
            </div>
            <div class="col-md-12">
             <!-- <a class="btn btn-primary" href="<?php echo HTTP_ROOT.'Homes/payForTest'?>">Take Test Again</a> -->

          </div>
          <?php }?>  
          </div>
        <?php  }?>
        <?php }else{?>
          <div class="profile-content">
          
            <div class="col-md-12">
              <button class="btn btn-primary">No Test Found</button>

            </div>
          </div>  
         <?php }?> 
      </div>
    </div>
  </div>
</section>
<style type="text/css">
.hidediv
{
  display:none;
}
</style>
<script>
$(function() {
    $( ".addsbutton" ).click(function() {
        $( ".hidediv" ).slideToggle('slow','swing');
    });
});
  var win;
  $('#start-test').on('click',function(){
    //window.open();
    win = window.open("<?php echo HTTP_ROOT.'Homes/test/'.base64_encode($test['Test']['id']).'/'.$testType;?>","_blank",12,'scrollbars=1,menubar=0,resizable=1,width=850,height=500');
    window.location.reload();
      //win.close();
    
  });

// Set the date we're counting down to
//var countDownDate = new Date(":<?php //echo $test['Test']['minute']?>:00").getTime();

/**/
</script>