    <div class="wrapper">
        
        <section class="slider">
            <div class="fslider">
              <div class="container-fluid">
                <div class="row">
                  <div class="flexslider">
                    <ul class="slides">
                      <?php foreach($slides as $slide){?>
                      <li>
                        <img src="<?php echo HTTP_ROOT?>/img/gallery/<?php echo $slide['Slider']['image']?>" />
                        <!-- <div class="college-name">
                          <a href="">Photographer's Name</a>
                        </div> -->
                      </li>
                      <?php }?>

                      
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="search-wrap">
              <div class="container">
                <div class="row">
                  <div class="col-md-10 col-md-offset-1">
                    <div class="search-main">
                      <h2 class="type-text"></h2>
                      <h4>Search . Explore . Find Best College in India !</h4>
                      <!-- Nav tabs -->
                      <!-- <ul class="nav nav-tabs nav-tabs-cstm" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Photographer</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Videographer</a></li>
                      </ul>
 -->
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                          <form>
                            <div class="select-wrap">
                            <span class="lnr lnr-apartment select-wrap-span"></span>
                              <select id="source">
                                  <option selected="selected" value="IN">College</option>
                                  <!-- <option value="IN">Videographer</option> -->
                              </select>
                              <span class="lnr lnr-chevron-down select-chevron"></span>
                            </div>
                            <div class="input-wrap location-input">
                              <span class="lnr lnr-map-marker"></span>
                              <input type="text" name="cities" class="form-control" id="autocomplete" placeholder="Select Your Location">
                            </div>
                            <div class="input-wrap categ-input">
                              <span class="lnr lnr-list"></span>
                              <input type="text" name="category" class="form-control" id="autocomplete2" placeholder="Select a Category">
                            </div>
                            <button type="submit" class="btn btn-default"><span class="lnr lnr-magnifier"></span></button>
                          </form>
                          <ul class="srch-suggestion">
                            <li><a href="">Top Searches: </a></li>
                            <li><a href="">Top Colleges Delhi ,</a></li>
                            <li><a href="">Gurgaon ,</a></li>
                            <li><a href="">Noida ,</a></li>
                            <li><a href="">Mayur Vihar ,</a></li>
                            <li><a href="">Ludhiana ,</a></li>
                            <li><a href="">Jalandhar ,</a></li>
                            <li><a href="">Chandigarh</a></li>
                          </ul>
                        </div>
                        <!-- <div role="tabpanel" class="tab-pane" id="profile">
                          <form>
                            <div class="select-wrap">
                              <span class="lnr lnr-chevron-down"></span>
                              <select id="source">
                                  <option selected="selected" value="IN">Music Videos</option>
                              </select>
                            </div>
                            <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Search Video Directors or Videos!">
                            <button type="submit" class="btn btn-default"><span class="lnr lnr-magnifier"></span></button>
                          </form>
                        </div> -->
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="social-media">
              <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width=79&layout=button_count&action=like&size=small&show_faces=false&share=false&height=21&appId" width="79" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
              &nbsp;
              <iframe src="https://platform.twitter.com/widgets/follow_button.html?screen_name=TwitterDev&show_screen_name=false&show_count=false&size=0" title="Follow TwitterDev on Twitter" width="65" height="20" style="border: 0; overflow: hidden;"></iframe>
            </div> -->
        </section>
        <section class="description-sec">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1 text-center m-tb-15">
                <div class="col-md-6">
                  <div class="section-headings">
                    <h2 class="m-b-10">Current News</h2>
                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p> -->
                    <div class="heading-border"></div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="section-headings">
                    <h2 class="m-b-10">About GetCollege.IN</h2>
                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p> -->
                    <div class="heading-border"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="row">
                  <div class="col-md-6">
                    <div class="about-in-wrap">
                      <ul>
                        <marquee direction="up" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();" style="height: 300px;" height="300">
                          <li><p class="date" style="color: #ED8D38;"><b>12/07/2017</b></p><a href="">Explore what all is going on in education sector. Current notification of Scholarship, Test Olympiad, NTSE.</a>
                          </li>
                          <li><br></li>
                          <li><!--<p class="date">05/04/2017</p>--><a href="">Notification of IIT-JEE, NEET and all State Entrance Exams.</a>
                          </li>
                          <li><br></li>
                          <li><!--<p class="date">05/04/2017</p>--><a href="https://scholarship.up.nic.in/" target="_blank">Uttar Pradesh UP Scholarship Schemes 2017 for Class 9th, 10th, 11th and 12th | Apply Online for Uttar Pradesh UG, PG, Pre and Post Matric Scholarships Online Form</a>
                          </li>
                          <li><br></li>
                          <li><!--<p class="date">05/04/2017</p>--><a href="#" >AIIMS MBBS 2017 Application Form, Exam Date</a>
                          </li>
                          <li><br></li>
                          <li><!--<p class="date">05/04/2017</p>--><a href="#" >JIPMER MBBS 2017 Application Form, Exam Dates</a>
                          </li>
                          <li><br></li>
                          <li><!--<p class="date">05/04/2017</p>--><a href="#" >Class 10th 12th Exam Time Table 2018 / Board Exam Date Sheet</a>
                          </li>
                          <li><br></li>
                          <li><!--<p class="date">05/04/2017</p>--><a href="#" >UP Board Date Sheet 2018 | Time Table for 12th Class Exams</a>
                          </li>
                          </marquee>
                      </ul>
                      <a href="" class="btn btn-view">Know more</a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="about-in-wrap">
                      <iframe width="560" height="315" src="https://www.youtube.com/embed/vqkz9wEbIPI" frameborder="0" allowfullscreen></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>
    <section class="featured-section parallax">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 text-center m-tb-15">
            <div class="social-media-headings heading-featured">
              <h2 class="m-b-10">College Program</h2>
              <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p> -->
              <div class="heading-border"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="career-wrap">
              <!-- Nav tabs -->
              <div class="nav-tabs-wrap m-b-30 m-t-20">
                <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                  
                  <li role="presentation" class="active"><a href="#wed" aria-controls="wed" role="tab" data-toggle="tab">
                    <i class="fa fa-wrench" style="font-size: 500%" aria-hidden="true"></i>
                  Engineering</a></li>
                  <li role="presentation" ><a href="#nature" aria-controls="wed" role="tab" data-toggle="tab">
                    <i class="fa fa-briefcase" style="font-size: 500%" aria-hidden="true"></i>
                  Managements</a></li>
                  <li role="presentation"><a href="#fashion" aria-controls="fashion" role="tab" data-toggle="tab">
                  <i class="fa fa-money" style="font-size: 500%" aria-hidden="true"></i>
                  Commerce</a></li>
                  <li role="presentation"><a href="#babies" aria-controls="fashion" role="tab" data-toggle="tab">
                  <i class="fa fa-heartbeat" style="font-size: 500%" aria-hidden="true"></i>
                  Medical</a></li>
                  
                  
                  
                </ul>
              </div>
              <div class="tab-content-wrap">
                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade in active" id="wed">
                    <div class="featured-wrapper">
                      <?php if(!empty($engineeringColleges)){ foreach ($engineeringColleges as $college1){ ?>
                      <div class="featured">
                        <a href="<?php echo HTTP_ROOT?>college/<?=@$college1['College']['name'].'-'.$college1['College']['id']?>" target="_blank" title="<?php echo @$college1['College']['name'];?>">
                          <img title="" src="<?php echo HTTP_ROOT.'img/collegeImages/thumbnail/'?><?php echo empty($college1['College']['image']) ? 'college-default.jpg' : $college1['College']['image'];?>">
                          <div class="caption">
                            <small><?php echo empty(@$college1['College']['short_name']) ? substr(@$college1['College']['name'],0,10) : @$college1['College']['short_name'];?></small>
                            <!--LIKES Removed-->
                            <!-- <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                            <span class="lnr lnr-thumbs-up pull-right"></span>  -->
                          </div>
                        </a>
                      </div>
                      <?php } }?>
                      <!-- <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/centurian.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/christ.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/iitrorkee.jpg">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/lpu.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/amity.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/centurian.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/christ.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div> -->
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="fashion">
                    <div class="featured-wrapper">
                      <?php if(!empty($managementColleges)){ foreach ($managementColleges as $college2){ ?>
                      <div class="featured">
                        <a href="<?php echo HTTP_ROOT?>college/<?=@$college2['College']['name'].'-'.$college2['College']['id']?>" target="_blank" title="<?php echo @$college2['College']['name'];?>">
                          <img title="" src="<?php echo HTTP_ROOT.'img/collegeImages/thumbnail/'?><?php echo empty($college2['College']['image']) ? 'college-default.jpg' : $college2['College']['image'];?>">
                          <div class="caption">
                            <small><?php echo empty(@$college2['College']['short_name']) ? substr(@$college2['College']['name'],0,10) : @$college2['College']['short_name'];?></small>
                            <!--Removed Likes-->
                            <!-- <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                            <span class="lnr lnr-thumbs-up pull-right"></span>  -->
                          </div>
                        </a>
                      </div>
                      <?php } }?>
                      <!-- <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/amity.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/centurian.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/christ.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/iitrorkee.jpg">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/lpu.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/amity.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/centurian.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/christ.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div> -->
                    </div>
                  </div>  
                  <div role="tabpanel" class="tab-pane fade" id="nature">
                    <div class="featured-wrapper">
                      <?php if(!empty($commerceColleges)){ foreach ($commerceColleges as $college3){ ?>
                      <div class="featured">
                        <a href="<?php echo HTTP_ROOT?>college/<?=@$college3['College']['name'].'-'.$college3['College']['id']?>" target="_blank" title="<?php echo @$college3['College']['name'];?>">
                          <img title="" src="<?php echo HTTP_ROOT.'img/collegeImages/thumbnail/'?><?php echo empty($college3['College']['image']) ? 'college-default.jpg' : $college3['College']['image'];?>">
                          <div class="caption">
                            <small><?php echo empty(@$college3['College']['short_name']) ? substr(@$college3['College']['name'],0,10) : @$college3['College']['short_name'];?></small>
                            <!--Removed Likes-->
                            <!-- <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                            <span class="lnr lnr-thumbs-up pull-right"></span>  -->
                          </div>
                        </a>
                      </div>
                      <?php } }?>
                      <!-- <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/amity.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/centurian.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/christ.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/iitrorkee.jpg">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/lpu.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/amity.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/centurian.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/christ.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div> -->
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="babies">
                    <div class="featured-wrapper">
                      <?php if(!empty($medicalColleges)){ foreach ($medicalColleges as $college4){ ?>
                      <div class="featured">
                        <a href="<?php echo HTTP_ROOT?>college/<?=@$college4['College']['name'].'-'.$college4['College']['id']?>" target="_blank" title="<?php echo @$college4['College']['name'];?>">
                          <img title="" src="<?php echo HTTP_ROOT.'img/collegeImages/thumbnail/'?><?php echo empty($college4['College']['image']) ? 'college-default.jpg' : $college4['College']['image'];?>">
                          <div class="caption">
                            <small><?php echo empty(@$college4['College']['short_name']) ? substr(@$college4['College']['name'],0,10) : @$college4['College']['short_name'];?></small>
                            <!--Removed Likes-->
                            <!-- <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                            <span class="lnr lnr-thumbs-up pull-right"></span>  -->
                          </div>
                        </a>
                      </div>
                      <?php } }?>
                      <!-- <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/amity.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/centurian.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/christ.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/iitrorkee.jpg">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/lpu.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/amity.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/centurian.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div>
                      <div class="featured">
                        <img src="<?php echo HTTP_ROOT?>/images/christ.png">
                        <div class="caption">
                          <small>John Markee</small>
                          <small class="pull-right">&nbsp;&nbsp;25&nbsp;&nbsp;</small>
                          <span class="lnr lnr-thumbs-up pull-right"></span> 
                        </div>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </section>
    <section class="featured-photographer">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 text-center m-tb-15">
            <div class="section-headings">
              <h2 class="m-b-10">Top Colleges</h2>
              <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p> -->
              <div class="heading-border"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <a href="" class="pull-right"><small>view all</small></a>
          <div class="col-md-12">
            <div class="owl-carousel">
              <?php if(!empty($trendingColleges)){ foreach ($trendingColleges as $college){ ?>
              <div class="photographer p-10" >
                <a href="<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>" target="_blank" title="<?php echo @$college['College']['name'];?>">
                  <img title="" src="<?php echo HTTP_ROOT.'img/collegeImages/thumbnail/'?><?php echo empty($college['College']['image']) ? 'college-default.jpg' : $college['College']['image'];?>">
                  <h4><?php echo empty(@$college['College']['short_name']) ? substr(@$college['College']['name'],0,10) : @$college['College']['short_name'];?></h4>
                  <small><?php echo substr(@$college['College']['name'],0,17) .' ...';?></small>
                </a>
              </div>
              <?php } }?>
              <!-- <div class="photographer p-10">
                  <img src="<?php echo HTTP_ROOT?>newfrontend/images/lunch-2485717_640.jpg">
                  <h4>Dennis Hawkings</h4>
                  <small>Hawkings Studios</small>
              </div>
              <div class="photographer p-10">
                  <img src="<?php echo HTTP_ROOT?>newfrontend/images/portrait-2132190_1920.jpg">
                  <h4>Tom Weered</h4>
                  <small>T&W Studios Inc.</small>
              </div>
              <div class="photographer p-10">
                  <img src="<?php echo HTTP_ROOT?>newfrontend/images/psychology-2647941_640.jpg">
                  <h4>Tom Weered</h4>
                  <small>T&W Studios Inc.</small>
              </div>
              <div class="photographer p-10">
                  <img src="<?php echo HTTP_ROOT?>newfrontend/images/gray-blazer-2494673_640.jpg">
                  <h4>Tom Weered</h4>
                  <small>T&W Studios Inc.</small>
              </div>
              <div class="photographer p-10">
                  <img src="<?php echo HTTP_ROOT?>newfrontend/images/lunch-2485717_640.jpg">
                  <h4>Dennis Hawkings</h4>
                  <small>Hawkings Studios</small>
              </div>
              <div class="photographer p-10">
                  <img src="<?php echo HTTP_ROOT?>newfrontend/images/portrait-2132190_1920.jpg">
                  <h4>Tom Weered</h4>
                <small>T&W Studios Inc.</small>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="statistics">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="stats-wrap">
              <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
              <h2>7.2M+</h2>
              <h5>Total Visits on GetCollege.in</h5>
            </div>
          </div>
          <div class="col-md-3">
            <div class="stats-wrap">
              <i class="fa fa-university" aria-hidden="true"></i>
              <h2>82K+</h2>
              <h5>Colleges</h5>
            </div>
          </div>
          <div class="col-md-3">
            <div class="stats-wrap">
              <i class="fa fa-book" aria-hidden="true"></i>
              <h2>1.8K+</h2>
              <h5>Courses</h5>
            </div>
          </div>
          <div class="col-md-3">
            <div class="stats-wrap">
              <i class="fa fa-user" aria-hidden="true"></i>
              <h2>3.4M+</h2>
              <h5>Students</h5>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="reg-pgraphersec">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 text-center m-tb-15">
            <div class="social-media-headings">
              <h2 class="m-b-10">Testimonials</h2>
              <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p> -->
              <div class="heading-border" ></div>
            </div>
          </div>
          <div class="col-md-10 col-md-offset-1 text-center m-tb-15">
            <div class="flexslider" style="background: transparent; color: white;">
              <ul class="slides">
                <?php $i=0;foreach($testimonials as $testi){?>
                <li style="height:35vh;">
                  <div class="col-sm-8 col-sm-offset-2">
                   <?php if($testi['Testimonial']['image']!=''){ ?>
                      <img style='width:25%; margin-left: 38%;' class="img-circle" src="<?php echo HTTP_ROOT?>/img/userImages/<?php echo $testi['Testimonial']['image']?>">
                   <?php } else{ ?>
                      <img style='width:25%; margin-left: 38%;' class="img-circle" src="<?php echo HTTP_ROOT?>/img/dummy-profile-pic-male1.jpg">
                    
                    <?php } ?>   
                      <p>"
                      <?php echo $testi['Testimonial']['content']?>
                      "</p>
                      <h4><?php echo $testi['Testimonial']['name']?></h4>
                  </div>
                </li>
                <?php $i++; }?>
              </ul>
            </div>
          </div>
          <!-- <div class="col-md-6">
            <ul class="reg-benefits text-center">
              <li>
                <span class="lnr lnr-alarm"></span><br>
                <small>Get Latest Notifications about Favourites</small>
              </li>
              <li>
                <span class="lnr lnr-laptop-phone"></span><br>
                <small>Show your Profile to large Audience</small>
              </li>
              <li>
                <span class="lnr lnr-bubble"></span><br>
                <small>Get Instant Profile Reviews</small>
              </li>
              <li>
                <span class="lnr lnr-heart"></span><br>
                <small>Access to your favorites</small>
              </li>
              <li>
                <span class="lnr lnr-phone-handset"></span><br>
                <small>Contact your favourite Professional</small>
              </li>
              <li>
                <span class="lnr lnr-alarm"></span><br>
                <small>Get Latest Notifications about Favourites</small>
              </li>
            </ul>
          </div> -->
        </div>
      </div>
    </section>
    <section class="social-media-sec">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 text-center m-tb-15">
            <div class="social-media-headings">
              <h2 class="m-b-10">Get Connected on Social Media with GetCollege.in</h2>
              <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p> -->
              <div class="heading-border" style="background: #fff;margin-bottom: 20px;"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 col-md-offset-2 text-center m-t-30">
            <ul class="sm-ul">
              <li>
                <a href="https://www.instagram.com/getcollege.in/" target="_blank">
                  <svg class="icon">
                    <use xlink:href="#instagram"></use>
                  </svg>
                </a>
              </li>
              <li>
                <a href="https://www.linkedin.com/in/getcollegein" target="_blank">
                  <svg class="icon">
                    <use xlink:href="#linkedin"></use>
                  </svg>
                </a>
              </li>
              <li>
                <a href="">
                  <svg class="icon">
                    <use xlink:href="#whatsapp"></use>
                  </svg>
                </a>
              </li>
              <li>
                <a href="https://twitter.com/GetcollegeIn" target="_blank">
                  <svg class="icon">
                    <use xlink:href="#twitter"></use>
                  </svg>
                </a>
              </li>
              <li>
                <a href="https://www.youtube.com/channel/UCbrwR3eXoluGmNa36UUtkQQ" target="_blank">
                  <svg class="icon">
                    <use xlink:href="#youtube"></use>
                  </svg>
                </a>
              </li>
              <li>
                <a href="https://www.facebook.com/GetCollege.IN/" target="_blank">
                  <svg class="icon">
                    <use xlink:href="#facebook"></use>
                  </svg>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>  
    </section>