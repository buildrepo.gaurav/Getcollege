<style>  .input-error{
    margin: 39px 13px !important;
}

</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
  <div class="container">
      <div class="row profile">
      <?php echo $this->element('collegesidebar');?>
      <div class="col-md-9">
              <div class="profile-content">
           <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    
      
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Course Specialization</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="add-college-course" action="<?php echo HTTP_ROOT.'Homes/saveCollegeSpecialization'?>">
                  
                  <input type="hidden" placeholder="Enter Name" name="data[CourseSpecialization][id]" value="<?php echo @$special['CourseSpecialization']['id']?>">
                  <input type="hidden" placeholder="Enter Name" name="data[CourseSpecialization][college_id]" value="<?php echo @$collegeId?>">
                  <input type="hidden" placeholder="Enter Name" name="data[CourseSpecialization][college_course_id]" value="<?php echo @$collegeCourseId?>">
                  <div class="box-body">
                    <div class="form-group col-md-6">
                      <label for="exampleInputEmail1">Specialization</label>
                      <i class="fa input-error"></i>
                       <select class="form-control required stream-select" name="data[CourseSpecialization][specialization_id]" data-id="1">
                      <option value="">Select Specialization</option>
                      <?php foreach($specializations as $specialization){?>
                        <option value="<?php echo $specialization['Specialization']['id']?>" <?php echo @$specialization['Specialization']['id'] == @$special['CourseSpecialization']['specialization_id'] ? "selected" : ""; ?>><?php echo $specialization['Specialization']['specialization']?></option>
                      <?php }?>
                    </select>
                    </div>
                    
                    
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Fees</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Fees" name="data[CourseSpecialization][fees]" value="<?php echo @$special['CourseSpecialization']['fees']?>">
                    </div>

                    
                  
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Max Salary</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Max Salary" name="data[CourseSpecialization][max_salary]" value="<?php echo @$special['CourseSpecialization']['max_salary']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Min Salary</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Min Salary" name="data[CourseSpecialization][min_salary]" value="<?php echo @$special['CourseSpecialization']['min_salary']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Average Salary</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Average Salary" name="data[CourseSpecialization][avg_salary]" value="<?php echo @$special['CourseSpecialization']['avg_salary']?>">
                    </div>
                    
                    
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Seat</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Seats" name="data[CourseSpecialization][seat]" value="<?php echo @$special['CourseSpecialization']['seat']?>">
                    </div>
                    
                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>


            
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">College Courses</h3>
              </div>
              <table id="college-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Specialization</th>
                  <th>Fee</th>
                  <th>Avg Salary</th>
                  <th>Min Salary</th>
                  <th>Max Salary</th>
                  <th>seat</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($specials as $data){?>
                <tr>
                  
                   <td>
                    <?php echo $data['Specialization']['specialization']?>
                  </td>
                  <td>
                    <?php echo $data['CourseSpecialization']['fees']?>
                  </td>
                  <td>
                    <?php echo $data['CourseSpecialization']['avg_salary']?>
                  </td>
                  <td>
                    <?php echo $data['CourseSpecialization']['min_salary']?>
                  </td>
                  <td>
                    <?php echo $data['CourseSpecialization']['max_salary']?>
                  </td>
                  <td>
                    <?php echo $data['CourseSpecialization']['seat']?>
                  </td>
                  <td>
                    <a target="_blank" class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Homes/collegeSpecialization/'.$collegeCourseId.'/'.$data['CourseSpecialization']['id']?>"><i class="fa fa-edit"></i></a> 
                    
                   
                      <a model = "CourseSpecialization" data-id="<?php echo base64_encode($data['CourseSpecialization']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['CourseSpecialization']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
             <?php if(!empty($courses)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- </form> -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  
  
              </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">

    $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/deleteData/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      }
  });
</script>