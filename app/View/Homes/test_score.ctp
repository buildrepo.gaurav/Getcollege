<style>
  .ml5{
    margin-left: 5px;
  }
</style>
<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
  <div class="p-lr-50 p-tb-20">
    <div class="row">
      <?php echo $this->element('frontEnd/studentsidebar');?>
      <div class="col-md-9" style="background:#fff">
      
          <h2>Test Score</h2>
          

          <div class="col-md-12">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Test</th>
                  <th>Total Questions</th>
                  <th>Question Attempted</th>
                  <th>Total Marks</th>
                  <th>Marks Obtain</th>
                  <th>Correct Answered</th>
                  <th>Wrong Answered</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($score as $sc){?>
                  <tr>
                    <td><?php echo $sc['Test']['title']?></td>
                    <td><?php echo $sc['StudentAnswer']['total_question']?></td>
                    <td><?php echo $sc['StudentAnswer']['question_attempt']?></td>
                    <td><?php echo $sc['StudentAnswer']['total_marks']?></td>
                    <td><?php echo $sc['StudentAnswer']['total_mark_obtain']?></td>
                    <td><?php echo $sc['StudentAnswer']['question_correct']?></td>
                    <td><?php echo $sc['StudentAnswer']['question_wrong']?></td>
                    <td><?php echo $sc['StudentAnswer']['percentage'] > 33 ? "Qualified" : "Fail"?></td>
                  </tr>
                <?php }?>
              </tbody>
            </table>
          </div>
       
      </div>
    </div>
  </div>
</section>
<style type="text/css">
.hidediv
{
  display:none;
}
</style>
<script>
$(function() {
    $( ".addsbutton" ).click(function() {
        $( ".hidediv" ).slideToggle('slow','swing');
    });
});
  var win;
  $('#start-test').on('click',function(){
    //window.open();
    win = window.open("<?php echo HTTP_ROOT.'Homes/test/'.base64_encode($test['Test']['id'])?>","_blank",12,'scrollbars=1,menubar=0,resizable=1,width=850,height=500');
    window.location.reload();
      //win.close();
    
  });

// Set the date we're counting down to
//var countDownDate = new Date(":<?php //echo $test['Test']['minute']?>:00").getTime();

/**/
</script>