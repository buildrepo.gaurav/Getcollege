<script>
var lat1 ;
      var lng1;
  if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
              lat1 = position.coords.latitude;
              lng1 = position.coords.longitude;
              console.log(lat1);
              console.log(lng1);
          });
        }
</script>
<?php echo $this->Html->css(array(
                        //'bootstrap/css/bootstrap.min.css',
                        
                        '/plugins/datatables/dataTables.bootstrap.css',
                        'AdminLTE.min.css',
                        'skins/_all-skins.min.css',
                        '/plugins/iCheck/flat/blue.css',
                        '/plugins/morris/morris.css',
                        '/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
                        '/plugins/datepicker/datepicker3.css',
                        '/plugins/daterangepicker/daterangepicker.css',
                        '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
                        'bootstrap-multiselect.css',
                        '/dist/css/tooltipster.bundle.min.css'
                      )
                    );
      ?>
<?php echo $this->Html->script(array(
                      //'/plugins/jQuery/jquery-2.2.3.min.js',
                      'jquery-ui.min.js',
                      //'bootstrap/js/bootstrap.min.js',
                      'raphael-min.js'
                      )
                  );
        echo $this->Html->script(array('/plugins/morris/morris.min.js'));
      echo $this->Html->script(array( 
                      '/plugins/sparkline/jquery.sparkline.min.js',
                      '/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
                      '/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
                      '/plugins/knob/jquery.knob.js',
                      'moment.min.js',
                      '/plugins/daterangepicker/daterangepicker.js',
                      '/plugins/datepicker/bootstrap-datepicker.js',
                      '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',

                      '/plugins/datatables/jquery.dataTables.min.js',
                      '/plugins/datatables/dataTables.bootstrap.min.js',
                      '/plugins/slimScroll/jquery.slimscroll.min.js',
                      '/plugins/fastclick/fastclick.js',
                      'app.min.js',

                      )
                    );
        echo $this->Html->script(array('pages/dashboard.js'));

      echo $this->Html->script(array(

                      'demo.js',
                      'jquery.form.js'

                      )
                  );
  ?>
<style>
.inner h3
{
  color:#fff;
}
.inner p
{
  color:#fff;
}
.dropdown-menu{
  width: 350px !important;
    left: auto;
}
</style>
<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
	<div class="p-lr-50 p-tb-20">
	    <div class="row">
			<?php echo $this->element('frontEnd/studentsidebar');?>
			<div class="col-md-9">
				
	              <div class="profile-content">
	              	<!-- <div class="col-md-9">
				   <h2 style='color:green;'>Current Plan: <?php echo $planoo['MembershipPlan']['plan_name']; ?></h2>
				   
				   Current Plan Cost: <?php echo $planoo['MembershipPlan']['plan_cost']; ?>
				    </div>
	            <div class="col-md-3">
	            	<button data-toggle="modal" data-target="#membershipModal" data-action="page" class="btn btn-info moto-widget-menu-link moto-widget-menu-link-level-1 moto-link level_moto_2">Upgrade Membership Plan</button>
	            </div> -->
				<div class="">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student Dashboard
        <small><?php echo $details['Student']['name'];?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Student</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $testCount?></h3>

              <p>Test Taken</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo @$pro_count;?><sup style="font-size: 20px">%</sup></h3>

              <p>Profile Complete</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo @$count_compare;?></h3>

              <p>Your Compare Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 style="font-size:22px"><?php echo date('d/m/Y', strtotime(@$profile_count1['Student']['current_access']))?></h3>
              <h3 style="font-size:22px"><?php echo date('H:i:s', strtotime(@$profile_count1['Student']['current_access']))?></h3>

              <p>Current Access </p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#revenue-chart" data-toggle="tab">Report</a></li>
              <!-- <li><a href="#sales-chart" data-toggle="tab">Donut</a></li> -->
              <li class="pull-left header"><i class="fa fa-inbox"></i> My Best Result with Topper in <?php echo $bestTest['Test']['title']?>
              </li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="custom-chart" style="position: relative; height: 300px;"></div>
              <script>
                Morris.Area({
                  element: 'custom-chart',
                  data: [
                     <?php 
                        foreach($graphData as $section){
                     
                      ?>    
                          
                    {section: '<?php echo $section["section"];?>', y: <?php echo $section[$section['loginStuName']];?>,z: <?php echo $section[$section['topperStuName']];?>},
                    
                    <?php  
                       
                        }  ?>
                  ],
                  parseTime: false,
                  xkey: 'section',
                  ykeys: ['y','z'],
                  labels: ['<?php echo $graphData["0"]["loginStuName"];?>','<?php echo $graphData["0"]["topperStuName"];?>']
                }).on('click', function(i, row){
                  console.log(i, row);
                });
              </script>
               <div class="box-footer no-border">
              <div class="row text-center">
                  <h4 style="color:#000">Top 3 Students in <?php echo $bestTest['Test']['title']?></h4>
              </div>
              <div class="row">
              <?php foreach($topThreeInSameTest as $topthr){?>
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <input type="text" class="knob" data-readonly="true" value="<?php echo $topthr['StudentAnswer']['percentage']?>" data-width="60" data-height="60" data-fgColor="#39CCCC">

                  <div class="knob-label"><?php echo $topthr['Student']['name']?></div>
                </div>
              <?php }?>  
                
              </div>
            </div>
          </div>
          <!-- /.nav-tabs-custom -->

          <!-- Chat box -->
          
          <!-- /.box (chat box) -->

          <!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">To Do List</h3>

              <div class="box-tools pull-right">
                <ul class="pagination pagination-sm inline">
                  <li><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => ''));?></li>
                  <li><?php echo $this->Paginator->numbers(array('modulus' => '4','class'=>'AjaxPage'));?></li>
                  <li><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => ''));?></li>
                </ul>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="todo-list">
                <?php foreach($toDoList as $tdlist){?>
                <li id="todo-list-li-<?php echo $tdlist['ToDoList']['id']?>">
                  <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <!-- checkbox -->
                  <input type="checkbox" class="checkbox1" value="" id="<?php echo $tdlist['ToDoList']['id']?>">
                  <!-- todo text -->
                  <span class="text"><?php echo $tdlist['ToDoList']['title']?></span>
                  <!-- Emphasis label -->
                  <?php 
                  $date2 = date_create($tdlist['ToDoList']['work_date'].' '.$tdlist['ToDoList']['work_time']);
                  $date1=date_create();
                                    //$date2=date_create("2016-08-26 16:52:00");
                  $diff=date_diff($date1,$date2);
                  $date = " : ";
                  if($diff->y != 0){
                    $date .= $diff->y.' Year';
                  }
                  else if($diff->m != 0){
                    $date .= $diff->m.' Month';
                                    }
                                    else if($diff->d != 0){
                                        $date .= $diff->d.' Day';
                                    }
                                    else if($diff->h != 0){
                                        $date .= $diff->h.' Hour';
                                    }
                                    else if($diff->i != 0){
                                        $date .= $diff->i.' Mintue';
                                    }
                                    else if($diff->s != 0){
                                        $date .= ' Just now';
                                    }
                                    else{
                                      $date .= ' Just now';
                                    }
                                     
                                  ?>
                  <small class="label label-danger"><i class="fa fa-clock-o"></i> <?php echo $date; ?></small>
                  <!-- General tools such as edit or delete-->
                  <div class="tools">
                    <a href="javascript:void(0)" class="todo-edit" data-id="<?php echo base64_encode($tdlist['ToDoList']['id'])?>"><i class="fa fa-edit"></i></a>
                    <a href="javascript:void(0)" class="todo-delete" id="<?php echo base64_encode($tdlist['ToDoList']['id'])?>" title="Delete"><i class="fa fa-trash-o"></i></a>
                  </div>
                </li>
                <?php }?>
                
                <!-- <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Make the theme responsive</span>
                  <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Let theme shine like a star</span>
                  <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Let theme shine like a star</span>
                  <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Check your messages and notifications</span>
                  <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
                <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <input type="checkbox" value="">
                  <span class="text">Let theme shine like a star</span>
                  <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li> -->
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <button type="button" class="btn btn-primary pull-right todo-done">Done</button>
              <button type="button" data-toggle="modal" data-target="#todolist" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
            </div>
          </div>
          <!-- /.box -->

          <!-- quick email widget -->
          <div class="box box-info">
            <form action="<?php echo HTTP_ROOT.'homes/send_mail_by_student'?>" method="post">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Mail For Counselling </h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
              
                <div class="form-group">
                  <input type="email" class="form-control" name="data[to]" placeholder="Email to:" value="info@getcollege.in" readonly>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="data[subject]" placeholder="Subject">
                </div>
                <div>
                  <textarea class="form-control" name="data[message]" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
              
            </div>
            <div class="box-footer clearfix">
              <button type="submit" class="pull-right btn btn-default" id="sendEmail">Send
                <i class="fa fa-arrow-circle-right"></i></button>
            </div>
            </form>
          </div>

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

          <!-- Map box -->
          <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range">
                  <i class="fa fa-calendar"></i></button>
                <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                  <i class="fa fa-minus"></i></button>
              </div>
              <!-- /. tools -->

              <i class="fa fa-map-marker"></i>

              <h3 class="box-title">
                Your Location
              </h3>
            </div>
            <div class="box-body">
              <div id="map" style="height: 250px; width: 100%;"></div>
            </div>
            <!-- /.box-body-->
            <div class="box-footer no-border">
              <div class="row">
                <!-- <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <div id="sparkline-1"></div>
                  <div class="knob-label">Visitors</div>
                </div>
                <!-- ./col ->
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <div id="sparkline-2"></div>
                  <div class="knob-label">Online</div>
                </div>
                <!-- ./col ->
                <div class="col-xs-4 text-center">
                  <div id="sparkline-3"></div>
                  <div class="knob-label">Exists</div>
                </div> -->
                <!-- ./col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.box -->

          <!-- solid sales graph -->
          <div class="box box-solid bg-teal-gradient">
            <div class="box-header">
              <i class="fa fa-th"></i>

              <h3 class="box-title">Score Card</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <div class="box-body border-radius-none">
              <div class="chart" id="stu-graph" style="height: 250px;"></div>
              <script>
                Morris.Bar({
                element: 'stu-graph',
                data: [
                  <?php 
                        foreach($graphData as $section){
                     
                      ?>    
                          
                    {section: '<?php echo $section["section"];?>', y: <?php echo $section[$section['loginStuName']];?>},
                    
                    <?php  
                       
                        }  ?>
                ],
                parseTime: false,
                xkey: 'section',
                ykeys: ['y'],
                labels: ['<?php echo $graphData["0"]["loginStuName"];?>']
              }).on('click', function(i, row){
                console.log(i, row);
              });
              </script>
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-border">
              <div class="row text-center">
                  <h4 style="color:#000">Top 3 Students in level 1</h4>
              </div>
              <div class="row">
              <?php foreach($topThreeInAll as $topthr){?>
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <input type="text" class="knob" data-readonly="true" value="<?php echo $topthr['StudentAnswer']['percentage']?>" data-width="60" data-height="60" data-fgColor="#39CCCC">

                  <div class="knob-label"><?php echo $topthr['Student']['name']?></div>
                </div>
              <?php }?>  
                
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->

          <!-- Calendar -->

          <!-- /.box -->

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
				
				
				  </div>
				   
	           	
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="edit-todolist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="todolist-form" method="post" action="<?php echo HTTP_ROOT?>Homes/editTodolist">
      <div class="modal-header" style="color:#000 !important;background:#36454F !important;">
        <h4 class="modal-title" id="exampleModalLabel" >To Do List</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-header">
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Title</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" id="todo-title" class="form-control required" placeholder="Title" name="data[ToDoList][title]" value="">
          <input type="hidden" id="todo-id" name="data[ToDoList][id]" value="">
          </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Description</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" id="todo-des" class="form-control required" placeholder="Description" name="data[ToDoList][description]" value="">
          </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Date</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <input type="text" id="todo-date" class="form-control todo-datepicker required" placeholder="Date" name="data[ToDoList][work_date]" value="">
          </div>
        </div>
        <div class="form-group col-md-12">
          <label for="country" class="col-sm-3 control-label label_left">Time</label>
          <div class="col-sm-9">
          <i class="fa input-error"></i>
          <!-- <input type="email" class="form-control required" placeholder="Time" name="data[ToDoList][work_time]" value=""> -->
          <select class="form-control col-md-6 required" id="todo-hour" name="data[ToDoList][work_time_hour]">
            <option selected disabled>Select Hour</option>
            <?php for($i=1;$i<=24;$i++){ ?>
            <option value="<?php echo $i;?>"><?php echo $i;?></option>
            <?php }?>
          </select>
          <select class="form-control col-md-6 required" id="todo-min" name="data[ToDoList][work_time_minute]">
            <option selected disabled>Select Minute</option>
            <?php for($i=1;$i<=60;$i++){?>
            <option value="<?php echo $i;?>"><?php echo $i;?></option>
            <?php }?>
          </select>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>
    </div>
  </div>
  <p>*Note : Top Scores are subjected to change as more students are taking test every day.</p>
</div>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
      


      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      /*var map;
      var infowindow;

      function initMap() {
        
        

        var pyrmont = {lat:lat1, lng:lng1};
        console.log(pyrmont);

        map = new google.maps.Map(document.getElementById('map'), {
          center: pyrmont,
          zoom: 15
        });

        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
          location: pyrmont,
          radius: 500,
          type: ['store']
        }, callback);
      }

      function callback(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
          }
        }
      }

      function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.setContent(place.name);
          infowindow.open(map, this);
        });
      }*/

    </script>
    <script>

      function initMap() {
      	//var pyrmont = {lat:lat1, lng:lng1};
        var myLatLng = {lat: lat1, lng: lng1};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Your current location!!!'
        });
      }
    </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAE5HgkudkgGNefp2UNNS19qjevxqZ7Mqk&libraries=places&callback=initMap" async defer></script>