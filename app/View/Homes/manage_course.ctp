<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
  <div class="container">
      <div class="row profile">
      <?php echo $this->element('collegesidebar');?>
      <div class="col-md-9">
              <div class="profile-content">
           <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style>
  a.fafa-icons{
      margin-left: 10px;
}
.fa-trash,.fa-trash-o,.fa-edit,.fa-plus,.fa-save,.fa-building{
  font-size: 24px;
}
</style>
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage College Courses
        <a href="<?php echo HTTP_ROOT?>Homes/addCourse" class="btn btn-primary">Add Course</a>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">College Courses</h3>
              </div>
              <table id="college-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Course</th>
                  <th>Fee</th>
                  <th>FormsAdda Cost</th>
                  <th>Effective from</th>
                  <th>Effective to</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($courses as $data){?>
                <tr id="cours-<?php echo $data['CollegeCourse']['id']?>">
                  
                   <td>
                    <?php echo $data['Specialization']['specialization']?>
                  </td>
                  <td>
                    <?php echo $data['CollegeCourse']['fees']?>
                  </td>
                  <td>
                    <?php echo $data['CollegeCourse']['formsadda_cost']?>
                  </td>
                  <td>
                    <?php echo $data['CollegeCourse']['from_date']?>
                  </td>
                  <td>
                    <?php echo $data['CollegeCourse']['to_date']?>
                  </td>
                  <td>
                    <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Homes/addCourse/'.base64_encode($data['CollegeCourse']['id'])?>"><i class="fa fa-edit"></i></a> 
                    <a target="_blank" class="fafa-icons" title="Manage Specialization" href="<?php echo HTTP_ROOT?>Homes/collegeSpecialization/<?php echo $data['CollegeCourse']['id']?>"><i class="fa fa-building" ></i></a>
                    <a class="fafa-icons delete-row" title="Delete" data-id="<?php echo $data['CollegeCourse']['id']?>" href="javascript:void(0);"><i class="fa fa-trash"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
             <?php if(!empty($courses)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- </form> -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
  $('select').select2();
$(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Homes/deleteData/CollegeCourse/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         $('#cours-'+id).remove();
      }
  });
 
</script>
              </div>
      </div>
    </div>
  </div>
</section>