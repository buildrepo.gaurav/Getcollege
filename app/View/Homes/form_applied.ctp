<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
  <div class="container">
      <div class="row profile">
      <?php echo $this->element('frontEnd/studentsidebar');?>
      <div class="col-md-9">
              <div class="profile-content">
           <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style>
  a.fafa-icons{
      margin-left: 10px;
}
.fa-trash,.fa-trash-o,.fa-eye,.fa-plus,.fa-save,.fa-building{
  font-size: 24px;
}
</style>
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Applied Form Listing
        
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Orders</h3>
              </div>
              <table id="college-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Amount</th>
                  <th>No. of Forms</th>
                  <th>Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($orders as $data){?>
                <tr id="order-<?php echo $data['Order']['id']?>">
                  
                  <td>
                    <?php echo $data['Order']['amount']?>
                  </td>
                  <td>
                    <?php echo $data['Order']['no_of_forms']?>
                  </td>
                  <td>
                    <?php echo if($data['Order']['status'])?>
                  </td>
                  <td>
                    <?php 
                                             $date=date_create(@$data['Order']['created_date']);
                                             echo DATE_FORMAT($date,'d M Y')?>
                                             
                  </td>
                  
                  <td>
                    <a class="fafa-icons" title="View Complete Details" href="<?php echo HTTP_ROOT.'Homes/viewOrder/'.base64_encode($data['Order']['id'])?>"><i class="fa fa-eye"></i></a> 
                    </i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
          </div>
          <!-- </form> -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
              </div>
      </div>
    </div>
  </div>
</section>