<div class="row">
				<div class="col-md-6">
				<!--Part 01 Table 01-->
					<table class="table table-bordered t01">
						<thead>
						  <tr>
							<th><b>Personal Details</b></th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>
								<div class="">            
									  <table class="table table-hover">
										<tbody>
										  <tr>
											<td>Full Name</td>
											<td><input class="form-control" type="text" name="data[OrderApplicationForm][full_Name]" value="<?php echo $orderForm['Student']['name']?>">
												<input type="hidden" name="data[OrderApplicationForm][order_id]" value="<?php echo $orderId;?>">
											</td>
											
										  </tr>
										  <tr>
											<td>EMail</td>
											<td><input class="form-control" type="email" name="data[OrderApplicationForm][email]" value="<?php echo $orderForm['Student']['email']?>" readonly></td>
											
										  </tr>
										  <tr>
											<td>Contact No.</td>
											<td><input class="form-control" type="text" name="data[OrderApplicationForm][mobile]" value="<?php echo $orderForm['Student']['mobile']?>"></td>
										  </tr>
										   <tr>
											<td>Date of Birth (DOB)</td>
											<td><input class="form-control" type="text" name="data[OrderApplicationForm][dob]" value="<?php echo $orderForm['Student']['dob']?>"></td>
										  </tr>
										   <tr>
											<td>Gender</td>
											<td><input class="" type="radio" value="0" name="data[OrderApplicationForm][gender]" <?php echo $orderForm['Student']['gender'] == 0 ? 'checked' : '';?>>Male <input class="" type="radio" value="1" name="data[OrderApplicationForm][gender]" <?php echo $orderForm['Student']['gender'] == 1 ? 'checked' : '';?>>Female</td>
										  </tr>
										  <tr>
											<td>Category</td>
											<td>
												<select class="form-control"  name="data[OrderApplicationForm][category]"  >
		                                          <option value=''>SELECT CATEGORY</option>                             
		                                          <option <?php echo @$orderForm['Student']['category'] == 'GENERAL' ? 'selected' : '';?> value='GENERAL'  >GENERAL</option>
		                                          <option <?php echo @$orderForm['Student']['category'] == 'OBC' ? 'selected' : '';?> value='OBC'  >OBC</option>
		                                          <option <?php echo @$orderForm['Student']['category'] == 'SC' ? 'selected' : '';?> value='SC'  >SC</option>
		                                          <option <?php echo @$orderForm['Student']['category'] == 'ST' ? 'selected' : '';?> value='ST'  >ST</option>
		                                          <option <?php echo @$orderForm['Student']['category'] == 'NT' ? 'selected' : '';?> value='NT'  >NT</option>
		                                          <option <?php echo @$orderForm['Student']['category'] == 'OTHER' ? 'selected' : '';?> value='OTHER'  >OTHER</option>
		                                        </select>
											</td>
										  </tr>
										  <tr>
											<td>Mother Tongue</td>
											<td><input class="form-control" type="text" name="data[OrderApplicationForm][mother_tongue]" value="<?php echo $orderForm['Student']['mother_tongue']?>"></td>
										  </tr>
										   <tr>
											<td>Marital Status</td>
											<td><input class="" type="radio" value="0" name="data[OrderApplicationForm][marital_status]" <?php echo $orderForm['Student']['marital_status'] == 0 ? 'checked' : '';?>>Unmarried <input class="" type="radio" value="1" name="data[OrderApplicationForm][marital_status]" <?php echo $orderForm['Student']['marital_status'] == 1 ? 'checked' : '';?>>Married</td>
										  </tr>
										   <tr>
											<td>Nationality</td>
											<td><input class="form-control" type="text" name="data[OrderApplicationForm][nationality]" value="Indian"></td>
										  </tr>
										  <tr>
											<td>Photo</td>
											<td><input class="form-control" type="file" name="Photo"></td>
										  </tr>
										</tbody>
									  </table>
								</div>
							</td>
						  </tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-6">
				<!--Part 02 Table 01-->
					 <table class="table table-bordered t01">
						<thead>
						  <tr>
							<th><b>Academics Details (10th, 12th, Graduation, Others)</b></th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>
								<div class="">            
									  <table class="table table-hover inner_table">
										<thead>
										  <tr>
											<th><b>Exam name</b></th>
											<th><b>Year of Passing</b></th>
											<th><b>Institution</b></th>
											<th><b>Board/University</b></th>
											<th><b>Percentage (%)</b></th>
										  </tr>
										</thead>
										<tbody>
											<?php $i=0;foreach($orderForm['StudentAcademic'] as $academic){?>
										  <tr>
										  	
												<td><input name="data[StudentAcademic][<?php echo $i?>][exam]" class="form-control" type="text" name="YoP_HSC" value="<?php echo $academic['exam']?>"></td>
												<td><input class="form-control" type="text" name="data[StudentAcademic][<?php echo $i?>][passing_year]" value="<?php echo $academic['passing_year']?>"></td>
												<td><input class="form-control" type="text" name="data[StudentAcademic][<?php echo $i?>][institute]" value="<?php echo $academic['institute']?>"></td>
												<td><input class="form-control" type="text" name="data[StudentAcademic][<?php echo $i?>][university]" value="<?php echo $academic['university']?>"></td>
												<td><input class="form-control" type="text" name="data[StudentAcademic][<?php echo $i?>][percentage]" value="<?php echo $academic['percentage']?>"></td>
												
										  </tr>
										  <?php $i++;}?>
										</tbody>
									  </table>
								</div>
							
							
							</td>
						  </tr>
						</tbody>
					  </table>
				<!--Part 02 Table 02-->
					<table class="table table-bordered t01">
						<thead>
						  <tr>
							<th><b>Address Details</b></th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>
								<div class="">            
									  <table class="table table-hover inner_table">
										
										<tbody>
										  <tr>
											<td colspan="2"><textarea class="form-control" rows="5" placeholder="Address" id="Address" name="data[OrderApplicationForm][address]"><?php echo $orderForm['Student']['address']?></textarea></td>
										  </tr>
										  <tr>
										  	<td>
										  		<select id="country" class="form-control required" name="data[OrderApplicationForm][country_id]">
                                              <option value="">Select Country</option>
                                              <?php foreach($country as $coun){?>
                                              <option value="<?php echo $coun['Country']['id']?>" <?php echo @$orderForm['Student']['country_id'] == $coun['Country']['id'] ? 'selected' : '';?>><?php echo $coun['Country']['country_name']?></option>
                                              <?php }?>
                                            </select>
										  	</td>
										  	<td>
												<select id="state" class="form-control required" name="data[OrderApplicationForm][state_id]">
						                          <option value="">Select State</option>
						                            <?php if(!empty($states)){?>
						                            <?php foreach($states as $state){?>
						                              <option value="<?php echo $state['State']['id']?>" <?php echo $state['State']['id'] == $orderForm['Student']['state_id'] ? 'selected' : '';?>><?php echo $state['State']['statename']?></option>
						                            <?php }?>  
						                            <?php }?>  
						                        </select>
						                      </td>
										  </tr>
										  <tr>
											<td>
												<select id="city" class="form-control required" name="data[OrderApplicationForm][city_id]">
                                            <option value="">Select City</option>
                                            <?php if(!empty($cities)){?>
                                                <?php foreach($cities as $city){?>
                                                  <option value="<?php echo $city['City']['id']?>" <?php echo $city['City']['id'] == $orderForm['Student']['city_id'] ? 'selected' : '';?>><?php echo $city['City']['city_name']?></option>
                                                <?php }?>  
                                            <?php }?>
                                          </select>
											</td>
											
											<td><input class="form-control" type="text" name="data[OrderApplicationForm][pincode]" placeholder="Pin Code" value="<?php echo $orderForm['Student']['pincode']?>"></td>
										  </tr>
										</tbody>
									  </table>
								</div>
							
							</td>
						  </tr>
						</tbody>
					  </table>
				
				</div>
			
			</div>
		  <!--Row 2-->
			
			<div class="row">
				<div class="col-md-6">
				
					<table class="table table-bordered t01">
						<thead>
						  <tr>
							<th><b>Work Experience</b></th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>
								<div class="">            
									  <table class="table table-hover inner_table">
										<thead>
										  <tr>
											<th><b>Organization</b></th>
											<th><b>Designation</b></th>
											<th><b>Joining Date</b></th>
											<th><b>Release Date</b></th>
											<th><b>Action</b></th>
										  </tr>
										</thead>
										<tbody id="experience-table">
											<?php $i=0;foreach($orderForm['StudentExperience'] as $exp){?>
										  <tr id="append_<?php echo $i;?>">
												<td><input class="form-control" type="text" name="data[StudentExperience][<?php echo $i;?>][name]" value="<?php echo $exp['name']?>"></td>
												<td><input class="form-control" type="text" name="data[StudentExperience][<?php echo $i;?>][designation]" value="<?php echo $exp['designation']?>"></td>
												<td><input class="form-control" type="text" name="data[StudentExperience][<?php echo $i;?>][joining]" value="<?php echo $exp['joining']?>"></td>
												<td><input class="form-control" type="text" name="data[StudentExperience][<?php echo $i;?>][releasing]" value="<?php echo $exp['releasing']?>"></td>
												<td>
													
																<a href="javascript:void(0);" class="add_sub_button append-experience-a">
																		<span class="glyphicon glyphicon-plus"></span>
																	</a>
																
																
																	<?php if($i!=0){?>
																	<a href="javascript:void(0);" class="add_sub_button delete-experience-tr" tr-id="append_<?php echo $i;?>">
																		<span class="glyphicon glyphicon-remove"></span>
																	 </a>
																	 <?php }?>
																
															
												
													  
												</td>
										  </tr>
										  <?php $i++;}?>
										</tbody>
									  </table>
								</div>
							
							</td>
						  </tr>
						</tbody>
					  </table>
				
				</div>
				<div class="col-md-6">
		  
					<table class="table table-bordered t01">
						<thead>
						  <tr>
							<th><b>Competitive Exam Details</b></th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>
								<div class="">            
									  <table class="table table-hover inner_table">
										<thead>
										  <tr>
											<th><b>Exam Name</b></th>
											<th><b>Exam Year</b></th>
											<th><b>Composite Score</b></th>
											<th><b>Percentile</b></th>
											<th><b>Action</b></th>
										  </tr>
										</thead>
										<tbody id="competitve-table">
										  <tr id="competitive_1">
											<td><input class="form-control" type="text" name="data[CompetitiveExam][1][name]" placeholder="Name"></td>
											<td><input class="form-control" type="text" name="data[CompetitiveExam][1][year]" placeholder="Year"></td>
											<td><input class="form-control" type="text" name="data[CompetitiveExam][1][score]" placeholder="Score"></td>
											<td><input class="form-control" type="text" name="data[CompetitiveExam][1][percentile]" placeholder="Percentile"></td>
											<td>
															<a href="javascript:void(0)" class="add_sub_button append-competitive">
																	<span class="glyphicon glyphicon-plus"></span>
																</a>
															
															
															
																
															
														
												  
											</td>
											
											
										  </tr>
										</tbody>
									  </table>
								</div>
							
							</td>
						  </tr>
						</tbody>
					  </table>
		 
				</div>
		  
			</div>
		  
			<!--Row 3-->
			<div class="row">
				<div class="col-md-6">
				
					<table class="table table-bordered t01">
						<thead>
						  <tr>
							<th><b>Parent Details</b></th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>
								<div class="">            
									  <table class="table table-hover inner_table">
										
										<tbody>
										  
										  <tr>
											<td><input class="form-control" type="text" name="data[OrderApplicationForm][father_name]" placeholder="Father's Name" value="<?php echo $orderForm['StudentMeta']['father_name']?>"></td>
										  </tr>
										  <tr>
											<td><input class="form-control" type="text" name="data[OrderApplicationForm][father_occupation]" placeholder="Father's Occupation" value="<?php echo $orderForm['StudentMeta']['father_occupation']?>"></td>
										  </tr>
										  <tr>
											<td><input class="form-control" type="text" name="data[OrderApplicationForm][father_mob]"" placeholder="Father's Contact" value="<?php echo $orderForm['StudentMeta']['father_mob']?>"></td>
											
										  </tr>
										</tbody>
									  </table>
								</div>
							
							</td>
						  </tr>
						</tbody>
					  </table>
				
				</div>
				<div class="col-md-6">
				
					<table class="table table-bordered t01">
						<thead>
						  <tr>
							<th><b>Custom fields for college</b></th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td>
								<div class="">            
									  <table class="table table-hover inner_table">
										<tbody>
										  <tr>
										  <th><b>Filed Name</b></th>	
										  <th><b>Filed Value</b></th>	
										</tr>
										<?php if(!empty($customFields)){?>
										<?php foreach($customFields as $fields){?>
											<tr>
										
										  	<td><?php echo $fields['CollegeField']['field_name']?></td>	
											<td><input class="form-control" name="data[CustomFileds][<?php echo $fields['CollegeField']['field_name']?>]" type="text" name="Coll_City" placeholder = "<?php echo $fields['CollegeField']['field_name']?>"></td>
											
											
										  </tr>
										<?php }}else{?>
											<tr>
											<b>No Custom Fileds</b>
											</tr>
										<?php }?>
										</tbody>
									  </table>
								</div>
							
							</td>
						  </tr>
						</tbody>
					  </table>
				
				</div>
			</div>
		  
		  
		  
		  
		  
		  <table class="table table-bordered t01">
			<thead>
			  <tr>
				<th><b>Declaration</b></th>
			  </tr>
			</thead>
			<tbody>
			  <tr>
				<td>
					<div class="container">            
						  <table class="table table-hover inner_table">
							
							<tbody>
							  <tr>
								<div class="checkbox">
								  <label><input type="checkbox" value=""> I hereby declare that all the information provided in this form is complete and accurate to the best of my knowledge. I confirm to have read all the rules and regulations of the organization and agree to abide by them.</label>
								</div>
							  </tr>
							  <tr>
								<button type="submit" class="btn btn-warning form_continu_button">Continue</button>
							  </tr>
							</tbody>
						  </table>
					</div>
				
				</td>
			  </tr>
			</tbody>
		 </table>