<link rel="stylesheet" href="<?php echo HTTP_ROOT?>/frontend/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
  .ml5{
    margin-left: 5px;
  }
</style>
<div class="container-fluid">
<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
  <div class="p-lr-50 p-tb-20">
    <div class="row">
      <div class="col-md-12" style="background:#fff">
        <div class="profile-content">
          <div class="col-md-12">
            <div class="col-md-6">
              <h2>Test </h2>
            </div>
            <div class="col-md-6">
              <h2>Time Left : <span id="setTime"></span></h2>
            </div>
            
          </div>
          <form method="post" action="<?php echo HTTP_ROOT.'Homes/test'?>" id="form" role="form">
          <input type="hidden" value="<?php echo $test['Test']['id'];?>" name="test[testId]">
          <input type="hidden" value="<?php echo $getId;?>" name="test[rowId]">
            <?php $i=1;foreach($test['TestSection'] as $q){?>
              
              <div class="col-md-12 ">
                <h3>Section : <?php echo $q['title']?></h3>
              </div>
              <?php ; foreach($q['TestQuestion'] as $que){?>
                
                <div class="col-md-12" >
                    <div class="col-md-12">
                      <label>Q.<?php echo $i;?> : <?php echo $que['question']?> ?</label>
                    </div>
                    <input type="hidden" value="<?php echo base64_encode($que['answer_id']);?>" name="question[<?php echo $i;?>][check]">
                    <input type="hidden" value="<?php echo $que['id'];?>" name="question[<?php echo $i;?>][qId]">
                    <input type="hidden" value="<?php echo $q['id'];?>" name="question[<?php echo $i;?>][sectionId]">
                    <?php $j=1;foreach($que['TestQuestionOption'] as $opt){?>
                      <div class="col-md-6" style="display: inline-flex;">

                        <input type="radio" value="<?php echo $opt['id']?>" name="question[<?php echo $i;?>][user_ans]"><p class="ml5">Ans <?php echo $j;?> : <?php echo $opt['option']?></p>
                      </div>
                    <?php $j++;}?>
                </div>
              <?php $i++;}?>
            <?php }?>
            <a onclick="sub()" class="btn btn-primary">Submit</a>
          </form>

        </div>
      </div>
    </div>
  </div>
</section>
</div>
<style type="text/css">
.hidediv
{
  display:none;
}
</style>
<script>
$(function() {
    $( ".addsbutton" ).click(function() {
        $( ".hidediv" ).slideToggle('slow','swing');
    });
});

</script>
<script>
// Set the date we're counting down to
//var countDownDate = new Date(":<?php //echo $test['Test']['minute']?>:00").getTime();
var k=0;
var d1 = new Date ();
var countDownDate = new Date ( d1 );
countDownDate.setHours ( d1.getHours() + <?php echo $test['Test']['hour']?> );
countDownDate.setMinutes ( d1.getMinutes() + <?php echo $test['Test']['minute']?> );
//console.log(countDownDate);
// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now an the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("setTime").innerHTML = hours + "h "
  + minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text 
  if (distance < 0) {
    //$('#form').submit();
    if(k==0){
      k++;
     var formData = new FormData();
               var params   = $('#form').serializeArray();
           

            $.each(params, function(i, val) {
                formData.append(val.name, val.value);
            });
     //console.log(formData);
          $.ajax({
              type:'POST',
              dataType:'json',
              contentType: false,
              processData: false,
              async:false,
              data:formData,
              url:'<?php echo HTTP_ROOT?>Homes/test', 
              success:function(resp)
              {   

              

              }
          
        
              

          });
}else{
window.opener.location.reload();
window.close(); 
}
   // window.opener.location.reload(false);
    //window.close();
    /*clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";*/
  }
}, 1000);

function sub(){
  var formData = new FormData();
               var params   = $('#form').serializeArray();
           

            $.each(params, function(i, val) {
                formData.append(val.name, val.value);
            });
     //console.log(formData);
          $.ajax({
              type:'POST',
              dataType:'json',
              contentType: false,
              processData: false,
              async:false,
              data:formData,
              url:'<?php echo HTTP_ROOT?>Homes/test', 
              success:function(resp)
              {  
              window.opener.location.replace('<?php echo HTTP_ROOT?>Homes/testScore'); 
window.close(); 
              

              }
          
        
              

          });

}
</script>