<style>
.select2{
  width:100% !important;
}
.about-doctor p{
  margin-bottom: 0px !important;
}
.hihello
{
  color: red;
  font-weight: 700;
  text-align: center;
}
</style>
<section id="section-content" class="content page-1 moto-section details_container bg_gray" data-widget="section" data-container="section">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-sm-9">
              <div class="row found-result">
                <ul class="breadcrumb">
                  <li><a href="<?php echo HTTP_ROOT?>">Home</a></li>
                  <li><a href="<?php echo HTTP_ROOT?>/search/<?php echo @$college['City']['city_name']?>"><?php echo @$college['City']['city_name']?></a></li>
                  <li><a href="<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>"><?php echo @$college['College']['name']?></a></li>
                </ul>
              </div>
              <div class="row border_box">
                <div class="col-md-12 col-sm-12">
                  <div class="show-list show-list_21">
                    <div class="row">
                      <div class="doctor-section">
                        <div class="doctor-details">
                          <div class="col-md-3 col-sm-3">
                            <div class="doctor-photo" college-id="<?php echo $college['College']['id']?>" college-name="<?php echo $college['College']['name']?>">
                              <img src="<?php echo HTTP_ROOT.'img/collegeImages/large/'.$college['College']['image']?>" class="img-responsive wow bounceInDown InDown">
                              <div class="margntopbtm">
                                
                              </div>
                            </div>
                            
                          </div>
                          <div class="col-md-6 col-sm-6">
                            <div class="about-doctor">
                              <div class="misc-title" style="margin-bottom:0px;">
                                <a href="javascript:void(0);"><?php echo @$college['College']['name']?>,<?php echo @$college['City']['city_name']?></a>
                              </div>
                              <p><b><?php echo @$college['CollegeMeta']['college_type'] == '0' ? 'Government' : 'Private';?> | Established <?php echo @$college['CollegeMeta']['Establishment']?></b></p>
                              <p><b>UGC Approved</b>   <i class="fa fa-info-circle" aria-hidden="true"></i></p>
                              <p><b><?php echo @$college['CollegeMeta']['college_type'] == '0' ? 'Government' : 'Private';?> University</b>   <i class="fa fa-info-circle" aria-hidden="true"></i></p>
                              <p><b>Member of AIU</b>   <i class="fa fa-info-circle" aria-hidden="true"></i></p>
                             <!-- <p><?php echo @$college['CollegeMeta']['description']?> <a href="#">More..</a>
                              </p>-->
                             <!-- <p><b>Application Fees</b>: <span class="ori-fee">5,000</span><span class="dis-fee dis-fee_1"> 1000</span></p>-->
                           
                              <div class="icon_colo">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3 col-sm-3">
                            <div class="navigation" >
                              <h4 style="margin-top: 0px;">Share this profile</h4>
                              <ul style="display: inline-flex;list-style: none;" class="ft-sm social-network social-circle desc-sm">
                                <li style="padding-bottom: 2px;">
                                    <a href="https://facebook.com/sharer/sharer.php?u=<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>" class="icoFacebook" style="background: #008190 none repeat scroll 0;border-radius: 12%;"><i class="fa fa-facebook" style="margin-left: 20%;"></i></a>
                                </li>
                                <li style="padding-bottom: 2px;">
                                    <a href="https://twitter.com/share?url=<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>" class="icoTwitter" style="background: #008190 none repeat scroll 0;border-radius: 12%;"><i class="fa fa-twitter" style="margin-left: 20%;"></i></a>
                                </li>
                                <li style="padding-bottom: 2px;">
                                    <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>" class="icoLinkedin" style="background: #008190 none repeat scroll 0;border-radius: 12%;"><i class="fa fa-linkedin" style="margin-left: 20%;"></i></a>
                                </li>
                            </ul>
                              <!-- <p class="Brochure Brochure6"><i class="fa fa-thumbs-up Feedback1"></i><strong class="Feedback1"> 97%</strong> 301 Votes</p> -->
                              <!-- <p class="Brochure Brochure7"><i class="fa fa-hand-rock-o"></i> 112 Feedback</p> -->
                              <!-- <p class="Brochure Brochure8"><i class="fa fa-id-card-o"></i> Rohni Delhi</p> -->
                              <!-- <p class="Brochure Brochure9"><strong>College Fees</strong></p> -->
                              <!-- <p><a href="#" class="btn btn-info">Notify me / Apply</a></p> -->
                              <!-- <h4>Share with</h4>
                              <a target="_blank" href="https://twitter.com/share?url=<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>" class="btn btn-success1 wd_200"><i class="fa fa-twitter">  Twitter</i></a>
                              <a  target="_blank" href="https://facebook.com/sharer/sharer.php?u=<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>" class="btn btn-success1 wd_200"><i class="fa fa-facebook"></i> Facebook</i></a>
                              <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>" class="btn btn-success1 wd_200"><i class="fa fa-linkedin"></i> LinkedIn</i></a> -->
                            </div>
                            <div class="add_compair"><a href="javascript:void(0)" class="colg-cmp" id="<?php echo $college['College']['id']?>" style="<?php echo @in_array($college['College']['id'], $this->Session->read('clgCompare')) ? 'color:blue' : '';?>"><i class="fa fa-exchange"></i><?php echo @in_array($college['College']['id'], $this->Session->read('clgCompare')) ? 'Added to Compare' : 'Add to Compare';?></a></div>
                                <div class="add_compair">
                                
                                  <!--<a target="_blank" href="<?php 
                                                              foreach($college['CollegeCourse'] as $clgcrs){
                                                                if($clgcrs['course_id'] == $this->Session->read('selectedCourse')){
                                                                    echo HTTP_ROOT.'Homes/downloadBrochure/'.$clgcrs['brochure'];
                                                                }
                                                              }
                                                        ?>"><i class="fa fa-arrow-circle-down"></i> Download Brochure</a> -->
                                  <a data-toggle="modal" data-target="#dwnloaddrchr" href="javascript:void(0);">
                                    <i class="fa fa-arrow-circle-down"></i> Download Brochure
                                  </a>
                                </div>
                                <?php 
                                if(!empty($this->Session->read('selectedCourse'))){
                                  foreach($college['CollegeCourse'] as $clgcrs){
                                    if($clgcrs['course_id'] == $this->Session->read('selectedCourse')){?>
                                  <?php 
                                   if($clgcrs['from_date'] <= date('Y-m-d') && $clgcrs['to_date'] >= date('Y-m-d')){?>
                                    <!-- <button class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm">Apply</button> -->
                                  <?php }else{?>
                                    <!-- <button style='margin-top: 4%;' class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm" data-toggle="modal" data-target="#notifyModal">Notify me </button> -->
                                  <?php }?> 
                                
                                <?php }}}else{?>
                                  <!-- <button data-toggle="tab" href="#my_tab" class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm">Apply</button> -->
                                <?php }?>  
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="doctor-details award-mr-top pd-bot-no" >
                            <ul class="nav nav-tabs">
                              <!-- <li class="active"><a data-toggle="tab" href="#Home" class="">Home</a></li>
                              <li><a data-toggle="tab" href="#my_tab" class="">Courses</a></li>
                              <li><a data-toggle="tab" href="#placement_tab" class="">Placement</a></li>

                              <li><a data-toggle="tab" href="#Facilities_tab" class="">Facilities</a></li>
                              <li><a data-toggle="tab" href="#infrastructure_tab" class="">Infrastructure</a></li>
                              
                              <li><a data-toggle="tab" href="#Gallery" class="">Gallery</a></li>
                              <li><a data-toggle="tab" href="#near_tab" class="">Near By</a></li>
                              <li><a data-toggle="tab" href="#my_tab1" class="">Contact Us</a></li> -->
                              <!-- <li><a data-toggle="tab" href="#my_tab2" class="">Reviews</a></li> -->
                            </ul>
                            <div class="tab-content">
                              <div id="Home" class="tab-pane fade in active">
                                <div class="galry_section">
                                  <h2 class="moto-text_system_6">Why Join <?php echo @$college['College']['name']?></h2>
                                  <?php echo @$college['CollegeMeta']['about']?>
                                </div>
                              </div>
                              <div id="placement_tab" class="tab-pane fade in">
                                <div class="galry_section">
                                <h2 class="moto-text_system_6">Placement Details</h2>
                                  <?php echo @$college['CollegeMeta']['placement']?>
                                </div>
                              </div>
                              <div id="infrastructure_tab" class="tab-pane fade in">
                                <div class="galry_section">
                                <h2 class="moto-text_system_6">Infrasturcture</h2>
                                  <?php echo @$college['CollegeMeta']['infrasturcture']?>
                                </div>
                              </div>
                              <div id="near_tab" class="tab-pane fade in">
                                <div class="galry_section">
                                  <h2 class="moto-text_system_6">Near By Locations</h2>
                                  <div class="col-md-4">
                                  <label>Nearest Hospitals</label>
                                  <p><?php echo @$college['CollegeNear']['near_hospital']?></p>
                                  </div>
                                  <div class="col-md-4">
                                  <label>Nearest Railway Station</label>
                                  <p><?php echo @$college['CollegeNear']['near_railway']?></p>
                                  </div>
                                  <div class="col-md-4">
                                  <label>Nearest Metro Station</label>
                                  <p><?php echo @$college['CollegeNear']['near_metro']?></p>
                                  </div>
                                  <div class="col-md-4">
                                  <label>Nearest Bus Stand</label>
                                  <p><?php echo @$college['CollegeNear']['near_bus']?></p>
                                  </div>
                                  <div class="col-md-4">
                                  <label>Nearest Bank</label>
                                  <p><?php echo @$college['CollegeNear']['near_bank']?></p>
                                  </div>
                                  <div class="col-md-4">
                                  <label>Nearest Police Station</label>
                                  <p><?php echo @$college['CollegeNear']['near_police']?></p>
                                  </div>
                                </div>
                              </div>
                              <div id="Facilities_tab" class="tab-pane fade in">
                                <div class="galry_section">
                                  <h2 class="moto-text_system_6">Facilities</h2>
                                  <ul>
                                    <?php if(!empty($college['CollegeFacility'])){
                                                        foreach($college['CollegeFacility'] as $fac){
                                                ?>
                                                  <li>
                                                     <?php echo $fac['Facility']['name']?>
                                                  </li>
                                                <?php   }
                                                      }
                                                ?> 
                                  </ul>
                                </div>
                              </div>
                              <div id="my_tab" class="tab-pane fade in " style='border:none;'>
                                <ul class="nav nav-tabs marg_bot" >
                                <?php $j=0;foreach($streams as $stream){?>
                                  <li class="<?php echo $j==0 ? 'active' : '';?>"><a data-toggle="tab" href="#<?php echo 'stream-'.$stream['Stream']['id']?>" class="bg-co_lo"><?php echo $stream['Stream']['stream'];?></a></li>
                                <?php $j++;}?>  
                                  <!-- <li><a data-toggle="tab" href="#PG" class="bg-co_lo">PG</a></li>
                                  <li><a data-toggle="tab" href="#PH" class="bg-co_lo">PH.D</a></li>
                                  <li><a data-toggle="tab" href="#Diploma" class="bg-co_lo">Diploma</a></li> -->

                                </ul>
                                <div class="tab-content">
                                  <?php foreach($streams as $strm){
                                  ?>        
                                  <?php foreach($college['CollegeCourse'] as $crs){
                                            if($crs['stream_id'] == $strm['Stream']['id']){
                                              //pr($crs);die;
                                          ?>
                                    <div class="col-md-6">
                                            <div id="stream-<?php echo $strm['Stream']['id']?>" class="tab-pane fade in active border_bot" style='border:none;'>
                                    
                                              <div class="main_tab border_bot_2">
                                                <div class="row">
                                                  <div class="col-md-8 col-sm-8">
                                                    <div class="doctor-location award-bdr pad-top-15">
                                                      <h2 class="moto-text_system_6"><?php echo @$crs['Specialization']['specialization'].' in '.@$crs['Course']['course'];?></h2>
                                                      <p class="electronics_2"><?php echo @$crs['Course']['duration'].' years  &nbsp &nbsp '.@$crs['Program']['program'];?></p>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-4 col-sm-4">
                                                    <div class="doctor-location pad-top-15">
                                                      <div class="button_teco">
                                                      <?php if(($crs['from_date'] <= date('Y-m-d')) && ($crs['to_date'] >= date('Y-m-d'))){?>
                                                        <!-- <p class="App"><a href="#" class="btn btn-success1"> Apply Now </a></p> -->
                                                        <?php }else{?>
                                                          <!-- <button style='margin-top: 4%;' class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm" data-toggle="modal" data-target="#notifyModal">Notify me </button> -->
                                                         <?php }?> 
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                                 
                                            </div>
                                            </div>
                                        <?php   }
                                          }
                                      ?> 
                                 <?php
                                        }
                                  ?>
                                  
                                </div>
                              </div>
                              <div id="my_tab1" class="tab-pane fade">
                                <div class="col-md-6 col-sm-6">
                                  <h2 class="moto-text_system_6">Contact Us</h2>
                                  <form class="tab_contact" id="clg-cnt-us" method="post">
                                    <i class="fa input-error"></i>
                                    <input type="text" class="form-control required" placeholder="Name" name="name" value="<?php echo @$this->Session->read('Student.name')?>">
                                    <input type="hidden" class="form-control required" placeholder="Name" name="college_id" value="<?php echo $college['College']['id']?>">
                                    <input type="hidden" class="form-control required" placeholder="Name" name="college_email" value="<?php echo $college['College']['email']?>">
                                    <i class="fa input-error"></i>
                                    <input type="email" class="form-control required" placeholder="Email" name="email" value="<?php echo @$this->Session->read('Student.email')?>">
                                    <input type="text" class="form-control" placeholder="Subject" name="subject">
                                    <i class="fa input-error"></i>
                                    <textarea class="form-control required" cals="40" row="3" placeholder="Your Message" name="message"></textarea>
                                    <button type="submit" class="btn btn-success1">Submit</button>
                                  </form>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                  <h2 class="moto-text_system_6">Our Address</h2>
                                  <h4><?php echo @$college['College']['address']?><br> <?php echo $college['City']['city_name']?> ,<?php echo $college['State']['statename']?> ,<br><?php echo $college['Country']['country_name']?>,<?php echo $college['College']['pincode']?></h4>
                                </div>
                              </div>
                             
                              <div id="Gallery" class="tab-pane fade">
                                <div class="galry_section">
                                  <?php if(!empty($college['CollegeGallery'])){
                                          foreach($college['CollegeGallery'] as $gal){
                                  ?>
                                  <img src="<?php echo HTTP_ROOT.'img/gallery/large/'.$gal['image']?>" class="img-responsive mg-respons" />
                                  <?php    }
                                        }

                                  ?>
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="doctor-details award-mr-top">
                          <div class="col-md-6 col-sm-6">
                            <div class="doctor-location award-bdr pad-top-15">
                              <div class="misc-title">
                                <h4 class="misc-title_h4color"><a href="#"><strong>Review &amp; Rating</strong></a></h4>
                                <div class="row">
                                  <div class="col-md-4 col-sm-4">
                                    <img src="<?php echo HTTP_ROOT?>frontend/star.png" class="img-responsive">
                                    <p>Average Rating Based on <?php echo count($college['Review'])?> rating</p>
                                  </div>
                                  <div class="col-md-8 col-sm-8">
                                    <div class="progress-setting">
                                      <div class="progress">
                                        <div class="progress-bar progress-bar_color" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"> 5 star </div>
                                      </div>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar_color" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;"> 4 star </div>
                                      </div>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar_color" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"> 3 star </div>
                                      </div>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar_color" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;"> 2 star </div>
                                      </div>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar_color" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;"> 1 star </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6">
                            <div class="doctor-location pad-top-15">
                              <?php if($this->Session->check('Student')){?>
                              <a data-target="#review-modal" data-toggle="modal" href="javascript:void(0);" class="btn btn-success1 pull-right">Write a review</a>
                              <?php }else{?>
                                <a data-target="#myModal" data-toggle="modal" href="javascript:void(0);" class="btn btn-success1 pull-right">Write a review</a>
                              <?php }?>
                              <div class="misc-title">
                                <h4><a href="#"><strong>Refine Review</strong></a></h4>
                                <p>4 and 5 star rated certified Student review</p>
                                <p>3 and 2 star rated certified Student review</p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="top-review">
                          <div class="top-review-title">
                            <h2>TOP REVIEWS</h2>
                            <!-- <a href="#">View All Top Review(415) </a>  -->
                          </div>
                          <div class="top-review-slogan">
                            <p class="">Top review list the most relevant product review only. 
                              <!-- <a href="#">Show All</a> Instead? <span class="pull-right">Expand all the Review</span> -->
                            </p>
                          </div>
                          <?php if(!empty($college['Review'])){
                           foreach($college['Review'] as $review){?>
                          <div class="rating-status">
                            <div class="row">
                              <div class="col-md-2 col-sm-2">
                                <div class="rating-person">
                                  <p><?php echo $review['name']?></p>
                                  <p><?php 
                                             $date=date_create(@$review['created_date']);
                                             echo DATE_FORMAT($date,'d M Y')?></p>
                                             <?php ?></p>
                                </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                <div class="rating-details">
                                  <div class="left_part">
                                    <p><b><?php echo $review['title']?></b></p>
                                  </div>
                                  <div class="left_part texr_right">
                                    Share With: 
                                    <a target="_blank" href="https://twitter.com/share?url=<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>"><i class="fa fa-twitter"> </i></a>
                                    <a  target="_blank" href="https://facebook.com/sharer/sharer.php?u=<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>"><i class="fa fa-facebook"></i></a>
                                    <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo HTTP_ROOT?>college/<?=@$college['College']['name'].'-'.$college['College']['id']?>"><i class="fa fa-linkedin"></i></a>
                                  </div>
                                  <div class="clearfix"></div>
                                  <p><?php echo $review['review']?></p>
                                  <p>Like (<span id="like-count-<?php echo $review['id']?>"><?php echo $review['ReviewCount']['review_like']?></span>) , Dislike(<span id="dislike-count-<?php echo $review['id']?>"><?php echo $review['ReviewCount']['review_dislike']?></span>)</p>
                                  <?php if($this->Session->check('Student') && $this->Session->read('review_user') != $this->Session->read('Student.id')){?>
                                  <p id="para-<?php echo $review['id']?>">Was this review helpful? <a class="like-count" href="javascript:void(0)" data-id="<?php echo $review['id']?>"><span  class="glyphicon glyphicon-thumbs-up blue"></span></a><strong>&nbspYes </strong> <a class="dislike-count" href="javascript:void(0)" data-id="<?php echo $review['id']?>"><span class="glyphicon glyphicon-thumbs-down red"></span></a><strong>&nbspNo</strong></p>
                                  <?php }?>
                                  <p>100% of 12 user found this review helpful.</p>
                                </div>
                              </div>
                            </div>
                          </div><hr>
                          <?php }}else{?>
                            <p>Be the first to provide review tothis college.</p>
                          <?php }?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="search-categories">
                <div class="go-back hidden-xs"> <!-- <a onclick="history.go(-1);" class="btn btn-success1"><i class="fa fa-angle-double-left"></i> Back to search results</a>  --></div>
                <div class="looking-for">
                  <div class="looking-for-title">
                    <h4>Didn't find who you're looking for?</h4>
                    <p><strong>Recommendations :</strong></p>
                  </div>
                  <?php 
                      if(!empty($suggestions)){
                        foreach($suggestions as $suggest){?>
                        <div class="looking-for-doctor for-doctor">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="look-img"> <img src="<?php echo HTTP_ROOT.'img/collegeImages/large/'.$suggest['College']['image']?>" class="img-responsive wow bounceInRight"> </div>
                            </div>
                            <div class="col-md-8">
                              <div class="find-name">
                                <a href="<?php echo HTTP_ROOT?>college/<?=@$suggest['College']['name'].'-'.$suggest['College']['id']?>" class="color_head_2"> <?php echo $suggest['College']['name']?> </a>
                                <p class="color_font-s"> <?php echo @$suggest['College']['shortname']?> <?php echo $suggest['City']['city_name']?></p>
                              </div>
                            </div>
                          </div>
                        </div>
                  <?php   }
                        }
                  ?>
                  <div class="looking-for-doctor">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="find-name"> <a href="<?php echo HTTP_ROOT?>Homes/search" class="btn btn-success1">Viwe More <i class="fa fa-angle-double-right"></i></a> </div><?php //pr($this->Session->read());?>
                      </div>
                    </div>
                  </div>
                  <div class="map_12">
                    <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
                    <div style='overflow:hidden;height:450px;width:100%'>
                      <?php if(!empty($college['College']['address'])){?>
                        <iframe width="100%" height="300" style="border:0" src="https://maps.google.com/maps?hl=en&q=<?php echo @$college['College']['name'].' . '.@$college['College']['address'].','.$college['City']['city_name'].','.$college['State']['statename'].',India'.$college['College']['pincode']?>&ie=UTF8&t=roadmap&z=10&iwloc=B&output=embed"></iframe>
                      <?php }else{?>  
                      <div id='gmap_canvas' style='height:450px;'></div>
                      <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
                      <script type='text/javascript'>function init_map(){var myOptions = {zoom:10,center:new google.maps.LatLng(<?php echo $college['CollegeMeta']['latitude']?>,<?php echo $college['CollegeMeta']['longitude']?>),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(<?php echo $college['CollegeMeta']['latitude']?>,<?php echo $college['CollegeMeta']['longitude']?>)});infowindow = new google.maps.InfoWindow({content:'<strong<?php echo $college['College']['name']?></strong><br><?php echo $college['City']['city_name']?>, India<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
                      <?php } ?>
                    </div>
                   
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>




    <!-----------------------write a review ---------------------------->
    

            <div id="review-modal" class="modal fade" role="dialog">
              <div class="modal-dialog" style="width:70%;">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center" style="color:#000 !important"><?php echo @$college['College']['name']?>,<?php echo @$college['City']['city_name']?></h4>
                  </div>
                  <div class="modal-body">
                    <div class="login-box-body">
                      <div class="row">
                        <div class="col-xs-12">
                          
                            <form id="clg-rvw" method="post" enctype="multipart/form-data">
                              <input type="hidden" class="form-control required" placeholder="Name" value="<?php echo @$this->Session->read('Student.name')?>" name="name">
                              <input type="hidden" value="<?php echo $college['College']['id']?>" name="college_id">

                                <div class="col-md-12">
                                    
                                    <div class="row">
                                      <div class="col-md-12">
                                          <p><b>Your experiences really help other students.Thanks!</b></p>
                                      </div>
                                    </div> 
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class=" found-result">
                                                <ul class="rate-ul">
                                                  <li><p>You are reviewing as a</p></li>
                                                  <li class="rate-li"><p><input type="radio" class="required" name="review_as" value="0">Teacher</p></li>
                                                  <li class="rate-li"><p><input type="radio" class="required" name="review_as" value="1">Parent</p></li>
                                                  <li class="rate-li"><p><input type="radio" class="required" name="review_as" value="2">Alumni</p></li>
                                                </ul>
                                            </div>
                                        </div>    
                                    </div> 
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label class="control-label">Title<span class="require" aria-required="true">*</span></label>
                                            <div class="input-icon right">
                                                          <i class="fa"></i>
                                              <input type="text" placeholder="Title" class="form-control required" name="title">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label class="control-label">Your Rating<span class="require" aria-required="true">*</span></label>
                                            <div class="input-icon right">
                                                          <i class="fa"></i>
                                              <input  data-step=1 name="rating" class="rating-loading common-rating required" data-show-clear="false" data-show-caption="false" data-size="xs">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-4 ">
                                          <div class="col-md-12 rate-box">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>Academic</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="rate-head-inline">Teacher's Competency:
                                                       <span>
                                                           <input  data-step=1 name="teacher_competency" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                       </span> 
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="rate-head-inline">Academic Rigour:
                                                        
                                                    </p>
                                                    <p class="pull-right">
                                                           <input  data-step=1 name="academic_rigour" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                       </p> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="rate-head-inline">Practical Training:
                                                        
                                                    </p>
                                                    <p class="pull-right">
                                                           <input  data-step=1 name="practical_training" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                       </p> 
                                                </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="col-md-4 ">
                                          <div class="col-md-12 rate-box">
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <h4>Co-Curricular</h4>
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <p class="rate-head-inline">Activities:
                                                          
                                                      </p>
                                                      <p class="pull-right">
                                                             <input  data-step=1 name="activities" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                         </p> 
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <p class="rate-head-inline">Instructor's Competence:
                                                         <span>
                                                             <input  data-step=1 name="instructor_competence" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                         </span> 
                                                      </p>
                                                       
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <p class="rate-head-inline">&nbsp;
                                                      &nbsp;
                                                      &nbsp;
                                                      &nbsp;
                                                      </p>
                                                      
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-4 ">
                                          <div class="col-md-12 rate-box">
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <h4>Value for Money</h4>
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <p class="rate-head-inline">Total Fee's:
                                                          
                                                      </p>
                                                      <p class="pull-right">
                                                             <input  data-step=1 name="total_fees" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                         </p> 
                                                      
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <p class="rate-head-inline">Cost Transparency:
                                                          
                                                      </p>
                                                      <p class="pull-right">
                                                             <input  data-step=1 name="cost_transparency" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                         </p> 
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <p class="rate-head-inline">Placement:
                                                          
                                                      </p>
                                                      <p class="pull-right">
                                                             <input  data-step=1 name="placement" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                         </p> 
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="row rate-mt-5">
                                        <div class="col-md-4 ">
                                                <div class="col-md-12 rate-box">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h4>Administration</h4>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="rate-head-inline">Management:
                                                                
                                                            </p>
                                                            <p class="pull-right">
                                                               <input  data-step=1 name="management" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                           </p> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="rate-head-inline">Parental Involvement:
                                                                
                                                            </p>
                                                            <p class="pull-right">
                                                               <input  data-step=1 name="parental_involvement" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                           </p> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="rate-head-inline">Safety & Hygiene:
                                                                
                                                            </p>
                                                            <p class="pull-right">
                                                               <input  data-step=1 name="safety_hygiene" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                           </p> 
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="col-md-8 ">
                                            <div class="col-md-12 rate-box">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h4>Infrastructure</h4>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="rate-head-inline">Campus:
                                                                
                                                            </p>
                                                            <p class="pull-right">
                                                               <input  data-step=1 name="campus" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                           </p> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="rate-head-inline">Sports:
                                                                
                                                            </p>
                                                            <p class="pull-right">
                                                               <input  data-step=1 name="sports" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                           </p> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="rate-head-inline">Canteen:
                                                                
                                                            </p>
                                                            <p class="pull-right">
                                                               <input  data-step=1 name="canteen" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                           </p> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h4>&nbsp;&nbsp;</h4>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="rate-head-inline">Classrooms:
                                                                
                                                            </p>
                                                            <p class="pull-right">
                                                               <input  data-step=1 name="classrooms" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                           </p> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="rate-head-inline">Transport:
                                                                
                                                            </p>
                                                            <p class="pull-right">
                                                               <input  data-step=1 name="transport" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                           </p> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="rate-head-inline">Labs:
                                                                
                                                            </p>
                                                            <p class="pull-right">
                                                               <input  data-step=1 name="labs" class="rating-loading common-rating" data-show-clear="false" data-show-caption="false" data-size="xs">
                                                           </p> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>
                                    <div class="row rate-mt-5">
                                        <div class="form-group col-md-12">
                                            <label class="control-label">Write your reviewing<span class="require" aria-required="true">*</span></label>
                                            <div class="input-icon right">
                                                          <i class="fa"></i>
                                              <textarea class="form-control required" name="review" placeholder="Review"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p><input type="checkbox" class="required" name="ok" value="0">
                                            <span> I certify this review is based on my personal experience and is my genuine opinion of this college, and that i have no business relationship with establishment, and have not been offered any incentive or payment originating from the establishment to write this review. This review is in accordance with Formsadda's Content Guidelines and Terms of Use and i understand that Formsadda has a zero-tolerance policy on fake reviews. </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="box-footer col-md-12">
                                        <button type="submit" class="btn btn-primary" style="margin-left: 43%;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          
        

            <div class="modal fade" id="dwnloaddrchr" style="margin-top: 18%; width: 20%; margin-left: 50%;" role="dialog">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <form action="" id="userlogin" method="post">
                    <div class="modal-body hello">
                      <div class="row">
                        <p class="hihello">Brochure is not available.</p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
        <style>
            .rate-box{
                border: 1px solid lightgrey;
                background: rgba(77, 144, 254, 0.08);
                min-height: 158px;
            }
            .rate-mt-5{
                margin-top: 5px;
            }
            .rating-xs {
                font-size: 1em;
            }
            .rate-head-inline{
                display: inline-flex;
            }
            .rate-ul{
                padding: 8px 15px;
                margin-bottom: 20px;
                list-style: none;
                background-color: #f5f5f5;
                border-radius: 4px;
            }
            .rate-ul > li{
                display: inline-block;
                
            }
            .rate-li{
                margin-left:20px;
            }
        </style>



   <!----------------------write a review ends---------------------------------------->                
      <style>
      .desc-sm li a {
    background: #b2a9a9 none repeat scroll 0 0!important;
    border-radius: 12%!important;
    float: left;
    height: 20px;
    line-height: 24px;
    margin: 0 4px;
    padding: 0;
    width: 20px !important;
}
/*.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 150%;
    left: 50%;
    margin-left: -60px;
}

.tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: black transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}*/
</style>
     
 