
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<div class="container main_cont">
		<div class="panel panel-default">
		  <div class="panel-body">
			  <div class="row">
					<div class="col-md-4"><div style="text-align:right;"><b>Step 1 - Select Courses</b></div></div>
					<div class="col-md-4"><div style="text-align:center"><div style="text-align:center;">Step 2 - Confirm Details</div></div></div>
					<div class="col-md-4" ><div style="text-align:left;">Step 3 - Make Payment</div></div>
				</div>	
				<br>
				<div class="row">
					<div class="col-md-4"><span style="float:right; margin-right:21%;" class="active_page" onclick=""></span><div><a onclick="history.go(-1)" class="btn btn-default"><span class="glyphicon glyphicon-triangle-left"></span>Back to List</a></div></div>
					<div class="col-md-4"><div style="text-align:center"><span class="dot" onclick=""></span></div></div>
					<div class="col-md-4" ><span style="float:left; margin-left:21%;" class="dot" onclick=""></span></div>
				</div>
			</div>		
		</div>
	<div class="row">
	<form method="post" action="">
		<div class="col-md-12">
			<div class="well">
				<div class="row">
					<div class="col-md-9 Main_Head_Orders"><h3>Colleges by FormsADDA</h3><br></div>
					<div class="col-md-2 Continue_shopp"><a href="<?php echo HTTP_ROOT?>search/<?php echo empty($selectedCourse) ? "" : "?course=".$selectedCourse['Course']['course'].'-'.$this->Session->read('selectedCourse');;?>" class="btn btn-success">Continue</a></div>
				</div>
				
				
				<div class="row">
					<div class="col-md-1"></div>
						<div class="col-md-3"><b>Colleges</b></div>
						<div class="col-md-2"><b>Specialization</b></div>
						<div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>Form Fees</b></div></div>
						<div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>FormsADDA Fees</b></div></div>
						<div class="col-md-1 Head_Fees"><div width="100%" style="text-align:centre;"><b>Action</b></div></div>
					</div>
					<br>
					<?php  $i=0; if(!empty(@$college_courses)){  foreach($college_courses as $college_course){
						if(@$college_course['CollegeCourse']['direct_apply'] == 0){?>
					<div class="row" id="row-id-<?php echo $college_course['CollegeCourse']['id']?>">
						<div class="col-md-1"></div>
						<div class="col-md-3"><?php echo $college_course['College']['name']?></div>
						<div class="col-md-2">
							<input type="hidden" name="data[<?php echo $i?>][college_course_id]" value="<?php echo $college_course['CollegeCourse']['id']?>">
							<select class="form-control specialization-sel" multiple name="data[<?php echo $i?>][specialization_id][]">
								<option value="" seletecd disabled> Select Specializations</option>
								<?php foreach($college_course['CourseSpecialization'] as $specs){?>
									<option value="<?php echo $specs['Specialization']['id']?>"> <?php echo $specs['Specialization']['specialization']?></option> 
								<?php }?>
							</select>
						
						</div>
						<div class="col-md-2"><div width="100%" class="Price"><strike>Rs.<?php echo $college_course['CollegeCourse']['registration_fee']?></strike></div></div>
						<div class="col-md-2"><div width="100%" class="Price">Rs.<?php echo $college_course['CollegeCourse']['formsadda_cost']?></div></div>
						<div class="col-md-1"><div width="100%" class="Price"><button data-clg-course-id="<?php echo $college_course['CollegeCourse']['id']?>" data-cost="<?php echo base64_encode($college_course['CollegeCourse']['formsadda_cost']);?>" class="btn btn-default btn_delete cart-delete"><span class="glyphicon glyphicon-remove-sign"></span></button></div></div>
					</div>
					<br>
					<?php $i++; @$tot = @$tot + $college_course['CollegeCourse']['formsadda_cost']; } }}?>

				</div>
				
		  </div>
		</div>

		
		

			<div class="row">
				<div class="col-md-8">
				<div class="well">
					<div class="row" width="100%"><div class="Main_Head_Orders"><h4>Direct apply to colleges</h4><br></div></div>
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-4"><b>Colleges</b></div>
						<div class="col-md-3"><div width="100%" class="Price"></div></div>
						<div class="col-md-3"><b>Link</b></div>
					</div>
					<br>
					<?php if(!empty(@$college_courses)){  foreach($college_courses as $college_course){ if(@$college_course['CollegeCourse']['direct_apply'] == 1){?>
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-4"><?php echo $college_course['College']['name']?></div>
						<div class="col-md-3"><div width="100%" class="Price"></div></div>
						<div class="col-md-3"><div width="100%" class="Price Apply_links"><a href="<?php echo @$college_course['CollegeCourse']['link']?>" target="_blank">Apply Now</a></div></div>
					</div>
					<br>
					<?php } } }?>
					<!-- <div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-4">GL Bajaj Institute of Technology & Management</div>
						<div class="col-md-3"><div width="100%" class="Price"></div></div>
						<div class="col-md-3"><div width="100%" class="Price Apply_links"><a href="#">Apply Now</a></div></div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-4">Galgotias - College of Engineering & Technology</div>
						<div class="col-md-3"><div width="100%" class="Price"></div></div>
						<div class="col-md-3"><div width="100%" class="Price Apply_links"><a href="#">Apply Now</a></div></div>
					</div> -->
				</div>
				
				</div>
			<div class="col-md-4">
				<div class="well">
					<div class="row" width="100%"><div class="Head_Fees"><h4>Price Details</h4><br></div></div>
					<div class="row Charges">
						<div class="col-md-6"><div>Amount:</div></div>
						<div class="col-md-6"><div>Rs. <span class="tot-cart"><?php echo @$tot;?></span></div></div>
					</div>
					<br>
					<div class="row Charges">
						<div class="col-md-6"><div>No of Forms:</div></div>
						<div class="col-md-6 Item_no"><div><span id="nof-cart"><?php echo $i;?></span></div></div>
					</div>
					<br>
					<div class="row divide" width="100%">..................................................</div>
					<br>
					<div class="row Charges">
						<div class="col-md-6"><div><b>Net Payable Amount: </b></div></div>
						<div class="col-md-6"><div><b>Rs. <span class="tot-cart"><?php echo @$tot;?></span></b></div></div>
					</div>
					<br>
					<?php if($this->Session->check('Student')){?>
					<?php if($count != 0){?>
					<div class="row" width="100%"><div class="Pay_button"><button class="btn btn-warning" type="submit">Proceed to Checkout</button></div></div>
					<?php } else{?>
					<div class="row" width="100%"><div class="Pay_button"><a href="javascript:void(0);" class="btn btn-warning">Proceed to Checkout</a></div></div>
					<?php } } else{?>
					<div class="row" width="100%"><div class="Pay_button"><button type="button" class="btn btn-warning" data-target="#my_Login12" data-toggle="modal" data-keyboard="true" href="#myModal">Proceed to Checkout</button></div></div>
					<?php }?>
				</div>
				
				<div class="row final_row">
					<div class="col-md-3"><img class="shield" src="<?php echo HTTP_ROOT?>/img/collegeImages/thumbnail/shield_a7ea6b.png" alt="shield"></div>
					<div class="col-md-9"><div class="secure_message">Safe & Secure Payments</div></div>
				</div>
			</div>
	</form>		
	</div>

	</div>


		
	</div>
	

<script>
    $(document).ready(function() {
        $('.specialization-sel').select2();
    });
  </script>
    <script type="text/javascript">
  //$('select').select2();
</script>
