<style>
  a.fafa-icons{
      margin-left: 10px;
}
.fa{
  font-size: 24px;
}
</style>
<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
  <div class="p-lr-50 p-tb-20">
      <div class="row">
      <?php echo $this->element('frontEnd/studentsidebar');?>
      <div class="col-md-9">
              <div class="profile-content">
             
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Members
        
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            
            <!-- <form method="post" action="<?php echo HTTP_ROOT?>Forms/generateForm"> -->
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($members as $data){?>
                <tr id="stu-<?php echo $data['Student']['id']?>">
                  <td style="text-align: center;">
                    <img src="<?php echo HTTP_ROOT.'img/studentImages/small/'.$data['Student']['image']?>" width="18%">
                  </td>
                  <td>
                    <?php echo $data['Student']['name']?>
                  </td>
                  <td>
                    <?php echo $data['Student']['email']?>
                  </td>
                   <td>
                    <?php echo $data['Student']['mobile']?>
                  </td>
                  
                  <td>
                    <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Homes/addMember/'.base64_encode($data['Student']['id'])?>"><i class="fa fa-edit"></i></a> 
                    <a class="fafa-icons delete-row" title="Delete" model = "Student" data-id="<?php echo $data['Student']['id']?>" href="javascript:void(0)"><i class="fa fa-trash"></i></a>  
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
               
            </div>
            
          </div>
          <!-- </form> -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">

   $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Homes/studentDelete/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         $('#stu-'+id).remove();
        
      }
  });

       
   })
</script>
              </div>
      </div>
    </div>
  </div>
</section>