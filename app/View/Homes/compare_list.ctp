<style>

</style>
<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
  <div class="p-lr-50 p-tb-20">
      <div class="row">
      <?php echo $this->element('frontEnd/studentsidebar');?>
      <div class="col-md-9">
              <div class="profile-content">
             
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Compares
        
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            
            <!-- <form method="post" action="<?php echo HTTP_ROOT?>Forms/generateForm"> -->
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Compare count</th>
                  <th>Created date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($comps as $data){?>
                <tr id="stu-<?php echo $data['Compare']['id']?>">
                  
                  <td>
                    <?php echo count($data['CollegeCompare'])?>
                  </td>
                  <td>
                    <?php echo $data['Compare']['created_date']?>
                  </td>
                  
                  <td>
                    <a class="fafa-icons" title="View" href="<?php echo HTTP_ROOT.'Homes/newCompare/'.base64_encode($data['Compare']['id'])?>"><i class="fa fa-eye" style="font-size:24px;"></i></a> 
                     
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
               
            </div>
            
          </div>
          <!-- </form> -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">

   $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Homes/studentDelete/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         $('#stu-'+id).remove();
        
      }
  });

       
   })
</script>
              </div>
      </div>
    </div>
  </div>
</section>