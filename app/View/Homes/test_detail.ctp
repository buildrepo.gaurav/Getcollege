<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
	<div class="p-lr-50 p-tb-20">
	    <div class="row">
			<?php echo $this->element('frontEnd/studentsidebar');?>
			<div class="col-md-9">
				<div class="profile-content">
				    <section class="content-header">
      <h1>
        Scholarship Test Details
        <small> - APPLY NOW</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Student</a></li>
        <li class="active"><a href='https://www.getcollege.in/Homes/testDetail'>Take Test</a></li>
      </ol>
    </section>
    <?php $qualify1 = 0; $qualify2 = 0; foreach ($score as $ans) {
    	if($ans['StudentAnswer']['test_id'] == 2 || $ans['StudentAnswer']['test_id'] == 3 || $ans['StudentAnswer']['test_id'] == 4){
    		if($ans['StudentAnswer']['percentage'] > 33){
    			$qualify1 = 1;
    		}

    	}
    	if($ans['StudentAnswer']['test_id'] == 5 || $ans['StudentAnswer']['test_id'] == 6 || $ans['StudentAnswer']['test_id'] == 7){
    		if($ans['StudentAnswer']['percentage'] > 33){
    			$qualify2 = 1;
    		}

    	}

    } //echo $qualify1;$qualify2;die;?>
				    <table>
						  <tr>
							<th>Exam Level</th>
							<th>Attempt 1</th>
							<th>Attempt 2</th>
							<th>Attempt 3</th>
						  </tr>
						  <tr>
							<td>Practise Test</td>
							<td><a href='<?php echo HTTP_ROOT?>homes/testInstruction/practise'>Take Test</a></td>
							<td>-</td>
							<td>-</td>
						  </tr>
						  <tr>
							<td>Level 1 Exam</td>
							<td>
								<?php  if(empty($testLevel1)){?>
								<a data-toggle="modal" data-target="#referModal" href='#'>Take Test</a>
								<?php } else{ if($std['Student']['test_taken'] == 0){?>
								<a href="<?php echo HTTP_ROOT.'homes/testInstruction/level1/'.base64_encode(2);?>">Take Test</a>
								<?php } elseif($std['Student']['test_taken'] == 1 || $std['Student']['test_taken'] == 2){?>
								<a href="#" style="color:red;">Close</a>
								<?php } }?>
							</td>
							<td>
								<?php  if(empty($testLevel1)){?>
								<a data-toggle="modal" data-target="#referModal" href='#'>Take Test</a>
								<?php } else{ if($std['Student']['test_taken'] == 1){?>
								<a href="<?php echo HTTP_ROOT.'homes/testInstruction/level1/'.base64_encode(3);?>">Take Test</a>
								<?php } elseif($std['Student']['test_taken'] == 2){?>
								<a href="#" style="color:red;">Close</a>
								<?php } }?>
							</td>
							<td>
								<?php  if(empty($testLevel1)){?>
								<a data-toggle="modal" data-target="#referModal" href='#'>Take Test</a>
								<?php } else{ if($std['Student']['test_taken'] == 2){?>
								<a href="<?php echo HTTP_ROOT.'homes/testInstruction/level1/'.base64_encode(4);?>">Take Test</a>
								<?php } }?>
							</td>
						  </tr>
						  <tr>
							<td>Level 2 Exam</td>
							<td>
								<?php  if(!$qualify1 || empty($testLevel2)){?>
								<a data-toggle="modal" data-target="#test-msg" href='#'>Take Test</a>
								<?php } else{ if($std['Student']['test_taken_level2'] == 0){?>
								<a href="<?php echo HTTP_ROOT.'homes/testInstruction/level2/'.base64_encode(5);?>">Take Test</a>
								<?php } elseif($std['Student']['test_taken_level2'] == 1 || $std['Student']['test_taken_level2'] == 2){?>
								<a href="#" style="color:red;">Close</a>
								<?php } }?>
							</td>
							<td>
								<?php if(!$qualify1 || empty($testLevel2)){?>
								<a data-toggle="modal" data-target="#test-msg" href='#'>Take Test</a>
								<?php } else{ if($std['Student']['test_taken_level2'] == 1){?>
								<a href="<?php echo HTTP_ROOT.'homes/testInstruction/level2/'.base64_encode(6);?>">Take Test</a>
								<?php } elseif($std['Student']['test_taken'] == 2){?>
								<a href="#" style="color:red;">Close</a>
								<?php } }?>
							</td>
							<td>
								<?php if(!$qualify1 || empty($testLevel2) ){?>
								<a data-toggle="modal" data-target="#test-msg" href='#'>Take Test</a>
								<?php } else{ if($std['Student']['test_taken_level2'] == 2){?>
								<a href="<?php echo HTTP_ROOT.'homes/testInstruction/level2/'.base64_encode(7);?>">Take Test</a>
								<?php } }?>
							</td>
						  </tr>
						  <tr>
							<td>Level 3 Exam</td>
							<td>
								<?php if(!$qualify2 || empty($testLevel3) ){?>
								<a data-toggle="modal" data-target="#test-msg" href='#'>Take Test</a>
								<?php } else{ if($std['Student']['test_taken_level3'] == 0){?>
								<a href="<?php echo HTTP_ROOT.'homes/testInstruction/level3/'.base64_encode(8);?>">Take Test</a>
								<?php } elseif($std['Student']['test_taken_level3'] == 1 || $std['Student']['test_taken_level3'] == 2){?>
								<a href="#" style="color:red;">Close</a>
								<?php } }?>
							</td>
							<td>
								<?php if(!$qualify2 || empty($testLevel3) ){?>
								<a data-toggle="modal" data-target="#test-msg" href='#'>Take Test</a>
								<?php } else{ if($std['Student']['test_taken_level3'] == 1){?>
								<a href="<?php echo HTTP_ROOT.'homes/testInstruction/level3/'.base64_encode(9);?>">Take Test</a>
								<?php } elseif($std['Student']['test_taken_level3'] == 2){?>
								<a href="#" style="color:red;">Close</a>
								<?php } }?>
							</td>
							<td>
								<?php if(!$qualify2 || empty($testLevel3) ){?>
								<a data-toggle="modal" data-target="#test-msg" href='#'>Take Test</a>
								<?php } else{ if($std['Student']['test_taken_level3'] == 2){?>
								<a href="<?php echo HTTP_ROOT.'homes/testInstruction/level3/'.base64_encode(10);?>">Take Test</a>
								<?php } }?>
							</td>
						  </tr>
						 
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>