<style>
p.textcom {
    line-height: 20px !important;
}
</style>

<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
    <div class="container">
        <div class="wrapper" style="background: #fff;">
            <div class="row">
                <div class="col-md-6">
                    <div class="compaere">
                        <ul class="compare_2">
                            <li><a href="<?php echo HTTP_ROOT?>">Home></a>
                            </li>
                            <li><a href="<?php echo HTTP_ROOT?>search">Search></a>
                            </li>
                            <li><a href="<?php echo HTTP_ROOT?>Homes/compare">College Comparision</a>
                            </li>
                        </ul>
                        <h3>College Comparison</h3>
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="sshare_coll">Share this comparison</p>
                </div>
                <div class="icon_coler">
                    <a href="#" class="radius_1"><i class="fa fa-facebook radius_1"></i></a>
                    <a href="#" class="radius_2"><i class="fa fa-twitter radius_2"></i></a>
                    <a href="#" class=""><i class="fa fa-google-plus radius_3"></i></a>
                    <?php if($this->Session->check('Student.id')){?>
                    <a class="btn btn-primary" href="<?php echo HTTP_ROOT.'Homes/saveCompare'?>">Save</a>
                    <?php }?>
                </div>
            </div>
        </div>

    </div>



    <div class="container" >
        <div class="wrapper" style="">
            <div class="">

                <table style="width:100%" class="table_section">
                    <tr>
                        <td class="coll_td1">
                            <!-- <div class="_23S53h-14R597d3GNyOp3U"><span class="_31A4F0ISRVEHVF5guM5IB5"><div class="_2kFyHgfB1l_P0DMs87Dzdk"><label><input type="checkbox" class="_3uUUD5dUpUPrZKj3wCl4Mr" readonly="" value="on"><div class="_1p7h2jcn-4XcWUtxlIWAn2"></div></label></div></span><span class="ZJVX_5g8MhqUjJBjcvN3l"><span><span>Show only differences</span></span>
                                </span>
                            </div> -->
                        </td>
                        <td class="coll_td1">
                            
                            <?php if(!empty($clgs['0']['College']['name'])){?>
                            <a href='<?php echo HTTP_ROOT?>Homes/removeCompare/<?php echo $clgs['0']['College']['id']?>' class='delete-colg'>X</a>
                            <a href="<?php echo HTTP_ROOT?>college/<?=@$clgs['0']['College']['name'].'-'.$clgs['0']['College']['id']?>" class="textcom_a">
                              <p class="textcom">
                                <?php echo @$clgs['0']['College']['name']?>,
                                </p>
                                <p class="textcom"><?php echo @$clgs['0']['College']['State']['statename']?>
                              </p>  
                            </a>
                            
                            <p class="loc-of-clg"><i class="fa fa-map-marker"> </i> <?php echo @$clgs['0']['College']['City']['city_name']?></p>
                            <div class="button_bgcoll button_bgcoll12">
                                <a target="_blank" href="<?php 
                                                              foreach($clgs['0']['CollegeCourse'] as $clgcrs){
                                                                if($clgcrs['course_id'] == $this->Session->read('selectedCourse')){
                                                                    echo HTTP_ROOT.'Homes/downloadBrochure/'.$clgcrs['brochure'];
                                                                }
                                                              }
                                                        ?>"><i class="fa fa-arrow-circle-down"></i> Download Brochure</a>
                                </div>
                            </div>
                        <?php }else{?>
                          <div class="text_NUMBER">1</div>  
                            <div class="button_bgcoll">
                                <a href="<?php echo HTTP_ROOT.'search'?>">Search College</a>
                            </div>
                        <?php }?>
                        </td>
                        <td class="coll_td1">
                            
                            <?php if(!empty($clgs['1']['College']['name'])){?>
                                <a href='<?php echo HTTP_ROOT?>Homes/removeCompare/<?php echo $clgs['1']['College']['id']?>' class='delete-colg'>X</a>
                            <a href="<?php echo HTTP_ROOT?>college/<?=@$clgs['1']['College']['name'].'-'.$clgs['1']['College']['id']?>" class="textcom_a">
                              <p class="textcom">
                                <?php echo trim(@$clgs['1']['College']['name'])?>,
                                </p>
                                <p class="textcom"><?php echo @$clgs['1']['College']['State']['statename']?>
                              </p>  
                            </a>
                            
                            <p class="loc-of-clg"><i class="fa fa-map-marker"> </i> <?php echo @$clgs['1']['College']['City']['city_name']?></p>
                            <div class="button_bgcoll button_bgcoll12">
                                <a target="_blank" href="<?php 
                                                              foreach($clgs['1']['CollegeCourse'] as $clgcrs){
                                                                if($clgcrs['course_id'] == $this->Session->read('selectedCourse')){
                                                                    echo HTTP_ROOT.'Homes/downloadBrochure/'.$clgcrs['brochure'];
                                                                }
                                                              }
                                                        ?>"><i class="fa fa-arrow-circle-down"></i> Download Brochure</a>
                            </div>
                        <?php }else{?>
                          <div class="text_NUMBER">2</div>  
                            <div class="button_bgcoll">
                                <a href="<?php echo HTTP_ROOT.'search'?>">Search College</a>
                            </div>
                        <?php }?>

                        </td>
                        <td class="coll_td1">
                            <?php if(!empty($clgs['2']['College']['name'])){?>
                                <a href='<?php echo HTTP_ROOT?>Homes/removeCompare/<?php echo $clgs['2']['College']['id']?>' class='delete-colg'>X</a>
                            <a href="<?php echo HTTP_ROOT?>college/<?=@$clgs['2']['College']['name'].'-'.$clgs['2']['College']['id']?>" class="textcom_a">
                              <p class="textcom">
                                <?php echo @$clgs['2']['College']['name']?>,<?php echo @$clgs['2']['College']['State']['statename']?>
                              </p>  
                            </a>
                            
                            <p class="loc-of-clg"><i class="fa fa-map-marker"> </i> <?php echo @$clgs['2']['College']['City']['city_name']?></p>
                            <div class="button_bgcoll button_bgcoll12">
                                <a target="_blank" href="<?php 
                                                              foreach($clgs['2']['CollegeCourse'] as $clgcrs){
                                                                if($clgcrs['course_id'] == $this->Session->read('selectedCourse')){
                                                                    echo HTTP_ROOT.'Homes/downloadBrochure/'.$clgcrs['brochure'];
                                                                }
                                                              }
                                                        ?>"><i class="fa fa-arrow-circle-down"></i> Download Brochure</a>
                            </div>
                        <?php }else{?>
                          <div class="text_NUMBER">3</div>  
                            <div class="button_bgcoll">
                                <a href="<?php echo HTTP_ROOT.'search'?>">Search College</a>
                            </div>
                        <?php }?> 
                        </td>
                        <td class="coll_td1">
                            <?php if(!empty($clgs['3']['College']['name'])){?>
                                  <a href='<?php echo HTTP_ROOT?>Homes/removeCompare/<?php echo $clgs['3']['College']['id']?>' class='delete-colg'>X</a>
                            <a href="<?php echo HTTP_ROOT?>college/<?=@$clgs['3']['College']['name'].'-'.$clgs['3']['College']['id']?>" class="textcom_a">
                              <p class="textcom">
                                <?php echo @$clgs['3']['College']['name']?>,<?php echo @$clgs['3']['College']['State']['statename']?>
                              </p>  
                            </a>
                            
                            <p class="loc-of-clg"><i class="fa fa-map-marker"> </i> <?php echo @$clgs['3']['College']['City']['city_name']?></p>
                            <div class="button_bgcoll button_bgcoll12">
                                <a target="_blank" href="<?php 
                                                              foreach($clgs['3']['CollegeCourse'] as $clgcrs){
                                                                if($clgcrs['course_id'] == $this->Session->read('selectedCourse')){
                                                                    echo HTTP_ROOT.'Homes/downloadBrochure/'.$clgcrs['brochure'];
                                                                }
                                                              }
                                                        ?>"><i class="fa fa-arrow-circle-down"></i> Download Brochure</a>
                            </div>
                        <?php }else{?>
                          <div class="text_NUMBER">4</div>  
                            <div class="button_bgcoll">
                                <a href="<?php echo HTTP_ROOT.'search'?>">Search College</a>
                            </div>
                        <?php }?> 
                        </td>
                    </tr>
                
                    <tr>
                        <td class="coll_td4">
                            <div class="rank">
                                <p>Establishment (Year) in:</p>
                            </div>
                        </td>
                        <?php if(!empty($clgs['0']['College']['name'])){?>
                        <td class="coll_td4">
                            <div class="star_icon">

                                <a class="" href="javascript:void(0);">
                                    <p class="textcom">
                                    <?php echo @$clgs['0']['CollegeMeta']['establishment']?>
                                    </p> 
                                </a>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['1']['College']['name'])){?>
                        <td class="coll_td4">
                            <div class="star_icon">

                                <a class="" href="javascript:void(0);">
                                    <p class="textcom">
                                    <?php echo @$clgs['1']['CollegeMeta']['establishment']?>
                                    </p>
                                </a>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['2']['College']['name'])){?>
                        <td class="coll_td4">
                            <div class="star_icon">

                                <a class="" href="javascript:void(0);">
                                    <p class="textcom">
                                    <?php echo @$clgs['2']['CollegeMeta']['establishment']?>
                                    </p>
                                </a>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['3']['College']['name'])){?>
                        <td class="coll_td4">
                            <div class="star_icon">

                                <a class="" href="javascript:void(0);">
                                    <p class="textcom">
                                    <?php echo @$clgs['3']['CollegeMeta']['establishment']?>
                                    </p>
                                </a>
                            </div>
                        </td>
                        <?php }?>
                    </tr>
                    <tr>
                        <td class="coll_td3">
                            <div class="rank">
                                <p>Affiliated By</p>
                            </div>
                        </td>
                        <?php if(!empty($clgs['0']['College']['name'])){?>
                        <td class="coll_td3">
                            <div class="Autonomot">
                                <p><?php echo @$clgs['0']['College']['CollegeMeta']['affiliated_by'] == '' ? '--' : @$clgs['0']['College']['CollegeMeta']['affiliated_by']?></p>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['1']['College']['name'])){?>
                        <td class="coll_td3">
                            <div class="Autonomot">
                                <p><?php echo @$clgs['1']['College']['CollegeMeta']['affiliated_by'] == '' ? '--' : @$clgs['1']['College']['CollegeMeta']['affiliated_by']?></p>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['2']['College']['name'])){?>
                        <td class="coll_td3">
                            <div class="Autonomot">
                                <p><?php echo @$clgs['2']['College']['CollegeMeta']['affiliated_by'] == '' ? '--' : @$clgs['2']['College']['CollegeMeta']['affiliated_by']?></p>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['3']['College']['name'])){?>
                        <td class="coll_td3">
                            <div class="Autonomot">
                                <p><?php echo @$clgs['3']['College']['CollegeMeta']['affiliated_by'] == '' ? '--' : @$clgs['3']['College']['CollegeMeta']['affiliated_by']?></p>
                            </div>
                        </td>
                        <?php }?>
                    </tr>
                    <tr>
                      
                        <td class="coll_td3">
                            <div class="rank">
                                <p>Type</p>
                            </div>
                        </td>
                      
                        <?php if(!empty($clgs['0']['College']['name'])){?>
                          <td class="coll_td3">
                              <div class="Autonomot">
                                  <p> <?php echo @$clgs['0']['College']['CollegeMeta']['college_type'] == '0' ? 'Government' : 'Private';?></p>
                              </div>
                          </td>
                        <?php }?>   
                        <?php if(!empty($clgs['1']['College']['name'])){?>
                          <td class="coll_td3">
                              <div class="Autonomot">
                                  <p> <?php echo @$clgs['1']['College']['CollegeMeta']['college_type'] == '0' ? 'Government' : 'Private';?></p>
                              </div>
                          </td>
                        <?php }?> <?php if(!empty($clgs['2']['College']['name'])){?>
                          <td class="coll_td3">
                              <div class="Autonomot">
                                  <p> <?php echo @$clgs['2']['College']['CollegeMeta']['college_type'] == '0' ? 'Government' : 'Private';?></p>
                              </div>
                          </td>
                        <?php }?> <?php if(!empty($clgs['3']['College']['name'])){?>
                          <td class="coll_td3">
                              <div class="Autonomot">
                                  <p> <?php echo @$clgs['3']['College']['CollegeMeta']['college_type'] == '0' ? 'Government' : 'Private';?></p>
                              </div>
                          </td>
                        <?php }?> 
                    </tr>
              <tr>
                <td class="coll_td2"> <div class="rank"><p> Course Name</p></div></td>

                        <?php if(!empty($clgs['0']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                    <select name="cars" class="select_20 crs-01" ng-model="selectacrs1"  ng-init="selectacrs1='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['0']['CollegeCourse'])){?>
                                        
                                      <?php foreach($clgs['0']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" ><?php echo $course['Course']['course']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>   
                                    </select>
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['1']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-11" ng-model="selectacrs2" ng-init="selectacrs2='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['1']['CollegeCourse'])){?>
                                      <?php foreach($clgs['1']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['Course']['course']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                           
                        </td>
                         <?php }?>
                         <?php if(!empty($clgs['2']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-21" ng-model="selectacrs3" ng-init="selectacrs3='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['2']['CollegeCourse'])){?>
                                      <?php foreach($clgs['2']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['Course']['course']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['3']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-31" ng-model="selectacrs4" ng-init="selectacrs4='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['3']['CollegeCourse'])){?>
                                      <?php foreach($clgs['3']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['Course']['course']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                    </tr>
                    <tr>
                <td class="coll_td2"> <div class="rank"><p> Avg Alumni Salary(INR)</p></div></td>

                        <?php if(!empty($clgs['0']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                    <select name="cars" class="select_20 crs-0" ng-model="selectacrs1"  ng-init="selectacrs1='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['0']['CollegeCourse'])){?>
                                      <?php foreach($clgs['0']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" ng-selected="<?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>"><?php echo $course['avg_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>   
                                    </select>
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['1']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-1" ng-model="selectacrs2"  ng-init="selectacrs2='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['1']['CollegeCourse'])){?>
                                      <?php foreach($clgs['1']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['avg_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                           
                        </td>
                         <?php }?>
                         <?php if(!empty($clgs['2']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-2" ng-model="selectacrs3"  ng-init="selectacrs3='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['2']['CollegeCourse'])){?>
                                      <?php foreach($clgs['2']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['avg_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['3']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-3" ng-model="selectacrs4"  ng-init="selectacrs4='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['3']['CollegeCourse'])){?>
                                      <?php foreach($clgs['3']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['avg_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                    </tr>
                            <tr>
                <td class="coll_td2"> <div class="rank"><p> Max. Alumni Salary(INR)</p></div></td>

                        <?php if(!empty($clgs['0']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                    <select name="cars" class="select_20 crs-0" ng-model="selectacrs1"  ng-init="selectacrs1='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['0']['CollegeCourse'])){?>
                                      <?php foreach($clgs['0']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['max_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>   
                                    </select>
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['1']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-1" ng-model="selectacrs2"  ng-init="selectacrs2='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['1']['CollegeCourse'])){?>
                                      <?php foreach($clgs['1']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['max_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                           
                        </td>
                         <?php }?>
                         <?php if(!empty($clgs['2']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-2" ng-model="selectacrs3"  ng-init="selectacrs3='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['2']['CollegeCourse'])){?>
                                      <?php foreach($clgs['2']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['max_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['3']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-3" ng-model="selectacrs4"  ng-init="selectacrs4='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['3']['CollegeCourse'])){?>
                                      <?php foreach($clgs['3']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['max_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                    </tr>
                    <tr>
                <td class="coll_td2"> <div class="rank"><p> Min. Alumni Salary(INR)</p></div></td>

                        <?php if(!empty($clgs['0']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                    <select name="cars" class="select_20 crs-0" ng-model="selectacrs1"  ng-init="selectacrs1='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['0']['CollegeCourse'])){?>
                                      <?php foreach($clgs['0']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['min_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>   
                                    </select>
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['1']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php" ng-model="selectacrs2"  ng-init="selectacrs2='<?php echo $this->Session->read('selectedCourse')?>'">
                                  <select name="cars" class="select_20 crs-1">
                                    <?php if(!empty($clgs['1']['CollegeCourse'])){?>
                                      <?php foreach($clgs['1']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['min_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                           
                        </td>
                         <?php }?>
                         <?php if(!empty($clgs['2']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-2" ng-model="selectacrs3"  ng-init="selectacrs3='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['2']['CollegeCourse'])){?>
                                      <?php foreach($clgs['2']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['min_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['3']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-3" ng-model="selectacrs4"  ng-init="selectacrs4='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['3']['CollegeCourse'])){?>
                                      <?php foreach($clgs['3']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['min_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                    </tr>

                    <tr>
                <td class="coll_td2"> <div class="rank"><p> Min. Alumni Salary(INR)</p></div></td>

                        <?php if(!empty($clgs['0']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                    <select name="cars" class="select_20 crs-0" ng-model="selectacrs1"  ng-init="selectacrs1='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['0']['CollegeCourse'])){?>
                                      <?php foreach($clgs['0']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['min_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>   
                                    </select>
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['1']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-1" ng-model="selectacrs2"  ng-init="selectacrs2='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['1']['CollegeCourse'])){?>
                                      <?php foreach($clgs['1']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['min_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                           
                        </td>
                         <?php }?>
                         <?php if(!empty($clgs['2']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-2" ng-model="selectacrs3"  ng-init="selectacrs3='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['2']['CollegeCourse'])){?>
                                      <?php foreach($clgs['2']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['min_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['3']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-3" ng-model="selectacrs4"  ng-init="selectacrs4='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['3']['CollegeCourse'])){?>
                                      <?php foreach($clgs['3']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>><?php echo $course['min_salary']?></option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                    </tr>
                <tr>
                    <td class="coll_td2"> <div class="rank"><p> Exam Accepted</p></div></td>

                        <?php if(!empty($clgs['0']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                    <select name="cars" class="select_20 crs-0" ng-model="selectacrs1"  ng-init="selectacrs1='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['0']['CollegeCourse'])){?>
                                      <?php foreach($clgs['0']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>>
                                            <?php foreach($course['CourseExam'] as $exam){
                                                echo @$exam['Exam']['exam'].',';
                                                ?>

                                            <?php }?>    
                                        </option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>   
                                    </select>
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['1']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-1" ng-model="selectacrs2"  ng-init="selectacrs2='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['1']['CollegeCourse'])){?>
                                      <?php foreach($clgs['1']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>>
                                            <?php foreach($course['CourseExam'] as $exam){
                                                echo @$exam['Exam']['exam'].',';
                                                ?>

                                            <?php }?> 
                                        </option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                           
                        </td>
                         <?php }?>
                         <?php if(!empty($clgs['2']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-2" ng-model="selectacrs3"  ng-init="selectacrs3='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['2']['CollegeCourse'])){?>
                                      <?php foreach($clgs['2']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>>
                                            <?php foreach($course['CourseExam'] as $exam){
                                                echo @$exam['Exam']['exam'].',';
                                                ?>

                                            <?php }?> 
                                        </option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['3']['College']['name'])){?>
                        <td class="coll_td2">
                          
                            <div class="select_colle">
                                <form action="/action_page.php">
                                  <select name="cars" class="select_20 crs-3" ng-model="selectacrs4"  ng-init="selectacrs4='<?php echo $this->Session->read('selectedCourse')?>'">
                                    <?php if(!empty($clgs['3']['CollegeCourse'])){?>
                                      <?php foreach($clgs['3']['CollegeCourse'] as $course){?>
                                        <option value="<?php echo $course['course_id']?>" <?php echo $course['course_id'] == $this->Session->read('selectedCourse') ? 'selected' : '';?>>
                                            <?php foreach($course['CourseExam'] as $exam){
                                                echo @$exam['Exam']['exam'].',';
                                                ?>

                                            <?php }?> 
                                        </option>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <option value=""><?php echo 'No Course Found.'?></option>
                                    <?php }?>
                                  </select>  
                                    <br>
                                    <br>
                                </form>
                            </div>
                            
                        </td>
                        <?php }?>
                    </tr>
                    

                    <tr>
                        <td>
                            <div class="rank">
                                <p>Infrastructure & Facilities</p>
                            </div>
                        </td>
                        <?php if(!empty($clgs['0']['College']['name'])){?>
                        <td>
                            <div class="td_menu">
                                <ul class="facilities-list">
                                     <?php if(!empty($clgs['0']['CollegeFacility'])){?>
                                      <?php foreach($clgs['0']['CollegeFacility'] as $fac){?>
                                        <li><a href="javascript:void(0);"><?php echo $fac['Facility']['name']?></a></li>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <li><a href="javascript:void(0);"><?php echo 'No Facility Provided.'?></a></li>
                                    <?php }?>
                                </ul>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['1']['College']['name'])){?>
                        <td>
                            <div class="td_menu">
                                <ul class="facilities-list">
                                     <?php if(!empty($clgs['1']['CollegeFacility'])){?>
                                      <?php foreach($clgs['1']['CollegeFacility'] as $fac){?>
                                        <li><a href="javascript:void(0);"><?php echo $fac['Facility']['name']?></a></li>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <li><a href="javascript:void(0);"><?php echo 'No Facility Provided.'?></a></li>
                                    <?php }?>
                                </ul>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['2']['College']['name'])){?>
                        <td>
                            <div class="td_menu">
                                <ul class="facilities-list">
                                     <?php if(!empty($clgs['2']['CollegeFacility'])){?>
                                      <?php foreach($clgs['2']['CollegeFacility'] as $fac){?>
                                        <li><a href="javascript:void(0);"><?php echo $fac['Facility']['name']?></a></li>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <li><a href="javascript:void(0);"><?php echo 'No Facility Provided.'?></a></li>
                                    <?php }?>
                                </ul>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['3']['College']['name'])){?>
                        <td>
                            <div class="td_menu">
                                <ul class="facilities-list">
                                     <?php if(!empty($clgs['3']['CollegeFacility'])){?>
                                      <?php foreach($clgs['3']['CollegeFacility'] as $fac){?>
                                        <li><a href="javascript:void(0);"><?php echo $fac['Facility']['name']?></a></li>
                                    <?php     }
                                          }else{
                                    ?>  
                                        <li><a href="javascript:void(0);"><?php echo 'No Facility Provided.'?></a></li>
                                    <?php }?>
                                </ul>
                            </div>
                        </td>
                        <?php }?>
                    </tr>
                   
                    <tr>
                        <td class="coll_td4">
                            <div class="rank">
                                <p>Interested in this course?</p>
                            </div>
                        </td>
                        <?php if(!empty($clgs['0']['College']['name'])){?>
                        <td class="coll_td4">
                            <div class="star_icon">

                                <?php 
                                if(!empty($this->Session->read('selectedCourse'))){
                                  foreach($clgs['0']['CollegeCourse'] as $clgcrs){
                                    if($clgcrs['course_id'] == $this->Session->read('selectedCourse')){?>
                                  <?php 
                                   if($clgcrs['from_date'] <= date('Y-m-d') && $clgcrs['to_date'] >= date('Y-m-d')){?>
                                    <button class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm">Apply</button>
                                  <?php }else{?>
                                    <button style='margin-top: 4%;' class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm" data-toggle="modal" data-target="#notifyModal">Notify me / Apply</button>
                                  <?php }?> 
                                
                                <?php }}}?>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['1']['College']['name'])){?>
                        <td class="coll_td4">
                            <div class="star_icon">

                                <?php 
                                if(!empty($this->Session->read('selectedCourse'))){
                                  foreach($clgs['1']['CollegeCourse'] as $clgcrs){
                                    if($clgcrs['course_id'] == $this->Session->read('selectedCourse')){?>
                                  <?php 
                                   if($clgcrs['from_date'] <= date('Y-m-d') && $clgcrs['to_date'] >= date('Y-m-d')){?>
                                    <button class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm">Apply</button>
                                  <?php }else{?>
                                    <button style='margin-top: 4%;' class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm" data-toggle="modal" data-target="#notifyModal">Notify me / Apply</button>
                                  <?php }?> 
                                
                                <?php }}}?>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['2']['College']['name'])){?>
                        <td class="coll_td4">
                            <div class="star_icon">

                                <?php 
                                if(!empty($this->Session->read('selectedCourse'))){
                                  foreach($clgs['2']['CollegeCourse'] as $clgcrs){
                                    if($clgcrs['course_id'] == $this->Session->read('selectedCourse')){?>
                                  <?php 
                                   if($clgcrs['from_date'] <= date('Y-m-d') && $clgcrs['to_date'] >= date('Y-m-d')){?>
                                    <button class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm">Apply</button>
                                  <?php }else{?>
                                    <button style='margin-top: 4%;' class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm" data-toggle="modal" data-target="#notifyModal">Notify me / Apply</button>
                                  <?php }?> 
                                
                                <?php }}}?>
                            </div>
                        </td>
                        <?php }?>
                        <?php if(!empty($clgs['3']['College']['name'])){?>
                        <td class="coll_td4">
                            <div class="star_icon">

                                <?php 
                                if(!empty($this->Session->read('selectedCourse'))){
                                  foreach($clgs['3']['CollegeCourse'] as $clgcrs){
                                    if($clgcrs['course_id'] == $this->Session->read('selectedCourse')){?>
                                  <?php 
                                   if($clgcrs['from_date'] <= date('Y-m-d') && $clgcrs['to_date'] >= date('Y-m-d')){?>
                                    <button class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm">Apply</button>
                                  <?php }else{?>
                                    <button style='margin-top: 4%;' class="btn btn23 btn-primary btn-search-cards-list btn-search-cards1 hidden-xs hidden-sm" data-toggle="modal" data-target="#notifyModal">Notify me / Apply</button>
                                  <?php }?> 
                                
                                <?php }}}?>
                            </div>
                        </td>
                        <?php }?>
                    </tr>
                    <!-- <tr>
                        <td>
                            <p class="Was">Was the comparison helpful?</p>
                            <div class="icon_thm">
                                <a href=""><i class="fa fa-thumbs-up"></i> YES</a>
                                <a href=""> <i class="fa fa-thumbs-up"></i> NO</a>
                            </div>
                        </td>
                    </tr> -->

                </table>



            </div>

        </div>
    </div>
</section>
        <style>
        .delete-colg {
            float: right;
            color: red;
            font-weight: 400px;
            font-size: 18px;
            margin-right: 5px;
            margin-top: -20px;
        }
        ._23S53h-14R597d3GNyOp3U
        {
          font-size: 12px;
          width: 100%;
          float: left;
          margin: 10px;
        }
        ._31A4F0ISRVEHVF5guM5IB5
        {
            float: left;
            margin-top: -3px;
        }
        .ZJVX_5g8MhqUjJBjcvN3l
        {
           float: left;
           margin-left: 9px;
        }
        td.coll_td1 {
    
            height: 195px !important;
        }
        .star_icon{
            padding-left:25%;
        }
        </style>
<script>
    $('select').addClass('form-control');

   /* $('.crs-31').on('change',function(){

        let val = $(this).val();
        $('.crs-3 option[value='+val+']').attr('selected','selected');
    });
    $('.crs-21').on('change',function(){
        let val = $(this).val();
        $('.crs-2 option[value='+val+']').attr('selected','selected');
    });
    $('.crs-11').on('change',function(){
        let val = $(this).val();
        $('.crs-1 option[value='+val+']').attr('selected','selected');
    });
    $('.crs-01').on('change',function(){
        //console.log('hi');
        let val = $(this).val();
        $('.crs-0 option[value='+val+']').change();
    });*/
</script>
<script>
var app = angular.module('myApp', []);
/*app.controller('myCtrl', function($scope) {
    $scope.name = "John Doe";
});*/
</script>