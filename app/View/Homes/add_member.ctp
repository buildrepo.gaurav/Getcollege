<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 14%;
    position: absolute;
}
</style>

<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
  <div class="p-lr-50 p-tb-20">
      <div class="row">
      <?php echo $this->element('frontEnd/studentsidebar');?>
      <div class="col-md-9">
        <div class="content-wrapper">
          <div class="container-fluid">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
              <?php echo !empty($editStudent) ? 'Edit' : 'Add';?>
               Details
               <!-- <a href="<?php echo HTTP_ROOT?>Homes/studentdashboard" class="btn btn-primary">Go To Dashboard</a> -->
              </h1>
            </section>
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Basic Details</a></li>
              <li role="presentation"><a href="#academic" aria-controls="profile" role="tab" data-toggle="tab">Academic Details</a></li>
              <li role="presentation"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">Personal Details</a></li>
              <li role="presentation"><a href="#experience" aria-controls="experience" role="tab" data-toggle="tab">Work Experience</a></li>
              <li role="presentation"><a href="#awards" aria-controls="awards" role="tab" data-toggle="tab">Student Awards</a></li>
              <li role="presentation"><a href="#Docs" aria-controls="Docs" role="tab" data-toggle="tab">Documents</a></li>
            </ul>
              <div class="tab-content">
              <!-- Main content -->
                <div role="tabpanel" class="tab-pane active" id="home">
                  <section class="content">
                        <div class="row">
                          <!-- left column -->
                          <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                              <div class="box-header with-border">
                                <h3 class="box-title">Basic Details</h3>
                              </div>
                              <form id="student-basics" action="" method="post">
                              <input type="hidden" class="student-id" name="id" value="<?php echo @$editStudent['Student']['id'];?>">
                                <div class="container-fluid">
                                  <div class="row"> 
                                  <?php if( @$editStudent['Student']['parent_id'] != '0'){?>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Relationship</label>
                                        <div class="input-icon right">
                                          <select class="form-control required" name="parent_relation" >
                                            <option value="">Select Relationship with member</option>
                                            <option <?php echo @$editStudent['Student']['parent_relation'] == '0' ? 'selected' : '';?> value="0">Son</option>
                                            <option <?php echo @$editStudent['Student']['parent_relation'] == '1' ? 'selected' : '';?> value="1">Daughter</option>
                                            <option <?php echo @$editStudent['Student']['parent_relation'] == '2' ? 'selected' : '';?> value="2">Brother</option>
                                            <option <?php echo @$editStudent['Student']['parent_relation'] == '3' ? 'selected' : '';?> value="3">Sister</option>
                                            <option <?php echo @$editStudent['Student']['parent_relation'] == '4' ? 'selected' : '';?> value="4">Friend</option>
                                            <option <?php echo @$editStudent['Student']['parent_relation'] == '5' ? 'selected' : '';?> value="5">Relative</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <?php }?>  
                                  </div>
                                  <div class="row">            
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Full Name<span class="require" aria-required="true">*</span></label>
                                        <div class="input-icon right">
                                                      <i class="fa"></i>
                                                      
                                          <input type="text" class="form-control required" name="name"  placeholder="Enter Full Name" value="<?php echo @$editStudent['Student']['name'];?>" >
                                          <?php if( @$editStudent['Student']['parent_id'] != '0'){?>
                                          <input type="hidden" class="form-control required" name="parent_id" value="<?php echo $this->Session->read('Student.id')?>" placeholder="Enter Full Name">
                                          <?php }?>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Email Id<span class="require" aria-required="true">*</span></label>
                                        <div class="input-icon right">
                                                      <i class="fa"></i>
                                          <input type="email" class="form-control <?php echo !empty($editStudent) ? '' : 'required email';?>" <?php echo !empty($editStudent) ? 'readonly' : '';?> name="<?php echo !empty($editStudent) ? '' : 'email';?>" placeholder="Enter Email Id" value="<?php echo @$editStudent['Student']['email'];?>">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Mobile Number<span class="require" aria-required="true">*</span></label>
                                        <div class="input-icon right">
                                                      <i class="fa"></i>
                                          <input type="text" class="form-control number required" name="mobile"  minlength="10" maxlength="10" placeholder="Enter Mobile Number" value="<?php echo @$editStudent['Student']['mobile'];?>">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Date of Birth<span class="require" aria-required="true">*</span></label>
                                        <div class="input-icon right">
                                          <input type="text" class="form-control required common-datepicker" id="datepicker"  name="dob"  placeholder="Enter Date of Birth" value="<?php echo @$editStudent['Student']['dob'];?>">
                                          
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Address </label>
                                        <div class="input-icon right">
                                                      <textarea type="text" type="text"  class="form-control required="" " rows="1" name="address"  placeholder="Enter Student Address"><?php echo @$editStudent['Student']['address'];?></textarea>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Country<span class="require" aria-required="true">*</span></label>
                                        <div class="">
                                            <select id="countyus" class="form-control required" name="country_id">
                                              <option value="">Select Country</option>
                                              <?php foreach($country as $coun){?>
                                              <option value="<?php echo $coun['Country']['id']?>" <?php echo @$editStudent['Student']['country_id'] == $coun['Country']['id'] ? 'selected' : '';?>><?php echo $coun['Country']['country_name']?></option>
                                              <?php }?>
                                            </select>
                                        </div>
                                      </div>
                                    </div>                      
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">State<span class="require" aria-required="true">*</span></label>
                                        <div class="">
                                          <select id="statevizag" class="form-control required" name="state_id">
                                            <option value="">Select State</option>
                                            <?php if(!empty($states)){?>
                                                <?php foreach($states as $state){?>
                                                  <option value="<?php echo $state['State']['id']?>" <?php echo $state['State']['id'] == $editStudent['Student']['state_id'] ? 'selected' : '';?>><?php echo $state['State']['statename']?></option>
                                                <?php }?>  
                                            <?php }?>  
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">City<span class="require" aria-required="true">*</span></label>
                                        <div class="">
                                          <select id="citylitu" class="form-control required" name="city_id">
                                            <option value="">Select City</option>
                                            <?php if(!empty($cities)){?>
                                                <?php foreach($cities as $city){?>
                                                  <option value="<?php echo $city['City']['id']?>" <?php echo $city['City']['id'] == $editStudent['Student']['city_id'] ? 'selected' : '';?>><?php echo $city['City']['city_name']?></option>
                                                <?php }?>  
                                            <?php }?>
                                          </select>     
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">                       
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Pincode</label>
                                        <div class="input-icon right">
                                            <input type="text" class="form-control" name="pincode"  placeholder="Enter Pincode" value="<?php echo @$editStudent['Student']['pincode'];?>">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Marital Status</label>
                                        <div class="radio-list">
                                          <label class="radio-inline">
                                              <input type="radio" class="required" name="marital_status"  value="0" <?php echo @$editStudent['Student']['marital_status'] == '0' ? 'checked' : '';?> >Married
                                            </label>
                                          <label class="radio-inline">
                                                <input type="radio" name="marital_status"  value="1"  class="required" <?php echo @$editStudent['Student']['marital_status'] == '1' ? 'checked' : '';?> >Unmarried
                                            </label>
                                        </div>
                                      </div>
                                    </div>                
                                  </div>
                                  
                                  <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Nationality<span class="require" aria-required="true">*</span></label>
                                        <div class="">
                                          <input class="form-control " name="nationality" id="nationality" placeholder="" value="India">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label>Gender</label>
                                        <div class="radio-list">
                                          <label class="radio-inline">
                                            <input type="radio" name="gender"  value="0"  <?php echo @$editStudent['Student']['gender'] == '0' ? 'checked' : ''; echo empty($editStudent['Student']) ? 'checked' : '';?>  > Male 
                                          </label>
                                          <label class="radio-inline">
                                            <input type="radio" name="gender"  value="1"  <?php echo @$editStudent['Student']['gender'] == '1' ? 'checked' : '';?>  > Female
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Religion<span class="require" aria-required="true">*</span></label>
                                        <div class="">
                                            <select  style="display:block;" class="form-control input-text  religion" name="religion"  placeholder="" value="">
                                              <option value="">Select</option>
                                              <option value="Brahmin" <?php echo @$editStudent['Student']['religion'] == 'Brahmin' ? 'selected' : '';?> >Brahmin</option>
                                              <option <?php echo @$editStudent['Student']['religion'] == 'Buddhist' ? 'selected' : '';?> value="Buddhist">Buddhist</option>
                                              <option <?php echo @$editStudent['Student']['religion'] == 'Christian' ? 'selected' : '';?> value="Christian">Christian</option>
                                              <option <?php echo @$editStudent['Student']['religion'] == 'Hindu' ? 'selected' : '';?> value="Hindu">Hindu</option>
                                              <option <?php echo @$editStudent['Student']['religion'] == 'Jain' ? 'selected' : '';?> value="Jain">Jain</option>
                                              <option <?php echo @$editStudent['Student']['religion'] == 'Muslim' ? 'selected' : '';?> value="Muslim">Muslim</option>
                                              <option <?php echo @$editStudent['Student']['religion'] == 'No religion' ? 'selected' : '';?> value="No religion">No religion</option>
                                              <option <?php echo @$editStudent['Student']['religion'] == 'Sikh' ? 'selected' : '';?> value="Sikh">Sikh</option>
                                              <option <?php echo @$editStudent['Student']['religion'] == 'Other religions' ? 'selected' : '';?> value="Other religions">Other religions</option>
                                            </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Category</label>
                                        <select class="form-control"  name="category"  >
                                          <option value=''>SELECT CATEGORY</option>                             
                                          <option <?php echo @$editStudent['Student']['category'] == 'GENERAL' ? 'selected' : '';?> value='GENERAL'  >GENERAL</option>
                                          <option <?php echo @$editStudent['Student']['category'] == 'OBC' ? 'selected' : '';?> value='OBC'  >OBC</option>
                                          <option <?php echo @$editStudent['Student']['category'] == 'SC' ? 'selected' : '';?> value='SC'  >SC</option>
                                          <option <?php echo @$editStudent['Student']['category'] == 'ST' ? 'selected' : '';?> value='ST'  >ST</option>
                                          <option <?php echo @$editStudent['Student']['category'] == 'NT' ? 'selected' : '';?> value='NT'  >NT</option>
                                          <option <?php echo @$editStudent['Student']['category'] == 'OTHER' ? 'selected' : '';?> value='OTHER'  >OTHER</option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Family Details -->
                                    <div class="row">
                                     <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Mother Toungue</label>
                                        <div class="input-icon right">
                                                      <i class="fa"></i>
                                          <input type="text" class="form-control " name="mother_tongue"  placeholder="Enter Mother Tounge" value="<?php echo @$editStudent['Student']['mother_tongue']?>" >
                                        </div>
                                      </div>
                                    </div>
                                      <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Medical Deficiency</label>
                                        <div class="input-icon right">
                                                      <input type="text" class="form-control " name="medical"  placeholder="Enter Medical Deficiency" value="<?php echo @$editStudent['Student']['medical']?>">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    
                                    <div class="col-md-6">
                                      <div class="fileinput fileinput-new form-group" data-provides="fileinput">
                                        <label class="control-label">Upload/Change Student Photograph</label>
                                        <div class="input-icon "><i class="fa"></i>
                                                            
                                            <span class="fileinput-new">
                                                Upload Photo</span>
                                            
                                            <input type="file" name="image" accept="image/*" />
                                                                   
                                                                                                                
                                                            
                                        </div>
                                      </div>
                                    </div>
                                     <div class="col-md-6">
                                      <img id='formimage' src='<?php echo HTTP_ROOT; ?>/img<?php echo empty($editStudent['Student']['formimage']) ? '/default-user.png' : '/studentImages/small/'.$editStudent['Student']['formimage'];?>' width="50%">
                                     </div>   
                                  </div>           
                                </div>  
                                 <div class="box-footer col-md-12">
                                    <button type="submit" class="btn btn-primary">Continue</button>
                                  </div>       
                              </form>
                            </div>
                          </div>
                        </div>
                  </section>   
                </div>
                <div role="tabpanel" class="tab-pane" id="academic">
                  <section class="content">
                        <div class="row">
                          <!-- left column -->
                          <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                              <div class="box-header with-border">
                                <h3 class="box-title"> Student Academics</h3>
                              </div>
                              <form id="student-academic" action="javascript:void(0);" method="post">
                                <div class="container-fluid">
                                <input type="hidden" class="student-id" name="studentId" value="<?php echo @$editStudent['Student']['id'];?>" >
                                  <table  class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                      <th>Exam Name</th>
                                      <th>Year of Passing</th>
                                      <th>School/Institution</th>
                                      <th>Board/University</th>
                                      <th>Percentage/CGPA</th>
                                      <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="academic-table">
                                    <?php if(empty($editStudent['StudentAcademic'])){?>
                                      <tr>
                                        <td>
                                        <i class="fa input-error1"></i>
                                          <input type="text" class="form-control required" placeholder="Exam Name" name="data[1][StudentAcademic][exam]">
                                        </td>  
                                        <td>
                                        <i class="fa input-error1"></i>
                                          <input type="text" maxlength="4" class="form-control required number" required placeholder="Year of passing" name="data[1][StudentAcademic][passing_year]" >
                                          
                                        </td>
                                        <td>
                                        <i class="fa input-error1"></i>
                                          <input type="text" class="form-control required" required placeholder="Institute" name="data[1][StudentAcademic][institute]" >
                                        </td> 
                                        <td>
                                        <i class="fa input-error1"></i>
                                         <input type="text" class="form-control required" required placeholder="Board/University" name="data[1][StudentAcademic][university]" >
                                        </td> 
                                        <td>
                                        <i class="fa input-error1"></i>
                                         <input type="text" class="form-control number" required placeholder="Percentage/CGPA" name="data[1][StudentAcademic][percentage]" >
                                        </td> 
                                        <td>
                                          <a href="javascript:void(0);" class="append-academic-a"><i class="fa fa-plus"></i></a>
                                        </td>
                                      </tr>
                                    <?php }else{?>
                                      <?php $i=1; foreach($editStudent['StudentAcademic'] as $academic){?>
                                        <tr id="dyna_<?php echo @$academic['id']?>">
                                          <td>
                                          <i class="fa input-error1"></i>
                                            <input type="text" class="form-control required" placeholder="Exam Name" name="data[<?php echo $i?>][StudentAcademic][exam]" value="<?php echo @$academic['exam']?>" >
                                          </td>  
                                          <td>
                                          <i class="fa input-error1"></i>
                                            <input type="text" maxlength="4" class="form-control required number" required placeholder="Year of passing" name="data[<?php echo $i?>][StudentAcademic][passing_year]" value="<?php echo @$academic['passing_year']?>" >
                                            
                                          </td>
                                          <td>
                                          <i class="fa input-error1"></i>
                                            <input type="text" class="form-control required" required placeholder="Institute" name="data[<?php echo $i?>][StudentAcademic][institute]" value="<?php echo @$academic['institute']?>" >
                                          </td> 
                                          <td>
                                          <i class="fa input-error1"></i>
                                           <input type="text" class="form-control required" required placeholder="Board/University" name="data[<?php echo $i?>][StudentAcademic][university]" value="<?php echo @$academic['university']?>" >
                                          </td> 
                                          <td>
                                          <i class="fa input-error1"></i>
                                           <input type="text" class="form-control required number" required placeholder="Percentage/CGPA" name="data[<?php echo $i?>][StudentAcademic][percentage]" value="<?php echo @$academic['percentage']?>" >
                                          </td> 
                                          <td>
                                            <a href="javascript:void(0);" class="append-academic-a"><i class="fa fa-plus"></i></a>
                                            <?php if($i!=1){?>
                                              <a class="delete-academic-tr-dyna fafa-icons"href=javascript:void(0); tr-id="<?php echo @$academic['id']?>"><i class="fa fa-trash"></i></a>
                                            <?php }?>  
                                          </td>
                                        </tr>
                                      <?php $i++;}?>
                                    <?php }?> 
                                    </tfoot>
                                  </table>         
                                </div>  
                                 <div class="box-footer col-md-12">
                                    <button type="submit" class="btn btn-primary">Continue</button>
                                  </div>       
                              </form>
                            </div>
                          </div>
                        </div>
                  </section>   
                </div>
                <div role="tabpanel" class="tab-pane" id="personal">
                  <section class="content">
                        <div class="row">
                          <!-- left column -->
                          <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                              <div class="box-header with-border">
                                <h3 class="box-title">Personal Details</h3>
                              </div>
                              <form id="personal-details" action="javascript:void(0);" method="post">
                              <div class="container-fluid">

                                              <div class="row">
                                               <input type="hidden" class="student-id" name="data[StudentMeta][student_id]" value="<?php echo @$editStudent['Student']['id'];?>">
                                <div class="col-md-6">
                                  <div class="form-group">

                                    <label class="control-label">Father's Name<span class="require" aria-required="true">*</span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>

                                      <input type="text" class="form-control " name="data[StudentMeta][father_name]" value="<?php echo @$editStudent['StudentMeta']['father_name'];?>" placeholder="Enter Father Name" >
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Father's Date of Birth<span class="require" aria-required="true"></span></label>
                                    <div class="input-icon"  data-date-format="dd-mm-yyyy" >
                                        <input type="text" class="form-control common-datepicker"  name="data[StudentMeta][father_dob]" value="<?php echo @$editStudent['StudentMeta']['father_dob'];?>" placeholder="Enter Father's dob">
                                     
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">               
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Father's Mobile Number<span class="require" aria-required="true">*</span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="text" class="form-control number " name="data[StudentMeta][father_mob]" minlength="10" maxlength="10" value="<?php echo @$editStudent['StudentMeta']['father_mob'];?>" placeholder="Enter Father's Mobile Number">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Father's Occupation<span class="require" aria-required="true"></span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="text" class="form-control " name="data[StudentMeta][father_occupation]" value="<?php echo @$editStudent['StudentMeta']['father_occupation'];?>" placeholder="Enter Father's Occupation">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">               
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Father's Qualification<span class="require" aria-required="true"></span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="text" class="form-control " name="data[StudentMeta][father_qualification]" value="<?php echo @$editStudent['StudentMeta']['father_qualification'];?>" placeholder="Enter Father's Qualification">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Father's Designation<span class="require" aria-required="true"></span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="text" class="form-control " name="data[StudentMeta][father_designation]" value="<?php echo @$editStudent['StudentMeta']['father_designation'];?>" placeholder="Enter Father's Designation">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">               
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Father's Organization<span class="require" aria-required="true"></span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="text" class="form-control " name="data[StudentMeta][father_company]" value="<?php echo @$editStudent['StudentMeta']['father_company'];?>" placeholder="Enter Father's Organization">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Father's Official Address<span class="require" aria-required="true"></span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <textarea type="text" class="form-control" rows="2" name="data[StudentMeta][father_office]"  placeholder="Enter Father's Official Address"><?php echo @$editStudent['StudentMeta']['father_office'];?></textarea>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">               
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Father's Income<span class="require" aria-required="true"></span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="text" class="form-control number" name="data[StudentMeta][father_income]" value="<?php echo @$editStudent['StudentMeta']['father_income'];?>" placeholder="Enter Father's Income">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Father's Email Id</label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="email" class="form-control email" name="data[StudentMeta][father_email]" value="<?php echo @$editStudent['StudentMeta']['father_email'];?>" placeholder="Enter Father's Email Id">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">               
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Father's Job Transferable<span class="require" aria-required="true"></span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="text" class="form-control " name="data[StudentMeta][job_transfer]" value="<?php echo @$editStudent['StudentMeta']['job_transfer'];?>" placeholder="Enter Father's Job Transferable">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">               
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Mother's Name<span class="require" aria-required="true">*</span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="text" class="form-control " name="data[StudentMeta][mother_name]" value="<?php echo @$editStudent['StudentMeta']['mother_name'];?>" placeholder="Enter Mother Name">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Mother's Date of Birth<span class="require" aria-required="true"></span></label>
                                    <div class="input-icon "  data-date-format="dd-mm-yyyy" >
                                                  <input type="text" class="form-control common-datepicker"  name="data[StudentMeta][mother_dob]" value="<?php echo @$editStudent['StudentMeta']['mother_dob'];?>" placeholder="Enter Mother dob">
                                    
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">               
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Mother's Mobile Number<span class="require" aria-required="true">*</span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="text" class="form-control number" name="data[StudentMeta][mother_mobile]" minlength="10" maxlength="10" value="<?php echo @$editStudent['StudentMeta']['mother_mobile'];?>" placeholder="Enter Mother Mobile Number">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Mother's Occupation<span class="require" aria-required="true"></span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="text" class="form-control " name="data[StudentMeta][mother_job]" value="<?php echo @$editStudent['StudentMeta']['mother_job'];?>" placeholder="Enter Mother Occupation">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">               
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Mother's Qualification<span class="require" aria-required="true"></span></label>
                                    <div class="input-icon right">
                                                  <i class="fa"></i>
                                      <input type="text" class="form-control " name="data[StudentMeta][mother_qualification]" value="<?php echo @$editStudent['StudentMeta']['mother_qualification'];?>" placeholder="Enter Mother Qualification">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Mother's Designation</label>
                                    <div class="input-icon right">
                                                  <input type="text" class="form-control " name="data[StudentMeta][mother_designation]" value="<?php echo @$editStudent['StudentMeta']['mother_designation'];?>" placeholder="Enter Mother Designation">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">               
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Mother's Organization</label>
                                    <div class="input-icon right">
                                                  <input type="text" class="form-control " name="data[StudentMeta][mother_company]" value="<?php echo @$editStudent['StudentMeta']['mother_company'];?>" placeholder="Enter Mother Organization">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Mother's Official Address</label>
                                    <div class="input-icon right">
                                                  <textarea class="form-control" rows="2" name="data[StudentMeta][mother_office]"  placeholder="Enter Mother Official Address"><?php echo @$editStudent['StudentMeta']['mother_office'];?></textarea>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">               
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Mother's Income</label>
                                    <div class="input-icon right">
                                                  <input type="text" class="form-control number" name="data[StudentMeta][mother_income]" value="<?php echo @$editStudent['StudentMeta']['mother_income'];?>" placeholder="Enter Mother Income">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Mother's Email Id</label>
                                    <div class="input-icon right">
                                                  <input type="email" class="form-control email" name="data[StudentMeta][mother_email]" value="<?php echo @$editStudent['StudentMeta']['mother_email'];?>" placeholder="Enter Mother Email Id">
                                    </div>
                                  </div>
                                </div>
                              </div>
                                    </div>  
                                     <div class="box-footer col-md-12">
                                    <button type="submit" class="btn btn-primary">Continue</button>
                                  </div>  
                              </form>
                            </div>  

                          </div>    
                        </div>
                  </section>          
                </div>
                <div role="tabpanel" class="tab-pane" id="experience">
                  <section class="content">
                        <div class="row">
                          <!-- left column -->
                          <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                              <div class="box-header with-border">
                                <h3 class="box-title">Work Experience</h3>
                              </div>
                              <form id="student-experience" action="javascript:void(0);" method="post">
                                <div class="container-fluid">
                                <input type="hidden" class="student-id" name="studentId" value="<?php echo @$editStudent['Student']['id'];?>">
                                  <table  class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                      <th>Name</th>
                                      <th>Designation</th>
                                      <th>Joining Date</th>
                                      <th>Releasing Date</th>
                                      <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="experience-table">
                                    <?php if(empty($editStudent['StudentExperience'])){?>
                                      <tr>
                                        <td>
                                        
                                          <input type="text" class="form-control" placeholder=" Name" name="data[1][StudentExperience][name]">
                                        </td>  
                                        <td>
                                        
                                          <input type="text" class="form-control" required placeholder="Designation" name="data[1][StudentExperience][designation]" >
                                          
                                        </td>
                                        <td>
                                        
                                          <input type="text" class="form-control common-datepicker" required placeholder="Joining Date" name="data[1][StudentExperience][joining]" >
                                        </td> 
                                        <td>
                                        
                                         <input type="text" class="form-control common-datepicker" required placeholder="Releasing Date" name="data[1][StudentExperience][releasing]" >
                                        </td> 
                                         
                                        <td>
                                          <a href="javascript:void(0);" class="append-experience-a"><i class="fa fa-plus"></i></a>
                                        </td>
                                      </tr>
                                    <?php }else{?>
                                    <?php $i=1; foreach($editStudent['StudentExperience'] as $exp){?>
                                      <tr id="expDyna_<?php echo $exp['id'];?>">
                                        <td>
                                        
                                          <input type="text" class="form-control " placeholder=" Name" name="data[<?php echo $i?>][StudentExperience][name]" value="<?php echo @$exp['name']?>">
                                        </td>  
                                        <td>
                                        
                                          <input type="text" class="form-control "  placeholder="Designation" name="data[<?php echo $i?>][StudentExperience][designation]" value="<?php echo @$exp['designation']?>" >
                                          
                                        </td>
                                        <td>
                                        
                                          <input type="text" class="form-control  common-datepicker"  placeholder="Joining Date" name="data[<?php echo $i?>][StudentExperience][joining]" value="<?php echo @$exp['joining']?>" >
                                        </td> 
                                        <td>
                                        
                                         <input type="text" class="form-control  common-datepicker"  placeholder="Releasing Date" name="data[<?php echo $i?>][StudentExperience][releasing]" value="<?php echo @$exp['releasing']?>" >
                                        </td> 
                                         
                                        <td>
                                          <a href="javascript:void(0);" class="append-experience-a"><i class="fa fa-plus"></i></a>
                                          <?php if($i!=1){?>
                                              <a class="delete-experience-tr-dyna fafa-icons"href=javascript:void(0); tr-id="<?php echo @$exp['id']?>"><i class="fa fa-trash"></i></a>
                                            <?php }?>
                                        </td>
                                      </tr>
                                    <?php $i++;}?>
                                    <?php }?>   
                                    </tfoot>
                                  </table>         
                                </div>  
                                 <div class="box-footer col-md-12">
                                    <button type="submit" class="btn btn-primary">Continue</button>
                                  </div>       
                              </form>
                            </div>
                          </div>
                        </div>
                  </section>   
                </div>
                <div role="tabpanel" class="tab-pane" id="awards">
                  <section class="content">
                        <div class="row">
                          <!-- left column -->
                          <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                              <div class="box-header with-border">
                                <h3 class="box-title">Student Awards</h3>
                              </div>
                              <form id="student-awards" action="javascript:void(0);" method="post">
                                <div class="container-fluid">
                                <input type="hidden" class="student-id" name="studentId" value="<?php echo @$editStudent['Student']['id'];?>">
                                  <table  class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                      <th>Year</th>
                                      <th>Award Name</th>
                                      <th>Awarding Institution</th>
                                      <th>Level(State/National/International)</th>
                                      <th>Remark</th>
                                      <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="awards-table">
                                    <?php if(empty($editStudent['StudentAward'])){?>
                                      <tr>
                                        <td>
                                        
                                          <input type="text" class="form-control  number" placeholder=" Year" name="data[1][StudentAward][year]">
                                        </td>  
                                        <td>
                                        
                                          <input type="text" class="form-control "  placeholder="Award Name" name="data[1][StudentAward][name]" >
                                          
                                        </td>
                                        <td>
                                        
                                          <input type="text" class="form-control "  placeholder="Awarding Institution" name="data[1][StudentAward][institute]" >
                                        </td> 
                                        <td>
                                        
                                         <input type="text" class="form-control "  placeholder="Level(State/National/International)" name="data[1][StudentAward][level]" >
                                        </td> 
                                        <td>
                                        
                                         <input type="text" class="form-control "  placeholder="
                                        Remark" name="data[1][StudentAward][remark]" >
                                        </td> 
                                         
                                        <td>
                                          <a href="javascript:void(0);" class="append-awards-a"><i class="fa fa-plus"></i></a>
                                        </td>
                                      </tr>
                                    <?php }else{?>
                                    <?php $i=1; foreach($editStudent['StudentAward'] as $award){?>
                                      <tr id="awardDyna_<?php echo $award['id'];?>"> 
                                        <td>
                                        
                                          <input type="text" class="form-control  number" placeholder=" Year" name="data[<?php echo $i?>][StudentAward][year]" value="<?php echo $award['year'];?>">
                                        </td>  
                                        <td>
                                        
                                          <input type="text" class="form-control "  placeholder="Award Name" name="data[<?php echo $i?>][StudentAward][name]" value="<?php echo $award['name'];?>">
                                          
                                        </td>
                                        <td>
                                        
                                          <input type="text" class="form-control "  placeholder="Awarding Institution" name="data[<?php echo $i?>][StudentAward][institute]" value="<?php echo $award['institute'];?>">
                                        </td> 
                                        <td>
                                        
                                         <input type="text" class="form-control "  placeholder="Level(State/National/International)" name="data[<?php echo $i?>][StudentAward][level]" value="<?php echo $award['level'];?>">
                                        </td> 
                                        <td>
                                        
                                         <input type="text" class="form-control "  placeholder="
                                        Remark" name="data[<?php echo $i?>][StudentAward][remark]" value="<?php echo $award['remark'];?>">
                                        </td> 
                                         
                                        <td>
                                          <a href="javascript:void(0);" class="append-awards-a"><i class="fa fa-plus"></i></a>
                                          <?php if($i!=1){?>
                                              <a class="delete-awards-tr-dyna fafa-icons"href=javascript:void(0); tr-id="<?php echo @$award['id']?>"><i class="fa fa-trash"></i></a>
                                            <?php }?>
                                        </td>
                                      </tr>
                                    <?php $i++;}?>
                                    <?php }?>    
                                    </tfoot>
                                  </table>         
                                </div>  
                                 <div class="box-footer col-md-12">
                                    <button type="submit" class="btn btn-primary">Continue</button>
                                  </div>       
                              </form>
                            </div>
                          </div>
                        </div>
                  </section>   
                </div>
                <div role="tabpanel" class="tab-pane" id="Docs">
                  <section class="content">
                        <div class="row">
                          <!-- left column -->
                          <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                              <div class="box-header with-border">
                                <h3 class="box-title">Student Documents</h3>
                              </div>
                              <form id="student-docs" action="javascript:void(0);" method="post">
                                <div class="container-fluid">
                                <input type="hidden" class="student-id" name="studentId" value="<?php echo @$editStudent['Student']['id'];?>">
                                  <table  class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                      <th>File Name</th>
                                      <th>File</th>
                                      <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="docs-table">
                                    <?php //if(empty($editStudent['StudentDocs'])){?>
                                      <tr>
                                        <td>
                                        
                                          <input type="text" class="form-control" placeholder=" Name" name="data[1][StudentDocs][name]">
                                        </td>  
                                        <td>
                                        
                                          <input type="file" class="form-control"  placeholder="Award Name" name="file[1]" >
                                          
                                        </td>
                                        
                                        <td>
                                          <a href="javascript:void(0);" class="append-docs-a"><i class="fa fa-plus"></i></a>
                                        </td>
                                      </tr>
                                    <?php /*}else{?>
                                    <?php $i=1; foreach($editStudent['StudentDocs'] as $docs){?>
                                      <tr id="docsDyna_<?php echo $docs['id'];?>">
                                        <td>
                                        
                                          <input type="text" class="form-control required" placeholder=" Name" name="data[<?php echo $i;?>][StudentDocs][name]" value="<?php echo @$docs['name']?>">
                                          <input type="hidden" class="form-control" placeholder=" Name" name="data[<?php echo $i;?>][StudentDocs][id]" value="<?php echo @$docs['id']?>">
                                        </td>  
                                        <td>
                                        
                                          <input type="file" class="form-control"  name="file[<?php echo $i;?>]" >
                                          
                                        </td>
                                        
                                        <td>
                                          <a href="javascript:void(0);" class="append-docs-a"><i class="fa fa-plus"></i></a>
                                          <?php if($i!=1){?>
                                              <a class="delete-docs-tr-dyna fafa-icons"href=javascript:void(0); tr-id="<?php echo @$docs['id']?>"><i class="fa fa-trash"></i></a>
                                            <?php }?>
                                        </td>
                                      </tr>
                                    <?php $i++;} ?>
                                    <?php }*/?>   
                                    </tfoot>
                                  </table>         
                                </div>  
                                 <div class="box-footer col-md-12">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                  </div>       
                              </form>
                            </div>
                          </div>
                        </div>
                  </section> 
                  <!----------------------------------------------------------------------> 
                 
                  <div class="box-body" style='margin-top:30px;'>
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>File Name</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php //print_r($walletdetails); 
               foreach ($editStudent['StudentDocs'] as $docdetail) { ?>
                                <tr id='stu-<?php echo $docdetail['id']; ?>'>
                  <td style="text-align: center;">
                    <?php echo $docdetail['name']; ?>   
                  </td>
                  
                  
                  <td>
                    <a style='font-size: 25px;' class="fafa-icons" target='_blank' title="View File" href="<?php echo HTTP_ROOT;?>img/docs/<?php echo $docdetail['file']; ?>  "><i class="fa fa-eye"></i></a> 
                    <a style='font-size: 25px;' class="fafa-icons delete-row" title="Delete" model="StudentDocs" data-id="<?php echo $docdetail['id']; ?>" href="javascript:void(0)"><i class="fa fa-trash"></i></a>  
                  </td>
                </tr>
               <?php  }

               ?>                 
                
              </tbody></table>
               
            </div>
                  <!----------------------------------------------------------------------> 
                </div>
              </div>   
          </div>              
        </div>
      </div>
    </div>
  </div>
</section>

 <script>
    //manage Program 

   let i = 100;
    $(document).on('click','.append-academic-a',function(){
      
      let div = '<tr id="append_'+i+'"><td><i class="fa input-error1"></i> <input class="form-control required"name="data['+i+'][StudentAcademic][exam]"placeholder="Exam Name"><td><i class="fa input-error1"></i> <input maxlength="4" class="form-control number required"name="data['+i+'][StudentAcademic][passing_year]"placeholder="Year of passing"required><td><i class="fa input-error1"></i> <input class="form-control required"name="data['+i+'][StudentAcademic][institute]"placeholder=Institute required><td><i class="fa input-error1"></i> <input class="form-control required"name="data['+i+'][StudentAcademic][university]"placeholder=Board/University required><td><i class="fa input-error1"></i> <input class="form-control number"name="data['+i+'][StudentAcademic][percentage]"placeholder=Percentage/CGPA required><td><a class=append-academic-a href=javascript:void(0);><i class="fa fa-plus"></i></a> <a class="delete-academic-tr fafa-icons"href=javascript:void(0); tr-id="append_'+i+'"><i class="fa fa-trash"></i></a></td></tr>';
      i++;
      $('#academic-table').append(div);
    });
    
    $(document).on('click','.delete-academic-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });
    $(document).on('click','.delete-academic-tr-dyna',function(){
      let tr_id = $(this).attr('tr-id');
      $('#dyna_'+tr_id).remove();
      $.ajax({
      type:'post',
      url :'<?php echo HTTP_ROOT?>Homes/deleteData/StudentAcademic/'+tr_id,
      success:function(){
        //do
      }
      });
    });

    $(document).on('click','.append-experience-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <input type="text" class="form-control " placeholder=" Name" name="data['+i+'][StudentExperience][name]"> </td><td> <input type="text" class="form-control "  placeholder="Designation" name="data['+i+'][StudentExperience][designation]" > </td><td> <input type="text" class="form-control  common-datepicker"  placeholder="Joining Date" name="data['+i+'][StudentExperience][joining]" > </td><td> <input type="text" class="form-control common-datepicker"  placeholder="Releasing Date" name="data['+i+'][StudentExperience][releasing]" > </td><td> <a href="javascript:void(0);" class="append-experience-a"><i class="fa fa-plus"></i></a> <a class="delete-experience-tr fafa-icons"href=javascript:void(0); tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#experience-table').append(div);
      $('.common-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd"
    });
    });
    
    $(document).on('click','.delete-experience-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });
    $(document).on('click','.delete-experience-tr-dyna',function(){
      let tr_id = $(this).attr('tr-id');
      $('#expDyna_'+tr_id).remove();
      $.ajax({
      type:'post',
      url :'<?php echo HTTP_ROOT?>Homes/deleteData/StudentExperience/'+tr_id,
      success:function(){
        //do
      }
      });
    });


    $(document).on('click','.append-awards-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td>  <input type="text" class="form-control  number" placeholder=" Year" name="data['+i+'][StudentAward][year]"> </td><td>  <input type="text" class="form-control "  placeholder="Award Name" name="data['+i+'][StudentAward][name]" > </td><td>  <input type="text" class="form-control "  placeholder="Awarding Institution" name="data['+i+'][StudentAward][institute]" > </td><td>  <input type="text" class="form-control"  placeholder="Level(State/National/International)" name="data['+i+'][StudentAward][level]" > </td><td>  <input type="text" class="form-control "  placeholder="Remark" name="data['+i+'][StudentAward][remark]" > </td><td> <a href="javascript:void(0);" class="append-awards-a"><i class="fa fa-plus"></i></a> <a class="delete-awards-tr fafa-icons"href=javascript:void(0); tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#awards-table').append(div);
    
    });
    
    $(document).on('click','.delete-awards-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });
    $(document).on('click','.delete-awards-tr-dyna',function(){
      let tr_id = $(this).attr('tr-id');
      $('#awardDyna_'+tr_id).remove();
      $.ajax({
      type:'post',
      url :'<?php echo HTTP_ROOT?>Homes/deleteData/StudentAward/'+tr_id,
      success:function(){
        //do
      }
      });
    });

    $(document).on('click','.append-docs-a',function(){
      
      let div = ' <tr id="append_'+i+'"> <td>  <input type="text" class="form-control " placeholder=" Name" name="data['+i+'][StudentDocs][name]"> </td><td>  <input type="file" class="form-control"  placeholder="Award Name" name="file['+i+']" > </td><td> <a href="javascript:void(0);" class="append-docs-a"><i class="fa fa-plus"></i></a> <a class="delete-docs-tr fafa-icons" href=javascript:void(0); tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#docs-table').append(div);
    
    });
    
    $(document).on('click','.delete-docs-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });
    $(document).on('click','.delete-docs-tr-dyna',function(){
      let tr_id = $(this).attr('tr-id');
      $('#docsDyna_'+tr_id).remove();
      $.ajax({
      type:'post',
      url :'<?php echo HTTP_ROOT?>Homes/deleteData/StudentDocs/'+tr_id,
      success:function(){
        //do
      }
      });
    });


    //manage Program 
  </script>
  <script type="text/javascript">
  $('select').select2();
</script>
<script type="text/javascript">

   $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Homes/DeleteDoc/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         $('#stu-'+id).remove();
        
      }
  });

       
   })
</script>