<div class="container main_cont">
		<div class="panel panel-default">
		  <div class="panel-body">
		  <div class="row">
				<div class="col-md-4"><div style="text-align:right;">Step 1 - Select Courses</div></div>
				<div class="col-md-4"><div style="text-align:center"><div style="text-align:center;">Step 2 - Confirm Details</div></div></div>
				<div class="col-md-4" ><div style="text-align:left;"><b>Step 3 - Make Payment</b></div></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4"><span style="float:right; margin-right:17%;" class="dot" onclick=""></span></div>
				<div class="col-md-4"><div style="text-align:center"><span class="dot" onclick=""></span></div></div>
				<div class="col-md-4" ><span style="float:left; margin-left:17%;" class="active_page" onclick=""></span></div>
			</div>
		  </div>
		</div>
		
		<div class="row">
			<div class="col-md-8">
				<div class="well">
					<div class="row">
						<div class="col-md-12 Main_Head_Orders"><h4>FormsAdda Application Form Review</h4><br></div>
						
					</div>
					
					
					<div class="row">
						<div class="col-md-4"><b>Colleges</b></div>
						<div class="col-md-2"><b>Specialization</b></div>
						<div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>Form Fees</b></div></div>
						<div class="col-md-2 Head_Fees"><div width="100%" style="text-align:centre;"><b>FormsADDA Fees</b></div></div>
						<!-- <div class="col-md-1 Head_Fees"><div width="100%" style="text-align:centre;"><b>Action</b></div></div> -->
					</div>
					<br>
					<?php foreach($order['OrderMeta'] as $ord){?>
					<div class="row">
						
						<div class="col-md-4"><?php echo $ord['College']['name']?></div>
						<div class="col-md-2">
							<?php if(!empty($ord['CollegeCourse']['CartSpecialization'])){
								foreach($ord['CollegeCourse']['CartSpecialization'] as $specs){?>
									<?php echo $specs['Specialization']['specialization'];?>,
							<?php }}?>	
						</div>
						<div class="col-md-2"><div width="100%" class="Price"><strike>Rs.<?php echo $ord['CollegeCourse']['registration_fee']?></strike></div></div>
						<div class="col-md-2"><div width="100%" class="Price">Rs. <?php echo $ord['CollegeCourse']['formsadda_cost']?></div></div>
						<!-- <div class="col-md-1"><div width="100%" class="Price"><button class="btn btn-default btn_delete"><span class="glyphicon glyphicon-remove-sign"></span></button></div></div> -->
					</div>
					<br>
					<?php }?>
					
				</div>
				</div>
				<div class="col-md-4">
				<div class="well">
					<div class="row" width="100%"><div class="Head_Fees"><h4>Payment</h4><br></div></div>
					<div class="row Charges">
						<div class="col-md-6"><div>No. of Courses Applied: </div></div>
						<div class="col-md-6"><div><?php echo count($order['OrderMeta'])?></div></div>
					</div>
					<br>
					<form method="post" action="">
					<div class="row Charges">
						<div class="col-md-1" style="padding-top:1%;"><div>
						<input name="type_of_pay" type="radio" id="promo-check" value="0"> </div></div>
						<div class="col-md-6" style="">
						<input type="text" class="form-control promo-class disabled" disabled placeholder="Promocode" id="promo-code" name="promocode">
						</div>
						<div class="col-md-3" style="margin-left:-5%; padding-left:0%;"><button type="button" class="btn btn-success promo-class disabled " id="promo-apply">Apply</button></div>
					</div>
					<?php if(@$plan_prem['MembershipPlan']['addon_feature4'] == 1){?>
					<div class="row Charges">
						<div class="col-md-1" style="padding-top:1%;"><div>
						<input name="type_of_pay" type="radio" id="onetime-check" value="1"> </div></div>
						<div class="col-md-10" style="padding-top:1%;">One time apply for 5 Colleges at fixed price.</div>
					</div>
					<?php }?>
					<br>
					<div class="row Charges">
						<div class="col-md-12"><div>
						<input type="checkbox" name="wallet" id="wallet-check" <?php echo empty(@$wallet['Wallet']['amount']) ? "disabled" : "";?>> Use Wallet</div></div>
						<div class="col-md-12"><div style="font-size:10px; margin-left: 6%;"> (Wallet Balance: Rs. 0<span id="wlt-amt"><?php echo @$wallet['Wallet']['amount']; ?></span>)</div></div>
					</div>
					<br>
					<div class="row Charges">
						<div class="col-md-6"><div><b>Net Payable Amount: </b></div></div>
						<div class="col-md-6"><div><b>Rs.</b><b class="total"> <?php echo $order['Order']['amount']?></b></div></div>
					</div>
					<br>
					<input type="hidden" value="<?php echo base64_encode($order['Order']['id']);?>" name="order_id">
					<div class="row" width="100%"><div class="Pay_button"><button type="submit" class="btn btn-warning">Pay Now</button></div></div>
				</div>
				</form>
				</div>
				
				
				
				
		</div>
</div>
<script type="text/javascript">
	$('#promo-check').on('click',function(){
		if($(this).is(':checked')){
			console.log('asd');
			$('.promo-class').removeClass('disabled');
			$('.promo-class').removeAttr('disabled');
		}
		else{
			$('.promo-class').addClass('disabled');
			$('.promo-class').attr('disabled',true);
		}
	});
	$('#onetime-check').on('click',function(){
		if($(this).is(':checked')){
			console.log('asd');
			$('.promo-class').addClass('disabled');
			$('.promo-class').attr('disabled',true);
			$('#promo-code').val('');
		}
		else{
			console.log('asda');
			$('.promo-class').removeClass('disabled');
			$('.promo-class').removeAttr('disabled')
		}
	});
	function calculate(promo1,wallet1,ontime1){
		$.ajax({
                  type : 'post',
                  url : baseUrl +'Homes/checkCode/<?php echo base64_encode($order["Order"]["id"]);?>/',
                  data	: {promo : promo1,wallet : wallet1,ontime : ontime1},
                  success : function(response){
                        resp = $.parseJSON(response);
                        if(resp.status == 'true'){
                        	$('#wlt-amt').text(resp.walletAmount);
                        	$('.total').text(resp.amount);
                        }
                        
                    }
              })
	}
	$('#wallet-check').on('click',function(){
		let code = $('#promo-code').val();
		let wallet;
		let onetime;
		if(code != ''){
			
		}
		if($('#wallet-check').is(':checked'))
			 wallet = 'true';
		else
			 wallet = 'false';
		if($('#onetime-check').is(':checked'))
			 onetime = 'true';
		else
			 onetime = 'false';
				
		calculate(code,wallet,onetime);
	});

	$('#promo-apply').on('click',function(){
		let code = $('#promo-code').val();
		let wallet;
		let onetime = 'false';
		if(code != ''){
			
		}else{
			alert('Please enter promo code !!!');
		}
		if($('#wallet-check').is(':checked'))
			 wallet = 'true';
		else
			 wallet = 'false';
				
		calculate(code,wallet,onetime);
	});
	$('#onetime-check').on('click',function(){
		let code ='';
		let wallet;
		let onetime;
		if($('#wallet-check').is(':checked'))
			 wallet = 'true';
		else
			 wallet = 'false';
		if($(this).is(':checked'))
			 onetime = 'true';
		else
			 onetime = 'false';
				
		calculate(code,wallet,onetime);
	})
</script>