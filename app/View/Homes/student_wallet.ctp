
<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
	<div class="p-lr-50 p-tb-20">
	  <div class="row">
			<?php echo $this->element('frontEnd/studentsidebar');?>
			<div class="col-md-9">
	      <div class="profile-content">
	        <div class="col-md-12">
	        	<div class="col-md-5">
				   		<h2>Wallet</h2>
				    </div>
				    <div class="col-md-7">
				    	<?php if(@$plan_prem['MembershipPlan']['addon_feature2'] == 1 && $walletdet['Wallet']['amount'] != 0){?>
				    	<a data-toggle="modal" href="javascript:void(0)" data-target="#walletModal" class="btn btn-primary" style="background:#007d94;color:#fff">Transferred To Another Wallet</a>
				    	<?php }?>
				    </div>
				  </div>
				  <div class="col-md-12">
				  	<div class="col-md-3">
				  		<lable>Amount</lable>
				  	</div>
				  	<div class="col-md-9">
				  		Rs. <sapn id="wallet-amt"><?php echo @$walletdet['Wallet']['amount'];?></sapn>
				  	</div>
				  </div>
				  <button  class="addsbutton btn btn-info moto-widget-menu-link pull-right moto-widget-menu-link-level-1 moto-link level_moto_2">+ Add Money To Wallet</button>

	        <div class="col-md-12 hidediv">
	      <form action="" method="post" class="ng-pristine ng-valid" novalidate="novalidate">
                             
                                <div class="container-fluid">
                                
                                  <div class="row">            
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Amount<span class="require" aria-required="true">*</span></label>
                                        <div class="input-icon right">
                                                      <i class="fa"></i>
                                                      
                                          <input type="text" class="form-control required" name="amount" placeholder="Enter Amount" value="" aria-required="true">
                                                                                  
                                                                                  </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Remarks (optional)</label>
                                        <div class="input-icon right">
                                                      <i class="fa"></i>
                                          <input type="email" class="form-control required email" name="remarks" placeholder="Enter Remarks" value="" aria-required="true">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                               
                               
                                            
                                </div>  
                                 <div class="box-footer col-md-12">
                                    <button type="submit" class="btn btn-primary pull-right" style="margin-bottom: 2%;margin-top: -1%;">Pay Now</button>
                                  </div>       
                              </form>
                          </div>

<!------------------------------------------------------------------>
<div class="box-body" style='margin-top:175px;'>
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Date</th>
                  <th>Description</th>
                  <th>Due TO</th>
                  <th>Amount</th>
                  
                </tr>
                </thead>
                <tbody>
               <?php //print_r($walletdetails); 
               foreach ($walletdetails as $walletdetail) { ?>
                <tr>
                  <td style="text-align: center;">
                    <?php 
                    $originalDate = $walletdetail['WalletTransaction']['created_date'];
                    echo $newDate = date("d-m-Y", strtotime($originalDate));
                    ?>
                  </td>
                  <td>
                    <?php echo $walletdetail['WalletTransaction']['description']; ?>                 </td>
                  <td>
                    <?php echo $walletdetail['WalletTransaction']['due_to']; ?>                  </td>
                   <td>
                   <?php echo $walletdetail['WalletTransaction']['Amount']; ?>                 </td>
                  
                </tr>
                
             <?php  }

               ?>
                
                                
                
              </tbody></table>
               
            </div>
<!------------------------------------------------------------------>




                          </div>
			</div>
		</div>
	</div>
</section>
<style type="text/css">
.hidediv
{
	display:none;
}
</style>
<script>
$(function() {
    $( ".addsbutton" ).click(function() {
        $( ".hidediv" ).slideToggle('slow','swing');
    });
});
</script>