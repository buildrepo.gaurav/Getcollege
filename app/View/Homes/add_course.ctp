<style>  .input-error{
    margin: 39px 13px !important;
}

</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
	<div class="container">
	    <div class="row profile">
			<?php echo $this->element('collegesidebar');?>
			<div class="col-md-9">
	            <div class="profile-content">
				   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Courses
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="add-college-course" action="<?php echo HTTP_ROOT?>Homes/addCourse">
                  <input type="hidden" placeholder="Enter Name" name="data[CollegeCourse][college_id]" value="<?php echo @$collegeId?>" class="college-id">
                  <input type="hidden" placeholder="Enter Name" name="data[CollegeCourse][id]" value="<?php echo @$special['CollegeCourse']['id']?>" class="college-id">
                  <div class="box-body">
                    <div class="form-group col-md-6">
                      <label for="exampleInputEmail1">Stream</label>
                      <i class="fa input-error"></i>
                       <select class="form-control required stream-select" id="stream-select-1" name="data[CollegeCourse][stream_id]" data-id="1">
                      <option value="">Select Stream</option>
                      <?php foreach($streams as $stream){?>
                        <option value="<?php echo $stream['Stream']['id']?>" <?php echo @$stream['Stream']['id'] == @$special['CollegeCourse']['stream_id'] ? "selected" : ""; ?>><?php echo $stream['Stream']['stream']?></option>
                      <?php }?>
                    </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Program</label>
                      <i class="fa input-error"></i>
                       <select class="form-control required program-select" id="program-select-1" name="data[CollegeCourse][program_id]" data-id="1">
                      <option value="">Select Program</option>
                      <?php if(!empty($specializationId)){
                              foreach($programs as $program){
                        ?>
                        <option value="<?php echo $program['Program']['id']?>" <?php echo $program['Program']['id'] == $special['CollegeCourse']['program_id'] ? "selected" : ""; ?>><?php echo $program['Program']['program']?></option>
                      <?php }}?>
                    </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="">Course</label>
                      <i class="fa input-error"></i>
                     <select class="form-control required course-select" id="course-select-1" name="data[CollegeCourse][course_id]" data-id="1">
                      <option value="">Select Course</option>
                      <?php if(!empty($specializationId)){
                              foreach($allcourses as $cours){
                        ?>
                        <option value="<?php echo $cours['Course']['id']?>" <?php echo $cours['Course']['id'] == $special['CollegeCourse']['course_id'] ? "selected" : ""; ?>><?php echo $cours['Course']['course']?></option>
                      <?php }}?>
                    </select>
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Fees</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Fees" name="data[CollegeCourse][fees]" value="<?php echo @$special['CollegeCourse']['fees']?>">
                    </div>

                    
                    <!-- <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Formsadda Cost</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Formsadda Cost" name="data[CollegeCourse][formsadda_cost]" value="<?php echo @$special['CollegeCourse']['formsadda_cost']?>">
                    </div> -->
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Eligibility</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required" placeholder="Eligibility" name="data[CollegeCourse][eligibility]" value="<?php echo @$special['CollegeCourse']['eligibility']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Max Salary</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Formsadda Cost" name="data[CollegeCourse][max_salary]" value="<?php echo @$special['CollegeCourse']['max_salary']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Min Salary</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Formsadda Cost" name="data[CollegeCourse][min_salary]" value="<?php echo @$special['CollegeCourse']['min_salary']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Average Salary</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Average Salary" name="data[CollegeCourse][avg_salary]" value="<?php echo @$special['CollegeCourse']['avg_salary']?>">
                    </div>
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Exam Excepted</label>
                      <i class="fa input-error"></i>

                      <select id="exams" class="form-control required" multiple="" name="data[CourseExam][exam_id][]">
                        <option value="">Select Exam</option>
                        <?php foreach($exams as $exam){?>
                          <option value="<?php echo $exam['Exam']['id']?>" <?php if(!empty($special['CourseExam'])){foreach($special['CourseExam'] as $ex){echo $ex['exam_id'] == $exam['Exam']['id'] ? 'selected' : '';}}?>><?php echo $exam['Exam']['exam']?></option>
                        <?php }?>
                      </select>
                    </div>
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Registration Fees</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Registration Fees" name="data[CollegeCourse][registration_fee]" value="<?php echo @$special['CollegeCourse']['registration_fee']?>">
                    </div>
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Application From Date</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required event-datepicker" placeholder="Application From Date" name="data[CollegeCourse][from_date]" value="<?php echo @$special['CollegeCourse']['from_date']?>">
                    </div>
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Application To Date</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required event-datepicker" placeholder="Application To Date" name="data[CollegeCourse][to_date]" value="<?php echo @$special['CollegeCourse']['to_date']?>">
                    </div>
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Trending From Date</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required event-datepicker" placeholder="Trending From Date" name="data[CollegeCourse][trending_from_date]" value="<?php echo @$special['CollegeCourse']['trending_from_date']?>">
                    </div>
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Trending To Date</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required event-datepicker" placeholder="Trending To Date" name="data[CollegeCourse][trending_to_date]" value="<?php echo @$special['CollegeCourse']['trending_to_date']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputFile">E-Brochure File</label>
                      <i class="fa input-error"></i>
                      <input type="file" name="brochure">

                      
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputFile">Fees Structure File</label>
                      <i class="fa input-error"></i>
                      <input type="file" name="Structure">

                      
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputFile">Placement Report File</label>
                      <i class="fa input-error"></i>
                      <input type="file" name="Placement">

                      
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputFile">Offline Application Form</label>
                      <i class="fa input-error"></i>
                      <input type="file" name="Offline">

                      
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputFile">FA Form Accepting</label>
                      <i class="fa input-error"></i>
                      <input type="radio" name="data[CollegeCourse][direct_apply]" value="0" <?php echo @$special['CollegeCourse']['direct_apply'] == '0' ? 'checked' : '';?> onclick="$('#direct_apply').hide();">Yes
                      <input type="radio" name="data[CollegeCourse][direct_apply]" value="1" onclick="$('#direct_apply').show();" <?php echo @$special['CollegeCourse']['direct_apply'] == '1' ? 'checked' : '';?>>No
                      <input id="direct_apply" type="text" name="data[CollegeCourse][link]" class="form-control link required" value="<?php echo @$special['CollegeCourse']['link']?>" style="display:<?php echo @$special['CollegeCourse']['direct_apply']=='1' ? 'block' : 'none'?>" Placeholder="Link">

                      
                    </div>
                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>


            
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
  
	            </div>
			</div>
		</div>
	</div>
</section>
<script>
    $('#exams').select2();
</script>