<style>
.fa-trash,.fa-trash-o,.fa-edit,.fa-plus{
  font-size: 24px;
}
</style>
<section id="section-content" class="content page-1 moto-section" data-widget="section" data-container="section">
	<div class="container">
	    <div class="row profile">
			<?php echo $this->element('collegesidebar');?>
			<div class="col-md-9">
	            <div class="profile-content">
				  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage College Gallery
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form role="form" id="add-state-form" method='post' enctype="multipart/form-data" action="">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Select Images</th>
                </tr>
                </thead>
                <tbody id="facility-table">
                <tr>
                  <td>
                    <i class="fa input-error"></i>
                    <input type="file" class="form-control required" required name="image[]" multiple >
                  </td>  
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All College Gallery Listing</h3>
              </div>
              <form action="<?php echo HTTP_ROOT.'Homes/saveGallery'?>" method="post"> 
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Gallery Name</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php $i=1; foreach($galleries as $data){?>
                  <tr id="tr-<?php echo $data['CollegeGallery']['id']?>">
                    <td>
                      <select name="data[<?php echo $i;?>][CollegeGallery][gallery_id]" class="form-control">
                        <option value="">Select Gallery Name</option>  
                        <?php foreach($gallery as $gal){?>
                          <option value="<?php echo $gal['Gallery']['id']?>" <?php echo $gal['Gallery']['id'] == $data['CollegeGallery']['gallery_id'] ? "selected" : '';?>><?php echo $gal['Gallery']['name']?></option>
                        <?php }?>
                      </select>  
                      <input type="hidden" name="data[<?php echo $i;?>][CollegeGallery][id]" value="<?php echo $data['CollegeGallery']['id']?>">
                    </td>
                    <td>
                      <img src="<?php echo HTTP_ROOT.'img/gallery/small/'.$data['CollegeGallery']['image'];?>" class="img-responisve" >
                    </td>
                     
                    
                    <td>
                      <!-- a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Colleges/manageFacility/'.$data['CollegeGallery']['id']?>"><i class="fa fa-edit"></i></a --> 
                      <a class="fafa-icons delete-row" title="Delete" href="javascript:void(0);" data-id="<?php echo $data['CollegeGallery']['id']?>"><i class="fa fa-trash"></i></a> 
                    </td>
                  </tr>
                  <?php $i++;}?>
                  </tbody> 
                  </tfoot>
                </table>
                <div class="box-footer col-md-12">
                  <button class="btn btn-primary" type="submit">Save All</button>
                </div>
               </form>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
    
	            </div>
			</div>
		</div>
	</div>
</section>
<script>
	   $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        //let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Homes/deleteData/CollegeGallery/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
        $('#tr-'+id).remove();
      }
  });
</script>