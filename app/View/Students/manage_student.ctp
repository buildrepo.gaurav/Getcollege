<style>
  a.fafa-icons{
      margin-left: 10px;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Students
        <!-- <a href="<?php echo HTTP_ROOT?>Students/addStudent" class="btn btn-primary">Add Students</a> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Students/manageStudent">Manage Students</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Students Listing</h3>

            </div>
            <!-- <form method="post" action="<?php echo HTTP_ROOT?>Forms/generateForm"> -->
            <!-- /.box-header -->
            <div class="box-body">
              <table id="Student-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($students as $data){?>
                <tr>
                  <td>
                    <img src="<?php echo HTTP_ROOT.'img/studentImages/small/'.$data['Student']['image']?>" width="18%">
                  </td>
                  <td>
                    <?php echo $data['Student']['name']?>
                  </td>
                  <td>
                    <?php echo $data['Student']['email']?>
                  </td>
                   <td>
                    <?php echo $data['Student']['mobile']?>
                  </td>
                  
                  <td>
                    <a class="fafa-icons" target="_blank" title="Edit" href="<?php echo HTTP_ROOT.'Students/addStudent/'.$data['Student']['id']?>"><i class="fa fa-edit"></i></a> 
                    <a model = "Student" class="fafa-icons change-status" data-id="<?php echo $data['Student']['id']?>" title="<?php echo $data['Student']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Student']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a> 
                    <a class="fafa-icons delete-row" title="Delete" model = "Student" data-id="<?php echo base64_encode($data['Student']['id'])?>" href="javascript:void(0)"><i class="fa <?php echo @$data['Student']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a>  
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
               <!-- <div class="pagination-div dataTables_paginate paging_simple_numbers" id="srch1">
                <ul class="pagination">
                  <li class="paginate_button previous"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'prev disabled'));?></li>
                  <li><?php echo $this->Paginator->numbers(array('separator'=>null));?></li>
                  <li class="paginate_button next"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'next disabled'));?></li>
                 
               </ul>
              </div> -->
            </div>
            <!-- /.box-body -->
           <!--  <div class="box-footer">
                <button type="submit" id="view-form" class="btn btn-primary">View form for selected</button>
              </div> -->
          </div>
          <!-- </form> -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    let offset = 10;
    let action  = 'getStudent';
    let resp = '';
let table = $('#Student-listing').DataTable({
                  "paging": false,
                  "searching":true,
                  "ordering":true
                });
    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           console.log(resp);
           resp.forEach(function(element) {
                  if(element.Student.status == 1){
                    var is_delete = (element.Student.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<img src="<?php echo HTTP_ROOT?>img/studentImages/small/'+element.Student.image+'" width="18%">',element.Student.name,element.Student.email,element.Student.mobile,'<a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT?>Students/addStudent/'+element.Student.id+'"><i class="fa fa-edit"></i></a><a model = "Student" class="fafa-icons change-status" data-id="'+element.Student.id+'" title="Active" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/active.png'?>"></a><a class="fafa-icons delete-row" title="Delete" model = "Student" data-id="'+btoa(element.Student.id)+'" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();
                    } 
                  else{
                    var is_delete = (element.Student.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<img src="<?php echo HTTP_ROOT?>img/studentImages/small/'+element.Student.image+'" width="18%">',element.Student.name,element.Student.email,element.Student.mobile,'<a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT?>Students/addStudent/'+element.Student.id+'"><i class="fa fa-edit"></i></a><a model = "Student" class="fafa-icons change-status" data-id="'+element.Student.id+'" title="Inactive" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/inactive.png'?>"></a><a class="fafa-icons delete-row" title="Delete" model = "Student" data-id="'+btoa(element.Student.id)+'" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();
                  }
              });
           offset += 10;
 
        }
  });
   $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Students/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>