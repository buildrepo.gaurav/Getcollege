<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 14%;
    position: absolute;
}
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <?php if(@$flag == 'edit'){?>
      <h1>
       Edit Student
      </h1>
      <?php } else{?>
      <h1>
       Add Student
      </h1>
      <?php }?>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Students/manageStudent">Manage Student</a></li>
        <?php if(@$flag == 'edit'){?>
        <li><a href="<?php echo HTTP_ROOT?>Students/addStudent">Edit Student</a></li>
        <?php } else{?>
        <li><a href="<?php echo HTTP_ROOT?>Students/addStudent">Add Student</a></li>
        <?php }?>
        
      </ol>
    </section>
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Basic Details</a></li>
      <li role="presentation"><a href="#academic" aria-controls="profile" role="tab" data-toggle="tab">Academic Details</a></li>
      <li role="presentation"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">Personal Details</a></li>
      <li role="presentation"><a href="#experience" aria-controls="experience" role="tab" data-toggle="tab">Work Experience</a></li>
      <li role="presentation"><a href="#awards" aria-controls="awards" role="tab" data-toggle="tab">Student Awards</a></li>
      <li role="presentation"><a href="#Docs" aria-controls="Docs" role="tab" data-toggle="tab">Documents</a></li>
    </ul>
      <div class="tab-content">
      <!-- Main content -->
        <div role="tabpanel" class="tab-pane active" id="home">
          <section class="content">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <div class="box-header with-border">
                        <?php if(@$flag == 'edit'){?>
                        <h3 class="box-title">Edit Details</h3>
                        <?php } else{?>
                        <h3 class="box-title">Add Details</h3>
                        <?php }?>
                        
                      </div>
                      <form id="student-basics" action="javascript:void(0);" method="post">
                      <input type="hidden" class="student-id" name="id" value="<?php echo @$edit['Student']['student-id']?>">
                        <div class="container-fluid">
                          <div class="row">               
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Full Name<span style="color: red">*</span></label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                              
                                  <input type="text" class="form-control required" name="name" value="<?php echo @$edit['Student']['name']?>" placeholder="Enter Full Name">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Email Id</label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                  <input type="email" class="form-control email" name="email" value="<?php echo @$edit['Student']['email']?>" placeholder="Enter Email Id" <?php echo empty($edit) ? '' : 'readonly';?>>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Mobile Number<span style="color: red">*</span></label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                  <input type="text" class="form-control number required" name="mobile" minlength="10" maxlength="10" placeholder="Enter Mobile Number" value="<?php echo @$edit['Student']['mobile']?>">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Date of Birth<span style="color: red">*</span></label>                          
                                <div class="input-icon right">
                                  <input type="text" class="form-control required common-datepicker" id="datepicker"  name="dob" placeholder="Enter Date of Birth" value="<?php echo @$edit['Student']['dob']?>">
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Address </label>
                                <div class="input-icon right">
                                              <textarea type="text" type="text"  class="form-control required="" " rows="1" name="address" value="" placeholder="Enter Student Address"><?php echo @$edit['Student']['address']?></textarea>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Country<span style="color: red">*</span></label>
                                <div class="">
                                    <select id="country" class="form-control required" name="country_id">
                                      <option value="">Select Country</option>
                                      <?php foreach($country as $coun){?>
                                      <option value="<?php echo $coun['Country']['id']?>" <?php echo @$edit['Student']['country_id'] == $coun['Country']['id'] ? 'selected' : '';?>><?php echo $coun['Country']['country_name']?></option>
                                      <?php }?>
                                    </select>
                                </div>
                              </div>
                            </div>                      
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">State<span style="color: red">*</span></label>
                                <div class="">
                                  <select id="state" class="form-control required" name="state_id">
                                    <option value="">Select State</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">City<span style="color: red">*</span></label>
                                <div class="">
                                  <select id="city" class="form-control required" name="city_id">
                                    <option value="">Select City</option>
                                  </select>     
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">                       
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Pincode</label>
                                <div class="input-icon right">
                                    <input type="text" class="form-control number" minlength="6" maxlength="6" name="pincode" value="<?php echo @$edit['Student']['pincode']?>" placeholder="Enter Pincode">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Marital Status</label>
                                <div class="radio-list">
                                  <label class="radio-inline">
                                      <input type="radio" class="required" name="marital_status"  value="0"  <?php echo @$edit['Student']['marital_status'] == 0 ? 'checked' : '';?>>Married
                                    </label>
                                  <label class="radio-inline">
                                        <input type="radio" name="marital_status"  value="1" checked class="required"  <?php echo @$edit['Student']['marital_status'] == 1 ? 'checked' : '';?> <?php echo @empty($edit) ? 'checked': '';?>>Unmarried
                                    </label>
                                </div>
                              </div>
                            </div>                
                          </div>
                          
                          <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Nationality<span style="color: red">*</span></label>
                                <div class="">
                                  <input class="form-control " name="nationality" id="nationality" placeholder="" value="India">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Gender</label>
                                <div class="radio-list">
                                  <label class="radio-inline">
                                    <input type="radio" name="gender"  value="0"  <?php echo @$edit['Student']['gender'] == 0 ? 'checked' : '';?>  > Male 
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" name="gender"  value="1" <?php echo @$edit['Student']['gender'] == 1 ? 'checked' : '';?>  > Female
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Religion<span style="color: red">*</span></label>
                                <div class="">
                                    <select  style="display:block;" class="form-control input-text  religion" name="religion"  placeholder="" value="">
                                      <option value="">Select</option>
                                      <option value="Brahmin" <?php echo @$edit['Student']['religion'] == 'Brahmin' ? 'selected' : '';?>>Brahmin</option>
                                      <option value="Buddhist" <?php echo @$edit['Student']['religion'] == 'Buddhist' ? 'selected' : '';?>>Buddhist</option>
                                      <option value="Christian" <?php echo @$edit['Student']['religion'] == 'Christian' ? 'selected' : '';?>>Christian</option>
                                      <option value="Hindu" <?php echo @$edit['Student']['religion'] == 'Hindu' ? 'selected' : '';?>>Hindu</option>
                                      <option value="Jain" <?php echo @$edit['Student']['religion'] == 'Jain' ? 'selected' : '';?>>Jain</option>
                                      <option value="Muslim" <?php echo @$edit['Student']['religion'] == 'Muslim' ? 'selected' : '';?>>Muslim</option>
                                      <option value="No religion" <?php echo @$edit['Student']['religion'] == 'No religion' ? 'selected' : '';?>>No religion</option>
                                      <option value="Sikh" <?php echo @$edit['Student']['religion'] == 'Sikh' ? 'selected' : '';?>>Sikh</option>
                                      <option value="Other religions" <?php echo @$edit['Student']['religion'] == 'Other religions' ? 'selected' : '';?>>Other religions</option>
                                    </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Category</label>
                                <select class="form-control"  name="category"  >
                                  <option value=''>SELECT CATEGORY</option>                             
                                  <option value='GENERAL'  <?php echo @$edit['Student']['category'] == 'GENERAL' ? 'selected' : '';?>>GENERAL</option>
                                  <option value='OBC' <?php echo @$edit['Student']['category'] == 'OBC' ? 'selected' : '';?> >OBC</option>
                                  <option value='SC' <?php echo @$edit['Student']['category'] == 'SC' ? 'selected' : '';?> >SC</option>
                                  <option value='ST' <?php echo @$edit['Student']['category'] == 'ST' ? 'selected' : '';?> >ST</option>
                                  <option value='NT' <?php echo @$edit['Student']['category'] == 'NT' ? 'selected' : '';?> >NT</option>
                                  <option value='OTHER' <?php echo @$edit['Student']['category'] == 'Other' ? 'selected' : '';?> >OTHER</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <!-- Family Details -->
                            <div class="row">
                             <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Mother Toungue</label>
                                <div class="input-icon right">
                                              <i class="fa"></i>                               
                                              <input type="text" class="form-control " name="mother_tongue" value="<?php echo @$edit['Student']['mother_toungue']?>" placeholder="Enter Mother Tounge">
                                </div>
                              </div>
                            </div>
                              <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Medical Deficiency</label>
                                <div class="input-icon right">
                                              <input type="text" class="form-control " name="medical_deficiency" value="<?php echo @$edit['Student']['medical_deficiency']?>" placeholder="Enter Medical Deficiency">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                             <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Skype ID</label>
                                <div class="input-icon right">
                                              <i class="fa"></i>
                                  <input type="text" class="form-control " name="skype_id" value="<?php echo @$edit['Student']['skype_id']?>" placeholder="Enter Skype ID">
                                </div>
                              </div>
                            </div>
                              <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">How do you came to know about GetCollege.IN?</label>
                                <div class="input-icon right">
                                  <select id="how_to_know" name="how_to_know" class="form-control">
                                    <option value="">Select</option>
                                    <option <?php echo @$edit['Student']['how_to_know'] == 'Friends/Relatives' ? 'selected' : '';?> value="Friends/Relatives">Friends/Relatives</option>
                                    <option <?php echo @$edit['Student']['how_to_know'] == 'Advertisement' ? 'selected' : '';?> value="Advertisement">Advertisement</option>
                                    <option <?php echo @$edit['Student']['how_to_know'] == 'Facebook' ? 'selected' : '';?> value="Facebook">Facebook</option>
                                    <option <?php echo @$edit['Student']['how_to_know'] == 'Google' ? 'selected' : '';?> value="Google">Google</option>
                                    <option <?php echo @$edit['Student']['how_to_know'] == 'Other' ? 'selected' : '';?> value="Other">Other</option>
                                  </select>
                                  <br><br>
                                  <input type="text" name="how_to_know" id="how_to_know_txt" class="form-control" placeholder="How do you came to know about GetCollege.IN?">                  

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="fileinput fileinput-new form-group" data-provides="fileinput">
                                <label class="control-label">Upload Student Photograph</label>
                                <div class="input-icon "><i class="fa"></i>
                                                    
                                    <span class="fileinput-new">
                                        Upload Photo</span>
                                    
                                    <input type="file" name="image" accept="image/*" />
                                               
                                </div>
                              </div>
                            </div>
                          </div>           
                        </div>  
                         <div class="box-footer col-md-12">
                            <button type="submit" class="btn btn-primary">Continue</button>
                          </div>       
                      </form>
                    </div>
                  </div>
                </div>
          </section>   
        </div>
        <div role="tabpanel" class="tab-pane" id="academic">
          <section class="content">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <div class="box-header with-border">
                        <h3 class="box-title">Add Student Details</h3>
                      </div>
                      <form id="student-academic" action="javascript:void(0);" method="post">
                        <div class="container-fluid">
                        <input type="hidden" class="student-id" name="studentId">
                          <table  class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>Exam Name</th>
                              <th>Year of Passing</th>
                              <th>School/Institution</th>
                              <th>Board/University</th>
                              <th>Percentage/CGPA</th>
                              <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="academic-table">
                            <tr>
                              <td>
                              <i class="fa input-error1"></i>
                                <input type="text" class="form-control required" placeholder="Exam Name" name="data[1][StudentAcademic][exam]">
                              </td>  
                              <td>
                              <i class="fa input-error1"></i>
                                <input type="text" class="form-control required number" required placeholder="Year of passing" name="data[1][StudentAcademic][passing_year]" >
                                
                              </td>
                              <td>
                              <i class="fa input-error1"></i>
                                <input type="text" class="form-control required" required placeholder="Institute" name="data[1][StudentAcademic][institute]" >
                              </td> 
                              <td>
                              <i class="fa input-error1"></i>
                               <input type="text" class="form-control required" required placeholder="Board/University" name="data[1][StudentAcademic][university]" >
                              </td> 
                              <td>
                              <i class="fa input-error1"></i>
                               <input type="text" class="form-control required number" required placeholder="Percentage/CGPA" name="data[1][StudentAcademic][percentage]" >
                              </td> 
                              <td>
                                <a href="javascript:void(0);" class="append-academic-a"><i class="fa fa-plus"></i></a>
                              </td>
                            </tr>
                            </tfoot>
                          </table>         
                        </div>  
                         <div class="box-footer col-md-12">
                            <button type="submit" class="btn btn-primary">Continue</button>
                          </div>       
                      </form>
                    </div>
                  </div>
                </div>
          </section>   
        </div>
        <div role="tabpanel" class="tab-pane" id="personal">
          <section class="content">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <div class="box-header with-border">
                        <h3 class="box-title">Personal Details</h3>
                      </div>
                      <form id="personal-details" action="javascript:void(0);" method="post">
                      <div class="container-fluid">

                                      <div class="row">
                                       <input type="hidden" class="student-id" name="data[StudentMeta][student_id]">
                        <div class="col-md-6">
                          <div class="form-group">

                            <label class="control-label">Father's Name<span class="require" aria-required="true">*</span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>

                              <input type="text" class="form-control " name="data[StudentMeta][father_name]" value="" placeholder="Enter Father Name">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Father's Date of Birth<span class="require" aria-required="true"></span></label>
                            <div class="input-icon"  data-date-format="dd-mm-yyyy" >
                                          <input type="text" class="form-control common-datepicker"  name="data[StudentMeta][father_dob]" value="" placeholder="Enter Father's dob">
                             
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">               
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Father's Mobile Number<span class="require" aria-required="true">*</span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <input type="text" class="form-control number" name="data[StudentMeta][father_mob]" minlength="10" maxlength="10" value="" placeholder="Enter Father's Mobile Number">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Father's Occupation<span class="require" aria-required="true"></span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <input type="text" class="form-control " name="data[StudentMeta][father_occupation]" value="" placeholder="Enter Father's Occupation">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">               
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Father's Qualification<span class="require" aria-required="true"></span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <input type="text" class="form-control " name="data[StudentMeta][father_qualification]" value="" placeholder="Enter Father's Qualification">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Father's Designation<span class="require" aria-required="true"></span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <input type="text" class="form-control " name="data[StudentMeta][father_designation]" value="" placeholder="Enter Father's Designation">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">               
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Father's Organization<span class="require" aria-required="true"></span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <input type="text" class="form-control " name="data[StudentMeta][father_company]" value="" placeholder="Enter Father's Organization">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Father's Official Address<span class="require" aria-required="true"></span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <textarea type="text" class="form-control" rows="2" name="data[StudentMeta][father_office]" value="" placeholder="Enter Father's Official Address"></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">               
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Father's Income<span class="require" aria-required="true"></span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <input type="text" class="form-control number" name="data[StudentMeta][father_income]" value="" placeholder="Enter Father's Income">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Father's Email Id</label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <input type="email" class="form-control " name="data[StudentMeta][father_email]" value="" placeholder="Enter Father's Email Id">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">               
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Father's Job Transferable<span class="require" aria-required="true"></span></label>
                            <div class="input-icon right">
                              <i class="fa"></i>
                              <input type="radio" name="data[StudentMeta][job_transfer]" value="yes"> Yes 
                              <input type="radio" name="data[StudentMeta][job_transfer]" value="No"> No
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">               
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Mother's Name<span class="require" aria-required="true">*</span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <input type="text" class="form-control " name="data[StudentMeta][mother_name]" value="" placeholder="Enter Mother Name">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Mother's Date of Birth<span class="require" aria-required="true"></span></label>
                            <div class="input-icon "  data-date-format="dd-mm-yyyy" >
                                          <input type="text" class="form-control common-datepicker"  name="data[StudentMeta][mother_dob]" value="" placeholder="Enter Mother dob">
                            
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">               
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Mother's Mobile Number<span class="require" aria-required="true">*</span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <input type="text" class="form-control number" name="data[StudentMeta][mother_mobile]" minlength="10" maxlength="10" value="" placeholder="Enter Mother Mobile Number">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Mother's Occupation<span class="require" aria-required="true"></span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <input type="text" class="form-control " name="data[StudentMeta][mother_job]" value="" placeholder="Enter Mother Occupation">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">               
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Mother's Qualification<span class="require" aria-required="true"></span></label>
                            <div class="input-icon right">
                                          <i class="fa"></i>
                              <input type="text" class="form-control " name="data[StudentMeta][mother_qualification]" value="" placeholder="Enter Mother Qualification">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Mother's Designation</label>
                            <div class="input-icon right">
                                          <input type="text" class="form-control " name="data[StudentMeta][mother_designation]" value="" placeholder="Enter Mother Designation">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">               
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Mother's Organization</label>
                            <div class="input-icon right">
                                          <input type="text" class="form-control " name="data[StudentMeta][mother_company]" value="" placeholder="Enter Mother Organization">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Mother's Official Address</label>
                            <div class="input-icon right">
                                          <textarea class="form-control" rows="2" name="data[StudentMeta][mother_office]" value="" placeholder="Enter Mother Official Address"></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">               
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Mother's Income</label>
                            <div class="input-icon right">
                                          <input type="text" class="form-control number" name="data[StudentMeta][mother_income]" value="" placeholder="Enter Mother Income">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Mother's Email Id</label>
                            <div class="input-icon right">
                                          <input type="email" class="form-control " name="data[StudentMeta][mother_email]" value="" placeholder="Enter Mother Email Id">
                            </div>
                          </div>
                        </div>
                      </div>
                            </div>  
                             <div class="box-footer col-md-12">
                            <button type="submit" class="btn btn-primary">Continue</button>
                          </div>  
                      </form>
                    </div>  

                  </div>    
                </div>
          </section>          
        </div>
        <div role="tabpanel" class="tab-pane" id="experience">
          <section class="content">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <div class="box-header with-border">
                        <h3 class="box-title">Work Experience</h3>
                      </div>
                      <form id="student-experience" action="javascript:void(0);" method="post">
                        <div class="container-fluid">
                        <input type="hidden" class="student-id" name="studentId">
                          <table  class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>Name</th>
                              <th>Designation</th>
                              <th>Joining Date</th>
                              <th>Releasing Date</th>
                              <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="experience-table">
                            <tr>
                              <td>
                              
                                <input type="text" class="form-control" placeholder=" Name" name="data[1][StudentExperience][name]">
                              </td>  
                              <td>
                              
                                <input type="text" class="form-control" placeholder="Designation" name="data[1][StudentExperience][designation]" >
                                
                              </td>
                              <td>
                              
                                <input type="text" class="form-control common-datepicker" placeholder="Joining Date" name="data[1][StudentExperience][joining]" >
                              </td> 
                              <td>
                              
                               <input type="text" class="form-control common-datepicker" placeholder="Releasing Date" name="data[1][StudentExperience][releasing]" >
                              </td> 
                               
                              <td>
                                <a href="javascript:void(0);" class="append-experience-a"><i class="fa fa-plus"></i></a>
                              </td>
                            </tr>
                            </tfoot>
                          </table>         
                        </div>  
                         <div class="box-footer col-md-12">
                            <button type="submit" class="btn btn-primary">Continue</button>
                          </div>       
                      </form>
                    </div>
                  </div>
                </div>
          </section>   
        </div>
        <div role="tabpanel" class="tab-pane" id="awards">
          <section class="content">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <div class="box-header with-border">
                        <h3 class="box-title">Student Awards</h3>
                      </div>
                      <form id="student-awards" action="javascript:void(0);" method="post">
                        <div class="container-fluid">
                        <input type="hidden" class="student-id" name="studentId">
                          <table  class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>Year</th>
                              <th>Award Name</th>
                              <th>Awarding Institution</th>
                              <th>Level(State/National/International)</th>
                              <th>Remark</th>
                              <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="awards-table">
                            <tr>
                              <td>
                              
                                <input type="text" class="form-control number" placeholder=" Year" name="data[1][StudentAward][year]">
                              </td>  
                              <td>
                              
                                <input type="text" class="form-control" placeholder="Award Name" name="data[1][StudentAward][name]" >
                                
                              </td>
                              <td>
                              
                                <input type="text" class="form-control" placeholder="Awarding Institution" name="data[1][StudentAward][institute]" >
                              </td> 
                              <td>
                              
                               <input type="text" class="form-control" placeholder="Level(State/National/International)" name="data[1][StudentAward][level]" >
                              </td> 
                              <td>
                              
                               <input type="text" class="form-control" placeholder="
                              Remark" name="data[1][StudentAward][remark]" >
                              </td> 
                               
                              <td>
                                <a href="javascript:void(0);" class="append-awards-a"><i class="fa fa-plus"></i></a>
                              </td>
                            </tr>
                            </tfoot>
                          </table>         
                        </div>  
                         <div class="box-footer col-md-12">
                            <button type="submit" class="btn btn-primary">Continue</button>
                          </div>       
                      </form>
                    </div>
                  </div>
                </div>
          </section>   
        </div>
        <div role="tabpanel" class="tab-pane" id="Docs">
          <section class="content">
                <div class="row">
                  <!-- left column -->
                  <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                      <div class="box-header with-border">
                        <h3 class="box-title">Student Documents</h3>
                      </div>
                      <form id="student-docs" action="javascript:void(0);" method="post">
                        <div class="container-fluid">
                        <input type="hidden" class="student-id" name="studentId">
                          <table  class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>File Name</th>
                              <th>File</th>
                              <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="docs-table">
                            <tr>
                              <td>
                              
                                <input type="text" class="form-control" placeholder=" Name" name="data[1][StudentDocs][name]">
                              </td>  
                              <td>
                              
                                <input type="file" class="form-control" placeholder="Award Name" name="file[1]" >
                                
                              </td>
                              
                              <td>
                                <a href="javascript:void(0);" class="append-docs-a"><i class="fa fa-plus"></i></a>
                              </td>
                            </tr>
                            </tfoot>
                          </table>         
                        </div>  
                         <div class="box-footer col-md-12">
                            <button type="submit" class="btn btn-primary">Save</button>
                          </div>       
                      </form>
                    </div>
                  </div>
                </div>
          </section>   
        </div>
      </div>   
               
</div>
 <script>
    //manage Program 

   let i = 100;
    $(document).on('click','.append-academic-a',function(){
      
      let div = '<tr id="append_'+i+'"><td><i class="fa input-error1"></i> <input class="form-control required"name="data['+i+'][StudentAcademic][exam]"placeholder="Exam Name"><td><i class="fa input-error1"></i> <input class="form-control number required"name="data['+i+'][StudentAcademic][passing_year]"placeholder="Year of passing"required><td><i class="fa input-error1"></i> <input class="form-control required"name="data['+i+'][StudentAcademic][institute]"placeholder=Institute required><td><i class="fa input-error1"></i> <input class="form-control required"name="data['+i+'][StudentAcademic][university]"placeholder=Board/University required><td><i class="fa input-error1"></i> <input class="form-control required number"name="data['+i+'][StudentAcademic][percentage]"placeholder=Percentage/CGPA required><td><a class=append-academic-a href=javascript:void(0);><i class="fa fa-plus"></i></a> <a class="delete-academic-tr fafa-icons"href=javascript:void(0); tr-id="append_'+i+'"><i class="fa fa-trash"></i></a></td></tr>';
      i++;
      $('#academic-table').append(div);
    });
    
    $(document).on('click','.delete-academic-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });

    $(document).on('click','.append-experience-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <input type="text" class="form-control" placeholder=" Name" name="data['+i+'][StudentExperience][name]"> </td><td> <input type="text" class="form-control" placeholder="Designation" name="data['+i+'][StudentExperience][designation]" > </td><td> <input type="text" class="form-control common-datepicker" placeholder="Joining Date" name="data['+i+'][StudentExperience][joining]" > </td><td> <input type="text" class="form-control common-datepicker" placeholder="Releasing Date" name="data['+i+'][StudentExperience][releasing]" > </td><td> <a href="javascript:void(0);" class="append-experience-a"><i class="fa fa-plus"></i></a> <a class="delete-experience-tr fafa-icons"href=javascript:void(0); tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#experience-table').append(div);
      $('.common-datepicker').datepicker({
      autoclose: true,
      format : "yyyy-mm-dd"
    });
    });
    
    $(document).on('click','.delete-experience-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });


    $(document).on('click','.append-awards-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td>  <input type="text" class="form-control number" placeholder=" Year" name="data['+i+'][StudentAward][year]"> </td><td>  <input type="text" class="form-control" required placeholder="Award Name" name="data['+i+'][StudentAward][name]" > </td><td>  <input type="text" class="form-control" placeholder="Awarding Institution" name="data['+i+'][StudentAward][institute]" > </td><td>  <input type="text" class="form-control" placeholder="Level(State/National/International)" name="data['+i+'][StudentAward][level]" > </td><td>  <input type="text" class="form-control" placeholder="Remark" name="data['+i+'][StudentAward][remark]" > </td><td> <a href="javascript:void(0);" class="append-awards-a"><i class="fa fa-plus"></i></a> <a class="delete-awards-tr fafa-icons"href=javascript:void(0); tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#awards-table').append(div);
    
    });
    
    $(document).on('click','.delete-awards-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });


    $(document).on('click','.append-docs-a',function(){
      
      let div = ' <tr id="append_'+i+'"> <td>  <input type="text" class="form-control" placeholder=" Name" name="data['+i+'][StudentDocs][name]"> </td><td>  <input type="file" class="form-control" placeholder="Award Name" name="file['+i+']" > </td><td> <a href="javascript:void(0);" class="append-docs-a"><i class="fa fa-plus"></i></a> <a class="delete-docs-tr fafa-icons" href=javascript:void(0); tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#docs-table').append(div);
    
    });
    
    $(document).on('click','.delete-docs-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });



    //manage Program 
  </script>
  <script type="text/javascript">
  $('select').select2();
  $(document).ready(function(){
    $('#how_to_know_txt').hide();
    $(document).on('change','#how_to_know',function(){
      console.log('hhh');
        checkval = $(this).val();
        if(checkval == 'Other'){
          $('#how_to_know_txt').show();
          $('#how_to_know').attr('name','');
        }
        else{
          $('#how_to_know_txt').hide();
          $('#how_to_know').attr('name','how_to_know');
        }
      });
  });
</script>