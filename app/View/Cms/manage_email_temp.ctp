<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Email Templates
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Cms/manageEmailTemp">Manage Email Templates</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->



            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Email Templates</h3>
              </div>
              <table id="faq-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1; foreach($emails as $data){?>
                <tr>
                  <td>
                    <?php echo $i;?>
                  </td>
                  <td>
                    <?php echo $data['EmailTemplate']['title']?>
                  </td>
                  <td>
                    <?php echo $data['EmailTemplate']['description']?>
                  </td>
                  <td>
                    <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Cms/editEmailTemp/'.$data['EmailTemplate']['id']?>"><i class="fa fa-edit"></i></a>
                    <!-- <a class="fafa-icons delete-row" title="Delete" model = "Faq" data-id="<?php echo base64_encode($data['EmailTemplate']['id'])?>" href="javascript:void(0)"><i class="fa fa-trash"></i></a> --> 
                  </td>
                </tr>
                <?php $i++; }?>
                
              </table>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript">
$('#faq-listing').DataTable({
                  "paging": true,
                  "searching":true,
                  "ordering":true
                });
   $(document).ready(function(){

    /*$(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }
  });*/

    /*$(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });*/
       
   })
</script>

  