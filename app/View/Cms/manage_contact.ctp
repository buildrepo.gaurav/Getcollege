<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Contacts
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageContact">Manage Contacts</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->

            <!-- /.box-header -->
            <!-- form start -->


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Contacts</h3>
              </div>
              <table id="faq-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Message</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($contact as $data){?>
                <tr>
                  <td>
                    <?php echo $data['Contact']['name']?>
                  </td>
                  <td>
                    <?php echo $data['Contact']['email']?>
                  </td>
                  <td>
                    <?php echo $data['Contact']['phone']?>
                  </td>
                  <td>
                    <?php echo $data['Contact']['message']?>
                  </td>
                  <td>
                    <!-- <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Cms/manageFaq/'.$data['Contact']['id']?>"><i class="fa fa-edit"></i></a> 
                    <a model = "Faq" data-id="<?php echo $data['Contact']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Contact']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Contact']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a> -->
                    <a class="fafa-icons delete-row" title="Delete" model = "Contact" data-id="<?php echo base64_encode($data['Contact']['id'])?>" href="javascript:void(0)"><i class="fa <?php echo @$data['Contact']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
              </table>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript">
let table = $('#faq-listing').DataTable({
                  "paging": true,
                  "searching":true,
                  "ordering":true
                });
   $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Cms/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>

  