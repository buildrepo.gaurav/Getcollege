<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Faqs
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageFaq">Manage Faqs</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Faqs</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="team-form">
                 
                  <input type="hidden" name="data[Faq][id]" value="<?php echo @$faq['Faq']['id']?>" class="college-id">
                  <div class="box-body">
                   
                    <div class="form-group col-md-12">
                      <label for="exampleInputPassword1">Question</label>
                      <i class="fa input-error"></i>
                      <textarea class="form-control required " id="editor1" placeholder="Question" name="data[Faq][question]" ><?php echo @$faq['Faq']['question']?></textarea>
                    </div>

                    
                    <div class="form-group col-md-12">
                      <label  for="exampleInputPassword1">Answer</label>
                      <i class="fa input-error"></i>
                      <textarea class="form-control required" id="editor2" placeholder="Answer" name="data[Faq][answer]"><?php echo @$faq['Faq']['answer']?></textarea>
                    </div>
                   


                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Faqs</h3>
              </div>
              <table id="faq-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Question</th>
                  <th>Answer</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($faqs as $data){?>
                <tr>
                  <td>
                    <?php echo $data['Faq']['question']?>
                  </td>
                  <td>
                    <?php echo $data['Faq']['answer']?>
                  </td>
                  <td>
                    <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Cms/manageFaq/'.$data['Faq']['id']?>"><i class="fa fa-edit"></i></a> 
                    <a model = "Faq" data-id="<?php echo $data['Faq']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Faq']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Faq']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                    <a class="fafa-icons delete-row" title="Delete" model = "Faq" data-id="<?php echo base64_encode($data['Faq']['id'])?>" href="javascript:void(0)"><i class="fa <?php echo @$data['Faq']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
              </table>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript">
let table = $('#faq-listing').DataTable({
                  "paging": false,
                  "searching":true,
                  "ordering":true
                });
   $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>

  