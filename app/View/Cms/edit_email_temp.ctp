<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Email Templates
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT.'Cms/editEmailTemp/'.$data['EmailTemplate']['id']?>">Edit Email Template</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Email Template</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="team-form">
                 
                  <div class="box-body">
                   
                    <div class="form-group col-md-12">
                      <label for="exampleInputPassword1">Title</label>
                      <i class="fa input-error"></i>
                      <input type="text" class="form-control required" name="data[EmailTemplate][title]" value="<?php echo $data['EmailTemplate']['title']?>">
                      <input type="hidden" name="data[EmailTemplate][id]" value="<?php echo $data['EmailTemplate']['id']?>">
                    </div>

                    
                    <div class="form-group col-md-12">
                      <label  for="exampleInputPassword1">Description <em style="color:red;">( Please Do not Change data under these {} braces)</em></label>
                      <i class="fa input-error"></i>
                      <textarea class="form-control required" id="editor2" placeholder="Description" name="data[EmailTemplate][description]"><?php echo $data['EmailTemplate']['description']?></textarea>
                    </div>
                   


                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>

          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript">
let table = $('#faq-listing').DataTable({
                  "paging": false,
                  "searching":true,
                  "ordering":true
                });

</script>

  