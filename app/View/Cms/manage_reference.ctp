<style>
  .college-select{
    display: none;
  }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Reference Amount
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Cms/manageReference">Manage Reference Amount</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Reference Amount</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="team-form">
                 
                  <input type="hidden" name="data[ReferenceAmount][id]" value="<?php echo @$amount['ReferenceAmount']['id']?>" >
                  <div class="box-body">
                   
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Referrer Amount</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Referer Amount" name="data[ReferenceAmount][referrer_amount]" value="<?php echo @$amount['ReferenceAmount']['referrer_amount']?>">
                    </div>

                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Referre Amount</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Referre Amount" name="data[ReferenceAmount][referre_amount]" value="<?php echo @$amount['ReferenceAmount']['referre_amount']?>">
                    </div>
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Effective From Date</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required event-datepicker" placeholder="Effective From Date" name="data[ReferenceAmount][from_date]" value="<?php echo @$amount['ReferenceAmount']['from_date']?>">
                    </div>
                     <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Effective To Date</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required event-datepicker" placeholder="Effective To Date" name="data[ReferenceAmount][to_date]" value="<?php echo @$amount['ReferenceAmount']['to_date']?>">
                    </div>
                    
                    


                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Reference List</h3>
              </div>
              <table id="team-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Referrer amount</th>
                  <th>Referre amount</th>
                  <th>From date</th>
                  <th>To date</th>
                  <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <?php foreach($amounts as $data){?>
                <tr>
                  <td>
                    <input type="text" class="form-control" value="<?php echo $data['ReferenceAmount']['referrer_amount']?>" name="referrer_amount" readonly >
                    <input type="hidden" class="form-control" value="<?php echo $data['ReferenceAmount']['id']?>" name="id" readonly >
                  </td>
                  <td>
                   <input type="text" class="form-control" value="<?php echo $data['ReferenceAmount']['referre_amount']?>" name="referre_amount" readonly >
                  </td>
                  <td>
                   <input type="text" class="event-datepicker form-control" value="<?php echo $data['ReferenceAmount']['from_date']?>" name="from_date" readonly >
                  </td>
                  <td>
                   <input type="text" class="event-datepicker form-control" value="<?php echo $data['ReferenceAmount']['to_date'] ;?>" name="to_date" readonly >
                  </td>
                  <td>
                    <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                    <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a> 
                   
                    <a model = "ReferenceAmount" data-id="<?php echo base64_encode($data['ReferenceAmount']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa fa-trash"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script type="text/javascript">

    let offset = 5;
    let action  = 'getReferenceamt';
    let resp = '';

    let table =   $('#team-listing').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           //console.log(resp);
           resp.forEach(function(element) {
           // console.log(element.ReferenceAmount.referre_email);
                      table.row.add([element.ReferenceAmount.referrer_amount,element.ReferenceAmount.referre_amount,element.ReferenceAmount.from_date,element.ReferenceAmount.to_date,' <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a> <a model = "ReferenceAmount" data-id="'+btoa(element.ReferenceAmount.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa fa-trash"></i></a>']).draw();

                      //$('#course-'+element.Course.id+' select').attr(element.Course.program_id);
              });
           offset += 5;
 
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','InActive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
  
   })
</script>
<script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editReference',
          data : {data : data},
          success : function(resp){
            alert('Data Save successfully.');
          }
      });
    })
  </script>

  