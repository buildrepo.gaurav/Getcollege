<style>
  .college-select{
    display: none;
  }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Reference List
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Cms/referenceList">Manage Reference List</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            
            <!-- /.box-header -->
            <!-- form start -->
            


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Reference List</h3>
              </div>
              <table id="team-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Referrer</th>
                  <th>Referre</th>
                  <th>Referrer Amount</th>
                  <th>Referre Amount</th>
                  <th>Code</th>
                  <th>Redeemed</th>
                  <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <?php foreach($references as $data){?>
                <tr>
                  <td>
                    <?php echo $data['Student']['name']?>
                  </td>
                  <td>
                    <?php echo $data['ReferenceList']['referre_email']?>
                  </td>
                  <td>
                    <?php echo $data['ReferenceList']['referrer_amount']?>
                  </td>
                  <td>
                    <?php echo $data['ReferenceList']['referre_amount']?>
                  </td>
                  <td>
                   <?php echo $data['ReferenceList']['reference_code'] ;?>
                  </td>
                  <td>
                    <?php echo $data['ReferenceList']['is_redeemed'] == '0' ? 'No' : 'Yes';?>
                  </td>
                  <td>
                    <!-- a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Cms/ReferenceList/'.$data['ReferenceList']['id']?>"><i class="fa fa-edit"></i></a --> 
                    <!--a model = "ReferenceList" data-id="<?php echo $data['ReferenceList']['id']?>" class="fafa-icons change-status" title="<?php echo $data['ReferenceList']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['ReferenceList']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a -->
                    <a model = "ReferenceList" data-id="<?php echo base64_encode($data['ReferenceList']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa fa-trash"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script type="text/javascript">

    let offset = 10;
    let action  = 'getReference';
    let resp = '';

    let table =   $('#team-listing').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           //console.log(resp);
           resp.forEach(function(element) {
           // console.log(element.ReferenceList.referre_email);
                      table.row.add([element.Student.name,element.ReferenceList.referre_email,element.ReferenceList.referrer_amount,element.ReferenceList.referre_amount,element.ReferenceList.reference_code,element.ReferenceList.is_redeemed == 0 ? "No" : "Ye",'<a model = "ReferenceList" data-id="'+btoa(element.ReferenceList.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa fa-trash"></i></a>']).draw();

                      //$('#course-'+element.Course.id+' select').attr(element.Course.program_id);
              });
           offset += 5;
 
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','InActive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });

    $('.get-code').on('click',function(){
      $.ajax({
                  type : 'post',
                  url : baseUrl +'Cms/getCode',
                  success : function(response){
                          //console.log(response);
                          $('.code').val(response)
                       },
                async: false
              });
    });
    $('.coupon-type').on('change',function(){
      if($(this).val() == '1'){
        $('.college-select').show();
        $('.college-sel').show();
      }else{
        $('.college-select').hide();
        $('.college-sel').hide();
      }
    })   
   })
</script>


  