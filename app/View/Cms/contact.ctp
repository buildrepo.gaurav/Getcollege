<section style="background:#ECECEC;" class="p-lr-50 p-tb-20">
	<div class="">
		<div class="row" >
  		<div class="col-md-12 p-lr-50" style="background:#fff;">	
				<div class="row">
					<div id="sec0" class="container-fluid ">
					  <h3 class="privacyhead m-t-10">Contact Us</h3>
					</div>
					<div class="col-md-6">
						<form method="post" id="cform" action="">
						<div class="form-group">
								<label class="control-label">Name<span class="text-danger">*</span></label>
								<input class="form-control required" type="text" name="data[Contact][name]" placeholder="Enter Your Name">
						</div>
						<div class="form-group">
								<label class="control-label">Email<span class="text-danger">*</span></label>
								<input class="form-control required email" type="text" name="data[Contact][email]" placeholder="Enter Your Email">
						</div>
						<div class="form-group">
								<label class="control-label">Phone<span class="text-danger">*</span></label>
								<input class="form-control required number" type="text" name="data[Contact][phone]" placeholder="Enter Your Phone">
						</div>
						<div class="form-group">
								<label class="control-label">Message<span class="text-danger">*</span></label>
								<textarea class="form-control required" name="data[Contact][message]" placeholder="Enter Your Message"></textarea>
						</div>
						<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
					</div>
					<div class="col-md-6">
					    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14028.694018864626!2d77.509877!3d28.474321!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xea2adeb990c32170!2sGet+College!5e0!3m2!1sen!2sin!4v1499859533318" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<h4><i class="fa fa-map-marker" aria-hidden="true"></i> Head Office:</h4>
							<p>D-85, Alpha 1,</p>
							<p>Greater Noida, Uttar Pradesh - 201310</p> 
							<p>Landmark: Reliance Fresh</p>
						</div>
						<div class="col-md-4">
							<h4><i class="fa fa-map-marker" aria-hidden="true"></i> Branch Office:</h4>
							<p>J-9/1, DLF Phase II</p>
							<p>Gurgaon (Gurugram), Haryana</p> 
							<p>Contact No: +91 - 9818840394</p>
						</div>
						<div class="col-md-4">
							<h4><i class="fa fa-phone" aria-hidden="true"></i> Phone number:</h4>
							<p> +91 9958330934</p>
							<h4><i class="fa fa-envelope" aria-hidden="true"></i> E-mail:</h4>
							<p>getincolleges@gmail.com</p>
						</div>
					</div>
				</div>
      </div>
    </div>
	</div>
</section>
<script>
$(document).ready(function(){
	$("#cform").validate();
});
</script>