<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Terms & Conditions
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Cms/manageTerm">Manage Terms & Conditions</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Terms & Conditions</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="team-form">
                 
                  <input type="hidden" name="data[Term][id]" value="<?php echo @$about['Term']['id']?>" class="college-id">
                  <div class="box-body">
                   
                    <div class="form-group col-md-12">
                      <label for="exampleInputPassword1">Title</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required" placeholder="Title" name="data[Term][title]" value="<?php echo @$about['Term']['title']?>">
                    </div>

                    
                    <div class="form-group col-md-12">
                      <label  for="exampleInputPassword1">Content</label>
                      <i class="fa input-error"></i>
                      <textarea class="form-control required" placeholder="Content" name="data[Term][content]" id="editor1"><?php echo @$about['Term']['content']?></textarea>
                    </div>
                   

                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>


            
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 

  