<style>
  .college-select{
    display: none;
  }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Coupon
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Cms/coupon">Manage Coupon</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Coupon</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="team-form">
                 
                  <input type="hidden" name="data[Coupon][id]" value="<?php echo @$coupon['Coupon']['id']?>" >
                  <div class="box-body">
                   
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Code</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required code" placeholder="Code" name="data[Coupon][code]" value="<?php echo @$coupon['Coupon']['code']?>" minlength="5">
                      
                    </div>
                    <div class="form-group col-md-6">
                     <label for="exampleInputPassword1">Generate Code</label><br>
                      <button type="button" class="btn btn-primary get-code" style="margin-top:14px">Generate Code</button>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Coupon type</label>
                      <i class="fa input-error"></i>
                      <select class="form-control required coupon-type" name="data[Coupon][coupon_type_id]">
                          <option value="">Select Coupon type</option>
                          <?php foreach($couponTypes as $ctp){?>
                            <option value="<?php echo $ctp['CouponType']['id']?>" <?php echo $ctp['CouponType']['id'] == @$coupon['Coupon']['coupon_type_id'] ? 'selected' : '';?>><?php echo $ctp['CouponType']['type']?></option>                            
                          <?php }?>  
                      </select>
                    </div>
                     <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Coupon Category</label>
                      <i class="fa input-error"></i>
                      <select class="form-control required" name="data[Coupon][category]">
                          <option value="">Select Coupon Category</option>
                          <option value="1" <?php echo  @$coupon['Coupon']['category'] == '1' ? 'selected' : '';?>>Percent Discount</option>
                          <option value="0" <?php echo  @$coupon['Coupon']['category'] == '0' ? 'selected' : '';?>>Flat Discount</option>
                            
                      </select>
                    </div>

                    <div class="form-group col-md-6 <?php echo @$coupon['Coupon']['coupon_type_id'] == '1' ? 'college-sel' : 'college-select';?>">
                      <label for="exampleInputPassword1">College</label>
                      <i class="fa input-error"></i>
                      <select class="form-control required" name="data[Coupon][college_id]">
                          <option value="">Select College</option>
                          <?php foreach($colleges as $key => $val){?>
                            <option value="<?php echo $key?>" <?php echo $key == @$coupon['Coupon']['college_id'] ? 'selected' : '';?>><?php echo $val?></option>                            
                          <?php }?>
                            
                      </select>
                    </div>
                    <div class="form-group col-md-6 <?php echo @$coupon['Coupon']['coupon_type_id'] == '1' ? 'college-sel' : 'college-select';?>">
                      <label for="exampleInputPassword1">Course</label>
                      <i class="fa input-error"></i>
                      <select class="form-control required" name="data[Coupon][course_id]">
                          <option value="">Select Course</option>
                          <?php foreach($courses as $key => $val){?>
                            <option value="<?php echo $key?>" <?php echo $key == @$coupon['Coupon']['course_id'] ? 'selected' : '';?>><?php echo $val?></option>                            
                          <?php }?>
                            
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Use Limit </label>
                      <i class="fa input-error"></i><br>
                      <label>Only Once</label><input type="radio" class="required" name="data[Coupon][use_limit]" value="1" <?php echo  @$coupon['Coupon']['use_limit'] == '1' ? 'checked' : '';?> >
                      <label style="margin-left:10px;">Unlimited</label><input type="radio" class="required" name="data[Coupon][use_limit]" value="0" <?php echo  @$coupon['Coupon']['use_limit'] == '0' ? 'checked' : '';?> >
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Amount/Percentage</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required number" placeholder="Coupon amount" name="data[Coupon][amount]" value="<?php echo @$coupon['Coupon']['amount']?>">
                    </div>
                   
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Valid From</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required event-datepicker" placeholder="Valid From" name="data[Coupon][valid_from]" value="<?php echo @$coupon['Coupon']['valid_from']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Valid To</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required event-datepicker" placeholder="Valid To" name="data[Coupon][valid_to]" value="<?php echo @$coupon['Coupon']['valid_to']?>">
                    </div>
                    
                    
                    
                    


                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Coupon List</h3>
              </div>
              <table id="team-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Code</th>
                  <th>Valid To</th>
                  <th>Amount</th>
                  <th>Coupan Type</th>
                  <th>Coupan Category</th>
                  <th>Use Limit</th>
                  <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <?php foreach($coupons as $data){?>
                <tr>
                  <td>
                    <?php echo $data['Coupon']['code']?>
                  </td>
                  <td>
                    <?php echo $data['Coupon']['valid_to']?>
                  </td>
                  <td>
                    <?php echo $data['Coupon']['amount']?>
                  </td>
                  <td>
                    <?php echo $data['CouponType']['type']?>
                  </td>
                  <td>
                   <?php echo $data['Coupon']['category'] == '0' ? 'Flat' : 'Percentage';?>
                  </td>
                  <td>
                    <?php echo $data['Coupon']['use_limit'] == '0' ? 'Unlimited' : 'Only Once';?>
                  </td>
                  <td>
                    <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Cms/coupon/'.$data['Coupon']['id']?>"><i class="fa fa-edit"></i></a> 
                    <a model = "Coupon" data-id="<?php echo $data['Coupon']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Coupon']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Coupon']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                    <a model = "Coupon" data-id="<?php echo base64_encode($data['Coupon']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Coupon']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script type="text/javascript">

    let offset = 3;
    let action  = 'getCoupon';
    let resp = '';

    let table =   $('#team-listing').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           console.log(resp);
           resp.forEach(function(element) {
                    if(element.Coupon.status == 1){
                      var is_delete = (element.Coupon.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                      table.row.add([element.Coupon.code,element.Coupon.valid_to,element.Coupon.amount,element.CouponType.type,element.Coupon.category == 0 ? "flat" : "Percentage",element.Coupon.use_limit == 0 ? "Unlimited" : "Only Once",'<a class="fafa-icons" title="Edit" href="'+baseUrl+'Cms/Coupon/'+element.Coupon.id+'"><i class="fa fa-edit"></i></a><a model = "Coupon" data-id="'+element.Coupon.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a model = "Coupon" data-id="'+btoa(element.Coupon.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();
                    }
                    else
                    {
                      var is_delete = (element.Coupon.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                      table.row.add([element.Coupon.code,element.Coupon.valid_to,element.Coupon.amount,element.CouponType.type,element.Coupon.category == 0 ? "flat" : "Percentage",element.Coupon.use_limit == 0 ? "Unlimited" : "Only Once",'<a class="fafa-icons" title="Edit" href="'+baseUrl+'Cms/Coupon/'+element.Coupon.id+'"><i class="fa fa-edit"></i></a><a model = "Coupon" data-id="'+element.Coupon.id+'" class="fafa-icons change-status" title="InActive" href="javascript:void(0)"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a model = "Coupon" data-id="'+btoa(element.Coupon.id)+'" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa '+is_delete+'"></i></a>']).draw();

                      //$('#course-'+element.Course.id+' select').attr(element.Course.program_id);
                    }
              });
           offset += 3;
 
        }
  });

$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','InActive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });

    $('.get-code').on('click',function(){
      $.ajax({
                  type : 'post',
                  url : baseUrl +'Cms/getCode',
                  success : function(response){
                          //console.log(response);
                          $('.code').val(response)
                       },
                async: false
              });
    });
    $('.coupon-type').on('change',function(){
      if($(this).val() == '1'){
        $('.college-select').show();
        $('.college-sel').show();
      }else{
        $('.college-select').hide();
        $('.college-sel').hide();
      }
    })   
   })
</script>


  