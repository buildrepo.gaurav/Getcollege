<!DOCTYPE html>
<html>
<head>
	<title></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
</head>
<body>
	<!--Container-->
	<div style=" background-color: #F1F1F1; margin: 5%; width: 600px; border: 1px solid;">
		<div style="background-color: #E0F2F6">
			<image src="http://52.33.154.113/Formsadda//frontend/mt-0362-home-logo.jpg" style="height: 67px; width:230px;">
		</div>
		<div class="row" style="background-color: #7976; color: black; text-align: center; margin-right: 0px!important; margin-left: 0px!important;">
			<div style="font-size: 18px; padding: 1%;"><b>Invoice</b></div>
		</div>
		<p></p>
		<p style="margin-left: 4%; margin-top: 3%; font-family: Arial, Helvetica, sans-serif;"><b>Hi <?php echo $details['OrderApplicationForm']['full_name']?>,</b></p>
		<p style="margin-left: 4%; margin-top: 1%; font-family: Arial, Helvetica, sans-serif;">You have Successfully filled your forms on FormsADDA</p>
		<p style="margin-left: 4%; margin-top: 1%; font-family: Arial, Helvetica, sans-serif;">The process is on its way, Meanwhile you can check the forms filled and their status on FormsADDA</p>
		<button class="btn btn-primary" style="margin-left: 41%; width: 19%; margin-bottom: 2%;">Check</button>
		<div class="row" style="background-color: white; margin-right: 0px!important; margin-left: 0px!important;">
			<p style="margin-left: 4%; margin-top: 3%; font-family: Arial, Helvetica, sans-serif;">Please find below the summary of your forms filled</p>
			<hr>
			<div class="row" style="text-align: center;">
				<div class="col-md-6"><b>Name</b></div>
				<div class="col-md-2"><b>Form Price</b></div>
				<div class="col-md-1"><b>Qty.</b></div>
				<div class="col-md-2"><b>Course</b></div>
			</div>	
			<?php foreach($ordermetas as $omt){?>
			<div class="row" style="text-align: center; margin-left:1%;">
				<div class="col-md-6"><?php echo $omt['College']['name'];?></div>
				<div class="col-md-2">Rs. <?php echo $omt['CollegeCourse']['formsadda_cost'];?></div>
				<div class="col-md-1">1</div>
				<div class="col-md-2"><?php echo $omt['CollegeCourse']['Course']['course'];?></div>
			</div>
			<?php }?>
			<hr>
			<div class="row" style="margin-right: 0px!important; padding: 1%; margin-left: 0px!important; background-color: #F1F1F1;">
			<div style="float: right; font-size: 23px; margin-right: 11%;">Total: Rs. <?php echo $order['Order']['amount']?></div>	
			</div>
			<hr>
			<p style="margin-left:4%;">Payment Status: <b>Paid</b></p>
			<hr>
		</div>
		<div class="row" style="background-color: white; margin-right: 0px!important; margin-left: 0%!important;">
		<div style="margin-left: 4%;">STUDENT DETAILS</div></div>
		<div class="row" style="background-color: white; margin-right: 0px!important; margin-left: 0%!important;">
		<div style="margin-left: 4%; margin-top: 4%;"><b><?php echo $details['OrderApplicationForm']['full_name']?></b></div></div>
		<div class="row" style="background-color: white; margin-right: 0px!important; margin-left: 0%!important;">
		<div style="margin-left: 4%; margin-top: 0%;"><b><?php echo $details['OrderApplicationForm']['mobile']?></b></div></div>
		<div class="row" style="background-color: white; margin-right: 0px!important; margin-left: 0%!important;">
		<div style="margin-left: 4%; margin-top: 0%; margin-bottom: 2%;"><?php echo $details['OrderApplicationForm']['address']?>,<br> <?php echo $details['City']['city_name'].', '.$details['State']['statename']?>, India  - <?php echo $details['OrderApplicationForm']['pincode']?>.</div></div>

		<div class="row" style="margin-left: 0%!important; margin-right: 0%!important;">
			<div class="col-md-6">
				<div style="font-size: 18px; padding-left: 1%; margin-top: 7%;">Whats Next?</div>
				<div style="padding-left: 1%; margin-top: 1%; margin-bottom: 7%; font-size: 12px;">You will recieve notifications on each step of the way</div>
			</div>
			<div class="col-md-1"></div>

			<div class="col-md-5">
				<div style="font-size: 18px; padding-left: 1%; margin-top: 7%;">Any Questions?</div>
				<div style="padding-left: 1%; margin-top: 1%; font-size: 12px; margin-bottom: 7%;">Get in touch with our 25/7 Customer care team</div>
			</div>
		</div>
		<div class="row" style="text-align: center;">FormsADDA</div>
		<hr>
		<div class="row" style="text-align: center; padding-bottom: 2%;"><a style="text-decoration: none;" href="">About US </a>|<a style="text-decoration: none;" href=""> Terms & Conditions </a>|<a style="text-decoration: none;" href=""> 24/7 Customer Care </a>|<a style="text-decoration: none;" href=""> Contact Us</a></div>

	</div>
</body>
</html>