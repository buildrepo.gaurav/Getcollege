<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 8%;
    position: absolute;
}
.form-control{
  width:100% !important;
}
.input-sm{
  float: right;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Membership
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Colleges/manageMembership">Manage Membership</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Membership</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="team-form">
                 
                  <input type="hidden" name="data[MembershipPlan][id]" value="<?php echo @$plan['MembershipPlan']['id']?>" class="college-id">
                  <div class="box-body">
                   
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Plan Name</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required" placeholder="Plan Name" name="data[MembershipPlan][plan_name]" value="<?php echo @$plan['MembershipPlan']['plan_name']?>">
                    </div>

                    
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Plan Type</label><br>
                      <i class="fa input-error"></i>
                      <input type="radio" class="required" name="data[MembershipPlan][plan_type]" value="1" <?php echo @$plan['MembershipPlan']['plan_type'] == 1 ? 'checked' : ''; ?>> Paid&nbsp;&nbsp;
                      <input type="radio" class="required" name="data[MembershipPlan][plan_type]" value="2" <?php echo @$plan['MembershipPlan']['plan_type'] == 2 ? 'checked' : ''; ?>> free<br><br>
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Plan Cost</label>
                      <i class="fa input-error"></i>
                      <input class="form-control" placeholder="Plan Cost" name="data[MembershipPlan][plan_cost]" value="<?php echo @$plan['MembershipPlan']['plan_cost']?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Duration</label>
                      <i class="fa input-error"></i>
                      <input class="form-control required" name="data[MembershipPlan][duration]" value="<?php echo @$plan['MembershipPlan']['duration']?>">
                        <br><br>
                    </div>
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Period</label>
                      <i class="fa input-error"></i>
                      <select class="form-control required" name="data[MembershipPlan][period]">
                        <option value="">Select Period</option>
                        <option value="0" <?php echo @$plan['MembershipPlan']['period']== 0 ? 'selected' : '';?>>Day</option>
                        <option value="1" <?php echo @$plan['MembershipPlan']['period']== 1 ? 'selected' : '';?>>Month</option>
                        <option value="2" <?php echo @$plan['MembershipPlan']['period']== 2 ? 'selected' : '';?>>Year</option>
                        
                      </select><br><br>
                    </div>
                   

                    
                    <div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Add On Features</label><br>
                      <i class="fa input-error"></i>
                      <input type="checkbox" name="data[MembershipPlan][addon_feature1]" value="1" <?php echo @$plan['MembershipPlan']['addon_feature1'] == 1 ? 'checked' : ''; ?>> No charge for Events<br>
                      <input type="checkbox" name="data[MembershipPlan][addon_feature2]" value="1" <?php echo @$plan['MembershipPlan']['addon_feature2'] == 1 ? 'checked' : ''; ?>> Wallet amount can be transferred to another wallet.<br>
                      <input type="checkbox" name="data[MembershipPlan][addon_feature3]" value="1" <?php echo @$plan['MembershipPlan']['addon_feature3'] == 1 ? 'checked' : ''; ?> onclick="if(this.checked)$('#credit-pr').show();else $('#credit-pr').hide();"> Cash Credit in wallet<br>
                      <i class="fa input-error"></i>
                      <input class="form-control number" placeholder="Credit Amount" name="data[MembershipPlan][wallet_credit]" value="<?php echo @$plan['MembershipPlan']['wallet_credit']?>" id="credit-pr" style="display:none">
                      <input type="checkbox" name="data[MembershipPlan][addon_feature4]" value="1" <?php echo @$plan['MembershipPlan']['addon_feature4'] == 1 ? 'checked' : ''; ?> onclick="if(this.checked)$('#fixed-pr').show();else $('#fixed-pr').hide();"> One time apply for 5 Colleges at fixed price.<br>
                      <i class="fa input-error"></i>
                      <input class="form-control number" placeholder="Fixed Price" name="data[MembershipPlan][fixed_price]" value="<?php echo @$plan['MembershipPlan']['fixed_price']?>" id="fixed-pr" style="display:none">
                    </div>
<div class="form-group col-md-6">
                      <label  for="exampleInputPassword1">Key Features</label>
                      <i class="fa input-error"></i>
                      <textarea id="editor1" class="form-control" name="data[MembershipPlan][key_features]"><?php echo @$plan['MembershipPlan']['key_features']?></textarea>
                    </div>

                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Course Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Plan Name</th>
                    <th>Plan Cost</th>
                    <th>Plan Type</th>
                    <th>Duration</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($plans as $data){?>
                  <tr class="course-list">
                   <td>
                       <?php echo $data['MembershipPlan']['plan_name'];?>
                    </td>
                    <td>
                      <?php echo $data['MembershipPlan']['plan_cost'];?>
                    </td>
                    <td>
                      <?php echo $data['MembershipPlan']['plan_type'] == 1 ? 'Paid' : 'Free';?>
                    </td>
                    <td>
                      <?php echo @$data['MembershipPlan']['duration'];
                              if (@$data['MembershipPlan']['period'] == 0)
                                  echo "Days";
                              else if(@$data['MembershipPlan']['period'] == 1)  
                                  echo "Month";
                              else
                                  echo "Year";  
                      ?>      
                    </td>
                    <td>
                      <a class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'Admins/manageMembership/'.$data['MembershipPlan']['id']?>"><i class="fa fa-edit"></i></a>
                       <a model = "MembershipPlan" data-id="<?php echo $data['MembershipPlan']['id']?>" class="fafa-icons change-status" title="<?php echo $data['MembershipPlan']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0);"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['MembershipPlan']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "MembershipPlan" data-id="<?php echo base64_encode($data['MembershipPlan']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0);"><i class="fa <?php echo @$data['MembershipPlan']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tfoot>
                </table>
            </tbody>    
             <?php if(!empty($programs)){?>
                <?php //echo  $this->element('admin/pagination');?>
              <?php }?>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
   <script>
    //manage Program 

    //manage Program 
  </script>
    <script type="text/javascript">
  $('.course-list').closest('tr').find("select").select2().enable(false);
</script>
<script>
    /*$(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).closest('tr').find("select").select2().enable(true);
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      $(this).closest('tr').find("select").select2().enable(false);
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editCourse',
          data : {data : data},
          success : function(resp){
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          
          }
      });
    })*/
  </script>
  <script type="text/javascript">

  /*  let offset = 4;
    let action  = 'getPlan';
    let resp = '';

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           //console.log(resp);
           resp.forEach(function(element) {
                if(element.MembershipPlan.status == 1){
                  if(element.MembershipPlan.plan_type == 1){
                    if(element.MembershipPlan.is_delete == 0){
                    table.row.add([element.MembershipPlan.plan_name,element.MembershipPlan.plan_cost,'Paid',element.MembershipPlan.duration+' '+element.MembershipPlan.period == 0 ? "Days" : .duration+' '+element.MembershipPlan.period == 1 ? "Months" : "Year",'<a class="fafa-icons edit" title="Edit" href="<?php echo HTTP_ROOT?>Admins/manageMembership/'+element.MembershipPlan.id+'"><i class="fa fa-edit"></i></a> <a model = "Course" data-id="'+element.MembershipPlan.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a> <a model = "Course" data-id="'+btoa(element.MembershipPlan.id)+'" class="fafa-icons delete-row"  href="javascript:void(0);" title="Delete"><i class="fa fa-trash-o"></i></a>']).draw(); 
                    }
                    else{
                      table.row.add([element.MembershipPlan.plan_name,element.MembershipPlan.plan_cost,'Paid',element.MembershipPlan.duration+' '+element.MembershipPlan.period == 0 ? "Days" : element.MembershipPlan.period == 1 ? "Months" : "Year",'<a class="fafa-icons edit" title="Edit" href="<?php echo HTTP_ROOT?>Admins/manageMembership/'+element.MembershipPlan.id+'"><i class="fa fa-edit"></i></a> <a model = "Course" data-id="'+element.MembershipPlan.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a> <a model = "Course" data-id="'+btoa(element.MembershipPlan.id)+'" class="fafa-icons delete-row"  href="javascript:void(0);" title="Delete"><i class="fa fa-trash"></i></a>']).draw(); 
                    }
                  }
                  else{
                    if(element.MembershipPlan.is_delete == 0){
                      table.row.add([element.MembershipPlan.plan_name,element.MembershipPlan.plan_cost,'Free',element.MembershipPlan.duration+' '+element.MembershipPlan.period == 0 ? "Days" : element.MembershipPlan.period == 1 ? "Months" : "Year",'<a class="fafa-icons edit" title="Edit" href="<?php echo HTTP_ROOT?>Admins/manageMembership/'+element.MembershipPlan.id+'"><i class="fa fa-edit"></i></a> <a model = "Course" data-id="'+element.MembershipPlan.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a> <a model = "Course" data-id="'+btoa(element.MembershipPlan.id)+'" class="fafa-icons delete-row"  href="javascript:void(0);" title="Delete"><i class="fa fa-trash-o"></i></a>']).draw(); 
                    }
                    else{
                      table.row.add([element.MembershipPlan.plan_name,element.MembershipPlan.plan_cost,'Free',element.MembershipPlan.duration+' '+element.MembershipPlan.period == 0 ? "Days" : element.MembershipPlan.period == 1 ? "Months" : "Year",'<a class="fafa-icons edit" title="Edit" href="<?php echo HTTP_ROOT?>Admins/manageMembership/'+element.MembershipPlan.id+'"><i class="fa fa-edit"></i></a> <a model = "Course" data-id="'+element.MembershipPlan.id+'" class="fafa-icons change-status" title="Active" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a> <a model = "Course" data-id="'+btoa(element.MembershipPlan.id)+'" class="fafa-icons delete-row"  href="javascript:void(0);" title="Delete"><i class="fa fa-trash"></i></a>']).draw(); 
                    }
                  }
                }
                    
                else
                  if(element.MembershipPlan.plan_type == 1){
                    if(element.MembershipPlan.is_delete == 0){
                    table.row.add([element.MembershipPlan.plan_name,element.MembershipPlan.plan_cost,'Paid',element.MembershipPlan.duration+' '+element.MembershipPlan.period == 0 ? "Days" : .duration+' '+element.MembershipPlan.period == 1 ? "Months" : "Year",'<a class="fafa-icons edit" title="Edit" href="<?php echo HTTP_ROOT?>Admins/manageMembership/'+element.MembershipPlan.id+'"><i class="fa fa-edit"></i></a> <a model = "Course" data-id="'+element.MembershipPlan.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a> <a model = "Course" data-id="'+btoa(element.MembershipPlan.id)+'" class="fafa-icons delete-row"  href="javascript:void(0);" title="Delete"><i class="fa fa-trash-o"></i></a>']).draw();
                    }
                    else{
                      table.row.add([element.MembershipPlan.plan_name,element.MembershipPlan.plan_cost,'Paid',element.MembershipPlan.duration+' '+element.MembershipPlan.period == 0 ? "Days" : element.MembershipPlan.period == 1 ? "Months" : "Year",'<a class="fafa-icons edit" title="Edit" href="<?php echo HTTP_ROOT?>Admins/manageMembership/'+element.MembershipPlan.id+'"><i class="fa fa-edit"></i></a> <a model = "Course" data-id="'+element.MembershipPlan.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a> <a model = "Course" data-id="'+btoa(element.MembershipPlan.id)+'" class="fafa-icons delete-row"  href="javascript:void(0);" title="Delete"><i class="fa fa-trash"></i></a>']).draw();
                    }
                  }
                  else{
                    if(element.MembershipPlan.is_delete == 0){
                    table.row.add([element.MembershipPlan.plan_name,element.MembershipPlan.plan_cost,'Free',element.MembershipPlan.duration+' '+element.MembershipPlan.period == 0 ? "Days" : .duration+' '+element.MembershipPlan.period == 1 ? "Months" : "Year",'<a class="fafa-icons edit" title="Edit" href="<?php echo HTTP_ROOT?>Admins/manageMembership/'+element.MembershipPlan.id+'"><i class="fa fa-edit"></i></a> <a model = "Course" data-id="'+element.MembershipPlan.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a> <a model = "Course" data-id="'+btoa(element.MembershipPlan.id)+'" class="fafa-icons delete-row"  href="javascript:void(0);" title="Delete"><i class="fa fa-trash-o"></i></a>']).draw();
                    }
                    else{
                      table.row.add([element.MembershipPlan.plan_name,element.MembershipPlan.plan_cost,'Free',element.MembershipPlan.duration+' '+element.MembershipPlan.period == 0 ? "Days" : element.MembershipPlan.period == 1 ? "Months" : "Year",'<a class="fafa-icons edit" title="Edit" href="<?php echo HTTP_ROOT?>Admins/manageMembership/'+element.MembershipPlan.id+'"><i class="fa fa-edit"></i></a> <a model = "Course" data-id="'+element.MembershipPlan.id+'" class="fafa-icons change-status" title="Inactive" href="javascript:void(0);"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a> <a model = "Course" data-id="'+btoa(element.MembershipPlan.id)+'" class="fafa-icons delete-row"  href="javascript:void(0);" title="Delete"><i class="fa fa-trash"></i></a>']).draw();
                    }
                  }
                    

              });
           offset += 4;
 
        }
  });
*/
$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to change delete status ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      }
  });

    $(document).on('click','.change-status',function(){
        if(window.confirm("Are you sure you want to change status ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      }
  });
       
   })
</script>
