<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Reviews
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Cms/manageReviews">Manage Reviews</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->



            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Reviews</h3>
              </div>
              <table id="faq-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Rating</th>
                  <th>Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1; foreach($reviews as $data){?>
                <tr>
                  <td>
                    <?php echo $data['Review']['name']?>
                  </td>
                  <td>
                    <?php echo $data['Review']['rating']?>
                  </td>
                  <td>
                    <?php echo $data['Review']['created_date']?>
                  </td>
                  <td>
                    <?php  if($data['Review']['status'] == 0)
                              echo "Pending";
                           else
                            echo "Rejecetd";   

                    ?>
                  </td>
                  <td>
                     
                    
                    <a class="fafa-icons delete-row" title="Reply" model = "Review" data-id="<?php echo $data['Review']['id']?>" data-msg="<?php $msgs = explode(' ',$data['Review']['review']);foreach($msgs as $m){ if(in_array($m, $words)) echo "<span style='color:red'>".$m."</span> ";else echo $m." ";}?>" href="javascript:void(0)"><i class="fa fa-envelope-o" aria-hidden="true" data-toggle="modal" data-target="#mailModal"></i></a>
                    <a class="fafa-icons delete-data" title="Reply" model = "Review" data-id="<?php echo $data['Review']['id']?>" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                <?php $i++; }?>
                
              </table>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <div class="modal fade" id="mailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="post" action="<?php echo HTTP_ROOT.'Admins/review_mail';?>">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Message<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></h5>
        
      </div>
      <div class="modal-body">
      <div class="form-group col-md-12">
        <lable><b>Review :</b> </lable>
          <i class="fa input-error"></i>
          <p id="msg"></p>
        </div>
        <div class="form-group col-md-12">
        <lable><b>Reply :</b> </lable>
          <i class="fa input-error"></i>
          <textarea class="form-control required" placeholder="Message" name="data[Review][message]"></textarea>
          <input type="hidden" name="data[Review][id]" value="" id="review-id">
        </div>
        <div class="form-group col-md-12">
        <lable><b>Status :</b> </lable>
          <i class="fa input-error"></i>
          <input type="radio" class="required" value="0" placeholder="Message" name="data[Review][status]">Pending
          <input type="radio" class="required" value="1" placeholder="Message" name="data[Review][status]">Approved
          <input type="radio" class="required" value="2" placeholder="Message" name="data[Review][status]">Rejected
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Send</button>
      </div>
    </form>
    </div>
  </div>
</div>
<style>
  .modal-footer {
     border-top: none; 
</style>
<script type="text/javascript">
 let table = $('#faq-listing').DataTable({
                  "paging": true,
                  "searching":true,
                  "ordering":true
                });
   $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let id = $(this).attr('data-id');
        //console.log(id);
        let msg = $(this).attr('data-msg');
        let model = $(this).attr('model');
        $('#review-id').val(id);
        $('#msg').html(msg);
        /*$.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      //}
  });

  $(document).on('click','.delete-data',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let id = btoa($(this).attr('data-id'));
        //console.log(id);
        let model = $(this).attr('model');
       
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       

  })
</script>

  