<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gallery
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Admins/gallery">Gallery</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Photos</h3>
              <a href="<?php echo HTTP_ROOT?>Admins/addphoto" class="btn btn-primary pull-right">Add Photo +</a>
            </div>
            <div class="box_make">
              <div class="row">
                <?php foreach ($pics as $img) { ?>
                <div class="col-md-2" style="text-align: center;"><img class="img-responsive" src="<?php echo HTTP_ROOT . 'img/photos/' . $img['Photo']['image'];?>"><br>
                <?php if($img['Photo']['to_show'] == 1){ ?>
                  <a href="<?php echo HTTP_ROOT . 'Admins/chngestatus/' . base64_encode($img['Photo']['id']) . '/0'?>"><i class="fa fa-check" aria-hidden="true"></i></a>
                <?php } elseif($img['Photo']['to_show'] == 0){ ?>
                  <a href="<?php echo HTTP_ROOT . 'Admins/chngestatus/' . base64_encode($img['Photo']['id']) . '/1'?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                <?php } ?>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
<style>
  .modal-footer {
     border-top: none; }
  .box_make
  {
    padding: 2%;
  }
  .fa-check
  {
    color: green;
  }
  .fa-times
  {
    color: red;
  }
</style>
<script type="text/javascript">
 let table = $('#faq-listing').DataTable({
                  "paging": true,
                  "searching":true,
                  "ordering":true
                });
   $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let id = $(this).attr('data-id');
        //console.log(id);
        let msg = $(this).attr('data-msg');
        let model = $(this).attr('model');
        $('#review-id').val(id);
        $('#msg').html(msg);
        /*$.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      //}
  });

  $(document).on('click','.delete-data',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let id = btoa($(this).attr('data-id'));
        //console.log(id);
        let model = $(this).attr('model');
       
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       

  })
</script>

  