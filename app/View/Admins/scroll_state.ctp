<?php foreach($countries as $country){?>
                <tr>
                  <td>
                    <?php echo $country['Country']['country_name']?>
                  </td>
                  <td>
                    <?php echo $country['State']['statename']?>
                  </td>
                  <td>
                    <!-- <a title="Edit" href="<?php echo HTTP_ROOT.'Colleges/editCollege/'.$college['College']['id']?>"><i class="fa fa-edit"></i></a>  -->
                    <a class="fafa-icons" title="<?php echo $country['State']['status'] == '1' ? 'Active' : 'Inactive';?>" href="<?php echo HTTP_ROOT.'Colleges/changeStatus/State/'.$country['State']['id']?>"><i class="fa <?php echo $country['State']['status'] == '0' ? 'fa-toggle-off' : 'fa-toggle-on';?>"></i></a>
                    <a onclick="return confirm('Are you sure,You want to delete this ?');" title="Delete" href="<?php echo HTTP_ROOT.'Admins/delete/State/'.base64_encode($country['State']['id'])?>"><i class="fa fa-trash"></i></a> 
                  </td>
                </tr>
                <?php }?>