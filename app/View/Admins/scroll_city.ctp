<?php foreach($countries as $country){?>
                <tr>
                  <td>
                    <?php echo $country['State']['statename']?>
                  </td>
                  <td>
                    <?php echo $country['City']['city_name']?>
                  </td>
                  <td>
                    <a class="fafa-icons" title="<?php echo $country['City']['status'] == '1' ? 'Active' : 'Inactive';?>" href="<?php echo HTTP_ROOT.'Colleges/changeStatus/City/'.$country['City']['id']?>"><i class="fa <?php echo $country['City']['status'] == '0' ? 'fa-toggle-off' : 'fa-toggle-on';?>"></i></a>
                    <a onclick="return confirm('Are you sure,You want to delete this ?');" title="Delete" href="<?php echo HTTP_ROOT.'Admins/delete/City/'.base64_encode($country['City']['id'])?>"><i class="fa fa-trash"></i></a> 
                  </td>
                </tr>
                <?php }?>