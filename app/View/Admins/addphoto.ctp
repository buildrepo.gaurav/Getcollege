<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Photos
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Admins/addphoto">Add Photo</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Photos</h3>
            </div>
            <div class="box_make">
              <form method="post" action="" enctype="multipart/form-data" id="addphotos">
                <div class="row">
                  <div class="col-md-6">
                  	<label>Upload Photos<span style="color: red">*</span> (Multiple Allowed)</label>
                    <input type="file" class="form-control required" name="image[]" multiple>
                  </div>
                  <div class="col-md-6 shift">
                    <button class="btn btn-primary">Save</button>
                  </div>
                </div>
                <!-- <div class="row">
                  <button class="btn btn-primary">Save</button>
                </div> -->
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    $('#addphotos').validate();
  </script>
<style>
  .modal-footer {
     border-top: none; }
  .box_make
  {
    padding: 2%;
  }
  .shift
  {
    margin-top: 2.2%;
  }
  .error
  {
    color: red;
  }
</style>
<script type="text/javascript">
 let table = $('#faq-listing').DataTable({
                  "paging": true,
                  "searching":true,
                  "ordering":true
                });
   $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let id = $(this).attr('data-id');
        //console.log(id);
        let msg = $(this).attr('data-msg');
        let model = $(this).attr('model');
        $('#review-id').val(id);
        $('#msg').html(msg);
        /*$.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      //}
  });

  $(document).on('click','.delete-data',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let id = btoa($(this).attr('data-id'));
        //console.log(id);
        let model = $(this).attr('model');
       
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        
                       },
                async: false
              });
         table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       

  })
</script>

  