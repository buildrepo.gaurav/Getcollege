<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 8%;
    position: absolute;
}
.form-control{
  width:100% !important;
}
.select2{
  width: auto !important;
}
.input-sm{
  float: right;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage City
        <!-- <a href="<?php echo HTTP_ROOT?>Admins/addCity" class="btn btn-primary">Add City</a> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Admins/manageCity">Manage City</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add City</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo HTTP_ROOT.'Admins/addCity'?>" id="add-state-form" method='post' enctype="multipart/form-data">
             <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Country<span style="color: red">*</span></th>
                  <th>State<span style="color: red">*</span></th>
                  <th>City<span style="color: red">*</span></th>
                </tr>
                </thead>
                <tbody id="stream-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                     <select id="country" class="form-control required" name="data[City][country_id]">
                    <option value="">Select Country</option>
                    <?php foreach($country as $coun){?>
                    <option value="<?php echo $coun['Country']['id']?>"><?php echo $coun['Country']['country_name']?></option>
                    <?php }?>
                  </select>
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                     <select id="state" class="form-control required" name="data[City][state_id]">
                    <option value="">Select State</option>
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                  <input type="text" class="form-control required" placeholder="Enter Name" name="data[City][city_name]" >
                  </td>
                  <!-- <td>
                  <i class="fa input-error1"></i>
                    <input type="file" class="form-control" placeholder="" name="image">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required number" placeholder="Conversion Rate" name="data[Country][conversion_rate]" value="<?php echo @$country['Country']['conversion_rate']?>">
                  </td> -->
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">City Listing</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="city-listing" class="table table-bordered table-striped"><!-- id="city-listing" -->
                <thead>
                <tr>
                  <th>State</th>
                  <th>City Name</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="resp">
                <?php foreach($countries as $country){?>
                <tr class="city-list">
                  <td>
                    <select class="form-control required" name="state_id" >
                      <option value="">Select Country</option>
                      <?php foreach($state as $coun){?>
                      <option value="<?php echo $coun['State']['id']?>" <?php echo $coun['State']['id'] == $country['City']['state_id'] ? 'selected' : '';?>><?php echo $coun['State']['statename']?></option>
                      <?php }?>
                    </select>
                    <input type="hidden" name="id" value="<?php echo @$country['City']['id']?>">
                  </td>
                  <td>
                    <input class="form-control required" placeholder="City" name="city_name" readonly value="<?php echo @$country['City']['city_name']?>">
                  </td>
                  <td>
                    <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                    <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a>
                    <a model = "City" data-id="<?php echo $country['City']['id']?>" class="fafa-icons change-status" title="<?php echo $country['City']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $country['City']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                    <a  class="delete-row" model = "City" data-id="<?php echo base64_encode($country['City']['id'])?>" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['City']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tbody>
              </table>
              <!-- <div class="pagination-div dataTables_paginate paging_simple_numbers" id="srch1">
                <ul class="pagination">
                  <li class="paginate_button previous"><?php echo $this->Paginator->prev(' << ' . __(''),array(),null,array('class' => 'prev disabled'));?></li>
                  <li><?php echo $this->Paginator->numbers(array('separator'=>null));?></li>
                  <li class="paginate_button next"><?php echo $this->Paginator->next(' >> ' . __(''),array(),null,array('class' => 'next disabled'));?></li>
                 
               </ul>
              </div> -->
            </div>
            <!-- /.box-body -->

          </div>

          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript">
  $('select').select2();
  $('.city-list').closest('tr').find("select").select2().enable(false);
</script>
<script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).closest('tr').find("select").select2().enable(true);
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      $(this).closest('tr').find("select").select2().enable(false);
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editCity',
          data : {data : data},
          success : function(resp){
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          
          }
      });
    })
  </script>
<script type="text/javascript">

    let offset = 10;
    let action  = 'getCity';
    let resp = '';

    let table =   $('#city-listing').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           console.log(resp);
           resp.forEach(function(element) {
                  if(element.City.status == 1){
                    var is_delete = (element.City.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<select id="city-'+element.City.id+'" class="form-control required" name="state_id" ><option value="">Select Country</option><?php foreach($state as $coun){?><option value="<?php echo $coun['State']['id']?>"><?php echo $coun['State']['statename']?></option><?php }?></select><input type="hidden" name="id" value="'+element.City.id+'">','<input class="form-control required" placeholder="City" name="city_name" readonly value="'+element.City.city_name+'">','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a class="fafa-icons change-status" title="Active" href="javascript:void(0)" model = "City" data-id="'+element.City.id+'"><img src="<?php echo HTTP_ROOT.'img/active.png'?>" width="30px"></a><a  title="Delete" href="javascript:void(0)" class="delete-row" model = "City" data-id="'+btoa(element.City.id)+'"><i class="fa '+is_delete+'"></i></a>']).draw(); 
                  }
                  else{
                    var is_delete = (element.City.is_delete == 0) ? "fa-trash-o" : "fa-trash";
                    table.row.add(['<select id="city-'+element.City.id+'" class="form-control required" name="state_id" ><option value="">Select Country</option><?php foreach($state as $coun){?><option value="<?php echo $coun['State']['id']?>"><?php echo $coun['State']['statename']?></option><?php }?></select><input type="hidden" name="id" value="'+element.City.id+'">','<input class="form-control required" placeholder="City" name="city_name" readonly value="'+element.City.city_name+'">','<a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a><a class="fafa-icons change-status" title="Inactive" href="javascript:void(0)" model = "City" data-id="'+element.City.id+'"><img src="<?php echo HTTP_ROOT.'img/inactive.png'?>" width="30px"></a><a title="Delete" href="javascript:void(0)" class="delete-row" model = "City" data-id="'+btoa(element.City.id)+'"><i class="fa '+is_delete+'"></i></a>']).draw(); 
                  }
                  $('#city-'+element.City.id).val(element.City.state_id).change();
              });
           offset += 10;
           $('select').select2();
            $('.odd').closest('tr').find("select").select2().enable(false);
            $('.even').closest('tr').find("select").select2().enable(false);
        }
  });
  $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title', 'Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title', 'Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>