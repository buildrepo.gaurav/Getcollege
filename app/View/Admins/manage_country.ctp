<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 15%;
    position: absolute;
}
</style>
<script>
  $(document).ready(function(){
    $('.img-input').hide();
  });
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Country
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Admins/manageCountry">Manage Country</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Country</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?php echo HTTP_ROOT.'Admins/addCountry'?>" id="add-country-form" method='post' enctype="multipart/form-data">
             <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Country<span style="color: red">*</span></th>
                  <th>Code<span style="color: red">*</span></th>
                  <th>Symbol<span style="color: red">*</span></th>
                  <th>Conversion Rate<span style="color: red">*</span></th>
                </tr>
                </thead>
                <tbody id="stream-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                     <input type="text" class="form-control required" placeholder="Enter Name" name="data[Country][country_name]" value="<?php echo @$country['Country']['country_name']?>">
                    <input type="hidden" name="data[Country][id]" value="<?php echo @$country['Country']['id']?>">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required" placeholder="Country Code" name="data[Country][country_code]" value="<?php echo @$country['Country']['country_code']?>" >
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="file" class="form-control required" placeholder="" name="image">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required number" placeholder="Conversion Rate" name="data[Country][conversion_rate]" value="<?php echo @$country['Country']['conversion_rate']?>">
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Country Listing</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="country-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Country Name</th>
                  <th>Country Code</th>
                  <th>Country Symbol</th>
                  <th>Conversion Rate</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($countries as $country){?>
                <tr>
                  <td>
                    <input readonly type="text" class="form-control required" placeholder="Enter Name" name="country_name" value="<?php echo $country['Country']['country_name']?>">
                    <input readonly type="hidden" name="id" value="<?php echo @$country['Country']['id']?>">
                    
                  </td>
                  <td>
                    <input readonly type="text" class="form-control required" placeholder="Country Code" name="country_code" value="<?php echo $country['Country']['country_code']?>" >
                    
                  </td>
                  <td>
                    <input type="file" class="form-control img-input required" placeholder="" name="image">
                    <img class="img-img" src="<?php echo HTTP_ROOT.'img/'.$country['Country']['symbol']?>">
                  </td>
                  <td>
                    <input type="text" class="form-control required number" placeholder="Conversion Rate" name="conversion_rate" value="<?php echo $country['Country']['conversion_rate']?>" readonly>
                    
                  </td>
                  <td>
                    <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                    <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a>
                    <a model = "Country" data-id="<?php echo $country['Country']['id']?>" class="fafa-icons change-status" title="<?php echo $country['Country']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $country['Country']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                    <a model = "Country" class="fafa-icons delete-row" data-id="<?php echo base64_encode($country['Country']['id'])?>" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Country']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                  </td>
                </tr>
                <?php }?>
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $('.img-img').hide();
      $(this).closest('tr').find("input,textarea,select").removeAttr('readonly');
      $(this).next().show();
      $('.img-input').show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $('.img-input').hide();
      $(this).prev().show();
      $('.img-img').show();

      let data = $(this).closest('tr').find("input,textarea,select").serializeArray();
      $(this).closest('tr').find("input,textarea,select").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editCountry',
          data : {data : data},
          success : function(resp){
            console.log(resp);
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          }
      });
    })
  </script>
<script type="text/javascript">
let table = $('#country-listing').DataTable({
                  "paging": false,
                  "searching":true,
                  "ordering":true
                });
   $(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }*/
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title', 'Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title', 'Active');
                          }
                       },
                async: false
              });
      
  });
       
   })
</script>
