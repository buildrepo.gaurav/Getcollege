<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 75%;
    position: absolute;
}

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Notify List
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>Admins/notifyList">Notify List</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->



            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Notify Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Course</th>
                    <th>Country</th>
                    <th>State</th>
                    <th>City</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($notify as $data){?>
                  <tr>
                    <td>
                      <?php echo $data['Notify']['name']?>
                    </td>
                    <td>
                      <?php echo $data['Notify']['email']?>
                    </td>
                    <td>
                      <?php echo $data['Notify']['mobile']?>
                    </td>
                    <td>
                      <?php echo $data['Course']['name']?>
                    </td>
                    <td>
                      <?php echo $data['Country']['country_name']?>
                    </td>
                    <td>
                      <?php echo $data['State']['statename']?>
                    </td>
                    <td>
                      <?php echo $data['City']['city_name']?>
                    </td>
                    <td>
                      <a model = "Notify" data-id="<?php echo base64_encode($data['Notify']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Notify']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tbody> 
                  
                </table>
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

      <script type="text/javascript">

    let offset = 10;
    let action  = 'getNotify';
    let resp = '';

    let table =   $('#facility-form').DataTable({
        "paging": false,
        "searching":true,
        "ordering":true
      });

    $(window).scroll(function() {
         
        if($(window).scrollTop() + $(window).height() == $(document).height()) {


           resp =  getpages( offset, action);
           //console.log(resp);
           resp.forEach(function(element) {

                    table.row.add([element.Notify.name,element.Notify.email,element.Notify.mobile,element.Course.name,element.Country.country_name,element.State.statename,element.City.city_name,'<a model = "Notify" data-id="'+btoa(element.Notify.id)+'" class="delete-row fafa-icons" title="Delete" href="javascript:void(0)"><i class="fa fa-trash"></i></a>']).draw();

              });
           offset += 10;
 
        }
  });
$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                       },
                async: false
              });
       table.row( $(this).parents('tr') )
        .remove()
        .draw();
      }
  });


       
   })
</script>
