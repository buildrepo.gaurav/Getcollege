<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <title>Get In Colleges | Dashboard</title>
	  <!-- Tell the browser to be responsive to screen width -->
	  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	 
	  <!-- Theme style -->
	  <link rel="stylesheet" href="">
	  <!-- AdminLTE Skins. Choose a skin from the css/skins
	       folder instead of downloading all of them to reduce the load. -->
	  <!-- iCheck -->
	 
			<?php echo $this->Html->css(array(
												'bootstrap/css/bootstrap.min.css',
												
												'/plugins/datatables/dataTables.bootstrap.css',
												'AdminLTE.min.css',
												'skins/_all-skins.min.css',
												'/plugins/iCheck/flat/blue.css',
												'/plugins/morris/morris.css',
												'/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
												'/plugins/datepicker/datepicker3.css',
												'/plugins/daterangepicker/daterangepicker.css',
												'/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
												'bootstrap-multiselect.css',
												'/dist/css/tooltipster.bundle.min.css'
											)
										);
			?>
		 <!-- Font Awesome -->
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	  <!-- Ionicons -->
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">		
	  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	  <!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	  <![endif]-->
	  <style>
	  .modal-body
	  {
	  	padding: 0px!important;
	  }
	  	.icon{
	  		    padding-top: 7%;
	  	}
	  	.has-success{
    
    -webkit-box-shadow: none;
    box-shadow: none;
}
.has-error{
    
    -webkit-box-shadow: none;
    box-shadow: none;
}
.input-error{
	float: right;
    right: 8px;
    margin: 33px 13px;
    position: absolute;
}
.fa-warning{
	color : red;
}
.fa-check{
	color :green;
}
.input-icon.right > i {
    right: 8px;
    float: right;
}
.input-icon > i {
    display: block;
    position: absolute;
    margin: 11px 12px 4px 10px;
    z-index: 3;
    width: 16px;
    font-size: 16px;
    text-align: center;
}
.fafa-icons >.fa, .fa-trash,.fa-plus{
	font-size: 24px;
}
.fafa-icons > img{
	margin-top:-10px !important;
}

	  </style>
	  <?php $currentAction = $this->params['action'];?>
	  <?php echo $this->Html->script(array(
											'/plugins/jQuery/jquery-2.2.3.min.js',
											'jquery-ui.min.js',
											'bootstrap/js/bootstrap.min.js',
											'raphael-min.js',
											'custom.js'
											)
									);
		  if($currentAction == 'dashboard'){
		  	echo $this->Html->script(array('/plugins/morris/morris.min.js'));
		  }
		  echo $this->Html->script(array(	
											'/plugins/sparkline/jquery.sparkline.min.js',
											'/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
											'/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
											'/plugins/knob/jquery.knob.js',
											'moment.min.js',
											'/plugins/daterangepicker/daterangepicker.js',
											'/plugins/datepicker/bootstrap-datepicker.js',
											'/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',

											'/plugins/datatables/jquery.dataTables.min.js',
											'/plugins/datatables/dataTables.bootstrap.min.js',
											'/plugins/slimScroll/jquery.slimscroll.min.js',
											'/plugins/fastclick/fastclick.js',
											'app.min.js',
											'bootstrap-multiselect.js',

											)
		  							);
		  if($currentAction == 'dashboard'){
		  	echo $this->Html->script(array('pages/dashboard.js'));
		  }

		  echo $this->Html->script(array(

											'demo.js',
											'validate.js',
											'jquery.form.js'

											)
									);
	?>
	<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
	</head>
	
	
	<body class="hold-transition skin-blue sidebar-mini" id="result">
		<div class="wrapper">
		<?php
			echo $this->element('admin/adminHeader');
			echo $this->element('admin/leftsidebar');
			echo $this->fetch('content');
			echo $this->element('admin/adminFooter');
		?>
		</div>
	</body>
	
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

</html>


<!--admin success-msg Modal -->
<div id="adminsuccess-msg" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Success Message</h4>
      </div>
      <div class="modal-body">
        <div class="login-box-body">
   
      
      <div class="row">
        <div class="col-xs-12">
          
          <h4><?php echo $this->Session->read('adminsuccess-msg');?></h4>
          
        </div>
        
      </div>

    
  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- modal ends---------------->


<!-- admin error-msg Modal -->
<div id="adminerror-msg" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error Message</h4>
      </div>
      <div class="modal-body">
        <div class="login-box-body">
   
      
      <div class="row">
        <div class="col-xs-12">
          
          <h4><?php echo $this->Session->read('adminerror-msg');?></h4>
          
        </div>
        
      </div>

    
  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- modal ends---------------->

<script>
	$(document).ready(function(){
		<?php if($this->Session->check('adminsuccess-msg')){?>
			$('#adminsuccess-msg').modal('show');
			setTimeout(function(){ $('#adminsuccess-msg').modal('hide'); }, 2000);
			<?php unset($_SESSION['adminsuccess-msg']);?>
		<?php }?>
		<?php if($this->Session->check('adminerror-msg')){?>
			$('#adminerror-msg').modal('show');
			setTimeout(function(){ $('#adminerror-msg').modal('hide'); }, 2000);
			<?php unset($_SESSION['adminerror-msg']);?>
		<?php }?>
	});
</script>