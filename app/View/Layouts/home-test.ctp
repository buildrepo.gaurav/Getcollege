<!DOCTYPE html>
<!-- saved from url=(0038)  -->
<html lang="en" data-ng-app="website" class="ng-scope">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <?php 
    $title=$this->params['action'];
    //echo $title;
         
    ?>
    <!-- website title -->
    <?php if($title == 'index') { ?>
        <title>GetCollege.in | Organizing College life in one click </title>
        <meta name="keywords" content="Scholarship, admission, procedures, testseries">
        <meta name="description" content="Getcollege.in a recognised GOI start-up company, provides dedicated login portal for student and college. The login portal for students provides them with all  the information about their career option, scholarships, admission procedures, test series." />
    <?php } elseif($title == 'aboutus'){?>
        <title>GetCollege.in | Student Career Counselling | Student Login Portal | Scholarship Scheme | College Login</title>
        <meta name="keywords" content="Basic details, Acadmic details, personal details, work experience, student awards">
        <meta name="description" content="GetCollege.in provides overall 360 degree solution for all student requirements. We provide to student login portal in which they can update their bacic details, academic details, personal details, work experience, student awards etc" />
    <?php }elseif($title == 'ourteam'){?>
        <title>GetCollege.in | Buddy in your search | Exploring with you | Answering your question | Explore career</title>
        <meta name="keywords" content="GetCollege.in team, Support Team">
        <meta name="description" content="Our team comprises of more than 50 member, working 24x7 tirelessly to serve you better. Student satisfaction is our ultimate goal." />
    <?php } elseif($title == 'contact'){?>
        <title>GetCollege.in | May I Help You | Exploring with you | 24x7 Service</title>
        <meta name="keywords" content="Contact Us, Query Regarding Career">
        <meta name="description" content="For any enquiry, kindly contact Tel:01204202822 and email:getincolleges@gmail.com" />
    <?php }elseif($title == 'terms'){?>
        <title>GetCollege.in | Terms and Conditions</title>
        <meta name="keywords" content="Terms and Conditions">
        <meta name="description" content="Get to know our terms of use. These have been be made for making you aware of right use of website." />
    <?php } elseif($title == 'faqs'){?>
        <title>Get College.in | Frequently Ask Questions</title>
        <meta name="keywords" content="Frequently Ask Questions">
        <meta name="description" content="We provides solutions of your questions and we provide 24x7 support" />
    <?php }else{?>
        <title>Get College.in | College Admission -2017 | MBA | Engineering | Counselling for Schools</title>
        <meta name="keywords" content="Counselling for 11th & 12th, admission in college, MBA 2018, Engineering Admission 2018, career after 12th, College Admission 2018, B.Tech 2018, College Ranking 2018, Compare college, searching colleges">
        <meta name="description" content="Get College.in is a part of every student’s journey, beginning from decision to opt for a career, searching colleges, filling forms, completing their course and reviewing their journey. More info call now: 9818734659" />
    <?php }?>
		<meta name="robots" content="noindex, follow">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="SHORTCUT ICON" href="<?php echo HTTP_ROOT?>/frontend/favicon.png" type="image/vnd.microsoft.icon">

        <link href="<?php echo HTTP_ROOT?>newcss/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo HTTP_ROOT?>newcss/css/custom.css" rel="stylesheet">
        <link href="<?php echo HTTP_ROOT?>newcss/css/margins.css" rel="stylesheet">
        <link href="<?php echo HTTP_ROOT?>newcss/css/flexslider.css" rel="stylesheet">
        <link href="<?php echo HTTP_ROOT?>newcss/css/owl.carousel.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <script src="<?php echo HTTP_ROOT?>newjs/js/bootstrap.min.js"></script>
        <script src="<?php echo HTTP_ROOT?>newjs/js/jquery.flexslider-min.js"></script>
        <script src="<?php echo HTTP_ROOT?>newjs/js/owl.carousel.min.js"></script>
    </head>
    <body cz-shortcut-listen="true">
        <?php            
                    //echo $this->element('frontEnd/feedback');
                    echo $this->element('frontEnd/new_feedback');
            ?>
        <div class="page">
            <?php
                $title=$this->params['action'];
                    if($title == 'landingPage'){
                        echo $this->element('frontEnd/landing_header');
                    }else{
                        echo $this->element('frontEnd/header');
                        //echo $this->element('frontEnd/new_header');
                    }
                    
                    echo $this->fetch('content');
            ?>
        </div>
            <?php     
                
                if($title == 'landingPage'){
                    echo $this->element('frontEnd/landing_footer');
                }else{       
                    echo $this->element('frontEnd/footer');
                    //echo $this->element('frontEnd/new_footer');
                }
            ?>

    </body>
    <?php            
                    echo $this->element('frontEnd/modal');
    ?>

    <script type="text/javascript">
      $('.owl-carousel').owlCarousel({
          items:1,
          merge:true,
          loop:true,
          margin:10,
          video:true,
          lazyLoad:true,
          center:true,
          responsive:{
              480:{
                  items:2
              },
              600:{
                  items:4
              }
          }
      })
      $(window).load(function() {
          $('.flexslider').flexslider({
            animation: "fade",
            animationLoop: true,
            slideshowSpeed: 3500, 
            animationSpeed: 1000,
            controlNav: false,
            directionNav: false
          });
      });
      function createDropDown(){
      var source = $("#source");
      var selected = source.find("option[selected]");  // get selected <option>
      var options = $("option", source);  // get all <option> elements
      // create <dl> and <dt> with selected value inside it
      $("body").append('<dl id="target" class="dropdown"></dl>')
      $("#target").append('<dt><a href="#">' + selected.text() + 
          '<span class="value">' + selected.val() + 
          '</span></a></dt>')
      $("#target").append('<dd><ul></ul></dd>')
      // iterate through all the <option> elements and create UL
      options.each(function(){
          $("#target dd ul").append('<li><a href="#">' + 
              $(this).text() + '<span class="value">' + 
              $(this).val() + '</span></a></li>');
      });
  }
  var feedbackTab = {
 
            speed:300,
            containerWidth:$('.feedback-panel').outerWidth(),
            containerHeight:$('.feedback-panel').outerHeight(),
            tabWidth:$('.feedback-tab').outerWidth(),
         
         
            init:function(){
                $('.feedback-panel').css('height',feedbackTab.containerHeight + 'px');
         
                $('a.feedback-tab').click(function(event){
                    if ($('.feedback-panel').hasClass('open')) {
                        $('.feedback-panel')
                        .animate({right:'-' + feedbackTab.containerWidth}, feedbackTab.speed)
                        .removeClass('open');
                    } else {
                        $('.feedback-panel')
                        .animate({right:'0'},  feedbackTab.speed)
                        .addClass('open');
                    }
                    event.preventDefault();
                });
            }
        };
         
        feedbackTab.init();
        $( '.type-text' ).teletype( {
            text: [ 'Our Successful Free Product Distribution Programs', 'Fill in the form to Get in Touch with us' ],
            typeDelay: 0,
            backDelay: 20
        } );
      

    </script>
   <script>
   $(document).ready(function() {
    <?php if($this->Session->check('membership_modal')){?>
        
        $('#membershipModal').modal('show');
    <?php unset($_SESSION['membership_modal']); }?>
    <?php if($this->Session->check('signup_modal')){?>
        
        
        $('#signup-msg').modal('show');
    <?php unset($_SESSION['signup_modal']); }?>
    <?php if($this->Session->check('activation_modal')){?>
        
        
        $('#activate-msg').modal('show');
    <?php unset($_SESSION['activation_modal']); }?>
   });
   </script>
   <script>
    $(document).ready(function(){
        <?php if($this->Session->check('frontsuccess-msg')){?>
            $('#frontend-msg').modal('show');
            <?php unset($_SESSION['frontsuccess-msg']);?>
        <?php }?>

    });
</script>
</html>