<!DOCTYPE html>
<!-- saved from url=(0038)  -->
<html lang="en" data-ng-app="website" class="ng-scope">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <?php 
    $title=$this->params['action'];
    //echo $title;
         
    ?>
    <!-- website title -->
    <?php if($title == 'index') { ?>
        <title>GetCollege.in | Organizing College life in one click </title>
        <meta name="keywords" content="Scholarship, admission, procedures, testseries">
        <meta name="description" content="Getcollege.in a recognised GOI start-up company, provides dedicated login portal for student and college. The login portal for students provides them with all  the information about their career option, scholarships, admission procedures, test series." />
    <?php } elseif($title == 'aboutus'){?>
        <title>GetCollege.in | Student Career Counselling | Student Login Portal | Scholarship Scheme | College Login</title>
        <meta name="keywords" content="Basic details, Acadmic details, personal details, work experience, student awards">
        <meta name="description" content="GetCollege.in provides overall 360 degree solution for all student requirements. We provide to student login portal in which they can update their bacic details, academic details, personal details, work experience, student awards etc" />
    <?php }elseif($title == 'ourteam'){?>
        <title>GetCollege.in | Buddy in your search | Exploring with you | Answering your question | Explore career</title>
        <meta name="keywords" content="GetCollege.in team, Support Team">
        <meta name="description" content="Our team comprises of more than 50 member, working 24x7 tirelessly to serve you better. Student satisfaction is our ultimate goal." />
    <?php } elseif($title == 'contact'){?>
        <title>GetCollege.in | May I Help You | Exploring with you | 24x7 Service</title>
        <meta name="keywords" content="Contact Us, Query Regarding Career">
        <meta name="description" content="For any enquiry, kindly contact Tel:01204202822 and email:getincolleges@gmail.com" />
    <?php }elseif($title == 'terms'){?>
        <title>GetCollege.in | Terms and Conditions</title>
        <meta name="keywords" content="Terms and Conditions">
        <meta name="description" content="Get to know our terms of use. These have been be made for making you aware of right use of website." />
    <?php } elseif($title == 'faqs'){?>
        <title>Get College.in | Frequently Ask Questions</title>
        <meta name="keywords" content="Frequently Ask Questions">
        <meta name="description" content="We provides solutions of your questions and we provide 24x7 support" />
    <?php }else{?>
        <title>Get College.in | College Admission -2017 | MBA | Engineering | Counselling for Schools</title>
        <meta name="keywords" content="Counselling for 11th & 12th, admission in college, MBA 2018, Engineering Admission 2018, career after 12th, College Admission 2018, B.Tech 2018, College Ranking 2018, Compare college, searching colleges">
        <meta name="description" content="Get College.in is a part of every studentís journey, beginning from decision to opt for a career, searching colleges, filling forms, completing their course and reviewing their journey. More info call now: 9818734659" />
    <?php }?>
		<meta name="robots" content="noindex, follow">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="SHORTCUT ICON" href="<?php echo HTTP_ROOT?>/frontend/favicon.png" type="image/vnd.microsoft.icon">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <link rel="stylesheet" href="<?php echo HTTP_ROOT?>/frontend/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT?>/frontend/assets.min.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT?>/frontend/jquery-ui.min.css">
        
        <script src="<?php echo HTTP_ROOT?>/frontend/jquery-ui.min.js" type="text/javascript"></script>
        
        <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
         <link rel="stylesheet" href="<?php echo HTTP_ROOT?>/frontend/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT?>/frontend/bootstrap-stars.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT?>/frontend/styles(1).css" id="moto-website-style">
        
        <link rel="stylesheet" href="<?php echo HTTP_ROOT?>/frontend/styles.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT?>/frontend/css/star-rating.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT?>css/bootstrap-multiselect.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT ?>css/bootstrap-datepicker3.min.css">
        <?php if($title == 'index') { ?>
        <!-- <link href="<?php echo HTTP_ROOT?>css/custom.css" rel="stylesheet">
        <link href="<?php echo HTTP_ROOT?>/css/margins.css" rel="stylesheet"> -->
        <?php }?>
        <script src="<?php echo HTTP_ROOT?>/frontend/js/jquery.js"></script>
         <script src="<?php echo HTTP_ROOT?>/frontend/website.assets.min.js.download" type="text/javascript" data-cfasync="false"></script>
         <script src="<?php echo HTTP_ROOT?>/frontend/website.min.js.download" type="text/javascript" data-cfasync="false"></script>
        <script src="<?php echo HTTP_ROOT?>/frontend/js/bootstrap.min.js"></script>
        <script src="<?php echo HTTP_ROOT?>/frontend/jquery.barrating.min.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_ROOT?>/frontend/jquery.flexslider-min.js"></script>
        <script src="<?php echo HTTP_ROOT?>/frontend/owl.carousel.js" type="text/javascript"></script>
        <style>
        .datepicker{
            min-width:220px !important;
            width:220px !important;
        }
        </style>                     
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        
         <script src="<?php echo HTTP_ROOT?>/frontend/js/star-rating.js" type="text/javascript"></script>
        <?php
          echo $this->Html->script(array(
                                            'validate.js',
                                            'jquery.form.js',
                                            'frontcustom.js',
                                            '/plugins/daterangepicker/daterangepicker.js',
                                            '/plugins/datepicker/bootstrap-datepicker.min.js',
                                            'bootstrap-multiselect.js',

                                            )
                                    );
        ?> 
        <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
        <script>
            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else { 
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            }
            
            function showPosition(position) {
                console.log(position.coords.latitude);/* = "Latitude: " +  + 
                "<br>Longitude: " + position.coords.longitude;*/
            }
        </script>
      
        <!-- <script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> -->
    </head>
    <body class="moto-background" cz-shortcut-listen="true">
       
        <?php            
                    echo $this->element('frontEnd/feedback');
            ?>
        <div class="page">
            <?php
                $title=$this->params['action'];
                    if($title == 'landingPage'){
                        echo $this->element('frontEnd/landing_header');
                    }else{
                        echo $this->element('frontEnd/header');
                    }
                    
                    echo $this->fetch('content');
            ?>
        </div>
            <?php     
                
                if($title == 'landingPage'){
                    echo $this->element('frontEnd/landing_footer');
                }else{       
                    echo $this->element('frontEnd/footer');
                }
            ?>

    </body>
    <?php            
                    echo $this->element('frontEnd/modal');
    ?>


    <style>
            .goog-te-banner-frame{
            display: none !important;
            }
            body{
            top : 0px !important;
            }
        </style>
       
        <script>
            var websiteConfig = websiteConfig || {};
            websiteConfig.address = ' ';
            websiteConfig.apiUrl = '/api.php';
            websiteConfig.preferredLanguage = 'en';
            websiteConfig.back_to_top_button = {
                "enabled": true,
                "topOffset": 300,
                "animationTime": 500
            };
            angular.module('website.plugins', []);
        </script>
       
        <script>
            $(document).ready(function() {
                //$('#preference-modal').modal('show');
            });
            $('.fdbk1').on('click', function() {
                $('.fdbk-pnl1').toggleClass('feedback-anim-show');
            });
            $('.fdbk2').on('click', function() {
                $('.fdbk-pnl2').toggleClass('feedback-anim-show');
            });
            $('.fdbk3').on('click', function() {
                $('.fdbk-pnl3').toggleClass('feedback-anim-show3');
            });
            $('.example1').barrating({
                theme: 'bootstrap-stars',
                initialRating: 0,
                readonly: true,
            });
            $(document).ready(function() {
                $('.trc-owl').owlCarousel({
                    loop: true,
                    margin: 10,
                    nav: true,
                    autoplay: true,
                    autoplayTimeout: 2000,
                    autoplayHoverPause: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 3
                        },
                        1000: {
                            items: 4
                        }
                    }
                })
            });
            $(document).ready(function() {
                $(".search-main-btn").hover(function() {
                    $(".srch-inner").show(800);
                    $(this).hide(600);
                    $(".bx-caption").hide(600);
                })
                $(".close-btn").click(function() {
                    $(".srch-inner").hide(800);
                    $(".search-main-btn").show(600);
                    $(".bx-caption").show(600);
                })
                $('#modal-owl').owlCarousel({
                    loop: true,
                    items: 1,
                    margin: 10,
                    nav: true
                })
            });
        </script>
        <script type="text/javascript">
            $(window).scroll(function() {    
               var scroll = $(window).scrollTop();
               if (scroll >= 100) {
                   $(".chng_logo").addClass("smaller");
               }
               else  {
                   $(".chng_logo").removeClass("smaller");
               }
            });
        </script>
        <script>
            $(document).scroll(function(){
            if(jQuery(this).scrollTop()){   
            $('darkcol_2').removeClass();
            
            }
            else {
             
             
              $('.moto-widget-image-picture').addClass('darkcol_2');
            
            }
            });
        </script>
   <script>
   $(document).ready(function() {
    <?php if($this->Session->check('membership_modal')){?>
        
        $('#membershipModal').modal('show');
    <?php unset($_SESSION['membership_modal']); }?>
    <?php if($this->Session->check('signup_modal')){?>
        
        
        $('#signup-msg').modal('show');
    <?php unset($_SESSION['signup_modal']); }?>
    <?php if($this->Session->check('activation_modal')){?>
        
        
        $('#activate-msg').modal('show');
    <?php unset($_SESSION['activation_modal']); }?>

    <?php if($title == 'index') { ?>
    /*$('#disclaimer-msg').modal('show');*/
    <?php }?>
   });

   </script>
   <script>
    $(document).ready(function(){
        <?php if($this->Session->check('frontsuccess-msg')){?>
            $('#frontend-msg').modal('show');
            <?php unset($_SESSION['frontsuccess-msg']);?>
        <?php }?>

    });
</script>
    <script type="text/javascript">
      $('.owl-carousel').owlCarousel({
          items:1,
          merge:true,
          loop:true,
          margin:10,
          video:true,
          lazyLoad:true,
          center:true,
          responsive:{
              480:{
                  items:2
              },
              600:{
                  items:4
              }
          }
      })
      $(window).load(function() {
          $('.flexslider').flexslider({
            animation: "fade",
            animationLoop: true,
            slideshowSpeed: 3500, 
            animationSpeed: 1000,
            controlNav: false,
            directionNav: false
          });
      });
      function createDropDown(){
      var source = $("#source");
      var selected = source.find("option[selected]");  // get selected <option>
      var options = $("option", source);  // get all <option> elements
      // create <dl> and <dt> with selected value inside it
      $("body").append('<dl id="target" class="dropdown"></dl>')
      $("#target").append('<dt><a href="#">' + selected.text() + 
          '<span class="value">' + selected.val() + 
          '</span></a></dt>')
      $("#target").append('<dd><ul></ul></dd>')
      // iterate through all the <option> elements and create UL
      options.each(function(){
          $("#target dd ul").append('<li><a href="#">' + 
              $(this).text() + '<span class="value">' + 
              $(this).val() + '</span></a></li>');
      });
  }
  var feedbackTab = {
 
            speed:300,
            containerWidth:$('.feedback-panel').outerWidth(),
            containerHeight:$('.feedback-panel').outerHeight(),
            tabWidth:$('.feedback-tab').outerWidth(),
         
         
            init:function(){
                $('.feedback-panel').css('height',feedbackTab.containerHeight + 'px');
         
                $('a.feedback-tab').click(function(event){
                    if ($('.feedback-panel').hasClass('open')) {
                        $('.feedback-panel')
                        .animate({right:'-' + feedbackTab.containerWidth}, feedbackTab.speed)
                        .removeClass('open');
                    } else {
                        $('.feedback-panel')
                        .animate({right:'0'},  feedbackTab.speed)
                        .addClass('open');
                    }
                    event.preventDefault();
                });
            }
        };
         
        feedbackTab.init();
        $( '.type-text' ).teletype( {
            text: [ 'Our Successful Free Product Distribution Programs', 'Fill in the form to Get in Touch with us' ],
            typeDelay: 0,
            backDelay: 20
        } );
      

    </script>
</html>