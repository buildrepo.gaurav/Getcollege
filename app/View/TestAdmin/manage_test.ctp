<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 18%;
    position: absolute;
}

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Test
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTest">Test</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Test</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form role="form" id="add-course-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>

                  <th>Test Level</th>
                  <th>Test</th>
                  <th>Time(Hour)</th>
                  <th>Time(Minute)</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="stream-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                    <select class="form-control" name="data[1][Test][test_level_id]">
                      <option value="">Select Test Level</option>
                      <?php foreach($levels as $level){?>
                        <option value="<?php echo $level['TestLevel']['id']?>" <?php echo @$test['Test']['test_level_id'] == $level['TestLevel']['id'] ? "selected" : "";?>><?php echo $level['TestLevel']['level']?></option>
                      <?php }?>
                    </select>
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required" required placeholder="Enter Test Name" name="data[1][Test][title]" value="<?php echo @$test['Test']['title']?>">
                    <input type="hidden" name="data[1][Test][id]" value="<?php echo @$testId?>">
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control number required" required placeholder="Hour" name="data[1][Test][hour]" value="<?php echo @$test['Test']['hour']?>">
                   
                  </td>
                  <td>
                    <input type="text" class="form-control number required" required placeholder="Minutes" name="data[1][Test][minute]" value="<?php echo @$test['Test']['minute']?>">
                  </td>
                  
                  <td>
                    <a href="javascript:void(0);" class="append-test-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Test Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th>Level</th>
                    <th>Test</th>
                    <th>Time</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($tests as $data){?>
                  <tr>
                  <td>
                     <?php echo $data['TestLevel']['level']?>
                      
                    </td>
                    <td>
                     <?php echo $data['Test']['title']?>
                      
                    </td>
                    <td>
                     <?php echo $data['Test']['hour'].' hrs '.$data['Test']['minute'].' mins';?>
                      
                    </td>
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="<?php echo HTTP_ROOT.'TestAdmin/ManageTest/'.$data['Test']['id']?>"><i class="fa fa-edit"></i></a>
                      
                      <a model = "Test" data-id="<?php echo $data['Test']['id']?>" class="fafa-icons change-status" title="<?php echo $data['Test']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['Test']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "Test" data-id="<?php echo base64_encode($data['Test']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['Test']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tbody> 
                  
                </table>
               
             
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea").removeAttr('readonly');
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea").serializeArray();
      $(this).closest('tr').find("input,textarea").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editTestLevel',
          data : {data : data},
          success : function(resp){
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          
          }
      });
    })
  </script>
     <script>
$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      //}
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
    let i= 100;
    $(document).on('click','.append-test-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i> <select class="form-control" name="data['+i+'][Test][test_level_id]"> <option value="">Select Test Level</option> <?php foreach($levels as $level){?> <option value="<?php echo $level['TestLevel']['id']?>"><?php echo $level['TestLevel']['level']?></option> <?php }?> </select> </td><td> <i class="fa input-error1"></i> <input type="text" class="form-control required" required placeholder="Enter Test Name" name="data['+i+'][Test][title]" value="<?php echo @$test['Test']['title']?>"> <input type="hidden" name="data['+i+'][Test][id]" value="<?php echo @$testId?>"> </td><td> <i class="fa input-error1"></i> <input type="text" class="form-control number required" required placeholder="Hour" name="data['+i+'][Test][hour]" > </td><td> <input type="text" class="form-control number required" required placeholder="Minutes" name="data['+i+'][Test][minute]" > </td><td> <a href="javascript:void(0);" class="append-test-a fafa-icons"><i class="fa fa-plus"></i></a><a href="javascript:void(0);" class="delete-test-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#stream-table').append(div);
    
    });

    $(document).on('click','.delete-test-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });
  })
</script>
