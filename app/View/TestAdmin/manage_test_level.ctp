<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 18%;
    position: absolute;
}

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Test Level
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTestLevel">Level</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Test Level</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form role="form" id="add-course-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Level</th>
                  <th>Order</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="stream-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required" required placeholder="Enter Test Level" name="data[1][TestLevel][level]" value="<?php echo @$level['TestLevel']['level']?>">
                    <input type="hidden" name="data[1][TestLevel][id]" value="<?php echo @$levelId?>">
                  </td>
                  <td>
                      <input type="text" class="form-control required number" required placeholder="Level Order" name="data[1][TestLevel][level_order]" value="<?php echo @$data['TestLevel']['order']?>">
                      
                    </td>
                  <td>
                    <a href="javascript:void(0);" class="append-testlevel-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Test Level Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Level</th>
                    <th>order</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($levels as $data){?>
                  <tr>
                    <td>
                      <input type="text" class="form-control required" required readonly placeholder="Enter Level" name="level" value="<?php echo $data['TestLevel']['level']?>">
                      <input type="hidden" name="id" value="<?php echo $data['TestLevel']['id']?>">
                    </td>
                    <td>
                      <input type="text" class="form-control required" required readonly placeholder="Level Order" name="level_order" value="<?php echo @$data['TestLevel']['level_order']?>">
                      
                    </td>
                    
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                      <a style="display:none" class="fafa-icons save" title="Save" href="javascript:void(0);"><i class="fa fa-save"></i></a> 
                      <a model = "TestLevel" data-id="<?php echo $data['TestLevel']['id']?>" class="fafa-icons change-status" title="<?php echo $data['TestLevel']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['TestLevel']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "TestLevel" data-id="<?php echo base64_encode($data['TestLevel']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['TestLevel']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tbody> 
                  
                </table>
               
             
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea").removeAttr('readonly');
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea").serializeArray();
      $(this).closest('tr').find("input,textarea").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editTestLevel',
          data : {data : data},
          success : function(resp){
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          
          }
      });
    })
  </script>
     <script>
$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      //}
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
    let i= 100;
    $(document).on('click','.append-testlevel-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i><input type="text" class="form-control required" required placeholder="Enter Test Level" name="data['+i+'][TestLevel][level]" > </td><td><input type="text" class="form-control required number" required placeholder="Level Order" name="data['+i+'][TestLevel][level_order]" value="<?php echo @$data['TestLevel']['order']?>"></td><td> <a href="javascript:void(0);" class="append-testlevel-a fafa-icons"><i class="fa fa-plus"></i></a><a href="javascript:void(0);" class="delete-testlevel-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#stream-table').append(div);
    
    });

    $(document).on('click','.delete-testlevel-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });
  })
</script>
