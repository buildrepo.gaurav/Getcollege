
<style>
  a.fafa-icons{
      margin-left: 10px;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Questions
        <a href="<?php echo HTTP_ROOT?>TestAdmin/addTestQuestion" class="btn btn-primary">Add Question</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageQuestion">Manage Questions</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Questions Listing</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="college-listing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  
                  <th>Level</th>
                  <th>Test</th>
                  <th>Section</th>
                  <th>Question</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($questions as $ques){?>
                <tr>
                  
                  <td>
                    <?php echo $ques['TestLevel']['level']?>
                  </td>
                  <td>
                    <?php echo $ques['Test']['title']?>
                  </td>
                  <td>
                    <?php echo $ques['TestSection']['title']?>
                  </td>
                  <td>
                    <?php echo $ques['TestQuestion']['question']?>
                  </td>
                  
                  <td>
                    <a target="_blank" class="fafa-icons" title="Edit" href="<?php echo HTTP_ROOT.'TestAdmin/addTestQuestion/'.$ques['TestQuestion']['id']?>"><i class="fa fa-edit"></i></a>
                    <!-- <a target="_blank" class="fafa-icons" title="Manage Options" href="<?php echo HTTP_ROOT.'TestAdmin/manageOption/'.$ques['TestQuestion']['id']?>"><i class="fa fa-info"></i></a> -->
                   
                    <a  model = "TestQuestion" data-id="<?php echo $ques['TestQuestion']['id']?>" class="fafa-icons change-status" title="<?php echo $ques['TestQuestion']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0);"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $ques['TestQuestion']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a> 
                    <a model = "TestQuestion" data-id="<?php echo base64_encode($ques['TestQuestion']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0);"><i class="fa <?php echo @$ques['TestQuestion']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    
                  </td>
                </tr>
                <?php }?>
                
                </tfoot>
              </table>
             
            </div>
            <!-- /.box-body -->
            
          </div>
         
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

     <script>
$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      //}
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       

  })
</script>