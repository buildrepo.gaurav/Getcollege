<style type="text/css">
  .error
  {
    color: red;
  }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Question
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTestQuestion">Manage Questions</a></li>
        <li><a href="<?php echo HTTP_ROOT?>TestAdmin/addTestQuestion">Add Question</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Question</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" id="add-question">
                  <input type="hidden" name="data[TestQuestion][id]" value="<?php echo @$testQuestionId?>">
                  
                  <div class="box-body">
                    <div class="form-group col-md-6">
                      <label for="exampleInputEmail1">Test Level<span style="color: red">*</span></label>
                      <i class="fa input-error"></i>
                       <select class="form-control required level-select" name="data[TestQuestion][test_level_id]" data-id="1">
                      <option value="">Select Level</option>
                      <?php foreach($levels as $level){?>
                        <option value="<?php echo $level['TestLevel']['id']?>" <?php echo @$level['TestLevel']['id'] == @$testquestion['TestQuestion']['test_level_id'] ? "selected" : ""; ?>><?php echo $level['TestLevel']['level']?></option>
                      <?php }?>
                    </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Test<span style="color: red">*</span></label>
                      <i class="fa input-error"></i>
                       <select class="form-control required test-select" id="program-select-1" name="data[TestQuestion][test_id]" data-id="1">
                      <option value="">Select Test</option>
                      <?php if(!empty($testQuestionId)){
                              foreach($tests as $test){
                        ?>
                        <option value="<?php echo $test['Test']['id']?>" <?php echo $test['Test']['id'] == $testquestion['TestQuestion']['test_id'] ? "selected" : ""; ?>><?php echo $test['Test']['title']?></option>
                      <?php }}?>
                    </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="">Select Test Section<span style="color: red">*</span></label>
                      <i class="fa input-error"></i>
                     <select class="form-control required section-select" id="course-select-1" name="data[TestQuestion][test_section_id]" data-id="1">
                      <option value="">Select Test Section</option>
                      <?php if(!empty($testQuestionId)){
                              foreach($sections as $sec){
                        ?>
                        <option value="<?php echo $sec['TestSection']['id']?>" <?php echo $sec['TestSection']['id'] == $testquestion['TestQuestion']['test_section_id'] ? "selected" : ""; ?>><?php echo $sec['TestSection']['title']?></option>
                      <?php }}?>
                    </select>
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label for="exampleInputPassword1">Question<span style="color: red">*</span></label>
                      <i class="fa input-error"></i>
                      <input class="form-control required" placeholder="Question" name="data[TestQuestion][question]"  value="<?php echo @$testquestion['TestQuestion']['question']?>">
                    </div>
                    <?php if(empty($testQuestionId)){?>
                    <div class="append-div">
                      <div class="form-group col-md-6">
                        
                        <div class="col-md-6">
                          <label for="exampleInputPassword1">Option 1<span style="color: red">*</span></label>
                          <i class="fa input-error"></i>
                          <input class="form-control required" placeholder="Option 1" name="data[TestQuestionOption][1]"  >
                        </div>
                        <div class="col-md-6">
                          <label for="exampleInputPassword1">Is Answer</label>
                          <i class="fa input-error"></i>
                          <input type="radio" class=" required" placeholder="Option 1" name="data[answer_id]" value="1">
                          <a title="Add More Option" href="javascript:void(0)" class="add-option"><i class="fa fa-plus"></i></a>
                        </div>
                      </div>
                    </div>  
                    <?php }else{?>
                      
                      <div class="append-div">
                      <?php $i=1; foreach($testquestion['TestQuestionOption'] as $opt){?>
                      <div class="form-group col-md-6" id="option_<?php echo $i;?>">
                        
                        <div class="col-md-6">
                          <label for="exampleInputPassword1">Option <?php echo $i?></label>
                          <i class="fa input-error"></i>
                          <input class="form-control required" placeholder="Option <?php echo $i?>" name="data[TestQuestionOption][<?php echo $i?>]" value="<?php echo $opt['option']?>">
                          
                        </div>
                        <div class="col-md-6">
                          <label for="exampleInputPassword1">Is Answer</label>
                          <i class="fa input-error"></i>
                          <input type="radio" class="required" placeholder="Option <?php echo $i?>" name="data[answer_id]" value="<?php echo $i?>" <?php echo $opt['id'] == $testquestion['TestQuestion']['answer_id'] ? "checked" : '';?> >
                          <a title="Add More Option" href="javascript:void(0)" class="add-option"><i class="fa fa-plus"></i></a>
                          <?php if($i > 1){?>
                            <a href="javascript:void(0)" class="remove-option" data-id="<?php echo $i;?>"><i class="fa fa-trash"></i></a>
                            <?php }?>
                        </div>
                      </div>
                      <?php $i++; }?>
                    </div>

                    <?php }?>
                    
                    <div class="box-footer col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  </div>
                  <!-- /.box-body -->

                  
                </form>


            
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script>
    $('#add-question').validate();

   $(document).on('change','.level-select',function(){
      var key = $(this).val();
        //console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'TestAdmin/findTest/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('.test-select').children().remove();
                        $('.test-select').append(resp);
                        $('.section-select').children().remove();
                        
                    }
              })
    });
    $(document).on('change','.test-select',function(){
      var key = $(this).val();
        //console.log(ajax_url);
        $.ajax({
                  type : 'post',
                  url : baseUrl +'TestAdmin/findSection/'+key,
                  success : function(response){
                        resp = $.parseJSON(response);
                        //console.log(response);
                        $('.section-select').children().remove();
                        $('.section-select').append(resp);
                        
                    }
              })
    });
    let i=2;
    <?php if(!empty($testQuestionId)){?>
      i = <?php echo @count($testquestion['TestQuestionOption'])?>+1;
    <?php }?>
    $(document).on('click','.add-option',function(){
      let app = '<div class="form-group col-md-6" id="option_'+i+'"> <div class="col-md-6"> <label for="exampleInputPassword1">Option '+i+'</label> <i class="fa input-error"></i> <input class="form-control required" placeholder="Option '+i+'" name="data[TestQuestionOption]['+i+']" required > </div><div class="col-md-6"> <label for="exampleInputPassword1">Is Answer</label> <i class="fa input-error"></i> <input type="radio" class=" required" name="data[answer_id]" required value="'+i+'"> <a title="Add More Option" href="javascript:void(0)" class="add-option"><i class="fa fa-plus"></i></a> <a href="javascript:void(0)" class="remove-option" data-id="'+i+'"><i class="fa fa-trash"></i></a> </div></div>';
      $('.append-div').append(app);
      i++;
    })
    $(document).on('click','.remove-option',function(){
      let did = $(this).attr('data-id');
      $('#option_'+did).remove();
    });
</script>
  
  