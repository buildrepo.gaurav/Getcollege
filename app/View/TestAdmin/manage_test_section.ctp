<style type="text/css">
  .input-error1 {
    float: right;
    margin: 10px 18%;
    position: absolute;
}

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Test Section
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo HTTP_ROOT?>Admins/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo HTTP_ROOT?>TestAdmin/manageTestSection">Section</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Test Section</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form role="form" id="add-course-form" method='post' enctype="multipart/form-data">
              
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Test</th>
                  <th>Section</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="stream-table">
                <tr>
                  <td>
                  <i class="fa input-error1"></i>
                    <select class="form-control required" name="data[1][TestSection][test_id]">
                      <option value="">Select Test</option>
                      <?php foreach($tests as $test){?>
                        <option value="<?php echo $test['Test']['id']?>" <?php echo $test['Test']['id'] == @$section['TestSection']['id'] ? "selected" : "";?>><?php echo $test['Test']['title']?></option>
                      <?php }?>
                    </select>
                  </td>
                  <td>
                  <i class="fa input-error1"></i>
                    <input type="text" class="form-control required" required placeholder="Enter Section Name" name="data[1][TestSection][title]" value="<?php echo @$section['TestSection']['title']?>">
                    <input type="hidden" name="data[1][TestSection][id]" value="<?php echo @$sectionId?>">
                  </td>
                  
                  <td>
                    <a href="javascript:void(0);" class="append-testsection-a"><i class="fa fa-plus"></i></a>
                  </td>
                </tr>
                </tfoot>
              </table>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save All</button>
              </div>
            </form>


            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">All Test Section Listing</h3>
              </div>
              <table id="facility-form" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Test</th>
                    <th>Section</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach($sections as $data){?>
                  <tr>
                  <td>
                     <?php echo $data['Test']['title']?>
                      
                    </td>
                    <td>
                     <?php echo $data['TestSection']['title']?>
                      
                    </td>
                    
                    <td>
                      <a class="fafa-icons edit" title="Edit" href="<?php echo HTTP_ROOT.'TestAdmin/ManageTestSection/'.$data['TestSection']['id']?>"><i class="fa fa-edit"></i></a>
                      
                      <a model = "TestSection" data-id="<?php echo $data['TestSection']['id']?>" class="fafa-icons change-status" title="<?php echo $data['TestSection']['status'] == '1' ? 'Active' : 'Inactive';?>" href="javascript:void(0)"><img width="30px" src="<?php echo HTTP_ROOT.'img/'?><?php echo $data['TestSection']['status'] == '1' ? 'active.png' : 'inactive.png';?>"></a>
                      <a model = "TestSection" data-id="<?php echo base64_encode($data['TestSection']['id'])?>" class="fafa-icons delete-row" title="Delete" href="javascript:void(0)"><i class="fa <?php echo @$data['TestSection']['is_delete'] == 0 ? 'fa-trash-o' : 'fa-trash';?>"></i></a> 
                    </td>
                  </tr>
                  <?php }?>
                  </tbody> 
                  
                </table>
               
             
          </div>
          <!-- /.box -->

          

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script>
    $(document).on('click','.edit',function(){
      $(this).hide();
      $(this).closest('tr').find("input,textarea").removeAttr('readonly');
      $(this).next().show();

    })
    $(document).on('click','.save',function(){
      $(this).hide();
      $(this).prev().show();

      let data = $(this).closest('tr').find("input,textarea").serializeArray();
      $(this).closest('tr').find("input,textarea").attr('readonly','true');
      $.ajax({
          type : 'post',
          url : '<?php echo HTTP_ROOT?>/AjaxEdit/editTestLevel',
          data : {data : data},
          success : function(resp){
            if(resp == 'true')
              alert('Data Save successfully.');
            else
              alert('Duplicate entry cannot be saved.');
          
          }
      });
    })
  </script>
     <script>
$(document).ready(function(){

    $(document).on('click','.delete-row',function(){
       //if(window.confirm("Are you sure you want to delete this ?")){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/delete/'+model+'/'+id,
                  success : function(response){
                        if(response == 0){
                            th.children('i').removeClass("fa-trash");
                            th.children('i').addClass("fa-trash-o");
                          }else{
                            th.children('i').removeClass("fa-trash-o");
                            th.children('i').addClass("fa-trash");
                          }
                       },
                async: false
              });
         /*table.row( $(this).parents('tr') )
        .remove()
        .draw();*/
      //}
  });

    $(document).on('click','.change-status',function(){
        let th = $(this);
        let id = $(this).attr('data-id');
        let model = $(this).attr('model');
        $.ajax({
                  type : 'post',
                  url : baseUrl +'Colleges/changeStatus/'+model+'/'+id,
                  success : function(response){
                          console.log(response);
                          if(response == 0){
                            let src = '<?php echo HTTP_ROOT?>img/inactive.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Inactive');
                          }else{
                            let src = '<?php echo HTTP_ROOT?>img/active.png';
                            th.children('img').attr('src',src);
                            th.attr('title','Active');
                          }
                       },
                async: false
              });
      
  });
       
    let i= 100;
    $(document).on('click','.append-testsection-a',function(){
      
      let div = '<tr id="append_'+i+'"> <td> <i class="fa input-error1"></i> <select class="form-control required" name="data['+i+'][TestSection][test_id]"> <?php foreach($tests as $test){?> <option value="<?php echo $test['Test']['id']?>"><?php echo $test['Test']['title']?></option> <?php }?> </select> </td><td> <i class="fa input-error1"></i> <input type="text" class="form-control required" required placeholder="Enter Section Name" name="data['+i+'][TestSection][title]" value="<?php echo @$level['TestSection']['level']?>"> </td><td> <a href="javascript:void(0);" class="append-testsection-a fafa-icons"><i class="fa fa-plus"></i></a><a href="javascript:void(0);" class="delete-testsection-tr fafa-icons" tr-id="append_'+i+'"><i class="fa fa-trash"></i></a> </td></tr>';
      i++;
      $('#stream-table').append(div);
    
    });

    $(document).on('click','.delete-testsection-tr',function(){
      let tr_id = $(this).attr('tr-id');
      $('#'+tr_id).remove();
    });
  })
</script>
